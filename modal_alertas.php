
<!-- Modal -->
<div class="modal fade" id="modAlertas" tabindex="-1" role="dialog" aria-labelledby="modAlertas" aria-hidden="true">
  <div class="modal-dialog" style="width:300px;">
        <div class="modal-content">
            <div style="padding:20px; text-align: center;">
                <h4>Ocurrio un Problema!</h4>
                <div class="alert alert-danger" id="texto_alerta_pop" style="font-size:15px;"> 
                    
                </div>
                <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Cerrar Mensaje</button>
                <br>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- Modal -->
<div class="modal fade" id="modExitos" tabindex="-1" role="dialog" aria-labelledby="modExitos" aria-hidden="true">
  <div class="modal-dialog" style="width:300px;">
        <div class="modal-content">
            <div style="padding:20px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>Bien!</h4>
                <div class="alert alert-success" id="texto_exito_pop" style="font-size:15px;"> 
                    
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>