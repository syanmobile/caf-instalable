   <div class="navbar navbar-default navbar-fixed-top" role="navigation" id="ocultar_cabecera" style="background-color: #2070B1 !important">
  <div class="container">
    <div class="collapse navbar-collapse" style="float:left; background-color: #2070B1 !important">
      <ul class="nav navbar-nav" style="background-color: #2070B1 !important"> 
                
        <li style="border-right: 1px solid #fff;">
        <a href="<?php echo $url_base; 
          ?>index.php" style="color:#fff !important;"><span class="glyphicon glyphicon-home" style="font-size: 14px; padding-right: 2px;"></span></a>
        </li>
        
        <li class="dropdown" style="border-right: 1px solid #fff;">
          
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style=" color:#fff !important;"><span class="glyphicon glyphicon-folder-open" style="font-size: 14px; padding-right: 2px;"></span> ACTIVOS <span class="caret"></span></a>

          <ul class="dropdown-menu"> 
            <?php
            foreach($_tipo_activos_menu as $tac){ 
              if(in_array("act_".strtolower($tac)."_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('acf_listado.php','&filtro_tipo=<? echo $tac; ?>');"><span class="<? echo $_tipo_activos_icono[$tac]; ?>" style="font-size: 14px; padding-right: 3px;"></span> <? echo $_tipo_activo[$tac]; ?></a></li><?
              } 
            } 
            ?>                
          </ul>
        </li>
        <?php 

        if(
          in_array("pres_realizar",$opciones_persona) or 
          in_array("asig_realizar",$opciones_persona) or 
          in_array("devo_realizar",$opciones_persona) or 
          in_array("reub_realizar",$opciones_persona) or 
          in_array("ingr_realizar",$opciones_persona) or 
          in_array("baja_realizar",$opciones_persona) or 
          in_array("etiq_realizar",$opciones_persona) 
        ){
          ?>
          <li class="dropdown" style="border-right: 1px solid #fff;">
            
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style=" color:#fff !important;"><span class="glyphicon glyphicon-thumbs-up" style="font-size: 14px; padding-right: 2px;"></span> OPERACIONES <span class="caret"></span></a>

            <ul class="dropdown-menu">
              <?php  
              if(in_array("ingr_realizar",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('ing_nuevo2.php','');"><span class="glyphicon glyphicon-plus" style="font-size: 14px; padding-right: 3px;"></span> Ingreso de Activos/Insumos</a></li><?
              } 
              if(in_array("pres_realizar",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('acf_prestamo.php','');"><span class="glyphicon glyphicon-arrow-left" style="font-size: 14px; padding-right: 3px;"></span> Prestamo de Activos</a></li><?
              } 
              if(in_array("asig_realizar",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('acf_asignacion.php','');"><span class="glyphicon glyphicon-arrow-left" style="font-size: 14px; padding-right: 3px;"></span> Asignación de Activos</a></li><?
              } 
              if(in_array("devo_realizar",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('acf_devolucion.php','');"><span class="glyphicon glyphicon-arrow-right" style="font-size: 14px; padding-right: 3px;"></span> Devolución de Activos</a></li><?
              } 
              if(in_array("reub_realizar",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('acf_reubicacion.php','');"><span class="glyphicon glyphicon-random" style="font-size: 14px; padding-right: 3px;"></span> Reubicación de Activos</a></li><?
              }
              if(in_array("baja_realizar",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('acf_baja.php','');"><span class="glyphicon glyphicon-remove" style="font-size: 14px; padding-right: 3px;"></span> Baja de Activo</a></li><?
              }
              ?>                
            </ul>
          </li>
          <?php
        }

        /*
        if(in_array("toma_ver",$opciones_persona)){
          ?> 
          <li class="dropdown" style="border-right: 1px solid #fff;">
            
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style=" color:#fff !important;"><span class="glyphicon glyphicon-edit" style="font-size: 14px; padding-right: 2px;"></span> INVENTARIOS <span class="caret"></span></a>

            <ul class="dropdown-menu">
              <li><a style="color:#333 !important;" href="javascript:carg3('inv_listados.php','');"><span class="glyphicon glyphicon-edit" style="font-size: 14px; padding-right: 3px;"></span> Tomas de Inventario</a></li>
            </ul>
          </li>
          <?
        }
        */

        if(_opc("activo_INS") == "1" && in_array("invi_ver",$opciones_persona)){
          ?> 
          <li class="dropdown" style="border-right: 1px solid #fff;">
            
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style=" color:#fff !important;"><span class="glyphicon glyphicon-shopping-cart" style="font-size: 14px; padding-right: 2px;"></span> INSUMOS <span class="caret"></span></a>

            <ul class="dropdown-menu">
              <li><a style="color:#333 !important;" href="javascript:carg3('ins_inventario.php','');"><span class="glyphicon glyphicon-tasks" style="font-size: 14px; padding-right: 3px;"></span> Stock de Insumos</a></li>
              <?
              if(in_array("cons_realizar",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('ins_entrega.php','');"><span class="glyphicon glyphicon-shopping-cart" style="font-size: 14px; padding-right: 3px;"></span> Consumo de Insumos</a></li><?
              }
              if(in_array("tomi_realizar",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('ins_control.php','');"><span class="glyphicon glyphicon-check" style="font-size: 14px; padding-right: 3px;"></span> Ajuste Stock de Insumos</a></li><?
              }
              ?>                   
            </ul>
          </li>
          <?
        }



        if(
          in_array("pers_ver",$opciones_persona) or 
          in_array("auxi_ver",$opciones_persona)
        ){
          ?> 
          <li class="dropdown" style="border-right: 1px solid #fff;">
            
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style=" color:#fff !important;"><span class="glyphicon glyphicon-user" style="font-size: 14px; padding-right: 2px;"></span> RECURSOS <span class="caret"></span></a>

            <ul class="dropdown-menu">
              <?php
              if(in_array("pers_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('per_listado.php','');"><span class="glyphicon glyphicon-user" style="font-size: 14px; padding-right: 3px;"></span> Personas</a></li><?
              } 
              if(in_array("auxi_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('aux_listado.php','');"><span class="glyphicon glyphicon-lock" style="font-size: 14px; padding-right: 3px;"></span> Proveedores</a></li><?
              }
              ?>                   
            </ul>
          </li>
          <?
        }

        if(
          in_array("mant_ver",$opciones_persona) or 
          in_array("cont_ver",$opciones_persona) or 
          in_array("gara_ver",$opciones_persona) or 
          in_array("etiq_ver",$opciones_persona) or 
          in_array("fisc_ver",$opciones_persona)
        ){
          ?>
          <li class="dropdown" style="border-right: 1px solid #fff;">
            
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style=" color:#fff !important;"><span class="glyphicon glyphicon-th-list" style="font-size: 14px; padding-right: 2px;"></span> TRABAJOS <span class="caret"></span></a>

            <ul class="dropdown-menu">
              <?php
              if(in_array("mant_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('man_listado.php','');"><span class="glyphicon glyphicon-wrench" style="font-size: 14px; padding-right: 3px;"></span> Mantención de Activos</a></li><?
              } 
              if(in_array("gara_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('gar_listado.php','');"><span class="glyphicon glyphicon-dashboard" style="font-size: 14px; padding-right: 3px;"></span> Garantías de Activos</a></li><?
              }
              if(in_array("cont_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('ctr_listado.php','');"><span class="glyphicon glyphicon-flag" style="font-size: 14px; padding-right: 3px;"></span> Control Toma Inv. Activo por Ubicación</a></li><?
              } 
              if(in_array("etiq_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('acf_sin_etiquetas.php','');"><span class="glyphicon glyphicon-barcode" style="font-size: 14px; padding-right: 3px;"></span> Asociar Activos sin Etiquetas</a></li><?
              } 
              if(in_array("etim_realizar",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:alert('No disponible');"><span class="glyphicon glyphicon-print" style="font-size: 14px; padding-right: 3px;"></span> Impresión de Etiquetas</a></li><?
              }
              ?>
            </ul>
          </li>
          <? 
        }

        if(
          in_array("inf1_ver",$opciones_persona) or 
          in_array("inf2_ver",$opciones_persona) or 
          in_array("inf3_ver",$opciones_persona) or 
          in_array("inf4_ver",$opciones_persona) or 
          in_array("inf5_ver",$opciones_persona) or 
          in_array("inf6_ver",$opciones_persona) or 
          in_array("inf7_ver",$opciones_persona) or 
          in_array("inf8_ver",$opciones_persona) or 
          in_array("inf9_ver",$opciones_persona) or 
          in_array("inf10_ver",$opciones_persona) or 
          in_array("inf11_ver",$opciones_persona) or 
          in_array("inf12_ver",$opciones_persona) or 
          in_array("inf13_ver",$opciones_persona) or 
          in_array("inf14_ver",$opciones_persona) or 
          in_array("inf15_ver",$opciones_persona) or 
          in_array("inf16_ver",$opciones_persona) or 
          in_array("inf17_ver",$opciones_persona) or 
          in_array("inf18_ver",$opciones_persona) or 
          in_array("inf19_ver",$opciones_persona) or 
          in_array("inf20_ver",$opciones_persona) or 
          in_array("inf21_ver",$opciones_persona) or 
          in_array("inf22_ver",$opciones_persona) or 
          in_array("inf23_ver",$opciones_persona) or 
          in_array("inf24_ver",$opciones_persona)
        ){
          ?>
          <li style="border-right: 1px solid #fff;">
            <a href="javascript:carg3('informes.php','');" style="color:#fff !important;"><span class="glyphicon glyphicon-print" style="font-size: 14px; padding-right: 2px;"></span> INFORMES</a>
          </li>
          <?
        } 

        if(
          in_array("lug_ver",$opciones_persona) or 
          in_array("ubi_ver",$opciones_persona) or 
          in_array("pro_ver",$opciones_persona) or 
          in_array("insu_ver",$opciones_persona) or 
          in_array("cat_ver",$opciones_persona) or 
          in_array("ipc_ver",$opciones_persona) or 
          in_array("unid_ver",$opciones_persona) or 
          in_array("ccos_ver",$opciones_persona)
        ){
          ?>
          <li class="dropdown" style="border-right: 1px solid #fff;">
            
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style=" color:#fff !important;"><span class="glyphicon glyphicon-th" style="font-size: 14px; padding-right: 2px;"></span> TABLAS <span class="caret"></span></a>

            <ul class="dropdown-menu">  
              <?php 
              if(in_array("lug_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('bod_listado.php','');"><span class="glyphicon glyphicon-globe" style="font-size: 14px; padding-right: 3px;"></span> Lugares</a></li><?
              } 
              if(in_array("ubi_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('ubi_listado.php','');"><span class="glyphicon glyphicon-map-marker" style="font-size: 14px; padding-right: 3px;"></span> Ubicaciones</a></li><?
              } 
              if(in_array("pro_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('pro_listado.php','');"><span class="glyphicon glyphicon-briefcase" style="font-size: 14px; padding-right: 3px;"></span> Productos</a></li><?
              } 
              if(in_array("insu_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('ins_listado.php','');"><span class="glyphicon glyphicon-briefcase" style="font-size: 14px; padding-right: 3px;"></span> Insumos</a></li><?
              } 
              if(in_array("cat_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('cat_listado.php','');"><span class="glyphicon glyphicon-tag" style="font-size: 14px; padding-right: 3px;"></span> Clasificación de Productos</a></li><?
              }
              if(in_array("unid_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('uni_listado.php','');"><span class="glyphicon glyphicon-tag" style="font-size: 14px; padding-right: 3px;"></span> Unidades de Productos</a></li><?
              } 
              if(in_array("ccos_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('cco_listado.php','');"><span class="glyphicon glyphicon-usd" style="font-size: 14px; padding-right: 3px;"></span> Centros de Costo</a></li><?
              }    
              if(in_array("ipc_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('ipc_listado.php','');"><span class="glyphicon glyphicon-usd" style="font-size: 14px; padding-right: 3px;"></span> Tabla IPC</a></li><?
              }   
              ?>
            </ul>
          </li>
          <?
        }

        if(
          in_array("mie_ver",$opciones_persona) or 
          in_array("cam_Ver",$opciones_persona) or 
          in_array("usua_ver",$opciones_persona) or 
          in_array("pfl_ver",$opciones_persona)
        ){
          ?>
          <li class="dropdown" style="border-right: 1px solid #fff;">
            
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style=" color:#fff !important;"><span class="glyphicon glyphicon-cog" style="font-size: 14px; padding-right: 2px;"></span> CONFIGURACIÓN <span class="caret"></span></a>

            <ul class="dropdown-menu">  
              <?php
              if(in_array("mie_ver",$opciones_persona)){
                if(in_array("mie_editar",$opciones_persona)){
                  ?><li><a style="color:#333 !important;" href="javascript:carg3('cfg_configuracion_editar.php','');"><span class="glyphicon glyphicon-home" style="font-size: 14px; padding-right: 3px;"></span> Mi Empresa</a></li><?
                }else{
                  ?><li><a style="color:#333 !important;" href="javascript:carg3('cfg_configuracion.php','');"><span class="glyphicon glyphicon-home" style="font-size: 14px; padding-right: 3px;"></span> Mi Empresa</a></li><?
                }
              }
              if(in_array("cam_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('cam_listado.php','');"><span class="glyphicon glyphicon-tags" style="font-size: 14px; padding-right: 3px;"></span> Campos Personalizados</a></li><?
              } 
              if(in_array("usua_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('usu_listado.php','');"><span class="glyphicon glyphicon-user" style="font-size: 14px; padding-right: 3px;"></span> Usuarios</a></li><?
              }
              if(in_array("pfl_ver",$opciones_persona)){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('pfl_listado.php','');"><span class="glyphicon glyphicon-check" style="font-size: 14px; padding-right: 3px;"></span> Perfiles de Usuarios</a></li><?
              }

              if($_SESSION["per_conectado"]["per_id"] == 1){
                ?><li><a style="color:#333 !important;" href="javascript:carg3('implementacion.php','');"><span class="glyphicon glyphicon-cog" style="font-size: 14px; padding-right: 3px;"></span> Implementación CAF</a></li><?
                ?><li><a style="color:#333 !important;" href="javascript:carg2('inicio.php','');"><span class="glyphicon glyphicon-phone" style="font-size: 14px; padding-right: 3px;"></span> Versión Mobile</a></li><?
              }
              ?>
            </ul>
          </li>
          <?
        } 
        ?>
      </ul>
    </div>  
          
    <div class="collapse navbar-collapse" style="float:right; background-color: #fff;">  
      <ul class="nav navbar-nav" style="background-color: #2070B1 !important">
          
        <li class="dropdown" style="border-right: 1px solid #fff;">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" 
            style=" color:#fff !important;"><span class="glyphicon glyphicon-user"></span> <?
          $resusr = mysqli_query($cnx,"select * from conectados a 
          left join personas b on a.con_per_id = b.per_id  
          order by b.per_nombre asc");
          $online = mysqli_num_rows($resusr);
          echo $online;
          ?></a>
            <ul class="dropdown-menu pull-right">
              <?php
            if($online>0){ 
                while($rowusr = mysqli_fetch_array($resusr)){
                    ?>
                    <li>
                      <a style="color:#333 !important;" href="#"><?php echo $rowusr["per_nombre"]; ?></a>
                    </li>
                    <?

                }
            }
            ?>
            </ul>
        </li> 
        <li style="border-right: 1px solid #fff;"><a href="http://www.controldeactivos.cl/ayuda/" target="_blank" title="AYUDA" style="color:#fff !important;"><span class="glyphicon glyphicon-question-sign
" style="font-size: 14px;"></span></a></li>
        <li><a href="<?php echo $url_base; 
          ?>logout.php" title="SALIR" style="color:#fff !important;"><span class="glyphicon glyphicon-off" style="font-size: 14px;"></span></a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div> 
</div>