<?php
session_start();
session_unset();
unset($_SESSION);
$_SESSION = array();
header("Location: login.php");
?>