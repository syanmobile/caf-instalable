<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
construir_breadcrumb("Lista de Activos Fijos");
//----------------------------------------------------------------------------------------
$pagina = "acf_listado.php"; 

include("ayuda.php"); 
require("filtros.php");
?> 

<input type="hidden" name="pagina" id="pagina" value="<? echo $pagina; ?>" class="campos">
<?
$sql_paginador = sql_activos_fijos_rapido("*"," $filtros ORDER BY acf.acf_producto asc, acf.acf_serie asc","s",""); 
$cant_paginador = 10;
require("../../_paginador.php");
$res = mysqli_query($cnx,$sql_final); 
if(mysqli_num_rows($res) > 0){
	?>
	<table class="table table-striped table-bordered tabledrag table-condensed" style="margin-bottom: 0px;"> 
	<thead>
		<tr>  
			<th width="65"></th> 
			<th width="1">Imagen</th>
 
			<th>Códigos</th>
			<th>Producto</th>
			 
			<th>Lugar</th>
			<th>Ubicación</th>
			<th>Responsable</th>
			<th>Info Ingreso</th>

			<th>Clasificación</th>
			<th>Más Información</th> 

			<th width="1">Estado</th>
		</tr>
	</thead>
	<tbody> 
	<?
	while($row = mysqli_fetch_array($res)){  
		?>
		<tr> 
			<td><? 
			construir_boton("acf_info.php","&acf=".$row["acf_id"],"buscar","Ver Activo Fijo"); 
			construir_boton("acf_editar.php","&id=".$row["acf_id"],"editar","Editar Activo Fijo");
			construir_boton("visor('acf_pdf.php?acf=".$row["acf_id"]."')","1","imprimir","Imprimir Ficha",4);
			if($row["acf_etiqueta"] <> ""){
				construir_boton("abrir_pagina('cdu_pdf.php?cdu[]=".$row["acf_etiqueta"]."')","1","barcode","Imprimir Código",4);
			}
			?></td> 
			<td><? 
			$imagen = ($row["pro_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$row["pro_imagen"];
			?><img src="<?php echo $imagen; ?>" style="width:40px !important;"></td> 
			<td>
				<b>Cod.Activo:</b> <?php echo $row["acf_codigo"]; ?><br>
				<b>Serie:</b> <?php echo $row["acf_serie"]; ?><br>
				<b>Etiqueta:</b> <?php echo $row["acf_etiqueta"]; ?>
			</td>
			<td>
				<?php echo $row["pro_nombre"]; ?><br>
				<b><?php echo $row["pro_codigo"]; ?></b>
			</td> 	
			<td><?php 
			if($row["ubi_bodega"] <> 0){
				echo $_bodegas[$row["ubi_bodega"]]; ?><br>
				<b><? echo $_bodegas_codigo[$row["ubi_bodega"]]; ?></b><?
			}
			?></td>
			<td><?php
			if($row["acf_ubicacion"] <> 0){
				echo $_ubicaciones[$row["acf_ubicacion"]]; ?><br>
				<b><?php echo $_ubicaciones_codigo[$row["acf_ubicacion"]]; ?></b><?
			}
			?></td> 
			<td><?php echo $_personas[$row["acf_responsable"]]; ?></td> 
			<td>
				Fecha Ingreso: <?php echo _fec($row["acf_fecha_ingreso"],5); ?><br>
				Doc.Ingreso: <? echo $row["acf_nro_factura"]; ?><br>
				Proveedor: <? echo $_SESSION["auxiliares"][$row["acf_proveedor"]]; ?><br>
				Valor Compra (<? echo $_configuraciones["cfg_moneda"]; ?>): <?php 
				echo _num($row["acf_valor"],$_configuraciones["cfg_moneda_decimal"]);
				if($row["acf_valor_sec"] > 0){
					?><br>
					2da Moneda (<? echo $_configuraciones["cfg_moneda_sec"]; ?>): <?php 
					echo _num($row["acf_valor_sec"],$_configuraciones["cfg_moneda_sec_decimal"]); 
				}
				?>
			</td>  
			<td>
				<?php echo $_categorias[$row["pro_categoria"]]; ?><br>
				<b><?php echo $_categorias_codigo[$row["pro_categoria"]]; ?></b>
			</td> 
			<td>
				<b>C.Costo:</b> <? echo $_centro_costos_corto[$row["acf_cco_id"]]; ?>
				<?
				foreach($_campo_pro as $dato){
					if($row["pro_extra_".$dato[1]] <> ""){
						?><br><b><? echo $dato[0]; ?>:</b> <? echo $row["pro_extra_".$dato[1]]; ?><?
					}
				}
				foreach($_campo_acf as $dato){
					if($row["acf_extra_".$dato[1]] <> ""){
						?><br><b><? echo $dato[0]; ?>:</b> <? echo $row["acf_extra_".$dato[1]]; ?><?
					}
				}
				?>
			</td> 
			<td><?php activo_fijo_estado($row["acf_disponible"]); ?></td> 
		</tr>
		<?  
	}
	?>
	</tbody>
	</table> 
	<?
	echo $paginador_dibujo;
}else{
	?>
	<div class="alert alert-danger">
		<strong>Sin registros creados</strong>
	</div>
	<?php 
} 
?>