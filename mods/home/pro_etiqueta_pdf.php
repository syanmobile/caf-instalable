<?php 
ob_start(); 
require("../../inc/conf_dentro.php"); 

// para utilizar la librería
define('FPDF_FONTPATH','font/');
require_once('../../pdf/fpdf.php');

// Parametros pdf
$pdf=new FPDF("L","mm",array(102,20));
$pdf->AliasNbPages();

$pdf->AddPage();

$sqlserver = explode(";",_opc("sql_db"));
$cnx2 = mssql_connect($sqlserver[0],$sqlserver[1],$sqlserver[2]);
mssql_select_db($sqlserver[3],$cnx2);

$ancho = 0; 

$i = 0;
$lineas = explode("***",$_REQUEST["lineas"]);
if(count($lineas) > 1){
	foreach($lineas as $linea){
		$datos = explode(";;;",$linea);		

		if($i > 0){  
			$sql = "SELECT * FROM softland.iw_tprod where codProd = '".$datos[0]."' ";
			$res = msmysqli_query($cnx,$sql);
			if(mssql_num_rows($res) > 0){
				$row = mssql_fetch_array($res);
			}
					
			for($i = 1;$i <= $datos[1];$i++){
				$xhoja++;
				if($xhoja > 3){
					$pdf->AddPage();
					$xhoja = 1;
					$ancho = 0;
				}

				$pdf->Image('http://10.9.10.36/panal/pdf/image.php?text='.$row["CodBarra"].'&code=code128&o=1&dpi=72&t=30&r=3&rot=0&f1=-1&f2=14&a1=&a2=NULL&a3=&.png',3 + $ancho,2,28,0,'PNG'); 

				$pdf->Ln(4);

				$pdf->SetFont('Arial','',6); 
				$pdf->Text(3 + $ancho,11,substr($row["DesProd"],0,20));
				$pdf->Text(3 + $ancho,14,$row["CodProd"]);

				$pdf->SetFont('Arial','b',6); 
				$pdf->Text(3 + $ancho,17,"Precio:");
				$pdf->Text(18 + $ancho,17,"$ "._num($row["PrecioBol"]));

				$ancho+= 35;
			}
			
		}
		$i++;
	}
} 

$pdf->Output(); 
?>