<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Nueva Toma de Inventario";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function grabar(){
	if(confirm("Crear este proceso de Toma de Inventario?")){
		carg("inv_save.php","");
	}
} 
</script>
<input type="hidden" name="modo" value="nuevo" class="campos"> 
<div style="width:600px;">
<table class="table table-bordered table-condensed" style="margin-bottom:0px !important;"> 
<thead>
<tr> 
    <th colspan="20">Nueva Toma de Inventario</th>                        
</tr>
</thead>
<tbody>
<tr>
    <th width="130">Lugar:</th>
    <td><?php input_bodega(''); ?></td>
</tr>
<tr>
    <th>Fecha:</th>
    <td><input type="text" id="fecha" name="fecha" class="campos fecha"></td>
</tr>
<tr>
    <th>Nombre Proceso:</th>
    <td><input type="text" name="glosa" class="campos w10"></td>
</tr>
<tr>
    <th>Responsables:</th>
    <td> 
		<div style="width: 100%; height: 300px; overflow: scroll">
			<table class="table table-bordered table-condensed">
			<?php
			$res = sql_personas("*"," ORDER BY per.per_nombre asc ","","");  
			if(mysqli_num_rows($res) > 0){
				while($row = mysqli_fetch_array($res)){
					?>
					<tr>
						<td width="1"><input type="checkbox" name="responsable[]" class="campos" value="<? echo $row["per_id"]; ?>"></td>
						<td><?php echo $row["per_nombre"]; ?></td>
					</tr>
					<?
				}
			}
			?>
			</table>
	   </div>
    </td>
</tr>
</tbody>
</table>
<?php  
construir_boton("grabar()","","grabar","Crear Toma Inventario",4); 
construir_boton("inv_listado.php","","eliminar","Cancelar",2); 
?>  
</div>