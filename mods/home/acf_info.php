<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$res = sql_activos_fijos("*","and acf.acf_id = '$_REQUEST[acf]' ");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<b>ERROR EN CONSULTA</b><br>No se encontró el dato
	</div>
    <?php
	exit();
}
//----------------------------------------------------------------------------------------
$titulo_pagina = "Ficha Activo | ".$datos["pro_codigo"]." - ".$datos["acf_codigo"]." (".$_REQUEST["acf"].")";
construir_breadcrumb($titulo_pagina); 
//----------------------------------------------------------------------------------------
$pagina = "acf_info.php";
include("ayuda.php");
//----------------------------------------------------------------------------------------
?> 
<script language="javascript"> 
function save(){
	var div = document.getElementById("grabacion"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/acf_ajax.php",a+"&modo=datos",div); 
}  
function doc_listado(){
	var div = document.getElementById("dtab_3"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/acf_ajax_doc.php",a+"&modo=listado",div);
} 
function historial(){
	var div = document.getElementById("dtab_4"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/acf_historial.php",a,div);
}
function doc_descargar(archivo){
	window.open("upload/<? echo $_SESSION["key_id"]; ?>/"+archivo);
}
function crear_etiqueta(acf){
	AJAXPOST("mods/home/acf_ajax.php","modo=crear_etiqueta&acf="+acf,document.getElementById("modGeneral_lugar"),false,function(){            
        $('#modGeneral').modal('show');

    });
}
function crear_etiqueta_save(){
	if(document.getElementById("codigo_unico").value == ""){
		alert("Ingrese la etiqueta a crear");
		document.getElementById("codigo_unico").focus();
		return;
	}else{ 
		var div = document.getElementById("form-etiqueta-save"); 
		AJAXPOST("mods/home/acf_ajax.php","modo=crear_save&cdu="+document.getElementById("codigo_unico").value+"&acf=<? echo $datos["acf_id"]; ?>",div);
	}
}
</script>

<input type="hidden" name="acf" value="<? echo $row2["acf_id"]; ?>" class="campos">
<input type="hidden" name="codigo" value="<? echo $_REQUEST["codigo"]; ?>" class="campos">
<input type="hidden" name="serial" value="<? echo $_REQUEST["serial"]; ?>" class="campos">
 

<table class="table table-bordered table-condensed"> 
<thead>
	<tr>
		<th colspan="5">Información del Activo Fijo</th>
	</tr>
</thead> 
<tbody>
	<tr valign="top">
		<td width="200">
			<?
			$imagen = ($datos["pro_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_REQUEST["db"]."/".$datos["pro_imagen"];
			?>
			<img src="<?php echo $url_base.$imagen; ?>" style="width:170px !important;"> 
		</td>
		<td>
			<table class="table table-bordered table-condensed">   
			<tbody>
				<tr>
					<th style="text-align:right;">Código Activo:</th>
					<th colspan="2"><?php echo $datos["acf_codigo"]; ?></th> 
				</tr> 
				<tr>
					<th style="text-align:right;">Nombre Producto:</th> 
					<td colspan="2"><?php echo $datos["pro_nombre"]; ?></td>
				</tr>
				<tr>
					<th style="text-align:right;">Serie:</th>
					<td colspan="2"><?php echo $datos["acf_serie"]; ?></td> 
				</tr> 
				<tr>
					<th style="text-align:right;">Etiqueta:</th> 
					<td colspan="2"><?php echo $datos["acf_etiqueta"]; ?></td>
				</tr>  
				<tr>
					<th style="text-align:right;">Tipo Activo:</th> 
					<td colspan="2"><?php echo $_tipo_activo[$datos["pro_tipo"]]; ?></td>
				</tr>  
				<tr>
					<th style="text-align:right;">Cuenta Contable:</th>
					<td colspan="2"><?php echo $_tipo_cuenta_contable[$datos["pro_tipo"]]; ?></td> 
				</tr> 
			</tbody>
			</table>
		</td>
		<td width="400">
			<table class="table table-bordered table-condensed">   
			<tbody> 
				<tr>
					<th style="text-align:right;" width="140">Ubicación:</th>
					<td><? echo $_ubicaciones[$datos["acf_ubicacion"]]; ?></td>
				</tr>
				<tr>
					<th style="text-align:right;">Centro de Costo:</th>
					<td><? echo $_centro_costos[$datos["acf_cco_id"]]; ?></td>
				</tr>     
				<tr>
					<th style="text-align:right;">Disponibilidad:</th> 
					<td><?php activo_fijo_estado($datos["acf_disponible"]); ?></td> 
				</tr> 
				<tr>
					<th style="text-align:right;">Responsable:</th>
					<td><?php echo $datos["per_nombre"]; ?></td> 
				</tr>
				<tr>
					<th style="text-align:right;">Ult.Modificación:</th>
					<td><?php echo $datos["acf_modificacion"]; ?></td> 
				</tr> 
			</tbody>
			</table>
		</td> 
		<td width="170"> 
		    <table width="100%">
		    <tr>
		        <td><?php construir_boton("acf_listado.php","&fil_tipo=".$datos["pro_tipo"],"izquierda","Activos Fijos",2); ?></td>
		    </tr> 
		    <tr>
				<td><?php construir_boton("acf_info.php","&acf=".$_REQUEST["acf"],"refrescar","Refrescar",2); ?></td>
		    </tr> 
		    <tr>
				<td><?php construir_boton("acf_editar.php","&id=".$datos["acf_id"],"editar","Editar Activo",2);  ?></td>
		    </tr> 
		    <tr>
				<td><?php construir_boton("pro_editar.php","&id=".$datos["pro_id"],"editar","Editar Producto",2);  ?></td>
		    </tr>
		    <tr>
				<td><?php construir_boton("man_nuevo.php","&acf=".$datos["acf_id"],"crear","Crear Mantención",2);  ?></td>
		    </tr>
		    <tr>
				<td><?php construir_boton("inf_kardex_producto.php","&producto=".$datos["pro_id"],"buscar","Ver Kardex",2);	?></td>
		    </tr> 
		    <tr>
				<td><?php construir_boton("visor('acf_pdf.php?acf=".$datos["acf_id"]."')","","imprimir","Imprimir Ficha",4); ?></td>
		    </tr> 
			</table>
		</td>
	</tr>
</tbody>
</table>
<br>
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li class="active"><a href="#tab_1" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Info Producto</a></li>
    <li><a href="#tab_2" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Situación Inicial/Final</a></li>
    <li><a href="#tab_3" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Info Contable</a></li>
    <li><a href="#tab_4" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Activos Relacionados</a></li>
    <li><a href="#tab_5" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Mantención</a></li> 
</ul>

<div class="tab-content">
	<div class="tab-pane active" id="tab_1"> 
		<table width="100%">
	    <tr valign="top">
	        <td width="50%"> 
				<table class="table table-bordered table-condensed">   
				<tbody>
					<tr>
						<th width="130" style="text-align:right;">Código Producto:</th>
						<th><?php echo $datos["pro_codigo"]; ?></th>
					</tr>   
					<tr>
						<th style="text-align:right;">Código Barra:</th> 
						<td><?php echo $datos["pro_codigo_barra"]; ?></td>
					</tr>
					<tr>
						<th style="text-align:right;">Descripción:</th> 
						<td><?php echo nl2br($datos["pro_descripcion"]); ?></td>
					</tr>
                    <tr>
                        <th style="text-align:right;">Mantenimiento:</th> 
                        <td><?php echo ($datos["pro_mantenimiento"])?"Requiere mantención":"-"; ?></td>
                    </tr> 
					<tr valign="top">					
						<th style="text-align:right;">Notas Activo:</th>
						<td><?php echo nl2br($datos["acf_notas"]); ?></td>
					</tr> 
				</tbody>
				</table>
			</td>
			<td style="padding-left: 15px;">
				<table class="table table-bordered table-condensed">   
				<tbody>
					<tr>
						<th width="130" style="text-align:right;">Clasificación:</th>
						<td><?php
						$labels = explode(",",_opc("label_clasificacion")); 
						$valores = explode(" > ",$_categorias_largo[$datos["pro_categoria"]]); 
						for($ic = 0;$ic < count($valores);$ic++){
							echo "<b>".$labels[$ic]."</b>: ".$valores[$ic]."<br>";
						}
						?></td>
					</tr>  
					<tr>
						<th style="text-align:right;">Unidad:</th>
						<td><?php echo $_unidades[$datos["pro_unidad"]]; ?></td>
					</tr>
					<?php
					for($iex = 1;$iex <= 10;$iex++){				 
			            $res = sql_campos("*"," and cam_producto = $iex order by cam_nombre asc");  
						if(mysqli_num_rows($res) > 0){
							while($row = mysqli_fetch_array($res)){
								?> 
								<tr> 
									<th style="text-align: right;"><? echo $row["cam_nombre"]; ?>:</th>
									<td><? echo $datos["pro_extra_".$iex]; ?></td> 
								</tr>
								<? 
							}
						}
					}
					for($iex = 1;$iex <= 10;$iex++){				 
			            $res = sql_campos("*"," and cam_activo = $iex order by cam_nombre asc");  
						if(mysqli_num_rows($res) > 0){
							while($row = mysqli_fetch_array($res)){
								?> 
								<tr> 
									<th style="text-align: right;"><? echo $row["cam_nombre"]; ?>:</th>
									<td><? echo $datos["acf_extra_".$iex]; ?></td> 
								</tr>
								<? 
							}
						}
					} 
					?>  
				</tbody>
				</table>
			</td>
		</tr>
		</table>
	</div>
	<div class="tab-pane" id="tab_2">  
		<table width="100%">
	    <tr valign="top">
	        <td width="50%"> 
				<table class="table table-bordered table-condensed">   
				<tbody>
					<tr>
						<th width="130" style="text-align:right;">Fecha Ingreso:</th>
						<td><?php echo _fec($datos["acf_fecha_ingreso"],5); ?></td>
					</tr>
					<tr>
						<th style="text-align:right;">Concepto:</th>
						<td><?php echo $_concepto_ing[$datos["acf_concepto_ingreso"]]; ?></td>
					</tr>
					<tr>
						<th style="text-align:right;">Nro Documento:</th>
						<td><?php echo $datos["acf_nro_factura"]; ?></td>
					</tr>
					<tr>
						<th style="text-align:right;">Valor Compra (<? echo $_configuraciones["cfg_moneda"]; ?>):</th>
						<td><?php echo _num($datos["acf_valor"],$_configuraciones["cfg_moneda_decimal"]); ?></td>
					</tr>
					<?
					if($datos["acf_valor_sec"] > 0){
						$precio = ($datos["acf_valor"] / $datos["acf_valor_sec"]);
						?>
						<tr>
							<th style="text-align:right;">2da Moneda (<? echo $_configuraciones["cfg_moneda_sec"]; ?>):</th>
							<td><?php echo _num($precio,$_configuraciones["cfg_moneda_sec_decimal"]); ?> (Tipo Cambio: <? echo $datos["acf_valor_sec"]; ?>)</td>
						</tr>
						<?
					}
					?>
					<tr>
						<th style="text-align:right;">Proveedor:</th>
						<td><?php 
						if($datos["acf_proveedor"] <> ""){
							$res_prov = sql_auxiliares("*"," and aux.aux_id = '".$datos["acf_proveedor"]."' ");  
							if(mysqli_num_rows($res_prov) > 0){
								$datos_prov = mysqli_fetch_array($res_prov);
								echo $datos_prov["aux_nombre"];
							}
						}
						?></td>
					</tr>
				</tbody>
				</table> 
			</td>
			<td style="padding-left: 15px;">
				<table class="table table-bordered table-condensed">   
				<tbody>
					<tr>
						<th width="130" style="text-align:right;">Fecha Salida:</th>
						<td><?php echo _fec($datos["acf_fecha_salida"],5); ?></td>
					</tr>
					<tr>
						<th style="text-align:right;">Concepto:</th>
						<td><?php echo $_concepto_baj[$datos["acf_concepto_salida"]]; ?></td>
					</tr> 
					<tr>
						<th style="text-align:right;">Valor Salida:</th>
						<td><?php echo _num($datos["acf_valor_salida"]); ?></td>
					</tr> 
				</tbody>
				</table>
			</td>
		</tr>
		</table>
	</div>
	<div class="tab-pane" id="tab_3">
		<?
		if($datos["pro_tipo"] <> "DIG"){
			?>
			<table width="100%">
		    <tr valign="top">
		        <td width="40%" style="padding-right: 15px;"> 
					<table class="table table-bordered table-condensed">   
					<tbody> 
					<tr>
						<th style="text-align:right;">Tipo Depreciación:</th>
						<td><?php echo ($datos["acf_tipo_depreciacion"] == "L")?"Lineal":"Acelerada"; ?></td>
					</tr> 
					<tr>
						<th style="text-align:right;">Vida Util:</th>
						<td><?php echo $datos["acf_vida_util"]; ?> meses</td>
					</tr>
					<tr>
						<th style="text-align:right;">Valor Residual:</th>
						<td><?php echo _num($datos["acf_valor_residual"]); ?></td>
					</tr>
					<tr>
						<th style="text-align:right;" width="130">Inicio Revalorización:</th>
						<td><?php echo $datos["acf_mes_ano_inicio_revalorizacion"]; ?><br> 
						<i>(*) Fecha debe ser igual a la de Inicio de Depreciación</i></td>
					</tr>
					<tr>
						<th style="text-align:right;">Período Depreciación:</th>
						<td><?php echo $datos["acf_mes_ano_inicio_depreciacion"]; ?> al <?php 
						echo substr($datos["acf_mes_ano_termino_depreciacion"],0,7); ?></td>
					</tr> 
					<tr>
						<th style="text-align:right;">Tipo Adquisición:</th>
						<td><?php echo ($datos["acf_tipo_adquisicion"] == "P")?"Propio":"En Leasing"; ?></td>
					</tr>  
					<tr>
						<th style="text-align:right;">Situación Dep:</th>
						<td><?php echo ($datos["acf_totalmente_dep"] == "1")?"Totalmente Depreciado":"Con Saldo de Depreciación"; ?></td>
					</tr>
					</tbody> 
					</table>  
				</td>
				<td style="padding: 0px;"> 
					<?
					if($datos["acf_totalmente_dep"] == "0"){
						?><iframe src="mods/home/acf_graf_depreciacion.php?id=<? echo $datos["acf_id"]; ?>" frameborder="0" scrolling="no" style="width: 100%; height: 260px;"></iframe><?
					}else{
						?>
						<div class="alert alert-danger" style="margin:20px;">
							<strong>Activo totalmente depreciado</strong><br>
							No se puede generar el gráfico para el año <? echo date("Y"); ?>
						</div>
						<?
					}
					?>
				</td>
			</tr>
			</table>
			<?
		}else{
			?>
			<div class="alert alert-info">
				<b>NO APLICA</b>
			</div>
			<?
		}
		?>
	</div> 
	<div class="tab-pane" id="tab_4">
		<table class="table table-bordered table-condensed">  
		<tbody>
		<tr>
			<th width="1"></th>  

			<th width="1">Codigo</th>
			<th width="1">Serie</th>
			<th width="1">Etiqueta</th>
			
			<th>Ubicacion</th>
			<th>Responsable</th>

			<th>Clasificación</th>
			<th>Unidad</th>
			
			<th width="1">Ingreso</th>
			<th width="1">Baja</th>
			<th width="1">Valor</th> 
			
			<th width="1">Estado</th>
		</tr>
		<?php
		$res3 = sql_activos_fijos("*","and acf_producto = '$datos[acf_producto]' order by acf_serie asc");
		if(mysqli_num_rows($res3) > 0){ 
			while($row3 = mysqli_fetch_array($res3)){
				?>
				<tr <? if($row3["acf_id"] == $datos["acf_id"]){ echo 'style="background-color: #ffffeb !important;"'; } ?>>
					<td><?
					construir_boton("acf_info.php","&acf=".$row3["acf_id"],"buscar","Ver Activo");
					?></td>
					<th><?php echo $row3["acf_codigo"]; ?></th>
					<th><?php echo $row3["acf_serie"]; ?></th>
					<th><?php echo $row3["acf_etiqueta"]; ?></th>  
								
					<td><?php echo $row3["ubi_nombre"]; ?></td> 
					<td><?php echo $row3["per_nombre"]; ?></td> 
					 
					<td><?php echo $_categorias_largo[$row3["pro_categoria"]]; ?></td> 
					<td><?php echo $_unidades[$row3["pro_unidad"]]; ?></td>
					
					<td><?php echo _fec($row3["acf_fecha_ingreso"],5); ?></td> 
					<td><?php echo _fec($row3["acf_fecha_salida"],5); ?></td> 
					<td style="text-align: right"><?php echo _num($row3["acf_valor"]); ?></td> 
					
					<td><?php activo_fijo_estado($row3["acf_disponible"]); ?></td>
				</tr>
				<?
			}
			?>
			<?
		}
		?>
		</tbody>
		</table>
	</div> 
	<div class="tab-pane" id="tab_5">
		<table class="table table-bordered table-condensed">   
		<tbody> 
		<tr>
			<th style="text-align:right;">Condición:</th>
			<td><?php echo $datos["acf_condicion"]; ?></td>
		</tr>
		<tr>
			<th style="text-align:right;" width="150">Próx. Mantención:</th> 
			<td><?php echo _fec($datos["acf_fecha_mantencion"],5); ?></td> 
		</tr>
		<tr>
			<th style="text-align:right;" width="150">Fecha Ult.Mantención:</th> 
			<td><?php echo _fec($datos["acf_mantencion_anterior"],5); ?></td> 
		</tr>
		<tr>
			<th style="text-align:right;" width="150">Fecha de Garantía:</th> 
			<td><?php echo _fec($datos["acf_fecha_garantia"],5); ?></td> 
		</tr>
		<tr>
			<th style="text-align:right;">Costos Adicionales:</th>
			<td><?php echo _num($datos["acf_valor_adicional"]); ?></td>
		</tr>
		</tbody> 
		</table> 

		<?
		$res3 = sql_acf_mantenimientos("*","and mnt.man_acf_id = '$datos[acf_id]'","","");
		if(mysqli_num_rows($res3) > 0){
			?>
			<table class="table table-bordered table-condensed">   
			<tbody>
			<tr>
				<th width="70"></th>
				<th width="1">Folio</th>
				<th width="90">Inicio</th>
				<th width="90">Termino</th>
				<th>Responsable</th>
				<th>Titulo</th>
				<th width="1">Estado</th>
				<th width="1">Costo</th>
			</tr>
			<?
			while($row3 = mysqli_fetch_array($res3)){
				?>
				<tr>
					<td width="1"><?
					construir_boton("man_editar.php","&man=".$row3["man_id"],"editar","Editar");
					construir_boton("visor('man_pdf.php?id=".$row3["man_id"]."')","1","imprimir","Imprimir",4); 
					?></td>
					<th style="text-align: center"><? echo $row3["man_id"]; ?></th>
					<td><? echo $row3["man_inicio"]; ?></td>
					<td><? echo $row3["man_termino"]; ?></td>
					<td><? echo $_personas[$row3["man_responsable"]]; ?></td>
					<td><? echo $row3["man_titulo"]; ?></td>
					<td><? 
					switch($row3["man_estado"]){
						case "PND": ?><span class="label label-info">Realizando</span><? break;
						case "TER": ?><span class="label label-success">Terminado</span><? break; 
					} 
					?></td>
					<td style="text-align: right;"><? echo _num($row3["man_costo"]); ?></td>
				</tr>
				<?
				$total += $row3["man_costo"];
			}
			?>
			<tr style="background-color: #F5FFB2 !important;">
				<th colspan="7" style="text-align: right">TOTAL COSTOS:</th>
				<th style="text-align: right;"><? echo _num($total); ?></th>
			</tr>
			</tbody>
			</table>
			<?
		}else{
			?>
			<div class="alert alert-info">
				Sin mantenimientos registrados
			</div>
			<?
		} 
		?> 
	</div> 
</div>