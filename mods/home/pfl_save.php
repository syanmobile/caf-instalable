<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabación Perfiles";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

$opcion = $_REQUEST["permiso"];
if(count($opcion) > 0){
	foreach($opcion as $opc){
		$opciones .= ",".$opc;
	}
} 
$operacion = $_REQUEST["operacion"];
if(count($operacion) > 0){
	foreach($operacion as $ope){
		$operaciones .= ",".$ope;
	}
}  
switch($_POST["modo"]){
	case "crear":
		$sql = "INSERT INTO perfiles ( 
			pfl_nombre,
			pfl_navegacion,
			pfl_operaciones 
		) VALUES ( 
			'$_POST[nombre]',
			'$opciones',
			'$operaciones' 
		)";
		$res = mysqli_query($cnx,$sql);
		$_POST["id"] = mysqli_insert_id($cnx);
		?>
        <div class="alert alert-success"> 
            <strong>Perfil '<? echo $_POST["nombre"]; ?>' creado con &eacute;xito</strong>
        </div>
        <div class="alert alert-info"> 
            <strong>NOTA:</strong> Refresque el navegador para visualizar el menú de grabación
        </div>
		<?php   
		break;
		
	case "editar":
		$SQL_ = "UPDATE perfiles SET "; 
		$SQL_.= "pfl_nombre = '$_POST[nombre]', ";
		$SQL_.= "pfl_navegacion = '$opciones', ";
		$SQL_.= "pfl_operaciones = '$operaciones' ";
		$SQL_.= "WHERE pfl_id = '$_POST[id]' ";  
		$res = mysqli_query($cnx,$SQL_);
		?>   
        <div class="alert alert-success"> 
            <strong>Perfil '<? echo $_POST["nombre"]; ?>' editado con &eacute;xito</strong> 
        </div>  
        <div class="alert alert-info"> 
            <strong>NOTA:</strong> Refresque el navegador para visualizar el menú de grabación
        </div>
		<?php  
		break;
		
	case "eliminar":
		$SQL_ = "UPDATE perfiles SET "; 
		$SQL_.= "pfl_elim = '1' ";
		$SQL_.= "WHERE pfl_id = '$_POST[id]' ";  
		$res = mysqli_query($cnx,$SQL_);
		?>   
        <div class="alert alert-success"> 
            <strong>Registro eliminado con &eacute;xito</strong> 
        </div>  
		<?php  
		break;
}

construir_boton("pfl_listado.php","","buscar","Listado de perfiles",2);
if($_REQUEST["modo"] <> "eliminar"){
	construir_boton("pfl_editar.php","&id=".$_POST["id"],"editar","Editar este perfil",2);
}
construir_boton("pfl_nuevo.php","","crear","Crear otra Perfil",2);
?>