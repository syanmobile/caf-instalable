<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Control Toma Inv. Activo por Ubicación";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "ctr_listado.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>   
<table class="table table-bordered table-condensed table-info" style="margin-bottom:0px !important;">  
<tbody> 
<tr>  
    <th width="1">Folio:</th>
    <td width="120"><input type="text" name="fil_folio" class="campos w10" value="<? echo $_REQUEST["fil_folio"]; ?>"></td>
    <td><?php
    construir_boton("ctr_listado.php","","buscar","Filtrar");
    ?></td> 
</tr>
</table>

<?php    
if($_REQUEST["fil_folio"] <> ""){
	$extra .= "and mov.mov_folio = '".$_REQUEST["fil_folio"]."' ";
}
?>
<input type="hidden" name="pagina" id="pagina" value="ctr_listado.php" class="campos">
<?
$sql_paginador = sql_controles("*","   $extra order by ctr.ctr_id desc ","s"); 
$cant_paginador = 20;
require("../../_paginador.php");
$res = mysqli_query($cnx,$sql_final);
if(mysqli_num_rows($res) > 0){
    ?>
    <table class="table table-bordered table-hover table-condensed"> 
    <thead>
    <tr>   
        <th width="1">&nbsp;</th>   
        <th style="text-align:center;" width="1">Fecha</th> 
        <th style="text-align:center;" width="1">Folio</th> 
        <th>Responsable</th> 
        <th>Ubicación</th>
        <th width="1">Buscados</th>
        <th width="1">Encontrados</th>
        <th width="1">Extravíos</th>
        <th width="1">Otros</th>
        <th width="100">Acierto</th>
        <th width="1">%</th>
    </tr>
    </thead>
    <tbody> 
    <?php 
    while($row = mysqli_fetch_array($res)){ 
        ?>
        <tr> 
            <td style="text-align:center;"><?php
            construir_boton("abrir_pagina('ctr_pdf.php?id=".$row["ctr_id"]."')","1","imprimir","Mostrar Detalle",4); 
            ?></td>    
            <td style="text-align:center;"><?php echo _fec($row["ctr_fecha"],5); ?></td> 
            <td style="text-align:center;"><?php echo $row["ctr_id"]; ?></td>
            <td><?php echo $_personas[$row["ctr_per_id"]]; ?></td>
            <td><?php echo $_ubicaciones[$row["ctr_ubi_id"]]; ?></td>
            <td style="text-align: center"><?php echo _num($row["ctr_buscados"]); ?></td>
            <td style="text-align: center"><?php echo _num($row["ctr_encontrados"]); ?></td>
            <td style="text-align: center"><?php echo _num($row["ctr_extravios"]); ?></td>
            <td style="text-align: center"><?php echo _num($row["ctr_otros"]); ?></td>
            <td>
                <?
                $porc = round(_porc($row["ctr_encontrados"],$row["ctr_buscados"]));
                ?>
                <div class="progress" style="margin-bottom: 0px;">
                  <div class="progress-bar progress-bar-<?
                  if($porc < 50){ echo "danger"; }else{ echo "success"; } 
                  ?>" role="progressbar" style="width: <? echo $porc; ?>%;" aria-valuenow="<? 
                  echo $porc; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </td>
            <th><? echo $porc; ?>%</th>
        </tr> 
        <?php 
    }
    ?> 
    </tbody>
    </table>   
    <?php
	echo $paginador_dibujo;
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin registros creados</strong>
    </div>
    <?php 
} 
?>