<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Editar Activo Fijo";
construir_breadcrumb($titulo_pagina); 
//----------------------------------------------------------------------------------------
$pagina = "acf_editar.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
$res = sql_activos_fijos("*","and acf.acf_id = '$_REQUEST[id]' ");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<b>ERROR EN CONSULTA</b><br>No se encontró el dato
	</div>
    <?php
	exit();
}
?> 
<script language="javascript"> 
$(document).ready(function() { 
	campos_adicionales_set(); 
});
function save(){
	var div = document.getElementById("grabacion"); 
	var a = $(".campos").fieldSerialize();   
	AJAXPOST("mods/home/acf_ajax.php",a+"&modo=datos",div); 
}  
function campos_adicionales_set(){   
	var a = $(".campos").fieldSerialize();
	var div = document.getElementById("campos_adicionales_lugar");
	AJAXPOST("mods/home/cam_ajax2.php",a+"&modo=campos_adicionales",div);
} 
</script>

<input type="hidden" name="acf" value="<? echo $datos["acf_id"]; ?>" class="campos">  
 
<div id="grabacion"></div>
<table class="table table-bordered table-condensed">   
	<thead>
		<tr>
			<th colspan="5">Atributos del Activo Fijo</th>
		</tr>
	</thead>   
	<tbody>  
		<tr>
			<th style="text-align:right;">Código Activo:</th>
			<td><input type="text" value="<?php echo $datos["acf_codigo"]; ?>" readonly class="campos"></td> 
		</tr>
		<tr>
			<th style="text-align:right;">Serie:</th>
			<td><input type="text" name="serie" value="<?php echo $datos["acf_serie"]; ?>" class="campos"></td> 
		</tr>
		<tr>
			<th width="130" style="text-align:right;">Etiqueta:</th>
			<td><input type="text" name="etiqueta" value="<?php echo $datos["acf_etiqueta"]; ?>" class="campos"></td> 
		</tr>  
		<tr>
			<th style="text-align:right;">Ubicación:</th>
			<td>
				<select name="ubicacion" id="ubicacion" class="campos buscador w10">
			    <?php
			    $res = sql_ubicaciones("*"," order by ubi.ubi_codigo asc"); 
			    if(mysqli_num_rows($res) > 0){
			        while($row = mysqli_fetch_array($res)){
			            ?>
			            <option value="<? echo $row["ubi_id"]; ?>" <?
			            if($datos["acf_ubicacion"] == $row["ubi_id"]){ echo "selected"; } 
			            ?>>U<? echo $row["ubi_codigo"]." - ".$row["ubi_nombre"]; ?></option>
			            <?
			        }
			    }
			    ?></select>
			</td>
		</tr>
		<tr>
			<th style="text-align:right;">Responsable:</th>
			<td>
				<select name="responsable" id="responsable" class="campos buscador">
				<option value=""></option>
				<?php 
				$res = sql_personas("*","  and per.per_tipo = 1 and per.per_elim = 0  ORDER BY per.per_nombre asc ");  
				if(mysqli_num_rows($res) > 0){
					while($row = mysqli_fetch_array($res)){
						?>
						<option value="<? echo $row["per_id"]; ?>" <?
						if($row["per_id"] == $datos["acf_responsable"]){ echo "selected"; } 
						?>><?php echo $row["per_nombre"]; ?></option>
						<?
					}
				} 
				?>
				</select>
			</td>
		</tr>
		<tr>
			<th style="text-align:right;">Centro de Costo:</th>
			<td><?php input_centro_costo($datos["acf_cco_id"]); ?></td>
		</tr>
		<tr valign="top">					
			<th style="text-align:right;">Notas Activo:</th>
			<td>
			<textarea name="notas" class="campos" rows="4" style="width: 90%;"><?php 
			echo $datos["acf_notas"]; ?></textarea>
			</td>
		</tr>  
		<tr>
			<th style="text-align:right;">Estado:</th>
			<td>
				<select name="estado" id="estado" class="campos buscador">
				<option value="0" <? 
				if($datos["acf_disponible"] == "0"){ echo "selected"; } ?>>No Disponible</option>
				<option value="1" <? 
				if($datos["acf_disponible"] == "1"){ echo "selected"; } ?>>Disponible</option>
				<option value="2" <? 
				if($datos["acf_disponible"] == "2"){ echo "selected"; } ?>>Prestado</option>
				<option value="3" <? 
				if($datos["acf_disponible"] == "3"){ echo "selected"; } ?>>En Mantenimiento</option>
				<option value="4" <? 
				if($datos["acf_disponible"] == "4"){ echo "selected"; } ?>>De Baja</option>
				<option value="5" <? 
				if($datos["acf_disponible"] == "5"){ echo "selected"; } ?>>Req.Mantenimiento</option>
				<option value="6" <? 
				if($datos["acf_disponible"] == "6"){ echo "selected"; } ?>>Asignado</option>
				</select>
			</td>
		</tr> 
	</tbody>
</table>  

<table class="table table-bordered table-condensed">   
	<thead>
		<tr>
			<th colspan="5">Información del Producto</th>
		</tr>
	</thead>   
	<tbody>   
		<tr>
			<th width="130" style="text-align:right;">Código Producto:</th>
			<th><?php echo $datos["pro_codigo"]; ?></th> 
		</tr>
		<tr>
			<th style="text-align:right;">Nombre Producto:</th> 
			<td><b><?php echo $datos["pro_codigo"]; ?></b> - <?php echo $datos["pro_nombre"]; ?></td>
		</tr> 
		<tr>
			<th style="text-align:right;">Tipo Activo:</th> 
			<td><?php echo $_tipo_activo[$datos["pro_tipo"]]; ?></td>
		</tr>
	</tbody>
</table>
 
<table class="table table-bordered table-condensed">   
	<thead>
		<tr>
			<th colspan="5">Ingreso del Activo Fijo</th>
		</tr>
	</thead>     
	<tbody>
		<tr>
			<th width="130" style="text-align:right;">Fecha Ingreso:</th>
			<td><input type="text" name="fecha_ingreso" class="campos fecha" value="<?php echo _fec($datos["acf_fecha_ingreso"],5); ?>"></td>
		</tr>
		<tr>
			<th style="text-align:right;">Concepto:</th>
			<td><?php input_concepto($datos["acf_concepto_ingreso"],'E'); ?></td>
		</tr>
		<tr>
			<th style="text-align:right;">Nro Factura:</th>
			<td><input type="text" name="nro_factura" class="campos" value="<?php echo $datos["acf_nro_factura"]; ?>"></td>
		</tr>
		<tr>
			<th style="text-align:right;">Valor Compra:</th>
			<td><input type="text" name="valor_ingreso" class="campos" value="<?php echo $datos["acf_valor"]; ?>"></td>
		</tr>
		<tr>
			<th style="text-align:right;">Proveedor:</th> 
			<td><?php input_auxiliar2($datos["acf_proveedor"]); ?></td>
		</tr>
	</tbody>
</table>


<table class="table table-bordered table-condensed"> 
	<thead>
		<tr>
			<th colspan="2">Campos personalizados del Activo Fijo</th>
		</tr>
	</thead>    
	<tbody> 
		<?
		for($iex = 1;$iex <= 10;$iex++){				 
            $res = sql_campos("*"," and cam_activo = $iex order by cam_nombre asc");  
			if(mysqli_num_rows($res) > 0){
				while($row = mysqli_fetch_array($res)){
					$infor = $datos["acf_extra_".$iex]; 
					?> 
					<tr> 
						<th width="130" style="text-align: right;"><? echo $row["cam_nombre"]; ?>:</th>
						<td><? 
			            campo_dinamico($row["cam_tipo"],"extra_".$iex,$row["cam_opciones"],$infor,$url_base);
			            ?></td> 
					</tr>
					<? 
				}
			}
		} 
		?> 


	</tbody>
</table>

<table class="table table-bordered table-condensed"> 
	<thead>
		<tr>
			<th colspan="2">Depreciación del Activo Fijo</th>
		</tr>
	</thead>    
	<tbody> 
	<tr>
		<th style="text-align:right;" width="130">Tipo Depreciación:</th>
		<td><select name="tipo_depreciacion" class="campos">
			<option value="L" <?php if($datos["acf_tipo_depreciacion"] == "L"){
				echo "selected";
			} ?>>Lineal</option>
			<option value="A" <?php if($datos["acf_tipo_depreciacion"] == "A"){
				echo "selected";
			} ?>>Acelerada</option>
		</select></td>
	</tr> 
	<tr>
		<th style="text-align:right;">Vida Util:</th>
		<td><input type="text" name="vida_util" class="campos numero" value="<?php echo $datos["acf_vida_util"]; ?>"> meses</td>
	</tr>
	<tr>
		<th style="text-align:right;">Valor Residual:</th>
		<td><input type="text" name="valor_residual" class="campos numero" value="<?php echo $datos["acf_valor_residual"]; ?>"></td>
	</tr>
	<tr>
		<th style="text-align:right;">Inicio Revalorización:</th>
		<td><input type="text" name="mes_ano_inicio_revalorizacion" class="campos" value="<?php echo $datos["acf_mes_ano_inicio_revalorizacion"]; ?>"></td>
	</tr>
	<tr>
		<th style="text-align:right;">Inicio Depreciación:</th>
		<td><input type="text" name="mes_ano_inicio_depreciacion" class="campos" value="<?php echo $datos["acf_mes_ano_inicio_depreciacion"]; ?>"></td>
	</tr> 
	<tr>
		<th style="text-align:right;">Termino Depreciación:</th>
		<td><input type="text" name="mes_ano_termino_depreciacion" class="campos" value="<?php echo $datos["acf_mes_ano_termino_depreciacion"]; ?>"></td>
	</tr> 
	<tr>
		<th style="text-align:right;">Tipo Adquisición:</th>
		<td><select name="tipo_adquisicion" class="campos">
			<option value="P" <?php if($datos["acf_tipo_adquisicion"] == "P"){
				echo "selected";
			} ?>>Propio</option>
			<option value="A" <?php if($datos["acf_tipo_adquisicion"] == "A"){
				echo "selected";
			} ?>>En Leasing</option>
		</select></td>
	</tr> 
	</tbody> 
</table>
<?php 
construir_boton("","","grabar","Guardar Cambios",3); 
construir_boton("acf_info.php","&acf=".$_REQUEST["id"],"eliminar","Cancelar",2); 
?> 