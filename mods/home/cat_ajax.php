<?php
require("../../inc/conf_dentro.php");

switch($_REQUEST["modo"]){ 
	case "codigo_unico":
		$res = sql_categorias("*"," and cat_codigo = '$_REQUEST[precodigo]$_REQUEST[codigo]' "); 
		if(mysqli_num_rows($res) == 0){
			construir_boton("#","","check","Código disponible: ".$_REQUEST["precodigo"].$_REQUEST["codigo"],4);
			?>
			<script language="javascript">
			carg("cat_save.php","");
			</script>
			<? 
		}else{
			construir_boton("#","","eliminar","Ya existe el código: ".$_REQUEST["precodigo"].$_REQUEST["codigo"],4);
		}
		break;   

	case "buscar":
		$res = sql_categorias("*"," where cat_codigo = '$_REQUEST[codigo]' "); 
		if(mysqli_num_rows($res) > 0){
			$row = mysqli_fetch_array($res); 
			?>
			<script language="javascript">
			document.getElementById("categoria").value = "<?php echo $row["cat_codigo"]; ?>";
			document.getElementById("categoria_nomb").value = "<?php echo $row["cat_nombre"]; ?>"; 
			</script>
			<?
		}else{
			?>
			<script language="javascript">
			alerta_js("No existe la categoria");
			document.getElementById("categoria").value = "";
			document.getElementById("categoria_nomb").value = ""; 
			</script>
			<?
		}
		break;
	case "listado": 
		$res = sql_categorias("*","   order by cat.cat_codigo asc"); 
		$num = mysqli_num_rows($res);
		?>
		<h4 id="titulo_alerta">Clasificación</h4> 
        <?
		if($num > 0){
			?>
			<table class="table table-bordered table-hover table-condensed"> 
			<thead>
			<tr> 
				<th width="80">Opción</th>
				<th width="1">Código</th>
				<th>Clasificación</th>
			</tr>
			</thead>
			<tbody> 
			<?php 
			while($row = mysqli_fetch_array($res)){
				?>
				<tr>    
					<td><?
					construir_boton("elegir_categoria('$row[cat_codigo]','$row[cat_nombre]')","","derecha","Elegir",4); 
					?></td>
					<th style="text-align:center"><?php echo $row["cat_codigo"]; ?></th>
					<td><?php echo $row["cat_nombre"]; ?></td> 
				</tr>
				<?php 
			}
			?> 
			</tbody>
			</table>  
			<?php
		}else{
			?>
			<div class="alert alert-danger">
				<strong>Sin resultados</strong>
			</div>
			<?php 
		}
		break;
}
?> 