<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Tipos de Activos";
construir_breadcrumb($titulo_pagina);
//---------------------------------------------------------------------------------------- 
?>
<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td><?php
		construir_boton("imple_tac_nuevo.php","","crear","Nuevo Tipo Activo",2);
        construir_boton("implementacion.php","","izquierda","Volver a Implementación",2);
		?></td>
    </tr>
</tbody>
</table>

<table class="table table-bordered tabledrag table-condensed" id="tbl_modulos">
<thead>
<tr style="cursor: move;">
    <th width="1" align="center"></th> 
    <th width="1">Código</th>
    <th>Nombre Activo</th>
    <th>Ejemplo</th>
    <th width="1">Amortizable</th>
    <th width="1">Depreciable</th>
    <th width="1">Revalorizable</th>
    <th width="1">Readecuable</th>
    <th width="1">Estado</th> 
</tr>
</thead>
<tbody> 
<? 
$res_ta = mysqli_query($cnx,"select * from activos_tipos order by tac_orden asc");
while($row = mysqli_fetch_array($res_ta)){  
	?>
    <tr fila="<?php echo $row["tac_id"] ;?>" id="<?php echo $row["tac_id"] ;?>">  
        <td style="text-align:center;"><?php
		construir_boton("imple_tac_editar.php","&id=".$row["tac_id"],"editar","Editar");  
		?></td> 
        <th style="text-align:center;"><?php echo $row["tac_codigo"]; ?></th>
        <td><span class="<?php echo $row["tac_icono"]; ?>"></span> <?php echo $row["tac_nombre"]; ?></td>
        <td><?php echo $row["tac_ejemplo"]; ?></td> 
        <td style="text-align:center;"><?php echo ($row["tac_amortizable"])?"SI":"-"; ?></td> 
        <td style="text-align:center;"><?php echo ($row["tac_depreciable"])?"SI":"-"; ?></td>  
        <td style="text-align:center;"><?php echo ($row["tac_revalorizable"])?"SI":"-"; ?></td> 
        <td style="text-align:center;"><?php echo ($row["tac_readecuable"])?"SI":"-"; ?></td>
        <td style="text-align:center;"><?php echo ($row["tac_activo"])?"Activo":"-"; ?></td>  
    </tr>
    <? 
}
?> 
</tbody>
</table>  
<script language="javascript"> 
$(".tabledrag").tableDnD({
    onDragClass:"draging",
    onDrop:function(){
        var valores = $.tableDnD.serialize();
        SoloEnviar("mods/home/imple_tac_save.php","modo=reordenar&"+valores);
    }
});
</script>