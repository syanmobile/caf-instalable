<?php   
include("../../inc/conf_dentro.php");

/****************************************************************************************/
function revalorizacion_activo($valor_activo,$ano_recorrido,$ano,$ipc,$depreciacion_acumulada,$fecha,$vez,$meses,$meses_acum,$vida_util,$saldo_ant_activo,$meses_saldo,$baja){

	// Obtengo el factor de la tabla IPC 
	$vez++;
	if($vez == 1){
		$fecha_ = str_replace("-","",$fecha);
		$fecha_ = substr($fecha_,0,6);
		$ipc_valor = $ipc[$fecha_][0]; 
	}else{
		$ipc_valor = $ipc[$ano_recorrido."01"][1]; 
	}
	/*echo "vez: ".$vez."<br>";
	echo "ano_recorrido: ".$ano_recorrido."<br>";
	echo $ipc_valor."<br>";
	echo "factor: ".$factor."<hr>";
	*/
	$factor = ($ipc_valor / 100) + 1; 
	
	// Vida util remanente 
	if($vez == 1){
		$vida_util_remanente = $vida_util;
	}else{
		$vida_util_remanente = $meses_saldo;
	}

	// Meses saldo segun la vida util
	$meses_saldo = $vida_util - $meses_acum;  

	// Nuevo Valor Activo Revalorizado
	$var = $valor_activo * $factor; 

	// Valor de la correccion monetaria
	$valor_cm = $var - $valor_activo; 

	// Valor de la Cuota mensual
	$cuota_mes = $var / $vida_util; 

	// Ejercicio del año (la primera vez multiplico por los meses transc, sino lo hago fijo por 12)	
	if($vez == 1){
		$ejercicio = $cuota_mes * $meses;
		$meses_a_dep = $meses;
	}else{
		// Analizamos el ultimo año... si no es el ultimo 
		if($vida_util_remanente >= 12){
			$ejercicio = $cuota_mes * 12;
			$meses_a_dep = 12;
		}else{ 
			$ejercicio = $cuota_mes * $vida_util_remanente;
			$meses_a_dep = $vida_util_remanente;
		}
	}  

	// Depreciacion Acumulada del ejercicio
	$depreciacion_acumulada_ejercicio = $cuota_mes * $meses_acum;

	// Valor libro
	$valor_libro = $var - $depreciacion_acumulada_ejercicio; 

	// Valor CM Depreciado
	$valor_cm_acum = $depreciacion_acumulada * $factor;
	$valor_cm_dep = $valor_cm_acum - $depreciacion_acumulada;

	// Valor Neto
	$valor_neto = $var - $valor_cm_acum;

	// Analicemos ya habiendo calculado todo........ SI ES QUE TIENE BAJA
	if($baja == $ano_recorrido){ 
		$meses_a_dep += $meses_saldo;
		$meses_saldo = 0;
		$ejercicio = $valor_neto;
		$depreciacion_acumulada_ejercicio = $valor_cm_acum + $ejercicio;
		$valor_libro = 0;
	}

	// Si estoy en el año solicitado, entonces entrego los resultados
	if($ano_recorrido == $ano){
		return array(
			"dep_acumulada"=>$depreciacion_acumulada,
			"dep_acumulada_ejercicio"=>$depreciacion_acumulada_ejercicio,
			"valor_libro"=>$valor_libro,
			"cuota_mes"=>$cuota_mes,
			"valor_activo"=>$valor_activo,
			"saldo_ant_activo"=>$saldo_ant_activo,
			"var"=>$var,
			"valor_cm"=>$valor_cm,
			"valor_cm_dep"=>$valor_cm_dep,
			"valor_cm_acum"=>$valor_cm_acum,
			"valor_neto"=>$valor_neto,
			"factor"=>$factor,
			"ipc_valor"=>$ipc_valor,
			"ejercicio"=>$ejercicio,
			"meses"=>$meses,
			"meses_acum"=>$meses_acum,
			"meses_saldo"=>$meses_saldo,
			"meses_a_dep"=>$meses_a_dep,
			"vida_util_remanente"=>$vida_util_remanente
		);
	}else{
		// Aumento para ir a recorrer el siguiente año
		$ano_recorrido++;
		// Saldo Anterior del Activo
		$saldo_ant_activo = $var;
		// Actualizamos el valor de la compra
		$valor_activo = $var;

		// Si tengo meses de saldo (vida util), recorro el proximo año
		if($meses_saldo > 0){
			// Aumento los meses transcurridos en un año (12 meses)
			if($meses_saldo > 12){
				$meses_acum += 12; 
			}else{
				$meses_acum += $meses_saldo;
			}
			return revalorizacion_activo($valor_activo,$ano_recorrido,$ano,$ipc,$depreciacion_acumulada_ejercicio,$fecha,$vez,$meses,$meses_acum,$vida_util,$saldo_ant_activo,$meses_saldo,$baja);
		}else{
			return array(
				"dep_acumulada"=>$depreciacion_acumulada,
				"dep_acumulada_ejercicio"=>$depreciacion_acumulada_ejercicio,
				"valor_libro"=>$valor_libro,
				"cuota_mes"=>$cuota_mes,
				"valor_activo"=>$valor_activo,
				"saldo_ant_activo"=>$saldo_ant_activo,
				"var"=>$var,
				"valor_cm"=>$valor_cm,
				"valor_cm_dep"=>$valor_cm_dep,
				"valor_cm_acum"=>$valor_cm_acum,
				"valor_neto"=>$valor_neto,
				"factor"=>$factor,
				"ipc_valor"=>$ipc_valor,
				"ejercicio"=>$ejercicio,
				"meses"=>$meses,
				"meses_acum"=>$meses_acum,
				"meses_saldo"=>$meses_saldo,
				"meses_a_dep"=>$meses_a_dep,
				"vida_util_remanente"=>$vida_util_remanente
			);
		}
	}
}

/****************************************************************************************/
// Creo un Array con todas las tablas de IPC.............................................
$res = mysqli_query($cnx,"select * from tabla_ipc");
if(mysqli_num_rows($res) > 0){
    while($row = mysqli_fetch_array($res)){
        $ipc[$row["ipc_periodo"]] = array($row["ipc_valor"],$row["ipc_anterior"]);
    }
} 
/****************************************************************************************/ 
if($_REQUEST["fil_codigo2"] <> ""){
    $filtros .= " and acf_codigo like '".$_REQUEST["fil_codigo2"]."' ";
}
if($_REQUEST["fil_codigo"] <> ""){
    $filtros .= " and pro_codigo like '".$_REQUEST["fil_codigo"]."' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
    $filtros .= " and pro_nombre like '".$_REQUEST["fil_descripcion"]."' ";
} 
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and pro.pro_nombre like '".$_REQUEST["fil_descripcion"]."' ";
}   
if($_REQUEST["fil_categoria"] <> ""){
	$filtros .= " and pro.pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
}
if($_REQUEST["fil_cco"] <> ""){
	$filtros .= " and acf.acf_cco_id = '".$_REQUEST["fil_cco"]."' ";
}    
if($_REQUEST["fil_tipo"] <> ""){
	$filtros .= " and pro.pro_tipo = '".$_REQUEST["fil_tipo"]."' ";
} 
?>
<input type="hidden" name="pagina" id="pagina" value="inf_de_depreciacion_por_periodo_trib_datos.php" class="campos">
<?
if($_REQUEST["fil_periodo"] <> ""){
	?> 
	<table class="table table-striped table-bordered tabledrag table-condensed" border="1" style="font-size: 10px !important;" id="tablita_1"> 
	<thead>
		<tr>     
			<th>CodActivo</th>  
			<th>Descripcion</th>
			<th>CCosto</th> 
			<th>Fecha Ingreso</th>
			<th>Fecha Baja</th>
			<th>Docto Ingreso</th> 
			<th>Fecha Inicio Deprec.</th>
			<th>Fecha Termino Deprec.</th>

			<th>Valor Compra Activo</th> 
			<th title="Saldo Anterior Activo Actualizado">Saldo Ant. Actualiz</th> 
			<th>C.M. %</th>
			<th>Valor C.M.</th>
			<th>Valor AF Actualizado</th>

			<th title="Depreciación Acumulada">Deprec Acc</th>
			<th>Valor CM Dep</th>
			<th title="Depreciación Acumulada Actualizada">Dep.Acc Actualiz</th> 
			<th>Valor Neto</th>

			<th title="Vida Util Remanente">VU Rem</th>
			<th title="Meses a Depreciar">MAD</th>
			<th title="Vida Util Restante">VU Res</th>
			<th title="Depreciación del Ejercicio">Dep.del Ejerc</th>
			<th title="Depreciación Acumulada del Ejercicio">Dep.Acc Ejerc</th>
			<th>Valor Libro</th>
		</tr> 
	</thead>
	<tbody>
	<?php 
	$res = sql_activos_fijos_rapido("acf.acf_mes_ano_inicio_depreciacion,
	acf.acf_fecha_salida, acf.acf_valor,acf.acf_vida_util,acf.acf_mes_ano_termino_depreciacion,
	acf.acf_codigo, acf.acf_cco_id,acf.acf_fecha_ingreso,acf.acf_proveedor,pro.pro_nombre,
	acf.acf_nro_factura  ",
	" and 
	acf.acf_mes_ano_inicio_depreciacion <= '".$_REQUEST["fil_periodo"]."-12-31' $filtros","","");   
	if(mysqli_num_rows($res) > 0){
		while($row = mysqli_fetch_array($res)){
			$fecha_reval = substr($row["acf_mes_ano_inicio_depreciacion"],0,4);
			$mes = substr($row["acf_mes_ano_inicio_depreciacion"],5,2) * 1;
			$meses_transcurridos = 12 - ($mes - 1);
			if($row["acf_fecha_salida"] <> "0000-00-00"){
				$baja = substr($row["acf_fecha_salida"],0,4) * 1;
			}else{
				$baja = "";
			}

			$valores = revalorizacion_activo($row["acf_valor"],$fecha_reval,$_REQUEST["fil_periodo"],$ipc,0,$row["acf_mes_ano_inicio_depreciacion"],0,$meses_transcurridos,$meses_transcurridos,$row["acf_vida_util"],0,0,$baja);
			$contador++;
			 
			?> 
			<tr>  
				<td><? echo $row["acf_codigo"]; ?></td>
				<td><? echo $row["pro_nombre"]; ?></td> 
				<td><? echo $_centro_costos_corto[$row["acf_cco_id"]]; ?></td> 
				<td><?php echo _fec($row["acf_fecha_ingreso"],10); ?></td>
				<td><?php echo _fec($row["acf_fecha_salida"],10); ?></td> 
				<td><?php echo $row["acf_nro_factura"]; ?></td> 
				<td><?php echo substr($row["acf_mes_ano_inicio_depreciacion"],0,7); ?></td> 
				<td><?php echo substr($row["acf_mes_ano_termino_depreciacion"],0,7); ?></td>

				<td><?php echo _num($row["acf_valor"]); ?></td> 
				<td><? echo _num($valores["saldo_ant_activo"]); ?></td>
				<td><? echo _num($valores["ipc_valor"],2); ?></td>
				<td><? echo _num($valores["valor_cm"]); ?></td>
				<td><? echo _num($valores["var"]); ?></td>
 
				<td><? echo _num($valores["dep_acumulada"]); ?></td>  
				<td><? echo _num($valores["valor_cm_dep"]); ?></td> 
				<td><? echo _num($valores["valor_cm_acum"]); ?></td> 
				<td><? echo _num($valores["valor_neto"]); ?></td>

				<td><? echo $valores["vida_util_remanente"]; ?></td>
				<td><? echo $valores["meses_a_dep"]; ?></td>
				<td><? echo $valores["meses_saldo"]; ?></td>
				<td><? echo _num($valores["ejercicio"]); ?></td>
				<td><? echo _num($valores["dep_acumulada_ejercicio"]); ?></td> 
				<td><? echo _num($valores["valor_libro"]); ?></td>  
			</tr>
			<?  
			$total_acf_valor += $row["acf_valor"];
			$total_valor_cm += $valores["valor_cm"];
			$total_dep_acumulada += $valores["dep_acumulada"];
			$total_ejercicio += $valores["ejercicio"];
			$total_dep_acumulada_ejercicio += $valores["dep_acumulada_ejercicio"];
			$total_valor_libro += $valores["valor_libro"];
		} 
	} 
	?> 
	<tr style="background-color:#FFFBD2;">     
		<th></th>  
		<th></th>
		<th></th> 
		<th></th>
		<th></th>
		<th></th> 
		<th></th> 
		<th></th> 
		<th style="text-align:right;"><? echo _num($total_acf_valor); ?></th>
		<th></th> 
		<th></th>
		<th style="text-align:right;"><? echo _num($total_valor_cm); ?></th>
		<th></th>

		<th style="text-align:right;"><? echo _num($total_dep_acumulada); ?></th>
		<th></th>
		<th></th>
		<th></th>

		<th></th>
		<th></th>
		<th></th>
		<th style="text-align:right;"><? echo _num($total_ejercicio); ?></th>
		<th style="text-align:right;"><? echo _num($total_dep_acumulada_ejercicio); ?></th>
		<th style="text-align:right;"><? echo _num($total_valor_libro); ?></th>
	</tr>
	</tbody> 
	</table>

	<script language="javascript"> 
	$("#tablita_1").tableExport({
	    formats: ["xlsx"], //Tipo de archivos a exportar ("xlsx","txt", "csv", "xls")
	    position: 'button',  // Posicion que se muestran los botones puedes ser: (top, bottom)
	    bootstrap: false,//Usar lo estilos de css de bootstrap para los botones (true, false)
	    fileName: "Inf.Depreciacion",    //Nombre del archivo 
	    buttonContent: "Exportar Detalle a Excel",
	});
	</script> 
	<?  
}else{
	?>
	<div class="alert alert-info">
		Debe filtrar por un <b>periodo</b>
	</div>
	<?
}
?>