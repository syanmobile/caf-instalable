<div class="col-md-<? echo $ancho; ?> grid-item">
	<div class="panel panel-primary">  
		<div class="panel-heading">
			<b>Ubicaciones con más Activos</b>
		</div>
		<div class="panel-body"> 
			<?php
			$res = mysqli_query($cnx,"select * from resumen_ubicacion order by res_total desc limit 0,10");
			if(mysqli_num_rows($res) > 0){
				?>
				<table class="table table-condensed table-bordered"> 
				<thead>
					<tr>
						<th width="1">Stock</th>
						<th>Ubicación</th>
					</tr>
				</thead>
				<tbody>
				<? 
				while($row = mysqli_fetch_array($res)){ 
					?>
					<tr>  
						<th style="text-align: center;"><? echo _num2($row["res_total"]); ?></th>
						<td><? echo $_ubicaciones[$row["res_ubicacion"]]; ?></td> 
					</tr>
					<? 
				}
				?>  
				</tbody>
				</table>
				<? 
			}else{
				?>
				<div class="alert alert-danger">
					<strong>Sin stock de activos</strong>
				</div>
				<?php 
			}
			?>  
		</div>
	</div>
</div>