<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Administración de Perfiles de Usuario";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function save(){ 
	var div = document.getElementById("datos_"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/pfl_ajax.php",a+"&modo=grabar_todos",div);
}
</script> 
   
<?
$navegacion_completa = file_get_contents("http://www.controldeactivos.cl/central/json_navegacion.php");
$grupos_nav = explode("___",$navegacion_completa);
?>
<div id="datos_">
<table class="table table-striped table-bordered table-condensed"> 
<thead>
<tr>
	<?php
	$res = sql_perfiles("*","   ORDER BY pfl.pfl_nombre asc");
	if(mysqli_num_rows($res) > 0){
		while($row = mysqli_fetch_array($res)){
			?>
			<input type="hidden" value="<?php echo $row["pfl_id"]; ?>" name="perfil[]" class="campos">
			<th width="1"><?php echo $row["pfl_nombre"]; ?></th>
			<?
			$perfiles[] = $row["pfl_id"];
			$perfiles_acceso[$row["pfl_id"]] = explode(",",$row["pfl_navegacion"]);
		}
	}
	?>
	<th colspan="2">Navegación</th>
</tr>
</thead>
<tbody> 
<?php
foreach($grupos_nav as $grupos_nav_){
	$partes_nav = explode(";;;",$grupos_nav_);
	$gr++;
	?>
    <tr>
		<th style="background-color: gold;" colspan="<?php echo count($perfiles); ?>"></th>
		<th style="background-color: gold;" colspan="2"><? echo $partes_nav[0]; ?></th> 
	</tr>
    <? 
	$secciones = explode("---",$partes_nav[1]);
    foreach($secciones as $seccion){
    	$seccion_datos = explode("|||",$seccion);
    	if(trim($seccion_datos[0]) <> ""){
			?>
			<tr>   
				<?php
				foreach($perfiles as $perfil){
					?>
					<td>
						<input type="checkbox" name="acc_<?php echo $perfil; ?>[]" value="<?php echo $seccion_datos[0]; ?>" class="campos" <?
						if(in_array($seccion_datos[0],$perfiles_acceso[$perfil])){ 
							echo "checked"; 
						} ?>>	
					</td>
					<?
				}
				?>
				<th width="1"><?php echo $seccion_datos[0]; ?></th>
				<td><?php echo $seccion_datos[1]; ?></td>
			</tr>
			<?  
		}
	}
}
?>  
</tbody>
</table> 
<?php
construir_boton("","","grabar","Guardar",3);
construir_boton("pfl_listado.php","","eliminar","Cancelar",2);
?> </div>   