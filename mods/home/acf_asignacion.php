<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Asignación de Activos Fijos";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "acf_asignacion.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function save(){  
	if(document.getElementById("responsable").value == ""){
		alerta_js("Es obligación seleccionar al RESPONSABLE");
		return;	
	}  
	if(document.getElementById("glosa").value == ""){
		alerta_js("Es obligación ingresar NOTAS");
		return;	
	}
	if((document.getElementById("total_lineas").value * 1) > 0){
		if(confirm("Grabar Documento?")){
			carg("acf_save.php","");
		}
	}else{
		alerta_js("Sin activos en el detalle");
	}
}
function cargar_productos(){ 
	if(document.getElementById("ubicacion").value == ""){ 
		$("#disponibles").html("");
	}else{
		document.getElementById("lineas").value = "";
		detalle();
		var div = document.getElementById("disponibles"); 
		var a = $(".campos").fieldSerialize();  
		$("#disponibles").html("<h4>Cargando activos fijos, espere un momento...</h4>");
		AJAXPOST("mods/home/acf_asignacion_ajax.php",a+"&modo=disponibles",div);
	}
}
function detalle(){
	var div = document.getElementById("detalle"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/acf_prestamo_ajax.php",a+"&modo=detalle",div);
}
function elegir(acf){
	$("#acf"+acf).hide("fast");
	document.getElementById("lineas").value = document.getElementById("lineas").value + "," + acf; 
	detalle();
}
function quitar_linea(acf){
	$("#acf"+acf).show("fast");
	
	var str = document.getElementById("lineas").value;
	document.getElementById("lineas").value = str.replace(","+acf, "");
	detalle();
}
</script>

<table style="margin-bottom:5px; width:100%">
<tr valign="top"> 
    <td width="400"> 
    	<input type="hidden" name="modo" value="asignacion" class="campos" />
    	<input type="hidden" name="tipo" id="tipo" value="A" class="campos" /> 
    	<input type="hidden" name="concepto" value="10" class="campos" /> 
    	<input type="hidden" name="fecha" value="<? echo date("d/m/Y"); ?>" class="campos"> 
    	
		<table class="table table-striped table-bordered table-condensed"> 
        <thead>
            <tr>
                <th colspan="4">Información Movimiento</th>
            </tr>
        </thead>
        <tbody> 
		    <tr>
		        <th style="text-align:right;">Folio</th>
		        <td><input type="text" value="Autom." disabled class="campos w5"></td>
		    </tr>
		    <tr>
		        <th style="text-align:right;">Ubicación</th>
		        <td>
		            <select name="ubicacion" id="ubicacion" class="campos buscador w10" onchange="javascript:cargar_productos();">
					<option value=""></option>
					<?php
					$res = sql_ubicaciones("*"," order by bod.bod_nombre asc, ubi.ubi_nombre asc"); 
					if(mysqli_num_rows($res) > 0){
						while($row = mysqli_fetch_array($res)){
							?>
							<option value="<? echo $row["ubi_id"]; ?>" <?
							if($_REQUEST["fil_ubicacion"] == $row["ubi_id"]){ echo "selected"; } 
							?>>U<? echo $row["ubi_codigo"]." - ".$row["ubi_nombre"]; ?></option>
							<?
						}
					}
					?></select>
		        </td>
		    </tr>
            <tr>
                <th width="100" style="text-align:right;">Responsable:</th> 
				<td>
				<select name="responsable" id="responsable" class="campos buscador">
				<option value=""></option>
				<?php 
				$res = sql_personas("*"," and per.per_tipo = 1 and per.per_elim = 0  ORDER BY per.per_nombre asc ");  
				if(mysqli_num_rows($res) > 0){
					while($row = mysqli_fetch_array($res)){
						?>
						<option value="<? echo $row["per_id"]; ?>"><?php echo $row["per_nombre"]; ?></option>
						<?
					}
				} 
				?>
				</select>
				</td>   
			</tr> 
			<tr>
				<th style="text-align:right;">Centro Costo:</th>
				<td><?php input_centro_costo(''); ?></td>
			</tr>  
            <tr>
                <th style="text-align:right;">Notas:</th>
                <td><input type="text" name="glosa" id="glosa" class="campos w10" value=""></td>
            </tr>
            <?php 
			for($i = 1;$i <= 5;$i++){
				if(_opc("extra_mov_".$i) > 0){
					$res = sql_campos("*"," and cam_id = "._opc("extra_mov_".$i));  
					$row = mysqli_fetch_array($res); 
					?> 
					<tr> 
						<th style="text-align: right;"><? echo $row["cam_nombre"]; ?>:</th>
						<td><? 
			            campo_dinamico($row["cam_tipo"],"extra_".$i,$row["cam_opciones"],"",$url_base);
			            ?></td> 
					</tr> 
					<?php
				}
			} 
			?> 
        </tbody>
        </table> 
        <div id="detalle" style="margin: 10px 0px;"></div>
        <input type="hidden" id="lineas" name="lineas" class="campos"> 
		<?php 
        construir_boton("","","grabar","Guardar Movimiento",3); 
        ?>
		<br><br>
   		<div id="operacion"></div>	
    </td> 
	<td style="padding-left:10px;" id="disponibles"> 
    </td>
</tr>
</table> 