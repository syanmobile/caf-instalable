<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Proveedores";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "aux_listado.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>  
<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td><?php
		construir_boton("aux_nuevo.php","","crear","Nuevo Proveedor",2);   
		construir_boton("aux_importar.php","","importar","Importar Proveedores",2);    
		?></td>
    </tr>
</tbody>
</table> 

<table class="table table-bordered table-condensed table-info" style="margin-bottom:0px !important;">  
<tbody> 
<tr>
    <td width="100">
    <b>Código:</b><br />
    <input type="text" name="fil_codigo" id="fil_codigo" class="campos w10" value="<? echo $_REQUEST["fil_codigo"]; ?>"></td> 
    <td width="100">
    <b>RUT:</b><br />
    <input type="text" name="fil_rut" id="fil_rut" class="campos w10" value="<? echo $_REQUEST["fil_rut"]; ?>"></td> 
    <td>
    <b>Descripción:</b><br />
    <input type="text" name="nombre_buscando" id="nombre_buscando" class="campos w10" value="<? echo $_REQUEST["nombre_buscando"]; ?>"></td>  
    <td width="1"><?
	construir_boton("aux_listado.php","","buscar","Filtrar");
	?></td>
</tr>
</tbody>
</table>
<?php 
if($_REQUEST["nombre_buscando"] <> ""){
	$adicional .= "and (aux.aux_nombre like '%".$_REQUEST["nombre_buscando"]."%') ";
}
if($_REQUEST["fil_codigo"] <> ""){
	$adicional .= "and aux.aux_codigo like '%".$_REQUEST["fil_codigo"]."%' ";
}
if($_REQUEST["fil_rut"] <> ""){
	$adicional .= "and aux.aux_rut like '%".$_REQUEST["fil_rut"]."%' ";
}
?>
<input type="hidden" name="pagina" id="pagina" value="aux_listado.php" class="campos">
<?  
$sql_paginador = sql_auxiliares("*"," $adicional ORDER BY aux.aux_nombre asc","S");  
require("../../_paginador.php");
$res = mysqli_query($cnx,$sql_final);
if(mysqli_num_rows($res) > 0){
	?> 
	<table class="table table-striped table-bordered table-condensed"> 
	<thead>
	<tr>
		<th width="1">&nbsp;</th>
		<th width="1">Código</th>
		<th width="1">Rut</th>
		<th>Nombre Proveedor</th>
		<th>Dirección</th>
		<th>Comuna</th>
		<th>Telefono</th> 
		<th>Correo</th> 
	</tr>
	</thead>
	<tbody> 
	<?
	while($row = mysqli_fetch_array($res)){ 
		?>
		<tr fila="<?php echo $row["aux_codigo"] ;?>" id="<?php echo $row["aux_codigo"] ;?>">  
			<td style="text-align:center;"><?php
			//construir_boton("aux_ver.php","&codigo=".$row["aux_codigo"],"buscar","Ficha Auxiliar"); 
			construir_boton("aux_editar.php","&id=".$row["aux_id"],"editar","Editar");
			?></td>
			<td><?php echo $row["aux_codigo"]; ?></td>
			<td><?php echo $row["aux_rut"]; ?></td>
			<td><?php echo $row["aux_nombre"]; ?></td>
			<td><?php echo $row["aux_direccion"]; ?></td>
			<td><?php echo $row["aux_comuna"]; ?></td>
			<td><?php echo $row["aux_telefono"]; ?></td>
			<td><?php echo $row["aux_correo"]; ?></td>
		</tr>
		<? 
	}
	?> 
	</tbody>
	</table> 
	<?php
	echo $paginador_dibujo;
}else{
	?>
	<div class="alert alert-danger">
		<strong>Sin registros creados</strong>
	</div>
	<?php
} 
?>