<?php 
require("../../inc/conf_dentro.php");

construir_boton("vista('');","","buscar","Ver Todo",4);
construir_boton("vista('rojo');","","buscar","Fallas",4);
construir_boton("vista('ok');","","buscar","Registros OK",4);

// Cargar codigos....
$res = sql_ubicaciones("*",""); 
if(mysqli_num_rows($res) > 0){
	while($row = mysqli_fetch_array($res)){
		$codigos[] = $row["ubi_codigo"];
	}
}
?>
<table class="table table-bordered table-hover table-condensed"> 
<thead> 
    <tr>
        <th width="1">#</th>
        <th width="1">Código</th>
        <th>Nombre</th>
        <th>Bodega</th>
        <th>Validación</th>
    </tr>
</thead>
<tbody>
<?php 
$capt = date("YmdHis");
if (isset($_FILES["archivo"]) && is_uploaded_file($_FILES['archivo']['tmp_name'])) {							
    $fp = fopen($_FILES['archivo']['tmp_name'], "r");
    while (!feof($fp)){
		$data = explode(";", fgets($fp));  

		$nlinea++;
		if($nlinea > 1){  
			if(count($data) > 1){  
				$codigo = rtrim($data[0]);
				$nombre = _u8e($data[1]);
				$cbod = rtrim($data[2]);
				$total_campos = 3;

				//Validaciones
				$errores = ""; 
				if($codigo == "" || $nombre == "" || $cbod == ""){ 
					$errores.= "<li>Un campo obligatorio no ingresado</li>";
				}
				if(in_array($codigo,$codigos)){ 
					$errores.= "<li>La Ubicación ya existe</li>";
				}
				if(!bodega_existe($cbod)){ 
					$errores.= "<li>No existe la Bodega</li>";
				}
				$linea++;
				?>
				<tr class="fila_<?php echo ($errores <> "")?"rojo":"ok"; ?>">
					<td><? echo $linea; ?></td>
					<?
					for($i = 0;$i < $total_campos;$i++){
						?><td><? echo _u8e($data[$i]); ?></td><?
					}

					if($errores <> ""){
						$fallo = "S";
						?><td style="padding-left:15px;" class="alert alert-danger"><? echo $errores; ?></td><?php
					}else{
						?><td style="padding-left:15px;"  class="alert alert-success"><li>Todo OK</li></td><?php
					}
					?>
				</tr>
				<?  
			}
		}// nlinea > 1
    }
} 
?>
</tbody>
</table>

<script language="javascript"> 
function grabar(){
    <?php
    if($fallo == ""){
        ?>
        document.multiple_upload_form.action = "<?php echo $url_web; ?>mods/home/ubi_importar_grabacion.php";
        $('#multiple_upload_form').ajaxForm({
            target:'#procesando_upload',
            beforeSubmit:function(e){
                $('.uploading').show();
            },
            success:function(e){
                $('.uploading').hide();
            },
            error:function(e){
            }
        }).submit();
        <?
    }else{
        ?>
		alert("Deben estar todos los datos validados");
        <?
    }
    ?>
}
</script> 