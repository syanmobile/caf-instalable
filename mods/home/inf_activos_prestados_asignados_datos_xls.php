<?php
header('Content-Type: text/csv');
header("Content-Disposition:  filename=\"activos_prestados_asignados.csv\";");

include("../../inc/conf_dentro.php");

if($_REQUEST["fil_codigo2"] <> ""){
    $filtros .= " and acf_codigo like '".$_REQUEST["fil_codigo2"]."' ";
}
if($_REQUEST["fil_codigo"] <> ""){
    $filtros .= " and pro_codigo like '".$_REQUEST["fil_codigo"]."' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
    $filtros .= " and pro_nombre like '".$_REQUEST["fil_descripcion"]."' ";
}  
if($_REQUEST["fil_categoria"] <> ""){
    $filtros .= " and pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
} 
if($_REQUEST["fil_ubicacion"] <> ""){
    $filtros .= " and acf_ubicacion = '".$_REQUEST["fil_ubicacion"]."' ";
}  
if($_REQUEST["fil_responsable"] <> ""){
    $filtros .= " and acf_responsable = '".$_REQUEST["fil_responsable"]."' ";
}
echo "Cod.Activo;Cod.Producto;Producto;Fecha Entrega/Asignado el;Responsable;Estado;Situaci&oacute;n
";

$res = sql_activos_fijos("*"," and acf_disponible = 2 and pro_tipo <> 'INS' order by acf_fecha_pres_asig_compromiso desc, pro_nombre asc"); 
if(mysqli_num_rows($res) > 0){ 
    while($row = mysqli_fetch_array($res)){ 
        echo $row["acf_codigo"];
        echo ";";
        echo $row["pro_codigo"];
        echo ";";
        echo _u8d($row["pro_nombre"]);
        echo ";";
        echo _fec($row["acf_fecha_pres_asig_compromiso"],5);
        echo ";";
        echo _u8d($_personas[$row["acf_responsable"]]);
        echo ";";
        $dias = dias_transcurridos($row["acf_fecha_pres_asig_compromiso"],date("Y-m-d")); 
        if($dias < 0){
            echo "ATRASADO";
            echo ";";
            echo ($dias * -1)." dias de atraso";
        }else{
            echo "AL DIA";
            echo ";";
            echo $dias." dias para entregar";
        }
        echo "
";
    }
}

$res = sql_activos_fijos("*"," and acf_disponible = 6 and pro_tipo <> 'INS' 
order by acf_fecha_pres_asig_compromiso desc, pro_nombre asc"); 
if(mysqli_num_rows($res) > 0){ 
    while($row = mysqli_fetch_array($res)){  
        echo $row["acf_codigo"];
        echo ";";
        echo $row["pro_codigo"];
        echo ";";
        echo _u8d($row["pro_nombre"]);
        echo ";";
        echo _fec($row["acf_fecha_pres_asig"],5);
        echo ";";
        $dias = dias_transcurridos(date("Y-m-d"),$row["acf_fecha_pres_asig"]); 
        echo _u8d($_personas[$row["acf_responsable"]]);
        echo ";";
        echo "ASIGNADO";
        echo ";";
        echo $dias." dias asignado";
        echo "
";
    }
}
?>