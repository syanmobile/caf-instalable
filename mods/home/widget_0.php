<div class="col-md-<? echo $ancho; ?> grid-item">
	<div class="panel panel-primary">  
		<div class="panel-heading">
			<b>Insumos con Stock Critico</b>
		</div>
		<div class="panel-body"> 
			<?php
			$sql = "select *,sum(total) as stock from insumos group by pro_id order by pro_nombre asc ";
			$res = mysqli_query($cnx,$sql);  
			if(mysqli_num_rows($res) > 0){
				?>
				<table class="table table-condensed table-bordered"> 
				<thead>
					<tr>
						<th width="1">xComprar</th>
						<th>Insumo</th>
					</tr>
				</thead>
				<tbody>
				<?
				while($row = mysqli_fetch_array($res)){
					if($row["pro_stock_minimo"] >= $row["stock"]){
						?>
						<tr>  
							<th style="text-align: center;"><? echo _num2($row["pro_stock_minimo"] - $row["stock"]); ?></th>
							<td><? echo $row["pro_nombre"]; ?></td> 
						</tr>
						<? 
					}
				}
				?>  
				</tbody>
				</table>
				<?
			}else{
				?>
				<div class="alert alert-danger">
					<strong>Sin stock de insumos</strong>
				</div>
				<?php 
			}
			?>  
		</div>
	</div>
</div> 