<?php
require("../../inc/conf_dentro.php");
require("../../_css.php");

$res3 = sql_activos_fijos("*","and pro_garantia = 1"); 
$productos = mysqli_num_rows($res3);

$res3 = sql_activos_fijos("*","and acf_fecha_garantia = '0000-00-00' and pro_garantia = 1"); 
$sin_mantencion = mysqli_num_rows($res3);

$res3 = sql_activos_fijos("*","and acf_fecha_garantia <> '0000-00-00' and acf_fecha_garantia < '".date("Y-m-d")."' and pro_garantia = 1 "); 
$atrasadas = mysqli_num_rows($res3);

$res3 = sql_activos_fijos("*","and acf_fecha_garantia <> '0000-00-00' and acf_fecha_garantia >= '".date("Y-m-d")."' and pro_garantia = 1 "); 
$al_dia = mysqli_num_rows($res3);
 
if($productos == 0){
	?>
	<div class="alert alert-danger" style="margin:20px;">
		<strong>Sin activos fijos</strong>
	</div>
	<?php 
	exit(); 
}
$sin_mantencion = round(_porc($sin_mantencion,$productos),1);
$al_dia = round(_porc($al_dia,$productos),1);
$atrasadas = round(_porc($atrasadas,$productos),1); 
?>
<!DOCTYPE HTML>
<html>
<head>
<script>
window.onload = function () {

	CanvasJS.addColorSet("customColorSet1",["#f02522","#3dba44","#ffff4f"]);

	var options = {
		animationEnabled: true, 
		colorSet: "customColorSet1",
		data: [{
			type: "doughnut",
			innerRadius: "30%", 
			legendText: "{label}",
			indexLabel: false,
			dataPoints: [
				{ label: "Vencidas", y: <? echo $atrasadas; ?> },
				{ label: "Al dia", y: <? echo $al_dia; ?> },
				{ label: "Sin info", y: <? echo $sin_mantencion; ?> }
			]
		}]
	};
	$("#chartContainer").CanvasJSChart(options);

}
</script>
</head>
<body>
<div id="chartContainer" style="height: 290px; width: 100%;"></div>
<script src="../../js/jquery-1.11.1.min.js"></script>
<script src="../../js/jquery.canvasjs.min.js"></script>
</body>
</html>