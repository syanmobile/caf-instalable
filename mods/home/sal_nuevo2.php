<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Nuevo Documento Salida"; 
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
$(document).ready(function(){
	detalle();
});
function ubicaciones(){
	var div = document.getElementById("ubicacion"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/ubi_ajax.php",a+"&modo=opciones",div);
}
function elegir(){
	var div = document.getElementById("detalle_pro"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/sal_ajax2.php",a+"&modo=elegir_pro",div);
}
function detalle(){
	var div = document.getElementById("detalle"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/sal_ajax2.php",a+"&modo=detalle",div);
} 
function save(){ 
	if(document.getElementById("folio").value == ""){
		alerta_js("Es obligación ingresar el FOLIO");
		return;	
	} 
	if(document.getElementById("fecha").value == ""){
		alerta_js("Es obligación ingresar la FECHA del documento");
		return;	
	} 
	if(document.getElementById("concepto").value == ""){
		alerta_js("Es obligación ingresar el CONCEPTO");
		return;	
	}
	if(document.getElementById("ubicacion").value == ""){
		alerta_js("Es obligación seleccionar una UBICACION");
		return;	
	}
	if(document.getElementById("lineas").value == ""){
		alerta_js("Es obligación ingresar al menos una linea");
		return;	
	}   
	
	if(confirm("Grabar Documento Ingreso?")){
		carg("sal_save.php","");
	}
} 
</script>

<input type="hidden" name="modo" value="crear2" class="campos" />
<div id="operacion"></div>
<table width="100%">
<tr valign="top"> 
<td width="420"> 
    <table class="table table-bordered table-condensed"> 
    <thead>
    <tr>
    	<th colspan="10">Información General</th>
    </tr>	
    </thead> 
    <tbody>
    <tr>
        <th style="text-align:right;">Lugar</th>
        <td colspan="3">
            <?php input_bodega('','ubicaciones()'); ?>
        </td>
    </tr>   
    <tr>
        <th width="1" style="text-align:right;">Folio:</th>
        <td><input type="text" name="folio" id="folio" class="campos w10" value="" style="text-align:center; width: 100px !important;"></td>  
        <th width="1">Fecha:</th>
        <td><input type="text" name="fecha" id="fecha" value="<? echo date("d/m/Y"); ?>" class="campos fecha"></td>
    </tr>
    <tr>
        <th style="text-align:right;">Concepto:</th>
        <td colspan="3"><?php input_concepto('','S'); ?></td>
    </tr> 
	<tr>
		<th width="1" style="text-align:right;">Ubicacion:</th> 
		<td colspan="3">
		<select name="ubicacion" id="ubicacion" class="campos"> 
		</select>
		</td>   
	</tr>
	<tr>
        <th width="1" style="text-align:right;">Auxiliar:</th>
        <td colspan="3"><?php input_auxiliar2(''); ?></td>        
    </tr> 
    <tr>
    	<th style="text-align:right;">Responsable:</th>
    	<td colspan="3">
		<select name="responsable" id="responsable" class="campos buscador">
		<option value=""></option>
		<? 
		$res = sql_personas("*"," and per.per_tipo = 1 and per.per_elim = 0 ORDER BY per.per_nombre asc ");  
		if(mysqli_num_rows($res) > 0){
			while($row = mysqli_fetch_array($res)){
				?>
				<option value="<? echo $row["per_id"]; ?>"><?php echo $row["per_nombre"]; ?></option>
				<?
			}
		} 
		?>
		</select>
   		</td>
    </tr>    
    <tr>
        <th style="text-align:right;">Notas:</th>
        <td colspan="3"><input type="text" name="glosa" id="glosa" class="campos w10"></td> 
    </tr> 
	</tbody>   
    </table>
    <input type="hidden" name="tipo" id="tipo" value="E" class="campos" />
</td>
<td style="padding-left:10px;">
    <table class="table table-bordered table-condensed" style="margin-bottom: 0px !important;"> 
    <thead>
    <tr>
    	<th colspan="2">Agregar un Producto</th>
    </tr>	
    </thead> 
    <tbody>
    <tr>
        <th width="80" style="text-align: right;">Producto:</th>
        <td>
		    <select name="producto" id="producto" class="campos buscador" onchange="javascript:elegir();">
		    <option value=""></option>
			<?php
			// Sacar los productos con movimiento
			$res = sql_productos("*"," and pro_tipo <> 'INS' order by pro_nombre asc");
			$num = mysqli_num_rows($res);
			if($num > 0){
				while($row = mysqli_fetch_array($res)){
					?><option value="<?php echo $row["pro_id"]; ?>"><?php
					echo $row["pro_codigo"]." - ".$row["pro_nombre"];  
					?></option><?php 
				}
			}
		    ?>
		    </select>
		</td>
    </tr>
	</tbody>
	</table> 

 	<div id="detalle_pro"></div>
	<div id="detalle"></div>
</td>
</table> 
<?php 
construir_boton("","","grabar","Guardar Documento",3);
construir_boton("transacciones.php","","eliminar","Cancelar",2);
construir_boton("sal_nuevo2.php","","refrescar","Refrescar",2);
?> 