<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Listado Ubicaciones";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "ubi_listado.php"; 
include("ayuda.php");
//---------------------------------------------------------------------------------------- 
?>
<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td><?php 
		  construir_boton("ubi_nuevo.php","","crear","Nueva Ubicación",2);
          construir_boton("ubi_importar.php","","importar","Importar Ubicaciones",2);
        ?></td>
    </tr>
</tbody>
</table>
<?php
$res = sql_ubicaciones("*"," ORDER BY ubi_bodega asc, ubi.ubi_nombre asc","","");
if(mysqli_num_rows($res) > 0){
    ?>
    <table class="table table-striped table-bordered table-condensed" id="tablita"> 
    <thead>
    <tr>
        <th width="1" align="center">&nbsp;</th>
        <th width="1" align="center">Código</th>
        <th>Ubicación</th>  
        <th>Lugar</th>  
    </tr>
    </thead>
    <tbody> 
    <?
    while($row = mysqli_fetch_array($res)){ 
        ?>
        <tr fila="<?php echo $row["ubi_id"] ;?>" id="<?php echo $row["ubi_id"] ;?>">  
            <td style="text-align:center;"><?php
			construir_boton("ubi_editar.php","&id=".$row["ubi_id"],"editar","Editar");
			?></td> 
            <th style="text-align: center"><?php echo $row["ubi_codigo"]; ?></th>
            <td><?php echo $row["ubi_nombre"]; ?></td>  
            <td><?php echo $row["bod_codigo"]." - ".$row["bod_nombre"]; ?></td>  
        </tr>
        <? 
    }
    ?> 
    </tbody>
    </table>  
    
    <script language="javascript">
    $(document).ready(function() { 
        $('#tablita').DataTable({
          "columns": [   
            { "width": "1px" }, 
            { "width": "1px" },  
            null  ,  
            null  
          ],
          lengthMenu: [20000000],
          "bPaginate": false,
          "bLengthChange": false
        });
    } );   
    </script>  
    <?php
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin registros creados</strong>
    </div>
    <?php
}
?>