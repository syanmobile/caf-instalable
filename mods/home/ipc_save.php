<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabación"; 
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

switch($_POST["modo"]){   
		
	case "crear": 
		$res = mysqli_query($cnx,"select * from tabla_ipc where ipc_periodo like '$_POST[ano]%'");
		if(mysqli_num_rows($res) > 0){
			?> 
	        <table width="100%">
	        <tr valign="top">
	            <td width="300">
	                <div class="alert alert-danger"> 
	        			<span class="glyphicon glyphicon-remove" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
	                    Año ya fue creado<br />
	                    <b style="font-size:20px;"><? echo $_POST["ano"]; ?></b>
	                </div>
	                <?php 
					construir_boton("ipc_listado.php","","izquierda","Ir a Tabla Resumen",2);  
					?>
	            </td>
	            <td style="padding-left:15px;"> 
	            </td>
	        </tr>
	        </table>
			<?php 
		}else{

	    	for($i = 1;$i <= 12;$i++){
				$mes = ($i < 10)?"0".$i:$i;
				$sql = "INSERT INTO tabla_ipc ( 
					ipc_periodo,
					ipc_valor,
					ipc_anterior
				) VALUES (  
					'".$_POST["ano"].$mes."',
					'".$_POST["mes_".$i]."',
					'".$_POST["anterior"]."'
				)"; 
				$res = mysqli_query($cnx,$sql);
			}
			?> 
	        <table width="100%">
	        <tr valign="top">
	            <td width="300">
	                <div class="alert alert-success"> 
	        			<span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
	                    Año guardado con &eacute;xito<br />
	                    <b style="font-size:20px;"><? echo $_POST["ano"]; ?></b>
	                </div>
	                <?php 
					construir_boton("ipc_listado.php","","izquierda","Ir a Tabla Resumen",2);  
					?>
	            </td>
	            <td style="padding-left:15px;"> 
	            </td>
	        </tr>
	        </table>
			<?php  
		} 
		break;  
		
	case "editar": 
    	for($i = 1;$i <= 12;$i++){
			$mes = ($i < 10)?"0".$i:$i;
			$sql = "update tabla_ipc set 
				ipc_valor = '".$_POST["mes_".$i]."',
				ipc_anterior = '".$_POST["anterior"]."'  
			 where ipc_periodo = '".$_POST["ano"].$mes."' ";  
			$res = mysqli_query($cnx,$sql); 
		} 
		?> 
        <table width="100%">
        <tr valign="top">
            <td width="300">
                <div class="alert alert-success"> 
        			<span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
                    Año guardado con &eacute;xito<br />
                    <b style="font-size:20px;"><? echo $_POST["ano"]; ?></b>
                </div>
                <?php 
				construir_boton("ipc_listado.php","","izquierda","Ir a Tabla Resumen",2);  
				?>
            </td>
            <td style="padding-left:15px;"> 
            </td>
        </tr>
        </table>
		<?php 
		break; 
} 
?>