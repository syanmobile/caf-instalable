<?php 
require("../../inc/conf_dentro.php");

$capt = date("YmdHis");
if (isset($_FILES["archivo"]) && is_uploaded_file($_FILES['archivo']['tmp_name'])) {							
    $fp = fopen($_FILES['archivo']['tmp_name'], "r");
    while (!feof($fp)){
		$data = explode(";", fgets($fp));  
		$nlinea++;
		if($nlinea > 1){ 
			$codigo = rtrim($data[0]);
			$nombre = rtrim($data[1]);
			$descripcion = rtrim($data[2]);
			$codbarra = rtrim($data[3]);
			$tipo = rtrim($data[4]);
			$clasificacion = $_categorias_cod[rtrim($data[5])];
			$unidad = $_unidades_cod[rtrim($data[6])];
			$manteni = rtrim($data[7]);
			$garantia = rtrim($data[8]);
			$prestable = rtrim($data[9]);
			$asignable = rtrim($data[10]);

			$extra1 = rtrim($data[11]);
			$extra2 = rtrim($data[12]);
			$extra3 = rtrim($data[13]);
			$extra4 = rtrim($data[14]);
			$extra5 = rtrim($data[15]);
			$extra6 = rtrim($data[16]);
			$extra7 = rtrim($data[17]);
			$extra8 = rtrim($data[18]);
			$extra9 = rtrim($data[19]);
			$extra10 = rtrim($data[20]);

			if(count($data) > 1){		
				$sql_det = "insert into productos ( 
					pro_codigo, 
					pro_nombre, 
					pro_descripcion, 
					pro_codigo_barra, 
					pro_tipo, 
					pro_categoria, 
					pro_unidad, 
					pro_mantenimiento,
					pro_garantia,
					pro_prestable,
					pro_asignable,
					pro_extra_1,
					pro_extra_2,
					pro_extra_3,
					pro_extra_4,
					pro_extra_5,
					pro_extra_6,
					pro_extra_7,
					pro_extra_8,
					pro_extra_9,
					pro_extra_10
				) values (
					'$codigo', 
					'$nombre', 
					'$descripcion', 
					'$codbarra', 
					'$tipo',   
					'$clasificacion',
					'$unidad', 
					'$manteni', 
					'$garantia', 
					'$prestable', 
					'$asignable',
					'$extra1',
					'$extra2',
					'$extra3',
					'$extra4',
					'$extra5',
					'$extra6',
					'$extra7',
					'$extra8',
					'$extra9',
					'$extra10'
				)"; 
				$res_det = mysqli_query($cnx,$sql_det);
				$productos++;
			}
		} // nlinea > 1
    }
}
?> 
<div class="alert alert-success"> 
    <strong><?php echo $productos; ?> producto(s) cargado(s) con éxito</strong>
</div>
<script language="javascript">
document.multiple_upload_form.action = "<?php echo $url_web; ?>mods/home/pro_importar_validacion.php";
</script>