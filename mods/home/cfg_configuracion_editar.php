<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Mi Empresa";
construir_breadcrumb($titulo_pagina); 
//---------------------------------------------------------------------------------------- 
?>  
<script type="text/javascript">
function grabar(){ 
    var a = $(".campos").fieldSerialize();
    var div = document.getElementById("div_ajax");  
    AJAXPOST("mods/home/cfg_ajax.php",a,div); 
}
</script>

<?
if($_REQUEST["cfg"] == "1"){
    ?>
    <div class="alert alert-success">
        <b>Cambios Realizados</b>
    </div>
    <?
}
?>
<div id="div_ajax"></div>
<input type="hidden" name="modo" value="grabar" class="campos">

<table width="100%">
<tr valign="top">
    <td>
        <table class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <th colspan="10">Datos de mi Empresa</th>
        </tr>
        </thead>
        <tbody>
        <tr>  
            <th style="text-align: right;" width="110">RUT:</th> 
            <td style="text-align: left !important;" colspan="6"><input type="text" name="rut" class="campos w10" value="<? echo _opc("rut"); ?>"></td> 
        </tr>
        <tr>  
            <th style="text-align: right;">Nombre Empresa:</th> 
            <td style="text-align: left !important;" colspan="6"><input type="text" name="nombre_empresa" class="campos w10" value="<? echo _opc("nombre_empresa"); ?>"></td> 
        </tr>
        <tr>  
            <th style="text-align: right;">Razón Social:</th> 
            <td style="text-align: left !important;" colspan="6"><input type="text" name="razon_social" class="campos w10" value="<? echo _opc("razon_social"); ?>"></td> 
        </tr>
        <tr>  
            <th style="text-align: right;">Dirección:</th> 
            <td style="text-align: left !important;"><input type="text" name="direccion" class="campos w10" value="<? echo _opc("direccion"); ?>"></td> 
            <th style="text-align: right;" width="80">Ciudad:</th> 
            <td style="text-align: left !important;" width="250"><input type="text" name="ciudad" class="campos w10" value="<? echo _opc("ciudad"); ?>"></td> 
            <th style="text-align: right;" width="80">Región:</th> 
            <td style="text-align: left !important;" width="200"><input type="text" name="region" class="campos w10" value="<? echo _opc("region"); ?>"></td> 
        </tr>
        <tr>  
            <th style="text-align: right;">Telefonos:</th> 
            <td style="text-align: left !important;"><input type="text" name="telefonos" class="campos w10" value="<? echo _opc("telefonos"); ?>"></td>   
            <th style="text-align: right;">Correo:</th> 
            <td style="text-align: left !important;"><input type="text" name="correo" class="campos w10" value="<? echo _opc("correo"); ?>"></td> 
            <th style="text-align: right;">Web:</th> 
            <td style="text-align: left !important;"><input type="text" name="website" class="campos w10" value="<? echo _opc("website"); ?>"></td> 
        </tr>
        </tbody>
        </table>
    </td>
    <td width="350" style="padding-left: 10px;"> 

        <table class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <th colspan="10">Tipo Depreciación Empresa</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th width="130" style="text-align: right;">Tipo Depreciación:</th> 
            <td>
                <select name="tipo_depreciacion" class="campos">
                    <option value=""></option>
                    <option value="L" <?php if(_opc("tipo_depreciacion") == "L"){
                        echo "selected";
                    } ?>>Lineal</option>
                    <option value="A" <?php if(_opc("tipo_depreciacion") == "A"){
                        echo "selected";
                    } ?>>Acelerada</option>
                </select>
            </td>
        </tr>
        <tr>
            <th style="text-align: right;">Valorización Activo:</th> 
            <td>
                <select name="bajo_norma" class="campos">
                    <option value="IFRS" <?php if(_opc("bajo_norma") == "IFRS"){
                        echo "selected";
                    } ?>>IFRS</option> 
                </select>
            </td>
        </tr>
        </tbody>
        </table>

        <table class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <th colspan="10">Configuración Monedas</th>
        </tr>
        </thead>
        <tbody> 
        <tr>  
            <th style="text-align: right;" width="130">Moneda Principal:</th> 
            <td style="text-align: left !important;" width="70"><input type="text" name="moneda" class="campos w10" value="<? echo _opc("moneda"); ?>" style="text-align: center;"></td>  
            <th style="text-align: right;" width="1">Decimales:</th> 
            <td style="text-align: left !important;"><input type="text" name="moneda_decimal" class="campos w10" value="<? echo _opc("moneda_decimal"); ?>" style="text-align: center;"></td>
        </tr>
        <tr>  
            <th style="text-align: right;">Moneda Secundaria:</th> 
            <td style="text-align: left !important;"><input type="text" name="moneda_sec" class="campos w10" value="<? echo _opc("moneda_sec"); ?>" style="text-align: center;"></td>  
            <th style="text-align: right;">Decimales:</th> 
            <td style="text-align: left !important;"><input type="text" name="moneda_sec_decimal" class="campos w10" value="<? echo _opc("moneda_sec_decimal"); ?>" style="text-align: center;"></td>
        </tr>
        <tr>  
            <th colspan="3" style="text-align: right;">Ultimo Valor Moneda Secundaria:</th> 
            <td style="text-align: left !important;"><input type="text" name="moneda_ult_cambio" class="campos w10" value="<? echo _opc("moneda_ult_cambio"); ?>" style="text-align: center;"></td>   
        </tr>
        </tbody>
        </table>
    </td>
</tr>
</table> 
 
<table class="table table-striped table-bordered table-condensed">
<thead>
<tr>
    <th colspan="20">Campos Adicionales en Movimientos</th>
</tr>
</thead>
<tbody>
<tr> 
<?
for($i = 1;$i <= 5;$i++){
    ?> 
    <th style="text-align: right;" width="110">Campo <? echo $i; ?>:</th> 
    <td><select name="extra_mov_<? echo $i; ?>" class="campos w10">
    <option value=""></option>
    <?
    $res = sql_campos("*","   ORDER BY cam_nombre asc","","");
    if(mysqli_num_rows($res) > 0){ 
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<?php echo $row["cam_id"] ;?>" <?php 
            if(_opc("extra_mov_".$i) == $row["cam_id"]){ echo "selected"; } ?>><?php echo $row["cam_nombre"]; ?></option>
            <?
        }
    }
    ?></select></td>
    <?
}
?>
</tr>
</tbody>
</table> 

<table class="table table-striped table-bordered table-condensed">
<thead>
<tr>
    <th colspan="20">Configuración del Sistema</th>
</tr>
</thead>
<tbody>
<tr>
    <th width="110" style="text-align: right;"></th> 
    <th width="100" style="text-align: center;">Cuenta Contable Activo</th>
    <th width="100" style="text-align: center;">Cuenta Contable Depreciación</th>
    <th width="100" style="text-align: center;">Cuenta Contable Deprec. Acum.</th>
    <th width="100" style="text-align: center;">Cuenta Contable Revalorización</th>
    <th width="100" style="text-align: center;">Cuenta Contable Readecuación</th>
    <th>Ejemplo para cada tipo de producto</th>
    <th width="100" style="text-align: center;">Amortización</th>
    <th width="100" style="text-align: center;" title="Vida Util Depreciación Lineal">VU.Dep.Lineal</th>
    <th width="100" style="text-align: center;" title="Vida Util Depreciación Acelerada">VU.Dep.Acele.</th>
</tr> 
<tr> 
    <th style="text-align: right;">Insumos:</th> 
    <td><input type="text" name="cta_contable_INS" class="campos w10" value="<? echo _opc("cta_contable_INS"); ?>" style="text-align: center;"></td>
    <td style="text-align:center">No Aplica</td>
    <td style="text-align:center">No Aplica</td>
    <td style="text-align:center">No Aplica</td>
    <td style="text-align:center">No Aplica</td>
    <td style="font-style: italic;">Guantes, Mascarillas, Lentes de Seguridad, Resmas de Papel</td>
    <td style="text-align:center">No Aplica</td>
    <td style="text-align:center">No Aplica</td>
    <td style="text-align:center">No Aplica</td>
</tr>
<? 
$res_ta = mysqli_query($cnx,"select * from activos_tipos order by tac_orden asc");
while($row_ta = mysqli_fetch_array($res_ta)){
    if($row_ta["tac_activo"]){
        ?>
        <input type="hidden" name="tac[]" class="campos" value="<? echo $row_ta["tac_id"]; ?>">
        <tr> 
            <th style="text-align: right;"><? echo $row_ta["tac_nombre"]; ?>:</th> 
            <td><input type="text" name="cta_contable_<? echo $row_ta["tac_id"]; ?>" class="campos w10" value="<? 
            echo $row_ta["tac_cuenta_contable"]; ?>" style="text-align: center;"></td>
            <?php
            if($row_ta["tac_depreciable"]){
                ?>
                <td><input type="text" name="depre_<? echo $row_ta["tac_id"]; ?>" class="campos w10" value="<? 
                echo $row_ta["tac_depreciacion"]; ?>" style="text-align: center;"></td>
                <td><input type="text" name="depre_acum_<? echo $row_ta["tac_id"]; ?>" class="campos w10" value="<? 
                echo $row_ta["tac_depreciacion_acum"]; ?>" style="text-align: center;"></td>
                <?
            }else{
                ?>
                <td style="text-align:center">No Aplica</td>
                <td style="text-align:center">No Aplica</td>
                <?
            }
            if($row_ta["tac_readecuable"]){
                ?>
                <td><input type="text" name="readecuacion_<? echo $row_ta["tac_id"]; ?>" class="campos w10" value="<? 
                echo $row_ta["tac_readecuacion"]; ?>" style="text-align: center;"></td>
                <?
            }else{
                ?>
                <td style="text-align:center">No Aplica</td> 
                <?
            }
            if($row_ta["tac_revalorizable"]){
                ?>
                <td><input type="text" name="revalorizacion_<? echo $row_ta["tac_id"]; ?>" class="campos w10" value="<? 
                echo $row_ta["tac_revalorizacion"]; ?>" style="text-align: center;"></td>
                <?
            }else{
                ?>
                <td style="text-align:center">No Aplica</td> 
                <?
            }
            ?>

            <td style="font-style: italic;"><? echo $row_ta["tac_ejemplo"]; ?></td> 

            <?php
            if($row_ta["tac_amortizable"]){
                ?>
                <td><input type="text" name="amorti_<? echo $row_ta["tac_id"]; ?>" class="campos w10" value="<? 
                echo $row_ta["tac_amortizacion"]; ?>" style="text-align: center;"></td>
                <?
            }else{
                ?>
                <td style="text-align:center">No Aplica</td> 
                <?
            }
            if($row_ta["tac_depreciable"]){
                ?>
                <td><input type="text" name="dep_lineal_<? echo $row_ta["tac_id"]; ?>" class="campos w10" value="<? 
                echo $row_ta["tac_vu_dep_lineal"]; ?>" style="text-align: center;"></td>
                <td><input type="text" name="dep_acelerada_<? echo $row_ta["tac_id"]; ?>" class="campos w10" value="<? 
                echo $row_ta["tac_vu_dep_acele"]; ?>" style="text-align: center;"></td> 
                <?
            }else{
                ?>
                <td style="text-align:center">No Aplica</td>
                <td style="text-align:center">No Aplica</td>
                <?
            }
            ?>
        </tr>
        <?
    }
}
?>
<tr>
    <td colspan="7"></td>
    <td colspan="3" style="text-align: center;">(*) Valores expresados en años</td>
</tr>
</tbody>
</table>

<div style="border-top: 1px solid #ccc; padding-top: 10px;" id="div_ajax"><? 
construir_boton("grabar()","","grabar","Grabar Información",4); 
?></div> 