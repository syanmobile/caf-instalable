<?php
require("../../inc/conf_dentro.php");

$res = sql_personas("*","and per.per_id = '$_REQUEST[id]'","");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<strong>No se encontró el dato</strong>
	</div>
    <?php
	exit();
}
//----------------------------------------------------------------------------------------
$titulo_pagina = "Editar Usuario";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function save(){    
	if(document.getElementById("nombre").value == ""){
		alerta_js("Ingresar NOMBRE","Es obligaci&oacute;n ingresar el nombre de la persona");
		return;	
	} 
	carg("usu_save.php","");
}
$(document).ready(function() { 
	$("input[name=checktodos1]").change(function(){
		var valor = $("input[name=checktodos1]:checked").length;
		if(valor){
			 $('input[alt1=1]').each( function() {
				  this.checked = true;
			 });
		}else{
			 $(".asis1").removeAttr("checked");
		} 
	});  
	$("input[name=checktodos2]").change(function(){
		var valor = $("input[name=checktodos2]:checked").length;
		if(valor){
			 $('input[alt2=1]').each( function() {
				  this.checked = true;
			 });
		}else{
			 $(".asis2").removeAttr("checked");
		} 
	});  
});
</script>

<input type="hidden" name="modo" value="editar" class="campos">
<input type="hidden" name="id" value="<?php echo $_REQUEST["id"]; ?>" class="campos">

<table width="100%">
	<tr valign="top">  
		<td style="padding-left: 10px;">
			<table class="table  table-bordered"> 
		    <thead>
		    	<tr>
		    		<th colspan="2">FICHA PRINCIPAL</th>
		    	</tr>    
			</thead>
			<tbody>
				<tr>
					<th width="120" style="text-align:right;">Nombre:</th>
					<td><input type="text" class="campos w10" id="nombre" name="nombre" value="<?php
					echo $datos["per_nombre"]; ?>"></td>
				</tr> 
				<tr>
					<th style="text-align:right;">Correo:</th>
					<td><input type="text" class="campos w10" id="correo" name="correo" value="<?php
					echo $datos["per_correo"]; ?>"></td>
				</tr>  
				<tr>
					<th width="120" style="text-align:right;">Perfil:</th>
					<td>
						<select name="perfil" id="perfil" class="campos">
						<option value=""></option>
						<?php
						$res_pfl = sql_perfiles("*"," ORDER BY pfl.pfl_nombre asc");
						while($row_pfl = mysqli_fetch_array($res_pfl)){
							?>
							<option value="<?php echo $row_pfl["pfl_id"]; ?>" <?php
							if($datos["per_pfl_id"] == $row_pfl["pfl_id"]){ echo "selected"; } ?>><?php
							echo $row_pfl["pfl_nombre"]; ?></option><?php
						}
						?>
						</select>
					</td>
				</tr>
				<tr>
					<th style="text-align:right;">Usuario:</th>
					<td><input type="text" class="campos" id="usuario" name="usuario" value="<?php
					echo $datos["per_usuario"]; ?>"></td>
				</tr>
				<tr>
					<th style="text-align:right;">Clave:</th>
					<td><input type="text" class="campos" id="clave" name="clave" value=""><br />
					<b>(*) Solo modificar si se requiere modificar</b></td>
				</tr>  
				<tr>
					<th style="text-align:right;">Estado:</th>
					<td>
						<select name="estado" id="estado" class="campos">
						<option value="0" <?php if($datos["per_elim"] == "0"){ echo "selected"; } ?>>Activado</option>
						<option value="1" <?php if($datos["per_elim"] == "1"){ echo "selected"; } ?>>Desactivado</option> 
						</select>
					</td>
				</tr> 
			</tbody>
			</table> 
		</td> 
		<td style="padding-left: 10px;"> 
			<table class="table table-bordered">
			<thead>
				<tr>
					<th width="1"><input name="checktodos1" type="checkbox"></th> 
					<th>Marque Lugares donde tiene acceso:</th>
				</tr>				
			</thead> 
			<tbody>
				<?
				$res_pfl = mysqli_query($cnx,"select * from bodegas ORDER BY bod_nombre asc");
				while($row_pfl = mysqli_fetch_array($res_pfl)){
					?>
					<tr>
						<td width="1"><input type="checkbox" value="<?php echo $row_pfl["bod_id"]; ?>" name="bodega[]" alt1="1" class="campos asis1" <?
						if(mysqli_num_rows(mysqli_query($cnx,"select * from personas_bodegas where per_id = '".$datos["per_id"]."' and bod_id = '".$row_pfl["bod_id"]."' ")) > 0){
							echo "checked"; 
						}
						?>></td>
						<td><b><?php echo $row_pfl["bod_codigo"]; ?></b> - <?php echo $row_pfl["bod_nombre"]; ?></td>
					</tr>
					<?php
				}
				?>
			</tbody>
			</table> 
		</td>  
		<td style="padding-left: 10px;"> 
			<table class="table table-bordered">
			<thead>
				<tr>
					<th width="1"><input name="checktodos2" type="checkbox"></th>  
					<th>Marque Operaciones que tiene disponible:</th>
				</tr> 
			</thead>
			<tbody>
				<?
				require("usu_operaciones.php");
				foreach($operaciones_lista as $operacion){
					?>
					<tr valign="top">
						<td width="1"><input type="checkbox" value="<?php echo $operacion[0]; 
						?>" name="operacion[]" alt2="1" class="campos asis2" <?
						if(mysqli_num_rows(mysqli_query($cnx,"select * from personas_operaciones where per_id = '".$datos["per_id"]."' 
							and ope_id = '".$operacion[0]."'")) > 0){
							echo "checked"; 
						}
						?>></td>
						<td><b><?php echo $operacion[1]; ?></b><br><?php echo $operacion[2]; ?></td>
					</tr>
					<?php
				}
				?>
			</tbody>
			</table> 
		</td> 
	</tr>
</table>
<?php
construir_boton("","","grabar","Guardar",3); 
construir_boton("usu_listado.php","","eliminar","Cancelar",2);
?> 