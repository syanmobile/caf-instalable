<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Ingreso de Activos/Insumos"; 
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
$(document).ready(function(){
	detalle();
	input_auxiliar("");
	input_producto("");
});
function ubicaciones(){
	var div = document.getElementById("ubi_lugar"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/ubi_ajax.php",a+"&modo=selector",div,false,function(){
		$(".buscador").chosen({
			width:"95%",
			no_results_text:'Sin resultados con',
			allow_single_deselect:true 
		});
	});
}
function elegir(){
	var div = document.getElementById("detalle_pro"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/ing_ajax2.php",a+"&modo=elegir_pro",div);
}
function detalle(){
	var div = document.getElementById("detalle"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/ing_ajax2.php",a+"&modo=detalle",div);
}
function agregar_linea(){  
	calcular();
	if(document.getElementById("producto").value == ""){
		alerta_js("Es obligación ingresar el PRODUCTO");
		return;	
	}
	cant = document.getElementById("cantidad").value * 1;
	document.getElementById("cantidad").value = cant;
	if(cant < 1){
		alerta_js("Ingrese una CANTIDAD");
		return;
	}
	prec = document.getElementById("precio").value * 1;
	document.getElementById("precio").value = prec;
	if(prec < 1){
		alerta_js("Ingrese un PRECIO");
		return;
	}
	if(document.getElementById("tipo_depreciacion").value != ""){ 
		if(document.getElementById("meses").value == ""){
			alerta_js("Indique la cantidad de MESES");
			return;	
		} 
	} 
	
	prec = document.getElementById("residual").value;
	prec = prec.replace(".", "") * 1;
	document.getElementById("residual").value = prec;

	var div = document.getElementById("operacion"); 
	var a = $(".campos").fieldSerialize();   
	AJAXPOST("mods/home/ing_ajax2.php",a+"&modo=agregar_linea",div);
}
function quitar_linea(id){
	var div = document.getElementById("operacion"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/ing_ajax2.php",a+"&modo=quitar_linea&elim="+id,div);
}
function calcular(){
	var total = 0;
	var subtotal = 0;
	var cantidad = 0;
	var precio = 0;
	
	cant = document.getElementById("cantidad").value * 1;
	document.getElementById("cantidad").value = cant;
	
	prec = document.getElementById("precio").value; 
	document.getElementById("precio").value = prec;
	
	stot = cant * prec;
	document.getElementById("stot").value = stot;
}
function save(){ 
	if(document.getElementById("folio").value == ""){
		alerta_js("Es obligación ingresar el FOLIO");
		return;	
	} 
	if(document.getElementById("fecha").value == ""){
		alerta_js("Es obligación ingresar la FECHA del documento");
		return;	
	} 
	if(document.getElementById("concepto").value == ""){
		alerta_js("Es obligación ingresar el CONCEPTO");
		return;	
	} 
	if(document.getElementById("lineas").value == ""){
		alerta_js("Es obligación ingresar al menos una linea");
		return;	
	}   
	
	if(confirm("Grabar Documento Ingreso?")){
		carg("ing_save.php","");
	}
}
function editar_lotes(id,pro,can,series){
	AJAXPOST(url_base+modulo_base+"ing_ajax2.php","modo=lotes&series="+series+"&pro="+pro+"&can="+can+"&id="+id,document.getElementById("modGeneral_lugar"),false,function(){			
		$('#modGeneral').modal('show');
	});
}
function editar_dep(id,valor,precio){
	AJAXPOST(url_base+modulo_base+"ing_ajax2.php","modo=depre&id="+id+"&valor="+valor+"&precio="+precio,document.getElementById("modGeneral_lugar"),false,function(){			
		$('#modGeneral').modal('show');
	});
} 
function previsualizar(){
	AJAXPOST(url_base+modulo_base+"ing_ajax2.php","modo=previsualizar",document.getElementById("modGeneral_lugar"),false,function(){			
		$('#modGeneral').modal('show');

		var fecha = document.getElementById("fecha").value;
		var precio = document.getElementById("precio").value;
		var meses = document.getElementById("meses").value;
		var residual = document.getElementById("residual").value;
		AJAXPOST(url_base+modulo_base+"ing_ajax2.php","modo=calcular_depre&fecha="+fecha+"&precio="+precio+"&meses="+meses+"&residual="+residual,document.getElementById("lug_espacio"));
	});
}
function edicion_rapida2(linea,campo,valor){
	var nuevos_valores = "";
	var info = document.getElementById(campo+"_det").value;
	var valor = prompt("Ingresar valor:", valor);
	if (valor != null) {
		var valores = info.split(";");
		for(i = 1;i < valores.length;i++){
			if(i == linea){
				nuevos_valores = nuevos_valores + ";" + valor;
			}else{
				nuevos_valores = nuevos_valores + ";" + valores[i];
			}
		}
		document.getElementById(campo+"_det").value = nuevos_valores;
		detalle();
	}
} 
/*****************************************************************/
function input_auxiliar(codigo){
	AJAXPOST(url_base+modulo_base+"aux_ajax.php","modo=inicio_input&codigo="+codigo,document.getElementById("input_auxiliar"));
}
function crear_auxiliar(){
	AJAXPOST(url_base+modulo_base+"aux_ajax.php","modo=crear_form",document.getElementById("modGeneral_lugar"),false,function(){			
		$('#modGeneral').modal('show'); 
	});
}
function save_auxiliar(){  
	if(document.getElementById("aux_codigo").value == ""){
		document.getElementById("aux_codigo").focus();
		alert("Es obligación ingresar el CODIGO");
		return;	
	} 
	if(document.getElementById("aux_nombre").value == ""){
		document.getElementById("aux_nombre").focus();
		alert("Es obligación ingresar el NOMBRE");
		return;	
	}
	var a = $(".campos").fieldSerialize();  
	var div = document.getElementById("lugar_aux_save");
	AJAXPOST("mods/home/aux_ajax.php",a+"&modo=crear_rapido",div);
}
/*****************************************************************/
function input_producto(prod){
	AJAXPOST(url_base+modulo_base+"pro_ajax.php","modo=inicio_input&prod="+prod,document.getElementById("input_producto"));
}
function crear_producto(){
	AJAXPOST(url_base+modulo_base+"pro_ajax.php","modo=crear_form",document.getElementById("modGeneral_lugar"),false,function(){			
		$('#modGeneral').modal('show'); 
	});
}
function save_producto(){  
	if(document.getElementById("pro_codigo").value == ""){
		document.getElementById("pro_codigo").focus();
		alert("Es obligación ingresar el CODIGO");
		return;	
	} 
	if(document.getElementById("pro_nombre").value == ""){
		document.getElementById("pro_nombre").focus();
		alert("Es obligación ingresar el NOMBRE");
		return;	
	}
	var a = $(".campos").fieldSerialize();  
	var div = document.getElementById("lugar_pro_save");
	AJAXPOST("mods/home/pro_ajax.php",a+"&modo=crear_rapido",div);
}
/*****************************************************************/
</script>

<input type="hidden" name="modo" value="crear2" class="campos" />
<div id="operacion"></div>
<table width="100%">
<tr valign="top"> 
<td width="420"> 
    <table class="table table-bordered table-condensed"> 
    <thead>
    <tr>
    	<th colspan="10">Información General</th>
    </tr>	
    </thead> 
    <tbody>
    <tr>
        <th style="text-align:right;" width="120">Folio:</th>
        <td width="1"><input type="text" name="folio" id="folio" class="campos w10" value="" style="text-align:center; width: 100px !important;"></td> 
        <th width="1" style="text-align:right;">Fecha:</th>
        <td><input type="text" name="fecha" id="fecha" value="<? echo date("d/m/Y"); ?>" class="campos fecha"></td>
    </tr>
    <tr>
        <th style="text-align:right;">Ubicación:</th>
        <td colspan="3">
            <select name="ubicacion" id="ubicacion" class="campos buscador w10" onchange="javascript:cargar_productos();">
			<option value=""></option>
			<?php
			$res = sql_ubicaciones("*"," order by bod.bod_nombre asc, ubi.ubi_nombre asc"); 
			if(mysqli_num_rows($res) > 0){
				while($row = mysqli_fetch_array($res)){
					?>
					<option value="<? echo $row["ubi_id"]; ?>" <?
					if($_REQUEST["fil_ubicacion"] == $row["ubi_id"]){ echo "selected"; } 
					?>>U<? echo $row["ubi_codigo"]." - ".$row["ubi_nombre"]; ?></option>
					<?
				}
			}
			?></select>
        </td>
    </tr>   
    <tr>
        <th style="text-align:right;">Concepto:</th>
        <td colspan="3"><?php input_concepto('','E'); ?></td>
    </tr> 
	<tr>
        <th style="text-align:right;">Proveedor:</th>
        <td colspan="3" id="input_auxiliar"></td>        
    </tr> 
    <tr>
    	<th style="text-align:right;">Responsable:</th>
    	<td colspan="3">
		<select name="responsable" id="responsable" class="campos buscador">
		<option value=""></option>
		<? 
		$res = sql_personas("*"," and per.per_tipo = 1 and per.per_elim = 0 ORDER BY per.per_nombre asc ");  
		if(mysqli_num_rows($res) > 0){
			while($row = mysqli_fetch_array($res)){
				?>
				<option value="<? echo $row["per_id"]; ?>"><?php echo $row["per_nombre"]; ?></option>
				<?
			}
		} 
		?>
		</select>
   		</td>
    </tr>   
	<tr>
		<th style="text-align:right;">Centro Costo:</th>
		<td colspan="3"><?php input_centro_costo(''); ?></td>
	</tr>    
    <tr>
        <th style="text-align:right;">Notas:</th>
        <td colspan="3"><input type="text" name="glosa" id="glosa" class="campos w10"></td> 
    </tr> 
    <?php 
	for($i = 1;$i <= 5;$i++){
		if(_opc("extra_mov_".$i) > 0){
			$res = sql_campos("*"," and cam_id = "._opc("extra_mov_".$i));  
			$row = mysqli_fetch_array($res); 
			?> 
			<tr> 
				<th style="text-align: right;"><? echo $row["cam_nombre"]; ?>:</th>
				<td colspan="3"><? 
	            campo_dinamico($row["cam_tipo"],"extra_".$i,$row["cam_opciones"],"",$url_base);
	            ?></td> 
			</tr> 
			<?php
		}
	} 
	?>
	</tbody>   
    </table>
    <input type="hidden" name="tipo" id="tipo" value="E" class="campos" /> 
	<?php 
	construir_boton("","","grabar","Guardar Documento",3); 
	?> 
</td>
<td style="padding-left:10px;">
    <table class="table table-bordered table-condensed" style="margin-bottom: 0px !important;"> 
    <thead>
    <tr>
    	<th colspan="2">Agregar un Producto</th>
    </tr>	
    </thead> 
    <tbody>
    <tr>
        <th width="80" style="text-align: right;">Producto:</th>
        <td id="input_producto"></td>
    </tr>
	</tbody>
	</table> 

 	<div id="detalle_pro"></div>
	<div id="detalle"></div>
</td>
</table>