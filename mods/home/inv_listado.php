<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Tomas de Inventarios"; 
construir_breadcrumb($titulo_pagina);
//---------------------------------------------------------------------------------------- 
?> 
<table style="margin-bottom:5px; width:100%">
<tr> 
    <td><?php
    construir_boton("inv_nuevo.php","","crear","Nueva Toma de Inventario",2);
    construir_boton("inv_listado.php","","buscar","Listado");
    ?></td>
</tr>
</table> 
<?php
$res = sql_inventarios("*"," order by tom.tom_proceso desc "); 
if(mysqli_num_rows($res) > 0){
	?> 
	<table class="table table-bordered table-condensed"> 
	<thead>
		<th width="1">Opciones</th> 
		<th width="1">Folio</th>
		<th width="200"><? echo $terminos_panal["Bodega"]; ?></th>
		<th width="1">Tipo</th>
		<th width="1">Stock</th>
		<th>Proceso Inventario</th> 
		<th width="100">Estado</th>
	</thead>
	<tbody>
	<?php
	while($row = mysqli_fetch_array($res)){
		?>
		<tr fila="<? echo $row["tom_id"]; ?>">
            <td><?php 
			construir_boton("inv_editar.php?id=".$row["tom_proceso"],"","editar","Editar");
			construir_boton("inv_ver_inventario.php?id=".$row["tom_proceso"],"","buscar","Ver"); 
			?></td> 
            <th style="text-align: center;"><?php echo $row["tom_proceso"]; ?></th>
            <td><?php echo $_bodegas[$row["tom_bodega"]]; ?></td> 
            <td style="text-align: center;"><?php echo $row["tom_tipo"]; ?></td>
            <td style="text-align: center;"><?php echo $row["tom_stock"]; ?></td>
            <td><?php echo $row["tom_nombre"]; ?></td> 
            <td><?php 
			if($row["tom_estado"] == "TER"){
				?><img src="img/icons/tick.png" width="10"> Terminado<?
			}else{
				?><img src="img/icons/clock_frame.png" width="10"> Pendiente<?
			}
			?></td>
		</tr>
		<?
	} 
	?>
    </tbody>
    </table>
    <?
}else{
	?>
    <div class="alert alert-danger">
        <strong>Sin registros creados</strong>
    </div>
	<?
} 
?>