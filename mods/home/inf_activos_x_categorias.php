<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Productos por Clasificación";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script type="text/javascript">
function excel(){
	var a = $(".campos").fieldSerialize();
	window.open("mods/home/inf_activos_x_categorias_datos_xls.php?"+a);
} 
</script> 

<table class="table table-bordered table-condensed table-info">
<tr>  
    <td width="100">
    <b>Cod.Producto:</b><br />
    <input type="text" name="fil_codigo" id="fil_codigo" class="campos w10" value="<? echo $_REQUEST["fil_codigo"]; ?>" placeholder="%"></td> 
    <td>
    <b>Nombre Producto:</b><br />
    <input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? echo $_REQUEST["fil_descripcion"]; ?>" placeholder="%"></td>   
    <td>
    <b>Clasificación:</b><br />
    <select name="fil_categoria" id="fil_categoria" class="campos buscador w10">
    <option value=""></option>
    <?php
    $res = sql_categorias("*"," order by cat.cat_codigo asc"); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["cat_codigo"]; ?>" <?
            if($_REQUEST["fil_categoria"] == $row["cat_id"]){ echo "selected"; } 
            ?>>C<? echo $row["cat_codigo"]." - ".$row["cat_nombre_largo"]; ?></option>
            <?
        }
    }
    ?></select></td> 

    <td width="90"><?
    construir_boton("inf_activos_x_categorias.php","","buscar","Filtrar");
    construir_boton("carg3('inf_activos_x_ubicacion.php','')","1","limpiar","Limpiar",4);
    construir_boton("excel();","1","importar","Descargar",4); 
    ?></td>  
</tr>
</table>

<?php
require("inf_activos_x_categorias_datos.php");
?>