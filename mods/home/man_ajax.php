<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabacion";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
 
switch($_REQUEST["modo"]){  
	case "crear":  
		$sql = "INSERT INTO mantenciones_activos (
			man_acf_id,
			man_autor,
			man_inicio, 
			man_termino,
			man_limite,
			man_proxima_mant,
			man_responsable,
			man_titulo,
			man_descripcion,
			man_notas,
			man_imagen,
			man_extra_1,
			man_extra_2,
			man_extra_3,
			man_extra_4,
			man_extra_5,
			man_extra_6,
			man_extra_7,
			man_extra_8,
			man_extra_9,
			man_extra_10,
			man_extra_11,
			man_extra_12,
			man_extra_13,
			man_extra_14,
			man_extra_15,
			man_extra_16,
			man_extra_17,
			man_extra_18,
			man_extra_19,
			man_extra_20, 
			man_tipo,
			man_prioridad,
			man_hh,
			man_costo,
			man_estado
		) VALUES ( 
			'$_POST[activo]',
			'".$_SESSION["per_conectado"]["per_id"]."',
			'"._fec($_POST["inicio1"],9)." ".$_POST["inicio2"].":00',
			'"._fec($_POST["termino1"],9)." ".$_POST["termino2"].":00',
			'"._fec($_POST["limite"],9)."',
			'"._fec($_POST["proxima"],9)."',
			'$_POST[responsable]', 
			'$_POST[titulo]',
			'$_POST[descripcion]',
			'$_POST[notas]',
			'$_POST[imagen]',
			'$_POST[extra_1]',
			'$_POST[extra_2]',
			'$_POST[extra_3]',
			'$_POST[extra_4]',
			'$_POST[extra_5]',
			'$_POST[extra_6]',
			'$_POST[extra_7]',
			'$_POST[extra_8]',
			'$_POST[extra_9]',
			'$_POST[extra_10]',
			'$_POST[extra_11]',
			'$_POST[extra_12]',
			'$_POST[extra_13]',
			'$_POST[extra_14]',
			'$_POST[extra_15]',
			'$_POST[extra_16]',
			'$_POST[extra_17]',
			'$_POST[extra_18]',
			'$_POST[extra_19]',
			'$_POST[extra_20]',  
			'$_POST[tipo]',
			'$_POST[prioridad]',
			'$_POST[hh]',
			'$_POST[costo]',
			'$_POST[estado]'
		)";  
		$res = mysqli_query($cnx,$sql);
		$mant = mysqli_insert_id($cnx);

		$sql = "select *, sum(man_costo) as costo from mantenciones_activos 
		where man_acf_id = '$_REQUEST[activo]' 
		group by man_acf_id ";
		$res = mysqli_query($cnx,$sql);
		$row = mysqli_fetch_array($res);

		$sql = "update activos_fijos set 
		acf_valor_adicional = '$row[costo]',
		acf_fecha_mantencion = '$row[man_proxima_mant]',  
		acf_mantencion_anterior = '$row[man_termino]'   
		where acf_id = '$_REQUEST[activo]' 
		order by man_termino desc";
		$res = mysqli_query($cnx,$sql);
		?> 
        <table width="100%">
        <tr valign="top">
            <td width="300"> 
				<div class="alert alert-success">
					<b>Mantenimiento Folio #<? echo $mant; ?></b><br>
					creado con éxito
				</div>
                <?php 
				construir_boton("man_listado.php","","izquierda","Ir a Mantenciones",2); 
				construir_boton("man_nuevo.php","","crear","Crear otra mantención",2);
				construir_boton("acf_info.php","&acf=".$_REQUEST["activo"],"buscar","Ir a Ficha Activo",2);
				?>
            </td>
            <td style="padding-left:15px;">
            	<iframe src="<?php echo "mods/home/man_pdf.php?id=".$mant; ?>" width="100%" height="400"></iframe>
            </td>
        </tr>
        </table>
		<?
		break;

	case "editar": 
		$sql = "update mantenciones_activos set 
			man_acf_id = '$_POST[activo]', 
			man_inicio = '"._fec($_POST["inicio1"],9)." ".$_POST["inicio2"].":00', 
			man_termino = '"._fec($_POST["termino1"],9)." ".$_POST["termino2"].":00',
			man_limite = '"._fec($_POST["limite"],9)."',
			man_proxima_mant = '"._fec($_POST["proxima"],9)."',
			man_responsable = '$_POST[responsable]', 
			man_titulo = '$_POST[titulo]',
			man_descripcion = '$_POST[descripcion]',
			man_notas = '$_POST[notas]',
			man_imagen = '$_POST[imagen]',
			man_extra_1 = '$_POST[extra_1]',
			man_extra_2 = '$_POST[extra_2]',
			man_extra_3 = '$_POST[extra_3]',
			man_extra_4 = '$_POST[extra_4]',
			man_extra_5 = '$_POST[extra_5]',
			man_extra_6 = '$_POST[extra_6]',
			man_extra_7 = '$_POST[extra_7]',
			man_extra_8 = '$_POST[extra_8]',
			man_extra_9 = '$_POST[extra_9]',
			man_extra_10 = '$_POST[extra_10]',
			man_extra_11 = '$_POST[extra_11]',
			man_extra_12 = '$_POST[extra_12]',
			man_extra_13 = '$_POST[extra_13]',
			man_extra_14 = '$_POST[extra_14]',
			man_extra_15 = '$_POST[extra_15]',
			man_extra_16 = '$_POST[extra_16]',
			man_extra_17 = '$_POST[extra_17]',
			man_extra_18 = '$_POST[extra_18]',
			man_extra_19 = '$_POST[extra_19]',
			man_extra_20 = '$_POST[extra_20]',   
			man_tipo = '$_POST[tipo]',
			man_prioridad = '$_POST[prioridad]',
			man_hh = '$_POST[hh]',
			man_costo = '$_POST[costo]',
			man_estado = '$_POST[estado]' 
		where man_id = '$_POST[man]' ";
		$res = mysqli_query($cnx,$sql); 

		$sql = "select *, sum(man_costo) as costo from mantenciones_activos 
		where man_acf_id = '$_REQUEST[activo]' 
		group by man_acf_id 
		order by man_termino desc";
		$res = mysqli_query($cnx,$sql);
		$row = mysqli_fetch_array($res);

		$sql = "update activos_fijos set 
		acf_valor_adicional = '$row[costo]',
		acf_fecha_mantencion = '$row[man_proxima_mant]',  
		acf_mantencion_anterior = '$row[man_termino]'  
		where acf_id = '$_REQUEST[activo]' ";
		$res = mysqli_query($cnx,$sql);
		?> 
        <table width="100%">
        <tr valign="top">
            <td width="300"> 
				<div class="alert alert-success">
					<b>Mantenimiento Folio #<? echo $_POST["man"]; ?></b><br>
					editado con éxito
				</div>
                <?php 
				construir_boton("man_listado.php","","izquierda","Ir a Mantenciones",2); 
				construir_boton("man_nuevo.php","","crear","Crear otra mantención",2);
				construir_boton("acf_info.php","&acf=".$_REQUEST["activo"],"buscar","Ir a Ficha Activo",2);
				?>
            </td>
            <td style="padding-left:15px;">
            	<iframe src="<?php echo "mods/home/man_pdf.php?id=".$_POST["man"]; ?>" width="100%" height="400"></iframe>
            </td>
        </tr>
        </table>
		<? 
		break;
}
?> 