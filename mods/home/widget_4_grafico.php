<?php
require("../../inc/conf_dentro.php");
require("../../_css.php");

$sql = "select *,sum(total) as stock from insumos group by pro_id order by pro_nombre asc ";
$res = mysqli_query($cnx,$sql);  
if(mysqli_num_rows($res) == 0){ 
	?>
	<div class="alert alert-danger" style="margin:20px;">
		<strong>Sin stock de insumos</strong>
	</div>
	<?php 
	exit();
}else{
	while($row = mysqli_fetch_array($res)){
		if($row["pro_stock_minimo"] >= $row["stock"]){
			$criticos++; 
		}else{
			$con_stock++;
		}
		$productos++;
	}  
}
$criticos = round(_porc($criticos,$productos),1);
$con_stock = round(_porc($con_stock,$productos),1);
?>
<!DOCTYPE HTML>
<html>
<head>
<script>
window.onload = function () {

	CanvasJS.addColorSet("customColorSet1",["#f02522","#3dba44"]);

	var options = {
		animationEnabled: true, 
		colorSet: "customColorSet1",
		data: [{
			type: "doughnut",
			innerRadius: "40%", 
			legendText: "{label}",
			indexLabel: false,
			dataPoints: [
				{ label: "Stock Crítico", y: <? echo $criticos; ?> },
				{ label: "Con Stock", y: <? echo $con_stock; ?> }
			]
		}]
	};
	$("#chartContainer").CanvasJSChart(options);

}
</script>
</head>
<body>
<div id="chartContainer" style="height: 290px; width: 100%;"></div>
<script src="../../js/jquery-1.11.1.min.js"></script>
<script src="../../js/jquery.canvasjs.min.js"></script>
</body>
</html>