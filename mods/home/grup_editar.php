<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Editar Grupo";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

$res = sql_grupos("*"," and gru.gru_id = '$_REQUEST[id]'","");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<strong>No se encontró el dato</strong>
	</div>
    <?php
	construir_boton("grup_listado.php","","buscar","Listado Grupos",2);
	exit();
}
?>
<script language="javascript">
function save(){
	if(document.getElementById("nombre").value == ""){
		alerta_js("Es obligación ingresar el nombre");
		return;	
	}
	carg("grup_save.php","");
}
</script> 

<form class="form-horizontal" role="form" method="post">
	<input type="hidden" name="modo" value="editar" class="campos"> 
	<input type="hidden" name="id" value="<? echo $_REQUEST["id"]; ?>" class="campos"> 
    <div class="form-group">
        <label for="orden" class="col-sm-2 control-label">Código <span class="oblig">(*)</span></label>
        <div class="col-sm-2">
            <input type="text" class="form-control campos" disabled value="<? echo $datos["gru_codigo"]; ?>">
        	<input type="hidden" name="codigo" class="campos" value="<?php echo $datos["gru_codigo"]; ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="nombre" class="col-sm-2 control-label">Nombre Grupo <span class="oblig">(*)</span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control campos" id="nombre" name="nombre" value="<? echo $datos["gru_nombre"]; ?>">
        </div>
    </div>  
    
    <div class="form-group">
        <div class="col-sm-2"/>
        <div class="col-sm-10">
        	<?php
			construir_boton("","","grabar","Guardar",3);
			construir_boton("grup_listado.php","","eliminar","Cancelar",2);
			?>
        </div>
    </div>
</form> 