<?php 
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Mantención de Activos Fijos";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "man_listado.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function excel(){
	window.open("mods/home/acf_mantenimiento.php?excel=S");
}
</script>
<table width="100%" class="tabla_opciones">
<tbody> 
	<tr>
		<td><?php 
		construir_boton("man_nuevo.php","","crear","Crear Mantención",2); 
		//construir_boton("excel()","","derecha","Exportar Mantenimientos",4); 
		?>
			
<table class="table table-bordered table-condensed table-info" style="margin-bottom:0px !important;">  
<tbody> 
<tr>
    <td width="90">
	    <b>Folio:</b><br />
	    <input type="text" name="fil_folio" id="fil_folio" class="campos w10" value="<? echo $_REQUEST["fil_folio"]; ?>">
	</td> 
    <td width="100">
	    <b>Código:</b><br />
	    <input type="text" name="fil_codigo" id="fil_codigo" class="campos w10" value="<? echo $_REQUEST["fil_codigo"]; ?>">
	</td> 
    <td>
	    <b>Descripción:</b><br />
	    <input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? echo $_REQUEST["fil_descripcion"]; ?>">
	</td>  
    <td width="1">
	    <b>Tipo:</b><br>
	    <select name="fil_tipo" id="fil_tipo" class="campos">
			<option value=""></option>
			<option value="P" <? if($_REQUEST["fil_tipo"] == "P"){ echo "selected"; } ?>>Preventiva</option>
			<option value="C" <? if($_REQUEST["fil_tipo"] == "C"){ echo "selected"; } ?>>Correctiva</option>
		</select>
	</td>
	<td width="1">
		<b>Prioridad:</b><br>
		<select name="fil_prioridad" id="fil_prioridad" class="campos">
			<option value=""></option>
			<option value="1" <? if($_REQUEST["fil_prioridad"] == "1"){ echo "selected"; } ?>>Urgente–Importante</option>
			<option value="2" <? if($_REQUEST["fil_prioridad"] == "2"){ echo "selected"; } ?>>Urgente–No importante</option>
			<option value="3" <? if($_REQUEST["fil_prioridad"] == "3"){ echo "selected"; } ?>>No urgente–Importante</option>
			<option value="4" <? if($_REQUEST["fil_prioridad"] == "4"){ echo "selected"; } ?>>No urgente–No importante</option> 
		</select>
    </td> 
	<td width="1">
		<b>Estado:</b><br>
		<select name="fil_estado" id="fil_estado" class="campos">
			<option value=""></option>
			<option value="PND" <? if($_REQUEST["fil_estado"] == "PND"){ echo "selected"; } ?>>Pendiente</option>
			<option value="TER" <? if($_REQUEST["fil_estado"] == "TER"){ echo "selected"; } ?>>Terminado</option> 
		</select>
    </td>   
    <td width="1"><?
	construir_boton("man_listado.php","1","buscar","Filtrar",2); 
	construir_boton("excel()","","importar","Descargar",4); 
	?></td> 
</tr>
</tbody>
</table>

		</td>
		<td width="280" style="padding-left: 15px;">
		<div class="alert alert-warning" style="padding: 5px 10px; margin:0px;">
			<b>Urg-Imp</b>: Urgente – Importante<br>
			<b>Urg-NoImp</b>: Urgente – No Importante<br>
			<b>NoUrg-Imp</b>: No Urgente – Importante<br> 
			<b>NoUrg-NoImp</b>: No Urgente – No Importante
		</div>
		</td>
	</tr>
</tbody>
</table> 
<?php 
/****************************************************************************************/
if($_REQUEST["fil_folio"] <> ""){
	$filtros .= " and mnt.man_id = '".$_REQUEST["fil_folio"]."' ";
}
if($_REQUEST["fil_codigo"] <> ""){
	$filtros .= " and pro.pro_codigo like '%".$_REQUEST["fil_codigo"]."%' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and pro.pro_nombre like '%".$_REQUEST["fil_descripcion"]."%' ";
} 
if($_REQUEST["fil_tipo"] <> ""){
	$filtros .= " and mnt.man_tipo = '".$_REQUEST["fil_tipo"]."' ";
}
if($_REQUEST["fil_prioridad"] <> ""){
	$filtros .= " and mnt.man_prioridad = '".$_REQUEST["fil_prioridad"]."' ";
}  
if($_REQUEST["fil_estado"] <> ""){
	$filtros .= " and mnt.man_estado = '".$_REQUEST["fil_estado"]."' ";
} 
?>
<input type="hidden" name="pagina" id="pagina" value="man_listado.php" class="campos">
<?
$sql_paginador = sql_acf_mantenimientos("*"," $filtros ORDER BY mnt.man_inicio desc","s"); 
$cant_paginador = 20;
require("../../_paginador.php");
$res3 = mysqli_query($cnx,$sql_final);  
if(mysqli_num_rows($res3) > 0){ 
	?>
	<table class="table table-bordered table-condensed">
	<thead>
	<tr>
		<th <? if($_REQUEST["excel"] == ""){ ?>width="90"<? } ?>></th>
		<th <? if($_REQUEST["excel"] == ""){ ?>width="1"<? } ?>>Folio</th>
		<th <? if($_REQUEST["excel"] == ""){ ?>width="1"<? } ?>>Codigo</th> 
		<th>Producto</th>
		<th <? if($_REQUEST["excel"] == ""){ ?>width="1"<? } ?>>Tipo</th>
		<th <? if($_REQUEST["excel"] == ""){ ?>width="95"<? } ?>>Prioridad</th>
		<th>Titulo</th> 
		<th <? if($_REQUEST["excel"] == ""){ ?>width="90"<? } ?>>Inicio</th>
		<th <? if($_REQUEST["excel"] == ""){ ?>width="90"<? } ?>>Limite</th>
		<th <? if($_REQUEST["excel"] == ""){ ?>width="90"<? } ?>>Termino</th>
		<th>Responsable</th>
		<th <? if($_REQUEST["excel"] == ""){ ?>width="90"<? } ?>>Estado</th>
		<th <? if($_REQUEST["excel"] == ""){ ?>width="1"<? } ?>>Costo</th>
	</tr>
	</thead>  
	<tbody>
	<?
	while($row3 = mysqli_fetch_array($res3)){
		?>
		<tr>
			<?
			if($_REQUEST["excel"] == ""){
				?><td><? 
				construir_boton("man_editar.php","&man=".$row3["man_id"],"editar","Editar"); 
				construir_boton("acf_info.php","&acf=".$row3["man_acf_id"],"buscar","Ver Activo"); 
				construir_boton("visor('man_pdf.php?id=".$row3["man_id"]."')","1","imprimir","Imprimir",4);
				?></td><?
			}
			?>
			<th style="text-align: center"><? echo $row3["man_id"]; ?></th>
			<th><? echo $row3["acf_codigo"]; ?></th>
			<td><? echo $row3["pro_nombre"]; ?></td>
			<td><? 
			if($row3["man_tipo"] == "P"){ 
				echo "Preventiva";
			}
			if($row3["man_tipo"] == "C"){
				echo "Correctiva";
			}
			?></td> 
			<td><?
			switch($row3["man_prioridad"]){
				case "1": echo "Urg–Imp"; break;
				case "2": echo "Urg–NoImp"; break;
				case "3": echo "NoUrg–Imp"; break;
				case "4": echo "NoUrg–NoImp"; break;
			} 
			?></td> 
			<td><? echo $row3["man_titulo"]; ?></td> 
			<td><? echo _fec($row3["man_inicio"],5); ?></td>
			<td><? echo _fec($row3["man_limite"],5); ?></td>
			<td><? echo _fec($row3["man_termino"],5); ?></td>
			<td><? echo $_personas[$row3["man_responsable"]]; ?></td>
			<td><? 
			switch($row3["man_estado"]){
				case "PND": ?><span class="label label-info">Pendiente</span><? break;
				case "TER": ?><span class="label label-success">Terminado</span><? break; 
			} 
			?></td>
			<td style="text-align: right;"><? echo _num($row3["man_costo"]); ?></td>
		</tr>
		<? 
	}
	?> 
	</tbody>
	</table>
	<?
	echo $paginador_dibujo;
}else{
	?>
	<div class="alert alert-info">
		Sin mantenimientos registrados
	</div>
	<?
} 
?>