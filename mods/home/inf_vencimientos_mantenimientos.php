<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Vencimientos de Mantenimientos";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?> 
<table class="table table-bordered table-condensed table-info">
<tr>  
    <td width="100">
    <b>Código:</b><br />
    <input type="text" name="fil_codigo" id="fil_codigo" class="campos w10" value="<? echo $_REQUEST["fil_codigo"]; ?>"></td> 
    <td>
    <b>Descripción:</b><br />
    <input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? echo $_REQUEST["fil_descripcion"]; ?>"></td>  
    <td>
    <b>Clasificación:</b><br />
    <select name="fil_categoria" id="fil_categoria" class="campos buscador w10">
    <option value=""></option>
    <?php
    $res = sql_categorias("*"," order by cat.cat_codigo asc"); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["cat_codigo"]; ?>" <?
            if($_REQUEST["fil_categoria"] == $row["cat_id"]){ echo "selected"; } 
            ?>>C<? echo $row["cat_codigo"]." - ".$row["cat_nombre_largo"]; ?></option>
            <?
        }
    }
    ?></select></td>
    <td>
    <b>Ubicación:</b><br />
    <select name="fil_ubicacion" id="fil_ubicacion" class="campos buscador w10">
    <option value=""></option>
    <?php
    $res = sql_ubicaciones("*"," order by bod.bod_nombre asc, ubi.ubi_nombre asc"); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["ubi_id"]; ?>" <?
            if($_REQUEST["fil_ubicacion"] == $row["ubi_id"]){ echo "selected"; } 
            ?>>U<? echo $row["ubi_codigo"]." - ".$row["ubi_nombre"]; ?></option>
            <?
        }
    }
    ?></select></td>

    <td width="1"><?
    construir_boton("alerta_js('Excel no disponible (Demo)');","1","importar","Descargar",4); 
    ?></td> 
    <td width="1"><?php
    construir_boton("inf_vencimientos_mantenimientos.php","","buscar","Filtrar");
    ?></td> 
</tr>
</table>

<?php 
$v_codigo = $_REQUEST["fil_codigo"];
$v_descripcion =$_REQUEST["fil_descripcion"];
$v_grupo = $_REQUEST["fil_grupo"];
$v_subgrupo = $_REQUEST["fil_subgrupo"];
$v_categoria = $_REQUEST["fil_categoria"];
$v_ubicacion = $_REQUEST["fil_ubicacion"];
$v_filtro = "";


if($v_codigo!=""){
$v_filtro = " AND pro_codigo LIKE '%$v_codigo%' ";
}
if($v_descripcion !=""){
$v_filtro = $v_filtro." AND pro_nombre LIKE '%$v_descripcion%' ";
}

if($v_ubicacion !=""){
$v_filtro = $v_filtro." AND acf_ubicacion = '$v_ubicacion' ";
}

if($v_grupo !=""){
$v_filtro = $v_filtro." AND pro_grupo='$v_grupo' ";
}

if($v_subgrupo !=""){
$v_filtro = $v_filtro." AND pro_subgrupo='$v_subgrupo' ";
}

if($v_categoria !=""){
$v_filtro = $v_filtro." AND pro_categoria='$v_categoria' ";
}  

$res = sql_activos_fijos("*"," and pro_mantenimiento = 1 $v_filtro order by acf_fecha_mantencion desc, pro_nombre asc "); 
if(mysqli_num_rows($res) > 0){ 
    ?>
    <table width="100%">
    <tr valign="top">
    <td>
        <table class="table table-striped table-bordered tabledrag table-condensed"> 
        <thead>
        <tr>
            <th <? if($_REQUEST["excel"] == ""){ ?>width="1"<? } ?>></th>
            <th <? if($_REQUEST["excel"] == ""){ ?>width="1"<? } ?>>Codigo</th>
            <th <? if($_REQUEST["excel"] == ""){ ?>width="1"<? } ?>>Serial</th>
            <th>Producto</th>
            <th <? if($_REQUEST["excel"] == ""){ ?>width="100"<? } ?>>Prox.Mantención</th>
            <th <? if($_REQUEST["excel"] == ""){ ?>width="60"<? } ?>>Estado</th>
            <th <? if($_REQUEST["excel"] == ""){ ?>width="100"<? } ?>>Situación</th>
        </tr>
        </thead>
        <tbody> 
        <?
        while($row = mysqli_fetch_array($res)){ 
            $mantenciones++;
            ?>
            <tr> 
                <td><?
                construir_boton("acf_info.php","&acf=".$row["acf_id"],"buscar","Ver Activo Fijo"); 
                ?></td>
                <th><? echo $row["acf_producto"]; ?></th>
                <th><? echo $row["acf_serie"]; ?></th>
                <td><? echo $row["pro_nombre"]; ?></td> 
                <td style="text-align: center;"><?php echo _fec($row["acf_fecha_mantencion"],5); ?></td>
                <?
                if($row["acf_fecha_mantencion"] <> "0000-00-00"){                    
                    $dias = dias_transcurridos($row["acf_fecha_mantencion"],date("Y-m-d")); 
                    if($dias < 0){
                        ?>
                        <td><span class="label label-danger">CADUCO</span></td>
                        <td><? echo ($dias * -1); ?> de atraso</td>
                        <?
                        $mantencion_vencida++;
                    }else{
                        if($dias <= 30){
                            ?>
                            <td><span class="label label-warning">POR VENCER</span></td>
                            <td><? echo $dias; ?> de vigencia</td>
                            <?
                            $mantencion_x_vencer++;
                        }else{
                            ?>
                            <td><span class="label label-success">AL DÍA</span></td>
                            <td><? echo $dias; ?> de vigencia</td>
                            <?
                            $mantencion_aldia++;
                        }
                    } 
                }else{
                    ?>
                    <td><span class="label label-info">SIN DEFINIR</span></td>
                    <td>No aplica</td>
                    <?
                    $mantencion_sin_definir++;
                }
                ?>
            </tr>
            <?
        }
        ?>   
        </tbody>
        </table>  
    </td>
    <td style="padding-left:20px;" width="300">  
        <div class="alert alert-warning" style="color: #666666 !important;">
            <table width="100%">
                <tr valign="top"> 
                    <td style="font-size: 13px;">
                        AL DIA: <b><?php echo _num2($mantencion_aldia); 
                        echo "/";
                        echo _num2($mantenciones); 

                        $porcen = round(_porc($mantencion_aldia,$mantenciones));
                        ?></b><br> 
                        <div class="progress">
                          <div class="progress-bar bg-success" role="progressbar" aria-valuenow="<? echo $porcen; 
                          ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php
                          echo $porcen; ?>%;">
                            <? echo $porcen; ?>%
                          </div>
                        </div> 

                        POR VENCER: <b><?php echo _num2($mantencion_x_vencer); 
                        echo "/";
                        echo _num2($mantenciones); 

                        $porcen = round(_porc($mantencion_x_vencer,$mantenciones));
                        ?></b><br> 
                        <div class="progress">
                          <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="<? echo $porcen; 
                          ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php
                          echo $porcen; ?>%;">
                            <? echo $porcen; ?>%
                          </div>
                        </div> 

                        VENCIDAS: <b><?php echo _num2($mantencion_vencida); 
                        echo "/";
                        echo _num2($mantenciones); 

                        $porcen = round(_porc($mantencion_vencida,$mantenciones));
                        ?></b><br> 
                        <div class="progress">
                          <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="<? echo $porcen; 
                          ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php
                          echo $porcen; ?>%;">
                            <? echo $porcen; ?>%
                          </div>
                        </div> 

                        SIN DEFINIR: <b><?php echo _num2($mantencion_sin_definir); 
                        echo "/";
                        echo _num2($mantenciones); 

                        $porcen = round(_porc($mantencion_sin_definir,$mantenciones));
                        ?></b><br> 
                        <div class="progress">
                          <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="<? echo $porcen; 
                          ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php
                          echo $porcen; ?>%;">
                            <? echo $porcen; ?>%
                          </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div> 
    </td>
    </tr>
    </table>
    <?
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin activos fijos con fecha de próx.mantención ingresadas</strong>
    </div>
    <?php 
} 
?>