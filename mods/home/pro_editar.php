<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$res = sql_productos("*","and pro.pro_id = '$_REQUEST[id]'","");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		No se encontró el Producto con id <strong>"<? echo $_REQUEST["id"]; ?>"</strong>
	</div>
    <?php
	exit();
}
//----------------------------------------------------------------------------------------
$titulo_pagina = "Editar Producto: ".$datos["pro_codigo"]." - ".$datos["pro_nombre"]." (".$_REQUEST["id"].")"; 
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

?>
<script language="javascript">
$(document).ready(function() {
	$('#archivo').on('change',function(){ 
		$('#multiple_upload_form').ajaxForm({
			target:'#procesando_upload',
			data: { modo: "upload" },
			beforeSubmit:function(e){
				$('.uploading').show();
			},
			success:function(e){
				$('.uploading').hide();
				$("#imagen_ruta").attr("src","upload/<? echo $_SESSION["key_id"]; ?>/"+document.getElementById("imagen").value); 
			},
			error:function(e){
			}
		}).submit(); 
	});
	campos_adicionales_set(); 
});
function save(){
	if(document.getElementById("nombre").value == ""){
		alerta_js("Es obligación ingresar el NOMBRE");
		return;	
	} 
	carg("pro_save.php","");
}  
function campos_adicionales_set(){   
	var a = $(".campos").fieldSerialize();
	var div = document.getElementById("campos_adicionales_lugar");
	AJAXPOST("mods/home/cam_ajax.php",a+"&modo=campos_adicionales",div);
} 
</script>

<input type="hidden" name="modo" value="editar" class="campos" />
<input type="hidden" name="id" value="<? echo $_REQUEST["id"]; ?>" class="campos" />
<table width="100%">
<tr valign="top"> 
<td width="250">  
    <table class="table table-bordered table-condensed">
    <thead>
    	<tr>
    		<th colspan="2">IMAGEN</th>
    	</tr>    
	</thead>  
    <tbody>
        <tr>
            <td>
            <?
			$imagen = ($datos["pro_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$datos["pro_imagen"];
			?>
			<img src="<?php echo $imagen; ?>" id="imagen_ruta" class="img-thumbnail" style="width:250px !important;">
			<form method="post" name="multiple_upload_form" id="multiple_upload_form" enctype="multipart/form-data" action="<?php echo $url_web; ?>mods/home/pro_ajax.php"> 
                <input type="file" name="archivo" id="archivo" accept='image/*'>
                
                <div class="uploading none" style="display:none;"><img src="img/uploading.gif"/></div>
                <div id="eliminando"></div> 
                <div id="procesando_upload">
                    <input type="hidden" name="imagen" value="<?php echo $datos["pro_imagen"]; ?>" class="campos" />
                </div>
            </form> 
            </td>
		</tr>
	</tbody>
    </table>

    <div class="alert alert-info">
    	<b>Ultima modificación del producto:</b><br>
    	<?php echo $datos["pro_modificacion"]; ?>
    </div>
</td>
<td style="padding-left:10px;" width="600">
    <table class="table table-bordered table-condensed">
    <thead>
    	<tr>
    		<th colspan="2">FICHA PRINCIPAL</th>
    	</tr>    
	</thead>
	<tbody>
		<tr>
			<th width="130" style="text-align:right;">Código:</th>
			<td>
				<input type="hidden" name="codigo" class="campos" value="<?php echo $datos["pro_codigo"]; ?>">
				<input type="text" value="<?php echo $datos["pro_codigo"]; ?>" class="campos w10" style="text-align:center; width:130px !important;" readonly>
			</td>
		</tr> 
		<tr>
			<th style="text-align:right;">Nombre:</th>
			<td><input type="text" name="nombre" id="nombre" class="campos w10" value="<?php echo $datos["pro_nombre"]; ?>"></td> 
		</tr>    
		<tr>
			<th style="text-align:right;">Cod.Barra:</th>
			<td><input type="text" name="codigo_barra" id="codigo_barra" class="campos" value="<?php echo $datos["pro_codigo_barra"]; ?>"></td> 
		</tr> 
		<tr>
			<th style="text-align:right;">Tipo Producto:</th> 
			<td><?php input_tipo_activo($datos["pro_tipo"],'S'); ?></td>
		</tr>    
		<tr>
			<th style="text-align:right;">Clasificación:</th>
			<td><?php input_clasificacion($datos["pro_categoria"]); ?></td>
		</tr>  
		<tr>
			<th style="text-align:right;">Unidad:</th>
			<td><?php input_unidad($datos["pro_unidad"]); ?></td>
		</tr>
		<tr>
			<th style="text-align:right;">Mantenimiento:</th>
			<td><select name="mantenimiento" class="campos">
			<option value="">- Sin mantenimiento -</option>
			<option value="1" <? if($datos["pro_mantenimiento"] == "1"){ echo "selected"; } 
			?>>Activo requiere mantenimiento</option>
			</select>
			</td>
		</tr>
		<tr>
			<th style="text-align:right;">Garantía:</th>
			<td><select name="garantia" class="campos">
			<option value="">- Sin garantía -</option>
			<option value="1" <? if($datos["pro_garantia"] == "1"){ echo "selected"; } 
			?>>Activo maneja fecha garantía</option>
				</select>
			</td>
		</tr>
		<tr>
			<th style="text-align:right;">Prestación:</th>
			<td><select name="prestacion" class="campos">
			<option value="0" <? if($datos["pro_prestable"] == "0"){ echo "selected"; } 
			?>>NO, los activos no son prestables</option>
			<option value="1" <? if($datos["pro_prestable"] == "1"){ echo "selected"; } 
			?>>SI, los activos son prestables</option>
			</select>
			</td>
		</tr> 
		<tr>
			<th style="text-align:right;">Asignación:</th>
			<td><select name="asignacion" class="campos">
			<option value="0" <? if($datos["pro_asignable"] == "0"){ echo "selected"; } 
			?>>NO, los activos no son asignables</option>
			<option value="1" <? if($datos["pro_asignable"] == "1"){ echo "selected"; } 
			?>>SI, los activos son asignables</option>
			</select>
			</td>
		</tr>  
    </tbody>
	</table>    
    <br> 
	<?php 
	construir_boton("","","grabar","Guardar Producto",3);
	construir_boton("pro_ver.php","&id=".$datos["pro_id"],"eliminar","Cancelar",2);
	?> 
</td>
<td style="padding-left:10px;">
	<table class="table table-bordered table-condensed"> 
		<thead>
			<tr>
				<th colspan="2">CAMPOS ADICIONALES</th>
			</tr>
		</thead>    
		<tbody> 
			<?
			for($iex = 1;$iex <= 10;$iex++){				 
	            $res = sql_campos("*"," and cam_producto = $iex order by cam_nombre asc");  
				if(mysqli_num_rows($res) > 0){
					while($row = mysqli_fetch_array($res)){
						$infor = $datos["pro_extra_".$iex];
						?> 
						<tr> 
							<th width="130" style="text-align: right;"><? echo $row["cam_nombre"]; ?>:</th>
							<td><? 
				            campo_dinamico($row["cam_tipo"],"extra_".$iex,$row["cam_opciones"],$infor,$url_base);
				            ?></td> 
						</tr>
						<? 
					}
				}
			} 
			?> 


		</tbody>
	</table>

	<table class="table table-bordered table-condensed"> 
	<tbody> 
		<tr>
			<td>
			<b>Descripción del Producto:</b><br>
			<textarea name="descripcion" class="campos" rows="8" style="width: 90%;"><?php 
			echo $datos["pro_descripcion"]; ?></textarea>
			</td>
		</tr> 
	</tbody>
	</table>  
</td>
</table>  