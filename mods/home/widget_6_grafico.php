<?php
require("../../inc/conf_dentro.php");
require("../../_css.php");

$no_disponibles = $_REQUEST["ndis"];
$disponibles = $_REQUEST["disp"];
$prestadas += $_REQUEST["pres"];
$mantenimiento+= $_REQUEST["mant"];
$baja+= $_REQUEST["baja"];
$r_mantenimiento+= $_REQUEST["rman"];
$asignadas+= $_REQUEST["asig"];
$productos+= $_REQUEST["prod"];
?>
<script>
window.onload = function () {

	CanvasJS.addColorSet("customColorSet1",["#f02522","#3dba44","#ffff4f","#34aeeb","#eb9334","#6234eb","#eb34de"]);

	var options = {
		animationEnabled: true, 
		colorSet: "customColorSet1",
		data: [{
			type: "doughnut",
			innerRadius: "10%", 
			legendText: "{label}",
			indexLabel: false,
			dataPoints: [
				<?
				if($prestadas > 0){
					$otro++;
					?>{ label: "Prestadas", y: <? echo $prestadas; ?> }<?
				}
				if($disponibles > 0){
					if($otro > 0){ echo ","; }
					$otro++;
					?>{ label: "Disponibles", y: <? echo $disponibles; ?> }<?
				}
				if($no_disponibles > 0){
					if($otro > 0){ echo ","; }
					$otro++;
					?>{ label: "No disponibles", y: <? echo $no_disponibles; ?> }<?
				}
				if($mantenimiento > 0){
					if($otro > 0){ echo ","; }
					$otro++;
					?>{ label: "En Mantenimiento", y: <? echo $mantenimiento; ?> }<?
				}
				if($baja > 0){
					if($otro > 0){ echo ","; }
					$otro++;
					?>{ label: "De Baja", y: <? echo $baja; ?> }<?
				}
				if($r_mantenimiento > 0){
					if($otro > 0){ echo ","; }
					$otro++;
					?>{ label: "Requiere mantenimiento", y: <? echo $r_mantenimiento; ?> }<?
				}
				if($asignadas > 0){
					if($otro > 0){ echo ","; }
					$otro++;
					?>{ label: "Asignadas", y: <? echo $asignadas; ?> }<?
				}
				?>
			]
		}]
	};
	$("#chartContainer").CanvasJSChart(options);

}
</script>
<div id="chartContainer" style="height: 200px; width: 100%;"></div>
<script src="../../js/jquery-1.11.1.min.js"></script>
<script src="../../js/jquery.canvasjs.min.js"></script>