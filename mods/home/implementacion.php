<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Implementación CAF";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<style type="text/css">
body{ background-color:#EAEAEA !important; } 
.boton_inicio{  
	border-top: 1px solid #CCC;
	width: 100%; 
}
.boton_inicio a{
	color:#000 !important;
	text-decoration:none;
}
.boton_inicio span{
	font-size:11px; 
	font-weight:bold;
}
.boton_inicio:hover{  
	background-color:#FFC;
}
a:hover .span{ 
	font-size:13px !important; 
}
</style> 
<div class="row">  
	<div class="col-md-3 grid-item">
		<div class="panel panel-primary">  
			<div class="panel-heading">
				<b>Resumen de Cargas</b>
			</div> 
			<div class="panel-body" style="padding:5px;"> 
				<table class="table table-bordered table-hover table-condensed"> 
				<thead> 
				    <tr>
				        <th>Carga</th>
				        <th>Tipo</th>
				        <th width="1">Total</th>
				    </tr>
				</thead>
				<tbody>
				<?
				$sql = "SELECT a.acf_id_carga,b.pro_tipo,count(*) as total FROM activos_fijos a 
				left join productos b on a.acf_producto = b.pro_id 
				group by a.acf_id_carga,b.pro_tipo 
				ORDER BY acf_id_carga ASC "; 
				$res = mysqli_query($cnx,$sql);
				if(mysqli_num_rows($res) > 0){
					while($row = mysqli_fetch_array($res)){
						?>
						<tr>
							<td style="padding: 1px 5px;"><? echo $row["acf_id_carga"]; ?></td>
							<td style="padding: 1px 5px;"><? echo $row["pro_tipo"]; ?> - <? echo $_tipo_activo[$row["pro_tipo"]]; ?></td>
							<td style="text-align: right;padding: 1px 5px;"><? echo _num2($row["total"]); ?></td>
						</tr>
						<?
					}
				}
				?>
				</tbody>
				</table>

				<table class="table table-bordered table-hover table-condensed"> 
				<thead> 
				    <tr>
				        <th>Tipo</th>
				        <th width="1">Total</th>
				    </tr>
				</thead>
				<tbody>
				<?
				$sql = "SELECT b.pro_tipo,count(*) as total FROM activos_fijos a 
				left join productos b on a.acf_producto = b.pro_id 
				group by b.pro_tipo 
				ORDER BY total deSC "; 
				$res = mysqli_query($cnx,$sql);
				if(mysqli_num_rows($res) > 0){
					while($row = mysqli_fetch_array($res)){
						?>
						<tr>
							<td style="padding: 1px 5px;"><? echo $row["pro_tipo"]; ?> - <? echo $_tipo_activo[$row["pro_tipo"]]; ?></td>
							<td style="text-align: right;padding: 1px 5px;"><? echo _num2($row["total"]); ?></td>
						</tr>
						<?
						$final += $row["total"];
					}
				}
				?>
				<tr>
					<th style="padding: 1px 5px; text-align: right;">FINAL:</th>
					<th style="text-align: right;padding: 1px 5px;"><? echo _num2($final); ?></th>
				</tr>
				</tbody>
				</table>
			</div> 
		</div>
	</div>
	<div class="col-md-3 grid-item">
		<div class="panel panel-primary">  
			<div class="panel-heading">
				<b>Tablas del Sistema</b>
			</div> 
			<div class="panel-body" style="padding:0px;"> 
				<table class="table table-bordered table-hover table-condensed">  
				<tbody> 
				<tr>				
					<td width="40" style="text-align:center"><img src="img/modulos/iconfinder_19_330401.png" width="35"></td> 	
					<td><b>ACTIVOS</b><br>
					<? if($final > 0){ echo _num2($final)." registros"; }else{ ?><span style="color:red; font-weight: bold;">Sin registros</span><? } ?></td>
					<td width="1"></td>
				</tr>
				<?
				$res = sql_productos("*","");
				$num = mysqli_num_rows($res) * 1;				
				?>	
				<tr>				
					<td width="40" style="text-align:center"><img src="img/modulos/productos.png" width="35"></td> 	
					<td><b>PRODUCTOS</b><br>
					<? if($num > 0){ echo _num2($num)." registros"; }else{ ?><span style="color:red; font-weight: bold;">Sin registros</span><? } ?></td>
					<td width="1"><? construir_boton("pro_listado.php","","derecha","Acceder",2); ?></td>
				</tr>
				<?
				$res = sql_categorias("*","");
				$num = mysqli_num_rows($res) * 1;				
				?>	
				<tr>			
					<td width="40" style="text-align:center"><img src="img/modulos/categorias.png" width="35"></td> 		
					<td><b>CLASIFICACIÓN</b><br>
					<? if($num > 0){ echo _num2($num)." registros"; }else{ ?><span style="color:red; font-weight: bold;">Sin registros</span><? } ?></td>
					<td><? construir_boton("cat_listado.php","","derecha","Acceder",2); ?></td>
				</tr> 
				<?
				$res = sql_unidades("*","");
				$num = mysqli_num_rows($res) * 1;				
				?>	
				<tr> 
					<td width="40" style="text-align:center"><img src="img/modulos/unidades.png" width="35"></td> 
					<td><b>UNIDADES</b><br>
					<? if($num > 0){ echo _num2($num)." registros"; }else{ ?><span style="color:red; font-weight: bold;">Sin registros</span><? } ?></td>
					<td><? construir_boton("uni_listado.php","","derecha","Acceder",2); ?></td>
				</tr>
				<?
				$res = sql_bodegas("*","");
				$num = mysqli_num_rows($res) * 1;				
				?>	
				<tr> 
					<td width="40" style="text-align:center"><img src="img/modulos/bodegas.png" width="35"></td> 
					<td><b>LUGARES</b><br>
					<? if($num > 0){ echo _num2($num)." registros"; }else{ ?><span style="color:red; font-weight: bold;">Sin registros</span><? } ?></td>
					<td><? construir_boton("bod_listado.php","","derecha","Acceder",2); ?></td>
				</tr>
				<?
				$res = sql_ubicaciones("*","");
				$num = mysqli_num_rows($res) * 1;				
				?>	
				<tr> 
					<td width="40" style="text-align:center"><img src="img/modulos/ubicaciones.png" width="35"></td> 
					<td><b>UBICACIONES</b><br>
					<? if($num > 0){ echo _num2($num)." registros"; }else{ ?><span style="color:red; font-weight: bold;">Sin registros</span><? } ?></td>
					<td><? construir_boton("ubi_listado.php","","derecha","Acceder",2); ?></td>
				</tr>
				<?
				$res = sql_centro_costo("*","");
				$num = mysqli_num_rows($res) * 1;				
				?>	
				<tr> 
					<td width="40" style="text-align:center"><img src="img/modulos/iconfinder_19_330401.png" width="35"></td> 
					<td><b>CENTROS DE COSTO</b><br>
					<? if($num > 0){ echo _num2($num)." registros"; }else{ ?><span style="color:red; font-weight: bold;">Sin registros</span><? } ?></td>
					<td><? construir_boton("cco_listado.php","","derecha","Acceder",2); ?></td>
				</tr>
				<?
				$res = sql_auxiliares("*","");
				$num = mysqli_num_rows($res) * 1;				
				?>	
				<tr> 
					<td width="40" style="text-align:center"><img src="img/modulos/auxiliares.png" width="35"></td> 
					<td><b>PROVEEDORES</b><br>
					<? if($num > 0){ echo _num2($num)." registros"; }else{ ?><span style="color:red; font-weight: bold;">Sin registros</span><? } ?></td>
					<td><? construir_boton("aux_listado.php","","derecha","Acceder",2); ?></td>
				</tr>
				<?
				$res = sql_perfiles("*","");
				$num = mysqli_num_rows($res) * 1;				
				?>	
				<tr> 
					<td width="40" style="text-align:center"><img src="img/modulos/user_examiner_group.png" width="35"></td> 
					<td><b>PERFILES</b><br>
					<? if($num > 0){ echo _num2($num)." registros"; }else{ ?><span style="color:red; font-weight: bold;">Sin registros</span><? } ?></td>
					<td><? construir_boton("pfl_listado.php","","derecha","Acceder",2); ?></td>
				</tr>
				<?
				$res = sql_personas("*","and per.per_tipo = 0");
				$num = mysqli_num_rows($res) * 1;				
				?>	
				<tr> 
					<td width="40" style="text-align:center"><img src="img/modulos/usuarios.png" width="35"></td> 
					<td><b>USUARIOS</b><br>
					<? if($num > 0){ echo _num2($num)." registros"; }else{ ?><span style="color:red; font-weight: bold;">Sin registros</span><? } ?></td>
					<td><? construir_boton("usu_listado.php","","derecha","Acceder",2); ?></td>
				</tr>
				<?
				$res = sql_personas("*","and per.per_tipo = 1");
				$num = mysqli_num_rows($res) * 1;				
				?>	
				<tr> 
					<td width="40" style="text-align:center"><img src="img/modulos/usuarios.png" width="35"></td> 
					<td><b>PERSONAS</b><br>
					<? if($num > 0){ echo _num2($num)." registros"; }else{ ?><span style="color:red; font-weight: bold;">Sin registros</span><? } ?></td>
					<td><? construir_boton("per_listado.php","","derecha","Acceder",2); ?></td>
				</tr>
				</tbody>
				</table>
			</div> 
		</div>
	</div> 
	<div class="col-md-3 grid-item">
		<div class="panel panel-primary">  
			<div class="panel-heading">
				<b>Parametrización Empresa</b>
			</div> 
			<div class="panel-body" style="padding:0px;"> 
				<div class="boton_inicio">
					<a href="javascript:carg('imple_tac.php','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="40" style="text-align:center"><img src="img/modulos/parametros.png" width="35"></td> 
						<td>
						<span>Tipo de Activos</span>
						<p>Configurar que tipo de Activos Fijos posee la empresa</p>
						</td>
					</tr>
					</table>
					</a> 
				</div> 
				<div class="boton_inicio">
					<a href="javascript:carg('imple_widgets.php','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="40" style="text-align:center"><img src="img/modulos/parametros.png" width="35"></td> 
						<td>
						<span>Dashboard Inicio</span>
						<p>Configurar pantalla inicial del sistema</p>
						</td>
					</tr>
					</table>
					</a> 
				</div>
				<div class="boton_inicio">
					<a href="javascript:carg('imple_parametros.php','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="40" style="text-align:center"><img src="img/modulos/parametros.png" width="35"></td> 
						<td>
						<span>Datos Personalizados</span>
						<p>Configurar datos del sistema</p>
						</td>
					</tr>
					</table>
					</a> 
				</div> 
				<div class="boton_inicio">
					<a href="javascript:carg('usu_log.php','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="40" style="text-align:center"><img src="img/modulos/usuarios.png" width="35"></td> 
						<td>
						<span>Navegación Monitoreo</span>
						<p>Acciones de los usuarios dentro del sistema</p>
						</td>
					</tr>
					</table>
					</a> 
				</div>   
			</div> 
		</div>  
		<div class="panel panel-primary">  
			<div class="panel-heading">
				<b>Rutinas de MYSQL</b>
			</div> 
			<div class="panel-body" style="padding:0px;"> 
				<div class="boton_inicio">
					<a href="javascript:carg('rutinas.php','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="40" style="text-align:center"><img src="img/modulos/categorias.png" width="35"></td> 
						<td>
						<span>Rutinas</span>
						<p>Acceder a scripts</p>
						</td>
					</tr>
					</table> 
					</a> 
				</div> 
			</div>
		</div> 
	</div> 
	<div class="col-md-3 grid-item">
		<div class="panel panel-primary">  
			<div class="panel-heading">
				<b>Importación de Datos</b>
			</div> 
			<div class="panel-body" style="padding:0px;">  
				<div class="boton_inicio">
					<a href="javascript:carg('acf_importar.php','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="40" style="text-align:center"><img src="img/modulos/iconfinder_19_330401.png" width="35"></td> 
						<td>
						<span>Activos fijos</span>
						<p>Tabla de Activos fijos</p>
						</td>
					</tr>
					</table>
					</a> 
				</div>     
				<div class="boton_inicio">
					<a href="javascript:carg('pro_importar.php','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="40" style="text-align:center"><img src="img/modulos/productos.png" width="35"></td> 
						<td>
						<span>Productos</span>
						<p>Tabla de Productos</p>
						</td>
					</tr>
					</table>
					</a> 
				</div> 
				<div class="boton_inicio">
					<a href="javascript:carg('aux_importar.php','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="40" style="text-align:center"><img src="img/modulos/auxiliares.png" width="35"></td> 
						<td>
						<span>Proveedores</span>
						<p>Tabla de Proveedores</p>
						</td>
					</tr>
					</table>
					</a> 
				</div>
				<div class="boton_inicio">
					<a href="javascript:carg('cat_importar.php','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="40" style="text-align:center"><img src="img/modulos/categorias.png" width="35"></td> 
						<td>
						<span>Clasificación</span>
						<p>Tabla de Clasificación de Productos</p>
						</td>
					</tr>
					</table>
					</a> 
				</div>
				<div class="boton_inicio">
					<a href="javascript:carg('cco_importar.php','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="40" style="text-align:center"><img src="img/modulos/iconfinder_19_330401.png" width="35"></td> 
						<td>
						<span>Centro de Costo</span>
						<p>Tabla de Centros de Costos</p>
						</td>
					</tr>
					</table>
					</a> 
				</div>
				<div class="boton_inicio">
					<a href="javascript:carg('bod_importar.php','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="40" style="text-align:center"><img src="img/modulos/bodegas.png" width="35"></td> 
						<td>
						<span>Lugares</span>
						<p>Tabla de Lugares</p>
						</td>
					</tr>
					</table>
					</a> 
				</div>
				<div class="boton_inicio">
					<a href="javascript:carg('ubi_importar.php','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="40" style="text-align:center"><img src="img/modulos/ubicaciones.png" width="35"></td> 
						<td>
						<span>Ubicaciones</span>
						<p>Tabla de Ubicaciones</p>
						</td>
					</tr>
					</table>
					</a> 
				</div>
				<div class="boton_inicio">
					<a href="javascript:carg('uni_importar.php','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="40" style="text-align:center"><img src="img/modulos/unidades.png" width="35"></td> 
						<td>
						<span>Unidades</span>
						<p>Tabla de Unidades</p>
						</td>
					</tr>
					</table>
					</a> 
				</div>
			</div>
		</div>
	</div>
</div>