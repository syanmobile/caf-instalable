<?php
require("../../inc/conf_dentro.php");
//---------------------------------------------------------------------------------------- 

switch($_REQUEST["modo"]){ 		
	case "eliminar": 
		$sql = "update activos_fijos set acf_etiqueta = '' where acf_etiqueta = '$_REQUEST[etiqueta]' ";
		$res = mysqli_query($cnx,$sql);  
			
		$sql = "delete from etiquetas where eti_codigo = '$_REQUEST[etiqueta]' ";
		$res = mysqli_query($cnx,$sql);  
		break;
			
	case "desasociar": 
		foreach($_POST["cdu"] as $cdu){
			$sql = "update activos_fijos set acf_etiqueta = '' where acf_etiqueta = '$cdu' ";
			$res = mysqli_query($cnx,$sql);  
		
			$sql = "update etiquetas set eti_activo = 0 where eti_codigo = '$cdu'";
			$res = mysqli_query($cnx,$sql); 
		}
		?>
		<br>
		<div class="alert alert-success"> 
			<span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
			Códigos desasociados
		</div>
		<?
		construir_boton("cdu_listado.php","","izquierda","Volver al listado",2);
		break; 
		
	case "asociar":
		$res = sql_activos_fijos("*","and acf.acf_id = '$_REQUEST[acf]' ");  
		if(mysqli_num_rows($res) > 0){
			$datos = mysqli_fetch_array($res);
		}
		?>
		<table class="table table-bordered table-condensed"> 
		<thead>
			<tr>
				<th colspan="2">Datos del Producto</th>
			</tr>
		</thead> 
		<tbody>
			<tr>
				<th width="110" style="text-align:right;">Código:</th>
				<th><?php echo $datos["pro_codigo"]; ?></th> 
			</tr> 
			<tr>
				<th style="text-align:right;">Cod.Barra:</th> 
				<td><?php echo $datos["pro_codigo_barra"]; ?></td>
			</tr>
			<tr>
				<th style="text-align:right;">Descripción:</th> 
				<td><?php echo $datos["pro_nombre"]; ?></td>
			</tr> 
			<tr>
				<th style="text-align:right;">Serial:</th>
				<th><?php echo $datos["acf_serie"]; ?></th>
			</tr>
		</tbody>
		</table>
		
		<table class="table table-bordered table-condensed" style="margin-top: 0px;">  
		<tbody>
		<tr>
			<th width="110" style="text-align:right;">Etiqueta:</th>
			<td id="cdu_html"><select name="codigo_af" id="codigo_af" class="campos buscador">
			<option value=""></option>
			<?php
			// Sacar los productos con movimiento
			$res3 = sql_etiquetas("*"," and eti_activo = 0 order by eti_codigo asc");
			$num3 = mysqli_num_rows($res3);
			if($num3 > 0){
				while($row3 = mysqli_fetch_array($res3)){
					?><option value="<?php echo $row3["eti_codigo"]; ?>"><?php
					echo $row3["eti_codigo"];  
					?></option><?php 
				}
			}
			?>
			</select></td>
			<td width="1"><?
			construir_boton("crear_etiqueta(".$datos["acf_id"].")","","crear","Crear",4);
			?></td>
		</tr> 
		</tbody>
		</table>    
		<div id="btn_save"><?
		construir_boton("","","grabar","Guardar Datos",3);
		?></div>
		
		<script language="javascript">
		$(document).ready(function(){
			$(".buscador").chosen({
				width:"95%",
				no_results_text:'Sin resultados con',
				allow_single_deselect:true 
			});
		});
		function save(){
			if(document.getElementById("codigo_af").value == ""){
				alert("Seleccione una etiqueta o bien cree una nueva");
				return;
			}
			var etiqueta = document.getElementById("codigo_af").value;
			var div = document.getElementById("lugarcito");  
			AJAXPOST("mods/home/cdu_ajax2.php","modo=asociar_save&acf=<? echo $datos["acf_id"]; ?>&codigo_af="+etiqueta,div,false,function(){
				$("#label<? echo $datos["acf_id"]; ?>").html('<span class="label label-success">ASOCIADO</span>');
			}); 
		} 
		function crear_etiqueta_save(){
			if(document.getElementById("codigo_unico").value == ""){
				alert("Ingrese la etiqueta a crear");
				document.getElementById("codigo_unico").focus();
				return;
			}else{ 
				var div = document.getElementById("form-etiqueta-save"); 
				AJAXPOST("mods/home/acf_ajax.php","modo=crear_save&cdu="+document.getElementById("codigo_unico").value+"&acf=<? echo $datos["acf_id"]; ?>",div,false,function(){
					$("#label<? echo $datos["acf_id"]; ?>").html('<span class="label label-success">ASOCIADO</span>');
					$("#btn_save").hide("fast");
				});
			}
		}
		</script>
		<?
		break;
		
	case "asociar_save": 
		$sql = "update activos_fijos set  
		acf_etiqueta = '$_REQUEST[codigo_af]' 
		where acf_id = '$_REQUEST[acf]' "; 
		$res = mysqli_query($cnx,$sql);   
		
		$sql = "update etiquetas set eti_activo = $_REQUEST[acf] where eti_codigo = '$_REQUEST[codigo_af]'"; 
		$res = mysqli_query($cnx,$sql);
		?>
        <div class="alert alert-success"> 
            <span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
            Datos del Activo Fijo guardados
        </div>
		<? 
		break;
}
?> 