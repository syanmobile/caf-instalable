<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabación Ingreso"; 
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

switch($_POST["modo"]){  
	case "crear":
		$sql = "INSERT INTO movimientos ( 
			mov_per_id,
			mov_tipo, 
			mov_concepto,
			mov_bodega, 
			mov_folio,
			mov_fecha,
			mov_auxiliar,
			mov_responsable, 
			mov_cco_id,
			mov_extra_1,
			mov_extra_2,
			mov_extra_3,
			mov_extra_4,
			mov_extra_5,
			mov_glosa,
			mov_total  
		) VALUES (  
			'".$_SESSION["per_conectado"]["per_id"]."',
			'$_POST[tipo]',
			'$_POST[concepto]',
			'$_POST[bodega]', 
			'$_POST[folio]',
			'"._fec($_POST["fecha"],9)."',
			'$_POST[auxiliar]', 
			'$_POST[responsable]', 
			'$_POST[centro_costo]',
			'$_POST[extra_1]',
			'$_POST[extra_2]',
			'$_POST[extra_3]',
			'$_POST[extra_4]',
			'$_POST[extra_5]',
			'$_POST[glosa]',
			'$_POST[total]' 
		)"; 
		$res = mysqli_query($cnx,$sql);
		$movi = mysqli_insert_id($cnx);  
		
		$prod_det = explode("*****",$_REQUEST["prod_det"]);
		$lote_det = explode("*****",$_REQUEST["lote_det"]);
		$cant_det = explode("*****",$_REQUEST["cant_det"]);
		$prec_det = explode("*****",$_REQUEST["prec_det"]);
		for($i = 1;$i < count($prod_det);$i++){			
			
			if($cant_det[$i] > 0){ /************************************************************************************/
				
				$resp = sql_productos("*","and pro.pro_codigo = '".$prod_det[$i]."'");  
				if(mysqli_num_rows($resp) > 0){
					$producto = mysqli_fetch_array($resp);
				}
				
				if($lote_det[$i] == ""){
					$subtotal = $cant_det[$i] * $prec_det[$i];
					$sql3 = "INSERT INTO movimientos_detalle (
						det_mov_id,
						det_ubi_id,
						det_producto,
						det_ingreso,
						det_valor,
						det_total 
					) VALUES (
						'$movi',
						'".$_REQUEST["ubicacion"]."', 
						'".$producto["pro_id"]."',
						'".$cant_det[$i]."',
						'".$prec_det[$i]."',
						'$subtotal' 
					)"; 
					$res3 = mysqli_query($cnx,$sql3);   
				}else{
					$lotes = explode(",,,,,",$lote_det[$i]);
					if(count($lotes) > 1){
						for($i2 = 1;$i2 < count($lotes);$i2++){ 
							$valor_sec = $prec_det[$i] / $cambio;
							// Aca de crean los Activos Fijos como dato
							$sql3 = "insert into activos_fijos (acf_producto,acf_serie,acf_ubicacion,acf_valor,acf_valor_sec,  acf_fecha_ingreso) 
							values ('".$producto["pro_id"]."','".$lotes[$i2]."','$_REQUEST[ubicacion]','".$prec_det[$i]."',
							'".$valor_sec."','"._fec($_POST["fecha"],9)."')";
							$res3 = mysqli_query($cnx,$sql3);
							$id3 = mysqli_insert_id($cnx); 
							
							$activos[] = $id3;
							 				
							$sql3 = "INSERT INTO movimientos_detalle (
								det_mov_id,
								det_ubi_id,
								det_producto,
								det_ingreso,
								det_valor,
								det_total,
								det_lote  
							) VALUES (
								'$movi', 
								'".$_REQUEST["ubicacion"]."',  
								'".$producto["pro_id"]."',
								'1',
								'".$prec_det[$i]."',
								'".$prec_det[$i]."',
								'".$lotes[$i2]."' 
							)";  
							$res3 = mysqli_query($cnx,$sql3); 
						}
					}
				}
				
				costo_promedio($producto["pro_id"]);
			} /************************************************************************************/
		}
		?>
        <table width="100%">
        <tr valign="top">
            <td width="300">
                <div class="alert alert-success"> 
        			<span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
                    Documento de Ingreso guardado con &eacute;xito<br />
                    <b style="font-size:20px;"><? echo $_POST["tipo"]."/".$_POST["folio"]; ?></b>
                </div>
                <?php 
				construir_boton("transacciones.php","","buscar","Volver a Transacciones",2); 
				construir_boton("ing_nuevo.php","","crear","Crear Otro Documento de Ingreso",2);
				?>
            </td>
            <td style="padding-left:15px;">
            	<iframe src="<?php echo "mods/home/aju_pdf.php?id=".$movi; ?>" width="100%" height="400"></iframe>
				
            	<?php 
				if($activos <> ""){
					?>
					<div class="alert alert-info">
						<b>Ha creado <? echo count($activos); ?> nuevos activos</b><br>
						Es importante asociarle una etiqueta a cada item<br><br>
						<?php 
						construir_boton("cdu_acf_sin_etiquetas.php","","buscar","Ver Activos sin Etiquetas",2); 
						?>
					</div>					
					<?
				} 
				?>
            </td>
        </tr>
        </table>
		<?php   
		break;  

	case "crear2":  
		if($_REQUEST["compra_sec"] <> ""){
			$cambio = $_REQUEST["cambio"];

			$sql = "UPDATE configuraciones SET cfg_moneda_ult_cambio = '$cambio' WHERE cfg_id = 1"; 
			$res = mysqli_query($cnx,$sql);
		}else{
			$cambio = 0;
		}
		$sql = "INSERT INTO movimientos ( 
			mov_per_id,
			mov_tipo, 
			mov_concepto,
			mov_bodega, 
			mov_folio,
			mov_cco_id,
			mov_fecha,
			mov_auxiliar,
			mov_responsable, 
			mov_extra_1,
			mov_extra_2,
			mov_extra_3,
			mov_extra_4,
			mov_extra_5,
			mov_glosa,
			mov_valor_cambio,
			mov_total  
		) VALUES (  
			'".$_SESSION["per_conectado"]["per_id"]."',
			'$_POST[tipo]',
			'$_POST[concepto]',
			'$_POST[bodega]', 
			'$_POST[folio]',
			'$_POST[centro_costo]',
			'"._fec($_POST["fecha"],9)."',
			'$_POST[auxiliar]', 
			'$_POST[responsable]', 
			'$_POST[extra_1]',
			'$_POST[extra_2]',
			'$_POST[extra_3]',
			'$_POST[extra_4]',
			'$_POST[extra_5]',
			'$_POST[glosa]',
			'$cambio',
			'$_POST[total]' 
		)";  
		$res = mysqli_query($cnx,$sql);
		$movi = mysqli_insert_id($cnx);  
		
		$prod_det = explode(";",$_REQUEST["prod_det"]);
		$depr_det = explode(";",$_REQUEST["depr_det"]);
		$lote_det = explode(";",$_REQUEST["lote_det"]);
		$cant_det = explode(";",$_REQUEST["cant_det"]);
		$prec_det = explode(";",$_REQUEST["prec_det"]);
		$gara_det = explode(";",$_REQUEST["gara_det"]);

		$res = sql_auxiliares("*"," and aux.aux_id = '$_REQUEST[auxiliar]'","");  
		if(mysqli_num_rows($res) > 0){
			$auxiliar = mysqli_fetch_array($res);
		}
		
		$periodo = substr($_POST["fecha"],3,20);
		$periodo = str_replace("/","-",$periodo); 
		$ano_ = substr($periodo,3,4);
		$mes_ = substr($periodo,0,2); 

		for($i = 1;$i < count($prod_det);$i++){ 
			
			if($cant_det[$i] > 0){
				$lineas++;

				$resp = sql_productos("*","and pro.pro_id = '".$prod_det[$i]."'");  
				if(mysqli_num_rows($resp) > 0){
					$prod = mysqli_fetch_array($resp);
				}

				// Subtotal
				$subtotal = $prec_det[$i] * $cant_det[$i];
				$total += $subtotal;

				if($prod["pro_tipo"] == "INS"){
					$sql3 = "INSERT INTO movimientos_detalle (
						det_mov_id,
						det_ubi_id,
						det_producto,
						det_ingreso,
						det_valor,
						det_total 
					) VALUES (
						'$movi',
						'".$_REQUEST["ubicacion"]."', 
						'".$prod_det[$i]."',
						'".$cant_det[$i]."',
						'".$prec_det[$i]."',
						'$subtotal'
						
					)"; 
					$res3 = mysqli_query($cnx,$sql3);
				}else{
					$lotes = explode(",",$lote_det[$i]);
					$dep = explode(",",$depr_det[$i]);

					for($i2 = 1;$i2 <= $cant_det[$i];$i2++){
						// Aca de crean los Activos Fijos como dato 

						$valor_sec = $cambio; //$prec_det[$i] / $cambio; 
						$sql3 = "insert into activos_fijos (
							acf_producto,
							acf_serie,
							acf_ubicacion,

							acf_cco_id,
							acf_valor,
							acf_valor_sec,
							acf_fecha_ingreso,
							acf_concepto_ingreso,
							acf_nro_factura,
							acf_proveedor,

							acf_tipo_depreciacion,
							acf_vida_util,
							acf_valor_residual,
							acf_tipo_adquisicion 
						) values (
							'".$prod_det[$i]."',
							'".$lotes[$i2]."',
							'$_REQUEST[ubicacion]',
							
							'$_REQUEST[centro_costo]',
							'".$prec_det[$i]."',
							'$valor_sec',
							'"._fec($_POST["fecha"],9)."',
							'$_POST[concepto]',
							'$_POST[folio]',
							'".$auxiliar["aux_id"]."',

							'".$dep[0]."',
							'".$dep[1]."',
							'".$dep[2]."',
							'".$dep[3]."' 
						)";  
						$res3 = mysqli_query($cnx,$sql3);
						$id3 = mysqli_insert_id($cnx); 
						$activos[] = $id3; 

						// Activos que requieren garantias
						if(($gara_det[$i] * 1) > 0){ 
							$activos_garantias .= ",".$id3;
							$activos_garantias_total++; 
						}
					 				
						$sql3 = "INSERT INTO movimientos_detalle (
							det_mov_id,
							det_ubi_id,
							det_acf_id,
							det_producto,
							det_ingreso,
							det_valor,
							det_total,
							det_lote  
						) VALUES (
							'$movi', 
							'".$_REQUEST["ubicacion"]."',
							'$id3',   
							'".$prod_det[$i]."',
							'1',
							'".$prec_det[$i]."',
							'".$prec_det[$i]."',
							'".$lotes[$i2]."' 
						)";   
						$res3 = mysqli_query($cnx,$sql3); 
					}
				}
				costo_promedio($prod_det[$i]);
			}
		}

		foreach ($activos as $acti) {
			$res = sql_activos_fijos("*"," and acf_id = '$acti'");  
			if(mysqli_num_rows($res) > 0){
				while($datos = mysqli_fetch_array($res)){

					$codigo  = $datos["pro_tipo"]."-".$_categorias_codigo[$datos["pro_categoria"]]."-";
					$codigo .= agregar_ceros($datos["acf_id"],6);
					$fecha_inicio = $datos["acf_fecha_ingreso"];

					//sumo x meses 
					$meses = $datos["acf_vida_util"];
					$fecha_termino = date("Y-m-d",strtotime($fecha_inicio."+ ".($meses- 1)." month")); 
					
					switch($datos["acf_tipo_depreciacion"]){
						case "L": $meses_divisor = $meses; break;
						case "A": $meses_divisor = round($meses / 3); break;
					}
					
					$valor_activo = $datos["acf_valor"] * 1;
					$residual = $datos["acf_valor_residual"] * 1;
					$valor = $valor_activo - $residual; 

					$valor_mensual = round(($valor / $meses_divisor),2);

					$sql3 = "update activos_fijos set
					acf_codigo = '$codigo',
					acf_vida_util = '$meses',
					acf_mes_ano_inicio_revalorizacion = '$fecha_inicio',
					acf_mes_ano_inicio_depreciacion = '$fecha_inicio',
					acf_mes_ano_termino_depreciacion = '$fecha_termino',
					acf_valor_a_depreciar = '$valor',
					acf_cuota_depreciacion = '$valor_mensual' 
					where acf_id = '$datos[acf_id]' ";
					$res3 = mysqli_query($cnx,$sql3);

					$conta++;
				}
			}
			# code...
		}
		?>
        <table width="100%">
        <tr valign="top">
            <td width="300">
                <div class="alert alert-success"> 
        			<span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
                    Documento de Ingreso guardado con &eacute;xito<br />
                    <b style="font-size:20px;"><? echo $_POST["tipo"]."/".$_POST["folio"]; ?></b>
                </div>
                <?php 
				construir_boton("transacciones.php","","buscar","Volver a Transacciones",2); 
				construir_boton("ing_nuevo2.php","","crear","Crear Otro Documento de Ingreso",2);

				if($activos_garantias_total > 0){
					?>
					<br><br>
	                <div class="alert alert-danger"> 
	        			<span class="glyphicon glyphicon-dashboard" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
	                    Activo(s) requieren ingresar su Garantía<br />
	                    <b style="font-size:20px;"><? echo $activos_garantias_total; ?> activos</b>
	                </div>
	                <?php 
					//construir_boton("gar_ingresar.php","&acf=".$activos_garantias,"crear","Ingresar Garantías",2); 
				}
				?>
            </td>
            <td style="padding-left:15px;">
            	<iframe src="<?php echo "mods/home/aju_pdf.php?id=".$movi; ?>" width="100%" height="400"></iframe>
				
            	<?php 
				if($activos <> ""){
					?>
					<div class="alert alert-info">
						<b>Ha creado <? echo count($activos); ?> nuevos activos</b><br>
						Es importante asociarle una etiqueta a cada item<br><br>
						<?php 
						construir_boton("acf_sin_etiquetas.php","","buscar","Ver Activos sin Etiquetas",2); 
						?>
					</div>					
					<?
				} 
				?>
            </td>
        </tr>
        </table>
		<?php   
		break;  
}
?>