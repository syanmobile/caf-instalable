<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabación Ubicación";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

switch($_POST["modo"]){
	case "crear":
		$res = sql_ubicaciones("*"," and ubi.ubi_codigo = '$_REQUEST[codigo]'","","");
		if(mysqli_num_rows($res) > 0){
			?>
            <div class="alert alert-danger"> 
               <b>ERROR</b>, código ya existe: '<strong><? echo $_POST["codigo"]; ?></strong>'
            </div>
            <?
		}else{ 
			$sql = "INSERT INTO ubicaciones ( 
				ubi_codigo,
				ubi_nombre,
				ubi_bodega 
			) VALUES ( 
				'$_POST[codigo]',
				'$_POST[nombre]' ,
				'$_POST[bodega]' 
			)";
			$res = mysqli_query($cnx,$sql);
			$_POST["id"] = mysqli_insert_id($cnx);
			?>
			<div class="alert alert-success"> 
			
				<strong>Ubicación '<? echo $_POST["nombre"]; ?>' creado con &eacute;xito</strong>
			</div>
			<?php 
		}
		break;
		
	case "editar":
		$SQL_ = "UPDATE ubicaciones SET 
			ubi_nombre = '$_POST[nombre]',
			ubi_bodega = '$_POST[bodega]' 
		WHERE ubi_id = '$_POST[id]' ";   
		$res = mysqli_query($cnx,$SQL_);
		?>   
        <div class="alert alert-success"> 
           Ubicación <strong>'<? echo $_POST["nombre"]; ?>'</strong> editado con &eacute;xito
        </div>  
		<?php  
		break;
}

construir_boton("ubi_listado.php","","buscar","Listado Ubicaciones",2);
if($_REQUEST["modo"] <> "eliminar"){
	construir_boton("ubi_editar.php","&id=".$_POST["id"],"editar","Editar esta Ubicación",2);
}
construir_boton("ubi_nuevo.php","","crear","Crear otra Ubicación",2);
?>