<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------

$res = sql_activos_fijos("*"," and acf_id >= $_REQUEST[desde_7] and acf_id <= $_REQUEST[hasta_7] ");  
if(mysqli_num_rows($res) > 0){
	while($datos = mysqli_fetch_array($res)){
		
		$sql2 = generar_depreciaciones_del_activo($datos["acf_id"]);
		if($sql2 <> ""){
			echo $sql2.";<br><br>";
		}
	}
}

function generar_depreciaciones_del_activo($id){

	$res = sql_activos_fijos("*"," and acf.acf_id = '$id' "); 
	$num = mysqli_num_rows($res);
	if($num > 0){
		$datos = mysqli_fetch_array($res); 

		$periodo = explode("-",$datos["acf_mes_ano_inicio_depreciacion"]);
		$mes = $periodo[1] * 1;
		$ano = $periodo[0] * 1;
		$meses = $datos["acf_vida_util"] * 1;
		if($datos["acf_tipo_depreciacion"] == "L"){
			$meses_divisor = $meses;
		}else{
			$meses_divisor = round($meses / 3);
		}

		$valor_activo = $datos["acf_valor_a_depreciar"] * 1;
		$valor_mensual = round(($valor_activo / $meses_divisor),2);

		$ano_limite = substr($datos["acf_fecha_salida"],0,4) * 1;
		$mes_limite = substr($datos["acf_fecha_salida"],5,2) * 1;

		for($i = 1;$i <= $meses_divisor;$i++){
			$acumulado += $valor_mensual; 
			$valor_activo -= $valor_mensual;

			$mesx = ($mes < 10)?"0".$mes:$mes;

			if($ano > 2019){
				if(($datos["acf_fecha_salida"] == "0000-00-00") || ($ano <= $ano_limite && $mes <= $mes_limite)){ 
					if($sql_final == ""){
						$sql_final = "delete from depreciaciones where dep_acf_id = $id;<br><br> 

						insert into depreciaciones (
							dep_acf_id,
							dep_periodo,
							dep_cuota,
							dep_acumulada,
							dep_neto 
						) values ";
					}else{
						$sql_final .= ",";
					}

					$sql_final .= "
					(
						'$id',
						'".$ano."-".$mesx."-01',
						'$valor_mensual',
						'$acumulado',
						'$valor_activo'
					)"; 
				}
			}
			if($mes == 12){
				$mes = 0;
				$ano++;
			} 
			$mes++;
		} 
	}
	return $sql_final;
} 
?>