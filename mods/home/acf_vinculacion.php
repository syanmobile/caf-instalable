<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Agrupar/Desagrupar Activos Fijos";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript"> 	
$(document).ready(function(){
	<?php
	if($_REQUEST["activo"] <> ""){
		?>
		disponibles();
		agrupados();
		<?
	}
	?>
});
function disponibles(){  
	var div = document.getElementById("disponibles"); 
	var a = $(".campos").fieldSerialize();  
	$("#disponibles").html("<h4>Cargando...</h4>");
	AJAXPOST("mods/home/acf_vinculacion_ajax.php",a+"&modo=disponibles",div); 
}
function agrupados(){  
	var div = document.getElementById("agrupados"); 
	var a = $(".campos").fieldSerialize();  
	$("#agrupados").html("<h4>Cargando...</h4>");
	AJAXPOST("mods/home/acf_vinculacion_ajax.php",a+"&modo=agrupados",div); 
}
function agrupar(acf){
	$("#acf2_"+acf).hide("fast");
	var div = document.getElementById("operacion"); 
	var a = $(".campos").fieldSerialize();   
	AJAXPOST("mods/home/acf_vinculacion_ajax.php",a+"&modo=agrupar&acf="+acf,div,false,agrupados); 
}
function desagrupar(acf){
	$("#acf1_"+acf).hide("fast");
	var div = document.getElementById("operacion"); 
	var a = $(".campos").fieldSerialize();   
	AJAXPOST("mods/home/acf_vinculacion_ajax.php",a+"&modo=desagrupar&acf="+acf,div,false,disponibles); 
}
</script>
<table class="table  table-bordered"> 
<tr> 
    <td>
    <b>Seleccione un Activo Disponible:</b>
    <br>
    <select name="activo" id="activo" class="campos buscador" onChange="javascript:carg('acf_vinculacion.php','');">
    <option value=""></option>
	<?php
	// Sacar los productos con movimiento
	$res = sql_activos_fijos("*"," order by acf.acf_producto asc, acf.acf_serie asc");
	$num = mysqli_num_rows($res);
	if($num > 0){
		while($row = mysqli_fetch_array($res)){
			?><option value="<?php echo $row["acf_id"]; ?>" <?php
			if($row["acf_id"] == $_REQUEST["activo"]){ echo "selected"; $datos = $row; } ?>><?php
			echo $row["pro_codigo"]." - ".$row["pro_nombre"]." | SERIE: ".$row["acf_serie"];  
			?></option><?php 
		}
	}
    ?>
    </select></td>
</tr> 
</table>
<div id="operacion"></div>
<?php
if($_REQUEST["activo"] <> ""){
	?>
	<table width="100%">
		<tr valign="top">
			<td width="50%" id="agrupados"></td>
			<td id="disponibles" style="padding-left: 15px;"></td>
		</tr>
	</table>
	<?
}
?>