<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabación";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

$tipos = explode(",",$_configuracion["tipo_auxiliares"]);
for($i = 0;$i < count($tipos);$i++){
	if($_REQUEST["tipo_".$i]){
		$tipos_marcados[$tipos[$i]] = "S";
	}else{
		$tipos_marcados[$tipos[$i]] = "";
	}
}
$tipos_dato = serialize($tipos_marcados);
 
switch($_POST["modo"]){
	case "crear":
		$res = sql_auxiliares("*"," and aux.aux_codigo = '$_REQUEST[codigo]'","","");
		if(mysqli_num_rows($res) > 0){
			?>
            <div class="alert alert-danger"> 
               <b>ERROR</b>, código ya existe: '<strong><? echo $_POST["codigo"]; ?></strong>'
            </div>
            <?
		}else{ 
			$sql = "INSERT INTO auxiliares ( 
				aux_codigo,
				aux_rut,
				aux_nombre, 
				aux_direccion,
				aux_comuna,
				aux_telefono,
				aux_correo, 
				aux_imagen 
			) VALUES ( 
				'$_POST[codigo]',
				'$_POST[rut]', 
				'$_POST[nombre]',
				'$_POST[direccion]',
				'$_POST[comuna]',
				'$_POST[telefono]',
				'$_POST[correo]', 
				'$_POST[imagen]' 
			)"; 
			$res = mysqli_query($cnx,$sql);
			$_POST["id"] = mysqli_insert_id($cnx);
			?>
			<div class="alert alert-success"> 
			   Proveedor <strong>'<? echo $_POST["nombre"]; ?>'</strong> creado con &eacute;xito
			</div>
			<?php  
		}
		break; 
		
	case "editar":
		$SQL_ = "UPDATE auxiliares SET 
			aux_rut = '$_POST[rut]', 
			aux_nombre = '$_POST[nombre]',
			aux_direccion = '$_POST[direccion]',
			aux_comuna = '$_POST[comuna]',
			aux_telefono = '$_POST[telefono]',
			aux_correo = '$_POST[correo]', 
			aux_imagen = '$_POST[imagen]' 
		WHERE aux_id = '$_POST[id]' ";   
		$res = mysqli_query($cnx,$SQL_);
		?>   
        <div class="alert alert-success"> 
           Proveedor <strong>'<? echo $_POST["nombre"]; ?>'</strong> editado con &eacute;xito
        </div>  
		<?php  
		break;
}

construir_boton("aux_listado.php","","buscar","Listado de Proveedores",2);
if($_REQUEST["modo"] <> "eliminar"){
	construir_boton("aux_editar.php","&id=".$_POST["id"],"editar","Editar este Proveedor",2);
}
construir_boton("aux_nuevo.php","","crear","Crear otro Proveedor",2);
?>