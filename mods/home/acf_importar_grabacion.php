<?php 
require("../../inc/conf_dentro.php");

$tipo_dep = _opc("tipo_depreciacion");

$capt = date("YmdHis");
if (isset($_FILES["archivo"]) && is_uploaded_file($_FILES['archivo']['tmp_name'])) {							
    $fp = fopen($_FILES['archivo']['tmp_name'], "r");
    while (!feof($fp)){
		$data = explode(";", fgets($fp));  
		$nlinea++;
		if($nlinea > 1){ 

			$etiqueta = rtrim($data[0]);
			$serie = rtrim($data[1]);
			$producto = rtrim($data[2]); 
			$fecha = rtrim($data[3]);
			$monto = rtrim($data[4]);
			$proveedor = rtrim($data[5]);
			$folio = rtrim($data[6]);
			$cco = rtrim($data[7]); 
			$ubicacion = rtrim($data[8]); 
			$fecha_ini_dep = rtrim($data[9]);
			$valor_residual = rtrim($data[10]);
			$notas = rtrim($data[11]);
			$compra = $producto.";;;".$proveedor.";;;".$folio;

			//Por defecto por ahora...
			$concepto = 1; 
			
			if(!in_array($producto,$productos_consultados)){
				$productos_consultados[] = $producto;
				$respro = mysqli_query($cnx,"SELECT * FROM productos where pro_codigo like '$producto' ");  
				$rowpro = mysqli_fetch_array($respro);
				$productos_id[$producto] = $rowpro["pro_id"];
				$productos_ids[] = $rowpro["pro_id"];

				if($tipo_dep == "L"){
					$vida_util_[$producto] = $_tipo_activos_lineal[$rowpro["pro_tipo"]];
				}else{
					$vida_util_[$producto] = $_tipo_activos_acele[$rowpro["pro_tipo"]];
				}
			}
			if(!in_array($proveedor,$proveedores_consultados)){
				$proveedores_consultados[] = $proveedor;
				$resaux = mysqli_query($cnx,"SELECT * FROM auxiliares where aux_codigo like '$proveedor' "); 
				$rowaux = mysqli_fetch_array($resaux);
				$proveedores_id[$proveedor] = $rowaux["aux_id"]; 
			} 
			$movimientos_detalle[] = array($productos_id[$producto],$monto,$_ubicaciones_ubi_id[$ubicacion]);

			$vida_util = $vida_util_[$producto];
			$fecha_ter_dep = suma_a_fecha_meses($fecha_ini_dep,$vida_util);

			switch($tipo_dep){
				case "L": $meses_divisor = $vida_util; break;
				case "A": $meses_divisor = round($vida_util / 3); break;
			}
			 
			$valor_a_depreciar = $monto - $valor_residual; 

			$cuota_depreciacion = round(($valor_a_depreciar / $meses_divisor),2);
 		
			$sql_det = "insert into activos_fijos ( 
				acf_etiqueta,
				acf_serie,
				acf_producto,
				acf_fecha_ingreso,
				acf_valor,
				acf_proveedor,
				acf_nro_factura,
				acf_concepto_ingreso,
				acf_cco_id,					
				acf_ubicacion,
				acf_valor_residual,
				acf_vida_util,
				acf_mes_ano_inicio_revalorizacion,
				acf_mes_ano_inicio_depreciacion,
				acf_mes_ano_termino_depreciacion,
				acf_valor_a_depreciar,
				acf_cuota_depreciacion,
				acf_id_item_carga,
				acf_compra_info,
				acf_notas
			) values (
				'$etiqueta', 
				'$serie', 
				'".$productos_id[$producto]."', 
				'$fecha',  
				'$monto', 
				'".$proveedores_id[$proveedor]."', 
				'$folio', 
				'$concepto',
				'".$_centro_costos_cod[$cco]."', 
				'".$_ubicaciones_ubi_id[$ubicacion]."',
				'$valor_residual',
				'$vida_util',
				'$fecha_ini_dep',
				'$fecha_ini_dep',
				'$fecha_ter_dep',
				'$valor_a_depreciar',
				'$cuota_depreciacion',
				'$capt',
				'$compra',
				'$notas'
			)"; 
			$res_det = mysqli_query($cnx,$sql_det); 
			$productos++; 
		} // nlinea > 1
    }
}
?> 
<div class="alert alert-success"> 
    <strong><?php echo $productos; ?> activo(s) cargado(s) con éxito</strong>
</div>
<script language="javascript">
document.multiple_upload_form.action = "<?php echo $url_web; ?>mods/home/acf_importar_validacion.php";
</script>
<?
// COLOCAR CODIGO PROPIO.....................................................
$res = sql_activos_fijos("*"," and acf_codigo = ''  ","","");  
if(mysqli_num_rows($res) > 0){
	while($datos = mysqli_fetch_array($res)){
		
		$codigo  = $datos["pro_tipo"]."-".$_categorias_codigo[$datos["pro_categoria"]]."-";
		$codigo .= agregar_ceros($datos["acf_id"],6);
		$sql3 = "update activos_fijos set 
		acf_codigo = '$codigo' 
		where acf_id = '$datos[acf_id]' ";
		$res3 = mysqli_query($cnx,$sql3); 
	}
}

$sql = "INSERT INTO movimientos ( 
	mov_per_id,
	mov_tipo, 
	mov_concepto,
	mov_bodega, 
	mov_fecha,    
	mov_glosa  
) VALUES (  
	'".$_SESSION["per_conectado"]["per_id"]."',
	'E',
	'1',
	'1', 
	'".date("Y-m-d")."', 
	'CARGA DE ACTIVOS ".date("Y-m-d")."' 
)"; 
$res = mysqli_query($cnx,$sql);
$movi = mysqli_insert_id($cnx);  

foreach($movimientos_detalle as $detalle){
	$prod_id = $detalle[0];
	$monto = $detalle[1];
	$ubicacion = $detalle[2];
	$total += $monto;

	$sql3 = "INSERT INTO movimientos_detalle (
		det_mov_id,
		det_ubi_id,
		det_producto,
		det_ingreso,
		det_valor,
		det_total 
	) VALUES (
		'$movi',
		'$ubicacion', 
		'$prod_id',
		'1',
		'$monto',
		'$monto' 
	)"; 
	$res3 = mysqli_query($cnx,$sql3);  
}

$sql = "update movimientos set mov_folio = '$movi',mov_total = '$total' where mov_id = '$movi' "; 
$res = mysqli_query($cnx,$sql); 
?>