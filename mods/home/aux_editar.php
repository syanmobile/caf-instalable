<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Editar Proveedor"; 
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

$res = sql_auxiliares("*"," and aux.aux_id = '$_REQUEST[id]'","");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<b>ERROR EN CONSULTA</b><br>No se encontró el dato
	</div>
    <?php
	exit();
}
?>
<script language="javascript">
$(document).ready(function() {
	$("#rut").Rut({
		format_on: 'keyup',
	   on_error: function(){
			alert('El rut ingresado es incorrecto');
			document.getElementById("rut").value = "";
	   }
	}); 
	$('#archivo').on('change',function(){ 
		$('#multiple_upload_form').ajaxForm({
			target:'#procesando_upload',
			data: { modo: "upload" },
			beforeSubmit:function(e){
				$('.uploading').show();
			},
			success:function(e){
				$('.uploading').hide();
				$("#imagen_ruta").attr("src","upload/<? echo $_SESSION["key_id"]; ?>/"+document.getElementById("imagen").value); 
			},
			error:function(e){
			}
		}).submit(); 
	});
});
function save(){  
	if(document.getElementById("nombre").value == ""){
		alerta_js("Es obligación ingresar el NOMBRE");
		return;	
	}
	carg("aux_save.php","");
} 
</script>

<input type="hidden" name="modo" value="editar" class="campos" />
<input type="hidden" name="id" value="<? echo $_REQUEST["id"]; ?>" class="campos" />
<table width="100%">
<tr valign="top">
<td width="250">
    <?
	$imagen = ($datos["aux_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$datos["aux_imagen"];
	?>
	<img src="<?php echo $imagen; ?>" id="imagen_ruta" class="img-thumbnail" style="width:250px !important;"> 
    <table class="table table-bordered" style="margin-top:10px;"> 
    <tbody>
        <tr>
            <td>
            <form method="post" name="multiple_upload_form" id="multiple_upload_form" enctype="multipart/form-data" action="<?php echo $url_web; ?>mods/home/aux_ajax.php"> 
                <input type="file" name="archivo" id="archivo" accept='image/*'>
                
                <div class="uploading none" style="display:none;"><img src="img/uploading.gif"/></div>
                <div id="eliminando"></div> 
                <div id="procesando_upload">
                    <input type="hidden" name="imagen" value="<?php echo $datos["aux_imagen"]; ?>" class="campos" />
                </div>
            </form> 
            </td>
		</tr>
	</tbody>
    </table>
</td>
<td style="padding-left:10px;">
    <table class="table table-bordered table-condensed"> 
    <tr>
        <th width="80">Código:</th>
        <td>
        	<input type="hidden" name="codigo" class="campos" value="<?php echo $datos["aux_codigo"]; ?>">
        	<input type="text" value="<?php echo $datos["aux_codigo"]; ?>" class="campos w10" style="text-align:center; width:200px !important;" readonly>
        </td>
    </tr> 
    <tr>
        <th>RUT:</th>
        <td><input type="text" name="rut" id="rut" class="campos w10" value="<?php echo $datos["aux_rut"]; ?>" style="text-align:center; width:200px !important;"></td>
    </tr>    
    <tr>
        <th>Nombre:</th>
        <td><input type="text" name="nombre" id="nombre" class="campos w10" value="<?php echo $datos["aux_nombre"]; ?>"></td> 
    </tr>  
    </table> 
	
	<table class="table table-bordered table-condensed">    
	<tr>
		<th width="80">Dirección:</th>
		<td><input type="text" name="direccion" id="direccion" class="campos w10" value="<?php echo $datos["aux_direccion"]; ?>"></td>
	</tr>   
	<tr>
		<th>Comuna:</th>
		<td><input type="text" name="comuna" id="comuna" class="campos w10" value="<?php echo $datos["aux_comuna"]; ?>"></td>
	</tr>   
	<tr>
		<th>Telefono:</th>
		<td><input type="text" name="telefono" id="telefono" class="campos w10" value="<?php echo $datos["aux_telefono"]; ?>"></td>
	</tr>  
	<tr>
		<th>Correo:</th>
		<td><input type="text" name="correo" id="correo" class="campos w10" value="<?php echo $datos["aux_correo"]; ?>"></td>
	</tr> 
	</table> 
</td>
</table>  
<?php 
construir_boton("","","grabar","Guardar",3);
construir_boton("aux_listado.php","","eliminar","Cancelar",2);
?> 