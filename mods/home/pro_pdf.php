<?php
ob_start();
require("../../inc/conf_dentro.php");

$titulo_pdf = "Productos Codigos";
require('../../pdf/cabecera_barcode.php');

if($_REQUEST["fil_codigo"] <> ""){
	$filtros .= " and pro.pro_codigo like '".$_REQUEST["fil_codigo"]."' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and pro.pro_nombre like '".$_REQUEST["fil_descripcion"]."' ";
}
if($_REQUEST["fil_barra"] <> ""){
	$filtros .= " and pro.pro_codigo_barra like '".$_REQUEST["fil_barra"]."' ";
}
for($i = 1;$i <= 20;$i++){
	if($_REQUEST["fil_extra_".$i] <> ""){
		$filtros .= " and pro.pro_extra_".$i." like '".$_REQUEST["fil_extra_".$i]."' ";
	}
}
if($_REQUEST["fil_unidad"] <> ""){
	$filtros .= " and pro.pro_unidad = '".$_REQUEST["fil_unidad"]."' ";
}
if($_REQUEST["fil_grupo"] <> ""){
	$filtros .= " and pro.pro_grupo = '".$_REQUEST["fil_grupo"]."' ";
}
if($_REQUEST["fil_subgrupo"] <> ""){
	$filtros .= " and pro.pro_subgrupo = '".$_REQUEST["fil_subgrupo"]."' ";
}
if($_REQUEST["fil_categoria"] <> ""){
	$filtros .= " and pro.pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
}
if($_REQUEST["fil_tipo"] <> ""){
	$filtros .= " and pro.pro_tipo = '".$_REQUEST["fil_tipo"]."' ";
}

$res = sql_productos("*","   $filtros ORDER BY pro.pro_nombre asc"); 
if(mysqli_num_rows($res) > 0){
	while($row = mysqli_fetch_array($res)){
		
		if($row["pro_codigo_barra"] <> ""){
			
			$pdf->AddPage();
			
			$pdf->SetFont('Arial','',12);
			$pdf->Cell(0,7,$row["pro_codigo_barra"],0,1,'L');
			$pdf->SetFont('Arial','',17);
			$pdf->Cell(0,7,utf8_decode($row["pro_nombre"]),0,1,'L');
			
			$code = $row["pro_codigo_barra"];
			$pdf->Code128(10,30,$code,100,20);
		}
		
	}
}

$pdf->Output('pro_codigos.pdf','I');
$pdf->Close();
?>