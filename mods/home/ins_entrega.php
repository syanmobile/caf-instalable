<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Consumo de Insumos";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "ins_entrega.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>
<script language="javascript"> 
function save(){ 
	if(document.getElementById("fecha").value == ""){
		alerta_js("Es obligación ingresar la FECHA del documento");
		return;	
	} 
	if(document.getElementById("responsable").value == ""){
		alerta_js("Es obligación seleccionar al RESPONSABLE");
		return;	
	}
	cantidades_valores();
	if(valida_cantidades()){
		alerta_js("Ha indicado una cantidad invalida");
		return;	
	}
	if(confirm("Grabar Documento?")){
		carg("ins_save.php","");
	}
}
function cargar_productos(){
	if(document.getElementById("bodega").value == ""){ 
		$("#disponibles").html("");
	}else{
		var div = document.getElementById("disponibles"); 
		var a = $(".campos").fieldSerialize();  
		AJAXPOST("mods/home/ins_entrega_ajax.php",a+"&modo=disponibles",div);
	}
} 	 
</script>

<table style="margin-bottom:5px; width:100%">
<tr valign="top"> 
    <td width="400"> 
    	<input type="hidden" name="modo" value="entrega" class="campos" />
    	<input type="hidden" name="tipo" id="tipo" value="S" class="campos" />
    	<input type="hidden" name="concepto" value="6" class="campos"> 
    	
		<table class="table table-striped table-bordered table-condensed"> 
        <thead>
            <tr>
                <th colspan="4">Información Movimiento</th>
            </tr>
        </thead>
        <tbody> 
            <tr>
                <th style="text-align:right;">Fecha:</th>
                <td><input type="text" name="fecha" id="fecha" class="campos fecha" value="<? echo date("d/m/Y"); ?>"></td>
            </tr> 
		    <tr>
		        <th style="text-align:right;">Lugar</th>
		        <td>
		            <?php input_bodega('','cargar_productos()'); ?>
		        </td>
		    </tr>  
            <tr>
                <th width="100" style="text-align:right;">Responsable:</th> 
				<td>
				<select name="responsable" id="responsable" class="campos buscador">
				<option value=""></option>
				<?php 
				$res = sql_personas("*","  and per.per_tipo = 1 and per.per_elim = 0  ORDER BY per.per_nombre asc ");  
				if(mysqli_num_rows($res) > 0){
					while($row = mysqli_fetch_array($res)){
						?>
						<option value="<? echo $row["per_id"]; ?>"><?php echo $row["per_nombre"]; ?></option>
						<?
					}
				} 
				?>
				</select> 
				</td>   
			</tr>  
			<tr>
				<th style="text-align:right;">Centro Costo:</th>
				<td><?php input_centro_costo(''); ?></td>
			</tr>   
            <tr>
                <th style="text-align:right;">Notas:</th>
                <td><input type="text" name="glosa" class="campos w10" value="Entrega de Insumos"></td>
            </tr> 
            <?php 
			for($i = 1;$i <= 5;$i++){
				if(_opc("extra_mov_".$i) > 0){
					$res = sql_campos("*"," and cam_id = "._opc("extra_mov_".$i));  
					$row = mysqli_fetch_array($res); 
					?> 
					<tr> 
						<th style="text-align: right;"><? echo $row["cam_nombre"]; ?>:</th>
						<td><? 
			            campo_dinamico($row["cam_tipo"],"extra_".$i,$row["cam_opciones"],"",$url_base);
			            ?></td> 
					</tr> 
					<?php
				}
			}  
			?> 
        </tbody>
        </table>  
		<?php 
        construir_boton("","","grabar","Guardar Movimiento",3); 
        ?>
		<br><br>
   		<div id="operacion"></div>	
    </td> 
	<td style="padding-left:10px;" id="disponibles"> 
    </td>
</tr>
</table> 