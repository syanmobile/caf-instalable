<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Nuevo Proveedor"; 
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
$(document).ready(function() {
	$("#rut").Rut({
		format_on: 'keyup',
	   on_error: function(){
			alert('El rut ingresado es incorrecto');
			document.getElementById("rut").value = "";
	   }
	}); 
	$('#archivo').on('change',function(){ 
		$('#multiple_upload_form').ajaxForm({
			target:'#procesando_upload',
			data: { modo: "upload" },
			beforeSubmit:function(e){
				$('.uploading').show();
			},
			success:function(e){
				$('.uploading').hide();
				$("#imagen_ruta").attr("src","upload/<? echo $_SESSION["key_id"]; ?>/"+document.getElementById("imagen").value); 
			},
			error:function(e){
			}
		}).submit(); 
	});
});
function save(){  
	if(document.getElementById("codigo").value == ""){
		alerta_js("Es obligación ingresar el CODIGO");
		return;	
	} 
	if(document.getElementById("nombre").value == ""){
		alerta_js("Es obligación ingresar el NOMBRE");
		return;	
	}
	var div = document.getElementById("codigo_unico");
	AJAXPOST("mods/home/aux_ajax.php","&modo=codigo_unico&codigo="+document.getElementById("codigo").value,div);
} 
</script>

<input type="hidden" name="modo" value="crear" class="campos" />
<table width="100%">
<tr valign="top">
<td width="250">
    <img src="img/sin_imagen.jpg" id="imagen_ruta" class="img-thumbnail" style="width:250px !important;"> 
    <table class="table table-bordered" style="margin-top:10px;"> 
    <tbody>
        <tr>
            <td>
            <form method="post" name="multiple_upload_form" id="multiple_upload_form" enctype="multipart/form-data" action="<?php echo $url_web; ?>mods/home/aux_ajax.php"> 
                <input type="file" name="archivo" id="archivo" accept='image/*'>
                
                <div class="uploading none" style="display:none;"><img src="img/uploading.gif"/></div>
                <div id="eliminando"></div> 
                <div id="procesando_upload"></div>
            </form> 
            </td>
		</tr>
	</tbody>
    </table>
</td>
<td style="padding-left:10px;">
    <table class="table table-bordered table-condensed"> 
    <tr>
        <th width="80">Código:</th>
        <td>
        	<table width="100%">
            <tr>
            <td width="1"><input type="text" name="codigo" id="codigo" class="campos w10" style="text-align:center; width:130px !important;"></td>
            <td id="codigo_unico" style="padding-left:5px;"></td>
            </tr>
            </table>
        </td>
    </tr> 
    <tr>
        <th>RUT:</th>
        <td><input type="text" name="rut" id="rut" class="campos w10" style="text-align:center; width:130px !important;"></td>
    </tr>    
    <tr>
        <th>Nombre:</th>
        <td><input type="text" name="nombre" id="nombre" class="campos w10"></td> 
    </tr>  
    </table> 
    
    <table width="100%" style="margin-top:10px;">
    <tr valign="top">
        <td>
            <table class="table table-bordered table-condensed">    
            <tr>
                <th width="80">Dirección:</th>
        		<td><input type="text" name="direccion" id="direccion" class="campos w10"></td>
            </tr>   
            <tr>
                <th>Comuna:</th>
        		<td><input type="text" name="comuna" id="comuna" class="campos w10"></td>
            </tr>   
            <tr>
                <th>Telefono:</th>
        		<td><input type="text" name="telefono" id="telefono" class="campos w10"></td>
            </tr>  
            <tr>
                <th>Correo:</th>
        		<td><input type="text" name="correo" id="correo" class="campos w10"></td>
            </tr> 
            </table>
		</td>
    </tr>
    </table> 
</td>
</table>  
<?php 
construir_boton("","","grabar","Guardar",3);
construir_boton("aux_listado.php","","eliminar","Cancelar",2);
?> 