<?php 
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Editar Garantía";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$res3 = sql_garantias("*"," and gar.gar_id = '$_REQUEST[gar]' "); 
if(mysqli_num_rows($res3) > 0){  
	$row3 = mysqli_fetch_array($res3);
}else{
	?>
	<div class="alert alert-danger"> 
		<b>ERROR EN CONSULTA</b><br>No se encontró el dato
	</div>
    <?php
	exit();
}
?>
<script type="text/javascript">
$(document).ready(function(){
	input_auxiliar("<? echo $row3["gar_aux_id"]; ?>");
	detalle();
});
	
function save(){
	if(document.getElementById("lineas").value == ""){
		alerta_js("Es obligación seleccionar un ACTIVO");
		return;	
	} 
	if(document.getElementById("titulo").value == ""){
		alerta_js("Es obligación ingresar el TITULO");
		return;	
	} 
	if(confirm("Grabar Garantia?")){
		carg("gar_save.php","");
	}
} 
	
function detalle(){
	var div = document.getElementById("detalle"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/gar_ajax.php",a+"&modo=detalle",div);
}
function elegir(){
	var acf = document.getElementById("activo").value;
	var lin = document.getElementById("lineas").value;
	if(acf===""){
		return;
	}
	if(lin.indexOf(acf)<0){
		document.getElementById("lineas").value = document.getElementById("lineas").value + "," + acf; 
		detalle();
	}else{
		alert("el activo ya fue seleccionado");
	}
	
	
}
function quitar_linea(acf){
	var str = document.getElementById("lineas").value;
	document.getElementById("lineas").value = str.replace(","+acf, "");
	detalle();
}
	

	
//**************************************
function crear_auxiliar(){
	AJAXPOST(url_base+modulo_base+"aux_ajax.php","modo=crear_form",document.getElementById("modGeneral_lugar"),false,function(){			
		$('#modGeneral').modal('show'); 
	});
}
	function save_auxiliar(){  
	if(document.getElementById("aux_codigo").value == ""){
		document.getElementById("aux_codigo").focus();
		alert("Es obligación ingresar el CODIGO");
		return;	
	} 
	if(document.getElementById("aux_nombre").value == ""){
		document.getElementById("aux_nombre").focus();
		alert("Es obligación ingresar el NOMBRE");
		return;	
	}
	var a = $(".campos").fieldSerialize();  
	var div = document.getElementById("lugar_aux_save");
	AJAXPOST("mods/home/aux_ajax.php",a+"&modo=crear_rapido",div);
}
function input_auxiliar(codigo){
	AJAXPOST(url_base+modulo_base+"aux_ajax.php","modo=inicio_input&codigo="+codigo,document.getElementById("input_auxiliar"));
}
</script> 


<input type="hidden" name="modo" value="editar" class="campos">
<table width="100%" class="tabla_opciones">
<tbody> 
	<tr valign="top">
		<td width="40%">
			<table class="table table-bordered table-condensed"> 
			<thead>
			<tr>
				<th colspan="4">Detalles de la Garantía</th>
			</tr>
			</thead> 
			<tbody>
			<tr>
				<th width="110">Folio:</th>
				<td colspan="3"><input type="text" disabled value="Autom." style="text-align: center;">
				<input type="hidden" id="garid" name="garid" class="campos" value="<? echo $_REQUEST["gar"]; ?>"></td>
			</tr> 
			<tr>
				<th>Titulo:</th>
				<td colspan="3"><input type="text" name="titulo" id="titulo" class="campos w10" value="<? echo $row3["gar_titulo"]; ?>"></td>
			</tr> 
			<tr>
				<th>Notas:</th>
				<td colspan="3"><textarea name="notas" id="notas" class="campos w10" rows="4"><? echo $row3["gar_notas"]; ?></textarea></td>
			</tr>
			<tr>
				<th>Proveedor:</th>
				<td id="input_auxiliar" colspan="3"></td>        
			</tr> 
			<tr>
				<th>Póliza:</th>
				<td><input type="text" name="poliza" id="poliza" class="campos" style="float: left;" value="<? echo $row3["gar_poliza"]; ?>"></td>
				<th width="120">Nro Documento:</th>
	        	<td><input type="text" name="documento" id="documento" class="campos" style="float: left;" value="<? echo $row3["gar_documento"]; ?>"></td>
			</tr>   
			</tbody>
			</table>
		</td>
		<td width="380" rowspan="3" style="padding-left: 15px;"><table class="table table-bordered table-condensed">
		  <thead>
		    <tr>
		      <th colspan="2">Mantención a Activo</th>
	        </tr>
	      </thead>
		  <tbody>
		    <tr>
		      <th width="110">Activo Fijo:</th>
		      <td><select name="activo" id="activo" class="campos buscador">
		        <option value=""></option>
		        <? 
	$res = sql_activos_fijos("*"," ORDER BY acf.acf_producto asc, acf.acf_serie asc ");  
	if(mysqli_num_rows($res) > 0){
		while($row = mysqli_fetch_array($res)){
			?>
		        <option value="<? echo $row["acf_id"]; ?>" <? if($row["acf_id"] == $_REQUEST["acf"]){ echo "selected"; } ?>><?php echo $row["pro_nombre"]." (Cod: ".$row["pro_codigo"]." | Serie: ".$row["acf_serie"].")"; ?></option>
		        <?
		}
	} 
	?>
		        </select>
		        <? construir_boton("elegir()","1","crear","Crear",4);?></td>
	        </tr>
	      </tbody>
	    </table><?
			
			$res4 = sql_garantias_detalle("*"," and gar.gar_id = '$_REQUEST[gar]' "); 
			if(mysqli_num_rows($res4) > 0){  
				while($row4 = mysqli_fetch_array($res4)){
					if($row4["det_acf_id"]<>""){
						$idsact = ",".$row4["det_acf_id"].$idsact;
					}
				} 
			}
			?>
	    <div id="detalle" style="margin: 10px 0px;"></div>
		<input type="hidden" id="lineas" name="lineas" class="campos" value="<? echo $idsact; ?>"> 
	    </td>
	</tr>
	<tr valign="top"> 
	  <td><table class="table table-bordered table-condensed">
	    <thead>
	      <tr>
	        <th colspan="4">Información</th>
	        </tr>
	      </thead>
	    <tbody>
	      <tr>
	        <th width="120">Inicio:</th>
	        <td><input type="text" name="fecha_ini" id="fecha_ini" class="fecha campos" style="float: left;" value="<? echo _fec($row3["gar_fecha_ini"],5); ?>" ></td>
	        <th>Término:</th>
	        <td><input type="text" name="fecha_fin" id="fecha_fin" class="fecha campos" style="float: left;" value="<? echo _fec($row3["gar_fecha_fin"],5); ?>"></td>
	        </tr>

	      <tr>
	        <th>Costo:</th>
	        <td colspan="3"><input type="text" name="costo" id="costo" class="campos" style="float: left;" value="<? echo $row3["gar_costo"]; ?>"></td>
	        </tr>
	      <tr>
	        <th>Estado:</th>
	        <td><select name="estado" id="estado" class="campos">
	          <option value="ACT" <? if($row3["gar_estado"]=="ACT"){ echo "selected";}?>>Activa</option>
	          <option value="VEN" <? if($row3["gar_estado"]=="VEN"){ echo "selected";}?>>Vencida</option>
	          <option value="NUL" <? if($row3["gar_estado"]=="NUL"){ echo "selected";}?>>NULA</option>
	          </select></td>
	        </tr>
	      <tr>
	        <th>Responsable:</th>
	        <td colspan="3"><select name="responsable" id="responsable" class="campos buscador">
	          <option value=""></option>
	          <? 
				$res = sql_personas("*","  and per.per_tipo = 1 and per.per_elim = 0  ORDER BY per.per_nombre asc ");  
				if(mysqli_num_rows($res) > 0){
					while($row = mysqli_fetch_array($res)){
			?>
	          <option value="<? echo $row["per_id"]; ?>" 
	          <? if($row3["gar_per_id"]==$row["per_id"]){ echo "selected";}?>><?php echo $row["per_nombre"]; ?></option>
	          <?
					}
				} 
				?>
	          </select></td>
	        </tr>
	      </tbody>
      </table></td>
    </tr>
	<tr valign="top">
	  <td>
	    <table class="table table-bordered table-condensed">
	      <thead>
	        <tr>
	          <th colspan="2">Archivos Adjuntos</th>
            </tr>
          </thead>
	      <tbody>
	        <tr>
	          <th>Archivos:</th>
	          <td><?
					$imagen = ($row3["man_imagenXXXXXX"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$row3["man_imagenXXXX"];
					?>
	            <form method="post" name="multiple_upload_form" id="multiple_upload_form" enctype="multipart/form-data" action="<?php echo $url_web; ?>mods/home/acf_ajax_manXXXXXXXX.php">
	              <input type="file" name="archivo" id="archivo" accept='image/*'>
	              <div class="uploading none" style="display:none;"><img src="img/uploading.gif"/></div>
	              <div id="eliminando"></div>
	              <div id="procesando_upload">
	                <input type="hidden" name="imagen" value="<?php echo $row3["man_imagenXXXX"]; ?>" class="campos" />
                  </div>
                </form></td>
            </tr>
          </tbody>
        </table>
     
      <?php 
construir_boton("save()","","grabar","Guardar Mantenimiento",4);
construir_boton("gar_listado.php","","eliminar","Cancelar",2);
?>
      </td>
    </tr>
</tbody>
</table>








<script language="javascript">
$(document).ready(function(){
	$(".buscador").chosen({
		width:"95%",
		no_results_text:'Sin resultados con',
		allow_single_deselect:true 
	});
	$(".fecha").datepicker();
	
	$('#archivo').on('change',function(){ 
		$('#multiple_upload_form').ajaxForm({
			target:'#procesando_upload',
			data: { modo: "upload" },
			beforeSubmit:function(e){
				$('.uploading').show();
			},
			success:function(e){
				$('.uploading').hide();
				$("#imagen_ruta").attr("src","upload/<?php echo $_SESSION["key_id"]; ?>/"+document.getElementById("imagen").value); 
			},
			error:function(e){
			}
		}).submit(); 
	});
});
</script>