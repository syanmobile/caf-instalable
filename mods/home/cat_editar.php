<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Editar Clasificación";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

$res = sql_categorias("*"," and cat.cat_id = '$_REQUEST[id]'","","");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<strong>No se encontró el dato</strong>
	</div>
    <?php
	construir_boton("cat_listado.php","","buscar","Volver a Clasificación",2);
	exit();
}
?>
<script language="javascript">
function save(){
	if(document.getElementById("nombre").value == ""){
		alerta_js("Es obligación ingresar el nombre");
		return;	
	}
	carg("cat_save.php","");
}
</script> 

<form class="form-horizontal" role="form" method="post">
	<input type="hidden" name="modo" value="editar" class="campos"> 
	<input type="hidden" name="id" value="<? echo $_REQUEST["id"]; ?>" class="campos"> 
	
	<table class="table  table-bordered"> 
	<thead>
		<tr>
			<th colspan="2">Información Clasificación</th>
		</tr> 
	</thead> 
	<tbody>
		<tr>
			<th width="120" style="text-align:right;">Código:</th>
			<td><input type="text" class="campos" id="codigo" name="codigo" value="<?php
			echo $datos["cat_codigo"]; ?>" readonly></td>
		</tr>
		<tr>
			<th style="text-align:right;">Nombre:</th>
			<td><input type="text" class="campos w10" id="nombre" name="nombre" value="<?php
			echo $datos["cat_nombre"]; ?>"></td>
		</tr>  
	</tbody>
	</table> 
	<?php
	construir_boton("","","grabar","Guardar",3);
	construir_boton("cat_listado.php","","eliminar","Cancelar",2);
	?>  
</form> 