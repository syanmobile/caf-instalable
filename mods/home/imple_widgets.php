<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Widgets";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

$inicio_widget = explode(";",_opc("inicio")); 
$widget_orden = explode(",",$inicio_widget[0]);
$widget_activo = explode(",",$inicio_widget[1]);
$widget_ancho = explode(",",$inicio_widget[2]);
?> 
<script type="text/javascript">
function administrar(){
	AJAXPOST("mods/home/inicio_ajax.php","modo=inicio",document.getElementById("modGeneral_lugar"),false,function(){            
        $('#modGeneral').modal('show');
        $(".tabledrag3").tableDnD({
            onDragClass:"draging",
            onDrop:function(){
                var valores = $.tableDnD.serialize();
				AJAXPOST("mods/home/inicio_ajax.php","&modo=orden&"+valores,document.getElementById("ordencito"));
            }
       });
    });
} 
function grabar_widgets(){
	var a = $(".we").fieldSerialize();
	AJAXPOST("mods/home/inicio_ajax.php",a+"&modo=organizar",document.getElementById("lugarci"));
}
$(document).ready(function(){
	$(".tabledrag3").tableDnD({
        onDragClass:"draging",
        onDrop:function(){
            var valores = $.tableDnD.serialize();
			AJAXPOST("mods/home/inicio_ajax.php","&modo=orden&"+valores,document.getElementById("ordencito"));
        }
    });
});
</script> 

<div id="lugarci"></div>
<table class="table table-bordered tabledrag3 table-condensed" id="tbl_modulos2"> 
<thead>
<tr style="cursor: move;">  
	<th width="1" align="center">&nbsp;</th> 
	<th>Widget</th>
	<th>Ancho</th> 
</tr>
</thead>
<tbody> 
<?
for($i = 0;$i < count($widgets);$i++){
	if($widget_orden[$i] == ""){
		$id = $i;
	}else{
		$id = $widget_orden[$i];
	}
	?>
	<tr fila="<?php echo $id; ?>" id="<?php echo $id; ?>"> 
		<td><input type="checkbox" name="we_<? echo $id; ?>" value="1" <? if($widget_activo[$i]){ echo "checked"; } ?> class="we"></td>
		<td style="display: block;">
			<b><? echo $widgets[$id][0]; ?></b><br>
			<? echo $widgets[$id][1]; ?>
		</td>
		<td><select name="wa_<? echo $id; ?>" value="1" class="we">
			<?
			for($ii = 1;$ii <= 12;$ii++){
				?><option value="<? echo $ii; ?>" <? if($widget_ancho[$i] == $ii){ echo "selected"; } ?>><? echo $ii; 
				if($widgets[$id][2] == $ii){ echo " - Sugerido"; } ?></option><?
			}
			?> 
		</select></td>
	</tr>
	<?
	if($orden <> ""){ $orden .= ","; }
	$orden .= $id;
}
?>
</tbody>
</table>
<div id="ordencito">
	<input type="hidden" id="valor_widget" name="valor_widget" class="we" value="<? echo $orden; ?>">
</div>

<table width="100%">
<tr>
	<td width="1"><?  construir_boton("grabar_widgets()","","grabar","Guardar",4); ?></td>
	<td><?  construir_boton("location.href='index.php'","","refrescar","Refrescar Inicio",4); ?></td>
</tr>
</table> 