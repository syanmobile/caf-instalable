<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Nuevo Insumo"; 
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

if($_REQUEST["id"] <> ""){
	$res = sql_productos("*","and pro.pro_id = '$_REQUEST[id]'","");  
	if(mysqli_num_rows($res) > 0){
		$datos = mysqli_fetch_array($res);
	}
}
?>
<script language="javascript">
$(document).ready(function() {
	$('#archivo').on('change',function(){ 
		$('#multiple_upload_form').ajaxForm({
			target:'#procesando_upload',
			data: { modo: "upload" },
			beforeSubmit:function(e){
				$('.uploading').show();
			},
			success:function(e){
				$('.uploading').hide();
				$("#imagen_ruta").attr("src","upload/<? echo $_SESSION["key_id"]; ?>/"+document.getElementById("imagen").value); 
			},
			error:function(e){
			}
		}).submit(); 
	});
	campos_adicionales_set(); 
});
function save(){  
	if(document.getElementById("codigo").value == ""){
		alerta_js("Es obligación ingresar el CODIGO");
		return;	
	} 
	if(document.getElementById("nombre").value == ""){
		alerta_js("Es obligación ingresar el NOMBRE");
		return;	
	} 
	var div = document.getElementById("codigo_unico");
	AJAXPOST("mods/home/ins_ajax.php","&modo=codigo_unico&codigo="+document.getElementById("codigo").value,div);
} 
function subgrupo_opciones(valor){
	var div = document.getElementById("subgrupo");
	AJAXPOST("mods/home/sgru_ajax.php","modo=opciones&grupo="+valor,div);
} 
/***********************************************************/
function agregar_campo(){
	if(document.getElementById("campo_adicional").value == ""){
		alerta_js("Seleccione un campo personalizado");
		return;
	}else{
		document.getElementById("campos_adicionales").value = document.getElementById("campos_adicionales").value + ";;;" + document.getElementById("campo_adicional").value;
		campos_adicionales_set();
	}   	
} 
function eliminar_campo(id){ 
	var a = $(".campos").fieldSerialize();
	var div = document.getElementById("campos_adicionales_lugar");
	AJAXPOST("mods/home/cam_ajax.php",a+"&modo=campos_adicionales&elim_id="+id,div);
}
function campos_adicionales_set(){   
	var a = $(".campos").fieldSerialize();
	var div = document.getElementById("campos_adicionales_lugar");
	AJAXPOST("mods/home/cam_ajax.php",a+"&modo=campos_adicionales",div);
}
/***********************************************************/ 
</script>

<input type="hidden" name="modo" value="crear" class="campos" />
<input type="hidden" name="tipo_activo" value="INS" class="campos" />

<table width="100%">
<tr valign="top"> 
<td width="250">  
    <table class="table table-bordered table-condensed">
    <thead>
    	<tr>
    		<th colspan="2">IMAGEN</th>
    	</tr>    
	</thead>  
    <tbody>
        <tr>
            <td>
            <?
			$imagen = ($datos["pro_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$datos["pro_imagen"];
			?>
			<img src="<?php echo $imagen; ?>" id="imagen_ruta" class="img-thumbnail" style="width:250px !important;">
			<form method="post" name="multiple_upload_form" id="multiple_upload_form" enctype="multipart/form-data" action="<?php echo $url_web; ?>mods/home/pro_ajax.php"> 
                <input type="file" name="archivo" id="archivo" accept='image/*'>
                
                <div class="uploading none" style="display:none;"><img src="img/uploading.gif"/></div>
                <div id="eliminando"></div> 
                <div id="procesando_upload">
                    <input type="hidden" name="imagen" value="<?php echo $datos["pro_imagen"]; ?>" class="campos" />
                </div>
            </form> 
            </td>
		</tr>
	</tbody>
    </table>
</td>
<td style="padding-left:10px;" width="600">
    <table class="table table-bordered table-condensed">
    <thead>
    	<tr>
    		<th colspan="2">FICHA PRINCIPAL</th>
    	</tr>    
	</thead>
	<tbody>
		<tr>
			<th width="130" style="text-align:right;">Código:</th>
			<td>
				<table width="100%">
				<tr>
				<td width="1"><input type="text" name="codigo" id="codigo" class="campos w10" style="text-align:center; width:130px !important;"></td>
				<td id="codigo_unico" style="padding-left:5px;"></td>
				</tr>
				</table>
			</td>
		</tr> 
		<tr>
			<th style="text-align:right;">Nombre:</th>
			<td><input type="text" name="nombre" id="nombre" class="campos w10" value="<?php echo $datos["pro_nombre"]; ?>"></td> 
		</tr>    
		<tr>
			<th style="text-align:right;">Cod.Barra:</th>
			<td><input type="text" name="codigo_barra" id="codigo_barra" class="campos" value="<?php echo $datos["pro_codigo_barra"]; ?>"></td> 
		</tr>
		<tr>
			<th style="text-align:right;">Clasificación:</th>
			<td><?php input_clasificacion($datos["pro_categoria"]); ?></td>
		</tr>  
		<tr>
			<th style="text-align:right;">Unidad:</th>
			<td><?php input_unidad($datos["pro_unidad"]); ?></td>
		</tr> 
		<tr>
			<th style="text-align:right;">Stock Mínimo:</th>
			<td><input type="text" name="stock_minimo" id="stock_minimo" class="campos" style="width:70px; text-align:center" value="<?php echo $datos["pro_stock_minimo"]; ?>"></td>
		</tr> 
		<tr>
			<th style="text-align:right;">Stock Máximo:</th>
			<td><input type="text" name="stock_maximo" id="stock_maximo" class="campos" style="width:70px; text-align:center" value="<?php echo $datos["pro_stock_maximo"]; ?>"></td>
		</tr> 
    </tbody>
	</table>    
    <br> 
	<?php 
	construir_boton("","","grabar","Guardar Producto",3);
	construir_boton("ins_listado.php","","eliminar","Cancelar",2);
	?> 
</td>
<td style="padding-left:10px;">

	<table class="table table-bordered table-condensed"> 
		<thead>
			<tr>
				<th colspan="2">CAMPOS ADICIONALES</th>
			</tr>
		</thead>    
		<tbody> 
			<?
			for($iex = 1;$iex <= 10;$iex++){				 
	            $res = sql_campos("*"," and cam_producto = $iex order by cam_nombre asc");  
				if(mysqli_num_rows($res) > 0){
					while($row = mysqli_fetch_array($res)){
						$infor = "";

						?> 
						<tr> 
							<th width="130" style="text-align: right;"><? echo $row["cam_nombre"]; ?>:</th>
							<td><? 
				            campo_dinamico($row["cam_tipo"],"extra_".$iex,$row["cam_opciones"],$infor,$url_base);
				            ?></td> 
						</tr>
						<? 
					}
				}
			} 
			?> 


		</tbody>
	</table>

	<table class="table table-bordered table-condensed"> 
	<tbody> 
		<tr>
			<td>
			<b>Descripción del Insumo:</b><br>
			<textarea name="descripcion" class="campos" rows="8" style="width: 90%;"><?php 
			echo $datos["pro_descripcion"]; ?></textarea>
			</td>
		</tr> 
	</tbody>
	</table>
</td>
</table>  