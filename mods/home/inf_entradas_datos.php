<?php 
$fechaInicio = strtotime(str_replace("/","-",$_REQUEST["desde"])) ;
$fechaFin = strtotime(str_replace("/","-",$_REQUEST["hasta"])) ;

if($_REQUEST["bodega"] <> ""){
	$filtros .= " and mov.mov_bodega = '$_REQUEST[bodega]' ";
}
if($_REQUEST["concepto"] <> ""){
	$filtros .= " and mov.mov_concepto = '$_REQUEST[concepto]' ";
}
if($_REQUEST["auxiliar"] <> ""){
	$filtros .= " and mov.mov_auxiliar = '$_REQUEST[auxiliar]' ";
}

// ---------------------------------------------------------------------------------------------------
if($_REQUEST["fil_codigo"] <> ""){
	$filtros .= " and pro.pro_codigo like '%".$_REQUEST["fil_codigo"]."%' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and pro.pro_nombre like '%".$_REQUEST["fil_descripcion"]."%' ";
}
if($_REQUEST["fil_serie"] <> ""){
	$filtros .= " and det.det_lote = '".$_REQUEST["fil_serie"]."' ";
}
if($_REQUEST["fil_barra"] <> ""){
	$filtros .= " and pro.pro_codigo_barra like '%".$_REQUEST["fil_barra"]."%' ";
}
for($i = 1;$i <= 20;$i++){
	if($_REQUEST["fil_extra_".$i] <> ""){
		$filtros .= " and pro.pro_extra_".$i." like '%".$_REQUEST["fil_extra_".$i]."%' ";
	}
}
if($_REQUEST["fil_unidad"] <> ""){
	$filtros .= " and pro.pro_unidad = '".$_REQUEST["fil_unidad"]."' ";
}

/*
if($_REQUEST["fil_categoria"] <> ""){
	$filtros .= " and pro.pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
}
*/
if($_REQUEST["fil_categoria"] <> ""){
	$filtros .= " and cat.cat_codigo like '".$_REQUEST["fil_categoria"]."%' ";
}

if($_REQUEST["fil_tipo"] <> ""){
	$filtros .= " and pro.pro_tipo = '".$_REQUEST["fil_tipo"]."' ";
}
// ---------------------------------------------------------------------------------------------------
$productos = array();
?>
<table width="100%">
	<tr valign="top">
		<td>
			<?
    		//construir_boton("excel()","","importar","Excel",4);
			?>
			<table class="table table-bordered table-hover table-condensed"> 
			<thead>
				<tr>   
					<th style="text-align:center;" width="1">Folio</th>
					<th style="text-align:center;" width="1">Fecha</th> 
					<th>Concepto</th>
					<th>Proveedor</th> 
					<th>Responsable</th>  
					<th>Codigo</th>
					<th>Producto</th>
					<th>Serie</th>
					<th>Notas</th>
					<th style="text-align:center;" width="1">Cant</th>
					<th style="text-align:center;" width="1">Valor</th>
					<th style="text-align:center;" width="1">Subtotal</th>
				</tr>
			</thead>
			<tbody>
			<?
			for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
				$dia[] = date("d/m", $i);

				$res = sql_movimientos_detalle_clasif("*"," 
				and mov.mov_tipo = 'E' and det.det_ingreso > 0 and mov.mov_fecha = '".date("Y-m-d", $i)."' $filtros 
				order by mov.mov_fecha asc, mov.mov_folio asc "); 
				if(mysqli_num_rows($res) > 0){ 
					while($row = mysqli_fetch_array($res)){ 
						?>
						<tr> 
							<td style="text-align:center;"><?php echo $row["mov_folio"]; ?></td>
							<td style="text-align:center;"><?php echo _fec($row["mov_fecha"],5); ?></td> 
							<td><?php echo $_concepto_ing[$row["mov_concepto"]]; ?></td>
							<td><?php echo $row["aux_nombre"]; ?></td>
							<td><?php echo $_personas[$row["mov_responsable"]];	?></td>  
							<td width="1"><?php echo $row["pro_codigo"]; ?></td>
							<td><?php echo $row["pro_nombre"]; ?></td>
							<td><?php echo $row["det_lote"]; ?></td>
							<td><?php if($row["det_notas"] <> ""){ echo $row["det_notas"]; }else{ echo $row["mov_glosa"]; } ?></td>
							<td style="text-align:center;"><?php echo _num($row["det_ingreso"]); ?></td>
							<td style="text-align:right;"><?php echo _num($row["det_valor"]); ?></td>
							<td style="text-align:right;"><?php echo _num($row["det_total"]); ?></td>
						</tr>
						<?
						if(!in_array($row["pro_codigo"],$productos)){
							$productos[] = $row["pro_codigo"];
						} 
						$cantidades[$row["pro_codigo"]] += $row["det_ingreso"];
						$descripcion[$row["pro_codigo"]] = $row["pro_nombre"];
						
						$total += $row["det_ingreso"];
						$total_inversion += $row["det_total"];
					} 
				}
			}
			?> 
			</tbody>
			</table>
		</td>
		<td style="padding-left: 20px;">
			<?
    		//construir_boton("excel2()","","importar","Excel",4);
			?>
			<div class="alert alert-warning" style="color: #666666 !important; margin-bottom: 5px;">
				<table width="100%">
					<tr valign="top">
						<td width="1"><span class="glyphicon glyphicon-lock" aria-hidden="true" style="font-size:40px; float:left; margin-right:10px;"></span></td>
						<td style="font-size: 13px;">
							Unidades: <b><?php echo _num($total); ?></b><br>
							Inversión: <b><?php echo _num($total_inversion); ?></b>
						</td>
					</tr>
				</table>
			</div>
			
			<table class="table table-bordered table-hover table-condensed"> 
			<thead>
				<tr>    
					<th>Codigo</th>
					<th>Producto</th>
					<th style="text-align:right;" width="1">Cantidad</th>
				</tr>
			</thead>
			<tbody>
			<?
			foreach($productos as $pro){ 
				?>
				<tr>  
					<td width="1"><?php echo $pro; ?></td>
					<td><?php echo $descripcion[$pro]; ?></td>
					<td style="text-align:center;"><?php echo $cantidades[$pro]; ?></td>
				</tr>
				<?  
			}
			?> 
			</tbody> 
			<tfoot>
				<tr>
					<th colspan="2">TOTAL ITEMS</th>
					<th style="text-align:center;"><? echo $total; ?></th>
				</tr>
			</tfoot>
			</table>
		</td>
	</tr>
</table>