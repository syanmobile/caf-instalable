<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Rutinas Internas";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script type="text/javascript">
function cargar_rutina(pagina){
    var a = $(".campos").fieldSerialize();
    var div = document.getElementById("lugar_recep");
    AJAXPOST("mods/home/"+pagina,a,div);
}
</script> 
<table width="100%">
<tr valign="top">
    <td width="350">
        <table class="table table-bordered table-hover table-condensed">
        <thead>
            <tr>
                <th colspan="2">Ejecute la rutina que necesite</th>
            </tr>
        </thead>
        <tbody>    
        <tr>
            <td>
                <b>Resumen totalizados</b><br>
                Rutina 5</td>
            <td width="1"><?php construir_boton("cargar_rutina('rutina_script_5.php');","","derecha","Ejecutar",4); ?></td>
        </tr>
        <tr>
            <td>
                <b>Actualizar los activos con responsables</b><br>
                Rutina 6</td>
            <td width="1"><?php construir_boton("cargar_rutina('rutina_script_6.php');","","derecha","Ejecutar",4); ?></td>
        </tr>  
        </tbody>
        </table>
    </td> 
    <td style="padding-left: 15px;">
        <div id="lugar_recep"></div>
    </td>
</tr>
</table>
