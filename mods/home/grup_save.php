<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabación Grupos";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

switch($_POST["modo"]){
	case "crear":
		$res = sql_grupos("*"," and gru.gru_codigo = '$_REQUEST[codigo]'");
		if(mysqli_num_rows($res) > 0){
			?>
            <div class="alert alert-danger"> 
               <b>ERROR</b>, código ya existe: '<strong><? echo $_POST["codigo"]; ?></strong>'
            </div>
            <?
		}else{ 
			$sql = "INSERT INTO grupos ( 
				gru_codigo,
				gru_nombre 
			) VALUES ( 
				'$_POST[codigo]',
				'$_POST[nombre]' 
			)";
			$res = mysqli_query($cnx,$sql);
			$_POST["id"] = mysqli_insert_id($cnx);
			?>
			<div class="alert alert-success"> 
				<strong>Grupo '<? echo $_POST["nombre"]; ?>' creado con &eacute;xito</strong>
			</div>
			<?php 
		}
		break;
		
	case "editar":
		$SQL_ = "UPDATE grupos SET 
			gru_nombre = '$_POST[nombre]' 
		WHERE gru_id = '$_POST[id]' ";   
		$res = mysqli_query($cnx,$SQL_);
		?>   
        <div class="alert alert-success"> 
           Grupo <strong>'<? echo $_POST["nombre"]; ?>'</strong> editado con &eacute;xito
        </div>  
		<?php  
		break;
}

construir_boton("grup_listado.php","","buscar","Listado Grupos",2);
if($_REQUEST["modo"] <> "eliminar"){
	construir_boton("grup_editar.php","&id=".$_POST["id"],"editar","Editar este Grupo",2);
}
construir_boton("grup_nuevo.php","","crear","Crear otro Grupo ",2);
?>