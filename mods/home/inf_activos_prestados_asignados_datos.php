<?php
if($_REQUEST["fil_codigo2"] <> ""){
    $filtros .= " and acf_codigo like '".$_REQUEST["fil_codigo2"]."' ";
}
if($_REQUEST["fil_codigo"] <> ""){
    $filtros .= " and pro_codigo like '".$_REQUEST["fil_codigo"]."' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
    $filtros .= " and pro_nombre like '".$_REQUEST["fil_descripcion"]."' ";
}  
if($_REQUEST["fil_categoria"] <> ""){
    $filtros .= " and pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
} 
if($_REQUEST["fil_ubicacion"] <> ""){
    $filtros .= " and acf_ubicacion = '".$_REQUEST["fil_ubicacion"]."' ";
}  
if($_REQUEST["fil_responsable"] <> ""){
    $filtros .= " and acf_responsable = '".$_REQUEST["fil_responsable"]."' ";
}

$res = sql_activos_fijos("*"," and acf_disponible = 2 and pro_tipo <> 'INS' $filtros 
    order by acf_fecha_pres_asig_compromiso desc, pro_nombre asc","",""); 
if(mysqli_num_rows($res) > 0){ 
    ?>
    <table width="100%">
    <tr valign="top">
    <td>
        <table class="table table-striped table-bordered tabledrag table-condensed" border="1"> 
        <thead> 
            <tr>
                <th width="1"></th>
                <th width="1">Cod.Activo</th>
                <th width="1">Cod.Producto</th> 
                <th>Producto</th>
                <th width="100" style="text-align: center;">Fecha Entrega</th>
                <th>Responsable</th>
                <th width="60">Estado</th>
                <th width="130">Situaci&oacute;n</th>
            </tr> 
        </thead>
        <tbody> 
        <?
        while($row = mysqli_fetch_array($res)){ 
            $prestamos++;  
            ?> 
            <tr> 
                <td><?
                construir_boton("acf_info.php","&acf=".$row["acf_id"],"buscar","Ver Activo Fijo"); 
                ?></td> 
                <th><? echo $row["acf_codigo"]; ?></th>
                <th><? echo $row["pro_codigo"]; ?></th>
                <td><? echo $row["pro_nombre"]; ?></td> 
                <td style="text-align: center;"><?php echo _fec($row["acf_fecha_pres_asig_compromiso"],5); ?></td>
                <td><? echo $_personas[$row["acf_responsable"]]; ?></td>
                <?
                $dias = dias_transcurridos($row["acf_fecha_pres_asig_compromiso"],date("Y-m-d")); 
                if($dias < 0){
                    ?>
                    <td><span class="label label-danger">ATRASADO</span></td>
                    <td><? echo ($dias * -1); ?> dias de atraso</td>
                    <?
                    $prestamo_vencida++;
                }else{
                    ?>
                    <td><span class="label label-success">AL DIA</span></td>
                    <td><? echo $dias; ?> dias para entregar</td>
                    <?
                    $prestamo_aldia++; 
                } 
                ?>
            </tr>
            <? 
        }
        ?>
        </tbody>
        </table>
    </td>
    <td style="padding-left:20px;" width="300">  
        <div class="alert alert-warning" style="color: #666666 !important;">
            <table width="100%">
                <tr valign="top"> 
                    <td style="font-size: 13px;">
                        AL DIA: <b><?php echo _num2($prestamo_aldia); 
                        echo "/";
                        echo _num2($prestamos); 

                        $porcen = round(_porc($prestamo_aldia,$prestamos));
                        ?></b><br> 
                        <div class="progress">
                          <div class="progress-bar bg-success" role="progressbar" aria-valuenow="<? echo $porcen; 
                          ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php
                          echo $porcen; ?>%;">
                            <? echo $porcen; ?>%
                          </div>
                        </div>  

                        ATRASADAS: <b><?php echo _num2($prestamo_vencida); 
                        echo "/";
                        echo _num2($prestamos); 

                        $porcen = round(_porc($prestamo_vencida,$prestamos));
                        ?></b><br> 
                        <div class="progress">
                          <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="<? echo $porcen; 
                          ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php
                          echo $porcen; ?>%;">
                            <? echo $porcen; ?>%
                          </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div> 
    </td>
    </tr>
    </table>
    <?
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin activos fijos prestados</strong>
    </div>
    <?php 
} 

$res = sql_activos_fijos("*"," and acf_disponible = 6 and pro_tipo <> 'INS' $filtros 
order by acf_fecha_pres_asig_compromiso desc, pro_nombre asc"); 
if(mysqli_num_rows($res) > 0){ 
    ?>
    <table width="100%">
    <tr valign="top">
    <td>
        <table class="table table-striped table-bordered tabledrag table-condensed" border="1"> 
        <thead>
        <tr>
            <th width="1"></th>
            <th width="1">Cod.Activo</th>
            <th width="1">Cod.Producto</th>  
            <th>Producto</th> 
            <th width="100" style="text-align: center;">Asignado el</th>
            <th>Responsable</th>
            <th width="60">Estado</th>
            <th width="130">Situaci&oacute;n</th>
        </tr> 
        </thead>
        <tbody> 
        <?
        while($row = mysqli_fetch_array($res)){ 
            $asignados++;
            ?>
            <tr>
                <td><?
                construir_boton("acf_info.php","&acf=".$row["acf_id"],"buscar","Ver Activo Fijo"); 
                ?></td>
                <th><? echo $row["acf_codigo"]; ?></th> 
                <td><? echo $row["pro_codigo"]; ?></td> 
                <td><? echo $row["pro_nombre"]; ?></td> 
                <td style="text-align: center;"><?php echo _fec($row["acf_fecha_pres_asig"],5); ?></td>
                <?
                $dias = dias_transcurridos(date("Y-m-d"),$row["acf_fecha_pres_asig"]); 
                ?>
                <td><? echo $_personas[$row["acf_responsable"]]; ?></td>
                <td><span class="label label-info">ASIGNADO</span></td>
                <td><? echo $dias; ?> dias asignado</td>
            </tr>
            <? 
        }
        ?>   
        </tbody>
        </table>  
    </td>
    <td style="padding-left:20px;" width="300">  
        <div class="alert alert-warning" style="color: #666666 !important;">
            <table width="100%">
                <tr valign="top"> 
                    <td style="font-size: 13px;">
                        ASIGNADOS: <b><?php echo _num2($asignados); 
                        ?></b>
                    </td>
                </tr>
            </table>
        </div> 
    </td>
    </tr>
    </table>
    <?
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin activos fijos asignados</strong>
    </div>
    <?php 
} 
?>