<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabación";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

switch($_POST["modo"]){
	case "crear":
		$res = mysqli_query($cnx,"select * from activos_tipos where tac_codigo = '$_REQUEST[codigo]' ");
		if(mysqli_num_rows($res) > 0){
			?>
            <div class="alert alert-danger"> 
               <b>ERROR</b>, código ya existe: '<strong><? echo $_POST["codigo"]; ?></strong>'
            </div>
            <?
		}else{ 
			$sql = "INSERT INTO activos_tipos ( 
				tac_codigo,
				tac_nombre,
				tac_icono,
				tac_ejemplo,
				tac_amortizable,
				tac_depreciable,
				tac_revalorizable,
				tac_readecuable,
				tac_activo,
				tac_orden
			) VALUES ( 
				'$_POST[codigo]',
				'$_POST[nombre]' ,
				'$_POST[icono]',
				'$_POST[ejemplo]',
				'$_POST[amortizable]',
				'$_POST[depreciable]',
				'$_POST[revalorizable]',
				'$_POST[readecuable]',
				'$_POST[activo]',
				'$_POST[orden]'
			)";
			$res = mysqli_query($cnx,$sql);
			$_POST["id"] = mysqli_insert_id($cnx);
			?>
			<div class="alert alert-success"> 
			
				<strong>Creado con &eacute;xito</strong>
			</div>
			<?php 
		}    
		break;
		
	case "editar":
		$SQL_ = "UPDATE activos_tipos SET "; 
		$SQL_.= "tac_codigo = '$_POST[codigo]', ";
		$SQL_.= "tac_nombre = '$_POST[nombre]', ";
		$SQL_.= "tac_icono = '$_POST[icono]', ";
		$SQL_.= "tac_ejemplo = '$_POST[ejemplo]', ";
		$SQL_.= "tac_amortizable = '$_POST[amortizable]', ";
		$SQL_.= "tac_depreciable = '$_POST[depreciable]', ";
		$SQL_.= "tac_revalorizable = '$_POST[revalorizable]', ";
		$SQL_.= "tac_readecuable = '$_POST[readecuable]', ";
		$SQL_.= "tac_activo = '$_POST[activo]', ";
		$SQL_.= "tac_orden = '$_POST[orden]'  ";
		$SQL_.= "WHERE tac_id = '$_POST[id]' ";  
		$res = mysqli_query($cnx,$SQL_);
		?>   
        <div class="alert alert-success"> 
            <strong>Editado con &eacute;xito</strong> 
        </div>  
		<?php  
		break; 
		
	case "reordenar":
		$i = 1;
		foreach($_POST['tbl_modulos'] as $id){
			if(trim($id) != ""){
				mysqli_query($cnx,"UPDATE activos_tipos SET tac_orden = $i WHERE tac_id = $id");
				$i++;
			} 
		}
		break;
}

construir_boton("imple_tac.php","","izquierda","Volver",2);
?>