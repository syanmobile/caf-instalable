<?php 
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition:  filename=\"inf_entradas_detallado.xls\";");

require("../../inc/conf_dentro.php");

$fechaInicio = strtotime(str_replace("/","-",$_REQUEST["desde"])) ;
$fechaFin = strtotime(str_replace("/","-",$_REQUEST["hasta"])) ;

if($_REQUEST["bodega"] <> ""){
	$filtros .= " and mov.mov_bodega = '$_REQUEST[bodega]' ";
}
if($_REQUEST["concepto"] <> ""){
	$filtros .= " and mov.mov_concepto = '$_REQUEST[concepto]' ";
}
if($_REQUEST["auxiliar"] <> ""){
	$filtros .= " and mov.mov_auxiliar = '$_REQUEST[auxiliar]' ";
}

// ---------------------------------------------------------------------------------------------------
if($_REQUEST["fil_codigo"] <> ""){
	$filtros .= " and pro.pro_codigo like '%".$_REQUEST["fil_codigo"]."%' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and pro.pro_nombre like '%".$_REQUEST["fil_descripcion"]."%' "; 
}
if($_REQUEST["fil_barra"] <> ""){
	$filtros .= " and pro.pro_codigo_barra like '%".$_REQUEST["fil_barra"]."%' ";
}
for($i = 1;$i <= 20;$i++){
	if($_REQUEST["fil_extra_".$i] <> ""){
		$filtros .= " and pro.pro_extra_".$i." like '%".$_REQUEST["fil_extra_".$i]."%' ";
	}
}
if($_REQUEST["fil_unidad"] <> ""){
	$filtros .= " and pro.pro_unidad = '".$_REQUEST["fil_unidad"]."' ";
}

if($_REQUEST["fil_categoria"] <> ""){
	$filtros .= " and pro.pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
}
if($_REQUEST["fil_tipo"] <> ""){
	$filtros .= " and pro.pro_tipo = '".$_REQUEST["fil_tipo"]."' ";
}
// ---------------------------------------------------------------------------------------------------
$productos = array();
?>  
<table border="1" cellpadding="2" cellspacing="0"> 
<thead> 
	<tr>   
		<th>Folio</th>
		<th>Fecha</th>
		<th>Lugar</th>
		<th>Concepto</th>
		<th>Proveedor/Responsable</th>   
		<th>Cantidad</th>
		<th colspan="2">Producto</th> 
		<th>Lote/Serie</th> 
		<th>Notas</th>
	</tr> 
</thead>
<tbody>
<?
for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
	$dia[] = date("d/m", $i);

	$res = sql_movimientos_detalle("*"," 
	and con.con_tipo = 'E' and det.det_ingreso > 0 and mov.mov_fecha = '".date("Ymd", $i)."' $filtros  
	order by mov.mov_fecha asc, mov.mov_folio asc "); 
	if(mysqli_num_rows($res) > 0){ 
		while($row = mysqli_fetch_array($res)){  
			?>
			<tr> 
				<td style="text-align:center;"><?php echo $row["mov_folio"]; ?></td>
				<td style="text-align:center;"><?php echo _fec($row["mov_fecha"],5); ?></td>
				<td style="text-align:center;"><?php echo $row["mov_bodega"]; ?></td>
				<td><?php echo $row["con_nombre"]; ?></td>
				<td><?php echo $row["aux_rut"]; ?></td>
				<td><?php echo utf8_decode($row["aux_nombre"]); ?></td>
				<td style="text-align:center;"><?php echo $row["det_ingreso"]; ?></td>
				<td><?php echo $row["pro_codigo"]; ?></td>
				<td><?php echo utf8_decode($row["pro_nombre"]); ?></td>
				<td><?php echo utf8_decode($row["det_lote"]); ?></td>
				<td><?php if($row["det_notas"] <> ""){ echo utf8_decode($row["det_notas"]); }else{ echo utf8_decode($row["mov_glosa"]); } ?></td>
			</tr>
			<?
		} 
	}
}
?> 
</tbody> 
</table> 