<?php
require("../../inc/conf_dentro.php");

$res = sql_productos("*","and pro.pro_id = '$_REQUEST[id]'",""); 
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<b>ERROR EN CONSULTA</b><br>No se encontró el Producto con id <strong>"<? echo $_REQUEST["id"]; ?>"</strong>
	</div>
    <?php
	exit();
}
//----------------------------------------------------------------------------------------
$titulo_pagina = "Ficha Producto: ".$datos["pro_nombre"]; 
construir_breadcrumb($titulo_pagina); 
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function cambiar(){
	carg("pro_ver.php","");
}
function abrir_otro(codigo){
	carg3("pro_ver.php","&codigo="+codigo);
}
function eliminar(codigo){
	if(confirm("SEGURO DE ELIMINAR?")){
		carg("pro_save.php","&modo=eliminar&codigo="+codigo);
	}
}
function clonar(codigo,codigo_barra){  
	AJAXPOST(url_base+modulo_base+"pro_ajax.php","modo=clonar&codigo="+codigo+"&codigo_barra="+codigo_barra,document.getElementById("modGeneral_lugar"));	
	$('#modGeneral').modal('show'); 
}
function clonar_save(codigo,codigo_barra){  
	var div = document.getElementById("codigo_unico2");
	AJAXPOST("mods/home/pro_ajax.php","&modo=codigo_unico&clonar=S&codigo="+document.getElementById("codigo_clon").value,div);
}
function clonar_save2(){  
	var div = document.getElementById("clonar_save");
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/pro_ajax.php",a+"&modo=clonar_save",div);
}
</script>

<?
/*
<table class="table  table-bordered"> 
<tr>
    <th width="1">Ficha:</th>
    <td><select name="codigo" id="codigo" class="campos buscador" onchange="javascript:cambiar();">
    <?php
    $res = sql_productos("*","   order by pro.pro_codigo asc");
    while($row = mysqli_fetch_array($res)){
        ?><option value="<?php echo $row["pro_codigo"]; ?>" <?php
        if($row["pro_codigo"] == $_REQUEST["codigo"]){ echo "selected"; } ?>><?php
        echo $row["pro_codigo"]." - ".$row["pro_nombre"];  
        ?></option><?php } 
    ?>
    </select></td>
</tr>
</table>
*/
?>  
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li class="active"><a href="#tab_1" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Informaci&oacute;n</a></li>
    <?php
    if($datos["pro_tipo"] <> 'INS'){
        ?>
        <li><a href="#tab_2" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Activos Relacionados</a></li>
        <?
    }
    ?>
</ul>

<div class="tab-content">
	<div class="tab-pane active" id="tab_1">
    	<table width="100%">
        <tr valign="top">
            <td width="200">
                <? 
                $imagen = ($datos["pro_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$datos["pro_imagen"];
                ?><img src="../../<?php echo $imagen; ?>" style="width:200px !important;">
                
                <?php 
                construir_boton("window.print()","","imprimir","Imprimir Ficha",4);
                ?>
                <br><br><br>
                <div class="alert alert-info">
                    <b>Ultima modificación del producto:</b><br>
                    <?php echo $datos["pro_modificacion"]; ?>
                </div>
            </td>
            <td style="padding-left:10px;">
                <table class="table table-bordered table-condensed"> 
                    <thead>
                        <tr>
                            <th colspan="2">Datos del Producto</th>
                        </tr>    
                    </thead>
                <tbody>
                    <tr>
                        <th width="120" style="text-align:right;">Nombre:</th>
                        <th><?php echo $datos["pro_nombre"]; ?></th>
                    </tr> 
                    <tr>
                        <th style="text-align:right;">Código Producto:</th>
                        <td><?php echo $datos["pro_codigo"]; ?></td> 
                    </tr>
                    <tr>
                        <th style="text-align:right;">Código Barra:</th>
                        <td><?php echo $datos["pro_codigo_barra"]; ?></td>
                    </tr> 
                    <tr>
                        <th style="text-align:right;">Tipo Producto:</th>
                        <td><?php echo $_tipo_activo[$datos["pro_tipo"]]; ?></td>
                    </tr> 
                    <tr>
                        <th style="text-align:right;">Clasificación:</th>
                        <td><?php  
                        $labels = explode(",",_opc("label_clasificacion")); 
                        $valores = explode(" > ",$_categorias_largo[$datos["pro_categoria"]]); 
                        for($ic = 0;$ic < count($valores);$ic++){
                            echo "<b>".$labels[$ic]."</b>: ".$valores[$ic]."<br>";
                        }
                        ?>
                        </td>
                    </tr>  
                    <tr>
                        <th style="text-align:right;">Unidad:</th>
                        <td><?php echo $_unidades[$datos["pro_unidad"]]; ?></td>
                    </tr>
                    <tr>
                        <th style="text-align:right;">Mantenimiento:</th> 
                        <td><?php echo ($datos["pro_mantenimiento"])?"Requiere mantención":"-"; ?></td>
                    </tr> 
                    <tr>
                        <th style="text-align:right;">Garantía:</th> 
                        <td><?php echo ($datos["pro_garantia"])?"Maneja Garantía":"-"; ?></td>
                    </tr>  
                    <tr>
                        <th style="text-align:right;">Prestable:</th> 
                        <td><?php echo ($datos["pro_prestable"])?"SI, prestable":"NO, prestable"; ?></td>
                    </tr>  
                    <tr>
                        <th style="text-align:right;">Asignable:</th> 
                        <td><?php echo ($datos["pro_asignable"])?"SI, asignable":"NO, asignable"; ?></td>
                    </tr> 
                    <tr>
                        <th style="text-align:right;">Descripción:</th> 
                        <td><?php echo nl2br($datos["pro_descripcion"]); ?></td>
                    </tr> 
                </tbody>
                </table> 
            </td> 
            <td style="padding-left:10px;" width="350">
                <table class="table table-bordered table-condensed"> 
                <thead>
                    <tr>
                        <th colspan="2">Información Adicional</th>
                    </tr>    
                </thead>
                <tbody>
                    <?





                    for($iex = 1;$iex <= 10;$iex++){                 
                        $res = sql_campos("*"," and cam_producto = $iex order by cam_nombre asc");  
                        if(mysqli_num_rows($res) > 0){
                            while($row = mysqli_fetch_array($res)){
                                ?> 
                                <tr> 
                                    <th style="text-align: right;"><? echo $row["cam_nombre"]; ?>:</th>
                                    <td><? echo $datos["pro_extra_".$iex]; ?></td> 
                                </tr>
                                <? 
                            }
                        }
                    }


                    ?>  
                </tbody>
                </table> 
            </td> 
        </tr>
        </table> 
    </div>
	<div class="tab-pane" id="tab_2">  
        <table class="table table-bordered table-condensed"> 
        <thead>
        <tr>
            <th width="1"></th>  
 
            <th width="1">Cod.Activo</th>
            <th width="1">Serie</th>
            <th width="1">Etiqueta</th>
            
            <th>Lugar</th>
            <th>Ubicacion</th>
            <th>Responsable</th>
            
            <th width="1">Estado</th>
        </tr>
        </thead> 
        <tbody>
        <?php
        $res3 = sql_activos_fijos("*","and acf_producto = '$datos[pro_id]' order by acf_serie asc");
        if(mysqli_num_rows($res3) > 0){ 
            while($row3 = mysqli_fetch_array($res3)){
                ?>
                <tr>
                    <td><?
                    construir_boton("acf_info.php","&acf=".$row3["acf_id"],"buscar","Ver Activo");
                    ?></td>
                    <th><?php echo $row3["acf_codigo"]; ?></th>
                    <td><?php echo $row3["acf_serie"]; ?></td>
                    <td><?php if($row3["acf_etiqueta"] <> ""){ echo $row3["acf_etiqueta"]; } ?></td>  
                                
                    <td><?php echo $_bodegas[$row3["ubi_bodega"]]; ?></td>  
                    <td><?php echo $row3["ubi_nombre"]; ?></td> 
                    <td><?php echo $row3["per_nombre"]; ?></td> 
                    
                    <td><?php activo_fijo_estado($row3["acf_disponible"]); ?></td>
                </tr>
                <?
            }
            ?>
            <?
        }
        ?>
        </tbody>
        </table>
    </div>
</div>  

<div style="border-top:1px solid #CCC; padding-top:10px;">
    <table width="100%">
    <tr>
        <td><?php
        construir_boton("pro_listado.php","","izquierda","Volver al listado de Productos",2); 
        construir_boton("pro_editar.php","&id=".$_REQUEST["id"],"editar","Editar Producto",2);
        //construir_boton("clonar('".$datos["pro_codigo"]."','".$datos["pro_codigo_barra"]."')","","notas_editar","Clonar Producto",4);
        construir_boton("pro_ver.php","&id=".$_REQUEST["id"],"refrescar","Refrescar Ficha",2);
        //construir_boton("eliminar('".$datos["pro_id"]."')","","eliminar","Eliminar Producto",4);
        ?></td>
    </tr>
    </table> 
</div>
<br /><br />