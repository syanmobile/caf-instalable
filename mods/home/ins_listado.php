<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Insumos";
construir_breadcrumb($titulo_pagina);
//---------------------------------------------------------------------------------------- 
$pagina = "ins_listado.php"; 
include("ayuda.php");
//---------------------------------------------------------------------------------------- 
?>
<script language="javascript">
function imprimir(){
	var a = $(".campos").fieldSerialize();
	visor("pro_cod_barra.php?modalidad=varios_prod&"+a);
}
function excel(){
	var a = $(".campos").fieldSerialize();
	window.open("mods/home/pro_excel.php?"+a);
}
</script>
<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td><?php
		construir_boton("ins_nuevo.php","","crear","Nuevo Insumo",2);  
		?></td>
    </tr>
</tbody>
</table> 

<table class="table table-bordered table-condensed table-info" style="margin-bottom:0px !important;">  
<tbody> 
<tr>
    <td width="100">
    <b>Código:</b><br />
    <input type="text" name="fil_codigo" id="fil_codigo" class="campos w10" value="<? echo $_REQUEST["fil_codigo"]; ?>" placeholder="Usar % para búsquedas"></td> 
    <td>
    <b>Nombre Producto:</b><br />
    <input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? echo $_REQUEST["fil_descripcion"]; ?>" placeholder="Usar % para búsquedas"></td> 
    
    
    <td>
    <b>Clasificación:</b><br>
	<select name="fil_categoria" id="fil_categoria" class="campos buscador w10">
	<option value="">Todos</option>
	<?php
	$res = sql_categorias("*"," order by cat.cat_nombre asc"); 
	if(mysqli_num_rows($res) > 0){
		while($row = mysqli_fetch_array($res)){
			?>
            <option value="<? echo $row["cat_id"]; ?>" <?
            if($_REQUEST["fil_categoria"] == $row["cat_id"]){ echo "selected"; } 
            ?>>C<? echo $row["cat_codigo"]." - ".$row["cat_nombre_largo"]; ?></option>
            <?
		}
	}
	?></select></td>
    <td width="180">
    <b>Ordenar:</b><br />
    <?
    if($_REQUEST["ordenar"] == ""){
    	$_REQUEST["ordenar"] = "pro.pro_codigo asc";
    }
    ?>
    <select name="ordenar" id="ordenar" class="campos w10">
	    <option value="pro.pro_codigo asc" <? 
	    if($_REQUEST["ordenar"] == "pro.pro_codigo asc"){ echo "selected"; } ?>>Código (Asc)</option> 
	    <option value="pro.pro_codigo desc" <? 
	    if($_REQUEST["ordenar"] == "pro.pro_codigo desc"){ echo "selected"; } ?>>Código (Desc)</option>
	    <option value="pro.pro_nombre asc" <? 
	    if($_REQUEST["ordenar"] == "pro.pro_nombre asc"){ echo "selected"; } ?>>Nombre Insumo (Asc)</option>
	    <option value="pro.pro_nombre desc" <? 
	    if($_REQUEST["ordenar"] == "pro.pro_nombre desc"){ echo "selected"; } ?>>Nombre Insumo (Desc)</option>
	    <option value="cat.cat_codigo asc" <? 
	    if($_REQUEST["ordenar"] == "cat.cat_codigo asc"){ echo "selected"; } ?>>Código Categoría (Asc)</option>
	    <option value="cat.cat_codigo desc" <? 
	    if($_REQUEST["ordenar"] == "cat.cat_codigo desc"){ echo "selected"; } ?>>Código Categoría (Desc)</option>
	    <option value="cat.cat_nombre asc" <? 
	    if($_REQUEST["ordenar"] == "cat.cat_nombre asc"){ echo "selected"; } ?>>Nombre Categoría (Asc)</option>
	    <option value="cat.cat_nombre desc" <? 
	    if($_REQUEST["ordenar"] == "cat.cat_nombre desc"){ echo "selected"; } ?>>Nombre Categoría (Desc)</option>
    </select></td> 
   
    <td width="1"><?
	construir_boton("ins_listado.php","","buscar","Filtrar");
	?></td> 
</tr>
</tbody>
</table> 
<?php 
/****************************************************************************************/
if($_REQUEST["fil_codigo"] <> ""){
	$filtros .= " and pro.pro_codigo like '%".$_REQUEST["fil_codigo"]."%' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and pro.pro_nombre like '%".$_REQUEST["fil_descripcion"]."%' ";
}
if($_REQUEST["fil_barra"] <> ""){
	$filtros .= " and pro.pro_codigo_barra like '%".$_REQUEST["fil_barra"]."%' ";
}
for($i = 1;$i <= 20;$i++){
	if($_REQUEST["fil_extra_".$i] <> ""){
		$filtros .= " and pro.pro_extra_".$i." like '%".$_REQUEST["fil_extra_".$i]."%' ";
	}
}
if($_REQUEST["fil_unidad"] <> ""){
	$filtros .= " and pro.pro_unidad = '".$_REQUEST["fil_unidad"]."' ";
} 
if($_REQUEST["fil_categoria"] <> ""){
	$filtros .= " and pro.pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
}
$filtros .= " and pro.pro_tipo = 'INS' ";
?>
<input type="hidden" name="pagina" id="pagina" value="ins_listado.php" class="campos">
<?
$sql_paginador = sql_productos("*","   $filtros ORDER BY ".$_REQUEST["ordenar"],"s"); 
$cant_paginador = 20;
require("../../_paginador.php");
$res = mysqli_query($cnx,$sql_final);
if(mysqli_num_rows($res) > 0){
	?>  
	<table class="table table-bordered table-hover table-condensed"> 
	<thead>
	<tr> 
		<th width="120"></th> 
		<th width="1">Código</th> 
		<th width="1">CodBarra</th> 
		<th>Producto</th>  
		<th>Clasificación</th>
		<th>Unidad</th> 
		<?
		for($iex = 1;$iex <= 10;$iex++){				 
            $res2 = sql_campos("*"," and cam_producto = $iex order by cam_nombre asc");  
			if(mysqli_num_rows($res2) > 0){
				while($row2 = mysqli_fetch_array($res2)){
					$extras[] = $iex;
					?><th><? echo $row2["cam_nombre"]; ?></th><?
				}
			}
		}
		?>
		<th width="1">CostoPromedio</th>
		<th width="1">Mínimo</th>
		<th width="1">Máximo</th>
		<th width="1">$Inventario</th>
	</tr>
	</thead>
	<tbody> 
	<?php 
	while($row = mysqli_fetch_array($res)){  
		?>
		<tr>    
			<td><?
			construir_boton("ins_editar.php","&id=".$row["pro_id"],"editar","Editar Producto");
			construir_boton("ins_ver.php","&id=".$row["pro_id"],"buscar","Ver Producto");  
			construir_boton("inf_kardex_producto.php","&producto=".$row["pro_id"],"tabla","Kardex Producto"); 
			construir_boton("ins_nuevo.php","&id=".$row["pro_id"],"nota_editar","Clonar Producto");
			?></td>  
			<th><?php echo $row["pro_codigo"]; ?></th> 
			<td><?php echo $row["pro_codigo_barra"]; ?></td> 
			<td><?php echo $row["pro_nombre"]; ?></td>   
			<td><?php 
			if($row["pro_categoria"] <> 0){
				echo "C".$row["cat_codigo"]." - ".$_categorias_largo[$row["pro_categoria"]]; 
			}
			?></td>  
			<td><?php echo $_unidades[$row["pro_unidad"]]; ?></td> 
			<?
			foreach($extras as $iex){				 
	            ?><td><? echo $row["pro_extra_".$iex]; ?></td><?
	        }
			?> 
			<td style="text-align:right"><?php echo _num($row["pro_valor"]); ?></td>   
			<td style="text-align:center"><?php echo _num($row["pro_stock_minimo"]); ?></td> 
			<td style="text-align:center"><?php echo _num($row["pro_stock_maximo"]); ?></td> 
			<td style="text-align:right"><?php echo _num($row["pro_inventario"]); ?></td>     
		</tr>
		<?php 
	}
	?> 
	</tbody>
	</table>  
	<?php
	echo $paginador_dibujo;
}else{
	?>
	<div class="alert alert-danger">
		<strong>Sin resultados</strong>
	</div>
	<?php 
} 
?>