<?php
require("../../inc/conf_dentro.php");

switch($_REQUEST["modo"]){
	case "seleccionados": 
		?> 
		<h4 id="titulo_alerta">Editar Seleccionados</h4>  
				
		<table class="table table-bordered table-condensed">
		<thead>
		<tr>
			<th colspan="2">Edición de Activos</th>
		</tr>
		</thead>
		<tbody>
		<tr> 
			<th style="text-align:right;">Seleccionados:</th>
			<td><?php echo _num2(count($_REQUEST["acf"])); ?> activos</td>
		</tr> 
		<tr>
			<th style="text-align:right;" width="130">Tipo Depreciación:</th>
			<td><select name="tipo_depreciacion" class="campos">
				<option value=""></option>
				<option value="L">Lineal</option>
				<option value="A">Acelerada</option>
				<option value="C">Cliente</option>
			</select></td>
		</tr> 
		<tr>
			<th style="text-align:right;">Vida Util:</th>
			<td><input type="text" name="vida_util" class="campos numero" value="0"> meses</td>
		</tr>
		<tr>
			<th style="text-align:right;">Inicio Revalorización:</th>
			<td><input type="text" name="mes_ano_inicio_revalorizacion" class="campos" placeholder="mm-yyyy"></td>
		</tr>
		<tr>
			<th style="text-align:right;">Inicio Depreciación:</th>
			<td><input type="text" name="mes_ano_inicio_depreciacion" class="campos" placeholder="mm-yyyy"></td>
		</tr> 
		<tr>
			<th style="text-align:right;">Tipo Adquisición:</th>
			<td><select name="tipo_adquisicion" class="campos">
				<option value=""></option>
				<option value="P">Propio</option>
				<option value="A">En Leasing</option>
			</select></td>
		</tr> 
		</tbody>
		</table>  
        <?
		construir_boton("grabar_cambios();","","grabar","Grabar Series",4);
		?>
		<div id="grabar_cambios_lugar"></div>
		<br><br>
		<script type="text/javascript"> 
		function grabar_cambios(){
			var a = $(".campos").fieldSerialize();
			AJAXPOST(url_base+modulo_base+"dep_ajax.php",a+"&modo=editar_seleccionados",document.getElementById("grabar_cambios_lugar"));
		}
		</script>
		<?
		break; 

	case "editar_seleccionados":
		_p($_POST);
		break;

	case "depreciar_seleccionados": 
		?> 
		<h4 id="titulo_alerta">Depreciación Seleccionados</h4>  
				
		<?
		$activos = $_REQUEST["acf"];
		foreach($activos as $activo){

			$res = sql_activos_fijos("*"," and acf.acf_id = '$activo' ");  
			if(mysqli_num_rows($res) > 0){
				$row = mysqli_fetch_array($res);
			}
			/*
			echo "acf_valor: "._num($row["acf_valor"])."<br>";
			echo "acf_tipo_depreciacion: ";
			echo ($row["acf_tipo_depreciacion"] == "L")?"Lineal":"Acelerada";
			echo "<br>acf_vida_util: ".$row["acf_vida_util"]."<br>";
			echo "acf_valor_residual: "._num($row["acf_valor_residual"])."<br>";
			echo "acf_mes_ano_inicio_revalorizacion: ".$row["acf_mes_ano_inicio_revalorizacion"];
			*/
			$periodo = explode("-",$row["acf_mes_ano_inicio_depreciacion"]);
			$ano = $periodo[0] * 1;
			$mes = $periodo[1] * 1;
			$meses = $row["acf_vida_util"] * 1;
			switch($row["acf_tipo_depreciacion"]){
				case "L": $formula = 1; break;
				case "A": $formula = 3; break;
			}
			$meses_divisor = round($meses / $formula);

			$residual = $row["acf_valor_residual"] * 1;
			$valor_activo = $row["acf_valor"] * 1;
			$valor = $valor_activo - $residual;

			$valor_mensual = round(($valor / $meses_divisor),2);
			?>
			<div style="height: 350px; overflow: scroll; width: 100%;">
				<table class="table table-striped table-bordered tabledrag table-condensed"> 
				<thead>
					<tr>   
						<th style="text-align:center;" width="1">#</th>
						<th style="text-align:center;" width="1">Año</th>
						<th style="text-align:center;" width="1">Mes</th>
						<th style="text-align:right;">Cuota Depreciación $</th>
						<th style="text-align:right;">Depreciación Acumulada $</th>
						<th style="text-align:right;">Valor neto en Libros $</th>
					</tr>
				</thead>
				<tbody>
				<?php 
				$sql3 = "delete from depreciaciones where dep_acf_id = '$row[acf_id]'";
				$querys[] = $sql3;

				$ano_limite = substr($row["acf_fecha_salida"],0,4) * 1;
				$mes_limite = substr($row["acf_fecha_salida"],5,2) * 1;

				for($i = 1;$i <= $meses_divisor;$i++){
					$acumulado += $valor_mensual;
					$valor -= $valor_mensual;
					$valor_activo -= $valor_mensual;

					if(($row["acf_fecha_salida"] == "0000-00-00") || ($ano <= $ano_limite && $mes <= $mes_limite)){
						?>
						<tr>
							<td><? echo $i; ?></td>
							<td><? echo $ano; ?></td>
							<td><? echo _mes($mes); ?></td>
							<td style="text-align: right;"><? echo _num($valor_mensual); ?></td>
							<td style="text-align: right;"><? echo _num($acumulado); ?></td>
							<td style="text-align: right;"><? echo _num($valor_activo); ?></td>
						</tr>
						<?  
						$mesx = ($mes < 10)?"0".$mes:$mes;
					}
					if($mes == 12){
						$mes = 0;
						$ano++;
					} 
					$mes++;
				}
				?>
				</tbody>
				</table> 
			</div>
			<?
			foreach($querys as $sql){
				echo $sql."<br>";
			}
		} 
		break;
}
?> 