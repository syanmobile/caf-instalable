<?php
require("../../inc/conf_dentro.php");
 
switch($_REQUEST["modo"]){
	case "disponibles":
		$res = sql_insumos("*"," and ubi_bodega = '$_REQUEST[bodega]' and total > 0 group by pro_id order by pro_nombre asc "); 
		if(mysqli_num_rows($res) > 0){
			?> 
			<table class="table table-bordered table-condensed" id="tablita"> 
			<thead>
				<tr> 
					<th>Imagen</th> 
					<th>Producto</th>
					<th>Informacion</th> 
				</tr>
			</thead>
			<tbody> 
			<?
			while($row = mysqli_fetch_array($res)){  
				?>
				<tr>  
                    <td>
						<? 
						$imagen = ($row["pro_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$row["pro_imagen"];
						?><img src="<?php echo $imagen; ?>" style="width:80px !important;"></td>
						<td>
						<b><? echo $row["pro_nombre"]; ?></b><br />
						<strong>Codigo</strong>: <? echo $row["pro_codigo"]; ?><br />
                    
						<table class="table table-striped table-bordered table-condensed" style="width: 300px;">
						<?
						$res2 = sql_insumos("*"," and ubi_bodega = '$_REQUEST[bodega]' and total > 0 and pro_id = '$row[pro_id]' order by pro_nombre asc "); 
						if(mysqli_num_rows($res2) > 0){
							while($row2 = mysqli_fetch_array($res2)){ 
								$i++;
								?>
								<tr>
									<td style=" width: 50px;"><input type="text" value="" name="cant[]" class="w10" style="text-align: center; font-size: 20px; height: 30px;" id="cant_<? echo $i; ?>"> </td>
									<td style="text-align: center; font-size: 20px;" width="1"><?php echo $row2["total"]; ?></td>
									<td>
									<?php echo $row2["ubi_nombre"]; ?><br>
									<b><?php echo $row2["ubi_codigo"]; ?></b>
									</td>
								</tr> 
								<input type="hidden" value="<?php echo $row2["total"]; ?>" id="stock_<? echo $i; ?>">
								<input type="hidden" value="<?php echo $row2["pro_id"]."___".$row2["ubi_id"]; ?>" id="prod_<? echo $i; ?>">
								<?
							}
						}
						?>
						</table>
						
					</td>
                    <td> 
						<strong>Categoria</strong>: <? echo $_categorias[$row["pro_categoria"]]; ?> 
						<?php
						$extras = unserialize($_configuracion["productos_extras"]);
						for($ix = 1;$ix <= 20;$ix++){
							if($extras["Extra_".$ix] <> "" && $row["pro_extra_".$ix] <> ""){
								?>
								<br><strong><?php echo $extras["Extra_".$ix]; ?></strong>: <?php echo $row["pro_extra_".$ix]; ?>
								<?php
							}
						} 
						?>  
                    </td>
				</tr>
				<? 
			}
			?>
			</tbody>
			</table> 
            
			<input type="hidden" name="cantidades" id="cantidades" value="" class="campos" /> 
			<input type="hidden" name="productos" id="productos" class="campos" />
			<input type="hidden" id="total" value="<? echo $i; ?>">
           
            <script language="javascript">
            $(document).ready(function() { 
                $('#tablita').DataTable({ 
					"columns": [    
						{ "width": "1px" },   
						null ,   
						null 
					  ],
					"paging":   false,
					"info":     false,
					"lengthMenu": [100000],
					"oLanguage": {
						"sSearch": "Busque un insumos e ingrese la cantidad"
					}
				});
            } );   
            function cantidades_valores(){
				var texto = "";
				var texto2 = "";
				var total = 0; 
				<?php
				for($i2 = 1;$i2 <= $i;$i2++){
					?>
					total = document.getElementById("cant_<? echo $i2; ?>").value * 1;
					if(total > 0){
						document.getElementById("cant_<? echo $i2; ?>").value = total;
					}else{
						document.getElementById("cant_<? echo $i2; ?>").value = "";
					}
					texto = texto + ";" + document.getElementById("cant_<? echo $i2; ?>").value;
					texto2 = texto2 + ";" + document.getElementById("prod_<? echo $i2; ?>").value;
					<?
				}
				?>
				document.getElementById("cantidades").value = texto;
				document.getElementById("productos").value = texto2;
			}
			function valida_cantidades(){
				var error = 0;
				var ingreso = 0;
				var stock = 0;
				<?php
				for($i2 = 1;$i2 <= $i;$i2++){
					?>
					ingreso = document.getElementById("cant_<? echo $i2; ?>").value * 1;
					stock = document.getElementById("stock_<? echo $i2; ?>").value * 1;
					if(ingreso > 0 && ingreso > stock){
						return true;
					}
					<?
				}
				?>
				return false;
			}
            </script> 
			<?
		}else{
			?>
			<div class="alert alert-danger">
				<strong>Sin registros creados</strong>
			</div>
			<?php 
		} 
		break;
}
?> 