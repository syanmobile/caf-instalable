<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabación SubGrupos";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

switch($_POST["modo"]){
	case "crear":
		$res = sql_subgrupos("*"," and sgru.sgru_codigo = '$_REQUEST[codigo]'","","");
		if(mysqli_num_rows($res) > 0){
			?>
            <div class="alert alert-danger"> 
               <b>ERROR</b>, código ya existe: '<strong><? echo $_POST["codigo"]; ?></strong>'
            </div>
            <?
		}else{ 
			$sql = "INSERT INTO subgrupos ( 
				sgru_gru_id,
				sgru_codigo,
				sgru_nombre 
			) VALUES ( 
				'$_POST[grupo]',
				'$_POST[codigo]',
				'$_POST[nombre]' 
			)";
			$res = mysqli_query($cnx,$sql);
			$_POST["id"] = mysqli_insert_id($cnx);
			?>
			<div class="alert alert-success"> 
			
				<strong>SubGrupo '<? echo $_POST["nombre"]; ?>' creado con mucho &eacute;xito</strong>
			</div>
			<?php 
		}
		break;
		
	case "editar":
		$SQL_ = "UPDATE subgrupos SET 
			sgru_gru_id = '$_POST[grupo]',
			sgru_nombre = '$_POST[nombre]' 
		WHERE sgru_id = '$_POST[id]' ";   
		$res = mysqli_query($cnx,$SQL_);
		?>   
        <div class="alert alert-success"> 
           SubGrupo <strong>'<? echo $_POST["nombre"]; ?>'</strong> editado con &eacute;xito
        </div>  
		<?php  
		break;
}

construir_boton("sgru_listado.php","","buscar","Listado SubGrupos",2);
if($_REQUEST["modo"] <> "eliminar"){
	construir_boton("sgru_editar.php","&id=".$_POST["id"],"editar","Editar este SubGrupo",2);
}
construir_boton("sgru_nuevo.php","","crear","Crear otro SubGrupo ",2);
?>