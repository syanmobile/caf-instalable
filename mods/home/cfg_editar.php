<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Editar Configuración";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function save(){    
	if(document.getElementById("titulo").value == ""){
		alerta_js("Es obligación ingresar el TITULO del parámetro");
		return;	
	}
	carg("cfg_save2.php","");
}
</script>

<?php
$res = sql_configuracion("*","where cfg.cfg_codigo = '$_REQUEST[codigo]'","");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<strong>No se encontró el dato</strong>
	</div>
    <?php
	exit();
}
?> 
<form class="form-horizontal" role="form" method="post">
	<input type="hidden" name="modo" value="editar" class="campos">
    
    <div class="form-group">
        <label for="codigo" class="col-sm-2 control-label">Código <span class="oblig">(*)</span></label>
        <div class="col-sm-3">
            <input type="text" class="form-control campos" id="codigo" name="codigo" value="<?php
			echo $datos["cfg_codigo"]; ?>" readonly>
        </div>
    </div>
    
    <div class="form-group">
        <label for="grupo" class="col-sm-2 control-label">Grupo <span class="oblig">(*)</span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control campos" id="grupo" name="grupo" value="<?php
			echo $datos["cfg_grupo"]; ?>">
        </div>
    </div>
    
    <div class="form-group">
        <label for="titulo" class="col-sm-2 control-label">Titulo <span class="oblig">(*)</span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control campos" id="titulo" name="titulo" value="<?php
			echo $datos["cfg_titulo"]; ?>">
        </div>
    </div>
    
    <div class="form-group">
        <label for="tipo" class="col-sm-2 control-label">Tipo <span class="oblig">(*)</span></label>
        <div class="col-sm-10">
            <select name="tipo" id="tipo" class="form-control campos">
            <option value="texto" <?php if($datos["cfg_tipo"] == "texto"){ echo "selected"; } ?>>Texto</option>
            <option value="textarea" <?php if($datos["cfg_tipo"] == "textarea"){ echo "selected"; } ?>>Textarea</option>
            <option value="serial" <?php if($datos["cfg_tipo"] == "serial"){ echo "selected"; } ?>>Serial</option>
            </select>
        </div>
    </div>
    
    <div class="form-group">
        <label for="valor" class="col-sm-2 control-label">Valor <span class="oblig">(*)</span></label>
        <div class="col-sm-10">
            <textarea rows="6" type="text" class="form-control campos" id="valor" name="valor"><?php
			echo $datos["cfg_valor"]; ?></textarea>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-sm-2"/>
        <div class="col-sm-10">
        	<?php
			construir_boton("","","grabar","Guardar",3);
			construir_boton("navegacion.php","","eliminar","Cancelar",2);
			?>
        </div>
    </div>
</form> 