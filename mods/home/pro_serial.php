<?php 
ob_start(); 
require("../../inc/conf_dentro.php"); 

// para utilizar la librería
define('FPDF_FONTPATH','font/');
require_once('../../pdf/fpdf.php');

// Parametros pdf
$info = unserialize($_configuracion["pdf_producto"]);
$dato1 = explode(",",$info["Orientacion/Unidad"]);
$dato2 = explode(",",$info["Ancho/Alto"]);
$dato3 = explode(",",$info["Lugar_Codebar"]);
$dato4 = explode(",",$info["Campos"]);
$dato5 = explode(",",$info["Lugar_Campos"]);
$dato6 = explode(",",$info["Lugar_Logo"]);

$pdf=new FPDF($dato1[0],$dato1[1],array($dato2[0],$dato2[1]));
$pdf->AliasNbPages();

switch($_REQUEST["modo"]){
	case "":
		$res = sql_productos("*"," where pro.pro_tipo <> 'INS' and pro.pro_codigo = '".$_REQUEST["codigo"]."' ");
		$row = mysqli_fetch_array($res);

		$pdf->AddPage();

		//$pdf->SetFont('Arial','b',20);
		$pdf->SetFont($dato5[0],$dato5[1],$dato5[2]);
		foreach($dato4 as $texto){

			// Esto es un margen opcional
			if($dato5[7] > 0){ 
				$pdf->Cell($dato5[7],$dato5[4],"",0,0,'L');
			}

			//$pdf->Cell(0,7,$row[$texto],0,1,'C');
			$textos = explode("|",$texto);
			$pdf->Cell($dato5[3],$dato5[4],utf8_decode($row[$textos[0]].$textos[1]),$dato5[5],1,$dato5[6]);
		}

		$serial = $row["pro_codigo"]._opc("separador_activo_fijo").$_REQUEST["serial"];

		$serial = str_replace(" ","",$serial);
		$serial = str_replace("	","",$serial);
		$pdf->Image($url_base.'pdf/image.php?text='.$serial.'&'.$info["Codebar"],$dato3[0],$dato3[1],$dato3[2],0,'PNG'); 

		if($info["Lugar_Logo"] <> ""){
			$pdf->Image($url_base.'img/headers/'._opc("logo_empresa"),$dato6[0],$dato6[1],$dato6[2],0,'PNG');
		} 
		break;
		
	case "serial":
		$res = sql_stock("*,sum(total) as stock"," where pro_tipo <> 'INS' and pro_codigo = '$_REQUEST[codigo]' 
		group by pro_codigo, lot_codigo order by pro_grupo asc, pro_subgrupo asc, pro_codigo asc, lot_codigo asc "); 
		if(mysqli_num_rows($res) > 0){
			while($row = mysqli_fetch_array($res)){ 

				$pdf->AddPage();

				//$pdf->SetFont('Arial','b',20);
				$pdf->SetFont($dato5[0],$dato5[1],$dato5[2]);
				foreach($dato4 as $texto){

					// Esto es un margen opcional
					if($dato5[7] > 0){ 
						$pdf->Cell($dato5[7],$dato5[4],"",0,0,'L');
					}

					//$pdf->Cell(0,7,$row[$texto],0,1,'C');
					$textos = explode("|",$texto);
					$pdf->Cell($dato5[3],$dato5[4],utf8_decode($row[$textos[0]].$textos[1]),$dato5[5],1,$dato5[6]);
				}

				$serial = $row["pro_codigo"]._opc("separador_activo_fijo").$row["lot_codigo"];

				$serial = str_replace(" ","",$serial);
				$serial = str_replace("	","",$serial);
				$pdf->Image($url_base.'pdf/image.php?text='.$serial.'&'.$info["Codebar"],$dato3[0],$dato3[1],$dato3[2],0,'PNG'); 

				if($info["Lugar_Logo"] <> ""){
					$pdf->Image($url_base.'img/headers/'._opc("logo_empresa"),$dato6[0],$dato6[1],$dato6[2],0,'PNG');
				}
			}
		}
		break;
}

$pdf->Output();
?>