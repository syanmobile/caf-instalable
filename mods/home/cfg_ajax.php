<?php
require("../../inc/conf_dentro.php");
 
switch($_POST["modo"]){ 
	case "editar":
		
		$res = sql_configuracion("*"," and cfg.cfg_codigo = '".$_REQUEST["codigo"]."' ORDER BY cfg.cfg_titulo asc ");
		if(mysqli_num_rows($res) > 0){
			while($row = mysqli_fetch_array($res)){
				?>
           		<table class="table table-striped table-bordered table-condensed">
           		<thead>
           			<tr>
           				<th><? echo $row["cfg_titulo"]; ?></th>
           			</tr>
           		</thead>
           		<tbody> 
                   <tr>
                   	<td>
                    <?php
					switch($row["cfg_tipo"]){
						case "check":
							?>
							<select class="form-control campos" name="campo[]">
								<option value="1" <?php if($row["cfg_valor"] == "1"){ echo "selected"; } ?>>SI</option>
								<option value="0" <?php if($row["cfg_valor"] == "0"){ echo "selected"; } ?>>NO</option>
							</select>
							<?php
							break;
						case "texto":
							?><input type="text" class="form-control campos" name="campo" value="<?php
							echo $row["cfg_valor"]; ?>"><?php
							break;
						case "textarea":
							?><textarea type="text" class="form-control campos" name="campo" rows="4"><?php
							echo $row["cfg_valor"]; ?></textarea><?php
							break;
						case "serial":
							$valores = unserialize($row["cfg_valor"]);								
							?>
							<table class="table table-striped table-bordered table-condensed">
							<?php
							foreach($valores as $key => $value){
								?>
								<tr>
									<th width="120"><?php echo $key; ?>:</th>
									<td><input type="text" name="<?php echo $key; ?>" class="campos w10 <?php 
									echo $row["cfg_codigo"]; ?>_class" value="<?php echo $value; ?>"></td>
								</tr>
								<?
							}
							?>
							</table>
							<div style="display: none">
							<div id="serial_<?php echo $row["cfg_codigo"]; ?>"></div>
							<textarea type="text" class="form-control campos" name="campo" rows="4" id="campo_<?php echo $row["cfg_codigo"]; ?>"><?php
							echo $row["cfg_valor"]; ?></textarea>
							</div>
							<?php 
							break;
					}
					?> 
				   </td>
                </tr>					
           		</tbody>
				</table>
            	<?
				construir_boton("grabar('".$row["cfg_tipo"]."','".$row["cfg_codigo"]."')","","grabar","Grabar Cambios",4);
			}
		}
		break;

	case "grabar":  
		$sql2 = "update configuraciones set 
		cfg_id = 1 ";

		$campos[] = "rut";
		$campos[] = "nombre_empresa";
		$campos[] = "razon_social";
		$campos[] = "direccion";
		$campos[] = "ciudad";
		$campos[] = "region";
		$campos[] = "telefonos";
		$campos[] = "correo";
		$campos[] = "website";

		$campos[] = "moneda";
		$campos[] = "moneda_sec";
		$campos[] = "moneda_decimal";
		$campos[] = "moneda_sec_decimal";
		$campos[] = "moneda_ult_cambio";

		$campos[] = "tipo_depreciacion";
		$campos[] = "bajo_norma";  

		$campos[] = "cta_contable_INS";

		foreach($campos as $ca){
			$sql2 .= ", cfg_".$ca." = '".$_REQUEST[$ca]."' ";
        }

        for($i = 1;$i <= 5;$i++){
			$sql2 .= ", cfg_extra_mov_".$i." = '".$_REQUEST["extra_mov_".$i]."' ";
        }  
        $res2 = mysqli_query($cnx,$sql2);
 
        $tipo_ac = $_REQUEST["tac"];
        foreach($tipo_ac as $tac){
        	$sql3 = "update activos_tipos set 
        	tac_cuenta_contable = '".$_REQUEST["cta_contable_".$tac]."',
        	tac_depreciacion = '".$_REQUEST["depre_".$tac]."',
        	tac_depreciacion_acum = '".$_REQUEST["depre_acum_".$tac]."',
        	tac_vu_dep_lineal = '".$_REQUEST["dep_lineal_".$tac]."',
        	tac_vu_dep_acele = '".$_REQUEST["dep_acelerada_".$tac]."',
        	tac_amortizacion = '".$_REQUEST["amorti_".$tac]."', 
        	tac_readecuacion = '".$_REQUEST["readecuacion_".$tac]."'  ,
        	tac_revalorizacion = '".$_REQUEST["revalorizacion_".$tac]."'  
        	where tac_id = '$tac' "; 
        	$res3 = mysqli_query($cnx,$sql3);
        }
		?>
		<script type="text/javascript">
		carg("cfg_configuracion_editar.php","&cfg=1");
		</script>
		<?php  
		break;
}
?>