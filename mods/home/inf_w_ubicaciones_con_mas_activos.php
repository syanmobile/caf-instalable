<?php
require("../../inc/conf_dentro.php");
//---------------------------------------------------------------------------------------- 
$titulo_pagina = "Ubicaciones con más Activos"; 
construir_breadcrumb($titulo_pagina); 
//----------------------------------------------------------------------------------------

$res = mysqli_query($cnx,"SELECT count(*) as total FROM activos_fijos");
if(mysqli_num_rows($res) > 0){
	$row = mysqli_fetch_array($res);
	$total_de_activos = $row["total"] * 1;
}
?>
<div class="col-md-12 grid-item">
	<div class="panel panel-primary">  
		<div class="panel-heading">
			<b>Ubicaciones con más Activos</b>
			<div style="float: right;">
				<a href="javascript:carg('inf_w_ubicaciones_con_mas_activos.php','');" style="color:yellow !important;">Refrescar</a>
			</div>
		</div>
		<div class="panel-body"> 
			<table width="100%">
			<tr valign="top">
				<td width="30%" style="padding-right: 10px;"><?php
				$total = 0;
				unset($labels);
				unset($valores);

				$res = mysqli_query($cnx,"select * from resumen_ubicacion  a 
				left join ubicaciones b on a.res_ubicacion = b.ubi_id 
				order by res_total desc");
				if(mysqli_num_rows($res) > 0){
					?>
					<table class="table table-condensed table-bordered"> 
					<thead>
						<tr>
							<th width="1">Activos</th>
					        <th width="1" align="center">Código</th>
					        <th>Ubicación</th>  
						</tr>
					</thead>
					<tbody>
					<? 
					while($row = mysqli_fetch_array($res)){ 
						?>
						<tr>  
							<th style="text-align: center; padding: 1px 5px;"><? echo _num2($row["res_total"]); ?></th>
							<td style="padding: 1px 5px;"><? echo $row["ubi_codigo"]; ?></td> 
							<td style="padding: 1px 5px;"><? echo $_ubicaciones[$row["res_ubicacion"]]; ?></td>  
						</tr>
						<? 
					}
					?>  
					</tbody>
					</table>
					<? 
				}else{
					?>
					<div class="alert alert-danger">
						<strong>Sin activos registrados</strong>
					</div>
					<?php 
				}
				?></td>
				<td width="30%" style="padding-right: 10px;">
				<?
				$res = mysqli_query($cnx,"select *,sum(res_total) as total from resumen_ubicacion  a 
				left join ubicaciones b on a.res_ubicacion = b.ubi_id 
				group by ubi_bodega 
				order by total desc");
				if(mysqli_num_rows($res) > 0){
					?>
					<table class="table table-condensed table-bordered"> 
					<thead>
						<tr>
							<th width="1">Activos</th>
					        <th width="1" align="center">Código</th> 
					        <th>Lugar</th>  
						</tr>
					</thead>
					<tbody>
					<? 
					while($row = mysqli_fetch_array($res)){ 
						?>
						<tr>  
							<th style="text-align: center; padding: 1px 5px;"><? echo _num2($row["total"]); ?></th>
							<td style="padding: 1px 5px;"><? echo $_bodegas_codigo[$row["ubi_bodega"]]; ?></td> 
							<td style="padding: 1px 5px;"><? echo $_bodegas[$row["ubi_bodega"]]; ?></td> 
						</tr>
						<?
						$labeles.= ";".$row["ubi_bodega"];
						$valores.= ";".$row["total"];
					}
					?>  
					</tbody>
					</table>
					<? 
				}else{
					?>
					<div class="alert alert-danger">
						<strong>Sin activos registrados</strong>
					</div>
					<?php 
				}
				?></td>
				<td> 
					<iframe src="mods/home/widget_grafico_barras.php?label=<? echo $labeles; ?>&valor=<? echo $valores; ?>&alto=500&nombre=bodega" frameborder="0" scrolling="no" style="width: 100%; height: 490px;"></iframe> 
				</td>
			</tr>
			</table>
		</div>
	</div>
</div>  