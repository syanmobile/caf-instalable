<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Insumos con Stock Crítico";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?> 
<table class="table table-bordered table-condensed table-info">
<tr>  
    <td width="100">
    <b>Código:</b><br />
    <input type="text" name="fil_codigo" id="fil_codigo" class="campos w10" value="<? echo $_REQUEST["fil_codigo"]; ?>"></td> 
    <td>
    <b>Descripción:</b><br />
    <input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? echo $_REQUEST["fil_descripcion"]; ?>"></td>  
    <td>
    <b>Clasificación:</b><br />
    <select name="fil_categoria" id="fil_categoria" class="campos buscador w10">
    <option value=""></option>
    <?php
    $res = sql_categorias("*"," order by cat.cat_codigo asc"); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["cat_codigo"]; ?>" <?
            if($_REQUEST["fil_categoria"] == $row["cat_id"]){ echo "selected"; } 
            ?>>C<? echo $row["cat_codigo"]." - ".$row["cat_nombre_largo"]; ?></option>
            <?
        }
    }
    ?></select></td> 

    <td width="1"><?
    construir_boton("alerta_js('Excel no disponible (Demo)');","1","importar","Descargar",4); 
    ?></td> 
    <td width="1"><?php
    construir_boton("inf_insumos_stock_critico.php","","buscar","Filtrar");
    ?></td>  
</tr>
</table>

<?php
$v_codigo = $_REQUEST["fil_codigo"];
$v_descripcion =$_REQUEST["fil_descripcion"];
$v_grupo = $_REQUEST["fil_grupo"];
$v_subgrupo = $_REQUEST["fil_subgrupo"];
$v_categoria = $_REQUEST["fil_categoria"];
$v_filtro = "";

if($v_codigo!=""){
	$v_filtro .= " AND pro_codigo LIKE '%$v_codigo%' ";
}
if($v_descripcion !=""){
	$v_filtro .= " AND pro_nombre LIKE '%$v_descripcion%' ";
}

if($v_grupo !=""){
	$v_filtro .= " AND pro_grupo='$v_grupo' ";
}

if($v_subgrupo !=""){
	$v_filtro .= " AND pro_subgrupo='$v_subg rupo' ";
}

if($v_categoria !=""){
	$v_filtro .= " AND pro_categoria='$v_categoria' ";
}


$res = sql_insumos("*,sum(total) as stock"," ".$v_filtro." GROUP BY pro_codigo  order by pro_codigo asc "); 
if(mysqli_num_rows($res) > 0){
	$sw = 0;
	?>
	<table class="table table-striped table-bordered tabledrag table-condensed"> 
	<thead>
	<tr>   
		<th width="1">Código</th> 
		<th>Producto</th>
		<th>Categoría</th> 
		<th width="1">Stock</th>
		<th width="1">Minimo</th>
		<th width="1">xComprar</th>
		<th width="1">CostoPromedio</th>
		<th width="1">Total</th>
	</tr>				
	</thead>
	<tbody>
	<?
	while($row = mysqli_fetch_array($res)){  

		$cantidad =  $row["pro_stock_minimo"] - $row["stock"];
		
		if($cantidad > 0){
			$costo_promedio = $row["pro_valor"] * 1;
			$subtotal = $row["pro_inventario"] * 1;
			?>
			<tr>  
				<td><? echo $row["pro_codigo"]; ?></td> 
				<td><? echo $row["pro_nombre"]; ?></td>
				<td><? echo $_categorias[$row["pro_categoria"]]; ?></td> 
				<th style="text-align: center;"><? echo _num($row["stock"]); ?></th>
				<th style="text-align: center;"><? echo _num($row["pro_stock_minimo"]); ?></th>
				<th style="text-align: center;"><? echo _num($cantidad); ?></th>
				<td style="text-align: right;"><? echo _num($costo_promedio); ?></td>
				<td style="text-align: right;"><? echo _num($subtotal); ?></td>
			</tr>
			<? 
			$total_final += $subtotal;  
		}
	}
	?>  
	</tbody>
	</table>
	<h4 style="text-align: right">Total Final:<br><? echo _num($total_final); ?></h4>
	<?
}else{
	?>
	<div class="alert alert-danger">
		<strong>Sin registros creados</strong>
	</div>
	<?php 
} 
?>