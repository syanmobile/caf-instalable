<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabacion Usuario";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

$_POST["rut"] = strtoupper($_POST["rut"]);
switch($_POST["modo"]){
	case "crear": 
		$sql = "INSERT INTO personas ( 
			per_pfl_id, 
			per_nombre,
			per_tipo,
			per_correo, 
			per_usuario, 
			per_clave,  
			per_elim
		) VALUES ( 
			'$_POST[perfil]', 
			'$_POST[nombre]',
			'0',  
			'$_POST[correo]', 
			'$_POST[usuario]',
			'".md5($_POST["clave"])."', 
			'$_POST[estado]'
		)";  
		$res = mysqli_query($cnx,$sql); 
		$_POST["id"] = mysqli_insert_id($cnx); 
		?>
		<div class="alert alert-success"> 
			<strong>Persona '<?php echo $_POST["nombre"]; ?>' creado con &eacute;xito (Id: <?php echo $_POST["id"]; ?>)</strong>
		</div>
		<?php   
		$bod = $_REQUEST["bodega"];
		if(count($bod) > 0){
			foreach($bod as $bodega){
				$sql = "insert into personas_bodegas (per_id,bod_id) values ('$_POST[id]','$bodega')";
				$res = mysqli_query($cnx,$sql);
			}
		}    
		
		$ope = $_REQUEST["operacion"];
		if(count($ope) > 0){
			foreach($ope as $operacion){
				$sql = "insert into personas_operaciones (per_id,ope_id) values ('$_POST[id]','$operacion')";
				$res = mysqli_query($cnx,$sql);
			}
		} 
		break;
		
	case "editar":
		$SQL_ = "UPDATE personas SET ";  
		$SQL_.= "per_nombre = '$_POST[nombre]', "; 
		$SQL_.= "per_correo = '$_POST[correo]', "; 
		$SQL_.= "per_usuario = '$_POST[usuario]', ";  
		if($_POST["clave"] <> ""){
			$SQL_ .= "per_clave = '".md5($_POST["clave"])."', ";
		}
		$SQL_.= "per_elim = '$_POST[estado]', ";
		$SQL_.= "per_pfl_id = '$_POST[perfil]' ";
		$SQL_.= "WHERE per_id = '$_POST[id]' ";  
		$res = mysqli_query($cnx,$SQL_);
		
		$sql = "delete from personas_bodegas where per_id = '$_POST[id]' ";
		$res = mysqli_query($cnx,$sql);  
		
		$bod = $_REQUEST["bodega"];
		if(count($bod) > 0){
			foreach($bod as $bodega){
				$sql = "insert into personas_bodegas (per_id,bod_id) values ('$_POST[id]','$bodega')";
				$res = mysqli_query($cnx,$sql);
			}
		} 

		$sql = "delete from personas_operaciones where per_id = '$_POST[id]' ";
		$res = mysqli_query($cnx,$sql);  
		
		$ope = $_REQUEST["operacion"];
		if(count($ope) > 0){
			foreach($ope as $operacion){
				$sql = "insert into personas_operaciones (per_id,ope_id) values ('$_POST[id]','$operacion')";
				$res = mysqli_query($cnx,$sql);
			}
		} 
		?>   
        <div class="alert alert-success"> 
            <strong>Persona '<?php echo $_POST["nombre"]; ?>' editado con &eacute;xito (Id: <?php echo $_POST["id"]; ?>)</strong> 
        </div>  
		<?php  
		break;
		
	case "eliminar":
		$SQL_ = "UPDATE personas SET "; 
		$SQL_.= "per_elim = '1' ";
		$SQL_.= "WHERE per_id = '$_POST[id]' ";  
		$res = mysqli_query($cnx,$SQL_);
		?>   
        <div class="alert alert-success"> 
            <strong>Registro eliminado con &eacute;xito (Id: <?php echo $_POST["id"]; ?>)</strong> 
        </div>  
		<?php  
		break;
}

construir_boton("usu_listado.php","","buscar","Listado de Usuarios",2);
if($_REQUEST["modo"] <> "eliminar"){
	//construir_boton("per_ver.php","&id=".$_POST["id"],"buscar","Ver Ficha",2);
	construir_boton("usu_editar.php","&id=".$_POST["id"],"editar","Editar este Usuario",2);
}
construir_boton("usu_nuevo.php","","crear","Crear otro Usuario",2);
?>