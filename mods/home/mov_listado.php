<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Kardex Producto (Ajustes)";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "mov_listado.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?> 
<script language="javascript">
function cambiar_resp(id){
	$("[fila="+id+"]").fadeOut("normal");
	var resp = document.getElementById("resp_"+id).value;
	SoloEnviar(url_base+modulo_base+"mov_ajax.php","modo=cambiar&id="+id+"&resp="+resp);
	alert("Modificado");
}
function eliminar(id){
	if(confirm("Seguro de eliminar?")){
		$("[fila="+id+"]").fadeOut("normal");
		SoloEnviar(url_base+modulo_base+"mov_ajax.php","modo=eliminar_mov&id="+id);
	}
}
function eliminar2(id){
	if(confirm("Seguro de eliminar?")){
		$("[fila2="+id+"]").fadeOut("normal");
		SoloEnviar(url_base+modulo_base+"mov_ajax.php","modo=eliminar_det&id="+id);
	}
}
function detalle(id){
	var div = document.getElementById("detalle_"+id); 
	AJAXPOST(url_base+modulo_base+"mov_ajax.php","&modo=detalle&mov="+id,div);
} 
function cambiar(modo){
	if(modo == "pro"){
		$("#serie").val("");
	}
	carg("mov_listado.php","");
}
</script>
<table class="table  table-bordered"> 
<tr>
    <th width="1">Ficha:</th>
    <td colspan="3">
    <select name="codigo" id="codigo" class="campos buscador" onchange="javascript:cambiar('pro');">
    <option value=""></option>
	<?php
	// Sacar los productos con movimiento
	$res = sql_productos("*","  order by pro_nombre asc");
	$num = mysqli_num_rows($res);
	if($num > 0){
		while($row = mysqli_fetch_array($res)){
			?><option value="<?php echo $row["pro_codigo"]; ?>" <?php
			if($row["pro_codigo"] == $_REQUEST["codigo"]){ echo "selected"; } ?>><?php
			echo $row["pro_codigo"]." - ".$row["pro_nombre"];  
			?></option><?php 
		}
	}
    ?>
    </select></td>
</tr> 
</table>

<?php
if($_REQUEST["codigo"] <> ""){
	$res = sql_productos("*","where pro.pro_codigo = '$_REQUEST[codigo]'","");  
	if(mysqli_num_rows($res) > 0){
		$datos = mysqli_fetch_array($res); 
	}  
	$stock = 0;
	$res = sql_stock("*,sum(total) as stock"," 
	where pro_codigo = '".$datos["pro_codigo"]."'");
	if(mysqli_num_rows($res) > 0){
		$row = mysqli_fetch_array($res);
		$stock = $row["stock"]; 
	}
	?> 
    <table class="table table-bordered table-condensed"> 
    <thead>
    <tr>
        <th colspan="4">Informaci&oacute;n del Producto</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th width="1">Producto:</th>
        <td colspan="3"><strong><?php echo _t($datos["pro_codigo"]); ?></strong> - <?php echo _t($datos["pro_nombre"]); ?></td>
    </tr> 
    <tr>
        <th width="1">Stock:</th>
        <td width="1"><span class="label label-<?php
        if($stock >= $row["pro_stock_minimo"]){
            echo 'success';
        }else{
            echo 'danger';
        }
        ?>"><?php echo _num($stock); ?></span></td> 
        <th width="1">Minimo:</th>
        <td><?php echo _num($datos["pro_stock_minimo"]); ?></td>
    </tr>
    </tbody>
    </table>
    
    <table class="table table-bordered table-condensed table-info"> 
    <tbody>
    <tr>
       	<?php
		// Sacar los series de producto
		$res2 = sql_movimientos_detalle("det_lote"," where pro_codigo = '$_REQUEST[codigo]' group by pro_codigo,det_lote order by det_lote asc","","");
		$num2 = mysqli_num_rows($res2);
		if($num2 > 0){
			?>
			<th>Serie:</th>
			<td width="1">
			<select name="serie" id="serie" class="campos" onchange="javascript:cambiar('');">
			<option value="">- Todos -</option>
			<?
			while($row2 = mysqli_fetch_array($res2)){
				?><option value="<? echo $row2["det_lote"]; ?>" <? if($_REQUEST["serie"] == $row2["det_lote"]){ echo "selected"; } 
				?>><? echo $row2["det_lote"]; ?></option><?
			}
			?>
			</select></td>
			<?
		}
		?>
        <th width="1">Desde:</th>
        <td width="1"><input type="text" class="campos fecha" id="desde" name="desde" value="<?php
        if($_REQUEST["desde"] == ""){  
            $_REQUEST["desde"] =date('d/m/Y', strtotime('-3 month')) ;
        }
        echo $_REQUEST["desde"]; ?>" size="10"></td>
        <th width="1">Hasta:</th>
        <td width="1"><input type="text" class="campos fecha" id="hasta" name="hasta" value="<?php
        if($_REQUEST["hasta"] == ""){
            $_REQUEST["hasta"] = date("d/m/Y");
        }
        echo $_REQUEST["hasta"]; ?>" size="10"></td> 
        <td><?
        construir_boton("mov_listado.php","","refrescar","Recargar Informe");
        ?></td>
    </tr>
    </tbody>
    </table>
    
	<table class="table table-striped table-bordered tabledrag table-condensed"> 
	<thead>
		<tr>  
			<th style="text-align:center;" width="100"></th>
			<th style="text-align:center;" width="1">Fecha</th>
			<th style="text-align:center;" width="1">Folio</th> 
			<th width="150">Concepto</th>
			<th>Proveedor/Responsable</th>
			<th>Notas</th>
			<th>Autor</th>
        	<th>Detalles</th>   
			<th width="1">Entrada</th>
			<th width="1">Salida</th>
			<th width="1">Saldo</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$final = 0;
	$saldo = 0;  
	if($_REQUEST["serie"] <> ""){
		$extra = " and det.det_lote = '".$_REQUEST["serie"]."' ";
	}
	$res2 = sql_movimientos_detalle("*,sum(det_ingreso) as ingreso,sum(det_egreso) as egreso"," where pro_codigo = '$_REQUEST[codigo]' and mov.mov_fecha < '"._fec($_REQUEST["desde"],9)."' $extra group by mov.mov_id, pro_codigo 
	order by mov.mov_fecha asc, mov.mov_id asc");
	if(mysqli_num_rows($res2) > 0){
		while($row2 = mysqli_fetch_array($res2)){
			$saldo = $row2["ingreso"] - $row2["egreso"];
		} 
	}
	?> 
	<tr>
		<th style="text-align:right;" colspan="10">Saldo Inicial:</th>
		<th style="text-align:center;"><?php echo _num($saldo); ?></th>
	</tr>
	<?	
    $res = sql_movimientos_detalle("*,sum(det_ingreso) as ingreso,sum(det_egreso) as egreso"," 
    where pro_codigo = '$_REQUEST[codigo]'  and 
    mov.mov_fecha >= '"._fec($_REQUEST["desde"],9)."' and 
    mov.mov_fecha <= '"._fec($_REQUEST["hasta"],9)."' $extra 
	and (con.con_tipo = 'E' or con.con_tipo = 'S' or con.con_tipo = 'A') 
	and (ubi.ubi_tipo = 'ENT' or ubi.ubi_tipo = 'EXI') 
    group by mov.mov_id, pro_codigo 
	order by mov.mov_fecha asc, mov.mov_id asc ","",""); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){ 
			$saldo += $row["ingreso"];
			$saldo -= $row["egreso"]; 
			?>
			<tr id="<? echo $row["mov_id"]; ?>" fila="<? echo $row["mov_id"]; ?>">  
				<td style="text-align:center;"><?php
				if($row["mov_id"] <> ""){
					switch($row["con_tipo"]){
						case "E": construir_boton("visor('ing_pdf.php?id=".$row["mov_id"]."')","1","imprimir","Documento Ingreso #".$row["mov_id"],4); break;
						case "S": construir_boton("visor('sal_pdf.php?id=".$row["mov_id"]."')","1","imprimir","Documento Salida #".$row["mov_id"],4); break;
						case "A": construir_boton("visor('aju_pdf.php?id=".$row["mov_id"]."')","1","imprimir","Documento Ajuste #".$row["mov_id"],4); break;
					}
				}
				construir_boton("eliminar(".$row["mov_id"].")","1","eliminar","Eliminar Documento",4);
				construir_boton("detalle(".$row["mov_id"].")","1","buscar","Ver Detalle",4); 
				?></td>  
				<td style="text-align:center;"><?php echo _fec($row["mov_fecha"],5); ?></td>
				<td><?php echo $row["mov_folio"]; ?></td>  
				<td><?php echo $row["con_nombre"]; ?></td>   
				<td><?php
				if($row["aux_codigo"] <> ""){
					echo $row["aux_nombre"]; 
				}else{
					echo $_personas[$row["mov_responsable"]]; 
				}
				?></td>  
				<td><?php echo $row["mov_glosa"]; ?></td>  
				<td><?php echo nom_persona($row["mov_per_id"]); ?></td>
				<td><div id="detalle_<? echo $row["mov_id"]; ?>"></div></td>  
				
				<td style="text-align:center;"><?php echo _num($row["ingreso"]); ?></td>  
				<td style="text-align:center;"><?php echo _num($row["egreso"]); ?></td>  
				<td style="text-align:center;"><?php echo _num($saldo); ?></td>  
			</tr>
			<? 
        }
	}else{
		?>
		<tr>
			<td colspan="11">Sin movimientos entre las fechas</td>
		</tr>
		<?
	}
    ?>
	<tr>
		<th style="text-align:right;" colspan="10">Saldo Final:</th>
		<th style="text-align:center;"><?php echo _num($saldo); ?></th>
	</tr>
	</tbody>
	</table> 
    <? 
}
?>