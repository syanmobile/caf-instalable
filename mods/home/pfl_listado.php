<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Perfiles de Usuario";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "pfl_listado.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function eliminar(id){
     if(confirm("Seguro de eliminar?")){
          $("[fila="+id+"]").fadeOut("normal");
          SoloEnviar(url_base+modulo_base+"pfl_save.php","modo=eliminar&id="+id);
     }
}
</script>
<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td><?php
		construir_boton("pfl_nuevo.php","","crear","Nuevo Perfil",2);
		//construir_boton("pfl_editar_todos.php","","editar","Editar Todos los Perfiles",2);
		?></td>
    </tr>
</tbody>
</table>
<?php
$res = sql_perfiles("*"," ORDER BY pfl.pfl_nombre asc");
if(mysqli_num_rows($res) > 0){
    ?>
    <table class="table table-striped table-bordered table-condensed"> 
    <thead>
    <tr style="cursor: move;">
        <th width="70" align="center">Opciones</th>
        <th>Perfil de Usuario</th>
        <th>Personas con este perfil</th>
    </tr>
    </thead>
    <tbody> 
    <?
    while($row = mysqli_fetch_array($res)){
        $res2 = sql_personas("*"," and per.per_elim = 0 and per.per_pfl_id = '$row[pfl_id]' and per.per_id > 1 
		ORDER BY per.per_nombre asc");
		$num2 = mysqli_num_rows($res2);
		?>
        <tr fila="<?php echo $row["pfl_id"] ;?>" id="<?php echo $row["pfl_id"] ;?>">  
            <td style="text-align:center;"><?php
			construir_boton("pfl_editar.php","&id=".$row["pfl_id"],"editar","Editar"); 
			if($num2 > 0){
				construir_boton("alert('No se puede eliminar, personas asociadas.')","1","eliminar","Eliminar",4);
			}else{
				construir_boton("eliminar(".$row["pfl_id"].")","1","eliminar","Eliminar",4);
			}
			?></td>
            <td><?php echo $row["pfl_nombre"]; ?></td>
            <td style="padding-left:15px;"><?php
			if($num2 > 0){
				while($row2 = mysqli_fetch_array($res2)){
					echo "<li>".$row2["per_nombre"]."</li>";
				}
			}
			?></td>
        </tr>
        <? 
    }
    ?> 
    </tbody>
    </table>  
    <?
	echo $paginador_dibujo;
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin registros creados</strong>
    </div>
    <?
}
?>