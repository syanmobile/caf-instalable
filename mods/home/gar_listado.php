<?php 
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Gestion de Garantías de Activos Fijos";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "gar_listado.php"; 

//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function excel(){
	window.open("mods/home/acf_garantia.php?excel=S");
}
</script>
<table width="100%" class="tabla_opciones">
<tbody> 
	<tr>
		<td><?php 
		construir_boton("gar_nuevo.php","","crear","Crear Garantía",2); 
		//construir_boton("excel()","","derecha","Exportar Mantenimientos",4); 
		?>
<table class="table table-bordered table-condensed table-info" style="margin-bottom:0px !important;">  
<tbody> 
<tr>
    <td width="90">
	    <b>Folio:</b><br />
	    <input type="text" name="fil_folio" id="fil_folio" class="campos w10" value="<? echo $_REQUEST["fil_folio"]; ?>">
	</td> 

    <td>
	    
	</td>    
    <td width="1"><?
	construir_boton("gar_listado.php","","buscar","Filtrar",2); 
	?></td> 
</tr>
</tbody>
</table>

</td>
</tr>
</tbody>
</table> 
<?php 
/****************************************************************************************/
if($_REQUEST["fil_folio"] <> ""){
	$filtros .= " and gar.gar_id = '".$_REQUEST["fil_folio"]."' ";
}
if($_REQUEST["fil_fecha_ini"] <> ""){
	$filtros .= " and gar.gar_fecha_ini = '".$_REQUEST["fil_fecha_ini"]."' ";
}
?>
<input type="hidden" name="pagina" id="pagina" value="man_listado.php" class="campos">
<? 
$sql_paginador = sql_garantias("*"," $filtros ","s",""); 

$cant_paginador = 20;
require("../../_paginador.php");
$res3 = mysqli_query($cnx,$sql_final);  
if(mysqli_num_rows($res3) > 0){ 
	?>
	<table class="table table-bordered table-condensed">
	<thead>
	<tr>
		<th></th>
		<th>Folio</th>
		<th>Proveedor</th>
		<th>Vencimiento</th>
		<th>Poliza </th>
		<th>Titulo</th>
		<th>Notas</th>
		<th>Responsable</th>
		<th>Estado</th>
		<th>Costo</th>
	</tr>
	</thead>  
	<tbody>
	<?
	while($row3 = mysqli_fetch_array($res3)){
		//_p($row3);
		?>
		<tr>
			<?
			if($_REQUEST["excel"] == ""){
				?><td><? 
				construir_boton("gar_editar.php","&gar=".$row3["gar_id"],"editar","Editar"); 
				//construir_boton("visor('gar_pdf.php?id=".$row3["man_id"]."')","1","imprimir","Imprimir",4);
				?></td><?
			}
			?>
			<th style="text-align: center"><? echo $row3["gar_id"]; ?></th>
			<th><? echo $row3["aux_nombre"]; ?></th>
			<th><? echo _fec($row3["gar_fecha_fin"],5); ?></th>
			<td><? echo $row3["gar_poliza"]; ?></td>
			<td><? echo $row3["gar_titulo"]; ?></td> 
			<td><? echo $row3["gar_notas"]; ?></td> 
			<td><? echo $_personas[$row3["gar_per_id"]]; ?></td>
		
			<td><?
			switch($row3["gar_estado"]){
				case "ACT": echo "Activa"; break;
				case "VEN": echo "Vencida"; break;
				case "NUL": echo "Nula"; break;
			} 
			?></td> 
			<td style="text-align: right;"><? echo _num($row3["gar_costo"]); ?></td>
		</tr>
		<? 
	}
	?> 
	</tbody>
	</table>
	<?
	echo $paginador_dibujo;
}else{
	?>
	<div class="alert alert-info">
		Sin Garantías registradas
	</div>
	<?
} 
?>