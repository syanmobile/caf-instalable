<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Productos";
construir_breadcrumb($titulo_pagina);
//---------------------------------------------------------------------------------------- 
$pagina = "pro_listado.php"; 
include("ayuda.php");
//---------------------------------------------------------------------------------------- 

// Arreglo de los filtros en esta pagina
$campos_filtros = array("fil_codigo","fil_descripcion","fil_categoria","fil_tipo");
if($_REQUEST["mantener_filtros"] == ""){
	foreach($campos_filtros as $campito){ 
		$_REQUEST["".$campito] = $_SESSION["filtros"][$pagina][$campito]; 
	}	
}else{
	foreach($campos_filtros as $campito){
		$_SESSION["filtros"][$pagina][$campito] = $_REQUEST[$campito];		
	}
}
?>
<input type="hidden" name="mantener_filtros" value="S" class="campos">

<script language="javascript">
function imprimir(){
	var a = $(".campos").fieldSerialize();
	visor("pro_cod_barra.php?modalidad=varios_prod&"+a);
}
function excel(){
	var a = $(".campos").fieldSerialize();
	window.open("mods/home/pro_excel.php?"+a);
}
</script>

<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td><?php
		construir_boton("pro_nuevo.php","","crear","Nuevo Producto",2);  
		construir_boton("pro_importar.php","","importar","Importar Productos",2);  
		?></td>
    </tr>
</tbody>
</table> 

<table class="table table-bordered table-condensed table-info" style="margin-bottom:0px !important;">  
<tbody> 
<tr>
    <td width="100">
    <b>Código:</b><br />
    <input type="text" name="fil_codigo" id="fil_codigo" class="campos w10" value="<? echo $_REQUEST["fil_codigo"]; ?>" placeholder="Usar % para búsquedas"></td> 
    <td>
    <b>Nombre Producto:</b><br />
    <input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? echo $_REQUEST["fil_descripcion"]; ?>" placeholder="Usar % para búsquedas"></td> 
    <td width="500">
    <b>Clasificación:</b><br />
    <select name="fil_categoria" id="fil_categoria" class="campos buscador w10">
    <option value=""></option>
    <?php
    $res = sql_categorias("*"," order by cat.cat_codigo asc"); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["cat_codigo"]; ?>" <?
            if($_REQUEST["fil_categoria"] == $row["cat_codigo"]){ echo "selected"; } 
            ?>>C<? echo $row["cat_codigo"]." - ".$row["cat_nombre_largo"]; ?></option>
            <?
        }
    }
    ?></select></td>    
    <td width="150">
    <b>Tipo Activo:</b><br />
    <select name="fil_tipo" id="fil_tipo" class="campos buscador w10">
    <option value=""></option>
    <?php 
	foreach($_tipo_activos as $tp){
		if(in_array("act_".strtolower($tp)."_ver",$opciones_persona)){
			?><option value="<? echo $tp; ?>" <? if($_REQUEST["fil_tipo"] == $tp){ echo "selected"; } ?>><?
			echo $_tipo_activo[$tp]; ?></option>
			<?
		}
	}
	?>
    </select></td>  
    <td width="180">
    <b>Ordenar:</b><br />
    <?
    if($_REQUEST["ordenar"] == ""){
    	$_REQUEST["ordenar"] = "pro.pro_codigo asc";
    }
    ?>
    <select name="ordenar" id="ordenar" class="campos w10">
	    <option value="pro.pro_codigo asc" <? 
	    if($_REQUEST["ordenar"] == "pro.pro_codigo asc"){ echo "selected"; } ?>>Código (Asc)</option> 
	    <option value="pro.pro_codigo desc" <? 
	    if($_REQUEST["ordenar"] == "pro.pro_codigo desc"){ echo "selected"; } ?>>Código (Desc)</option>
	    <option value="pro.pro_nombre asc" <? 
	    if($_REQUEST["ordenar"] == "pro.pro_nombre asc"){ echo "selected"; } ?>>Nombre Producto (Asc)</option>
	    <option value="pro.pro_nombre desc" <? 
	    if($_REQUEST["ordenar"] == "pro.pro_nombre desc"){ echo "selected"; } ?>>Nombre Producto (Desc)</option>
	    <option value="cat.cat_codigo asc" <? 
	    if($_REQUEST["ordenar"] == "cat.cat_codigo asc"){ echo "selected"; } ?>>Código Categoría (Asc)</option>
	    <option value="cat.cat_codigo desc" <? 
	    if($_REQUEST["ordenar"] == "cat.cat_codigo desc"){ echo "selected"; } ?>>Código Categoría (Desc)</option>
	    <option value="cat.cat_nombre asc" <? 
	    if($_REQUEST["ordenar"] == "cat.cat_nombre asc"){ echo "selected"; } ?>>Nombre Categoría (Asc)</option>
	    <option value="cat.cat_nombre desc" <? 
	    if($_REQUEST["ordenar"] == "cat.cat_nombre desc"){ echo "selected"; } ?>>Nombre Categoría (Desc)</option>
    </select></td>
    <td width="1"><?
	construir_boton("pro_listado.php","","buscar","Filtrar");
	?></td> 
</tr>
</tbody>
</table> 
<?php 
/****************************************************************************************/
if($_REQUEST["fil_codigo"] <> ""){
	$filtros .= " and pro.pro_codigo like '".$_REQUEST["fil_codigo"]."' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and pro.pro_nombre like '".$_REQUEST["fil_descripcion"]."' ";
}
if($_REQUEST["fil_barra"] <> ""){
	$filtros .= " and pro.pro_codigo_barra like '".$_REQUEST["fil_barra"]."' ";
}
for($i = 1;$i <= 20;$i++){
	if($_REQUEST["fil_extra_".$i] <> ""){
		$filtros .= " and pro.pro_extra_".$i." like '".$_REQUEST["fil_extra_".$i]."' ";
	}
}
if($_REQUEST["fil_unidad"] <> ""){
	$filtros .= " and pro.pro_unidad = '".$_REQUEST["fil_unidad"]."' ";
}
if($_REQUEST["fil_categoria"] <> ""){
	$filtros .= " and cat.cat_codigo like '".$_REQUEST["fil_categoria"]."%' ";
}
if($_REQUEST["fil_tipo"] <> ""){
	$filtros .= " and pro.pro_tipo = '".$_REQUEST["fil_tipo"]."' ";
}
$filtros .= " and pro.pro_tipo <> 'INS' ";
?>
<input type="hidden" name="pagina" id="pagina" value="pro_listado.php" class="campos">
<?
$sql_paginador = sql_productos("*","   $filtros ORDER BY ".$_REQUEST["ordenar"],"s"); 
$cant_paginador = 20;
require("../../_paginador.php");
$res = mysqli_query($cnx,$sql_final);
if(mysqli_num_rows($res) > 0){
	?>  
	<table class="table table-bordered table-hover table-condensed"> 
	<thead>
	<tr> 
		<th width="120"></th>  
		<th width="1">Código</th> 
		<th>Producto</th>  
		<th>Categoría</th>
		<th>Unidad</th>
		<th>Tipo Activo</th> 
		<?
		for($iex = 1;$iex <= 10;$iex++){				 
            $res2 = sql_campos("*"," and cam_producto = $iex order by cam_nombre asc");  
			if(mysqli_num_rows($res2) > 0){
				while($row2 = mysqli_fetch_array($res2)){
					$extras[] = $iex;
					?><th><? echo $row2["cam_nombre"]; ?></th><?
				}
			}
		}
		?>
		<th width="1">Prestable</th>
		<th width="1">Asignable</th>   
		<th width="1" title="Requiere Mantenimiento">Mantención</th>
		<th width="1" title="Maneja Vencimiento">Garantia</th> 
	</tr>
	</thead>
	<tbody> 
	<?php 
	while($row = mysqli_fetch_array($res)){ 
		?>
		<tr>    
			<td><?
			construir_boton("pro_editar.php","&id=".$row["pro_id"],"editar","Editar Producto");
			construir_boton("pro_ver.php","&id=".$row["pro_id"],"buscar","Ver Producto");  
			construir_boton("inf_kardex_producto.php","&producto=".$row["pro_id"],"tabla","Kardex Producto"); 
			construir_boton("pro_nuevo.php","&id=".$row["pro_id"],"nota_editar","Clonar Producto");
			?></td>  
			<th><?php echo $row["pro_codigo"]; ?></th>  
			<td><?php echo $row["pro_nombre"]; ?></td>  
			<td><?php 
			if($row["pro_categoria"] <> 0){
				echo "C".$row["cat_codigo"]." - ".$_categorias_largo[$row["pro_categoria"]]; 
			}
			?></td>  
			<td><?php echo $_unidades[$row["pro_unidad"]]; ?></td>
			<td><?php echo $_tipo_activo[$row["pro_tipo"]]; ?></td> 
			<?
			foreach($extras as $iex){				 
	            ?><td><? echo $row["pro_extra_".$iex]; ?></td><?
	        }
			?>
			<td style="text-align:center"><?php echo ($row["pro_prestable"] == "1")?"SI":"-"; ?></td> 
			<td style="text-align:center"><?php echo ($row["pro_asignable"] == "1")?"SI":"-"; ?></td> 
			<td style="text-align:center"><?php echo ($row["pro_mantenimiento"] == "1")?"SI":"-"; ?></td> 
			<td style="text-align:center"><?php echo ($row["pro_garantia"] == "1")?"SI":"-"; ?></td>        
		</tr>
		<?php 
	}
	?> 
	</tbody>
	</table>  
	<?php
	echo $paginador_dibujo;
}else{
	?>
	<div class="alert alert-danger">
		<strong>Sin resultados</strong>
	</div>
	<?php 
} 
?>