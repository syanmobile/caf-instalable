<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
construir_breadcrumb("Depreciación de Activos Fijos");
//----------------------------------------------------------------------------------------
$pagina = "dep_listado.php"; 

include("ayuda.php");
//----------------------------------------------------------------------------------------
?>
<script type="text/javascript">
$(document).ready(function() { 
	//Checkbox
	$("input[name=checktodos1]").change(function(){
		var valor = $("input[name=checktodos1]:checked").length;
		if(valor){
			 $('input[alt1=1]').each( function() {
				  this.checked = true;
			 });
		}else{
			 $(".cdus").removeAttr("checked");
		} 
	}); 
});
function editar_seleccionados(){
	var a = $(".cdus").fieldSerialize();
	if(a == ""){
		alert("Seleccione algún activo");
		return;
	} 
	AJAXPOST(url_base+modulo_base+"dep_ajax.php",a+"&modo=seleccionados",document.getElementById("modGeneral_lugar"),false,function(){			
			$('#modGeneral').modal('show');
		});
}	
function depreciar_seleccionados(){
	var a = $(".cdus").fieldSerialize();
	if(a == ""){
		alert("Seleccione algún activo");
		return;
	} 
	AJAXPOST(url_base+modulo_base+"dep_ajax.php",a+"&modo=depreciar_seleccionados",document.getElementById("modGeneral_lugar"),false,function(){			
			$('#modGeneral').modal('show');
		});
}	
</script>

<input type="hidden" name="mantener_filtros" value="S" class="campos">

<table width="100%">
<tr valign="top">
	<td width="250">
		<table class="table table-bordered table-condensed">  
		<thead>
		<tr>
			<th>Filtre los activos</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>
			<b>Código Activo:</b><br>
			<input type="text" name="fil_codigo2" id="fil_codigo2" class="campos w10" value="<? 
			echo $_REQUEST["fil_codigo2"]; ?>">

			<b>Código Producto:</b><br>
			<input type="text" name="fil_codigo" id="fil_codigo" class="campos w10" value="<? 
			echo $_REQUEST["fil_codigo"]; ?>"> 

			<b>Descripción:</b><br>
			<input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? 
			echo $_REQUEST["fil_descripcion"]; ?>">

			<b>Serie:</b><br>
			<input type="text" name="fil_serie" id="fil_serie" class="campos w10" value="<? 
			echo $_REQUEST["fil_serie"]; ?>">

			<b>Etiqueta:</b><br>
			<input type="text" name="fil_etiqueta" id="fil_etiqueta" class="campos w10" value="<? 
			echo $_REQUEST["fil_etiqueta"]; ?>"> 

			<b>Tipo (Depreciables):</b><br>
			<select name="fil_tipo" id="fil_tipo" class="campos buscador">
			<option value=""></option>  
			<?php
			$res = mysqli_query($cnx,"select * from activos_tipos where tac_depreciable = 1 order by tac_orden asc");
			if(mysqli_num_rows($res) > 0){
				while($row = mysqli_fetch_array($res)){							
					?><option value="<? echo $row["tac_codigo"]; ?>" <? 
					if($_REQUEST["fil_tipo"] == $row["tac_codigo"]){ echo "selected"; } ?>><?
					echo $row["tac_nombre"]; ?></option>
					<?
				}
			}
			?>
			</select> 
			</td>
		</tr> 
	</tbody>
	</table>
	<table width="100%">
	<tr>
		<td><? construir_boton("dep_listado.php","","filtrar","Filtrar Activos",2); ?></td>
		<td width="1"><? construir_boton("dep_listado.php","&fil_tipo=".$_REQUEST["fil_tipo"]."&limpiar=S","limpiar","Limpiar",8); ?></td>
	</tr>
	</table>
</td>
<td style="padding-left: 10px;">

<?php 
/****************************************************************************************/
if($_REQUEST["fil_codigo"] <> ""){
	$filtros .= " and pro.pro_codigo like '".$_REQUEST["fil_codigo"]."' ";
}
if($_REQUEST["fil_codigo2"] <> ""){
	$filtros .= " and acf.acf_codigo like '".$_REQUEST["fil_codigo2"]."' ";
}
if($_REQUEST["fil_serie"] <> ""){
	$filtros .= " and acf.acf_serie like '".$_REQUEST["fil_serie"]."' ";
}
if($_REQUEST["fil_etiqueta"] <> ""){
	$filtros .= " and acf.acf_etiqueta like '".$_REQUEST["fil_etiqueta"]."' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and pro.pro_nombre like '".$_REQUEST["fil_descripcion"]."' ";
} 
if($_REQUEST["fil_unidad"] <> ""){
	$filtros .= " and pro.pro_unidad = '".$_REQUEST["fil_unidad"]."' ";
}    
if($_REQUEST["fil_tipo"] <> ""){
	$filtros .= " and pro.pro_tipo = '".$_REQUEST["fil_tipo"]."' ";
}   
if($_REQUEST["fil_estado"] <> ""){
	$filtros .= " and acf.acf_disponible = '".$_REQUEST["fil_estado"]."' ";
}
if($filtros <> ""){

	$res = sql_activos_fijos_rapido("*"," $filtros ORDER BY acf.acf_producto asc, acf.acf_serie asc ");  
	if(mysqli_num_rows($res) > 0){
		?>
		<table class="table table-bordered table-condensed">
		<tbody>
			<tr>
				<td><?
				construir_boton("editar_seleccionados()","","editar","Editar activos seleccionados",4);
				construir_boton("editar_todos()","","editar","Editar todos los activos encontrados",4);
				construir_boton("depreciar_seleccionados()","","peso","Depreciación de activos seleccionados",4);
				?></td>
				<td width="130" style="text-align: right;">
					Activos Encontrados:<br><b><? echo _num2(mysqli_num_rows($res)); ?></b>
				</td> 
			</tr>
		</tbody>
		</table>

		<table class="table table-striped table-bordered tabledrag table-condensed" style="margin-bottom: 0px;"> 
		<thead>
			<tr>  
				<th width="1"><input name="checktodos1" type="checkbox"></th> 
				<th width="1">Cod.Activo</th>
				<th>Producto</th>
				<th width="1">Serie</th>
				<th width="1">Valor</th>
				<th width="1" title="Tipo Depreciación">TipoDep</th>
				<th width="1" title="Vida Util">V.Util</th>
				<th width="1" title="Valor Residual">V.Residual</th>
				<th width="1" title="Inicio Revalorización">InicioRev</th>
				<th width="1" title="Inicio Depreciación">InicioDep</th>
				<th width="1" title="Tipo Adquisición">TipoAdq</th>
			</tr>
		</thead>
		<tbody> 
		<?
		while($row = mysqli_fetch_array($res)){  
			?>
			<tr> 
				<td><input type="checkbox" name="acf[]" alt1="1" class="campos cdus" value="<? 
				echo $row["acf_id"]; ?>"></td> 
				<th><?php echo $row["acf_codigo"]; ?></th>
				<td><?php echo $row["pro_nombre"]; ?></td>
				<td><?php echo $row["acf_serie"]; ?></td>
				
				<td><?php echo _num($row["acf_valor"]); ?></td>
				<td><?php echo ($row["acf_tipo_depreciacion"] == "L")?"Lineal":"Acelerada"; ?></td>
				<td><?php echo $row["acf_vida_util"]; ?></td>
				<td><?php echo _num($row["acf_valor_residual"]); ?></td>
				<td><?php echo $row["acf_mes_ano_inicio_revalorizacion"]; ?></td>
				<td><?php echo $row["acf_mes_ano_inicio_depreciacion"]; ?></td>
				<td><?php echo ($row["acf_tipo_adquisicion"] == "P")?"Propio":"En Leasing"; ?></td> 
			</tr>
			<?  
		}
		?>
		</tbody>
		</table> 
		<? 
	}else{
		?>
		<div class="alert alert-danger">
			<strong>Sin registros creados</strong>
		</div>
		<?php 
	} 
	?>
	</td>
	</tr>
	</table>
	<?
}else{
	?>
	<div class="alert alert-info">
		<strong>Realice un filtro</strong>
	</div>
	<?php 
} 
?>