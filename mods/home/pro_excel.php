<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition:  filename=\"pro_excel.xls\";");

require("../../inc/conf_dentro.php");

/***************************************************************************************/ 
$res2 = sql_stock("*,sum(total) as stock"," 
where (ubi_tipo = 'EXI' or ubi_tipo = 'ENT' ) 
group by pro_codigo ");
if(mysqli_num_rows($res2) > 0){
	while($row2 = mysqli_fetch_array($res2)){
		$stocks[$row2["pro_codigo"]] = ($row2["stock"] * 1);
	}
}
/****************************************************************************************/
if($_REQUEST["fil_codigo"] <> ""){
	$filtros .= " and pro.pro_codigo like '".$_REQUEST["fil_codigo"]."' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and pro.pro_nombre like '".$_REQUEST["fil_descripcion"]."' ";
}
if($_REQUEST["fil_barra"] <> ""){
	$filtros .= " and pro.pro_codigo_barra like '".$_REQUEST["fil_barra"]."' ";
}
for($i = 1;$i <= 20;$i++){
	if($_REQUEST["fil_extra_".$i] <> ""){
		$filtros .= " and pro.pro_extra_".$i." like '".$_REQUEST["fil_extra_".$i]."' ";
	}
}
if($_REQUEST["fil_unidad"] <> ""){
	$filtros .= " and pro.pro_unidad = '".$_REQUEST["fil_unidad"]."' ";
}
if($_REQUEST["fil_grupo"] <> ""){
	$filtros .= " and pro.pro_grupo = '".$_REQUEST["fil_grupo"]."' ";
}
if($_REQUEST["fil_subgrupo"] <> ""){
	$filtros .= " and pro.pro_subgrupo = '".$_REQUEST["fil_subgrupo"]."' ";
}
if($_REQUEST["fil_categoria"] <> ""){
	$filtros .= " and pro.pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
}
if($_REQUEST["fil_tipo"] <> ""){
	$filtros .= " and pro.pro_tipo = '".$_REQUEST["fil_tipo"]."' ";
}

$res = sql_productos("*","   $filtros ORDER BY pro.pro_nombre asc"); 
if(mysqli_num_rows($res) > 0){
	?>  
	<table border="1" style="font-family:arial; font-size:12px;" cellpadding="2" cellspacing="0"> 
	<thead>
	<tr> 
        <th>Codigo Producto</th>
        <th>Descripcion</th>
        <th>Cod.Barra</th>
        <th>Unidad</th>
        <th>Grupo</th>
        <th>Subgrupo</th>
        <th>Categoria</th> 
        <th>Extra 1</th>
        <th>Extra 2</th>
        <th>Extra 3</th>
        <th>Extra 4</th>
        <th>Extra 5</th>
        <th>Extra 6</th>
        <th>Extra 7</th>
        <th>Extra 8</th>
        <th>Extra 9</th>
        <th>Extra 10</th>
        <th>Stock Minimo</th>
        <th>Tipo</th>
        <th>M3</th>
        <th>Stock</th>
	</tr>
	</thead>
	<tbody> 
	<?php 
	while($row = mysqli_fetch_array($res)){ 
		$stock = ($stocks[$row["pro_codigo"]] * 1);
		?>
		<tr>  
			<th style='mso-number-format:"@"'><?php echo $row["pro_codigo"]; ?></th>
			<td style='mso-number-format:"@"'><?php echo $row["pro_nombre"]; ?></td>
			<td style='mso-number-format:"@"'><?php echo $row["pro_codigo_barra"]; ?></td> 
			<td style='mso-number-format:"@"'><?php echo $row["pro_unidad"]; ?></td> 
			<td style='mso-number-format:"@"'><?php echo $row["pro_grupo"]; ?></td> 
			<td style='mso-number-format:"@"'><?php echo $row["pro_subgrupo"]; ?></td> 
			<td style='mso-number-format:"@"'><?php echo $row["pro_categoria"]; ?></td>  
			<?php
            for($i = 1;$i <= 20;$i++){
                ?><td style='mso-number-format:"@"'><?php echo $row["pro_extra_".$i]; ?></td><?
            }
            ?> 
			<td style='mso-number-format:"0"'><?php echo $row["pro_stock_minimo"]; ?></td> 
			<td style='mso-number-format:"@"'><?php echo $row["pro_tipo"]; ?></td> 
			<td style='mso-number-format:"0"'><?php echo $row["pro_m3"]; ?></td> 
			<td style='mso-number-format:"0"'><?php echo $stock; ?></td> 
		</tr>
		<?php 
	}
	?> 
	</tbody>
	</table>  
	<?php 
}
?>