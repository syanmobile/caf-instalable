<?php
require("../../inc/conf_dentro.php");

switch($_REQUEST["modo"]){
	case "elegir_pro": 
		$res = sql_productos("*","and pro.pro_id = '$_REQUEST[producto]'","");  
		if(mysqli_num_rows($res) > 0){
			$datos = mysqli_fetch_array($res); 

			$row_ta = mysqli_fetch_array(mysqli_query($cnx,"select * from activos_tipos where tac_codigo = '$datos[pro_tipo]' "));
            if($row_ta["tac_depreciable"]){
				if(_opc("tipo_depreciacion") == "L"){
					$meses = $row_ta["tac_vu_dep_lineal"] * 12;
				}else{
					$meses = $row_ta["tac_vu_dep_acele"] * 12;
				}
			}
		}else{ 
			exit();
		}
		?>
		<script type="text/javascript">		
		function depr(valor){
			if(valor == ""){
				$("#box_depr").hide();
			}else{
				if(valor == "L"){
					document.getElementById("meses").value = "<? echo $row_ta["tac_vu_dep_lineal"] * 12; ?>";
				}else{
					document.getElementById("meses").value = "<? echo $row_ta["tac_vu_dep_acele"] * 12; ?>";
				}
				$("#box_depr").show();
			}
		}
		</script>

		<table class="table table-bordered table-hover table-condensed" style="margin: 0px !important;">
        <tbody> 
        	<tr>
        		<th width="80" style="text-align: right;">Cantidad:</th>
	        	<td width="80"><input type="text" name="cantidad" id="cantidad" class="campos w10" value="1" style="text-align: center;" onchange="javascript:calcular();"></td>
	        	<th width="1">Precio:</th>
                <td width="90"><input type="text" name="precio" id="precio" style="text-align:right" value="1" class="campos w10" onchange="javascript:calcular();"></td>
	        	<th width="1">Total:</th>
                <td width="90"><input type="text" name="stot" id="stot" style="text-align:right" class="campos w10" value="1" readonly></td>
                <?
                if($row_ta["tac_depreciable"]){
                	?>
                	<th width="1">Depreciación:</th>
        			<td width="1"><select name="tipo_depreciacion" id="tipo_depreciacion" class="campos" onchange="javascript:depr(this.value);">
        			<option value=""></option>
                    <option value="L" <?php if(_opc("tipo_depreciacion") == "L"){
                        echo "selected";
                    } ?>>Lineal</option>
                    <option value="A" <?php if(_opc("tipo_depreciacion") == "A"){
                        echo "selected";
                    } ?>>Acelerada</option>
	                </select></td>
	                <? 
	            }else{
	            	?>
					<input type="hidden" id="tipo_depreciacion">
	            	<?
	            }
	            ?> 
	            <td></td>
        	</tr> 
        </tbody>
    	</table>
    	<?
		if($datos["pro_tipo"] <> "INS"){ 
			?> 
			<table class="table table-bordered table-hover table-condensed" style="margin: 0px !important; <? 
			if($row_ta["tac_depreciable"] == 0){ echo 'display: none;'; } ?>" id="box_depr">
			<tbody>
			<tr>  
	        	<th width="80" style="text-align: right;">Meses Dep.:</th>
	        	<td width="80"><input type="text" name="meses" id="meses" value="<?  
	        	echo $meses; ?>" placeholder="(meses)" class="campos numero2 w10" style="text-align: center;"></td>
	        	<th width="90" style="text-align:right;">Valor Residual:</th>
	        	<td width="90"><input type="text" name="residual" id="residual" class="campos numero2 w10" style="text-align: right;" value="0"></td>
	        	<th width="1">Adquisición:</th>
        		<td width="1"><select name="adquisicion" id="adquisicion" class="campos">
    			<option value="P">Propio</option>
                <option value="L">Leasing</option>
                </select></td> 	
                <td><? construir_boton("previsualizar();","","buscar","Ver Depreciación",4); ?></td>
            </tr>	
	        </tbody>
	    	</table>	

	    	<script type="text/javascript">    		
			$(document).ready(function(){
				$('.numero2').priceFormat({
					prefix: '',
					centsLimit: 0,
					centsSeparator: ',',
					thousandsSeparator: '.'
				});
			}); 
	    	</script> 
        	<? 
		}else{
			?>
			<input type="hidden" id="garantia">
			<input type="hidden" id="tipo_depreciacion">
			<input type="hidden" id="meses">
			<input type="hidden" id="residual">
			<input type="hidden" id="adquisicion">
			<?
		} 
		?>
		<table width="100%" style="margin: 0px;">
		<tr><td style="padding: 5px 0px;"><? construir_boton("agregar_linea();","","crear","Agregar Linea",4); ?></td></tr>
		</table>
		<?
		break; 

	case "agregar_linea":
		?>
        <script language="javascript">
		document.getElementById("prod_det").value = document.getElementById("prod_det").value + ";<?php 
		echo $_REQUEST["producto"]; ?>";
		document.getElementById("lote_det").value = document.getElementById("lote_det").value + ";";
		document.getElementById("depr_det").value = document.getElementById("depr_det").value + ";<?php
		if($_REQUEST["tipo_depreciacion"] <> ""){ 
			echo $_REQUEST["tipo_depreciacion"].",";
			echo $_REQUEST["meses"].","; 
			echo $_REQUEST["residual"].","; 
			echo $_REQUEST["adquisicion"].",";
		}
		?>";
		document.getElementById("cant_det").value = document.getElementById("cant_det").value + ";<?php 
		echo $_REQUEST["cantidad"]; ?>";
		document.getElementById("prec_det").value = document.getElementById("prec_det").value + ";<?php 
		echo str_replace(".","",$_REQUEST["precio"]); ?>"; 
		detalle();
        </script>
        <?php 
		break;

	case "detalle": 
		?>
        <input type="hidden" name="prod_det" id="prod_det" value="<?php echo $_REQUEST["prod_det"]; ?>" class="campos">
        <input type="hidden" name="depr_det" id="depr_det" value="<?php echo $_REQUEST["depr_det"]; ?>" class="campos">
        <input type="hidden" name="lote_det" id="lote_det" value="<?php echo $_REQUEST["lote_det"]; ?>" class="campos">
        <input type="hidden" name="cant_det" id="cant_det" value="<?php echo $_REQUEST["cant_det"]; ?>" class="campos"> 
        <input type="hidden" name="prec_det" id="prec_det" value="<?php echo $_REQUEST["prec_det"]; ?>" class="campos"> 

        <?php
		$prod_det = explode(";",$_REQUEST["prod_det"]);
		$depr_det = explode(";",$_REQUEST["depr_det"]);
		$lote_det = explode(";",$_REQUEST["lote_det"]);
		$cant_det = explode(";",$_REQUEST["cant_det"]);
		$prec_det = explode(";",$_REQUEST["prec_det"]); 
		for($i = 1;$i < count($prod_det);$i++){
			
			$res = sql_productos("*"," and pro.pro_id = '".$prod_det[$i]."'"); 
			$prod = mysqli_fetch_array($res);
			
			if($cant_det[$i] > 0){
				$lineas++;

				if($cabe == ""){
					?>
					<div style="margin-top: 15px; clear: both;"></div>
					<table class="table table-bordered table-hover table-condensed">
			        <thead>
			        <tr> 
			            <th>Producto</th>  
			            <th width="200">Depreciación</th> 
			            <th width="150">Series</th>
			            <th width="1" style="text-align:center;">Cantidad</th>
			            <th width="80" style="text-align:right;">Valor</th>
			            <th width="90" style="text-align:right;">Total</th>
			            <th width="1"></th>
			        </tr>
			        </thead>
			        <tbody>
					<?
					$cabe = "S";
				}

				// Subtotal
				$subtotal = $prec_det[$i] * $cant_det[$i];
				$total += $subtotal;

				if($prod["pro_tipo"] == "INS"){
					?>
					<tr>
						<td><?php echo $prod["pro_nombre"]; ?><br>
						<b><?php echo $prod["pro_codigo"]; ?></b></td>
						<td colspan="2"></td> 
						<td style="text-align:center;" onclick="javascript:edicion_rapida2(<?php echo $i; ?>,'cant',<?php echo $cant_det[$i]; ?>);"><?php echo _num($cant_det[$i]); ?></td>
						<td style="text-align:right;" onclick="javascript:edicion_rapida2(<?php echo $i; ?>,'prec',<?php echo $prec_det[$i]; ?>);"><?php echo _num($prec_det[$i]); ?></td>
						<td style="text-align:right;"><?php echo _num($subtotal); ?></td>
						<td><?php
						construir_boton("quitar_linea('".$i."');","1","eliminar","Borrar Linea",4);
						?></td> 
					</tr>
					<?
				}else{
					?>
					<tr> 
						<td><?php echo $prod["pro_nombre"]; ?><br>
						<b><?php echo $prod["pro_codigo"]; ?></b></td>
						<td onclick="javascript:editar_dep(<? echo $i; ?>,'<? echo $depr_det[$i]; ?>','<?
						echo $prec_det[$i]; ?>');"><?php
						if($depr_det[$i] <> ""){ 
							$de = explode(",",$depr_det[$i]);
							echo "<b>Dep:</b> ";
							switch($de[0]){
								case "L": echo "Lineal"; break;
								case "A": echo "Acumulada"; break;
							}
							echo " | Meses:</b> ".$de[1]."<br><b>V.Residual:</b> $"._num($de[2] * 1)." | <b>Adq:</b> ";
							echo ($de[3] == "P")?"Propio":"Leasing";
						}
						?></td> 
						<td onclick="javascript:editar_lotes(<?php echo $i; ?>,<? echo $prod["pro_id"]; ?>,<?
							echo $cant_det[$i]; ?>,'<? echo $lote_det[$i]; ?>');"><?php
						$lotes = explode(",",$lote_det[$i]);
						$cant_series = 0;
						if(count($lotes) > 1){
							for($i2 = 1;$i2 < count($lotes);$i2++){ 
								if($lotes[$i2] <> ""){
									$cant_series++;
									?>
									<span class="label label-default"><?php echo $lotes[$i2]; ?></span>
									<?
								}
							}
						}
						if($cant_series <> $cant_det[$i]){
							?><span class="label label-danger">Items sin series</span><? 
						}
						?></td> 
						<td style="text-align:center;" onclick="javascript:edicion_rapida2(<?php echo $i; ?>,'cant',<?php echo $cant_det[$i]; ?>);"><?php echo _num($cant_det[$i]); ?></td>
						<td style="text-align:right;" onclick="javascript:edicion_rapida2(<?php echo $i; ?>,'prec',<?php echo $prec_det[$i]; ?>);"><?php echo _num($prec_det[$i]); ?></td>
						<td style="text-align:right;"><?php echo _num($subtotal); ?></td>
						<td><?php
						construir_boton("quitar_linea('".$i."');","1","eliminar","Borrar Linea",4);
						?></td> 
					</tr>
					<?
				}
			}
		}
		if($cabe <> ""){
			?> 
	        <tr>
	            <th colspan="5" style="text-align:right;">TOTAL(<? echo $_configuraciones["cfg_moneda"]; ?>):</th>
	            <th style="text-align:right;"><?php echo _num($total); ?></th>
	            <th></th>
	        </tr> 
	        </tbody>
	        </table>

	        <?   
		    if($_configuraciones["cfg_moneda_sec"] <> ""){
		    	?>
				<table class="table table-bordered table-hover table-condensed">
		    	<tr>
		    		<td>
		    		<b>¿Desea ingresar valor en moneda secundaria?</b><br>
		    		<select name="compra_sec" class="campos" onchange="javascript:compra_sec_mostrar(this.value);">
		    			<option value="">NO</option>
		    			<option value="1" <? if($_REQUEST["compra_sec"] == "1"){ echo "selected"; } ?>>SI</option>
		    		</select>
		    		</td>			
		            <th width="1" style="text-align: right; <? 
		            if($_REQUEST["compra_sec"] == ""){ echo 'display: none;'; } ?>" class="compra_sec">Ultimo Cambio:</th>
		            <td width="80" style="<? 
		            if($_REQUEST["compra_sec"] == ""){ echo 'display: none;'; } ?>" class="compra_sec"><input type="text" name="ult_cambio" id="ult_cambio" style="text-align:right" value="<? echo $_configuraciones["cfg_moneda_ult_cambio"] * 1; ?>" class="campos numero3 w10" readonly></td> 	
					<td width="1" class="compra_sec"><?php
					construir_boton("cargar_cambio();","1","derecha","Cargar Cambio",4);
					?></td> 
		            <th width="1" style="text-align: right; <? 
		            if($_REQUEST["compra_sec"] == ""){ echo 'display: none;'; } ?>" class="compra_sec">Cambio Moneda:</th>
		            <td width="80" style="<? 
		            if($_REQUEST["compra_sec"] == ""){ echo 'display: none;'; } ?>" class="compra_sec"><input type="text" name="cambio" id="cambio" style="text-align:right" value="<? echo $_REQUEST["cambio"] * 1; ?>" class="campos numero3 w10" onchange="javascript:calcular2();"></td> 
		            <th width="80" style="text-align:right; <? 
		            if($_REQUEST["compra_sec"] == ""){ echo 'display: none;'; } ?>" class="compra_sec">TOTAL(<? echo $_configuraciones["cfg_moneda_sec"]; ?>):</th>
		            <td width="80" style="text-align:right; <? 
		            if($_REQUEST["compra_sec"] == ""){ echo 'display: none;'; } ?>" class="compra_sec"><input type="text" name="stot2" id="stot2" style="text-align:right" class="campos numero2 w10" value="1" readonly></td>  
		        </tr>
		        </tbody>
		        </table>
		    	<?
		    }
	        ?> 
	        <script type="text/javascript">
	        function compra_sec_mostrar(valor){
	        	if(valor == ""){
	        		$(".compra_sec").hide();
	        	}else{
	        		$(".compra_sec").show();
	        	}
	        }
	        function cargar_cambio(){
	        	document.getElementById("cambio").value = document.getElementById("ult_cambio").value;
	        	calcular2();
	        }
			function calcular2(){
				var total = 0;
				var cambio = 0;
				
				total = document.getElementById("total").value * 1;  
				cambio = document.getElementById("cambio").value * 1;
				
				if(cambio > 0){
					stot = total / cambio;
				}else{
					stot = 0;
				}
				document.getElementById("stot2").value = stot.toFixed(<? echo $_configuraciones["cfg_moneda_sec_decimal"]; ?>);
			}
			<?
		    if($_configuraciones["cfg_moneda_sec"] <> ""){
		    	?>
				calcular2();     
				<?
			}
			?>
	        </script>
	        <?
	    }
	    ?>
        <input type="hidden" id="lineas" class="campos w10" value="<?php echo $lineas; ?>">
        <input type="hidden" name="total" id="total" class="campos w10" value="<?php echo $total; ?>"> 
		<? 
		break; 

	case "quitar_linea":
		$prod_det = explode(";",$_REQUEST["prod_det"]);
		$depr_det = explode(";",$_REQUEST["depr_det"]);
		$lote_det = explode(";",$_REQUEST["lote_det"]);
		$cant_det = explode(";",$_REQUEST["cant_det"]);
		$prec_det = explode(";",$_REQUEST["prec_det"]);
		$gara_det = explode(";",$_REQUEST["gara_det"]);
		for($i = 1;$i < count($prod_det);$i++){
			if($i <> $_REQUEST["elim"]){
				$prod_ .= ";".$prod_det[$i];
				$depr_ .= ";".$depr_det[$i];
				$lote_ .= ";".$lote_det[$i];
				$cant_ .= ";".$cant_det[$i];
				$prec_ .= ";".$prec_det[$i];
				$gara_ .= ";".$gara_det[$i];
			}
		}
		?>
		<script language="javascript">
		document.getElementById("prod_det").value = "<?php echo $prod_; ?>";
		document.getElementById("depr_det").value = "<?php echo $depr_; ?>";
		document.getElementById("lote_det").value = "<?php echo $lote_; ?>";
		document.getElementById("cant_det").value = "<?php echo $cant_; ?>";
		document.getElementById("prec_det").value = "<?php echo $prec_; ?>";
		document.getElementById("gara_det").value = "<?php echo $gara_; ?>";
		detalle();
		</script>
		<?php 
		break; 
		
	case "lotes":
		$lotes = explode(",",$_REQUEST["series"]); 
		?> 
		<h4 id="titulo_alerta">Definir Series</h4> 
		
		<div style="overflow: scroll; height: 400px; width: 100%; padding: 5px;">
		<table width="100%" style="margin-bottom:5px;">
        <tr valign="top">
        	<td width="220" style="padding-right: 10px;">
				<table class="table table-bordered table-condensed">
				<thead>
				<tr>
					<th>Información del Producto</th>
				</tr>
				</thead>
				<tbody>
				<tr> 
					<td>
					<?php
					$res = sql_productos("*"," and pro.pro_id = '".$_REQUEST["pro"]."'"); 
					$prod = mysqli_fetch_array($res);
					echo "<b>".$prod["pro_codigo"]."</b><br>".$prod["pro_nombre"];
					?>
					</td>
				</tr> 
				<tr>
					<td>
					<?php
					$res2 = sql_activos_fijos("*","and acf_producto = '$prod[pro_id]' 
					and acf_serie <> '' order by acf_serie desc");
					if(mysqli_num_rows($res2) > 0){
						?>
						<b>Series definidos:</b>
						<div style="padding-left: 10px;">
						<?
						while($row2 = mysqli_fetch_array($res2)){
							?><li><a href="javascript:elegir_ser('<? echo $row2["acf_serie"]; ?>');"><? echo $row2["acf_serie"]; ?></a></li><?
							$series[] = $row2["acf_serie"];

							$ultima_serie = $row2["acf_serie"] * 1;
						}
						?>
						</div>
						<?
					}else{
						?>
						<span style="color: red;"><b>No existen series para este producto</b></span>
						<?
					}
					$ultima_serie++;
					?>
					</td>
				</tr> 
				</tbody>
				</table>
			</td>
			<td>  
				<table class="table table-bordered table-condensed"> 
				<thead>
				<tr> 
					<th colspan="2">Serie definidas</th> 
				</tr>
				</thead>
				<tbody>
				<?php 
				for($i = 1;$i <= $_REQUEST["can"];$i++){ 
					?>
					<tr>
						<th style="text-align: right;" width="60">Serie <? echo $i; ?></th>
						<td><input type="text" name="lote_<? echo $i; ?>" id="lote_<? echo $i; 
						?>" value="<? echo $lotes[$i]; ?>" class="campos w10"></td>  
					</tr>
					<?
				}  
				?>
				</tbody>
				</table>

				<table class="table table-bordered table-condensed"> 
				<tr>
					<td colspan="3" style="background-color: #fff7e0;">Use esta operación para llenar rápidamente las series</td>
				</tr>
				<tr>
					<th width="100">Rellenar desde:</th>
					<td><input type="text" name="rellenar" id="rellenar" value="<? echo $ultima_serie; ?>" class="campos w10"></td>
					<td width="120"><?
					construir_boton("rellenar();","","importar","Cargar Series",4);
					?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
		</div>  
        <?
		construir_boton("grabar_serie();","","grabar","Grabar Series",4);
		?>
		<br><br>
		<script type="text/javascript">
		function rellenar(){
			if(document.getElementById("rellenar").value == ""){
				alert("Ingrese desde que valor desea rellenar");
				document.getElementById("rellenar").focus();
				return;
			}
			var valor = document.getElementById("rellenar").value * 1;
			<?php 
			for($i = 1;$i <= $_REQUEST["can"];$i++){ 
				?>
				document.getElementById("lote_<? echo $i; ?>").value = valor;
				valor = valor + 1;
				<?
			}  
			?>
		} 
		function grabar_serie(){
			var valor = "";
			<?
			for($i = 1;$i <= $_REQUEST["can"];$i++){ 
				?>
				valor = valor + "," + document.getElementById("lote_<? echo $i; ?>").value;
				<?
			} 
			?>
			var nuevos_valores = "";
			var info = document.getElementById("lote_det").value;
			var valores = info.split(";");
			for(i = 1;i < valores.length;i++){
				if(i == "<? echo $_REQUEST["id"]; ?>"){
					nuevos_valores = nuevos_valores + ";" + valor;
				}else{
					nuevos_valores = nuevos_valores + ";" + valores[i];
				}
			}
			document.getElementById("lote_det").value = nuevos_valores;
			$('#modGeneral').modal('hide');
			detalle();
		}
		</script>
		<?
		break;

	case "depre": 
		$info = explode(",",$_REQUEST["valor"]);
		?> 
		<h4 id="titulo_alerta">Definir Depreciación</h4> 
		 
		<table width="100%">
		<tr valign="top">
			<td width="220">
				<table class="table table-bordered table-condensed">
				<thead>
				<tr>
					<th colspan="2">Depreciación del Activo</th>
				</tr>
				</thead>
				<tbody>
		        <tr>
		        	<th style="text-align:right;">Precio:</th>
		        	<td>$<? echo _num($_REQUEST["precio"]); ?></td>
				</tr>
				<tr>
					<th width="100" style="text-align:right;">Depreciación:</th>
		    		<td><select name="tipo_depreciacion_" id="tipo_depreciacion_" class="campos">
		    			<option value=""></option>
		                <option value="L" <?php if($info[0] == "L"){
		                    echo "selected";
		                } ?>>Lineal</option>
		                <option value="A" <?php if($info[0] == "A"){
		                    echo "selected";
		                } ?>>Acelerada</option>
		                </select></td> 
		        </tr>
		        <tr>
		        	<th style="text-align:right;">Meses:</th>
		        	<td><input type="text" name="meses_" id="meses_" value="<? echo $info[1]; 
		        	?>" class="campos w10" style="text-align: center;"></td>
				</tr>
				<tr>
					<th style="text-align:right;">Valor Residual:</th>
			        <td><input type="text" name="residual_" id="residual_" value="<? echo $info[2]; 
		        	?>" class="campos w10" style="text-align: center;"></td>
			    </tr>
			    <tr>
			    	<th style="text-align:right;">Adquisición:</th>
		    		<td><select name="adquisicion_" id="adquisicion_" class="campos">
					<option value="P" <? if($info[3] == "P"){ echo "selected"; } ?>>Propio</option>
		            <option value="L" <? if($info[3] == "L"){ echo "selected"; } ?>>Leasing</option>
		            </select></td> 	
		        </tr>
				</tbody>
				</table> 
		        <?
				construir_boton("grabar_depre();","","grabar","Grabar Depreciación",4);
				?>
			</td>
			<td style="padding-left: 15px;">
				<div id="lug_espacio"></div> 
		        <?
				construir_boton("calcular_depre();","","refrescar","Calcular Depreciación",4);
				?>
			</td>
		</tr>
		</table>

		<script type="text/javascript"> 
		function calcular_depre(){
			var fecha = document.getElementById("fecha").value;
			var precio = "<? echo $_REQUEST["precio"]; ?>";
			var meses = document.getElementById("meses_").value * 1;
			var residual = document.getElementById("residual_").value * 1;
			AJAXPOST(url_base+modulo_base+"ing_ajax2.php","modo=calcular_depre&fecha="+fecha+"&precio="+precio+"&meses="+meses+"&residual="+residual,document.getElementById("lug_espacio"));
		}
		function grabar_depre(){
			var valor = document.getElementById("tipo_depreciacion_").value+",";
			valor = valor + document.getElementById("meses_").value;+",";
			valor = valor + document.getElementById("residual_").value;+",";
			valor = valor + document.getElementById("adquisicion_").value;

			var nuevos_valores = "";
			var info = document.getElementById("depr_det").value;
			var valores = info.split(";");
			for(i = 1;i < valores.length;i++){
				if(i == "<? echo $_REQUEST["id"]; ?>"){
					nuevos_valores = nuevos_valores + ";" + valor;
				}else{
					nuevos_valores = nuevos_valores + ";" + valores[i];
				}
			}
			document.getElementById("depr_det").value = nuevos_valores;
			$('#modGeneral').modal('hide');
			detalle();
		}
		calcular_depre();
		</script>
		<?
		break;

	case "previsualizar":
		?>
		<h4 id="titulo_alerta">Previsualizar Depreciación</h4> 
		<div id="lug_espacio"></div>
		<?
		break;

	case "calcular_depre": 
		$periodo = substr($_POST["fecha"],3,20);
		$periodo = str_replace("/","-",$periodo); 
		$ano = substr($periodo,3,4);
		$mes = substr($periodo,0,2); 

		$meses_divisor = str_replace(".","",$_REQUEST["meses"]); 
		$residual = str_replace(".","",$_REQUEST["residual"]);
		$valor_activo = str_replace(".","",$_REQUEST["precio"]);
		$valor = $valor_activo - $residual;

		$valor_mensual = round(($valor / $meses_divisor),2);
		?>
		<table class="table table-striped table-bordered tabledrag table-condensed"> 
		<thead>
			<tr>   
				<th style="text-align:center;">Periodo</th>
				<th style="text-align:center;">Precio</th>
				<th style="text-align:center;">Meses</th>
				<th style="text-align:center;">Valor Residual</th>
				<th style="text-align:center;">Valor Dep</th>
			</tr>
		</thead>
		<tbody> 
			<tr>
				<td style="text-align:center;"><? echo $periodo; ?></td>
				<td style="text-align:center;">$<? echo _num($valor_activo); ?></td>
				<td style="text-align:center;"><? echo _num2($meses_divisor); ?></td>
				<td style="text-align:center;">$<? echo _num($residual); ?></td>
				<td style="text-align:center;">$<? echo _num($valor); ?></td>
			</tr>
		</tbody>
		</table>

		<div style="height: 450px; width: 100%; overflow: scroll;">
			<table class="table table-striped table-bordered tabledrag table-condensed"> 
			<thead>
				<tr>   
					<th style="text-align:center;" width="1">#</th>
					<th style="text-align:center;" width="1">Periodo</th>
					<th style="text-align:right;">Cuota Depreciación $</th>
					<th style="text-align:right;">Depreciación Acumulada $</th>
					<th style="text-align:right;">Valor neto en Libros $</th>
				</tr>
			</thead>
			<tbody> 
			<? 
			for($i3 = 1;$i3 <= $meses_divisor;$i3++){
				$acumulado += $valor_mensual;
				$valor -= $valor_mensual;
				$valor_activo -= $valor_mensual;
				
				$count++;
				$mesx = ($mes < 10)?"0".$mes:$mes;
				?>
				<tr>
					<td style="text-align: center;"><? echo $count; ?></td>
					<td style="text-align: center;"><? echo $ano."-".$mesx."-01"; ?></td>
					<td style="text-align: right;"><? echo _num($valor_mensual); ?></td>
					<td style="text-align: right;"><? echo _num($acumulado); ?></td>
					<td style="text-align: right;"><? echo _num($valor_activo); ?></td>
				</tr>
				<?
				if($mes == 12){
					$mes = 0;
					$ano++;
				} 
				$mes++;
			} 
			?>
			</tbody>
			</table>
		</div>
		<?
		break;
}
?> 