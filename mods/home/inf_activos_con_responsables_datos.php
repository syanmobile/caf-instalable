<?php
if($_REQUEST["fil_codigo2"] <> ""){
    $filtros .= " and acf_codigo like '".$_REQUEST["fil_codigo2"]."' ";
}
if($_REQUEST["fil_codigo"] <> ""){
    $filtros .= " and pro_codigo like '".$_REQUEST["fil_codigo"]."' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
    $filtros .= " and pro_nombre like '".$_REQUEST["fil_descripcion"]."' ";
}  
if($_REQUEST["fil_categoria"] <> ""){
    $filtros .= " and pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
} 
if($_REQUEST["fil_ubicacion"] <> ""){
    $filtros .= " and acf_ubicacion = '".$_REQUEST["fil_ubicacion"]."' ";
}  
if($_REQUEST["fil_responsable"] <> ""){
    $filtros .= " and acf_responsable = '".$_REQUEST["fil_responsable"]."' ";
}
 
$res = sql_activos_fijos("*"," and acf_responsable <> 0 and pro_tipo <> 'INS' $filtros order by acf_responsable asc,acf_fecha_pres_asig_compromiso desc, pro_nombre asc"); 
if(mysqli_num_rows($res) > 0){ 
    ?>
    <table width="100%">
    <tr valign="top">
    <td>
        <table class="table table-striped table-bordered tabledrag table-condensed" border="1"> 
        <thead>
        <tr> 
            <th width="1"></th>
            <th width="1">Cod.Activo</th>
            <th width="1">Cod.Producto</th> 
            <th>Producto</th> 
            <th width="100" style="text-align: center;">Fecha Entrega</th>
            <th width="60">Vigencia</th>
            <th width="130">Situación</th>
        </tr>
        </thead>
        <tbody> 
        <?
        while($row = mysqli_fetch_array($res)){

            if($row["acf_responsable"] <> $resp){ 
                ?> 
                <tr>  
                    <th style="background-color:#FFFBD2;" colspan="14"><?php 
                    echo $_personas[$row["acf_responsable"]]; ?></th>
                </tr> 
                <? 
                $resp = $row["acf_responsable"];
                $_respo[] = $row["acf_responsable"];
            } 

            if($row["acf_disponible"] == 2){
                $prestamos++;
                $_tot_responsables[$row["acf_responsable"]]["prestamos"] += 1;
                
                $dias = dias_transcurridos($row["acf_fecha_pres_asig_compromiso"],date("Y-m-d")); 
                ?>
                <tr> 
                    <td><?
                    construir_boton("acf_info.php","&acf=".$row["acf_id"],"buscar","Ver Activo Fijo"); 
                    ?></td>
                    <th><? echo $row["acf_codigo"]; ?></th>
                    <th><? echo $row["pro_codigo"]; ?></th>
                    <td><? echo $row["pro_nombre"]; ?></td>    
                    <td style="text-align: center;"><?php echo _fec($row["acf_fecha_pres_asig_compromiso"],5); ?></td>
                    <?
                    if($dias < 0){
                        ?>
                        <td><span class="label label-danger">ATRASADO</span></td>
                        <td><? echo ($dias * -1); ?> días de atraso</td>
                        <?
                        $prestamo_vencida++;
                    }else{
                        ?>
                        <td><span class="label label-success">AL DÍA</span></td>
                        <td><? echo $dias; ?> días para entregar</td>
                        <?
                        $prestamo_aldia++; 
                    }  
                    ?>
                </tr>
                <?
            }else{
                $asignados++;
                $_tot_responsables[$row["acf_responsable"]]["asignados"] += 1;
                ?>
                <tr>  
                    <td><?
                    construir_boton("acf_info.php","&acf=".$row["acf_id"],"buscar","Ver Activo Fijo"); 
                    ?></td>
                    <th><? echo $row["acf_codigo"]; ?></th>
                    <th><? echo $row["pro_codigo"]; ?></th>
                    <td><? echo $row["pro_nombre"]; ?></td>   
                    <td style="text-align: center;"><?php echo _fec($row["acf_fecha_pres_asig"],5); ?></td>
                    <?
                    $dias = dias_transcurridos(date("Y-m-d"),$row["acf_fecha_pres_asig"]); 
                    ?>
                    <td><span class="label label-info">ASIGNADO</span></td>
                    <td><? echo $dias; ?> días asignado</td> 
                </tr>
                <?
            }
        }
        ?>   
        </tbody>
        </table>
    </td>
    <td style="padding-left:20px;" width="300">  
        <div class="alert alert-warning" style="color: #666666 !important;">
            <table width="100%">
                <tr valign="top"> 
                    <td style="font-size: 13px;">
                        PRESTAMOS: <b><?php echo _num2($prestamos); ?></b><br>
                        ASIGNADOS: <b><?php echo _num2($asignados); ?></b>
                    </td>
                </tr>
            </table>
        </div> 

        <table class="table table-striped table-bordered tabledrag table-condensed"> 
        <thead>
        <tr>
            <th>Responsable</th>
            <th width="1">Prestamos</th>
            <th width="1">Asignados</th>
        </tr>
        </thead>
        <tbody>
            <?
            foreach($_respo as $r){
                ?>
                <tr valign="top"> 
                    <td><? echo $_personas[$r]; ?></td> 
                    <th style="text-align: center;">
                    <? echo _num2($_tot_responsables[$r]["prestamos"]); ?>    
                    </th> 
                    <th style="text-align: center;">
                    <? echo _num2($_tot_responsables[$r]["asignados"]); ?>    
                    </th>
                </tr>
                <?
            }
            ?>
        </tbody>
        </table>
    </td>
    </tr>
    </table>
    <?  
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin activos fijos prestados ni asignados</strong>
    </div>
    <?php 
}  
?>