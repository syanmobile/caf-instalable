<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td><?php
		construir_boton("cfg_nuevo.php","","crear","Nuevo Parámetro",2);
		construir_boton("sincronizar2.php","","importar","Importar Configuraciones Faltantes",2); 
		?></td>
    </tr>
</tbody>
</table>
<?
$configuraciones = explode("*****",file_get_contents("http://www.syancloud.cl/central/configuraciones.php")); 
foreach($configuraciones as $codigo_){
	if($codigo_ <> ""){
		$res2 = sql_configuracion("*"," where cfg.cfg_codigo = '$codigo_' ");
		if(mysqli_num_rows($res2) == 0){
			$noexisten[] = $codigo_;
		}
	}
}
if(count($noexisten) > 0){
	?>
	<div class="alert alert-danger">
		<span class="glyphicon glyphicon-fire" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
		<strong>Existen Parametros sin cargar</strong>
		<ul style="margin-left: 30px;">
		<?
		foreach($noexisten as $info){
			?><li><? echo $info; ?> | <a href="javascript:carg('cfg_nuevo.php?codigo=<? echo $info; ?>');">Crear ahora >></a></li><?
		}
		?>	
		</ul>
	</div>
	<?
}
?>
<table class="table table-striped table-bordered table-condensed" id="tabl4"> 
<thead>
<tr>
	<th width="1" align="center">&nbsp;</th>
	<th width="1" align="center">&nbsp;</th> 
	<th width="1" align="center">Grupo</th>
	<th width="1" align="center">Código</th>
	<th>Titulo</th>
	<th>Tipo</th>
	<th>Valor</th>
</tr>
</thead>
<tbody> 
<?php	
$res2 = sql_configuracion("*"," where cfg.cfg_elim = 0 $adicional  ORDER BY cfg.cfg_grupo asc,cfg.cfg_titulo asc ");
if(mysqli_num_rows($res2) > 0){
	while($row = mysqli_fetch_array($res2)){
		?>
		<tr fila="cfg_<?php echo $row["cfg_codigo"] ;?>" id="cfg_<?php echo $row["cfg_codigo"] ;?>">  
			<td style="text-align:center;"><?php
			construir_boton("cfg_editar.php","&codigo=".$row["cfg_codigo"],"editar","Editar"); ?></td><td><?
			construir_boton("eliminar_cfg('".$row["cfg_codigo"]."')","","eliminar","Eliminar",4);
			?></td>
			<td align="center"><?php echo $row["cfg_grupo"]; ?></td>
			<td align="center"><?php echo $row["cfg_codigo"]; ?></td>  
			<td><?php echo $row["cfg_titulo"]; ?></td> 
			<td><?php echo $row["cfg_tipo"]; ?></td>  
			<td><?php echo $row["cfg_valor"]; ?></td>
		</tr>
		<?php
	}
}
?>
</tbody>
</table>
	
<script language="javascript">
function eliminar_cfg(codigo){
	if(confirm("Seguro?")){
          $("[fila=cfg_"+codigo+"]").fadeOut("normal");
          SoloEnviar("mods/home/cfg_save2.php","modo=eliminar&codigo="+codigo);
     }
}	
	
$(document).ready(function() { 
	$('#tabl4').DataTable({
	  "columns": [   
		{ "width": "1px" }, 
		{ "width": "1px" }, 
		null , 
		{ "width": "1px" }, 
		null , 
		{ "width": "1px" }, 
		null  
	  ],
	  lengthMenu: [20000000],
	  "bPaginate": false,
	  "bLengthChange": false
	});
} );   
</script>