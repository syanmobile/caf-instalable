<?php
require("../../inc/conf_dentro.php");

switch($_REQUEST["modo"]){ 

	case "auxiliares":  
		$res = sql_auxiliares("*"," and (aux.aux_nombre like '%$_REQUEST[q]%' or aux.aux_codigo like '%$_REQUEST[q]%') 
		order by aux.aux_codigo asc"); 
		$num = mysqli_num_rows($res); 
		if($num > 0){ 
			while($row = mysqli_fetch_array($res)){
				$data[] = array('id' => $row['aux_id'], 'text' => $row["aux_codigo"]." - ".$row["aux_nombre"]); 
			}
			echo json_encode($data);
		}
		break;

	case "categorias":  
		$res = sql_categorias("*"," and (cat.cat_nombre like '%$_REQUEST[q]%' or cat.cat_codigo like '%$_REQUEST[q]%') 
		order by cat.cat_codigo asc"); 
		$num = mysqli_num_rows($res); 
		if($num > 0){ 
			while($row = mysqli_fetch_array($res)){
				$data[] = array('id' => $row['cat_id'], 'text' => $row["cat_codigo"]." - ".$row["cat_nombre"]); 
			}
			echo json_encode($data);
		}
		break;

	case "ubicaciones":  
		$res = sql_ubicaciones("*"," and (ubi.ubi_nombre like '%$_REQUEST[q]%' or ubi.ubi_codigo like '%$_REQUEST[q]%') 
		order by ubi.ubi_codigo asc"); 
		$num = mysqli_num_rows($res); 
		if($num > 0){ 
			while($row = mysqli_fetch_array($res)){
				$data[] = array('id' => $row['ubi_id'], 'text' => $row["ubi_codigo"]." - ".$row["ubi_nombre"]); 
			}
			echo json_encode($data);
		}
		break;

	case "responsables":  
		$res = sql_personas("*"," and (per.per_nombre like '%$_REQUEST[q]%' or per.per_rut like '%$_REQUEST[q]%') 
		order by per.per_nombre asc"); 
		$num = mysqli_num_rows($res); 
		if($num > 0){ 
			while($row = mysqli_fetch_array($res)){
				$data[] = array('id' => $row['per_id'], 'text' => $row["per_nombre"]." (Rut: ".$row["per_rut"].")"); 
			}
			echo json_encode($data);
		}
		break;

	case "cco":  
		$res = sql_centro_costo("*"," and (cco.cco_nombre like '%$_REQUEST[q]%' or cco.cco_codigo like '%$_REQUEST[q]%') 
		order by cco.cco_codigo asc"); 
		$num = mysqli_num_rows($res); 
		if($num > 0){ 
			while($row = mysqli_fetch_array($res)){
				$data[] = array('id' => $row['cco_id'], 'text' => $row["cco_codigo"]." - ".$row["cco_nombre"]); 
			}
			echo json_encode($data);
		}
		break;

	case "lugares":  
		$res = sql_bodegas("*"," and (bod.bod_nombre like '%$_REQUEST[q]%' or bod.bod_codigo like '%$_REQUEST[q]%') 
		order by bod.bod_codigo asc"); 
		$num = mysqli_num_rows($res); 
		if($num > 0){ 
			while($row = mysqli_fetch_array($res)){
				$data[] = array('id' => $row['bod_id'], 'text' => $row["bod_codigo"]." - ".$row["bod_nombre"]); 
			}
			echo json_encode($data);
		}
		break;
}
?> 