<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Stock de Insumos";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "ins_listado.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>  
<table class="table table-bordered table-condensed table-info" style="margin-bottom:0px !important;">  
<tbody> 
<tr>
    <td width="100">
    <b>Código:</b><br />
    <input type="text" name="fil_codigo" id="fil_codigo" class="campos w10" value="<? echo $_REQUEST["fil_codigo"]; ?>"></td> 
    <td>
    <b>Descripción:</b><br />
    <input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? echo $_REQUEST["fil_descripcion"]; ?>"></td>
    
    <td>
    <b>Clasificación:</b><br>
	<select name="fil_categoria" id="fil_categoria" class="campos buscador w10">
	<option value=""></option>
	<?php
	$res = sql_categorias("*"," order by cat.cat_nombre asc"); 
	if(mysqli_num_rows($res) > 0){
		while($row = mysqli_fetch_array($res)){
			?>
            <option value="<? echo $row["cat_id"]; ?>" <?
            if($_REQUEST["fil_categoria"] == $row["cat_id"]){ echo "selected"; } 
            ?>>C<? echo $row["cat_codigo"]." - ".$row["cat_nombre_largo"]; ?></option>
            <?
		}
	}
	?></select></td> 

    <td width="1"><?
	construir_boton("ins_inventario.php","","buscar","Filtrar",2); 
	?></td> 
</tr>
</tbody>
</table>
<?php 
/****************************************************************************************/
if($_REQUEST["fil_codigo"] <> ""){
	$filtros .= " and pro.pro_codigo like '%".$_REQUEST["fil_codigo"]."%' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and pro.pro_nombre like '%".$_REQUEST["fil_descripcion"]."%' ";
} 
if($_REQUEST["fil_unidad"] <> ""){
	$filtros .= " and pro.pro_unidad = '".$_REQUEST["fil_unidad"]."' ";
}
if($_REQUEST["fil_grupo"] <> ""){
	$filtros .= " and pro.pro_grupo = '".$_REQUEST["fil_grupo"]."' ";
} 
if($_REQUEST["fil_categoria"] <> ""){
	$filtros .= " and pro.pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
} 
if($_REQUEST["fil_categoria"] <> ""){
	$filtros .= " and pro.pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
}  
?>
<input type="hidden" name="pagina" id="pagina" value="ins_inventario.php" class="campos">
<?
$sql_paginador = sql_insumos("*,sum(total) as stock"," $filtros GROUP BY pro_codigo ORDER BY pro_nombre asc","s","");  
$cant_paginador = 20;
require("../../_paginador.php");
$res = mysqli_query($cnx,$sql_final); 
if(mysqli_num_rows($res) > 0){
	?>
	<table class="table table-striped table-bordered tabledrag table-condensed"> 
	<thead>
		<tr>    
			<th width="1">Imagen</th>
			<th width="1">Codigo</th> 
			<th>Producto</th>
			 
			<th>Clasificación</th> 
			<th>Unidad</th>    
			
			<th width="1">Mínimo</th> 
			<th width="1">Stock</th> 
			<th width="1">xComprar</th> 
			<th width="1">Máximo</th>
		</tr>
	</thead>
	<tbody> 
	<?
	while($row = mysqli_fetch_array($res)){ 
		?>
		<tr>     
			<td><? 
			$imagen = ($row["pro_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$row["pro_imagen"];
			?><img src="<?php echo $imagen; ?>" style="width:40px !important;"></td> 
			<th><?php echo $row["pro_codigo"]; ?></th> 
			<td><?php echo $row["pro_nombre"]; ?></td> 
			 
			<td><?php 
			if($row["pro_categoria"] <> 0){
				echo "C".$row["cat_codigo"]." - ".$_categorias_largo[$row["pro_categoria"]]; 
			} ?></td>
			<td><?php echo $_unidades[$row["pro_unidad"]]; ?></td>    
			 			
			<td style="text-align:center; "><?php echo _num2($row["pro_stock_minimo"]); ?></td> 
			<td style="text-align:center; "><span class="label label-<?php
            if($row["stock"] > $row["pro_stock_minimo"]){
				echo 'success';
			}else{
				echo 'danger';
			}
			?>"><?php echo _num2($row["stock"]); ?></span></td> 
			<td style="text-align:center;"><?php 
			if($row["stock"] < $row["pro_stock_minimo"]){
				echo _num2($row["pro_stock_minimo"] - $row["stock"]);
			}else{
				echo "0";
			}
			?></td> 
			<td style="text-align:center; "><?php echo _num2($row["pro_stock_maximo"]); ?></td>  
		</tr>
		<?  
	}
	?>
	</tbody>
	</table> 
	<?
	echo $paginador_dibujo;
}else{
	?>
	<div class="alert alert-danger">
		<strong>Sin registros creados</strong>
	</div>
	<?php 
} 
?>