<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Listado SubGrupos";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "sgru_listado.php"; 
include("ayuda.php");
//---------------------------------------------------------------------------------------- 
?>
<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td><?php
		construir_boton("sgru_nuevo.php","","crear","Nuevo SubGrupo",2);
		?></td>
    </tr>
</tbody>
</table>
<?php
$res = sql_subgrupos("*"," ORDER BY gru.gru_nombre asc,sgru.sgru_nombre asc","","");
if(mysqli_num_rows($res) > 0){
    ?>
    <table class="table table-striped table-bordered table-condensed"> 
    <thead>
    <tr>
        <th width="1" align="center">&nbsp;</th>
        <th width="1" align="center">Código</th>
        <th>SubGrupo</th>
    </tr>
    </thead>
    <tbody> 
    <? 
    while($row = mysqli_fetch_array($res)){
        if($row["sgru_gru_id"] <> $aux){ 
            $aux = $row["sgru_gru_id"];
            ?>
            <tr>
                <th colspan="4" style="background-color: yellow;"><? echo $row["gru_codigo"]." - ".$row["gru_nombre"]; ?></th>
            </tr>
            <?
        }
        ?>
        <tr fila="<?php echo $row["sgru_id"] ;?>" id="<?php echo $row["sgru_id"] ;?>">  
            <td style="text-align:center;"><?php
			construir_boton("sgru_editar.php","&id=".$row["sgru_id"],"editar","Editar");
			?></td>
            <th style="text-align: center"><?php echo $row["sgru_codigo"]; ?></th>
            <td><?php echo $row["sgru_nombre"]; ?></td>
        </tr>
        <? 
    }
    ?> 
    </tbody>
    </table>  
    <?php
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin registros creados</strong>
    </div>
    <?php
}
?>