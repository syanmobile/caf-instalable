<?php
require("../../inc/conf_dentro.php");

//----------------------------------------------------------------------------------------
$titulo_pagina = "Eliminar Perfil";
$navegacion[] = array("inicio.php","","Configuración");
$navegacion[] = array("pfl_listado.php","","Perfiles");
$navegacion[] = array("","","Eliminar Perfil");
//----------------------------------------------------------------------------------------
construir_breadcrumb($titulo_pagina);
construir_titulo($titulo_pagina,$navegacion,$pagina_name);
//----------------------------------------------------------------------------------------

?>
<script language="javascript">
function save(){    
	if(confirm('Esta seguro?')){
		carg("pfl_save.php","");
	}
}
</script>

<?php
$res = sql_perfil("*","where pfl.pfl_id = '$_REQUEST[id]'","");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<strong>No se encontró el Perfil</strong>
	</div>
    <?php
	exit();
}
?>

<div class="alert alert-danger"> 
	<?php
	construir_boton("","","eliminar","Eliminar este registro del sistema",3);
    ?>
    <br />
    <br />
</div>
<?php _p($datos); ?>

<form class="form-horizontal" role="form" method="post">
	<input type="hidden" name="modo" value="eliminar" class="campos">
	<input type="hidden" name="id" value="<?php echo $_REQUEST["id"]; ?>" class="campos">
</form> 

<?php
construir_boton("pfl_listado.php","","eliminar","Cancelar",2);
?>