<?php 
ob_start(); 
require("../../inc/conf_dentro.php"); 

// para utilizar la librería
define('FPDF_FONTPATH','font/');
require_once('../../pdf/fpdf.php');

$pdf=new FPDF("L","mm",array(130,70));
$pdf->AliasNbPages();

$activos = explode(",",$_REQUEST["acf"]);

foreach($activos as $activo){
	if($activo <> ""){
		$res = sql_activos_fijos("*"," and acf.acf_id = '".$activo."' ");
		$row = mysqli_fetch_array($res);

		$pdf->AddPage();

		$pdf->Ln(15);

		$codigo = $row["acf_id"].'acfi'.md5($row["acf_id"]);
		$pdf->SetFont('Arial','',11);  
		$pdf->Cell(0,5,$codigo,0,1,"L");
		
		$pdf->SetFont('Arial','',11);  
		$pdf->Cell(0,5,utf8_decode($row["pro_nombre"]),0,1,"L");
		$pdf->SetFont('Arial','b',11);  
		$pdf->Cell(0,5,utf8_decode("SKU: ".$row["pro_codigo"]),0,1,"L"); 
		$pdf->SetFont('Arial','',11);  
		$pdf->Cell(0,5,utf8_decode("Serie: ".$row["acf_serie"]),0,1,"L"); 

		$pdf->Image($url_base.'pdf/image.php?text='.$codigo.'&code=code128&o=1&dpi=72&t=30&r=3&rot=0&f1=Arial.ttf&f2=-1&a1=&a2=NULL&a3=',10,5,110,19,'PNG');
	}		
}

$pdf->Output();
?>