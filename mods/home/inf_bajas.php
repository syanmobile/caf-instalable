<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Informe Bajas Activo Fijo";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<table class="table table-bordered table-condensed"> 
<tr>    
    <td width="1"><b>Desde:</b><br>
    <input type="text" class="campos fecha" id="desde" name="desde" value="<?php
    if($_REQUEST["desde"] == ""){  
        $_REQUEST["desde"] = "01/".date('m/Y');
    }
    echo $_REQUEST["desde"]; ?>" size="10"></td>
    
    <td width="1"><b>Hasta:</b><br>
    <input type="text" class="campos fecha" id="hasta" name="hasta" value="<?php
    if($_REQUEST["hasta"] == ""){
        $_REQUEST["hasta"] = date("d/m/Y");
    }
    echo $_REQUEST["hasta"]; ?>" size="10"></td> 
    
    <td><b>Concepto:</b><br>
	<?php input_concepto($_REQUEST["concepto"],'S'); ?></td>
   
    <td><b>Producto:</b><br>
    <input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? echo $_REQUEST["fil_descripcion"]; ?>"></td>

    <td width="120"><b>Serie:</b><br>
    <input type="text" name="fil_serie" id="fil_serie" class="campos w10" value="<? echo $_REQUEST["fil_serie"]; ?>"></td>

    <td>
    <b>Clasificación:</b><br />
    <select name="fil_categoria" id="fil_categoria" class="campos buscador w10">
    <option value=""></option>
    <?php
    $res = sql_categorias("*"," order by cat.cat_codigo asc"); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["cat_codigo"]; ?>" <?
            if($_REQUEST["fil_categoria"] == $row["cat_codigo"]){ echo "selected"; } 
            ?>>C<? echo $row["cat_codigo"]." - ".$row["cat_nombre_largo"]; ?></option>
            <?
        }
    }
    ?></select></td>
     
    <td width="120">
    <b>Tipo:</b><br />
    <select name="fil_tipo" id="fil_tipo" class="campos w10">
    <option value="">Todos</option>
    <?php
    foreach($_tipo_activos as $tp){
        if(in_array("act_".strtolower($tp)."_ver",$opciones_persona)){
            ?><option value="<? echo $tp; ?>" <? if($_REQUEST["fil_tipo"] == $tp){ echo "selected"; } ?>><?
            echo $_tipo_activo[$tp]; ?></option>
            <?
        }
    }
    ?>
    </select></td>
         
    <td width="1"><?
    construir_boton("alerta_js('Excel no disponible (Demo)');","1","importar","Descargar",4); 
    ?></td> 
    <td width="1"><?php
    construir_boton("inf_bajas.php","","buscar","Buscar");
    ?></td>  
</tr>
</table>
<?php
require("inf_bajas_datos.php"); 
?>