<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Kardex Producto";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------  
?>  
<table class="table  table-bordered"> 
<tr>
    <th width="1">Ficha:</th>
    <td>
    <select name="producto" id="producto" class="campos buscador">
    <option value=""></option>
	<?php
	// Sacar los productos con movimiento
	$res = sql_productos("*"," order by pro_nombre asc");
	$num = mysqli_num_rows($res);
	if($num > 0){
		while($row = mysqli_fetch_array($res)){
			?><option value="<?php echo $row["pro_id"]; ?>" <?php
			if($row["pro_id"] == $_REQUEST["producto"]){ echo "selected"; $datos = $row; } ?>><?php
			echo $row["pro_codigo"]." - ".$row["pro_nombre"];  
			?></option><?php 
		}
	}
    ?>
    </select></td>

    <td width="1"><?
    construir_boton("alerta_js('Excel no disponible (Demo)');","1","importar","Descargar",4); 
    ?></td> 
    <td width="1"><?
	construir_boton("inf_kardex_producto.php","","buscar","Filtrar");  
	?></td>
</tr> 
</table>

<?php
if($_REQUEST["producto"] <> ""){
	
	//costo_promedio($_REQUEST["producto"],"");
	?> 
    <table class="table table-bordered table-condensed"> 
    <thead>
    <tr>
        <th colspan="16">Informaci&oacute;n del Producto</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th width="1">Producto:</th>
        <td colspan="15"><strong><?php echo _t($datos["pro_codigo"]); ?></strong> - <?php echo _t($datos["pro_nombre"]); ?></td>
    </tr>
    <?php
    if($datos["pro_tipo"] == "INS"){  
		$stock = 0;
		$res = sql_insumos("*,sum(total) as stock"," and pro_id = '".$datos["pro_id"]."' group by pro_id");
		if(mysqli_num_rows($res) > 0){
			$row = mysqli_fetch_array($res);
			$stock = $row["stock"]; 
		}
    	?>
	    <tr>
	        <th width="1">Stock:</th>
	        <td width="1" style="padding-right: 15px;"><span class="label label-<?php
	        if($stock >= $row["pro_stock_minimo"]){
	            echo 'success';
	        }else{
	            echo 'danger';
	        }
	        ?>"><?php echo _num($stock); ?></span></td> 
	        <th width="1">Minimo:</th>
	        <td width="1" style="padding-right: 15px;"><?php echo _num($datos["pro_stock_minimo"]); ?></td>
	        <th width="1">CostoPromedio:</th>
	        <td width="1"><?php echo _num($datos["pro_valor"]); ?></td>
	        <td></td>
	    </tr>
	    <?
    }else{
    	?>
	    <tr>
	        <th width="1">Serie:</th>
	        <td colspan="4"><?php
			// Sacar los series de producto
			$res2 = sql_movimientos_detalle("det_lote"," and pro_id = '$_REQUEST[producto]' group by pro_codigo,det_lote order by det_lote asc");
			$num2 = mysqli_num_rows($res2);
			if($num2 > 0){
				?> 
				<select name="serie" id="serie" class="campos">
				<option value=""></option>
				<?
				while($row2 = mysqli_fetch_array($res2)){
					?><option value="<? echo $row2["det_lote"]; ?>" <? if($_REQUEST["serie"] == $row2["det_lote"]){ echo "selected"; } 
					?>><? echo $row2["det_lote"]; ?></option><?
				}
				?>
				</select> 
				<?
			}
			?></td>
		</tr>
    	<?
    } 
    ?>
    <tr>
	    <td colspan="10"><?
		construir_boton("pro_editar.php","&id=".$_REQUEST["producto"],"editar","Editar Producto",2);
		construir_boton("visor('pro_ficha.php?codigo=".$datos["pro_codigo"]."')","","buscar","Ver Ficha",4);
		?></td>
	</tr>
    </tbody>
    </table>  
    
	<table class="table table-striped table-bordered tabledrag table-condensed"> 
	<thead>
		<tr>  
			<th style="text-align:center;" width="1"></th>
			<th style="text-align:center;" width="1">Fecha</th>
			<th style="text-align:center;" width="1">Folio</th>
			<th style="text-align:center;" width="1">Tipo</th>
			<th width="120">Lugar</th> 
			<th width="150">Concepto</th>
			<th>Proveedor/Responsable</th>
			<th>Notas</th>
			<th>Autor</th>   
			<th width="1">Entrada</th>
			<th width="1">Salida</th>
			<th width="1">Saldo</th>
			<th width="1" style="text-align:center;">Precio $</th>
			<th width="1" style="text-align:center;">Subtotal $</th> 
			<th width="1" style="text-align:center;">Saldo $</th>
		</tr>
	</thead>
	<tbody>
	<?php
	$final = 0;
	$saldo = 0;  
	if($_REQUEST["serie"] <> ""){
		$extra = " and det.det_lote = '".$_REQUEST["serie"]."' ";
	}
	
    $res = sql_movimientos_detalle("*,sum(det_ingreso) as ingreso,sum(det_egreso) as egreso"," 
    and pro_id = '$_REQUEST[producto]' $extra  
    group by mov.mov_id, pro_codigo 
	order by mov.mov_fecha asc, mov.mov_id asc","",""); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){ 
			$movi = $row["ingreso"] - $row["egreso"];
			$subtotal = $movi * $row["det_valor"];
			$saldo2 += $subtotal;
			$saldo += $movi; 
			?>
			<tr>  
				<td style="text-align:center;"><?php
				if($row["mov_id"] <> ""){
					construir_boton("visor('aju_pdf.php?id=".$row["mov_id"]."')","1","imprimir","Vista #".$row["mov_id"],4);
				}
				?></td>  
				<td style="text-align:center;"><?php echo _fec($row["mov_fecha"],5); ?></td>
				<td style="text-align:center;"><?php echo $row["mov_folio"]; ?></td>  
				<td style="text-align:center;"><?php echo $_tipos[$row["mov_tipo"]]; ?></td>
				<td style="text-align:center;"><?php echo $_bodegas[$row["mov_bodega"]]; ?></td>
				<td><?php echo $_conceptos[$row["mov_concepto"]]; ?></td>   
				<td><?php
				if($row["aux_codigo"] <> ""){
					echo $row["aux_nombre"]; 
				}else{
					echo $_personas[$row["mov_responsable"]]; 
				}
				?></td>  
				<td><?php echo $row["mov_glosa"]; ?></td>  
				<td><?php echo nom_persona($row["mov_per_id"]); ?></td>
				
				<td style="text-align:center;"><?php echo _num2($row["ingreso"]); ?></td>  
				<td style="text-align:center;"><?php echo _num2($row["egreso"]); ?></td>  
				<td style="text-align:center;"><?php echo _num2($saldo); ?></td> 
				 
				<td style="text-align:center;"><?php echo _num($row["det_valor"]); ?></td>
				<td style="text-align:center;"><?php echo _num($subtotal); ?></td>    
				<td style="text-align:center;"><?php echo _num($saldo2); ?></td>    
			</tr>
			<? 
        }
	}else{
		?>
		<tr>
			<td colspan="20">Sin movimientos entre las fechas</td>
		</tr>
		<?
	}
    ?>
	<tr>
		<th style="text-align:right;" colspan="11">Saldo Final:</th>
		<th style="text-align:center;"><?php echo _num($saldo); ?></th>
		<th></th>
		<th></th>
		<th style="text-align:center;"><?php echo _num($saldo2); ?></th>
	</tr>
	</tbody>
	</table> 
    <? 
}
?>