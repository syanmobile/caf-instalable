<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Depreciación Contable del Período (2)";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------  

if($_REQUEST["anio"] == ""){
	$_REQUEST["anio"] = date("Y");
}
if($_REQUEST["mes_inicio"] == ""){
	$_REQUEST["mes_inicio"] = date("m");
}
if($_REQUEST["mes_termino"] == ""){
	$_REQUEST["mes_termino"] = date("m");
}
?>  
<script type="text/javascript">
function buscar(){
	var a = $(".campos").fieldSerialize();
	var div = document.getElementById("resultados");
	AJAXPOST("mods/home/inf_depreciacion_del_periodo_datos.php",a,div);
} 
$("#activo").select2({        
    ajax: {
        url: "mods/home/_ajax_activos.php",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term // search term
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data, function(obj) {
                    return { id: obj.id, text: obj.text };
                })
            };
        },
        cache: true
    },
    minimumInputLength: 3 
});
</script> 
 
<table class="table table-bordered table-info table-condensed"> 
<tbody> 
<tr>
    <td width="1">
        <b>Fecha:</b><br>
        <input type="text" class="campos fecha" id="fecha" name="fecha" value="<?php
        if($_REQUEST["fecha"] == ""){  
            $_REQUEST["fecha"] = "01/".date('m/Y');
        }
        echo $_REQUEST["fecha"]; ?>" size="10">
    </td> 
    <td>
        <b>Código Activo:</b><br>
        <input type="text" name="fil_codigo2" id="fil_codigo2" class="campos w10" value="<? 
        echo $_REQUEST["fil_codigo2"]; ?>">
    </td>
    <td>
        <b>Nombre Producto:</b><br>
        <input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? 
        echo $_REQUEST["fil_descripcion"]; ?>">
    </td> 
    <td>
        <b>Clasificación:</b><br>
        <select name="fil_categoria" id="fil_categoria" class="campos buscador w10">
        <option value=""></option>
        <?php
        $res = sql_categorias("*"," order by cat.cat_nombre asc"); 
        if(mysqli_num_rows($res) > 0){
            while($row = mysqli_fetch_array($res)){
                ?>
                <option value="<? echo $row["cat_id"]; ?>" <?
                if($_REQUEST["fil_categoria"] == $row["cat_id"]){ echo "selected"; } 
                ?>>C<? echo $row["cat_codigo"]." - ".$row["cat_nombre_largo"]; ?></option>
                <?
            }
        }
        ?></select>
    </td> 
    <td>
        <b>Centro Costo:</b><br>
        <select name="fil_cco" id="fil_cco" class="campos buscador">
        <option value=""></option>
        <?php
        $res = sql_centro_costo("*","order by cco_codigo asc");  
        if(mysqli_num_rows($res) > 0){
            while($datos = mysqli_fetch_array($res)){
                ?><option value="<?php echo $datos["cco_id"]; ?>" <?php 
                if($_REQUEST["fil_cco"] == $datos["cco_id"]){ echo "selected"; } 
                ?>>CC<?php echo $datos["cco_codigo"]; ?> - <?php echo $datos["cco_nombre"]; ?></option>
                <?
            }
        }
        ?>
        </select> 
    </td> 
    <td>
        <b>Tipo:</b><br>
        <select name="fil_tipo" id="fil_tipo" class="campos buscador">
        <option value="">Todos</option>  
        <?php
        foreach($_tipo_activos as $tp){
            if(in_array("act_".strtolower($tp)."_ver",$opciones_persona)){
                ?><option value="<? echo $tp; ?>" <? if($_REQUEST["fil_tipo"] == $tp){ echo "selected"; } ?>><?
                echo $_tipo_activo[$tp]; ?></option>
                <?
            }
        }
        ?>
        </select> 
    </td>
    <?php
    if($_configuraciones["cfg_moneda_sec"] <> ""){
        ?>
        <td width="1">
            <b>Moneda:</b><br>
            <select name="fil_moneda" id="fil_moneda" class="campos">
            <option value=""><? echo $_configuraciones["cfg_moneda"]; ?></option> 
            <option value="sec" <? if($_REQUEST["fil_moneda"] == "sec"){ echo "selected"; } 
            ?>><? echo $_configuraciones["cfg_moneda_sec"]; ?></option>
            </select> 
        </td>
        <?
    }
    ?>
    <td width="1"><? 
    construir_boton("buscar()","","buscar","Filtrar",4);
    construir_boton("inf_depreciacion_del_periodo.php","","limpiar","Limpiar",8);   
    ?></td>
</tr>
</tbody>
</table> 

<div id="resultados"></div>