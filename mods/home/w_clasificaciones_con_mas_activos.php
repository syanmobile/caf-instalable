<div class="col-md-4 grid-item">
	<div class="panel panel-primary">  
		<div class="panel-heading">
			<b>Clasificación con más Activos</b>
			<div style="float: right;">
				<a href="javascript:ampliar_clasificacion_con_activos()" style="color:#fff;">(Ampliar)</a>
			</div>
		</div>
		<div class="panel-body"> 
			<?php
			$total = 0;
			$labeles = "";
			$labeles_largo = "";
			$valores = "";

			$res = mysqli_query($cnx,"select * from resumen_clasificacion order by res_total desc limit 0,10"); 
			if(mysqli_num_rows($res) > 0){
				?>
				<table class="table table-condensed table-bordered"> 
				<thead>
					<tr>
						<th width="1">Stock</th>
						<th>Clasificación</th>
					</tr>
				</thead>
				<tbody>
				<? 
				while($row = mysqli_fetch_array($res)){ 
					?>
					<tr>  
						<th style="text-align: center;"><? echo _num2($row["res_total"]); ?></th>
						<td><? echo $_categorias[$row["res_categoria"]]; ?></td> 
					</tr>
					<? 
					$labeles.= ";".$_categorias[$row["res_categoria"]];
					$labeles_largo.= ";".$_categorias[$row["res_categoria"]]." (".$row["res_total"].")";
					$valores.= ";".$row["res_total"];
				}
				?>  
				</tbody>
				</table>
				<?
			}else{
				?>
				<div class="alert alert-danger">
					<strong>Sin stock de activos</strong>
				</div>
				<?php 
			}
			?>  
			<iframe src="mods/home/widget_grafico_donas.php?label=<? echo $labeles; ?>&valor=<? echo $valores; ?>&alto=240" frameborder="0" scrolling="no" style="width: 100%; height: 230px;"></iframe> 
			<script type="text/javascript">
			function ampliar_clasificacion_con_activos(){
				visor("widget_grafico_donas.php?label=<? echo $labeles_largo; ?>&valor=<? echo $valores; ?>&alto=390&titulo=Activos por Clasificación&imprimir=s");
			}
			</script> 
		</div>
	</div>
</div>