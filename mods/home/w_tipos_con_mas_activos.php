<div class="col-md-4 grid-item">
	<div class="panel panel-primary">  
		<div class="panel-heading">
			<b>Tipos de Activos con más Activos</b>
			<div style="float: right;">
				<a href="javascript:ampliar_tipos_con_mas_activos()" style="color:#fff;">(Ampliar)</a>
			</div>
		</div>
		<div class="panel-body">  
			<?php
			$total = 0;
			$labeles = "";
			$labeles_largo = "";
			$valores = "";

			$res = mysqli_query($cnx,"select * from resumen_tipo order by res_total desc");
			if(mysqli_num_rows($res) > 0){
				?>
				<table class="table table-condensed table-bordered"> 
				<thead>
					<tr>
						<th width="1">Activos</th>
						<th>Tipo de Activo</th>
						<th width="1">%</th>
					</tr>
				</thead>
				<tbody>
				<? 
				while($row = mysqli_fetch_array($res)){ 
					$porc_ = round(_porc($row["res_total"],$total_de_activos),2);
					?>
					<tr>  
						<th style="text-align: center; padding: 1px 5px;"><? echo _num2($row["res_total"]); ?></th>
						<td style="padding: 1px 5px;"><? echo $_tipo_activo[$row["res_tipo"]]; ?></td> 
						<td style="text-align: center; padding: 1px 5px;"><? echo $porc_; ?>%</td>
					</tr>
					<? 
					$labeles.= ";".$_tipo_activo[$row["res_tipo"]];
					$labeles_largo.= ";".$_tipo_activo[$row["res_tipo"]]." (".$row["res_total"].")";
					$valores.= ";".$row["res_total"];
				}
				?>  
				</tbody>
				</table>
				<?
			}
			?>
			<iframe src="mods/home/widget_grafico_donas.php?label=<? echo $labeles; ?>&valor=<? echo $valores; ?>&alto=240" frameborder="0" scrolling="no" style="width: 100%; height: 230px;"></iframe>
			<script type="text/javascript">
			function ampliar_tipos_con_mas_activos(){
				visor("widget_grafico_donas.php?label=<? echo $labeles_largo; ?>&valor=<? echo $valores; ?>&alto=390&titulo=Activos clasificados por Tipo de Activo&imprimir=s");
			}
			</script>
		</div>
	</div>
</div>