<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Mantenimientos de Activos Fijos";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?> 
<table class="table table-bordered table-condensed table-info">
<tr>  
    <td width="100">
    <b>Código:</b><br />
    <input type="text" name="fil_codigo" id="fil_codigo" class="campos w10" value="<? echo $_REQUEST["fil_codigo"]; ?>"></td> 
    <td>
    <b>Descripción:</b><br />
    <input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? echo $_REQUEST["fil_descripcion"]; ?>"></td>   
    <td>
    <b>Clasificación:</b><br />
    <select name="fil_categoria" id="fil_categoria" class="campos buscador w10">
    <option value=""></option>
    <?php
    $res = sql_categorias("*"," order by cat.cat_codigo asc"); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["cat_codigo"]; ?>" <?
            if($_REQUEST["fil_categoria"] == $row["cat_id"]){ echo "selected"; } 
            ?>>C<? echo $row["cat_codigo"]." - ".$row["cat_nombre_largo"]; ?></option>
            <?
        }
    }
    ?></select></td>
    <td>
    <b>Ubicación:</b><br />
    <select name="fil_ubicacion" id="fil_ubicacion" class="campos buscador w10">
    <option value=""></option>
    <?php
    $res = sql_ubicaciones("*"," order by bod.bod_nombre asc, ubi.ubi_nombre asc"); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["ubi_id"]; ?>" <?
            if($_REQUEST["fil_ubicacion"] == $row["ubi_id"]){ echo "selected"; } 
            ?>>U<? echo $row["ubi_codigo"]." - ".$row["ubi_nombre"]; ?></option>
            <?
        }
    }
    ?></select></td>

    <td width="1"><?
    construir_boton("alerta_js('Sin datos');","1","importar","Descargar",4); 
    ?></td> 
    <td width="1"><?php
    construir_boton("inf_mantenimientos.php","","buscar","Filtrar");
    ?></td> 
</tr>
</table>

<?php


$v_codigo = $_REQUEST["fil_codigo"];
$v_descripcion =$_REQUEST["fil_descripcion"];
$v_grupo = $_REQUEST["fil_grupo"];
$v_subgrupo = $_REQUEST["fil_subgrupo"];
$v_categoria = $_REQUEST["fil_categoria"];
$v_filtro = "";


if($v_codigo!=""){
$v_filtro = " AND pro_codigo LIKE '%$v_codigo%' ";
}
if($v_descripcion !=""){
$v_filtro = $v_filtro." AND pro_nombre LIKE '%$v_descripcion%' ";
}

if($v_ubicacion !=""){
$v_filtro = $v_filtro." AND acf_ubicacion = '$v_ubicacion' ";
}

if($v_grupo !=""){
$v_filtro = $v_filtro." AND pro_grupo='$v_grupo' ";
}

if($v_subgrupo !=""){
$v_filtro = $v_filtro." AND pro_subgrupo='$v_subgrupo' ";
}

if($v_categoria !=""){
$v_filtro = $v_filtro." AND pro_categoria='$v_categoria' ";
}
 
$res = sql_acf_mantenimientos("*,sum(man_costo) as costos,count(*) as cantidad,sum(man_hh) as hh","group by acf_id  order by man_inicio asc ,acf_serie asc"); 
if(mysqli_num_rows($res) > 0){
    ?>
    <table class="table table-striped table-bordered tabledrag table-condensed"> 
    <thead>
    <tr>
        <th <? if($_REQUEST["excel"] == ""){ ?>width="1"<? } ?>></th>
        <th <? if($_REQUEST["excel"] == ""){ ?>width="1"<? } ?>>Codigo</th>
        <th <? if($_REQUEST["excel"] == ""){ ?>width="1"<? } ?>>Serial</th>
        <th>Producto</th>
        <th width="100">Centro Costo</th>
        <th>Ubicacion</th>
        <th <? if($_REQUEST["excel"] == ""){ ?>width="1"<? } ?>>Mantenciones</th>
        <th <? if($_REQUEST["excel"] == ""){ ?>width="1"<? } ?>>Ult.Mantención</th>
        <th <? if($_REQUEST["excel"] == ""){ ?>width="1"<? } ?>>H.Hombre</th>
        <th <? if($_REQUEST["excel"] == ""){ ?>width="1"<? } ?>>Costo</th>
    </tr>
    </thead>
    <tbody>
    <?
	while($row = mysqli_fetch_array($res)){
        ?>
        <tr> 
            <td><?
            construir_boton("acf_info.php","&acf=".$row["acf_id"],"buscar","Ver Activo Fijo"); 
            ?></td>
            <th><? echo $row["acf_producto"]; ?></th>
            <th><? echo $row["acf_serie"]; ?></th>
            <td><? echo $row["pro_nombre"]; ?></td>
            <td><?php echo $row["ubi_centro_costo"]; ?></td> 
            <td><?php echo $row["ubi_nombre"]; ?></td> 
            <td><? echo _num2($row["cantidad"]); ?></td>
            <td><? echo _fec($row["man_inicio"],5); ?></td> 
            <td><? echo _num2($row["hh"]); ?></td>
            <td style="text-align: right;"><? echo _num($row["costos"]); ?></td>
        </tr>
        <?
        $hh_final += $row["hh"];
        $total_final += $row["costos"];
	}
	?> 
	</tbody>
	</table>
    <h4 style="text-align: right">HH Final:<br><? echo _num2($hh_final); ?></h4>
	<h4 style="text-align: right">Costo Final:<br><? echo _num($total_final); ?></h4>
	<?
}else{
	?>
	<div class="alert alert-danger">
		<strong>Sin registros creados</strong>
	</div>
	<?php 
} 
?>