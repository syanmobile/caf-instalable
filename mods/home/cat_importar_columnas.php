<?php 
require("../../inc/conf_dentro.php");
?>
<table width="100%">
<tr valign="top">
	<td width="1">
		<?
		if (isset($_FILES["archivo"]) && is_uploaded_file($_FILES['archivo']['tmp_name'])) {							
		    $fp = fopen($_FILES['archivo']['tmp_name'], "r");
		    while (!feof($fp)){ 
				$linea++;
				$data = explode(";", fgets($fp));
				if($linea == 1){
					$datos = $data; 
				}else{
					if($linea < 20){
						$lineas[$linea] = $data;
					}	
				}
		    }
		} 
		$campos[] = "Código Clasificación";
		$campos[] = "Código Clasificación Padre";
		$campos[] = "Nombre Clasificación";

		$obligatorios = array(1,3);
		?>
		<table class="table table-bordered table-hover table-condensed"> 
		<thead> 
		    <tr>
		        <th width="1">Campo</th>        
		        <th>Campo Archivo</th>
		    </tr>
		</thead>
		<tbody>
		<?php 
		foreach($campos as $campo){
			$campo_i++;
			?>
			<tr style="<? if(in_array($campo_i,$obligatorios)){ echo 'background:yellow;'; } ?>">
				<td><? echo "<b>".$campo."</b>"; 
				if(in_array($campo_i,$obligatorios)){ echo '<br>(*) Obligatorio'; }
				?></td>
				<td><select name="columna[]" class="campos buscador" id="col<? echo $campo_i; 
				?>" onchange="javascript:columnas_cambio();">
					<option value=""></option>
					<?
					$i = 0;
					foreach($datos as $dato){
						?><option value="<? echo $i; ?>"><? echo _u8e($dato); ?></option>
						<?
						$i++;
					}
					?>
				</select></td>
			</tr>
			<?
		}
		?>
		</tbody>
		</table>
		<input type="hidden" id="columnas" name="columnas">

		<script type="text/javascript">
		function columnas_cambio(){ 
			var datos = "";
			<?
			for($i = 1;$i <= $campo_i;$i++){
				?>datos = datos + ";" + document.getElementById("col<? echo $i; ?>").value;
				<?
			}
			?>
			document.getElementById("columnas").value = datos;
		}
		</script>
		<?php 
		construir_boton("grabar();","","derecha","Defina las Columnas",4); // funcion propia icono y texto
		?>
	</td>
	<td style="padding-left: 10px;">
		<table class="table table-bordered table-hover table-condensed"> 
		<thead> 
		    <tr>
		        <th colspan="<? echo count($datos); ?>">Visualizar los primeros registros del archivo</th>
		    </tr>
		</thead>
		<tbody style="font-size: 8px !important;">
		<tr>
			<?php
			foreach($datos as $dato){
				?><th><? echo _u8e($dato); ?></th><?
			}
			?>
		</tr>
		<?php
		for($i = 2;$i <= 15;$i++){
			?>
			<tr>
				<?php
				for($i2 = 0;$i2 < count($datos);$i2++){
					?><td><? echo _u8e($lineas[$i][$i2]); ?></td><?
				}
				?>
			</tr>
			<?
		}
		?>
		</tbody>
		</table>
	</td>
</tr>
</table>

<script language="javascript"> 
function grabar(){
	<?
	foreach($obligatorios as $obliga){
		?>
		if(document.getElementById("col<? echo $obliga; ?>").value == ""){
			alert("Campo obligatorio: <? echo $campos[$obliga - 1]; ?>");
			document.getElementById("col<? echo $obliga; ?>").focus();
			return;
		}
		<?
	}
	?> 
    document.multiple_upload_form.action = "<?php echo $url_web; ?>mods/home/cat_importar_validacion.php";
    $('#multiple_upload_form').ajaxForm({
        target:'#procesando_upload',
        data: { columnas: document.getElementById("columnas").value },
        beforeSubmit:function(e){
            $('.uploading').show();
        },
        success:function(e){
            $('.uploading').hide();
        },
        error:function(e){
        }
    }).submit(); 
}
</script> 