<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Transacciones";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "transacciones.php"; 
include("ayuda.php");
//---------------------------------------------------------------------------------------- 
?> 
<table class="table table-bordered table-condensed table-info" style="margin-bottom:0px !important;">  
<tbody> 
<tr> 
    <th width="1">Tipo:</th>
    <td width="1">
    <select name="fil_tipo" id="fil_tipo" class="campos">
    	<option value=""></option> 
		<option value="E" <? if($_REQUEST["fil_tipo"] == "E"){ echo "selected"; } ?>>Ingresos</option>
		<option value="S" <? if($_REQUEST["fil_tipo"] == "S"){ echo "selected"; } ?>>Salidas</option> 
		<option value="A" <? if($_REQUEST["fil_tipo"] == "A"){ echo "selected"; } ?>>Ajustes</option> 
    </select>
    </td>
    <th width="1">Folio:</th>
    <td width="120"><input type="text" name="fil_folio" class="campos w10" value="<? echo $_REQUEST["fil_folio"]; ?>"></td>
    <td><?php
    construir_boton("transacciones.php","","buscar","Filtrar");
    ?></td> 
</tr>
</table>

<?php   
if($_REQUEST["fil_tipo"] <> ""){
	$extra .= "and mov.mov_tipo = '".$_REQUEST["fil_tipo"]."' ";
}  
if($_REQUEST["fil_folio"] <> ""){
	$extra .= "and mov.mov_folio = '".$_REQUEST["fil_folio"]."' ";
}
?>
<input type="hidden" name="pagina" id="pagina" value="transacciones.php" class="campos">
<?
$sql_paginador = sql_movimientos("*","   $extra order by mov.mov_id desc ","s"); 
$cant_paginador = 20;
require("../../_paginador.php");
$res = mysqli_query($cnx,$sql_final);
if(mysqli_num_rows($res) > 0){
    ?>
    <table class="table table-bordered table-hover table-condensed"> 
    <thead>
    <tr>   
        <th width="1">&nbsp;</th>  
        <th width="1">&nbsp;</th>  
        <th style="text-align:center;" width="1">Fecha</th> 
        <th style="text-align:center;" width="1">Folio</th> 
        <th style="text-align:center;" width="1">Tipo</th>
        <th width="130">Concepto</th> 
        <th>Notas</th>
        <th>Lugar</th>
        <th width="200">Autor</th>
        <th width="200">Responsable</th> 
        <th width="1">Total</th>
    </tr>
    </thead>
    <tbody> 
    <?php 
    while($row = mysqli_fetch_array($res)){ 
        ?>
        <tr> 
            <td style="text-align:center;"><?php
            construir_boton("visor('aju_pdf.php?id=".$row["mov_id"]."')","1","imprimir","Mostrar Detalle",4); 
            ?></td>
            <td style="text-align:center;"><?php
            if($row["mov_tipo"] == "E"){
                construir_boton("elim_ingreso.php","&id=".$row["mov_id"],"eliminar","Eliminar"); 
            }
            ?></td>    
            <td style="text-align:center;"><?php echo _fec($row["mov_fecha"],5); ?></td> 
            <td style="text-align:center;"><?php echo $row["mov_folio"]; ?></td>
            <td><?php echo $_tipos[$row["mov_tipo"]]; ?></td> 
            <td><?php echo $_conceptos[$row["mov_concepto"]]; ?></td> 
            <td><?php echo $row["mov_glosa"]; ?></td>
            <td><?php echo $_bodegas[$row["mov_bodega"]]; ?></td>  
            <td><?php echo $_personas[$row["mov_per_id"]]; ?></td>
            <td><?php echo $_personas[$row["mov_responsable"]]; ?></td>
            <td style="text-align: right"><?php echo _num($row["mov_total"]); ?></td>
        </tr> 
        <?php 
    }
    ?> 
    </tbody>
    </table>  
    <?php
	echo $paginador_dibujo;
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin registros creados</strong>
    </div>
    <?php 
} 
?>