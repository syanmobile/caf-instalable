<?php 
require("../../inc/conf_dentro.php");
			
construir_boton("vista('');","","buscar","Ver Todo",4);
construir_boton("vista('rojo');","","buscar","Fallas",4);
construir_boton("vista('ok');","","buscar","Registros OK",4);

// Cargar codigos....
$res = sql_auxiliares("*",""); 
if(mysqli_num_rows($res) > 0){
	while($row = mysqli_fetch_array($res)){
		$codigos[] = $row["aux_codigo"];
	}
}
?>
<table class="table table-bordered table-hover table-condensed"> 
<thead> 
    <tr>
        <th width="1">#</th>
        <th>Código</th>
        <th>Rut</th>
        <th>Nombre Proveedor</th>
        <th>Dirección</th>
        <th>Comuna</th>
        <th>Telefono</th>
        <th>Correo</th>
        <th>Validación</th>
    </tr>
</thead>
<tbody>
<?php 
$capt = date("YmdHis");
if (isset($_FILES["archivo"]) && is_uploaded_file($_FILES['archivo']['tmp_name'])) {							
    $fp = fopen($_FILES['archivo']['tmp_name'], "r");
    while (!feof($fp)){ 
		$data = explode(";", utf8_encode(fgets($fp)));   
		$nlinea++;
		if($nlinea > 1){  
			if(count($data) > 1){  
				$codigo = rtrim($data[0]);
				$rut = rtrim($data[1]);
				$nombre = rtrim($data[2]);
				$direccion = rtrim($data[3]);
				$comuna = rtrim($data[4]);
				$telefono = rtrim($data[5]);
				$correo = rtrim($data[6]);
				$total_campos = 7;

				//Validaciones
				$errores = ""; 
				if($codigo == "" || 
					$nombre == ""){ 
					$errores.= "<li>Un campo obligatorio no ingresado</li>";
				}
				if(in_array($codigo,$codigos)){
					$errores.= "<li>Código de Proveedor ya existe</li>";
				} 
				$linea++;
				?>
				<tr class="fila_<?php echo ($errores <> "")?"rojo":"ok"; ?>">
					<td><? echo $linea; ?></td>
					<?
					for($i = 0;$i < $total_campos;$i++){
						?><td><? echo $data[$i]; ?></td><?
					}

					if($errores <> ""){
						$fallo = "S";
						?><td style="padding-left:15px;" class="alert alert-danger"><? echo $errores; ?></td><?php
					}else{
						?><td style="padding-left:15px;"  class="alert alert-success"><li>Todo OK</li></td><?php
					}
					?>
				</tr>
				<?  
			}
		} // if nlinea > 1
    }
} 
?>
</tbody>
</table>

<script language="javascript"> 
function grabar(){
	if(confirm("Importar los registros nuevos?")){
        document.multiple_upload_form.action = "<?php echo $url_web; ?>mods/home/aux_importar_grabacion.php";
        $('#multiple_upload_form').ajaxForm({
            target:'#procesando_upload',
            beforeSubmit:function(e){
                $('.uploading').show();
            },
            success:function(e){
                $('.uploading').hide();
            },
            error:function(e){
            }
        }).submit();
    }
}
</script> 