<?php 
require("../../inc/conf_dentro.php");
			
construir_boton("vista('');","","buscar","Ver Todo",4);
construir_boton("vista('rojo');","","buscar","Fallas",4);
construir_boton("vista('ok');","","buscar","Registros OK",4);

// Cargar codigos....
$res = mysqli_query($cnx,"select pro_codigo from productos"); 
if(mysqli_num_rows($res) > 0){
	while($row = mysqli_fetch_array($res)){
		$codigos[] = $row["pro_codigo"];
	}
}
?>
<table class="table table-bordered table-hover table-condensed"> 
<thead> 
    <tr>
        <th width="1">#</th>
        <th>Código Producto</th>
        <th>Nombre Producto</th>
        <th>Descripción</th>
        <th>Cód.Barra</th>
        <th>Tipo</th> 
        <th>Clasificación</th>
        <th>Unidad</th>
        <th>Req.Mantenimiento</th>
        <th>Garantía</th>
        <th>Prestable</th>
        <th>Asignable</th> 
        <th>Validación</th>
    </tr>
</thead>
<tbody>
<?php 
$capt = date("YmdHis");
if (isset($_FILES["archivo"]) && is_uploaded_file($_FILES['archivo']['tmp_name'])) {							
    $fp = fopen($_FILES['archivo']['tmp_name'], "r");
    while (!feof($fp)){ 
		$data = explode(";", fgets($fp));  
		$nlinea++;
		if($nlinea > 1){  
			if(count($data) > 1){			
				$codigo = rtrim($data[0]);
				$nombre = rtrim($data[1]);
				$descripcion = rtrim($data[2]);
				$codbarra = rtrim($data[3]);
				$tipo = rtrim($data[4]);
				$clasificacion = rtrim($data[5]);
				$unidad = rtrim($data[6]);
				$manteni = rtrim($data[7]);
				$garantia = rtrim($data[8]);
				$prestable = rtrim($data[9]);
				$asignable = rtrim($data[10]);
				$total_campos = 11;

				//Validaciones
				$errores = ""; 
				if($codigo == "" || $nombre == "" || $tipo == ""){
					$errores.= "<li>Campos obligatorio no ingresados: ";
					if($codigo == ""){ $errores .= "<br>- Código"; }
					if($nombre == ""){ $errores .= "<br>- Nombre"; }
					if($tipo == ""){ $errores .= "<br>- Tipo"; } 
					$errores.= "</li>";
				} 
				if(in_array($codigo,$codigos)){
					$errores.= "<li>Producto ya existe</li>";
				}
				if(in_array($codigo,$codigos_rep)){
					$errores.= "<li>Código Producto repetido</li>";
				}
				$codigos_rep[] = $codigo;
				if($tipo <> ""){
					if(!in_array($tipo,$_tipo_activos)){ 
						$errores.= "<li>Tipo Activo Desconocido</li>";
					}
				} 
				if($clasificacion <> ""){
					if(!in_array($clasificacion,$_categorias_codigos)){ 
						$errores.= "<li>Clasificación Desconocida</li>";
					}
				}
				if($unidad <> ""){
					if(!in_array($unidad,$_unidades_codigos)){ 
						$errores.= "<li>Unidad Desconocida</li>";
					}
				}			
				
				$linea++;
				?>
				<tr class="fila_<?php echo ($errores <> "")?"rojo":"ok"; ?>">
					<td><? echo $linea; ?></td>
					<?
					for($i = 0;$i < $total_campos;$i++){
						?><td><? echo $data[$i]; ?></td><?
					}

					if($errores <> ""){
						$fallo = "S";
						?><td style="padding-left:15px;" class="alert alert-danger"><? echo $errores; ?></td><?php
					}else{
						?><td style="padding-left:15px;"  class="alert alert-success"><li>Todo OK</li></td><?php
					}
					?>
				</tr>
				<?  
			}
		} // nlinea > 1
    }
} 
?>
</tbody>
</table>

<script language="javascript"> 
function grabar(){
    <?php
    if($fallo == ""){
        ?>
        document.multiple_upload_form.action = "<?php echo $url_web; ?>mods/home/pro_importar_grabacion.php";
        $('#multiple_upload_form').ajaxForm({
            target:'#procesando_upload',
            beforeSubmit:function(e){
                $('.uploading').show();
            },
            success:function(e){
                $('.uploading').hide();
            },
            error:function(e){
            }
        }).submit();
        <?
    }else{
        ?>
		alert("Deben estar todos los datos validados");
        <?
    }
    ?>
}
</script> 