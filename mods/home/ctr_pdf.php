<?php
ob_start();
require("../../inc/conf_dentro.php");

$res = sql_controles("*"," and ctr.ctr_id = ".$_REQUEST["id"]); 
if(mysqli_num_rows($res) > 0){
	$row = mysqli_fetch_array($res);
}
$titulo_pdf = "Control de Activos #".$_REQUEST["id"];
require('../../inc/pdf/cabecera.php');

/****************************************************************************************************/
 
$pdf->SetFont('Arial','b',8);

$total = $row["ctr_encontrados"] + $row["ctr_otros"];
$contados = $row["ctr_encontrados"] + $row["ctr_extravios"];

$y = $pdf->GetY();
$pdf->Cell(80,7,"Fecha/Hora: ".$row["ctr_fecha"],1,1,'L');
$pdf->Cell(80,7,"Responsable: "._u8e($_personas[$row["ctr_per_id"]]),1,1,'L');
$pdf->Cell(80,7,"Ubicacion: "._u8e($_ubicaciones[$row["ctr_ubi_id"]]),1,1,'L');
$pdf->Cell(80,7,"TAG en la ubicación: "._num2($contados),1,1,'L');
$pdf->Cell(80,7,"TAG detectados: "._num2($total),1,1,'L');
 
/****************************************************************************************************/

$pdf->SetFont('Arial','b',30);
$pdf->SetY($y);

$porc = round(_porc($row["ctr_encontrados"],$row["ctr_buscados"]));

$pdf->Cell(85,22,"",0,0,'L');
$pdf->SetTextColor(14,107,21); // VERDE
$pdf->Cell(35,22,_num2($row["ctr_encontrados"]),0,0,'C');
$pdf->SetTextColor(173,42,27); // ROJO
$pdf->Cell(35,22,_num2($row["ctr_extravios"]),0,0,'C');
$pdf->SetTextColor(0,0,0);
$pdf->Cell(0,22,_num2($porc)." %",0,1,'C');

$pdf->SetFont('Arial','b',13);

$pdf->Cell(85,7,"",0,0,'L');
$pdf->SetTextColor(14,107,21); // VERDE
$pdf->Cell(35,7,"Encontrados",0,0,'C');
$pdf->SetTextColor(173,42,27); // ROJO
$pdf->Cell(35,7,"Extraviados",0,0,'C');
$pdf->SetTextColor(0,0,0);
$pdf->Cell(0,7,"Acierto %",0,1,'C');

/****************************************************************************************************/
 
$pdf->Ln(10);

$pdf->SetFont('Arial','b',9);

$res = sql_controles_detalles("*"," and ctr.ctr_id = '$_REQUEST[id]' and det.det_estado = 1 group by det_tag");
$num = mysqli_num_rows($res);

$pdf->Cell(0,7,"ACTIVOS ENCONTRADOS ("._num2($num).")",1,1,'L');

if(mysqli_num_rows($res) > 0){

	$pdf->SetFont('Arial','b',8);
	$pdf->Cell(50,7,"TAG",1,0,'L');
	$pdf->Cell(20,7,"Serie",1,0,'L');
	$pdf->Cell(0,7,"Activo",1,1,'L');

	$pdf->SetFont('Arial','',7);
	while($row = mysqli_fetch_array($res)){
		$pdf->Cell(50,7,$row["det_tag"],1,0,'L');
		$pdf->Cell(20,7,$row["acf_serie"],1,0,'L');
		$pdf->Cell(0,7,_u8d($row["pro_codigo"]." - ".$row["pro_nombre"]),1,1,'L');
	}
}

/****************************************************************************************************/
 
$pdf->Ln(5);

$pdf->SetFont('Arial','b',9);

$res = sql_controles_detalles("*"," and ctr.ctr_id = '$_REQUEST[id]' and det.det_estado = 0");
$num = mysqli_num_rows($res);

$pdf->Cell(0,7,"ACTIVOS EXTRAVIADOS ("._num2($num).")",1,1,'L');

if($num > 0){

	$pdf->SetFont('Arial','b',8);
	$pdf->Cell(50,7,"TAG",1,0,'L');
	$pdf->Cell(20,7,"Serie",1,0,'L');
	$pdf->Cell(0,7,"Activo",1,1,'L');

	$pdf->SetFont('Arial','',7);
	while($row = mysqli_fetch_array($res)){
		$pdf->Cell(50,7,$row["det_tag"],1,0,'L');
		$pdf->Cell(20,7,$row["acf_serie"],1,0,'L');
		$pdf->Cell(0,7,_u8d($row["pro_codigo"]." - ".$row["pro_nombre"]),1,1,'L');
	}
}

/****************************************************************************************************/
 
$pdf->Ln(5);

$pdf->SetFont('Arial','b',9);

$res = sql_controles_detalles("*"," and ctr.ctr_id = '$_REQUEST[id]' and det_acf_id <> 0 and det.det_estado = 2");
$num = mysqli_num_rows($res);

$pdf->Cell(0,7,"ACTIVOS DE OTRA UBICACION ENCONTRADOS ("._num2($num).")",1,1,'L');

if($num > 0){

	$pdf->SetFont('Arial','b',8);
	$pdf->Cell(50,7,"TAG",1,0,'L');
	$pdf->Cell(20,7,"Serie",1,0,'L');
	$pdf->Cell(0,7,"Activo",1,1,'L');

	$pdf->SetFont('Arial','',7);
	while($row = mysqli_fetch_array($res)){
		$pdf->Cell(50,7,$row["det_tag"],1,0,'L');
		$pdf->Cell(20,7,$row["acf_serie"],1,0,'L');
		$pdf->Cell(0,7,_u8d($row["pro_codigo"]." - ".$row["pro_nombre"]),1,1,'L');
	}
}

/****************************************************************************************************/
 
$pdf->Ln(5);

$pdf->SetFont('Arial','b',9);

$res = sql_controles_detalles("*"," and ctr.ctr_id = '$_REQUEST[id]' and det_acf_id = 0 and det.det_estado = 2");
$num = mysqli_num_rows($res);

$pdf->Cell(0,7,"TAGS DESCONOCIDOS ("._num2($num).")",1,1,'L');

if($num > 0){ 

	$pdf->SetFont('Arial','',7);
	while($row = mysqli_fetch_array($res)){
		$pdf->Cell(0,7,$row["det_tag"],1,1,'L'); 
	}
}

/****************************************************************************************************/

$pdf->Output('control_'.$_REQUEST["id"].'.pdf','I');
$pdf->Close();
?>