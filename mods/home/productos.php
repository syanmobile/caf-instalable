<?php
require("../../inc/conf_dentro.php");
 
$titulo_pagina = "Productos";
construir_breadcrumb($titulo_pagina);  
?> 
<style type="text/css">
body{ background-color:#EAEAEA !important; } 
.boton_inicio{  
	border-top: 1px solid #CCC;
	width: 100%; 
}
.boton_inicio a{
	color:#000 !important;
	text-decoration:none;
}
.boton_inicio span{
	font-size:11px; 
	font-weight:bold;
}
.boton_inicio:hover{  
	background-color:#FFC;
}
a:hover .span{ 
	font-size:13px !important; 
}
</style> 

<div class="col-md-3 grid-item">
	<div class="panel panel-primary">  
		<div class="panel-heading">
			<b><?php echo $row["gru_nombre"]; ?></b>
		</div>
		<div class="panel-body" style="padding:0px;"> 
		<? 
		$res2 = sql_navegacion("*","where nav.nav_elim = 0 and gru.gru_id = 10 
		order by nav.nav_orden asc");
		if(mysqli_num_rows($res2) > 0){
			while($row2 = mysqli_fetch_array($res2)){
				if(in_array($row2["nav_codigo"],$opciones_persona)){ 
					?>
					<div class="boton_inicio">
						<a href="javascript:carg('<?php echo $row2["nav_link"]; ?>','');">
						<table width="100%" style="margin:3px;">
						<tr>
							<td width="45" style="text-align:center"><img src="img/modulos/<?php echo ($row2["nav_icono"] == "")?"noicon.png":$row2["nav_icono"]; ?>" width="40"></td> 
							<td>
							<span><?php echo $row2["nav_nombre"]; ?></span>
							<p><?php echo $row2["nav_descripcion"]; ?></p>
							</td>
						</tr>
						</table>
						</a> 
					</div>
					<?
				}
			}
		} 
		?>
		</div>
	</div>
</div>