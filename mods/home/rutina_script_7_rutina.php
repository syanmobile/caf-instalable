<?php
require("../../inc/conf_dentro.php");
require("../../_css.php"); 
//----------------------------------------------------------------------------------------
$sql = "select * from activos_fijos";
$res = mysqli_query($cnx,$sql);
$total = mysqli_num_rows($res);

$sql = "select * from activos_fijos where acf_procesando = 1 ";
$res = mysqli_query($cnx,$sql);
$por_actualizar = mysqli_num_rows($res); 

$listos = $total - $por_actualizar;
$porcentaje = round(_porc($listos,$total),2);

if($por_actualizar == 0){
	$porcentaje = 100;
}
?>
<body>
<table class="table table-bordered table-hover table-condensed">  
<tr>
	<td>
		<?php		
		echo "Activos Listos: <strong>"._num($listos)."</strong> de un total de <strong>"._num($total)."</strong>";
		?> 
	
		<div class="progress">
		  <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="<? echo intval($porcentaje); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <? echo intval($porcentaje); ?>%">
		  </div>
		</div>
		<h4><? echo $porcentaje; ?>% Completado</h4>
	</td>
</tr>
</table> 
<?
$res = sql_activos_fijos("*"," and acf_procesando = 1 limit 0,1 ");  
if(mysqli_num_rows($res) > 0){
	while($datos = mysqli_fetch_array($res)){
		echo "<B>".$datos["acf_codigo"]."</b><br>";

		$sql2 = "delete from depreciaciones where dep_acf_id = $id "; 
		$res2 = mysqli_query($cnx,$sql2);

		$sql2 = generar_depreciaciones_del_activo($datos["acf_id"]);
		echo "<br>".$sql2;
		$res2 = mysqli_query($cnx,$sql2);
	}
}
$sql3 = "update activos_fijos set acf_procesando = 2 where acf_id = '".$datos["acf_id"]."' "; 
$res3 = mysqli_query($cnx,$sql3); 
?>
</body>
<?
function generar_depreciaciones_del_activo($id){

	$res = sql_activos_fijos("*"," and acf.acf_id = '$id' "); 
	$num = mysqli_num_rows($res);
	if($num > 0){
		$datos = mysqli_fetch_array($res); 

		$periodo = explode("-",$datos["acf_mes_ano_inicio_depreciacion"]);
		$mes = $periodo[1] * 1;
		$ano = $periodo[0] * 1;
		$meses = $datos["acf_vida_util"] * 1;
		if($datos["acf_tipo_depreciacion"] == "L"){
			$meses_divisor = $meses;
		}else{
			$meses_divisor = round($meses / 3);
		}

		$valor_activo = $datos["acf_valor_a_depreciar"] * 1;
		$valor_mensual = round(($valor_activo / $meses_divisor),2);

		$ano_limite = substr($datos["acf_fecha_salida"],0,4) * 1;
		$mes_limite = substr($datos["acf_fecha_salida"],5,2) * 1;

		for($i = 1;$i <= $meses_divisor;$i++){
			$acumulado += $valor_mensual; 
			$valor_activo -= $valor_mensual;

			$mesx = ($mes < 10)?"0".$mes:$mes;

			if(($datos["acf_fecha_salida"] == "0000-00-00") || ($ano <= $ano_limite && $mes <= $mes_limite)){ 
				if($sql_final == ""){
					$sql_final = "insert into depreciaciones (
						dep_acf_id,
						dep_periodo,
						dep_cuota,
						dep_acumulada,
						dep_neto 
					) values ";
				}else{
					$sql_final .= ",";
				}

				$sql_final .= "
				(
					'$id',
					'".$ano."-".$mesx."-01',
					'$valor_mensual',
					'$acumulado',
					'$valor_activo'
				)"; 
			}
			if($mes == 12){
				$mes = 0;
				$ano++;
			} 
			$mes++;
		} 
	}
	return $sql_final;
}  
?>