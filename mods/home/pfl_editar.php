<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Editar Perfil";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript">  
function save(){    
	if(document.getElementById("nombre").value == ""){
		alerta_js("Ingresar NOMBRE","Es obligación ingresar el nombre del perfil");
		return;	
	}
	carg("pfl_save.php","");
}
</script>

<?php
$res = sql_perfiles("*"," and pfl.pfl_id = '$_REQUEST[id]'","");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
	$opciones = explode(",",$datos["pfl_navegacion"]);   
}else{
	?>
	<div class="alert alert-danger"> 
		<strong>No se encontró el dato</strong>
	</div>
    <?php
	exit();
}
?>
  
<input type="hidden" name="modo" value="editar" class="campos">
<input type="hidden" name="id" value="<?php echo $_REQUEST["id"]; ?>" class="campos">
    
<table class="table table-striped table-bordered table-condensed">
<tr> 
    <td>
    	<b>Nombre Perfil:</b><br>
    	<input type="text" class="form-control campos" id="nombre" name="nombre" value="<?php echo $datos["pfl_nombre"]; ?>">
    </td>
</tr>
</table>

<ul class="nav nav-tabs" role="tablist"> 
<?
foreach($permisos_set as $permiso){
	if($permiso[0] <> $aux){
		$gr++;
		$aux = $permiso[0];
		?> 
		<li <? if($gr == 1){ ?>class="active"<? } ?>><a href="#tab_<?php echo $gr; ?>" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;"><?php echo $permiso[0]; ?></a></li>
		<?
	} 
}
?>
</ul>

<div class="tab-content">  
<?php
$aux = "";
foreach($permisos_set as $permiso){
	if($permiso[0] <> $aux){
		if($aux <> ""){
			echo '</table></div>';
		}
		$modulos++;
		$aux = $permiso[0];
		?>
		<div class="tab-pane <? if($modulos == 1){ echo 'active'; } ?>" id="tab_<?php echo $modulos; ?>">
			<table class="table table-striped table-bordered table-condensed">
			<tr class="<? echo $permiso[0]; ?>_clase"> 
				<td colspan="10" style="font-style: italic;"><input type="checkbox" name="todos_<? echo $modulos; ?>"> Marcar todas las opciones</td>
			</tr>
			<tr class="<? echo $permiso[0]; ?>_clase">
				<th width="1">Ver</th>
				<th width="1">Crear</th>
				<th width="1">Editar</th>
				<th width="1">Eliminar</th>
				<th width="1">Realizar</th>
				<th>Secciones / Operación</th>
			</tr>
			<?
	}
	?> 
	<tr class="<? echo $permiso[0]; ?>_clase"> 
		<td style="padding-left:8px;"><? if($permiso[3]){ ?><input type="checkbox" name="permiso[]" value="<? echo $permiso[2]; ?>_ver" <?
		if(in_array($permiso[2]."_ver",$opciones)){ echo "checked"; } ?> class="campos asis<? echo $modulos; ?>" alt="<? echo $modulos; ?>"><? } ?></td> 

		<td style="padding-left:15px;"><? if($permiso[4]){ ?><input type="checkbox" name="permiso[]" value="<? echo $permiso[2]; ?>_crear" <?
		if(in_array($permiso[2]."_crear",$opciones)){ echo "checked"; } ?> class="campos asis<? echo $modulos; ?>" alt="<? echo $modulos; ?>"><? } ?></td> 

		<td style="padding-left:15px;"><? if($permiso[5]){ ?><input type="checkbox" name="permiso[]" value="<? echo $permiso[2]; ?>_editar" <?
		if(in_array($permiso[2]."_editar",$opciones)){ echo "checked"; } ?> class="campos asis<? echo $modulos; ?>" alt="<? echo $modulos; ?>"><? } ?></td> 

		<td style="padding-left:27px;"><? 
		if($permiso[6]){ ?><input type="checkbox" name="permiso[]" value="<? echo $permiso[2]; ?>_eliminar" <?
		if(in_array($permiso[2]."_eliminar",$opciones)){ echo "checked"; } ?> class="campos asis<? echo $modulos; ?>" alt="<? echo $modulos; ?>"><? } ?></td> 

		<td style="padding-left:27px;"><? if($permiso[7]){ ?><input type="checkbox" name="permiso[]" value="<? echo $permiso[2]; ?>_realizar" <?
		if(in_array($permiso[2]."_realizar",$opciones)){ echo "checked"; } ?> class="campos asis<? echo $modulos; ?>" alt="<? echo $modulos; ?>"><? } ?></td> 

		<td><b><? echo $permiso[0]; ?> >></b> <? echo $permiso[1]; ?></td>
	</tr>
	<?
} 
?> 
</table>
</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
<?
for($i = 1;$i <= $modulos;$i++){
	?>  
	$("input[name=todos_<? echo $i; ?>]").change(function(){
        var valor = $("input[name=todos_<? echo $i; ?>]:checked").length;
        if(valor){
               $('input[alt=<? echo $i; ?>]').each( function() {
                  this.checked = true;
             });
        }else{
             $(".asis<? echo $i; ?>").removeAttr("checked");
        }
   });
	<?
}
?>
});
</script>
<?
construir_boton("","","grabar","Guardar",3);
construir_boton("pfl_listado.php","","eliminar","Cancelar",2);
?>  