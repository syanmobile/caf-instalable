<?php
ob_start();
require("../../inc/conf_dentro.php");

$res = sql_movimientos("*"," and mov.mov_id = ".$_REQUEST["id"]); 
if(mysqli_num_rows($res) > 0){
	$row = mysqli_fetch_array($res);
}
$titulo_pdf = $_tipos[$row["mov_tipo"]]." / ".$row["con_nombre"]." #".$row["mov_folio"];
require('../../inc/pdf/cabecera.php');

/****************************************************************************************************/

$y = $pdf->GetY() + 5;
for($i = 1;$i <= 5;$i++){
	if(_opc("extra_mov_".$i) > 0){
		$res2 = sql_campos("*"," and cam_id = "._opc("extra_mov_".$i));  
		$row2 = mysqli_fetch_array($res2); 
		$extras["Extra_".$i] = $row2["cam_nombre"];
	}
} 

if($row["aux_nombre"] <> ""){
	$lineas_cabecera[] = array("Proveedor",$row["aux_nombre"]);
}
$lineas_cabecera[] = array("Folio",$row["mov_folio"]);
$lineas_cabecera[] = array("Fecha Movimiento",_fec($row["mov_fecha"],5));
$lineas_cabecera[] = array("Responsable",$_personas[$row["mov_responsable"]]);
$lineas_cabecera[] = array("Concepto",$_conceptos[$row["mov_concepto"]]);
$lineas_cabecera[] = array("Centro Costo",$_centro_costos[$row["mov_cco_id"]]);
$lineas_cabecera[] = array("Notas",$row["mov_glosa"]);   

foreach($lineas_cabecera as $linea){
	$pdf->SetFont('Arial','b',7);
	$pdf->Cell(30,5,_u8d($linea[0]),1,0,'L'); 
	$pdf->SetFont('Arial','',7);
	$pdf->Cell(0,5,_u8d($linea[1]),1,1,'L');
}

$pdf->Ln(5);
$pdf->SetFont('Arial','b',7);
  
$pdf->Cell(12,7,"Cant",1,0,'C'); 
$pdf->Cell(142,7,"Detalle Movimiento",1,0,'L');   
$pdf->Cell(16,7,"Valor",1,0,'C'); 
$pdf->Cell(0,7,"Total",1,1,'C');   

/****************************************************************************************************/

$pdf->SetFont('Arial','',7);

$res = sql_movimientos_detalle_activo("*"," and mov.mov_id = '$_REQUEST[id]' order by pro.pro_codigo asc "); 
if(mysqli_num_rows($res) > 0){ 
	while($row = mysqli_fetch_array($res)){  
		$i++;
		
		$cantidad = $row["det_validacion"] + $row["det_ingreso"] - $row["det_egreso"];		
		
		$pdf->Cell(12,5,$cantidad,0,0,'C'); 
		$pdf->Cell(142,5,_u8d($row["pro_nombre"]." (Cod.Producto: ".$row["pro_codigo"].")"),0,0,'L');
		$pdf->Cell(16,5,_num($row["det_valor"]),0,0,'R');  
		$pdf->Cell(0,5,_num($row["det_total"]),0,1,'R');  
				
		$pdf->Cell(12,4,'',0,0,'C'); 
		$pdf->Cell(0,4,"Ubicacion: "._u8d($_ubicaciones[$row["det_ubi_id"]]),0,1,'L');
		
		if($row["det_lote"] <> ""){
			$pdf->Cell(12,4,'',0,0,'C'); 
			$pdf->Cell(0,4,"Serie: ".$row["det_lote"],0,1,'L');
		}
		if($row["det_notas"] <> ""){
			$pdf->Cell(12,4,'',0,0,'C'); 
			$pdf->Cell(0,4,"Notas: ".$row["det_notas"],0,1,'L');
		}
		if($row["det_acf_id"] <> "0"){
			$pdf->Cell(12,4,'',0,0,'C'); 
			$pdf->Cell(0,4,"Cod.Activo: ".$row["acf_codigo"],0,1,'L');
		}
		
		$y = $pdf->GetY();
		$pdf->Line(10,$y,200,$y);
		
		$total += $row["det_total"];
	} 
}
$pdf->SetFont('Arial','b',8);
$pdf->Cell(171,4,"TOTAL:",0,0,'R');
$pdf->Cell(0,4,_num($total),0,1,'R'); 
 
$pdf->Output('ajuste_'.$_REQUEST["id"].'.pdf','I');
$pdf->Close();
?>