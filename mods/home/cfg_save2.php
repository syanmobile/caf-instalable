<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabación Parámetros de Configuración";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

switch($_POST["modo"]){
		
	case "crear":
		$sql = "select * from configuracion where cfg_codigo = '$_REQUEST[codigo]' ";
		$res = mysqli_query($cnx,$sql); 
		if(mysqli_num_rows($res) == 0){
			$sql = "INSERT INTO configuracion (
				cfg_grupo,
				cfg_codigo,
				cfg_titulo,
				cfg_valor,
				cfg_tipo
			) VALUES (
				'$_POST[grupo]',
				'$_POST[codigo]',
				'$_POST[titulo]',
				'$_POST[valor]',
				'$_POST[tipo]'
			)";
			$res = mysqli_query($cnx,$sql); 
			?>
			<div class="alert alert-success"> 
				<strong>Configuración '<? echo $_POST["titulo"]; ?>' creado con &eacute;xito</strong>
                <br />(codigo: <? echo $_POST["codigo"]; ?>)
			</div>
			<?php 
		}else{
			?>
			<div class="alert alert-danger"> 
				<strong>Código '<? echo $_POST["codigo"]; ?>' ya existe</strong>
			</div>
			<?php   
		}
		break;
		
	case "editar":
		_p($_POST);
		
		$SQL_ = "UPDATE configuracion SET "; 
		$SQL_.= "cfg_grupo = '$_POST[grupo]', "; 
		$SQL_.= "cfg_titulo = '$_POST[titulo]', ";
		$SQL_.= "cfg_valor = '$_POST[valor]', ";
		$SQL_.= "cfg_tipo = '$_POST[tipo]' ";
		$SQL_.= "WHERE cfg_codigo = '$_POST[codigo]' ";  
		
		echo $SQL_;
		$res = mysqli_query($cnx,$SQL_);
		?>   
        <div class="alert alert-success"> 
            <strong>Configuración '<? echo $_POST["titulo"]; ?>' editado con &eacute;xito</strong> 
			<br />(codigo: <? echo $_POST["codigo"]; ?>)
        </div>  
		<?php  
		break;
		
	case "eliminar":
		$SQL_ = "delete from configuracion WHERE cfg_codigo = '$_POST[codigo]' ";  
		$res = mysqli_query($cnx,$SQL_); 
		break;
}

construir_boton("navegacion.php","","buscar","Listado de parámetros de configuración",2);
if($_REQUEST["modo"] <> "eliminar"){
	construir_boton("cfg_editar.php","&codigo=".$_POST["codigo"],"editar","Editar este parámetro",2);
}
construir_boton("cfg_nuevo.php","","crear","Crear otro parámetro",2);
?>