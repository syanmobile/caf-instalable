<?php
require("../../inc/conf_dentro.php");

switch($_REQUEST["modo"]){
	case "elegir_pro":  
		$periodo = _fec($_POST["fecha"],9);
		$periodo = str_replace("/","-",$periodo);
		$periodo = substr($periodo,0,7);

		$res = sql_activos_fijos("*"," and acf.acf_producto = '$_REQUEST[producto]' 
			and acf_disponible in (1) order by acf.acf_serie asc");  
		if(mysqli_num_rows($res) > 0){
			?>
			<table class="table table-striped table-bordered tabledrag table-condensed"> 
			<thead>
				<tr>  
					<th width="1"></th> 
					<th width="1">Serie</th>
					<th width="1">Etiqueta</th>  
					<th>Producto</th> 

					<th style="text-align:right;">Valor Compra</th> 

					<th style="text-align:right;">Depreciación Acumulada $</th>
					<th style="text-align:right;">Valor neto en Libros $</th> 
					
					<th width="1">Estado</th>
				</tr>
			</thead>
			<tbody> 
			<?
			while($row = mysqli_fetch_array($res)){ 

				$sql2 = "select * from depreciaciones where dep_periodo >= '$periodo-01' and dep_periodo <= '$periodo-15' ";
				$sql2.= "and dep_acf_id = '$row[acf_id]' "; 
				echo $sql2;
				$res2 = mysqli_query($cnx,$sql2);
				$num2 = mysqli_num_rows($res2);
				unset($row2);
				if($num2 > 0){
					$row2 = mysqli_fetch_array($res2); 
				}
				?>
				<tr> 
					<td><? construir_boton("agregar('".$row["acf_id"]."')","1","crear","Agregar",4); ?></td>  
					 
					<th><?php echo $row["acf_serie"]; ?></th>
					<th><?php if($row["acf_etiqueta"] <> ""){ echo $row["acf_etiqueta"]; } ?></th>  
					<td><?php echo $row["pro_nombre"]; ?></td> 
					  
					<td style="text-align: right"><?php echo _num($row["acf_valor"]); ?></td> 

					<td style="text-align: right"><?php echo _num($row2["dep_acumulada"]); ?></td> 
					<td style="text-align: right"><?php echo _num($row2["dep_neto"]); ?></td> 
					
					<td><?php activo_fijo_estado($row["acf_disponible"]); ?></td> 
				</tr>
				<?  
			}
			?>
			</tbody>
			</table> 
			<? 
		}else{
			?>
			<div class="alert alert-danger">
				<strong>Sin registros creados</strong>
			</div>
			<?php 
		}  
		break; 

	case "detalle":
		?>
        <input type="hidden" name="prod_det" id="prod_det" value="<?php echo $_REQUEST["prod_det"]; ?>" class="campos">
        <input type="hidden" name="depr_det" id="depr_det" value="<?php echo $_REQUEST["depr_det"]; ?>" class="campos">
        <input type="hidden" name="lote_det" id="lote_det" value="<?php echo $_REQUEST["lote_det"]; ?>" class="campos">
        <input type="hidden" name="cant_det" id="cant_det" value="<?php echo $_REQUEST["cant_det"]; ?>" class="campos"> 
        <input type="hidden" name="prec_det" id="prec_det" value="<?php echo $_REQUEST["prec_det"]; ?>" class="campos">

        <?php
		$prod_det = explode(";",$_REQUEST["prod_det"]);
		$depr_det = explode(";",$_REQUEST["depr_det"]);
		$lote_det = explode(";",$_REQUEST["lote_det"]);
		$cant_det = explode(";",$_REQUEST["cant_det"]);
		$prec_det = explode(";",$_REQUEST["prec_det"]);
		for($i = 1;$i < count($prod_det);$i++){
			
			$res = sql_productos("*"," and pro.pro_id = '".$prod_det[$i]."'"); 
			$prod = mysqli_fetch_array($res);
			
			if($cant_det[$i] > 0){
				$lineas++;

				if($cabe == ""){
					?>
					<div style="margin-top: 15px; clear: both;"></div>
					<table class="table table-bordered table-hover table-condensed">
			        <thead>
			        <tr> 
			            <th>Producto</th>  
			            <th>Depreciación</th> 
			            <th>Series</th>
			            <th width="70" style="text-align:center;">Cantidad</th>
			            <th width="100" style="text-align:right;">Valor</th>
			            <th width="100" style="text-align:right;">Total</th>
			            <th width="1"></th>
			        </tr>
			        </thead>
			        <tbody>
					<?
					$cabe = "S";
				}

				// Subtotal
				$subtotal = $prec_det[$i] * $cant_det[$i];
				$total += $subtotal;

				if($prod["pro_tipo"] == "INS"){
					?>
					<tr>
						<td><?php echo $prod["pro_nombre"]; ?><br>
						<b><?php echo $prod["pro_codigo"]; ?></b></td>
						<td colspan="2"></td> 
						<td style="text-align:center;" onclick="javascript:edicion_rapida2(<?php echo $i; ?>,'cant',<?php echo $cant_det[$i]; ?>);"><?php echo _num($cant_det[$i]); ?></td>
						<td style="text-align:right;" onclick="javascript:edicion_rapida2(<?php echo $i; ?>,'prec',<?php echo $prec_det[$i]; ?>);"><?php echo _num($prec_det[$i]); ?></td>
						<td style="text-align:right;"><?php echo _num($subtotal); ?></td>
						<td><?php
						construir_boton("quitar_linea('".$i."');","1","eliminar","Borrar Linea",4);
						?></td> 
					</tr>
					<?
				}else{
					?>
					<tr> 
						<td><?php echo $prod["pro_nombre"]; ?><br>
						<b><?php echo $prod["pro_codigo"]; ?></b></td>
						<td onclick="javascript:editar_dep(<? echo $i; ?>,'<? echo $depr_det[$i]; ?>','<?
						echo $prec_det[$i]; ?>');"><?php
						if($depr_det[$i] <> ""){ 
							$de = explode(",",$depr_det[$i]);
							echo "<b>Dep:</b> ";
							switch($de[0]){
								case "L": echo "Lineal"; break;
								case "A": echo "Acumulada"; break;
							}
							echo " | <b>Meses:</b> ".$de[1]." | <b>V.Residual:</b> $"._num($de[2] * 1)." | <b>Adq:</b> ";
							echo ($de[3] == "P")?"Propio":"Leasing";
						}
						?></td>
						<td onclick="javascript:editar_lotes(<?php echo $i; ?>,<? echo $prod["pro_id"]; ?>,<?
							echo $cant_det[$i]; ?>,'<? echo $lote_det[$i]; ?>');"><?php
						$lotes = explode(",",$lote_det[$i]);
						$cant_series = 0;
						if(count($lotes) > 1){
							for($i2 = 1;$i2 < count($lotes);$i2++){ 
								if($lotes[$i2] <> ""){
									$cant_series++;
									?>
									<span class="label label-default"><?php echo $lotes[$i2]; ?></span>
									<?
								}
							}
						}
						if($cant_series <> $cant_det[$i]){
							?><span class="label label-danger">Items sin series</span><? 
						}
						?></td> 
						<td style="text-align:center;" onclick="javascript:edicion_rapida2(<?php echo $i; ?>,'cant',<?php echo $cant_det[$i]; ?>);"><?php echo _num($cant_det[$i]); ?></td>
						<td style="text-align:right;" onclick="javascript:edicion_rapida2(<?php echo $i; ?>,'prec',<?php echo $prec_det[$i]; ?>);"><?php echo _num($prec_det[$i]); ?></td>
						<td style="text-align:right;"><?php echo _num($subtotal); ?></td>
						<td><?php
						construir_boton("quitar_linea('".$i."');","1","eliminar","Borrar Linea",4);
						?></td> 
					</tr>
					<?
				}
			}
		}
		if($cabe <> ""){
			?> 
	        <tr>
	            <th colspan="5" style="text-align:right;">TOTAL:</th>
	            <th style="text-align:right;"><?php echo _num($total); ?></th>
	            <th></th>
	        </tr>
	        </tbody>
	        </table>
	        <?
	    }
	    ?>
        <input type="hidden" id="lineas" class="campos w10" value="<?php echo $lineas; ?>">
        <input type="hidden" name="total" id="total" class="campos w10" value="<?php echo $total; ?>"> 
		<? 
		break; 

	case "quitar_linea":
		$prod_det = explode(";",$_REQUEST["prod_det"]);
		$depr_det = explode(";",$_REQUEST["depr_det"]);
		$lote_det = explode(";",$_REQUEST["lote_det"]);
		$cant_det = explode(";",$_REQUEST["cant_det"]);
		$prec_det = explode(";",$_REQUEST["prec_det"]);
		for($i = 1;$i < count($prod_det);$i++){
			if($i <> $_REQUEST["elim"]){
				$prod_ .= ";".$prod_det[$i];
				$depr_ .= ";".$depr_det[$i];
				$lote_ .= ";".$lote_det[$i];
				$cant_ .= ";".$cant_det[$i];
				$prec_ .= ";".$prec_det[$i];
			}
		}
		?>
		<script language="javascript">
		document.getElementById("prod_det").value = "<?php echo $prod_; ?>";
		document.getElementById("depr_det").value = "<?php echo $depr_; ?>";
		document.getElementById("lote_det").value = "<?php echo $lote_; ?>";
		document.getElementById("cant_det").value = "<?php echo $cant_; ?>";
		document.getElementById("prec_det").value = "<?php echo $prec_; ?>";
		detalle();
		</script>
		<?php 
		break; 
		
	case "lotes":
		$lotes = explode(",",$_REQUEST["series"]); 
		?> 
		<h4 id="titulo_alerta">Definir Series</h4> 
		
		<div style="overflow: scroll; height: 400px; width: 100%; padding: 5px;">
		<table width="100%" style="margin-bottom:5px;">
        <tr valign="top">
        	<td width="220" style="padding-right: 10px;">
				<table class="table table-bordered table-condensed">
				<thead>
				<tr>
					<th>Información del Producto</th>
				</tr>
				</thead>
				<tbody>
				<tr> 
					<td>
					<?php
					$res = sql_productos("*"," and pro.pro_id = '".$_REQUEST["pro"]."'"); 
					$prod = mysqli_fetch_array($res);
					echo "<b>".$prod["pro_codigo"]."</b><br>".$prod["pro_nombre"];
					?>
					</td>
				</tr> 
				<tr>
					<td>
					<?php
					$res2 = sql_activos_fijos("*","and acf_producto = '$prod[pro_id]' 
					and acf_serie <> '' order by acf_serie desc");
					if(mysqli_num_rows($res2) > 0){
						?>
						<b>Series definidos:</b>
						<div style="padding-left: 10px;">
						<?
						while($row2 = mysqli_fetch_array($res2)){
							?><li><a href="javascript:elegir_ser('<? echo $row2["acf_serie"]; ?>');"><? echo $row2["acf_serie"]; ?></a></li><?
							$series[] = $row2["acf_serie"];
						}
						?>
						</div>
						<?
					}else{
						?>
						<span style="color: red;"><b>No existen series para este producto</b></span>
						<?
					}
					?>
					</td>
				</tr> 
				</tbody>
				</table>
			</td>
			<td>  
				<table class="table table-bordered table-condensed"> 
				<thead>
				<tr> 
					<th colspan="2">Serie definidas</th> 
				</tr>
				</thead>
				<tbody>
				<?php 
				for($i = 1;$i <= $_REQUEST["can"];$i++){ 
					?>
					<tr>
						<th style="text-align: right;" width="60">Serie <? echo $i; ?></th>
						<td><input type="text" name="lote_<? echo $i; ?>" id="lote_<? echo $i; 
						?>" value="<? echo $lotes[$i]; ?>" class="campos w10"></td>  
					</tr>
					<?
				}  
				?>
				</tbody>
				</table>
			</td>
		</tr>
		</table>
		</div>  
        <?
		construir_boton("grabar_serie();","","grabar","Grabar Series",4);
		?>
		<br><br>
		<script type="text/javascript"> 
		function grabar_serie(){
			var valor = "";
			<?
			for($i = 1;$i <= $_REQUEST["can"];$i++){ 
				?>
				valor = valor + "," + document.getElementById("lote_<? echo $i; ?>").value;
				<?
			} 
			?>
			var nuevos_valores = "";
			var info = document.getElementById("lote_det").value;
			var valores = info.split(";");
			for(i = 1;i < valores.length;i++){
				if(i == "<? echo $_REQUEST["id"]; ?>"){
					nuevos_valores = nuevos_valores + ";" + valor;
				}else{
					nuevos_valores = nuevos_valores + ";" + valores[i];
				}
			}
			document.getElementById("lote_det").value = nuevos_valores;
			$('#modGeneral').modal('hide');
			detalle();
		}
		</script>
		<?
		break;

	case "depre": 
		$info = explode(",",$_REQUEST["valor"]);
		?> 
		<h4 id="titulo_alerta">Definir Depreciación</h4> 
		 
		<table width="100%">
		<tr valign="top">
			<td width="220">
				<table class="table table-bordered table-condensed">
				<thead>
				<tr>
					<th colspan="2">Depreciación del Activo</th>
				</tr>
				</thead>
				<tbody>
		        <tr>
		        	<th style="text-align:right;">Precio:</th>
		        	<td>$<? echo _num($_REQUEST["precio"]); ?></td>
				</tr>
				<tr>
					<th width="100" style="text-align:right;">Depreciación:</th>
		    		<td><select name="tipo_depreciacion_" id="tipo_depreciacion_" class="campos">
		    			<option value=""></option>
		                <option value="L" <?php if($info[0] == "L"){
		                    echo "selected";
		                } ?>>Lineal</option>
		                <option value="A" <?php if($info[0] == "A"){
		                    echo "selected";
		                } ?>>Acelerada</option>
		                </select></td> 
		        </tr>
		        <tr>
		        	<th style="text-align:right;">Meses:</th>
		        	<td><input type="text" name="meses_" id="meses_" value="<? echo $info[1]; 
		        	?>" class="campos w10" style="text-align: center;"></td>
				</tr>
				<tr>
					<th style="text-align:right;">Valor Residual:</th>
			        <td><input type="text" name="residual_" id="residual_" value="<? echo $info[2]; 
		        	?>" class="campos w10" style="text-align: center;"></td>
			    </tr>
			    <tr>
			    	<th style="text-align:right;">Adquisición:</th>
		    		<td><select name="adquisicion_" id="adquisicion_" class="campos">
					<option value="P" <? if($info[3] == "P"){ echo "selected"; } ?>>Propio</option>
		            <option value="L" <? if($info[3] == "L"){ echo "selected"; } ?>>Leasing</option>
		            </select></td> 	
		        </tr>
				</tbody>
				</table> 
		        <?
				construir_boton("grabar_depre();","","grabar","Grabar Depreciación",4);
				?>
			</td>
			<td style="padding-left: 15px;">
				<div id="lug_espacio"></div> 
		        <?
				construir_boton("calcular_depre();","","refrescar","Calcular Depreciación",4);
				?>
			</td>
		</tr>
		</table>

		<script type="text/javascript"> 
		function calcular_depre(){
			var fecha = document.getElementById("fecha").value;
			var precio = "<? echo $_REQUEST["precio"]; ?>";
			var meses = document.getElementById("meses_").value * 1;
			var residual = document.getElementById("residual_").value * 1;
			AJAXPOST(url_base+modulo_base+"ing_ajax2.php","modo=calcular_depre&fecha="+fecha+"&precio="+precio+"&meses="+meses+"&residual="+residual,document.getElementById("lug_espacio"));
		}
		function grabar_depre(){
			var valor = document.getElementById("tipo_depreciacion_").value+",";
			valor = valor + document.getElementById("meses_").value;+",";
			valor = valor + document.getElementById("residual_").value;+",";
			valor = valor + document.getElementById("adquisicion_").value;

			var nuevos_valores = "";
			var info = document.getElementById("depr_det").value;
			var valores = info.split(";");
			for(i = 1;i < valores.length;i++){
				if(i == "<? echo $_REQUEST["id"]; ?>"){
					nuevos_valores = nuevos_valores + ";" + valor;
				}else{
					nuevos_valores = nuevos_valores + ";" + valores[i];
				}
			}
			document.getElementById("depr_det").value = nuevos_valores;
			$('#modGeneral').modal('hide');
			detalle();
		}
		calcular_depre();
		</script>
		<?
		break;

	case "previsualizar":
		?>
		<h4 id="titulo_alerta">Previsualizar Depreciación</h4> 
		<div id="lug_espacio"></div>
		<?
		break;

	case "calcular_depre": 
		$periodo = substr($_POST["fecha"],3,20);
		$periodo = str_replace("/","-",$periodo); 
		$ano = substr($periodo,3,4);
		$mes = substr($periodo,0,2); 

		$meses_divisor = str_replace(".","",$_REQUEST["meses"]); 
		$residual = str_replace(".","",$_REQUEST["residual"]);
		$valor_activo = str_replace(".","",$_REQUEST["precio"]);
		$valor = $valor_activo - $residual;

		$valor_mensual = round(($valor / $meses_divisor),2);
		?>
		<table class="table table-striped table-bordered tabledrag table-condensed"> 
		<thead>
			<tr>   
				<th style="text-align:center;">Periodo</th>
				<th style="text-align:center;">Precio</th>
				<th style="text-align:center;">Meses</th>
				<th style="text-align:center;">Valor Residual</th>
				<th style="text-align:center;">Valor Dep</th>
			</tr>
		</thead>
		<tbody> 
			<tr>
				<td style="text-align:center;"><? echo $periodo; ?></td>
				<td style="text-align:center;">$<? echo _num($valor_activo); ?></td>
				<td style="text-align:center;"><? echo _num2($meses_divisor); ?></td>
				<td style="text-align:center;">$<? echo _num($residual); ?></td>
				<td style="text-align:center;">$<? echo _num($valor); ?></td>
			</tr>
		</tbody>
		</table>

		<div style="height: 450px; width: 100%; overflow: scroll;">
			<table class="table table-striped table-bordered tabledrag table-condensed"> 
			<thead>
				<tr>   
					<th style="text-align:center;" width="1">#</th>
					<th style="text-align:center;" width="1">Periodo</th>
					<th style="text-align:right;">Cuota Depreciación $</th>
					<th style="text-align:right;">Depreciación Acumulada $</th>
					<th style="text-align:right;">Valor neto en Libros $</th>
				</tr>
			</thead>
			<tbody> 
			<? 
			for($i3 = 1;$i3 <= $meses_divisor;$i3++){
				$acumulado += $valor_mensual;
				$valor -= $valor_mensual;
				$valor_activo -= $valor_mensual;
				
				$count++;
				$mesx = ($mes < 10)?"0".$mes:$mes;
				?>
				<tr>
					<td style="text-align: center;"><? echo $count; ?></td>
					<td style="text-align: center;"><? echo $ano."-".$mesx."-01"; ?></td>
					<td style="text-align: right;"><? echo _num($valor_mensual); ?></td>
					<td style="text-align: right;"><? echo _num($acumulado); ?></td>
					<td style="text-align: right;"><? echo _num($valor_activo); ?></td>
				</tr>
				<?
				if($mes == 12){
					$mes = 0;
					$ano++;
				} 
				$mes++;
			} 
			?>
			</tbody>
			</table>
		</div>
		<?
		break;
}
?> 