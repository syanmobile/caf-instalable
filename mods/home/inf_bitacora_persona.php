<?php
require("../../inc/conf_dentro.php");

//----------------------------------------------------------------------------------------
$titulo_pagina = "Informe Bitácora Persona";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>   
<table class="table table-striped table-bordered tabledrag table-condensed">
<tr>
    <th width="1">Ficha:</th>
    <td width="1"><select name="idper" id="idper" class="campos">
    <option value=""></option>
    <?php
    $res = sql_personas("*"," and per.per_tipo = 1 and per.per_id <> 1 order by per.per_nombre asc");
    while($row = mysqli_fetch_array($res)){
        ?><option value="<?php echo $row["per_id"]; ?>" <?php
        if($row["per_id"] == $_REQUEST["idper"]){ 
            echo "selected"; 
            $datos = $row;
        } 
        ?>><?php
        echo $row["per_nombre"]; 
        
        if($row["per_elim"] == 1){
            echo " (**ELIMINADO**)";
        }
        ?></option><?php } 
    ?>
    </select></td>  
    <th width="1">Tipo:</th>
    <td width="1"><select name="fil_tipo" id="fil_tipo" class="campos">
    <option value=""></option>
    <option value="INS" <? if($_REQUEST["fil_tipo"] == "INS"){ echo "selected"; } ?>>Insumo</option>
    <option value="ACT" <? if($_REQUEST["fil_tipo"] == "ACT"){ echo "selected"; } ?>>Activo</option>
    </select></td>
    <td><?php
    construir_boton("inf_bitacora_persona.php","","refrescar","Refrescar Informe",2);
    ?></td>
</tr>
</table>  
<?
if($_REQUEST["idper"] <> ""){
    ?>   
    <table class="table  table-bordered"> 
    <thead>
        <tr>
            <th colspan="10">Información Persona</th>
        </tr>
    </thead>
    <tbody>
        <tr> 
            <td width="120"><b>Rut:</b> <?php echo $datos["per_rut"]; ?></td> 
            <td><b>Nombre:</b> <?php echo $datos["per_nombre"]; ?></td> 
            <td><b>Correo:</b> <?php echo $datos["per_correo"]; ?></td>
        </tr>
    </tbody>
    </table>

    <table class="table table-striped table-bordered tabledrag table-condensed"> 
    <thead>
        <tr>  
            <th style="text-align:center;" width="1"></th>
            <th style="text-align:center;" width="1">Fecha</th>
            <th style="text-align:center;" width="1">Folio</th> 
            <th width="120">Lugar</th> 
            <th width="150">Concepto</th>
            <th>Proveedor/Responsable</th>
            <th>Notas</th>
            <th width="1">Tipo</th>
            <th>CodProd</th>   
            <th>Producto</th>
            <th width="1">Entrada</th>
            <th width="1">Salida</th>
            <th width="1">Saldo</th>
            <th width="1" style="text-align:center;">Precio $</th>
            <th width="1" style="text-align:center;">Subtotal $</th> 
            <th width="1" style="text-align:center;">Saldo $</th>
        </tr>
    </thead>
    <tbody>
    <?php
    if($_REQUEST["fil_tipo"] <> ""){
        if($_REQUEST["fil_tipo"] == "INS"){
            $filtros .= " and pro.pro_tipo = 'INS' ";
        }else{
            $filtros .= " and pro.pro_tipo <> 'INS' ";
        }
    }

    $final = 0;
    $saldo = 0; 
    
    $res = sql_movimientos_detalle("*,sum(det_ingreso) as ingreso,sum(det_egreso) as egreso"," 
    and mov.mov_responsable = '$_REQUEST[idper]' $filtros   
    group by mov.mov_id, pro_codigo 
    order by mov.mov_fecha asc, mov.mov_id asc","",""); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){ 
            $movi = $row["ingreso"] - $row["egreso"];
            $subtotal = $movi * $row["det_valor"];
            $saldo2 += $subtotal;
            $saldo += $movi; 
            ?>
            <tr>  
                <td style="text-align:center;"><?php
                if($row["mov_id"] <> ""){
                    construir_boton("visor('aju_pdf.php?id=".$row["mov_id"]."')","1","imprimir","Vista #".$row["mov_id"],4);
                }
                ?></td>  
                <td style="text-align:center;"><?php echo _fec($row["mov_fecha"],5); ?></td>
                <td style="text-align:center;"><?php echo $row["mov_folio"]; ?></td>   
                <td style="text-align:center;"><?php echo $_bodegas[$row["mov_bodega"]]; ?></td>
                <td><?php echo $_conceptos[$row["mov_concepto"]]; ?></td>   
                <td><?php
                if($row["aux_codigo"] <> ""){
                    echo $row["aux_nombre"]; 
                }else{
                    echo $_personas[$row["mov_responsable"]]; 
                }
                ?></td>  
                <td><?php echo $row["mov_glosa"]; ?></td>  
                <td><? echo ($row["pro_tipo"] == "INS")?"Insumo":"Activo"; ?></td>
                <td><?php echo $row["pro_codigo"]; ?></td> 
                <td><?php echo $row["pro_nombre"]; ?></td> 
                
                <td style="text-align:center;"><?php echo _num2($row["ingreso"]); ?></td>  
                <td style="text-align:center;"><?php echo _num2($row["egreso"]); ?></td>  
                <td style="text-align:center;"><?php echo _num2($saldo); ?></td> 
                 
                <td style="text-align:center;"><?php echo _num($row["det_valor"]); ?></td>
                <td style="text-align:center;"><?php echo _num($subtotal); ?></td>    
                <td style="text-align:center;"><?php echo _num($saldo2); ?></td>    
            </tr>
            <? 
        }
    }else{
        ?>
        <tr>
            <td colspan="20">Sin movimientos entre las fechas</td>
        </tr>
        <?
    }
    ?>
    <tr>
        <th style="text-align:right;" colspan="12">Saldo Final:</th>
        <th style="text-align:center;"><?php echo _num($saldo); ?></th>
        <th></th>
        <th></th>
        <th style="text-align:center;"><?php echo _num($saldo2); ?></th>
    </tr>
    </tbody>
    </table> 
    <?
}
?>