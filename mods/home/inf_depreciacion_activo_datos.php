<?php
require("../../inc/conf_dentro.php");
 
if($_REQUEST["activo"] <> ""){

	$res = sql_activos_fijos("*"," and acf.acf_id = '$_REQUEST[activo]' "); 
	$num = mysqli_num_rows($res);
	if($num > 0){
		$datos = mysqli_fetch_array($res);
	} 

	$periodo = explode("-",$datos["acf_mes_ano_inicio_depreciacion"]);
	$mes = $periodo[1] * 1;
	$ano = $periodo[0] * 1;
	$meses = $datos["acf_vida_util"] * 1;
	if($datos["acf_tipo_depreciacion"] == "L"){
		$meses_divisor = $meses;
	}else{
		$meses_divisor = round($meses / 3);
	}

	$valor_activo = round($datos["acf_valor_a_depreciar"] * 1);

	//CORREGIR........... lo debo hacer antes  
	$datos["acf_cuota_depreciacion"] = ($valor_activo / $meses_divisor);
	//Cambiarlo !!
	$valor_mensual = $datos["acf_cuota_depreciacion"] * 1;
	$valor_activo_inicial = round($datos["acf_valor"] * 1); 
	?>
	<table width="100%">
	<tr valign="top">
		<td width="250" style="padding-right: 10px;">
		    <table class="table table-bordered table-condensed"> 
		    <thead>
		    <tr>
		        <th colspan="2">Información Activo</th>
		    </tr>
		    </thead>
		    <tbody>
		    <tr>
		        <td colspan="2"><strong><?php echo _t($datos["acf_codigo"]); ?></strong> - <?php echo _t($datos["pro_nombre"]); ?></td>
		    </tr> 
		    <tr>
		    	<th width="130">Valor de Activo:</th>
		    	<td><? echo _num($datos["acf_valor"]); ?></td>
		    </tr>
		    <tr>
		    	<th>Valor Residual:</th>
		    	<td><? echo _num($datos["acf_valor_residual"]); ?></td>
		    </tr>
		    <tr>
		    	<th>Valor a Depreciar:</th>
		    	<td><? echo _num($datos["acf_valor_a_depreciar"]); ?></td>
		    </tr>
		    <tr>
		    	<th>Cuota Depreciación:</th>
		    	<td><? echo _num($datos["acf_cuota_depreciacion"],3); ?></td>
		    </tr>
		    <tr>
		    	<th>Vida Util:</th>
		    	<td><? echo _num2($datos["acf_vida_util"]); ?> meses</td>
		    </tr>
		    <tr>
		    	<th>Tipo Depreciación:</th>
				<td><?php echo ($datos["acf_tipo_depreciacion"] == "L")?"Lineal":"Acelerada"; ?></td>  
		    </tr> 
		    <tr>  
		    	<th>Fecha Ingreso:</th>
				<td><?php echo _fec($datos["acf_fecha_ingreso"],5); ?></td>
		    </tr>
		    <tr>
		    	<th>Fecha Baja:</th>
				<td><?php echo _fec($datos["acf_fecha_salida"],5); ?></td>
		    </tr>
		    <tr>
				<th>Fecha Inicio Revaloriz:</th>
				<td><?php echo $datos["acf_mes_ano_inicio_revalorizacion"]; ?></td>
		    </tr>
			<tr>
				<th>Período Depreciación:</th>
				<td><?php echo substr($datos["acf_mes_ano_inicio_depreciacion"],0,7); ?> al <?php 
				echo substr($datos["acf_mes_ano_termino_depreciacion"],0,7); ?></td>
			</tr> 
			<tr>
				<th>Tipo Adquisición:</th>
				<td><?php echo ($datos["acf_tipo_adquisicion"] == "P")?"Propio":"En Leasing"; ?></td>
			</tr>  
			<tr>
				<th>Situación Activo:</th>
				<td><?php echo ($datos["acf_totalmente_dep"] == "1")?"Totalmente Depreciado":"Con Saldo de Depreciación"; ?></td>
			</tr>
		    </tbody>
		    </table> 
 		</td>
 		<td>
			<table class="table table-striped table-bordered tabledrag table-condensed" id="tablita_1"> 
			<thead>
				<tr>   
					<th style="text-align:center;" width="1">#</th>
					<th style="text-align:center;" width="1">Mes</th>
					<th style="text-align:center;" width="1">Año</th>
					<th style="text-align:right;">Cuota Depreciación $</th>
					<th style="text-align:right;">Depreciación Acumulada $</th>
					<th style="text-align:right;">Valor Libro $</th>
				</tr>
			</thead>
			<tbody>
			<?php  
			$ano_limite = substr($datos["acf_fecha_salida"],0,4) * 1;
			$mes_limite = substr($datos["acf_fecha_salida"],5,2) * 1;

			for($i = 1;$i <= $meses_divisor;$i++){
				$acumulado += $valor_mensual; 

				if(($datos["acf_fecha_salida"] == "0000-00-00") || ($ano <= $ano_limite && $mes <= $mes_limite)){
					?>
					<tr>
						<td><? echo $i; ?></td>
						<td><? echo _mes($mes); ?></td>
						<td><? echo $ano; ?></td>
						<td style="text-align: right;"><? echo _num($valor_mensual,3); ?></td>
						<td style="text-align: right;"><? echo _num($acumulado,2); ?></td>
						<td style="text-align: right;"><? echo _num($valor_activo_inicial - $acumulado); ?></td>
					</tr>
					<?  
				}
				if($mes == 12){
					$mes = 0;
					$ano++;
				} 
				$mes++;
			}
			?>
			</tbody>
			</table> 
			<script language="javascript"> 
			$("#tablita_1").tableExport({
			    formats: ["xlsx"], //Tipo de archivos a exportar ("xlsx","txt", "csv", "xls")
			    position: 'button',  // Posicion que se muestran los botones puedes ser: (top, bottom)
			    bootstrap: false,//Usar lo estilos de css de bootstrap para los botones (true, false)
			    fileName: "Inf.Depreciacion",    //Nombre del archivo
			    buttonContent: "Exportar a Excel", 
			});
			</script>
		</td>
	</tr>
	</table>
    <? 
}
?>