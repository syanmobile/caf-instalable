<div class="col-md-4 grid-item">
	<div class="panel panel-primary">  
		<div class="panel-heading">
			<b>Ubicaciones con más Activos</b> 
			<div style="float: right;">
				<a href="javascript:ampliar_ubicaciones_con_activos()" style="color:#fff;">(Ampliar)</a>
			</div>
		</div>
		<div class="panel-body">
			<?php
			$total = 0;
			$labeles = "";
			$labeles_largo = "";
			$valores = "";

			$res = mysqli_query($cnx,"select * from resumen_ubicacion order by res_total desc limit 0,10");
			if(mysqli_num_rows($res) > 0){
				?>
				<table class="table table-condensed table-bordered"> 
				<thead>
					<tr>
						<th width="1">Activos</th>
						<th>Ubicación</th>
					</tr>
				</thead>
				<tbody>
				<? 
				while($row = mysqli_fetch_array($res)){ 
					$total += $row["res_total"] * 1;
					?>
					<tr>  
						<th style="text-align: center; padding: 1px 5px;"><? echo _num2($row["res_total"]); ?></th>
						<td style="padding: 1px 5px;"><? echo $_ubicaciones[$row["res_ubicacion"]]; ?></td> 
					</tr>
					<?
					$labeles.= ";".$_ubicaciones[$row["res_ubicacion"]];
					$labeles_largo.= ";".$_ubicaciones[$row["res_ubicacion"]]." (".$row["res_total"].")";
					$valores.= ";".$row["res_total"];
				}
				$otros = $total_de_activos - $total;
				if($otros > 0){
					?>
					<tr>  
						<th style="text-align: center; padding: 1px 5px;"><? echo _num2($otros); ?></th>
						<td style="padding: 1px 5px;">Otras Ubicaciones</td> 
					</tr>
					<?
					$labeles.= ";Otras Ubicaciones";
					$labeles_largo.= ";Sin Otras Ubicaciones (".$otros.")";
					$valores.= ";".$otros;
				}
				?>  
				</tbody>
				</table>
				<? 
			}else{
				?>
				<div class="alert alert-danger">
					<strong>Sin activos registrados</strong>
				</div>
				<?php 
			}
			?>
			<iframe src="mods/home/widget_grafico_donas.php?label=<? echo $labeles; ?>&valor=<? echo $valores; ?>&alto=240" frameborder="0" scrolling="no" style="width: 100%; height: 230px;"></iframe> 
			<script type="text/javascript">
			function ampliar_ubicaciones_con_activos(){
				visor("widget_grafico_donas.php?label=<? echo $labeles_largo; ?>&valor=<? echo $valores; ?>&alto=390&titulo=Activos clasificados por Ubicaciones&imprimir=s");
			}
			</script> 
		</div>
	</div>
</div>