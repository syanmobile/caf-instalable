<?php
require("../../inc/conf_dentro.php");

$res = sql_productos("*","and pro.pro_id = '$_REQUEST[id]'",""); 
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<b>ERROR EN CONSULTA</b><br>No se encontró el Producto con id <strong>"<? echo $_REQUEST["id"]; ?>"</strong>
	</div>
    <?php
	exit();
}
//----------------------------------------------------------------------------------------
$titulo_pagina = "Ficha Insumo: ".$datos["pro_nombre"]; 
construir_breadcrumb($titulo_pagina); 
//----------------------------------------------------------------------------------------
?>  
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li class="active"><a href="#tab_1" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Informaci&oacute;n</a></li> 
</ul>

<div class="tab-content">
	<div class="tab-pane active" id="tab_1">
    	<table width="100%">
        <tr valign="top">
            <td width="200">
                <? 
                $imagen = ($datos["pro_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$datos["pro_imagen"];
                ?><img src="../../<?php echo $imagen; ?>" style="width:200px !important;">
                
                <?php 
                construir_boton("window.print()","","imprimir","Imprimir Ficha",4);
                ?>
            </td>
            <td style="padding-left:10px;">
                <table class="table table-bordered table-condensed"> 
                <tbody>
                    <tr>
                        <th width="120" style="text-align:right;">Nombre:</th>
                        <th><?php echo $datos["pro_nombre"]; ?></th>
                    </tr> 
                    <tr>
                        <th style="text-align:right;">Código:</th>
                        <td><?php echo $datos["pro_codigo"]; ?></td> 
                    </tr>
                    <tr>
                        <th style="text-align:right;">Cód.Barra:</th>
                        <td><?php echo $datos["pro_codigo_barra"]; ?></td>
                    </tr> 
                    <tr>
                        <th style="text-align:right;">Tipo Producto:</th>
                        <td><?php echo $_tipo_activo[$datos["pro_tipo"]]; ?></td>
                    </tr> 
                    <tr>
                        <th style="text-align:right;">Clasificación:</th>
                        <td><?php  
                        $labels = explode(",",_opc("label_clasificacion")); 
                        $valores = explode(" > ",$_categorias_largo[$datos["pro_categoria"]]); 
                        for($ic = 0;$ic < count($valores);$ic++){
                            echo "<b>".$labels[$ic]."</b>: ".$valores[$ic]."<br>";
                        }
                        ?>
                        </td>
                    </tr>  
                    <tr>
                        <th style="text-align:right;">Unidad:</th>
                        <td><?php echo $_unidades[$datos["pro_unidad"]]; ?></td>
                    </tr>
                    <tr>
                        <th style="text-align:right;">Mantenimiento:</th> 
                        <td><?php echo ($datos["pro_mantenimiento"])?"Requiere mantención":"-"; ?></td>
                    </tr> 
                    <tr>
                        <th width="120" style="text-align:right;">Stock Mínimo:</th>
                        <td colspan="2"><?php echo _num($datos["pro_stock_minimo"]); ?></td>
                    </tr>   
                    <?php                
                    $sql3 = "select * from campos_relaciones a 
                    left join campos b on a.nub_campo = b.cam_id 
                    where nub_tipo = 'PRO' and nub_key = '$datos[pro_id]' ";
                    $res3 = mysqli_query($cnx,$sql3);
                    if(mysqli_num_rows($res3) > 0){
                        while($row3 = mysqli_fetch_array($res3)){
                            ?>
                            <tr>
                                <th style="text-align:right"><?php echo $row3["cam_nombre"]; ?>:</th>
                                <td><?php 
                                switch($row3["cam_tipo"]){ 
                                    default:
                                        echo $row3["nub_valor"];
                                        break;                        
                                }
                                ?></td>
                            </tr>
                            <?php
                        }
                    } 
                    ?> 
                    <tr>
                        <th style="text-align:right;">Descripción:</th> 
                        <td><?php echo nl2br($datos["pro_descripcion"]); ?></td>
                    </tr> 
                </tbody>
                </table> 
            </td> 
        </tr>
        </table> 
    </div>
	<div class="tab-pane" id="tab_2">   
    </div>
</div>  

<div style="border-top:1px solid #CCC; padding-top:10px;">
    <table width="100%">
    <tr>
        <td><?php
        construir_boton("ins_listado.php","","izquierda","Volver al listado de Insumos",2); 
        construir_boton("ins_editar.php","&id=".$_REQUEST["id"],"editar","Editar Insumo",2); 
        construir_boton("ins_ver.php","&id=".$_REQUEST["id"],"refrescar","Refrescar Ficha",2); 
        ?></td>
    </tr>
    </table> 
</div>
<br /><br />