<?php
require("../../inc/conf_dentro.php");

$res = sql_personas("*","and per.per_id = '$_REQUEST[id]'","");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<strong>No se encontró el dato</strong>
	</div>
    <?php
	exit();
}
//----------------------------------------------------------------------------------------
$titulo_pagina = "Editar Persona";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function save(){    
	if(document.getElementById("nombre").value == ""){
		alerta_js("Ingresar NOMBRE","Es obligaci&oacute;n ingresar el nombre de la persona");
		return;	
	} 
	carg("per_save.php","");
}
$(document).ready(function() {
	$("#rut").Rut({
		format_on: 'keyup',
	   on_error: function(){
			alert('El rut ingresado es incorrecto');
			document.getElementById("rut").value = "";
	   }
	}); 
	//Checkbox
	$("input[name=checktodos1]").change(function(){
		var valor = $("input[name=checktodos1]:checked").length;
		if(valor){
			 $('input[alt1=1]').each( function() {
				  this.checked = true;
			 });
		}else{
			 $(".asis1").removeAttr("checked");
		} 
	}); 
	$('#archivo').on('change',function(){ 
		$('#multiple_upload_form').ajaxForm({
			target:'#procesando_upload',
			data: { modito: "upload" },
			beforeSubmit:function(e){
				$('.uploading').show();
			},
			success:function(e){
				$('.uploading').hide();
				$("#imagen_ruta").attr("src","upload/<? echo $_SESSION["key_id"]; ?>/"+document.getElementById("imagen").value); 
			},
			error:function(e){
			}
		}).submit(); 
	});
});
</script>

<input type="hidden" name="modo" value="editar" class="campos">
<input type="hidden" name="id" value="<?php echo $_REQUEST["id"]; ?>" class="campos">

<table width="100%">
	<tr valign="top"> 
		<td width="150">  
		    <table class="table table-bordered table-condensed">
		    <thead>
		    	<tr>
		    		<th colspan="2">IMAGEN</th>
		    	</tr>    
			</thead>  
		    <tbody>
		        <tr>
		            <td>
		            <?
					$imagen = ($datos["per_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$datos["per_imagen"];
					?>
					<img src="<?php echo $imagen; ?>" id="imagen_ruta" class="img-thumbnail" style="width:150px !important;">
					<form method="post" name="multiple_upload_form" id="multiple_upload_form" enctype="multipart/form-data" action="<?php echo $url_web; ?>mods/home/per_ajax.php"> 
		                <input type="file" name="archivo" id="archivo" accept='image/*'>
		                
		                <div class="uploading none" style="display:none;"><img src="img/uploading.gif"/></div>
		                <div id="eliminando"></div> 
		                <div id="procesando_upload">
		                    <input type="hidden" name="imagen" value="<?php echo $datos["per_imagen"]; ?>" class="campos" />
		                </div>
		            </form> 
		            </td>
				</tr>
			</tbody>
		    </table>
		</td>
		<td style="padding-left: 10px;">
			<table class="table  table-bordered"> 
		    <thead>
		    	<tr>
		    		<th colspan="2">FICHA PRINCIPAL</th>
		    	</tr>    
			</thead>
			<tbody>
				<tr>
					<th width="120" style="text-align:right;">Rut:</th>
					<td><input type="text" class="campos" id="rut" name="rut" value="<?php
					echo $datos["per_rut"]; ?>"></td>
				</tr>
				<tr>
					<th style="text-align:right;">Nombre:</th>
					<td><input type="text" class="campos w10" id="nombre" name="nombre" value="<?php
					echo $datos["per_nombre"]; ?>"></td>
				</tr>
				<tr>
					<th style="text-align:right;">Correo:</th>
					<td><input type="text" class="campos w10" id="correo" name="correo" value="<?php
					echo $datos["per_correo"]; ?>"></td>
				</tr>   
				<tr>
					<th style="text-align:right;">Estado:</th>
					<td>
						<select name="estado" id="estado" class="campos">
						<option value="0" <?php if($datos["per_elim"] == "0"){ echo "selected"; } ?>>Activado</option>
						<option value="1" <?php if($datos["per_elim"] == "1"){ echo "selected"; } ?>>Desactivado</option> 
						</select>
					</td>
				</tr> 
			</tbody>
			</table> 
			<?php
			construir_boton("","","grabar","Guardar",3); 
			construir_boton("per_listado.php","","eliminar","Cancelar",2);
			?> 
		</td> 
		<td style="padding-left: 10px;"> 
			<table class="table table-bordered">
			<thead>
				<tr>
					<th width="1"><input name="checktodos1" type="checkbox"></th> 
					<th>Marque Lugares donde tiene acceso:</th>
				</tr>				
			</thead> 
			<tbody>
				<?
				$res_pfl = mysqli_query($cnx,"select * from bodegas ORDER BY bod_codigo asc");
				while($row_pfl = mysqli_fetch_array($res_pfl)){
					?>
					<tr>
						<td width="1"><input type="checkbox" value="<?php echo $row_pfl["bod_id"]; ?>" name="bodega[]" alt1="1" class="campos asis1" <?
						if(mysqli_num_rows(mysqli_query($cnx,"select * from personas_bodegas where per_id = '".$datos["per_id"]."' and bod_id = '".$row_pfl["bod_id"]."' ")) > 0){
							echo "checked"; 
						}
						?>></td>
						<td><b><?php echo $row_pfl["bod_codigo"]; ?></b> - <?php echo $row_pfl["bod_nombre"]; ?></td>
					</tr>
					<?php
				}
				?>
			</tbody>
			</table> 
		</td> 
	</tr>
</table> 