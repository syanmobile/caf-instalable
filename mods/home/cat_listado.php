<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Clasificación de Productos";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "cat_listado.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>
<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td><?php
		construir_boton("cat_nuevo.php","","crear","Nueva Clasificación",2);
		construir_boton("cat_importar.php","","importar","Importar Clasificación",2);
		?></td>
    </tr>
</tbody>
</table>

<table class="table table-bordered table-condensed table-info" style="margin-bottom:0px !important;">  
<tbody> 
<tr>
    <td width="100">
    <b>Código:</b><br />
    <input type="text" name="fil_codigo" id="fil_codigo" class="campos w10" value="<? echo $_REQUEST["fil_codigo"]; ?>" placeholder="Usar % para búsquedas"></td> 
    <td>
    <b>Nombre:</b><br />
    <input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? echo $_REQUEST["fil_descripcion"]; ?>" placeholder="Usar % para búsquedas"></td> 
    <td width="1"><?
	construir_boton("cat_listado.php","","buscar","Filtrar");
	?></td> 
</tr>
</tbody>
</table> 

<input type="hidden" name="pagina" id="pagina" value="cat_listado.php" class="campos">
<?php
if($_REQUEST["fil_codigo"] <> ""){
	$filtros .= " and cat.cat_codigo like '".$_REQUEST["fil_codigo"]."' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and cat.cat_nombre like '".$_REQUEST["fil_descripcion"]."' ";
}
$sql_paginador = sql_categorias("*"," $filtros ORDER BY cat_codigo asc","S");
$cant_paginador = 12;
require("../../_paginador.php");
$res = mysqli_query($cnx,$sql_final);
if(mysqli_num_rows($res) > 0){
    ?>
    <table class="table table-striped table-bordered table-condensed" id="tablita"> 
    <thead>
    <tr>
        <th></th>
        <th></th>
        <th>Código</th> 
        <th>Clasificación</th>
        <th>Nombre Largo</th>
    </tr>
    </thead>
    <tbody> 
    <? 
    while($row = mysqli_fetch_array($res)){
        ?>
        <tr>  
            <td style="text-align:center;"><?php
            construir_boton("cat_nuevo.php","&padre=".$row["cat_codigo"],"crear","Crear Hijo");
            ?></td>
            <td style="text-align:center;"><?php
            construir_boton("cat_editar.php","&id=".$row["cat_id"],"editar","Editar");
            ?></td>
            <th>C<?php echo $row["cat_codigo"]; ?></th>  
            <td><?php echo $row["cat_nombre"]; ?></td>
            <td><?php echo $row["cat_nombre_largo"]; ?></td>
        </tr>
        <? 
    }
    ?>
    </tbody>
    </table> 
    <?php
	echo $paginador_dibujo;
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin registros creados</strong>
    </div>
    <?php
}
?>