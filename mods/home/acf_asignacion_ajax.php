<?php
require("../../inc/conf_dentro.php");

$labels = explode(",",_opc("label_clasificacion"));
 
switch($_REQUEST["modo"]){
	case "disponibles": 

		$res = sql_activos_fijos("*"," and acf_disponible <> 0 
		and acf_disponible <> 4 
		and ubi.ubi_id = '$_REQUEST[ubicacion]'  
		and pro.pro_asignable = 1 
		order by acf.acf_producto asc "); 
		if(mysqli_num_rows($res) > 0){
			?>
			<table class="table table-striped table-bordered tabledrag table-condensed" id="tablita"> 
			<thead>
				<tr>    
					<th>Imagen</th> 
					<th>Activo Fijo</th>
					<th>Clasificacion</th>
					<th>Situación</th>
					<th>Agregar</th> 
				</tr>
			</thead>
			<tbody> 
			<?
			while($row = mysqli_fetch_array($res)){  
				?>
				<tr valign="top" id="acf<? echo $row["acf_id"]; ?>">
					<td><? 
					$imagen = ($row["pro_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$row["pro_imagen"];
					?><img src="<?php echo $imagen; ?>" style="width:80px !important;"></td> 
					<td>
						<b><? echo $row["pro_nombre"]; ?></b><br />
						<b>Cod.Activo</b>: <? echo $row["acf_codigo"]; ?><br>
						<b>Cod.Producto</b>: <? echo $row["pro_codigo"]; ?><br>
						<b>Serie</b>: <? echo $row["acf_serie"]; ?><br>
						<b>Etiqueta</b>: <? echo $row["acf_etiqueta"]; ?>
					</td>
                    <td>
                    	<strong>Tipo Activo</strong>: <? 
                    	echo $_tipo_activo[$row["pro_tipo"]]; 
 
						$valores = explode(" > ",$_categorias_largo[$row["pro_categoria"]]); 
						for($ic = 0;$ic < count($valores);$ic++){
							echo "<br><b>".$labels[$ic]."</b>: ".$valores[$ic];
						} 
						?>   
                    </td>
                    <td>
                    	<b>Prox.Control:</b> <? echo _fec($row["acf_fecha_mantencion"],5); ?><br>
                    	<b>Condicion:</b> <?php echo $row["acf_condicion"]; ?><br>
						<b>Estado del activo:</b><br><?php activo_fijo_estado($row["acf_disponible"]); ?>
					</td> 	
                    <td style="padding: 0px;"><?
                    if($row["acf_disponible"] == 1){
                    	?><a href="javascript:elegir('<? echo $row["acf_id"]; ?>');"><img src="img/add.png" width="60"></a><?
                    }
                    ?></td>
				</tr> 
				<?  
			}
			?>
			</tbody>
			</table>
			<script language="javascript">
            $(document).ready(function() {  
				$('#tablita').DataTable({ 
					"columns": [     
						{ "width": "1px" },    
						null ,   
						null,  
						null,
						{ "width": "1px" }   
					  ],
					"paging":   false,
					"info":     false,
					"lengthMenu": [100000],
					"oLanguage": {
						"sSearch": "Busque un activo y presione la flecha para agregar: "
					}
				});
            } );   
            </script> 
			<?
		}else{
			?>
			<div class="alert alert-danger">
				<strong>Sin registros creados</strong>
			</div>
			<?php 
		} 
		break; 
}
?> 