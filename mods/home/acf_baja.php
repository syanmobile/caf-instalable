<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Baja de Activo Fijo";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "acf_baja.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function save(){   
	if(document.getElementById("activo").value != ""){
		if(confirm("Grabar Documento?")){
			carg("acf_save.php","");
		}
	}else{
		alerta_js("Sin activos asociados a la baja");
	}
}
function cargar_productos(){ 
	if(document.getElementById("ubicacion").value == ""){ 
		$("#disponibles").html("");
	}else{
		document.getElementById("activo").value = "";
		document.getElementById("valor").value = "";
		detalle();
		var div = document.getElementById("disponibles"); 
		var a = $(".campos").fieldSerialize();  
		$("#disponibles").html("<h4>Cargando activos fijos, espere un momento...</h4>");
		AJAXPOST("mods/home/acf_baja_ajax.php",a+"&modo=disponibles",div);
	}
}
function detalle(){
	var div = document.getElementById("detalle"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/acf_baja_ajax.php",a+"&modo=detalle",div);
}
function elegir(acf,valor){
	if(document.getElementById("activo").value == ""){
		$("#acf"+acf).hide("fast");
		document.getElementById("activo").value = acf; 
		document.getElementById("valor").value = valor;
		detalle();
	}else{
		alerta_js("Solo puede dar de baja un activo a la vez");
	}
}
function quitar_linea(acf){
	$("#acf"+acf).show("fast"); 
	document.getElementById("activo").value = "";
	document.getElementById("valor").value = "";
	detalle();
}
</script>

<table style="margin-bottom:5px; width:100%">
<tr valign="top"> 
    <td width="400"> 
    	<input type="hidden" name="modo" value="baja" class="campos" />
    	<input type="hidden" name="tipo" id="tipo" value="S" class="campos" />  
    	
		<table class="table table-striped table-bordered table-condensed"> 
        <thead>
            <tr>
                <th colspan="4">Información Movimiento</th>
            </tr>
        </thead>
        <tbody>  
		    <tr>
		        <th style="text-align:right;">Ubicación</th>
		        <td>
		            <select name="ubicacion" id="ubicacion" class="campos buscador w10" onchange="javascript:cargar_productos();">
					<option value=""></option>
					<?php
					$res = sql_ubicaciones("*"," order by bod.bod_nombre asc, ubi.ubi_nombre asc"); 
					if(mysqli_num_rows($res) > 0){
						while($row = mysqli_fetch_array($res)){
							?>
							<option value="<? echo $row["ubi_id"]; ?>" <?
							if($_REQUEST["fil_ubicacion"] == $row["ubi_id"]){ echo "selected"; } 
							?>>U<? echo $row["ubi_codigo"]." - ".$row["ubi_nombre"]; ?></option>
							<?
						}
					}
					?></select>
		        </td>
		    </tr>
            <tr>
                <th width="100" style="text-align:right;">Autoriza:</th> 
				<td>
				<select name="responsable" id="responsable" class="campos buscador">
				<option value=""></option>
				<?php 
				$res = sql_personas("*"," and per.per_tipo = 1 and per.per_elim = 0 ORDER BY per.per_nombre asc ");  
				if(mysqli_num_rows($res) > 0){
					while($row = mysqli_fetch_array($res)){
						?>
						<option value="<? echo $row["per_id"]; ?>"><?php echo $row["per_nombre"]; ?></option>
						<?
					}
				} 
				?>
				</select>
				</td>   
			</tr> 
			<tr>
				<th style="text-align:right;">Concepto:</th>
				<td><?php input_concepto('','S'); ?></td>
			</tr>    
			<tr>
				<th style="text-align:right;">Centro Costo:</th>
				<td><?php input_centro_costo(''); ?></td>
			</tr> 
            <tr>
                <th style="text-align:right;">Notas:</th>
                <td><input type="text" name="glosa" class="campos w10" value="Baja de Activos"></td>
            </tr> 
            <tr>
                <th style="text-align:right;">F.Baja:</th>
                <td><input type="text" name="fecha" id="fecha" class="campos fecha" value="<? 
                echo date("d/m/Y"); ?>">(*) Poner fecha de baja efectiva</td>
            </tr>
            <?php 
			for($i = 1;$i <= 5;$i++){
				if(_opc("extra_mov_".$i) > 0){
					$res = sql_campos("*"," and cam_id = "._opc("extra_mov_".$i));  
					$row = mysqli_fetch_array($res); 
					?> 
					<tr> 
						<th style="text-align: right;"><? echo $row["cam_nombre"]; ?>:</th>
						<td><? 
			            campo_dinamico($row["cam_tipo"],"extra_".$i,$row["cam_opciones"],"",$url_base);
			            ?></td> 
					</tr> 
					<?php
				}
			} 
			?> 
        </tbody>
        </table> 
        <div id="detalle" style="margin: 10px 0px;"></div>
        <input type="hidden" id="activo" name="activo" class="campos"> 
        <input type="hidden" id="valor" name="valor" class="campos"> 
		<?php 
        construir_boton("","","grabar","Guardar Movimiento",3); 
        //construir_boton("acf_baja.php","","refrescar","Refrescar",2);
        ?>
		<br><br>
   		<div id="operacion"></div>	
    </td> 
	<td style="padding-left:10px;" id="disponibles"> 
    </td>
</tr>
</table> 