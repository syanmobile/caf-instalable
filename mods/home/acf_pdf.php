<?php 
ob_start(); 
require("../../inc/conf_dentro.php"); 
  	
$res = sql_activos_fijos("*"," and acf.acf_id = '".$_REQUEST["acf"]."' ");
$datos = mysqli_fetch_array($res); 

$titulo_pdf = "Ficha Activo | ".$datos["acf_codigo"];
require('../../inc/pdf/cabecera.php');

/****************************************************************************************************/

$y = $pdf->GetY() + 5;
$extras = unserialize($_configuracion["ajuste_extras"]);

$campos[] = array("Codigo Activo:",$datos["acf_codigo"]);
$campos[] = array("Producto:",$datos["pro_nombre"]);
$campos[] = array("Codigo Producto:",$datos["pro_codigo"]);
$campos[] = array("Serie:",$datos["acf_serie"]);
$campos[] = array("Tipo Activo:",$_tipo_activo[$datos["pro_tipo"]]);
$campos[] = array("Responsable:",$datos["per_nombre"]);
$campos[] = array("Ubicación:",$_ubicaciones[$datos["acf_ubicacion"]]); 
$campos[] = array("Cuenta Contable:",$_tipo_cuenta_contable[$datos["pro_tipo"]]);
$campos[] = array("Disponibilidad:",activo_fijo_estado($datos["acf_disponible"],1));

foreach($campos as $campo){
	$pdf->SetFont('Arial','b',7);
	$pdf->Cell(25,6,_u8d($campo[0]),0,0,'L'); 
	$pdf->SetFont('Arial','',7);
	$pdf->Cell(88,6,_u8d($campo[1]),0,1,'L');

	$y = $pdf->GetY();
	$pdf->Line(10,$y,125,$y);
} 

$pdf->SetFont('Arial','b',7);
$pdf->Cell(25,6,_u8d("Notas del Activo Fijo:"),0,1,'L'); 
$pdf->SetFont('Arial','',7);

$partes = cortar_palabras(_u8d($datos["acf_notas"]),95);
foreach($partes as $parte){
	$pdf->Cell(118,6,$parte,0,1,'L');
} 		
$y = $pdf->GetY();
$pdf->Line(10,$y,200,$y);

// Imagen del Activo
$imagen = ($datos["pro_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_REQUEST["db"]."/".$datos["pro_imagen"];
$imagen_info = explode(".",$imagen);
$extension = strtoupper($imagen_info[count($imagen_info) - 1]);
$pdf->Image($url_base.$imagen,130,40,70,0,$extension);

// Codigo Activo   
$y = $pdf->GetY();
$pdf->SetFont('Arial','b',7);
$pdf->Cell(25,6,_u8d("Codigo CAF"),0,0,'L'); 
$y+=6;
$pdf->Image($url_base.'pdf/image.php?code=code128&o=1&dpi=72&t=30&r=1&rot=0&text='.$datos["acf_codigo"].'&f1=-1&f2=8&a1=&a2=NULL&a3=',10 + $ancho,$y,32,9,'PNG');

$pdf->Output();
?>