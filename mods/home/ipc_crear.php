<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Abrir Año en Tabla IPC";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "ipc_listado.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>
<script type="text/javascript">
function save(){  
    if(document.getElementById("ano").value == ""){
        alerta_js("Es obligación seleccionar el AÑO");
        return; 
    } 
    carg("ipc_save.php","");
}
</script>

<input type="hidden" name="modo" value="crear" class="campos">
<table class="table table-striped table-bordered table-condensed"> 
<thead>
<tr>
    <th>Año</th>
    <th>IPC A.Anterior</th>
    <?
    for($i = 1;$i <= 12;$i++){
        ?><th width="70" style="text-align: center;"><? echo _mes($i); ?></th><?
    }
    ?>
</tr>
</thead>
<tbody> 
    <tr>  
        <td><select name="ano" id="ano" class="campos w10">
            <option value=""></option>
            <?
            for($i = date("Y");$i >= 2010;$i--){
                ?><option value="<? echo $i; ?>"><? echo $i; ?></option><?
            }
            ?>
        </select></td>
        <td><input type="text" name="anterior" class="campos inpcen w10"></td>
        <?
        for($i = 1;$i <= 12;$i++){
            ?><td><input type="text" name="mes_<? echo $i; ?>" class="campos inpcen w10"></td><?
        }
        ?>
    </tr> 
</tbody>
</table>   
<br> 
<?php 
construir_boton("","","grabar","Guardar Año",3);
construir_boton("ipc_listado.php","","eliminar","Cancelar",2);
?> 