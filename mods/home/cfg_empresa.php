<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Mi Empresa";
construir_breadcrumb($titulo_pagina); 
//----------------------------------------------------------------------------------------
$pagina = "cfg_empresa.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------

$grupos[] = "Empresa";
$grupos[] = "Activos";
$grupos[] = "Mantenimientos";
$grupos[] = "Impresiones";
$grupos[] = "Transacciones";
$grupos[] = "Cuentas Contables";
?>    
<ul class="nav nav-tabs" role="tablist">
    <li class="active"><a href="#tab_1" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Datos de mi Empresa</a></li> 
    <li><a href="#tab_2" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Activos Fijos</a></li>
    <li><a href="#tab_3" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Mantenimientos</a></li>
    <li><a href="#tab_4" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Impresiones</a></li>   
    <li><a href="#tab_5" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Transacciones</a></li> 
    <li><a href="#tab_6" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Cuentas Contables</a></li>  
</ul>

<div class="tab-content" style="min-height: 380px;">
    <?php
    for($i = 0;$i < count($grupos);$i++){ 
        $grupo2++;
        ?>
        <div class="tab-pane <? if($grupo2 == 1){ echo 'active'; } ?>" id="tab_<? echo $grupo2; ?>">
        <table class="table table-striped table-bordered table-condensed"> 
        <?
        $res = sql_configuracion("*"," and cfg.cfg_elim = 0 and cfg.cfg_grupo = '".$grupos[$i]."' ORDER BY cfg.cfg_orden asc ");
        if(mysqli_num_rows($res) > 0){
            while($row = mysqli_fetch_array($res)){
                ?>
                <tr>  
                    <th width="200" style="text-align: right;"><?php echo $row["cfg_titulo"]; ?>:</th> 
                    <td style="text-align: left !important;"><?php
                    switch($row["cfg_tipo"]){
                        case "serial":
                            $valores = unserialize($row["cfg_valor"]);   
                            foreach($valores as $key => $value){
                                ?>
                                <li><b><?php echo $key; ?>:</b> <?php echo $value; ?></li>
                                <?
                            }
                            break;
                        case "imagen":
                            ?><a href="upload/<? echo $_SESSION["key_id"]; ?>/<?php echo $row["cfg_valor"]; ?>" target="_blank"><img src="upload/<? 
                            echo $_SESSION["key_id"]; ?>/<?php echo $row["cfg_valor"]; ?>" width="100"></a><?php
                            break;
                        default:
                            echo $row["cfg_valor"];
                            break;                        
                    }
                    ?></td> 
                </tr>
                <?
            }
        }
        ?>
        </table>
        </div>
        <?
    }
    ?>
</div>  
<div style="border-top: 1px solid #ccc; padding-top: 10px;"><?
construir_boton("cfg_configuracion.php","","editar","Editar Información",2);
?></div>