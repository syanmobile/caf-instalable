<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Activos Fijos con Responsables";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?> 
<script type="text/javascript">
function excel(){
    var a = $(".campos").fieldSerialize();
    window.open("mods/home/inf_activos_con_responsables_datos_xls.php?"+a);
} 
</script> 

<table class="table table-bordered table-condensed table-info">
<tr>  
    <td width="100">
    <b>Cod.Activo:</b><br />
    <input type="text" name="fil_codigo2" id="fil_codigo2" class="campos w10" value="<? echo $_REQUEST["fil_codigo2"]; ?>" placeholder="%"></td> 
    <td width="100">
    <b>Cod.Producto:</b><br />
    <input type="text" name="fil_codigo" id="fil_codigo" placeholder="%" class="campos w10" value="<? echo $_REQUEST["fil_codigo"]; ?>"></td> 
    <td>
    <b>Producto:</b><br />
    <input type="text" name="fil_descripcion" id="fil_descripcion" placeholder="%" class="campos w10" value="<? echo $_REQUEST["fil_descripcion"]; ?>"></td>  
    <td>
    <b>Clasificación:</b><br>
    <select name="fil_categoria" id="fil_categoria" class="campos buscador w10">
    <option value=""></option>
    <?php
    $res = sql_categorias("*"," order by cat.cat_nombre asc"); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["cat_id"]; ?>" <?
            if($_REQUEST["fil_categoria"] == $row["cat_id"]){ echo "selected"; } 
            ?>>C<? echo $row["cat_codigo"]." - ".$row["cat_nombre_largo"]; ?></option>
            <?
        }
    }
    ?></select></td>
    <td>
    <b>Ubicación:</b><br>
    <select name="fil_ubicacion" id="fil_ubicacion" class="campos buscador w10">
    <option value=""></option>
    <?php
    $res = sql_ubicaciones("*"," order by bod.bod_nombre asc, ubi.ubi_nombre asc"); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["ubi_id"]; ?>" <?
            if($_REQUEST["fil_ubicacion"] == $row["ubi_id"]){ echo "selected"; } 
            ?>>U<? echo $row["ubi_codigo"]." - ".$row["ubi_nombre"]; ?></option>
            <?
        }
    }
    ?></select></td>
    <td>
    <b>Responsable:</b><br>  
    <select name="fil_responsable" id="fil_responsable" class="campos buscador">
    <option value=""></option> 
    <?php
    $res = sql_personas("*","   ORDER BY per.per_nombre asc ");  
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["per_id"]; ?>" <?
            if($row["per_id"] == $_REQUEST["fil_responsable"]){ echo "selected"; } 
            ?>><?php echo $row["per_nombre"]; ?></option>
            <?
        }
    }
    ?> 
    </select></td> 

    <td width="90"><?
    construir_boton("inf_activos_con_responsables.php","","buscar","Filtrar");
    construir_boton("carg3('inf_activos_con_responsables.php','')","1","limpiar","Limpiar",4);
    construir_boton("excel();","1","importar","Descargar",4); 
    ?></td> 
</tr>
</table>

<?
require("inf_activos_con_responsables_datos.php");
?>