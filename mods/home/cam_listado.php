<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Campos Personalizados";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "cam_listado.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>
<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td><?php
		construir_boton("cam_nuevo.php","","crear","Nuevo Campo Personalizado",2);
		?></td>
    </tr>
</tbody>
</table>
<?php
$res = sql_campos("*","   ORDER BY cam_nombre asc","","");
if(mysqli_num_rows($res) > 0){
    ?>
    <table class="table table-striped table-bordered table-condensed"> 
    <thead>
    <tr>
        <th width="1" align="center">&nbsp;</th> 
        <th width="1">Tipo</th>
        <th>Campo Personalizado</th>
        <th>Opciones</th>
        <th width="1">Producto</th>
        <th width="1">Activo</th>
    </tr>
    </thead>
    <tbody> 
    <?php
    while($row = mysqli_fetch_array($res)){
        ?>
        <tr fila="<?php echo $row["cam_id"] ;?>" id="<?php echo $row["cam_id"] ;?>">  
            <td style="text-align:center;"><?php
			construir_boton("cam_editar.php","&id=".$row["cam_id"],"editar","Editar");
			?></td>
            <th style="text-align: center"><?php echo strtoupper($row["cam_tipo"]); ?></th>
            <td><?php echo $row["cam_nombre"]; ?></td>
            <td><?php echo $row["cam_opciones"]; ?></td>
            <td style="text-align:center"><?php echo ($row["cam_producto"] == "1")?"SI":"-"; ?></td>
            <td style="text-align:center"><?php echo ($row["cam_activo"] == "1")?"SI":"-"; ?></td>
        </tr>
        <? 
    }
    ?> 
    </tbody>
    </table>   
    <?php
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin registros creados</strong>
    </div>
    <?php
}
?>