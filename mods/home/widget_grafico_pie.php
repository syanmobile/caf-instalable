<?php
require("../../inc/conf_dentro.php"); 

$labels = explode(";",$_REQUEST["label"]);
$valores = explode(";",$_REQUEST["valor"]);
?>
<script src="../../js/jquery-1.11.1.min.js"></script>
<script src="../../js/graph/highcharts.js"></script>
<script src="../../js/graph/modules/exporting.js"></script>
<script src="../../js/graph/modules/export-data.js"></script>
<script src="../../js/graph/modules/accessibility.js"></script>

<script>
$(function () {
	$('#chartContainer').highcharts({
		colors: ["#C0392B","#9B59B6","#2980B9","#1ABC9C","#27AE60","#F1C40F","#E67E22","#95A5A6","#34495E", "#E74C3C","#8E44AD","#3498DB","#16A085","#2ECC71", "#F39C12","#D35400","#7F8C8D","#566573"],
	    credits: {
	      enabled: false
	    },
	    exporting: {
	        enabled: false
	    },
	    chart: {
	        plotBackgroundColor: null,
	        plotBorderWidth: null,
	        plotShadow: false,
	        type: 'pie'
	    },
	    title: false,
	    tooltip: {
	        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	    },
	    accessibility: {
	        point: {
	            valueSuffix: '%'
	        }
	    },
	    plotOptions: {
	        pie: {
	            allowPointSelect: true,
	            cursor: 'pointer',
	            dataLabels: {
	                enabled: false
	            },
	            showInLegend: false
	        }
	    },
	    series: [{
	        name: 'Activos',
	        colorByPoint: true,
	        data: [<?
			for($i = 1;$i < count($valores);$i++){
				if($i > 1){ echo ", "; }
				?>{
					name: '<? echo $labels[$i]; ?>',
		            y: <? echo $valores[$i]; ?>
		        }<?
			} 
			?>]
	    }]
	});
});
</script>
<body style="padding: 0px; margin: 0px;">
<div id="chartContainer" style="height: <? if($_REQUEST["alto"] <> ""){ echo "200px"; }else{ echo $_REQUEST["alto"]; } 
?>; width: <? if($_REQUEST["ancho"] == ""){ echo "100%"; }else{ echo $_REQUEST["ancho"]; } ?>;"></div>
</body>