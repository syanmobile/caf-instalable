<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Lugares";
construir_breadcrumb($titulo_pagina); 
//----------------------------------------------------------------------------------------
$pagina = "bod_listado.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>
<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td><?php
		construir_boton("bod_nuevo.php","","crear","Crear lugar",2);
		construir_boton("bod_importar.php","","importar","Importar Lugares",2); 
		//construir_boton("bod_listado.php","","refrescar","Refrescar"); 
		?></td>
    </tr>
</tbody>
</table>  
  
<? 
$res = sql_bodegas("*"," ORDER BY bod_nombre asc"); 
if(mysqli_num_rows($res) > 0){ 
    ?> 
    <table class="table table-striped table-hover table-bordered table-condensed" id="tablita"> 
    <thead>
    <tr>
        <th width="1" align="center">&nbsp;</th>
        <th width="1" align="center">Código</th>
        <th>Lugar</th> 
    </tr>
    </thead>
    <tbody> 
    <?
    while($row = mysqli_fetch_array($res)){
        ?>
        <tr fila="<?php echo $row["bod_id"] ;?>" id="<?php echo $row["bod_id"] ;?>">  
            <td style="text-align:center;"><?php
			construir_boton("bod_editar.php","&id=".$row["bod_id"],"editar","Editar");
			?></td>
            <th style="text-align: center"><?php echo $row["bod_codigo"]; ?></th>
            <td><?php echo $row["bod_nombre"]; ?></td>  
        </tr>
        <? 
    }
    ?> 
    </tbody>
    </table>   
    
    <script language="javascript">
    $(document).ready(function() { 
        $('#tablita').DataTable({
          "columns": [   
            { "width": "1px" }, 
            { "width": "1px" },  
            null   
          ],
          lengthMenu: [20000000],
          "bPaginate": false,
          "bLengthChange": false
        });
    } );   
    </script>   
    <?php 
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin registros creados</strong>
    </div>
    <?php
}
?>