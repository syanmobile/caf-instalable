<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Crear Lugar";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
$(document).ready(function() { 
	$("input[name=checktodos1]").change(function(){
		var valor = $("input[name=checktodos1]:checked").length;
		if(valor){
			 $('input[alt1=1]').each( function() {
				  this.checked = true;
			 });
		}else{
			 $(".asis1").removeAttr("checked");
		} 
	});
});
function save(){ 
	if(document.getElementById("codigo").value == ""){
		alerta_js("Es obligación ingresar el código");
		return;	
	}
	if(document.getElementById("nombre").value == ""){
		alerta_js("Es obligación ingresar el nombre");
		return;	
	}
	carg("bod_save.php","");
}
</script> 

<input type="hidden" name="modo" value="crear" class="campos"> 

<table width="100%">
	<tr valign="top">
		<td width="400">
			<table class="table  table-bordered"> 
			<tbody>
				<tr>
					<th width="120" style="text-align:right;">Código:</th>
					<td><input type="text" class="campos" id="codigo" name="codigo"></td>
				</tr>
				<tr>
					<th style="text-align:right;">Nombre:</th>
					<td><input type="text" class="campos" id="nombre" name="nombre"></td>
				</tr> 
			</tbody>
			</table>
		</td>  
		<td style="padding-left: 10px;"> 
			<table class="table table-bordered"> 
			<tbody>
				<tr>
					<th colspan="3">Marque quien tiene acceso:</th>
				</tr>
				<?
				$res_pfl = sql_personas("*"," ORDER BY per.per_nombre asc");
				if(mysqli_num_rows($res_pfl) > 0){
					while($row_pfl = mysqli_fetch_array($res_pfl)){
						?>
						<tr>
							<td width="1"><input type="checkbox" value="<?php echo $row_pfl["per_id"]; ?>" name="persona[]" class="campos"></td>
							<th width="1"><?php echo $row_pfl["per_rut"]; ?></th>
							<td><?php echo $row_pfl["per_nombre"]; ?></td>
						</tr>
						<?php
					}
				}
				?>
			</tbody>
			</table> 
		</td>
	</tr>
</table> 
<?php
construir_boton("","","grabar","Guardar",3);
construir_boton("bod_listado.php","","eliminar","Cancelar",2);
?> 