<?php
require("../../inc/conf_dentro.php");

$res = sql_auxiliares("*","and aux.aux_codigo = '$_REQUEST[codigo]'","");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<strong>No se encontró el dato</strong>
	</div>
    <?php
	exit();
}
//----------------------------------------------------------------------------------------
$titulo_pagina = "Ficha Auxiliar: ".$datos["aux_nombre"]; 
construir_breadcrumb($titulo_pagina); 
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
$(document).ready(function(){
	listDir();
});
function cambiar(){
	carg("aux_ver.php","");
}

function listDir(){ 
	var div = document.getElementById("dire_ajax");
	AJAXPOST("mods/home/aux_ajax.php","modo=listado&codigo="+document.getElementById("codigo").value,div);
}
function suprDir(dir){
	var auxi = document.getElementById("codigo").value;
	var div = document.getElementById("tab_2");
	AJAXPOST("mods/home/aux_ajax.php","&modo=dir_elim&id_dir="+dir+"&aux="+auxi,div,false,listDir);
}
function saveDir(){   
	if(document.getElementById("d_nom").value == ""){
		alerta_js("Es obligación ingresar Nombre");
		return;	
	} 
	if(document.getElementById("d_dir").value == ""){
		alerta_js("Es obligación ingresar Dirección");
		return;	
	}
	var div = document.getElementById("dire_ajax");
	var a = $(".camposd").fieldSerialize();
	AJAXPOST("mods/home/aux_ajax.php",a+"&modo=agregar",div,false,listDir);
} 


</script>

<table class="table  table-bordered"> 
<tr>
    <th width="1">Ficha:</th>
    <td><select name="codigo" id="codigo" class="campos camposd buscador" onchange="javascript:cambiar();">
    <?php
    $res = sql_auxiliares("*","   order by aux.aux_codigo asc");
    while($row = mysqli_fetch_array($res)){
        ?><option value="<?php echo $row["aux_codigo"]; ?>" <?php
        if($row["aux_codigo"] == $_REQUEST["codigo"]){ echo "selected"; } ?>><?php
        echo $row["aux_codigo"]." - ".$row["aux_nombre"];  
        ?></option><?php 
	} 
    ?>
    </select></td>
</tr>
</table>
  
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li class="active"><a href="#tab_1" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Informaci&oacute;n</a></li> 
    <li><a href="#tab_2" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Direcciones</a></li> 
</ul>

<div class="tab-content">
	<div class="tab-pane active" id="tab_1">
    	<table width="100%">
        <tr valign="top">
            <td width="300">
            	<?
				$imagen = ($datos["aux_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$datos["aux_imagen"];
				?>
            	<img src="<?php echo $imagen; ?>" class="img-thumbnail" style="width:200px !important;">
            </td>
            <td style="padding-left:10px;">
                <table class="table table-bordered"> 
                <tbody>
                    <tr>
                        <th width="120" style="text-align:right;">Rut:</th>
                        <td><?php echo $datos["aux_rut"]; ?></td>
                    </tr>
                    <tr>
                        <th style="text-align:right;">Nombre Auxiliar:</th>
                        <td><?php echo $datos["aux_nombre"]; ?></td>
                    </tr>
                    <tr>
                        <th width="1" style="text-align:right;">Dirección:</th> 
                        <td><?php echo $datos["aux_direccion"]; ?></td>
                    </tr> 
                    <tr>
                        <th style="text-align:right;">Comuna:</th>
                        <td><?php echo $datos["aux_comuna"]; ?></td>
                    </tr>  
                    <tr>
                        <th style="text-align:right;">Telefono:</th>
                        <td><?php echo $datos["aux_telefono"]; ?></td>
                    </tr> 
                    <tr>
                        <th style="text-align:right;">Estado:</th>
                        <td><?php echo ($datos["aux_estado"] == "S")?"SI":"NO"; ?></td>
                    </tr>
                </tbody>
                </table>
			</td>
    		<td width="250" style="padding-left:10px;">
        		<table class="table table-bordered table-condensed">  
                <tbody>  
                <?php
				$tipo_actualmente = unserialize($datos["aux_tipo"]);
                $tipos = explode(",",$_configuracion["tipo_auxiliares"]);
                for($i = 0;$i < count($tipos);$i++){
                    ?>
                    <tr>
                        <th width="100" style="text-align:right;"><?php echo $tipos[$i]; ?>:</th>
                        <td><?php if($tipo_actualmente[$tipos[$i]]){ echo "SI"; } ?></td>
                    </tr>
                    <?
                }
                ?>
                </tbody>
                </table>
            </td>
		</tr>
        </table>
    </div> 
	<div class="tab-pane" id="tab_2">
   		<div id="dire_ajax"></div>
    </div>
</div>  

<div style="border-top:1px solid #CCC; padding-top:10px;">
    <table width="100%">
    <tr>
        <td><?php
        construir_boton("aux_listado.php","","izquierda","Volver al listado de Auxiliares",2); 
        construir_boton("aux_editar.php","&codigo=".$_REQUEST["codigo"],"editar","Editar Auxiliar",2);
        construir_boton("aux_ver.php","&codigo=".$_REQUEST["codigo"],"refrescar","Refrescar Ficha",2);
        ?></td>
    </tr>
    </table> 
</div>
<br /><br />