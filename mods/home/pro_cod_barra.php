<?php 
ob_start(); 
require("../../inc/conf_dentro.php"); 

// para utilizar la librería
define('FPDF_FONTPATH','font/');
require_once('../../pdf/fpdf.php');

// Parametros pdf
$info = unserialize($_configuracion["pdf_producto"]);
$dato1 = explode(",",$info["Orientacion/Unidad"]);
$dato2 = explode(",",$info["Ancho/Alto"]);
$dato3 = explode(",",$info["Lugar_Codebar"]);
$dato4 = explode(",",$info["Campos"]);
$dato5 = explode(",",$info["Lugar_Campos"]);
$dato6 = explode(",",$info["Lugar_Logo"]);

$pdf=new FPDF($dato1[0],$dato1[1],array($dato2[0],$dato2[1]));
$pdf->AliasNbPages();

switch($_REQUEST["modalidad"]){
	case "":
		$res = sql_productos("*"," where pro.pro_codigo = '".$_REQUEST["codigo"]."' ");
		$row = mysqli_fetch_array($res);
		
		$pdf->AddPage();

		//$pdf->SetFont('Arial','b',20);
		$pdf->SetFont($dato5[0],$dato5[1],$dato5[2]);
		foreach($dato4 as $texto){
			
			// Esto es un margen opcional
			if($dato5[7] > 0){ 
				$pdf->Cell($dato5[7],$dato5[4],"",0,0,'L');
			}
			
			//$pdf->Cell(0,7,$row[$texto],0,1,'C');
			$textos = explode("|",$texto);
			
			$todoeltexto = $row[$textos[0]].$textos[1];
			if(strlen($todoeltexto) <= 30){
				$pdf->Cell($dato5[3],$dato5[4],utf8_decode($todoeltexto),$dato5[5],1,$dato5[6]);
			}else{
				$parte1 = substr($todoeltexto,0,30);
				$pdf->Cell($dato5[3],$dato5[4],utf8_decode($parte1),$dato5[5],1,$dato5[6]);
				
				// Esto es un margen opcional
				if($dato5[7] > 0){ 
					$pdf->Cell($dato5[7],$dato5[4],"",0,0,'L');
				}
				$parte2 = substr($todoeltexto,30,100);
				$pdf->Cell($dato5[3],$dato5[4],utf8_decode($parte2),$dato5[5],1,$dato5[6]);
			}
		}
		
		$row["pro_codigo_barra"] = str_replace(" ","",$row["pro_codigo_barra"]);
		$row["pro_codigo_barra"] = str_replace("	","",$row["pro_codigo_barra"]);
		if($row["pro_codigo_barra"] <> ""){
			$pdf->Image($url_base.'pdf/image.php?text='.$row["pro_codigo_barra"].'&'.$info["Codebar"],$dato3[0],$dato3[1],$dato3[2],0,'PNG');
		}
		if($_REQUEST["logo"] == ""){
			if($info["Lugar_Logo"] <> ""){
				$pdf->Image($url_base.'img/headers/'._opc("logo_empresa"),$dato6[0],$dato6[1],$dato6[2],0,'PNG');
			}
		}
		break;
		
	case "varios_prod":
		if($_REQUEST["fil_codigo"] <> ""){
			$filtros .= " and pro.pro_codigo like '".$_REQUEST["fil_codigo"]."' ";
		}
		if($_REQUEST["fil_descripcion"] <> ""){
			$filtros .= " and pro.pro_nombre like '".$_REQUEST["fil_descripcion"]."' ";
		}
		if($_REQUEST["fil_barra"] <> ""){
			$filtros .= " and pro.pro_codigo_barra like '".$_REQUEST["fil_barra"]."' ";
		}
		for($i = 1;$i <= 20;$i++){
			if($_REQUEST["fil_extra_".$i] <> ""){
				$filtros .= " and pro.pro_extra_".$i." like '".$_REQUEST["fil_extra_".$i]."' ";
			}
		}
		if($_REQUEST["fil_unidad"] <> ""){
			$filtros .= " and pro.pro_unidad = '".$_REQUEST["fil_unidad"]."' ";
		}
		if($_REQUEST["fil_grupo"] <> ""){
			$filtros .= " and pro.pro_grupo = '".$_REQUEST["fil_grupo"]."' ";
		}
		if($_REQUEST["fil_subgrupo"] <> ""){
			$filtros .= " and pro.pro_subgrupo = '".$_REQUEST["fil_subgrupo"]."' ";
		}
		if($_REQUEST["fil_categoria"] <> ""){
			$filtros .= " and pro.pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
		}
		if($_REQUEST["fil_tipo"] <> ""){
			$filtros .= " and pro.pro_tipo = '".$_REQUEST["fil_tipo"]."' ";
		}

		$res = sql_productos("*","   $filtros ORDER BY pro.pro_nombre asc"); 
		if(mysqli_num_rows($res) > 0){
			while($row = mysqli_fetch_array($res)){
		
				$pdf->AddPage();

				//$pdf->SetFont('Arial','b',20);
				$pdf->SetFont($dato5[0],$dato5[1],$dato5[2]);
				foreach($dato4 as $texto){

					// Esto es un margen opcional
					if($dato5[7] > 0){ 
						$pdf->Cell($dato5[7],$dato5[4],"",0,0,'L');
					}

					//$pdf->Cell(0,7,$row[$texto],0,1,'C');
					$textos = explode("|",$texto);
					$pdf->Cell($dato5[3],$dato5[4],$row[$textos[0]].utf8_decode($textos[1]),$dato5[5],1,$dato5[6]);
				}

				if(trim($row["pro_codigo_barra"]) <> ""){
					$pdf->Image($url_base.'pdf/image.php?text='.$row["pro_codigo_barra"].'&'.$info["Codebar"],$dato3[0],$dato3[1],$dato3[2],0,'PNG');
				}

				if($_REQUEST["logo"] == ""){
					if($info["Lugar_Logo"] <> ""){
						$pdf->Image($url_base.'img/headers/'._opc("logo_empresa"),$dato6[0],$dato6[1],$dato6[2],0,'PNG');
					}
				}
			}
		}
		break;
		
	case "especificos":
		$productos = $_REQUEST["p"];
		$cantidades = $_REQUEST["c"];
		for($i = 0;$i < count($productos);$i++){
			$producto = $productos[$i];
			$cantidad = $cantidades[$i] * 1;
			if($cantidad == 0){
				$cantidad = 1;
			}

			$res = sql_productos("*"," where pro.pro_codigo = '$producto' "); 
			if(mysqli_num_rows($res) > 0){
				while($row = mysqli_fetch_array($res)){

					for($i2 = 0;$i2 < $cantidad;$i2++){
						$pdf->AddPage();

						//$pdf->SetFont('Arial','b',20);
						$pdf->SetFont($dato5[0],$dato5[1],$dato5[2]);
						foreach($dato4 as $texto){

							// Esto es un margen opcional
							if($dato5[7] > 0){ 
								$pdf->Cell($dato5[7],$dato5[4],"",0,0,'L');
							}

							//$pdf->Cell(0,7,$row[$texto],0,1,'C');
							$textos = explode("|",$texto);
							$pdf->Cell($dato5[3],$dato5[4],$row[$textos[0]].utf8_decode($textos[1]),$dato5[5],1,$dato5[6]);
						}
						if($_REQUEST["adicional"] <> ""){
							// Esto es un margen opcional
							if($dato5[7] > 0){ 
								$pdf->Cell($dato5[7],$dato5[4],"",0,0,'L');
							}
							$pdf->Cell($dato5[3],$dato5[4],utf8_decode($_REQUEST["adicional"]),$dato5[5],1,$dato5[6]);
						}

						if(trim($row["pro_codigo_barra"]) <> ""){
							$pdf->Image($url_base.'pdf/image.php?text='.$row["pro_codigo_barra"].'&'.$info["Codebar"],$dato3[0],$dato3[1],$dato3[2],0,'PNG');
						}

						if($_REQUEST["logo"] == ""){
							if($info["Lugar_Logo"] <> ""){
								$pdf->Image($url_base.'img/headers/'._opc("logo_empresa"),$dato6[0],$dato6[1],$dato6[2],0,'PNG');
							}
						}
					}
					
				}
			}
			
		}
		break;
}
$pdf->Output();
?>