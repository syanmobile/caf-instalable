<?php 
ob_start();
require("../../inc/conf_dentro.php");

$res3 = sql_acf_mantenimientos("*","and mnt.man_id = '$_REQUEST[id]'");
if(mysqli_num_rows($res3) > 0){
	$row = mysqli_fetch_array($res3);
}
$titulo_pdf = "Mantenimiento #".$row["man_id"];
require('../../inc/pdf/cabecera.php');

/****************************************************************************************************/
  
$pdf->SetFont('Arial','b',7);
$pdf->Cell(27,7,_u8d("Cod.Activo"),1,0,'R');
$pdf->SetFont('Arial','',7);
$pdf->Cell(0,7,$row["acf_codigo"],1,1,'L');

$pdf->SetFont('Arial','b',7);
$pdf->Cell(27,7,_u8d("Serie"),1,0,'R');
$pdf->SetFont('Arial','',7);
$pdf->Cell(0,7,$row["acf_serie"],1,1,'L');

$pdf->SetFont('Arial','b',7);
$pdf->Cell(27,7,_u8d("Producto"),1,0,'R');
$pdf->SetFont('Arial','',7);
$pdf->Cell(0,7,_u8d($row["pro_nombre"]." (".$row["pro_codigo"].")"),1,1,'L');

/****************************************************************************************************/  
	  
$pdf->Ln(3);

$pdf->SetFont('Arial','b',7);
$pdf->Cell(27,7,_u8d("Inicio"),1,0,'R');
$pdf->SetFont('Arial','',7);
$pdf->Cell(0,7,$row["man_inicio"],1,1,'L');

$pdf->SetFont('Arial','b',7);
$pdf->Cell(27,7,_u8d("Término"),1,0,'R');
$pdf->SetFont('Arial','',7);
$pdf->Cell(0,7,$row["man_termino"],1,1,'L');

$pdf->SetFont('Arial','b',7);
$pdf->Cell(27,7,_u8d("Límite"),1,0,'R');
$pdf->SetFont('Arial','',7);
$pdf->Cell(0,7,$row["man_limite"],1,1,'L');

$pdf->SetFont('Arial','b',7);
$pdf->Cell(27,7,_u8d("Responsable"),1,0,'R');
$pdf->SetFont('Arial','',7);
$pdf->Cell(0,7,_u8d($_personas[$row["man_responsable"]]),1,1,'L');

$pdf->SetFont('Arial','b',7);
$pdf->Cell(27,7,_u8d("Titulo"),1,0,'R');
$pdf->SetFont('Arial','',7);
$pdf->Cell(0,7,_u8d($row["man_titulo"]),1,1,'L');

$pdf->SetFont('Arial','b',7);
$pdf->Cell(27,7,_u8d("Descripción"),1,0,'R');
$pdf->SetFont('Arial','',7);
$pdf->Cell(0,7,_u8d($row["man_descripcion"]),1,1,'L');

$pdf->SetFont('Arial','b',7);
$pdf->Cell(27,7,_u8d("Notas"),1,0,'R');
$pdf->SetFont('Arial','',7);
$pdf->Cell(0,7,_u8d($row["man_notas"]),1,1,'L');

$extras = unserialize($_configuracion["mantenimientos_extras"]);
for($i = 1;$i < count($extras);$i++){
	if($extras["Extra_".$i] <> ""){ 
		$pdf->SetFont('Arial','b',7);
		$pdf->Cell(27,7,_u8d($extras["Extra_".$i]),1,0,'R');
		$pdf->SetFont('Arial','',7);
		$pdf->Cell(0,7,$row["man_extra_".$i],1,1,'L');
	}
} 

$pdf->SetFont('Arial','b',7);
$pdf->Cell(27,7,_u8d("COSTO"),1,0,'R');
$pdf->SetFont('Arial','',7);
$pdf->Cell(0,7,"$ "._num($row["man_costo"]),1,1,'L');

$estado = ($row["man_estado"] == "PND")?"PENDIENTE":"TERMINADO";
$pdf->SetFont('Arial','b',7);
$pdf->Cell(27,7,_u8d("Estado"),1,0,'R');
$pdf->SetFont('Arial','',7);
$pdf->Cell(0,7,$estado,1,1,'L');

if($row["man_imagen"] <> ""){
	$pdf->Ln(3);
	$pdf->SetFont('Arial','b',9);
	$pdf->Cell(0,7,_u8d("IMAGEN EVIDENCIA:"),0,1,'L');
	$pdf->Ln(1);
	
	$y_actual = $pdf->GetY();
	$part_img = explode(".",$row["man_imagen"]);
	$extension = $part_img[count($part_img)];
	$pdf->Image($url_base."upload/".$_REQUEST["db"]."/".$row["man_imagen"],10,$y_actual,90,0,$extension);
}

/****************************************************************************************************/ 
 
$pdf->Output('control_'.$_REQUEST["id"].'.pdf','I');
$pdf->Close(); 
?>