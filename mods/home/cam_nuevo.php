<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Nuevo Campo Personalizado";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function save(){ 
    if(document.getElementById("nombre").value == ""){
        alerta_js("Es obligación ingresar el nombre");
        return; 
    }
	if(document.getElementById("tipo").value == ""){
		alerta_js("Es obligación ingresar el tipo");
		return;	
	}
	carg("cam_save.php","");
}
function tipo_campo(valor){
    switch(valor){
        case "check":
        case "radio":
        case "select":
            $("#opciones_").show();
            break;
        default:
            $("#opciones_").hide();
            break;
    }
}
$(document).ready(function() {
    $('#opciones').tagsInput({width:'auto'});
});
</script> 

<form class="form-horizontal" role="form" method="post">
	<input type="hidden" name="modo" value="crear" class="campos">  
    <div class="form-group">
        <label for="nombre" class="col-sm-2 control-label">Nombre <span class="oblig">(*)</span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control campos" id="nombre" name="nombre">
        </div>
    </div>  
    <div class="form-group">
        <label for="orden" class="col-sm-2 control-label">Tipo <span class="oblig">(*)</span></label>
        <div class="col-sm-3">
            <select class="form-control campos" id="tipo" name="tipo" onchange="javascript:tipo_campo(this.value);">
                <option value="texto">Texto</option>
                <option value="textarea">Textarea</option>
                <option value="numero">Número</option>
                <option value="fecha">Fecha</option>
                <option value="select">Selección</option>
                <option value="archivo">Archivo</option>
                <?
                /*
                <option value="imagen">Imagen</option>
                <option value="imagenes">Imagenes</option>
                <option value="check">Checkbox</option>
                <option value="radio">Radio</option>
                */
                ?>
            </select>
        </div>
    </div> 
    <div class="form-group" id="opciones_" style="display: none;">
        <label for="orden" class="col-sm-2 control-label">Opciones</label>
        <div class="col-sm-10">
            <textarea rows="8" class="form-control campos" id="opciones" name="opciones"><?php echo $datos["cam_opciones"]; ?></textarea>
        </div>
    </div> 
    <div class="form-group">
        <label for="orden" class="col-sm-2 control-label">Para Producto</label>
        <div class="col-sm-5">
           <select name="producto" class="campos">
            <option value="0" <? if($datos["cam_producto"] == "0"){ echo "selected"; } 
            ?>>-</option>
            <option value="1" <? if($datos["cam_producto"] == "1"){ echo "selected"; } 
            ?>>Campo disponible en la ficha del producto</option>
            </select>
        </div>
    </div> 
    <div class="form-group">
        <label for="orden" class="col-sm-2 control-label">Para Activo</label>
        <div class="col-sm-5">
           <select name="activo" class="campos">
            <option value="0" <? if($datos["cam_activo"] == "0"){ echo "selected"; } 
            ?>>-</option>
            <option value="1" <? if($datos["cam_activo"] == "1"){ echo "selected"; } 
            ?>>Campo disponible en la ficha del activo</option>
            </select>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-sm-2"/>
        <div class="col-sm-10">
        	<?php
			construir_boton("","","grabar","Guardar",3);
			construir_boton("cam_listado.php","","eliminar","Cancelar",2);
			?>
        </div>
    </div>
</form> 