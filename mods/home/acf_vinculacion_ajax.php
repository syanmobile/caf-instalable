<?php
require("../../inc/conf_dentro.php");
 
switch($_REQUEST["modo"]){
	case "disponibles":  
		$res = sql_activos_fijos("*"," and acf_disponible = 1 and acf_id <> $_REQUEST[activo] order by  acf.acf_producto asc, acf.acf_serie asc "); 
		if(mysqli_num_rows($res) > 0){
			?>
			<table class="table table-striped table-bordered tabledrag table-condensed" id="tablita2"> 
			<thead>
				<tr>
					<th width="1"></th> 
					<th width="1">Codigo</th>
					<th width="1">Serial</th>
					<th>Producto</th>

					<th>Ubicacion</th>  
					<th>Categoria</th>
				</tr>
			</thead>
			<tbody> 
			<?
			while($row = mysqli_fetch_array($res)){  
				?>
				<tr id="acf2_<?php echo $row["acf_id"]; ?>"> 
					<td><? 
					construir_boton("agrupar('".$row["acf_id"]."')","1","izquierda","Elegir",4);
					?></td>
					<th><?php echo $row["pro_codigo"]; ?></th>
					<th><?php echo $row["acf_serie"]; ?></th>
					<td><?php echo $row["pro_nombre"]; ?></td>

					<td><?php echo $row["ubi_nombre"]; ?></td>  
 
					<td><?php echo $_categorias[$row["pro_categoria"]]; ?></td> 
				</tr>
				<?  
			}
			?>
			</tbody>
			</table>
			<script language="javascript">
			$(document).ready(function() { 
				$('#tablita2').DataTable({ 
					"columns": [   
						{ "width": "1px" }, 
						null ,  
						null , 
						null ,  
						null ,  
						null ,  
						null ,  
						null 
					  ],
					 "lengthMenu": [ 10,30,50,100,200,500,1000,10000],
					"oLanguage": {
						"sSearch": "Busque un activo y presione flecha para agrupar al AF Objetivo"
					}
				});
			} );   
			</script> 
			<?
		}else{
			?>
			<div class="alert alert-danger">
				<strong>Sin registros creados</strong>
			</div>
			<?php 
		}  
		break;
		
	case "agrupados":  
		$res = sql_activos_fijos("*"," and acf_agrupado = $_REQUEST[activo] order by  acf.acf_producto asc, acf.acf_serie asc "); 
		if(mysqli_num_rows($res) > 0){
			?>
			<table class="table table-striped table-bordered tabledrag table-condensed" id="tablita"> 
			<thead>
				<tr>
					<th width="1">Codigo</th>
					<th width="1">Serial</th>
					<th>Producto</th>

					<th>Ubicacion</th> 

					<th>Grupo</th>
					<th>Subgrupo</th>
					<th>Categoria</th>
					<th width="1"></th> 
				</tr>
			</thead>
			<tbody> 
			<?
			while($row = mysqli_fetch_array($res)){  
				?>
				<tr id="acf1_<?php echo $row["acf_id"]; ?>"> 
					<th><?php echo $row["pro_codigo"]; ?></th>
					<th><?php echo $row["acf_serie"]; ?></th>
					<td><?php echo $row["pro_nombre"]; ?></td>

					<td><?php echo $row["ubi_nombre"]; ?></td>  

					<td><?php echo $_grupos[$row["pro_grupo"]]; ?></td>
					<td><?php echo $_subgrupos[$row["pro_subgrupo"]]; ?></td>
					<td><?php echo $_categorias[$row["pro_categoria"]]; ?></td> 
					
					<td><? 
					construir_boton("desagrupar('".$row["acf_id"]."')","1","derecha","Elegir",4);
					?></td>
				</tr>
				<?  
			}
			?>
			</tbody>
			</table>
			<script language="javascript">
			$(document).ready(function() { 
				$('#tablita').DataTable({ 
					"columns": [   
						null ,  
						null , 
						null ,  
						null ,  
						null ,  
						null ,  
						null ,
						{ "width": "1px" }
					  ],
					 "lengthMenu": [ 10,30,50,100,200,500,1000,10000],
					"oLanguage": {
						"sSearch": "Presione flecha para desagrupar un activo"
					}
				});
			} );   
			</script> 
			<?
		}else{
			?>
			<div class="alert alert-danger">
				<strong>Sin activos agrupados al Activo Objetivo</strong>
			</div>
			<?php 
		}  
		break;
		
	case "agrupar":		
		$sql3 = "update activos_fijos set acf_disponible = '6',acf_agrupado = '$_REQUEST[activo]' where acf_id = '".$_REQUEST["acf"]."' ";
		$res3 = mysqli_query($cnx,$sql3);		
		break;
		
	case "desagrupar":		
		$sql3 = "update activos_fijos set acf_disponible = '1',acf_agrupado = '0' where acf_id = '".$_REQUEST["acf"]."' ";
		$res3 = mysqli_query($cnx,$sql3);		
		break;
}
?> 