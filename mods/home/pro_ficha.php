<?php
require("../../inc/conf_dentro.php");
require("../../_css.php");

$res = sql_productos("*","and pro.pro_codigo = '$_REQUEST[codigo]'","");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<b>ERROR EN CONSULTA</b><br>No se encontró el Producto con código <strong>"<? echo $_REQUEST["codigo"]; ?>"</strong>
	</div>
    <?php
	exit();
}
?>
<div style="padding:5px 10px;">
<h3><?php echo $datos["pro_nombre"]; ?></h3>

<table width="100%">
<tr valign="top">
    <td width="200">
        <? 
		$imagen = ($datos["pro_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$datos["pro_imagen"];
		?><img src="../../<?php echo $imagen; ?>" style="width:200px !important;">

        <div class="alert alert-info">
            <b>Ultima modificación del producto:</b><br>
            <?php echo $datos["pro_modificacion"]; ?>
        </div>
        
		<?php 
        construir_boton("window.print()","","imprimir","Imprimir Ficha",4);
        ?>
    </td>
    <td style="padding-left:10px;">
        <table class="table table-bordered table-condensed"> 
        <tbody>
            <tr>
                <th width="120" style="text-align:right;">Nombre:</th>
                <th><?php echo $datos["pro_nombre"]; ?></th>
            </tr> 
            <tr>
                <th style="text-align:right;">Código:</th>
                <td><?php echo $datos["pro_codigo"]; ?></td> 
            </tr>
            <tr>
                <th style="text-align:right;">Cód.Barra:</th>
                <td><?php echo $datos["pro_codigo_barra"]; ?></td>
            </tr> 
			<tr>
				<th style="text-align:right;">Tipo Producto:</th>
                <td><?php echo $_tipo_activo[$datos["pro_tipo"]]; ?></td>
            </tr> 
            <tr>
                <th style="text-align:right;">Grupo:</th>
                <td><?php echo $datos["gru_codigo"]." - ".$datos["gru_nombre"]; ?></td>
            </tr> 
            <tr>
                <th style="text-align:right;">SubGrupo:</th>
                <td><?php echo $datos["sgru_codigo"]." - ".$datos["sgru_nombre"]; ?></td>
            </tr>  
            <tr>
                <th style="text-align:right;">Categoría:</th>
                <td><?php echo $datos["cat_codigo"]." - ".$datos["cat_nombre"]; ?></td>
            </tr> 
            <tr>
                <th style="text-align:right;">Unidad:</th> 
                <td><?php echo $datos["uni_codigo"]." - ".$datos["uni_nombre"]; ?></td>
            </tr>
            <tr>
                <th style="text-align:right;">Mantenimiento:</th> 
                <td><?php echo ($datos["pro_mantenimiento"])?"SI":"-"; ?></td>
            </tr> 
			<tr>
				<th width="120" style="text-align:right;">Stock Mínimo:</th>
				<td colspan="2"><?php echo _num($datos["pro_stock_minimo"]); ?></td>
			</tr>   
			<?php	

            for($iex = 1;$iex <= 10;$iex++){                 
                $res = sql_campos("*"," and cam_producto = $iex order by cam_nombre asc");  
                if(mysqli_num_rows($res) > 0){
                    while($row = mysqli_fetch_array($res)){
                        ?> 
                        <tr> 
                            <th style="text-align: right;"><? echo $row["cam_nombre"]; ?>:</th>
                            <td><? echo $datos["pro_extra_".$iex]; ?></td> 
                        </tr>
                        <? 
                    }
                }
            }			 


			?> 
            <tr>
                <th style="text-align:right;">Descripción:</th> 
                <td><?php echo nl2br($datos["pro_descripcion"]); ?></td>
            </tr> 
        </tbody>
        </table> 
    </td> 
</tr>
</table> 
</div> 