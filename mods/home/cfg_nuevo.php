<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Nueva Configuración";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

if($_REQUEST["codigo"] <> ""){
	$valores = explode("*****",file_get_contents("http://www.syancloud.cl/central/configuracion_valor.php?codigo=".$_REQUEST["codigo"])); 
	
	$grupo = $valores[1];
	$titulo = $valores[2];
	$valor = $valores[3];
	$tipo = $valores[4];
}
?>
<script language="javascript">
function save(){    
	if(document.getElementById("titulo").value == ""){
		alerta_js("Es obligación ingresar el TITULO del parámetro");
		return;	
	}
	carg("cfg_save2.php","");
}
</script>
 
<form class="form-horizontal" role="form" method="post">
	<input type="hidden" name="modo" value="crear" class="campos">
    
    <div class="form-group">
        <label for="codigo" class="col-sm-2 control-label">Código <span class="oblig">(*)</span></label>
        <div class="col-sm-3">
            <input type="text" class="form-control campos" id="codigo" name="codigo" value="<? echo $_REQUEST["codigo"]; ?>">
        </div>
    </div>
    
    <div class="form-group">
        <label for="grupo" class="col-sm-2 control-label">Grupo <span class="oblig">(*)</span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control campos" id="grupo" name="grupo" value="<? echo $grupo; ?>">
        </div>
    </div>
    
    <div class="form-group">
        <label for="titulo" class="col-sm-2 control-label">Titulo <span class="oblig">(*)</span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control campos" id="titulo" name="titulo" value="<? echo $titulo; ?>">
        </div>
    </div>
    
    <div class="form-group">
        <label for="tipo" class="col-sm-2 control-label">Tipo <span class="oblig">(*)</span></label>
        <div class="col-sm-10">
            <select name="tipo" id="tipo" class="form-control campos">
            <option value="texto" <? if($tipo == "texto"){ echo "selected"; } ?>>Texto</option>
            <option value="textarea" <? if($tipo == "textarea"){ echo "selected"; } ?>>Textarea</option>
            <option value="serial" <? if($tipo == "serial"){ echo "selected"; } ?>>Serial</option>
            </select>
        </div>
    </div>
    
    <div class="form-group">
        <label for="valor" class="col-sm-2 control-label">Valor <span class="oblig">(*)</span></label>
        <div class="col-sm-10">
            <textarea rows="6" type="text" class="form-control campos" id="valor" name="valor"><? echo $valor; ?></textarea>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-sm-2"/>
        <div class="col-sm-10">
        	<?php
			construir_boton("","","grabar","Guardar",3);
			construir_boton("navegacion.php","","eliminar","Cancelar",2);
			?>
        </div>
    </div>
</form> 