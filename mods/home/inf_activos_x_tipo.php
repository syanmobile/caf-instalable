<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Activos Fijos por Tipo";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?> 
<table class="table table-bordered table-condensed table-info">
<tr>  
    <td width="100">
    <b>Código:</b><br />
    <input type="text" name="fil_codigo" id="fil_codigo" class="campos w10" value="<? echo $_REQUEST["fil_codigo"]; ?>"></td> 
    <td>
    <b>Descripción:</b><br />
    <input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? echo $_REQUEST["fil_descripcion"]; ?>"></td>  
    <td>
    <b>Grupo:</b><br />
    <select name="fil_grupo" id="fil_grupo" class="campos buscador w10">
    <option value=""></option>
    <?php
    $res = sql_grupos("*"," order by gru.gru_nombre asc"); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["gru_id"]; ?>" <?
            if($_REQUEST["fil_grupo"] == $row["gru_id"]){ echo "selected"; } 
            ?>><? echo $row["gru_nombre"]; ?></option>
            <?
        }
    }
    ?></select></td> 
    <td>
    <b>Subgrupo:</b><br />
    <select name="fil_subgrupo" id="fil_subgrupo" class="campos buscador w10">
    <option value=""></option>
    <?php
    $res = sql_subgrupos("*"," order by sgru.sgru_nombre asc"); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["sgru_id"]; ?>" <?
            if($_REQUEST["fil_subgrupo"] == $row["sgru_id"]){ echo "selected"; } 
            ?>><? echo $row["sgru_nombre"]; ?></option>
            <?
        }
    }
    ?></select></td> 
    <td>
    <b>Categorias:</b><br />
    <select name="fil_categoria" id="fil_categoria" class="campos buscador w10">
    <option value=""></option>
    <?php
    $res = sql_categorias("*"," order by cat.cat_nombre asc"); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["cat_id"]; ?>" <?
            if($_REQUEST["fil_categoria"] == $row["cat_id"]){ echo "selected"; } 
            ?>><? echo $row["cat_nombre"]; ?></option>
            <?
        }
    }
    ?></select></td>
    <td>
    <b>Ubicación:</b><br />
    <select name="fil_ubicacion" id="fil_ubicacion" class="campos buscador w10">
    <option value=""></option>
    <?php
    $res = sql_ubicaciones("*"," order by bod.bod_nombre asc, ubi.ubi_nombre asc"); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["ubi_id"]; ?>" <?
            if($_REQUEST["fil_ubicacion"] == $row["ubi_id"]){ echo "selected"; } 
            ?>><? echo $row["bod_nombre"]." - ".$row["ubi_nombre"]; ?></option>
            <?
        }
    }
    ?></select></td>

    <td width="1"><?
    construir_boton("alerta_js('Excel no disponible (Demo)');","1","importar","Descargar",4); 
    ?></td> 
    <td width="1"><?php
    construir_boton("inf_activos_x_tipo.php","","buscar","Filtrar");
    ?></td> 
</tr>
</table>

<?php


$v_codigo = $_REQUEST["fil_codigo"];
$v_descripcion =$_REQUEST["fil_descripcion"];
$v_grupo = $_REQUEST["fil_grupo"];
$v_subgrupo = $_REQUEST["fil_subgrupo"];
$v_categoria = $_REQUEST["fil_categoria"];
$v_filtro = "";


if($v_codigo!=""){
$v_filtro = " AND pro_codigo LIKE '%$v_codigo%' ";
}
if($v_descripcion !=""){
$v_filtro = $v_filtro." AND pro_nombre LIKE '%$v_descripcion%' ";
}

if($v_ubicacion !=""){
$v_filtro = $v_filtro." AND acf_ubicacion = '$v_ubicacion' ";
}

if($v_grupo !=""){
$v_filtro = $v_filtro." AND pro_grupo='$v_grupo' ";
}

if($v_subgrupo !=""){
$v_filtro = $v_filtro." AND pro_subgrupo='$v_subgrupo' ";
}

if($v_categoria !=""){
$v_filtro = $v_filtro." AND pro_categoria='$v_categoria' ";
}
 
$res = sql_activos_fijos("*,count(*) as stock,sum(acf_valor) as valor"," and pro_tipo <> 'INS' group by pro_codigo  order by pro_tipo asc ,pro_nombre asc ","",""); 
if(mysqli_num_rows($res) > 0){
	while($row = mysqli_fetch_array($res)){ 
		if($row["pro_tipo"] <> $cat){
			if($cat <> ""){
				?> 
				<tr>  
					<th style="text-align: right;" colspan="4"><?php echo $_tipo_activo[$cat]; ?></th>
					<th style="text-align: right;"><? echo _num($total_cat); ?></th>
				</tr> 
				<?
				$total_cat = 0;
				echo '</tbody></table>';
			}
			?>
			<table class="table table-striped table-bordered tabledrag table-condensed"> 
			<thead>
			<tr>
				<th colspan="15"><?php 
                echo $_tipo_activo[$row["pro_tipo"]];
                ?></th>
			</tr>
			</thead>
			<tbody>
			<tr>   
				<th width="1">Código</th> 
				<th>Producto</th> 
                <th width="1">Stock</th>
                <th width="1">CostoPromedio</th>
				<th width="1">Total</th>
			</tr>
			<?
			$cat = $row["pro_tipo"]; 
		} 
		$subtotal = $row["valor"];
        $prome = round($subtotal / $row["stock"]);
		?>
		<tr>  
			<td><? echo $row["pro_codigo"]; ?></td> 
			<td><? echo $row["pro_nombre"]; ?></td> 
            <th style="text-align: center;"><? echo _num2($row["stock"]); ?></th>
            <td style="text-align: right;"><? echo _num($prome); ?></td> 
			<td style="text-align: right;"><? echo _num($subtotal); ?></td> 
		</tr>
		<?
        $stock_final += $row["stock"] * 1;
		$total_cat += $subtotal;
		$total_final += $subtotal;
	}
	?> 
	<tr>  
		<th style="text-align: right;" colspan="4"><?php echo $_tipo_activo[$cat]; ?></th>
		<th style="text-align: right;"><? echo _num($total_cat); ?></th>
	</tr> 
	</tbody>
	</table>
	<h4 style="text-align: right">Total Final:<br><? echo _num($total_final); ?></h4>
    <h5 style="text-align: right;">Total Items:<br><? echo _num2($stock_final); ?></h5>
	<?
}else{
	?>
	<div class="alert alert-danger">
		<strong>Sin registros creados</strong>
	</div>
	<?php 
} 
?>