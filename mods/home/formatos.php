<?php
require("../../inc/conf_dentro.php"); 
require("../../_css.php");

switch($_REQUEST["formato"]){
	case "tabla_simple":
		$columnas = array(
			array("Código Único","Caracter","SI",""),
			array("Nombre","Caracter","SI","") 
		);
		break;

    case "clasificacion":
        $columnas = array(
            array("Código Clasificación","Caracter","SI",""),
            array("Nombre","Caracter","SI",""),
            array("Código de Clasificación (Padre)","Caracter","NO","Debe existir en la Tabla: Clasificación")
        );
        break;  

	case "bodegas":
		$columnas = array(
			array("Código Bodega","Caracter","SI",""),
			array("Nombre","Caracter","SI","") 
		);
		break;  

	case "productos":
		$columnas = array(
			array("Código Producto","Caracter","SI",""),
			array("Nombre Producto","Caracter","SI",""),
            array("Descripción Producto","Caracter","NO",""),
			array("Código de Barra","Caracter","NO",""),
            array("Tipo de Producto","Caracter","SI",""), 
			array("Código de Clasificación","Caracter","NO","Debe existir en la Tabla: Clasificación"), 
            array("Código de Unidad","Caracter","NO","Debe existir en la Tabla: Unidades"),
			array("Requiere Mantenimiento","Boolean","NO",""),
            array("Usa Garantía","Boolean","NO",""),
            array("Activo es Prestable","Boolean","NO",""),
            array("Activo es Asignable","Boolean","NO","")    
		);
		break;

	case "auxiliares":
		$columnas = array(
			array("Código Proveedor","Caracter","SI",""),
			array("Rut Proveedor","Caracter","NO",""),
			array("Nombre Proveedor","Caracter","SI",""),
			array("Dirección","Caracter","NO",""),
			array("Comuna","Caracter","NO",""),
			array("Telefono","Caracter","NO",""),
			array("Correo","Caracter","NO",""),
		);
		break; 

	case "activos":  
		$columnas = array(
			array("Etiqueta","Caracter","NO",""),
			array("Serie","Caracter","NO",""),
			array("Producto","Caracter","SI","Debe existir en la Tabla: Productos"),
			array("Fecha Compra","Fecha","NO",""),
			array("Monto","Caracter","NO",""),
			array("Proveedor","Caracter","NO","Debe existir en la Tabla: Proveedores"),
			array("Folio","Caracter","NO",""),
			array("Centro Costo","Caracter","NO","Debe existir en la Tabla: Centro Costo"),
			array("Ubicación","Caracter","SI","Debe existir en la Tabla: Ubicación"),
			array("Fecha Inicio Depreciacion","Caracter","NO",""),
			array("Valor Residual","Caracter","NO",""), 
		);
		break; 
}
?>
<div style="padding:15px;">  
    <h2>Formato Archivo</h2> 
	
    <div class="alert alert-info"> 
	   <b>Información:</b><br>
		- La primera linea correspondiente a la cabecera no se importará<br>
		- El archivo a importar debe ser en formato CSV delimitado por punto y coma (;)
	</div>
    
    <table class="table table-bordered table-hover table-condensed"> 
    <thead> 
    <tr> 
        <th width="1">Nro</th>
        <th>Descripción</th>
        <th width="1">Tipo</th>
        <th width="1">Exigido</th>
        <th>Opciones</th>
        <th>Observaciones</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($columnas as $columna){
        $i++;
        ?>		
        <tr <?php if($columna[2] == "SI"){ ?>style="background-color:#FFC !important;"<? } ?>>
            <th style="text-align:center;"><? echo $i; ?></th>
            <td><? echo $columna[0]; ?></td>
            <td style="text-align:center;"><? echo $columna[1]; ?></td>
            <td style="text-align:center;"><? echo $columna[2]; ?></td>
            <td style="padding-left:15px;"><?
            switch($columna[0]){
				case "Tipo de Producto":  
                    echo "<li><b>INS</b> - Insumos</li>";
                    $contador = 0;
                    foreach($_tipo_activos as $tp){ 
                        $contador++;
                        if($contador == 10){
                            break;
                        }
                        echo "<li><b>$tp</b> - ".$_tipo_activo[$tp]."</li>";
                    }
                    break; 

				case "Código de Tipo de Documento":  
                    $res = sql_tipos("*"," order by tip.tip_codigo asc limit 0,10"); 
                    if(mysqli_num_rows($res) > 0){
                        while($row = mysqli_fetch_array($res)){  
                            ?>
                            <li><b><?php echo $row["tip_codigo"]; ?></b> - <?php echo $row["tip_nombre"]; ?></li> 
                            <?php  
                        }
                    } 
					break;

				case "Código de Concepto de Ingreso": 
                    $res = sql_conceptos("*","   and con.con_tipo = 'E' order by con.con_codigo asc limit 0,10"); 
                    if(mysqli_num_rows($res) > 0){
                        while($row = mysqli_fetch_array($res)){
                            ?>
                            <li><b><?php echo $row["con_codigo"]; ?></b> - <?php echo $row["con_nombre"]; ?></li> 
                            <? 
                        }
                    } 
					break;

				case "Código de Concepto de Salida": 
                    $res = sql_conceptos("*","   and con.con_tipo = 'S' order by con.con_codigo asc limit 0,10"); 
                    if(mysqli_num_rows($res) > 0){
                        while($row = mysqli_fetch_array($res)){
                            ?>
                            <li><b><?php echo $row["con_codigo"]; ?></b> - <?php echo $row["con_nombre"]; ?></li> 
                            <? 
                        }
                    } 
					break;

				case "Código de Concepto de Ajuste": 
                    $res = sql_conceptos("*","   and con.con_tipo = 'A' order by con.con_codigo asc limit 0,10"); 
                    if(mysqli_num_rows($res) > 0){
                        while($row = mysqli_fetch_array($res)){
                            ?>
                            <li><b><?php echo $row["con_codigo"]; ?></b> - <?php echo $row["con_nombre"]; ?></li> 
                            <? 
                        }
                    } 
					break;

				case "Código de Lugar":
					$res = sql_bodegas("*","   order by bod.bod_codigo asc limit 0,10"); 
                    if(mysqli_num_rows($res) > 0){
                        while($row = mysqli_fetch_array($res)){  
                            ?>
                            <li><b><?php echo $row["bod_codigo"]; ?></b> - <?php echo $row["bod_nombre"]; ?></li>  
                            <?php  
                        }
                    }
					break;

				case "Código de Clasificación":
                    $res = sql_categorias("*","   order by cat.cat_codigo asc limit 0,10"); 
                    if(mysqli_num_rows($res) > 0){
                        while($row = mysqli_fetch_array($res)){  
                            ?>
                            <li><b><?php echo $row["cat_codigo"]; ?></b> - <?php echo $row["cat_nombre"]; ?></li> 
                            <?php  
                        }
                    }
					break;

				case "Código de Unidad":
                    $res = sql_unidades("*","   order by uni.uni_codigo asc limit 0,10"); 
                    if(mysqli_num_rows($res) > 0){
                        while($row = mysqli_fetch_array($res)){  
                            ?>
                            <li><b><?php echo $row["uni_codigo"]; ?></b> - <?php echo $row["uni_nombre"]; ?></li> 
                            <?php  
                        }
                    }
					break; 
			}
            ?></td>
            <td><? echo $columna[3]; ?></td>
        </tr>
        <?
    }
    ?>
    </tbody>
    </table> 
</div>