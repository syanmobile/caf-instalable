<?php
require("../../inc/conf_dentro.php"); 

$labels = explode(";",$_REQUEST["label"]);
$valores = explode(";",$_REQUEST["valor"]);

if($_REQUEST["titulo"] <> ""){
	?>
	<link href="<?php echo $url_base; ?>css/theme/bootstrap_default.css" rel="stylesheet">
	<link href="<?php echo $url_base; ?>css/estilos.css" rel="stylesheet">
	<link href="<?php echo $url_base; ?>css/fontawesome-free/css/all.css" rel="stylesheet">

	<div style="padding:0px 10px;">
	<table width="100%">
	<tr valign="top">
		<td>
			<h1><? echo $_REQUEST["titulo"]; ?></h1>
			<i>Informe generado el <? echo date("d/m/Y"); ?> a las <? echo date("H:i"); ?></i>
		</td>
		<td width="1" style="padding-top: 15px;">
			<input type="button" onclick="javascript:window.print();" value="Imprimir">
		</td>
	</tr>
	</table>
	</div>
	<?
}
?>
<script>
window.onload = function () {

	CanvasJS.addColorSet("customColorSet1",["#C0392B","#9B59B6","#2980B9","#1ABC9C","#27AE60","#F1C40F","#E67E22","#95A5A6","#34495E", "#E74C3C","#8E44AD","#3498DB","#16A085","#2ECC71", "#F39C12","#D35400","#7F8C8D","#566573"]);

	var options = {
		credits: {
		      enabled: false
		  },
		animationEnabled: true, 
		colorSet: "customColorSet1",
		data: [{
			type: "doughnut",
			innerRadius: "10%",  
			indexLabel: false,
			dataPoints: [
				<?
				for($i = 1;$i < count($labels);$i++){ 
					$label =  preg_replace("/[\r\n|\n|\r]+/", " ", $labels[$i]);

					if($i > 1){ echo ","; }
					?>{ label: "<? echo $label; ?>", y: <? echo $valores[$i]; ?> }<?
				} 
				?>
			]
		}]
	};
	$("#chartContainer").CanvasJSChart(options);

}
</script>
<div id="chartContainer" style="border:1px solid #ccc; height: <? 
if($_REQUEST["alto"] == ""){ echo "200"; }else{ echo $_REQUEST["alto"]; } 
?>px; width: <? if($_REQUEST["ancho"] == ""){ echo "100%"; }else{ echo $_REQUEST["ancho"]."px"; } ?>"></div>

<script src="../../js/jquery-1.11.1.min.js"></script>
<script src="../../js/jquery.canvasjs.min.js"></script>