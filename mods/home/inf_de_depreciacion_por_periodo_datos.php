<?php 
include("../../inc/conf_dentro.php");

/****************************************************************************************/
if($_REQUEST["fil_codigo"] <> ""){
	$filtros .= " and pro.pro_codigo like '".$_REQUEST["fil_codigo"]."' ";
}
if($_REQUEST["fil_codigo2"] <> ""){
	$filtros .= " and acf.acf_codigo like '".$_REQUEST["fil_codigo2"]."' ";
}
if($_REQUEST["fil_serie"] <> ""){
	$filtros .= " and acf.acf_serie like '".$_REQUEST["fil_serie"]."' ";
}
if($_REQUEST["fil_etiqueta"] <> ""){
	$filtros .= " and acf.acf_etiqueta like '".$_REQUEST["fil_etiqueta"]."' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and pro.pro_nombre like '".$_REQUEST["fil_descripcion"]."' ";
} 
if($_REQUEST["fil_unidad"] <> ""){
	$filtros .= " and pro.pro_unidad = '".$_REQUEST["fil_unidad"]."' ";
}  
if($_REQUEST["fil_categoria"] <> ""){
	$filtros .= " and pro.pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
}
if($_REQUEST["fil_cco"] <> ""){
	$filtros .= " and acf.acf_cco_id = '".$_REQUEST["fil_cco"]."' ";
}
if($_REQUEST["fil_ubicacion"] <> ""){
	$filtros .= " and acf.acf_ubicacion = '".$_REQUEST["fil_ubicacion"]."' ";
}   
if($_REQUEST["fil_tipo"] <> ""){
	$filtros .= " and pro.pro_tipo = '".$_REQUEST["fil_tipo"]."' ";
}  
if($_REQUEST["fil_responsable"] <> ""){
	$filtros .= " and acf.acf_responsable = '".$_REQUEST["fil_responsable"]."' ";
} 
if($_REQUEST["fil_estado"] <> ""){
	$filtros .= " and acf.acf_disponible = '".$_REQUEST["fil_estado"]."' ";
}
for($ie = 1;$ie <= 10;$ie++){
	if($_REQUEST["fil_extra_acf_".$ie] <> ""){
		$filtros .= " and acf.acf_extra_".$ie." like '".$_REQUEST["fil_extra_acf_".$ie]."' ";
	}
	if($_REQUEST["fil_extra_pro_".$ie] <> ""){
		$filtros .= " and pro.pro_extra_".$ie." like '".$_REQUEST["fil_extra_pro_".$ie]."' ";
	}
}
?>
<input type="hidden" name="pagina" id="pagina" value="inf_de_depreciacion_por_periodo_datos.php" class="campos">
<?
if($_REQUEST["fil_periodo"] <> ""){
	?> 
	<table class="table table-striped table-bordered tabledrag table-condensed" id="tablita_1"> 
	<thead>
		<tr>     
			<th rowspan="2">CodActivo</th>  
			<th rowspan="2">Descripcion</th>
			<th rowspan="2">CCosto</th> 
			<th rowspan="2">F.Ingreso</th>
			<th rowspan="2" style="text-align:center;">Vida Util</th>
			<th rowspan="2">Doc.Ingreso</th> 
			<th rowspan="2" style="text-align:right;">Valor Activo</th>
			<?
			if($_REQUEST["fil_moneda"] <> ""){
				?>
				<th style="text-align:right;">Tipo Cambio</th>
				<?
			}
			?>
			<th rowspan="2" style="text-align:right;">Valor Residual</th>
			<th rowspan="2">F.Inicio Deprec.</th>
			<th rowspan="2">F.Termino Deprec.</th>
			<th rowspan="2">F.Baja</th>
			<th rowspan="2" style="text-align:right;">Cuota Mensual Dep.</th>

			<th style="text-align:center;" colspan="2">Dep.Ejercicio Anterior</th>
			<th style="text-align:center;" colspan="2">Dep.Ejercicio Periodo</th>
			<th style="text-align:center;" rowspan="2">Meses Restantes</th>
			<th style="text-align:center;" rowspan="2">Depreciación Acumulada</th>
			<th style="text-align:center;" rowspan="2">Valor Libro</th>
		</tr>
		<tr>
			<th>Meses</th>
			<th>Valor$</th>
			<th>Meses</th>
			<th>Valor$</th> 
		</tr>
	</thead>
	<tbody>
	<?php 
	$res = sql_activos_fijos_rapido("*"," and acf.acf_mes_ano_inicio_depreciacion <= '".$_REQUEST["fil_periodo"]."-12-31' $filtros ",""); 
	if(mysqli_num_rows($res) > 0){
		while($row = mysqli_fetch_array($res)){

			// Ajustar la cuota de depreciación----------------------------------------
			$meses = $row["acf_vida_util"] * 1;
			if($row["acf_tipo_depreciacion"] == "L"){
				$meses_divisor = $meses;
			}else{
				$meses_divisor = round($meses / 3);
			}
			$valor_activo = round($row["acf_valor_a_depreciar"] * 1);
			$row["acf_cuota_depreciacion"] = ($valor_activo / $meses_divisor);
			//-------------------------------------------------------------------------

			//echo $row["acf_fecha_ingreso"]." hasta el ".$_REQUEST["fil_periodo"]."-12-31";
			$row["meses_transcurridos"] = calcula_meses_transcurridos($row["acf_mes_ano_inicio_depreciacion"],$_REQUEST["fil_periodo"]."-12-31");
			//echo " = ".$row["meses_transcurridos"]." meses<hr>"; 

			if($_REQUEST["fil_moneda"] <> ""){
				$row["acf_valor"] = ($row["acf_valor"] / $row["acf_valor_sec"]);
				$row["acf_valor_residual"] = ($row["acf_valor_residual"] / $row["acf_valor_sec"]);
				$row["acf_cuota_depreciacion"] = ($row["acf_cuota_depreciacion"] / $row["acf_valor_sec"]);
				$row["acf_valor_a_depreciar"] = ($row["acf_valor_a_depreciar"] / $row["acf_valor_sec"]);
				$decimales = $_configuraciones["cfg_moneda_sec_decimal"];
				$tipo_cambio = $row["acf_valor_sec"];
			}else{
				$tipo_cambio = 0;
				$decimales = $_configuraciones["cfg_moneda_decimal"];
			}  
			?> 
			<tr>  
				<td><? echo $row["acf_codigo"]; ?></td>
				<td><? echo $row["pro_nombre"]; ?></td> 
				<td><? echo $_centro_costos_corto[$row["acf_cco_id"]]; ?></td> 
				<td><?php echo _fec($row["acf_fecha_ingreso"],10); ?></td>
				<td style="text-align: center;"><? echo $row["acf_vida_util"]; ?></td>  
				<td><?php echo $row["acf_nro_factura"]; ?></td> 
				<td style="text-align: right;"><?php echo _num($row["acf_valor"],$decimales); ?></td> 
				<?
				if($_REQUEST["fil_moneda"] <> ""){ 
					?>
					<td style="text-align: right;"><?php echo _num($tipo_cambio,$decimales); ?></td>
					<?
				}
				?>
				<td style="text-align: right;"><? echo _num($row["acf_valor_residual"],$decimales); ?></td>
				<td><?php echo substr($row["acf_mes_ano_inicio_depreciacion"],0,7); ?></td> 
				<td><?php echo substr($row["acf_mes_ano_termino_depreciacion"],0,7); ?></td> 
				<td><?php echo _fec($row["acf_fecha_salida"],10); ?></td> 
				<td style="text-align: right;"><? echo _num($row["acf_cuota_depreciacion"],2); ?></td>

				<?                
				$meses_transcurridos = $row["meses_transcurridos"] * 1;
				$vida_util = $row["acf_vida_util"] * 1;

				// El año actual................. 
				if($meses_transcurridos < 13){
					$pasado = 0;
					$presente = $meses_transcurridos;
					$futuro = $vida_util - $meses_transcurridos;
				}else{
					// Si los meses transc. supera la vida util, entonces
					if($meses_transcurridos > $vida_util){
						$pasado = $vida_util;
						$presente = 0;
						$futuro = 0;
					}else{
						// Ejemplo 14
						$presente = $vida_util - $meses_transcurridos; // 120 - 14 = 106
						if($presente > 12){
							$presente = 12;
						}
						// Hay ocasiones que calza justo
						if($presente == 0){
							$presente = 12;
						} 
						$pasado = $meses_transcurridos - $presente; // 14 - 12 = 2
						$futuro = $vida_util - $presente - $pasado; // 120 - 12 - 2 = 
					}
				}

				$dep_acumulada_valor1 = $pasado * $row["acf_cuota_depreciacion"];
				$dep_acumulada_valor2 = $presente * $row["acf_cuota_depreciacion"]; 

				$suma = $dep_acumulada_valor1 + $dep_acumulada_valor2;
				$final = $row["acf_valor"] - $suma;
				?>
				<td style="text-align: center;"><? echo $pasado; ?></td> 
				<td style="text-align: right;"><? echo _num($dep_acumulada_valor1,$decimales); ?></td>
				<td style="text-align: center;"><? echo $presente; ?></td>
				<td style="text-align: right;"><? echo _num($dep_acumulada_valor2,$decimales); ?></td>
				<td style="text-align: center;"><? echo $futuro; ?></td>  
				<td style="text-align: right;"><? echo _num($suma,$decimales); ?></td> 
				<td style="text-align: right;"><? echo _num($final,$decimales); ?></td>  
			</tr>
			<? 
		} 
	} 
	?> 
	</tbody> 
	</table>

	<script language="javascript"> 
	$("#tablita_1").tableExport({
	    formats: ["xlsx"], //Tipo de archivos a exportar ("xlsx","txt", "csv", "xls")
	    position: 'button',  // Posicion que se muestran los botones puedes ser: (top, bottom)
	    bootstrap: false,//Usar lo estilos de css de bootstrap para los botones (true, false)
	    fileName: "Inf.Depreciacion",    //Nombre del archivo 
	    buttonContent: "Exportar Detalle a Excel",
	});
	</script>
	<?
}
?>