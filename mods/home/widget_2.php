<div class="col-md-<? echo $ancho; ?> grid-item">
	<div class="panel panel-primary">  
		<div class="panel-heading">
			<b>Responsables con Activos Fijos</b>
		</div>
		<div class="panel-body"> 
			<?php
			$res = sql_activos_fijos("*,count(*) as stock"," and (acf_disponible = 2 or acf_disponible = 6) and acf_responsable <> 0 group by acf.acf_responsable order by stock desc"); 
			if(mysqli_num_rows($res) > 0){
				?>
				<table class="table table-condensed table-bordered"> 
				<thead>
					<tr>
						<th>Responsable</th>
						<th width="1">Activo(s)</th>
					</tr>
				</thead>
				<tbody>
				<?
				while($row = mysqli_fetch_array($res)){ 
					$i_w2++;
					?>
					<tr <? if($i_w2 > 5){ echo 'class="oculto_w2" style="display:none;"'; } ?>> 
						<td><?php echo $_personas[$row["acf_responsable"]]; ?></td>
						<th style="text-align: center"><? echo _num2($row["stock"]); ?></th>
					</tr>
					<?
				}
				?>
				</tbody>
				</table>
				<?
                if($i_w8 > 5){
                	construir_boton("mostrar_w2()","","buscar","Ver más",4); 
                }
                ?>                
				<script type="text/javascript">
				function mostrar_w2(){
					$(".oculto_w2").toggle("fast");
				}	
				</script>
				<?
			}else{
				?> 
				<div class="alert alert-success">
					<strong>AL DIA</strong> (Sin trabajadores morosos)
				</div>
				<?
			}
			?>   
		</div>
	</div>
</div>