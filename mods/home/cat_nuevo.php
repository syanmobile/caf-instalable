<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Nueva Clasificación";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function save(){ 
	if(document.getElementById("codigo").value == ""){
		alerta_js("Es obligación ingresar el código");
		return;	
	}
	if(document.getElementById("nombre").value == ""){
		alerta_js("Es obligación ingresar el nombre");
		return;	
	}
	var div = document.getElementById("codigo_unico");
	AJAXPOST("mods/home/cat_ajax.php","&modo=codigo_unico&codigo="+document.getElementById("codigo").value+"&precodigo="+document.getElementById("precodigo").value,div);
}
function elegir(valor){
	document.getElementById("precodigo").value = valor;
}
</script>   

<form class="form-horizontal" role="form" method="post">
	<input type="hidden" name="modo" value="crear" class="campos">  
	
	<table class="table  table-bordered"> 
	<thead>
		<tr>
			<th colspan="2">Información Clasificación</th>
		</tr> 
	</thead> 
	<tbody>
		<tr>
			<th width="120" style="text-align:right;">Padre:</th>
			<td>
			<select class="campos buscador" id="padre" name="padre" onchange="javascript:elegir(this.value);">
			<option value=""></option>
			<?
			$res = sql_categorias("*"," ORDER BY cat.cat_codigo asc","","");
			if(mysqli_num_rows($res) > 0){
			    while($row = mysqli_fetch_array($res)){
			    	?>
			    	<option value="<?php echo $row["cat_codigo"]; ?>" <?
			    	if($row["cat_codigo"] == $_REQUEST["padre"]){ echo "selected"; } ?>>C<?php
			    	echo $row["cat_codigo"]." - "; 
			    	echo $row["cat_nombre_largo"]; ?></option>
			    	<?
			    }
			}
			?>
			</select></td>
		</tr>
		<tr>
			<th style="text-align:right;">Código:</th>
			<td>
				<table width="100%">
				<tr>
				<td width="1"><input type="text" name="precodigo" id="precodigo" class="campos w10" style="text-align:center; width:130px !important;" readonly value="<? echo $_REQUEST["padre"]; ?>"></td>
				<td width="1"><input type="text" name="codigo" id="codigo" class="campos w10" style="text-align:center; width:130px !important;"></td>
				<td id="codigo_unico" style="padding-left:5px;"></td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<th style="text-align:right;">Nombre:</th>
			<td><input type="text" class="campos w10" id="nombre" name="nombre" value=""></td>
		</tr>  
	</tbody>
	</table> 
	<?php
	construir_boton("","","grabar","Guardar",3);
	construir_boton("cat_listado.php","","eliminar","Cancelar",2);
	?>  
</form> 