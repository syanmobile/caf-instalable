<?php  
require("../../inc/db.php"); 
$cnx = mysqli_connect($app_db_host,$app_db_usuario,$app_db_password) or die("Falla en conexion MySQL");
mysqli_select_db($cnx,$app_db_basededatos); 
mysqli_query($cnx,"SET NAMES 'utf8'"); 

$campos = "acf_id;acf_producto;acf_codigo;acf_serie;acf_etiqueta;acf_cco_id;acf_valor;acf_valor_adicional;acf_fecha_ingreso;acf_concepto_ingreso;acf_nro_factura;acf_proveedor;acf_mes_ano_inicio_revalorizacion;acf_mes_ano_inicio_depreciacion;acf_mes_ano_termino_depreciacion;acf_totalmente_dep;acf_fecha_garantia;acf_fecha_pres_asig;acf_fecha_pres_asig_compromiso;acf_tipo_depreciacion;acf_tipo_adquisicion;acf_valor_a_depreciar;acf_cuota_depreciacion;acf_vida_util;acf_valor_residual;acf_fecha_salida;acf_concepto_salida;acf_valor_salida;acf_fecha_mantencion;acf_mantencion_anterior;acf_condicion;acf_disponible;acf_agrupado;acf_ubicacion;acf_responsable;acf_procesando;acf_creacion;acf_modificacion;pro_codigo;pro_nombre;pro_codigo_barra;pro_categoria;pro_unidad;pro_stock_minimo;pro_stock_maximo;pro_tipo;pro_valor;pro_inventario;pro_mantenimiento;pro_garantia;pro_prestable;pro_asignable;pro_imagen;pro_estado;pro_modificacion;pro_id_item_carga;cat_id;cat_codigo;cat_padre;cat_nombre;cat_nombre_largo;ubi_id;ubi_cco_id;ubi_codigo;ubi_bodega;ubi_nombre;ubi_ultimo_control;ubi_orden;ubi_activo;per_id;per_tipo;per_rut;per_nombre;per_elim;cco_id;cco_codigo;cco_nombre;aux_id;aux_codigo;aux_rut;aux_nombre";
$campos_arreglo = explode(";",$campos);

$sql = "select count(*) as total from activos_fijos";
$res = mysqli_query($cnx,$sql);
while($row = mysqli_fetch_array($res)){
    $maximo = $row["total"];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exportar Controlaf</title>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script type='text/javascript'>
    var data = "data:text/csv;charset=utf-8,<? echo $campos; ?>\n";
    $(document).ready(function(){
        exportToCVS(0,<? echo $maximo; ?>);
    });

    function exportToCVS(start,max){
        $("#numero").html(start);
        if(start > max){
            descargar_boton();
            return;
        }
        $.ajax({
            url: "descargar_todo_info.php", 
            data:{
                start: start
            }, 
            success: function(result){
                $('#contenido').val($('#contenido').val()+result);
                exportToCVS(start+3421,max);
            }
        });
    }

    function descargar_boton(){
        var a = document.createElement('a');
        with (a) {
            href='data:text/csv;base64,' + btoa(document.getElementById('contenido').value);
            download='csvfile.csv';
        }
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }
    </script>
</head>
<body>
    <p id="response">Cargando <span id="numero"></span> / <? echo $maximo; ?> item(s)</p>
    <textarea id="contenido" style="width:100%;" rows="30">NroInterno;Producto;Codigo;Serie;Etiqueta;CCO;Valor;Valor_adicional;Fecha_ingreso;Concepto_ingreso;Nro_factura;Proveedor;Mes_ano_inicio_rev;Mes_ano_inicio_dep;Mes_ano_termino_dep;Totalmente_dep;Fecha_garantia;Fecha_pres_asig;Fecha_pres_asig_compromiso;Tipo_depreciacion;Tipo_adquisicion;Valor_a_depreciar;Cuota_depreciacion;Vida_util;Valor_residual;Fecha_salida;Concepto_salida;Valor_salida;Fecha_mantencion;Mantencion_anterior;Condicion;Disponible;Agrupado;Ubicacion;Responsable;Procesando;Creacion;Modificacion;ProCodigo;Prod.Nombre;ProCodigo_barra;Categoria;Unidad;Stock_minimo;Pro_stock_maximo;Pro_tipo;Pro_valor;Pro_inventario;Pro_mantenimiento;Pro_garantia;Pro_prestable;Pro_asignable;Pro_imagen;Pro_estado;Pro_modificacion;Pro_id_item_carga;Cat_id;Cat_codigo;Cat_padre;Cat_nombre;Cat_nombre_largo;Ubi_id;Ubi_cco_id;Ubi_codigo;Ubi_bodega;Ubi_nombre;Ubi_ultimo_control;Ubi_orden;Ubi_activo;Per_id;Per_tipo;Per_rut;Per_nombre;Per_elim;Cco_id;Cco_codigo;Cco_nombre;Aux_id;Aux_codigo;Aux_rut;Aux_nombre
</textarea>  
</body>
</html>