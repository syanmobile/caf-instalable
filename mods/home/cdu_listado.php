<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Generación de Etiquetas para Activos Fijos";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "cdu_listado.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function crear(){
	var cantidad = prompt("Ingresar cantidad de etiquetas a crear", "");
	if (cantidad != null) {
	   carg("cdu_ajax.php","&modo=crear&cantidad="+cantidad);
	}
}
function imprimir(){
	var a = $(".cdus").fieldSerialize(); 
	window.open("mods/home/cdu_pdf.php?"+a);
}
function desasociar(){
	if(confirm("Quitar estas etiquetas de los activos?")){
		var a = $(".cdus").fieldSerialize();
		AJAXPOST("mods/home/cdu_ajax2.php",a+"&modo=desasociar",document.getElementById("lugare")); 
	}
}
function eliminar(etiqueta){
     if(confirm("Seguro de eliminar esta etiqueta?")){
          $("[fila="+etiqueta+"]").fadeOut("normal");
          SoloEnviar("mods/home/cdu_ajax2.php","modo=eliminar&etiqueta="+etiqueta);
     }
}
</script>

<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td><?php
		construir_boton("crear()","","crear","Crear segun correlativo",4);
        //construir_boton("crear_individual()","","crear","Crear individual",4);
		construir_boton("imprimir()","","imprimir","Imprimir Códigos",4);
		construir_boton("cdu_acf_sin_etiquetas.php","","derecha","Etiquetar Activos",2); 
		construir_boton("desasociar()","","eliminar","Quitar asociación Códigos",4);
		?></td> 
    </tr>
</tbody>
</table> 

<table class="table table-bordered table-condensed table-info" style="margin-bottom:0px !important;">  
<tbody> 
<tr>
    <td width="200">
    <b>Etiqueta:</b><br />
    <input type="text" name="fil_etiqueta" id="fil_etiqueta" class="campos w10" value="<? echo $_REQUEST["fil_etiqueta"]; ?>"></td> 
    <td width="100">
    <b>Código:</b><br />
    <input type="text" name="fil_codigo" id="fil_codigo" class="campos w10" value="<? echo $_REQUEST["fil_codigo"]; ?>"></td> 
    <td width="100">
    <b>Serie:</b><br />
    <input type="text" name="fil_serie" id="fil_serie" class="campos w10" value="<? echo $_REQUEST["fil_serie"]; ?>"></td>
    <td>
    <b>Nombre Producto:</b><br />
    <input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? echo $_REQUEST["fil_descripcion"]; ?>"></td>  
    <td width="1"><?
	construir_boton("cdu_listado.php","","buscar","Filtrar",2); 
	?></td> 
</tr>
</tbody>
</table>

<div class="alert alert-info" style="margin-top: 5px; margin-bottom: 5px; padding: 8px;">
    <b>Última Etiqueta: <? echo _opc("proxima_etiqueta") * 1; ?></b>
</div>

<div id="lugare">
<?php
if($_REQUEST["fil_etiqueta"] <> ""){
	$extras .= "and eti.eti_codigo like '%".$_REQUEST["fil_etiqueta"]."%' ";
}
if($_REQUEST["fil_codigo"] <> ""){
	$extras .= "and pro.pro_codigo = '".$_REQUEST["fil_codigo"]."' ";
}
if($_REQUEST["fil_serie"] <> ""){
	$extras .= "and acf.acf_serie = '".$_REQUEST["fil_serie"]."' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$extras .= "and pro.pro_nombre like '%".$_REQUEST["fil_descripcion"]."%' ";
}
$res = sql_etiquetas("*"," $extras order by eti.eti_codigo asc");
if(mysqli_num_rows($res) > 0){
    ?>
    <table class="table table-striped table-bordered table-condensed"> 
    <thead>
    <tr>
        <th width="1" align="center">&nbsp;</th>
        <th width="1" align="center">Etiqueta</th>
        <th width="1" align="center">Código</th>
        <th width="1" align="center">Serie</th>
        <th>Nombre Producto</th>
        <th width="1" align="center"></th>
    </tr>
    </thead>
    <tbody> 
    <?
    while($row = mysqli_fetch_array($res)){ 
        ?>
        <tr fila="<? echo $row["eti_codigo"]; ?>">
            <td><input type="checkbox" name="cdu[]" value="<? echo $row["eti_codigo"]; ?>" class="campos cdus"></td>
            <th><?php echo $row["eti_codigo"]; ?></th>
            <th><?php echo $row["pro_codigo"]; ?></th>
			<th><?php echo $row["acf_serie"]; ?></th>
			<td><?php echo $row["pro_nombre"]; ?></td>
       		<td><?
			construir_boton("eliminar('".$row["eti_codigo"]."')","","eliminar","Eliminar",4);
			?></td>
        </tr>
        <? 
    }
    ?> 
    </tbody>
    </table>  
    <?php
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin registros creados</strong>
    </div>
    <?php
}
?>
</div>