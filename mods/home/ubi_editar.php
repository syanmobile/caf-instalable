<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Editar Ubicación";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

$res = sql_ubicaciones("*","and ubi.ubi_id = '$_REQUEST[id]'","","");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<strong>No se encontró el dato</strong>
	</div>
    <?php
	construir_boton("ubi_listado.php","","buscar","Listado de Ubicaciones",2);
	exit();
}
?>
<script language="javascript">
function save(){ 
	if(document.getElementById("bodega").value == ""){
		alerta_js("Es obligación seleccionar bodega");
		return;	
	} 
	if(document.getElementById("nombre").value == ""){
		alerta_js("Es obligación ingresar el nombre");
		return;	
	}
	carg("ubi_save.php","");
}
</script> 

<input type="hidden" name="modo" value="editar" class="campos"> 
<input type="hidden" name="id" value="<? echo $_REQUEST["id"]; ?>" class="campos">

<form class="form-horizontal" role="form" method="post"> 
      
    <div class="form-group">
        <label for="orden" class="col-sm-2 control-label">Lugar <span class="oblig">(*)</span></label>
        <div class="col-sm-7">
            <?php input_bodega($datos["ubi_bodega"]); ?>
        </div>
    </div> 
    <div class="form-group">
        <label for="orden" class="col-sm-2 control-label">Código <span class="oblig">(*)</span></label>
        <div class="col-sm-2">
            <input type="text" class="form-control campos" disabled value="<? echo $datos["ubi_codigo"]; ?>">
        	<input type="hidden" name="codigo" class="campos" value="<?php echo $datos["ubi_codigo"]; ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="nombre" class="col-sm-2 control-label">Nombre Ubicación <span class="oblig">(*)</span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control campos" id="nombre" name="nombre" value="<? echo $datos["ubi_nombre"]; ?>">
        </div>
    </div>  
    
    <div class="form-group">
        <div class="col-sm-2"/>
        <div class="col-sm-10">
        	<?php
			construir_boton("","","grabar","Guardar",3);
			construir_boton("ubi_listado.php","","eliminar","Cancelar",2);
			?>
        </div>
    </div>
</form>