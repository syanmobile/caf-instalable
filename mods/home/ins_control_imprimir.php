<?php
require("../../inc/conf_dentro.php");
require("../../_css.php");
?>
<h1>TOMA DE INVENTARIO</h1>
<h2>Lugar: <? echo $_bodegas[$_REQUEST["bodega"]]; ?></h2>
<div class="alert alert-info">
	Listado impreso por <b><? echo $_SESSION["per_conectado"]["per_nombre"]; ?></b> el <? echo date("d/m/Y"); ?> a las <? echo date("H:i"); ?>
</div>
<?
$res = sql_insumos("*"," and ubi_bodega = '$_REQUEST[bodega]' order by pro_grupo asc, pro_subgrupo asc ,pro_nombre asc "); 
if(mysqli_num_rows($res) > 0){
	while($row = mysqli_fetch_array($res)){
		if($_grupos[$row["pro_grupo"]] == ""){
			$row["gru_nombre"] = "* Sin grupo *";
		}else{
			$row["gru_nombre"] = $_grupos[$row["pro_grupo"]];
		}
		if($_subgrupos[$row["pro_subgrupo"]] == ""){
			$row["sgru_nombre"] = "* Sin subgrupo *";
		}else{
			$row["sgru_nombre"] = $_subgrupos[$row["pro_subgrupo"]];
		}
		
		if($row["gru_nombre"] <> $gru){
			?>
			<table class="table table-striped table-bordered tabledrag table-condensed" style="font-size: 14px !important;"> 
			<thead>
			<tr>
				<th colspan="15"><?php echo $row["gru_nombre"]; ?></th>
			</tr>
			</thead>
			<tbody>
			<?
			$gru = $row["gru_nombre"];
			$sgru = "";
		}
		if($row["sgru_nombre"] <> $sgru){ 
			?>
			<tr>
				<th style="background-color:#FFFBD2;" colspan="15"><?php echo $row["sgru_nombre"]; ?></th>
			</tr>
			<tr>   
				<th>Producto</th>
				<th width="220">Ubicación</th>
				<th width="100" style="text-align: center;">Stock</th>
				<th width="100" style="text-align: center;">Conteo</th>
			</tr>
			<?
			$sgru = $row["sgru_nombre"];
		}  
		?>
		<tr>  
			<td><? echo $row["pro_codigo"]; ?> - <? echo $row["pro_nombre"]; ?></td>
			<td><? echo $row["ubi_codigo"]; ?> - <? echo $row["ubi_nombre"]; ?></td>
			<th style="text-align: center;"><? echo _num2($row["total"]); ?></th>
			<th></th>
		</tr>
		<?
	}
	?> 
	</tbody>
	</table>
	<?
}else{
	?>
	<div class="alert alert-danger">
		<strong>Sin registros creados</strong>
	</div>
	<?php 
} 
?>