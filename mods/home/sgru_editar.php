<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Editar SubGrupo";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

$res = sql_subgrupos("*"," and sgru.sgru_id = '$_REQUEST[id]'","","");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<strong>No se encontró el dato</strong>
	</div>
    <?php
	construir_boton("sgru_listado.php","","buscar","Listado SubGrupos",2);
	exit();
}
?>
<script language="javascript">
function save(){ 
    if(document.getElementById("grupo").value == ""){
        alerta_js("Es obligación ingresar el grupo");
        return; 
    }
	if(document.getElementById("nombre").value == ""){
		alerta_js("Es obligación ingresar el nombre");
		return;	
	}
	carg("sgru_save.php","");
}
</script> 

<form class="form-horizontal" role="form" method="post">
	<input type="hidden" name="modo" value="editar" class="campos"> 
	<input type="hidden" name="id" value="<? echo $_REQUEST["id"]; ?>" class="campos">
    <div class="form-group">
        <label for="orden" class="col-sm-2 control-label">Grupo <span class="oblig">(*)</span></label>
        <div class="col-sm-10">
            <select class="form-control campos" id="grupo" name="grupo">
                <option value=""></option>
                <?
                $res = sql_grupos("*"," ORDER BY gru.gru_nombre asc","","");
                if(mysqli_num_rows($res) > 0){
                    while($row = mysqli_fetch_array($res)){
                        ?>
                        <option value="<?php echo $row["gru_id"]; ?>" <?
                        if($row["gru_id"] == $datos["sgru_gru_id"]){ echo "selected"; } 
                        ?>><?php echo $row["gru_nombre"]; ?></option>
                        <? 
                    }
                }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="orden" class="col-sm-2 control-label">Código <span class="oblig">(*)</span></label>
        <div class="col-sm-2">
            <input type="text" class="form-control campos" disabled value="<? echo $datos["sgru_codigo"]; ?>">
        	<input type="hidden" name="codigo" class="campos" value="<?php echo $datos["sgru_codigo"]; ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="nombre" class="col-sm-2 control-label">Nombre SubGrupo <span class="oblig">(*)</span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control campos" id="nombre" name="nombre" value="<? echo $datos["sgru_nombre"]; ?>">
        </div>
    </div>  
    
    <div class="form-group">
        <div class="col-sm-2"/>
        <div class="col-sm-10">
        	<?php
			construir_boton("","","grabar","Guardar",3);
			construir_boton("sgru_listado.php","","eliminar","Cancelar",2);
			?>
        </div>
    </div>
</form> 