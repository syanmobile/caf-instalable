<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabación"; 
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
  
switch($_POST["modo"]){
	case "prestamo":
		$folio = folio_proximo(); // obtengo folio proximo de un doc ajuste
		
		$sql = "INSERT INTO movimientos ( 
			mov_per_id,
			mov_responsable,
			mov_cco_id,
			mov_tipo, 
			mov_concepto,
			mov_bodega,
			mov_folio,
			mov_fecha,  
			mov_extra_1,
			mov_extra_2,
			mov_extra_3,
			mov_extra_4,
			mov_extra_5,
			mov_glosa,
			mov_total 
		) VALUES (  
			'".$_SESSION["per_conectado"]["per_id"]."',
			'".$_POST["responsable"]."',
			'$_POST[centro_costo]',
			'$_POST[tipo]',
			'$_POST[concepto]',
			'".$_ubicaciones_bod_id[$_POST["ubicacion"]]."',
			'$folio', 
			'"._fec($_POST["fecha"],9)."', 
			'$_POST[extra_1]',
			'$_POST[extra_2]',
			'$_POST[extra_3]',
			'$_POST[extra_4]',
			'$_POST[extra_5]',
			'$_POST[glosa]',
			'$_POST[total]' 
		)";  
		$res = mysqli_query($cnx,$sql);
		$movi = mysqli_insert_id($cnx);  
		
		$lineas = explode(",",$_REQUEST["lineas"]);
		for($i = 1;$i < count($lineas);$i++){
			
			$res2 = sql_activos_fijos("*"," and acf_id = '".$lineas[$i]."'"); 
			$activo = mysqli_fetch_array($res2); 
			
			$valor = $activo["acf_valor"] * 1;
			$ubicacion = $activo["acf_ubicacion"];
			$producto = $activo["acf_producto"]; 
			
			$sql3 = "INSERT INTO movimientos_detalle (
				det_mov_id,
				det_ubi_id,
				det_producto,
				det_acf_id,
				det_egreso,
				det_valor,
				det_total
			) VALUES (
				'$movi',
				'$ubicacion', 
				'$producto',
				'".$lineas[$i]."',
				'1',
				'$valor',
				'$valor'
				
			)"; 
			$res3 = mysqli_query($cnx,$sql3);  

			$sql3 = "update activos_fijos set 
			acf_disponible = '2',
			acf_responsable = '".$_POST["responsable"]."',
			acf_fecha_pres_asig = '".date("Y-m-d")."',
			acf_fecha_pres_asig_compromiso = '"._fec($_POST["devolver"],9)."' 
			where acf_id = '".$lineas[$i]."' ";  
			$res3 = mysqli_query($cnx,$sql3);	 
		}
		?>
        <table width="100%">
        <tr valign="top">
            <td width="300">
                <div class="alert alert-success"> 
        			<span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
                    Préstamo guardado con &eacute;xito<br />
                    <b style="font-size:20px;"><? echo $_POST["tipo"]."/".$folio; ?></b>
                </div>
                <?php 
				construir_boton("transacciones.php","","izquierda","Ver historial de transacciones",2); 
				construir_boton("acf_prestamo.php","","izquierda","Realizar otro Préstamo",2);
				?>
            </td>
            <td style="padding-left:15px;">
            	<iframe src="<?php echo "mods/home/aju_pdf.php?id=".$movi; ?>" width="100%" height="400"></iframe>
            </td>
        </tr>
        </table>
		<?php 
		break; 

	case "asignacion":
		$folio = folio_proximo(); // obtengo folio proximo de un doc ajuste
		
		$sql = "INSERT INTO movimientos ( 
			mov_per_id,
			mov_responsable,
			mov_cco_id,
			mov_tipo, 
			mov_concepto,
			mov_bodega,
			mov_folio,
			mov_fecha,  
			mov_extra_1,
			mov_extra_2,
			mov_extra_3,
			mov_extra_4,
			mov_extra_5,
			mov_glosa,
			mov_total 
		) VALUES (  
			'".$_SESSION["per_conectado"]["per_id"]."',
			'".$_POST["responsable"]."',
			'$_POST[centro_costo]',
			'$_POST[tipo]',
			'$_POST[concepto]',
			'".$_ubicaciones_bod_id[$_POST["ubicacion"]]."',
			'$folio', 
			'"._fec($_POST["fecha"],9)."', 
			'$_POST[extra_1]',
			'$_POST[extra_2]',
			'$_POST[extra_3]',
			'$_POST[extra_4]',
			'$_POST[extra_5]',
			'$_POST[glosa]',
			'$_POST[total]' 
		)";  
		$res = mysqli_query($cnx,$sql);
		$movi = mysqli_insert_id($cnx);  
		
		$lineas = explode(",",$_REQUEST["lineas"]);
		for($i = 1;$i < count($lineas);$i++){
			
			$res2 = sql_activos_fijos("*"," and acf_id = '".$lineas[$i]."'"); 
			$activo = mysqli_fetch_array($res2); 
			
			$valor = $activo["acf_valor"] * 1;
			$ubicacion = $activo["acf_ubicacion"];
			$producto = $activo["acf_producto"]; 
			
			$sql3 = "INSERT INTO movimientos_detalle (
				det_mov_id,
				det_ubi_id,
				det_producto,
				det_acf_id,
				det_egreso,
				det_valor,
				det_total
			) VALUES (
				'$movi',
				'$ubicacion', 
				'$producto',
				'".$lineas[$i]."',
				'1',
				'$valor',
				'$valor'
				
			)"; 
			$res3 = mysqli_query($cnx,$sql3);  
			 
			$sql3 = "update activos_fijos set 
			acf_disponible = '6',
			acf_fecha_pres_asig = '".date("Y-m-d")."',
			acf_responsable = '".$_POST["responsable"]."'  
			where acf_id = '".$lineas[$i]."' ";  
			$res3 = mysqli_query($cnx,$sql3); 		
		}
		?> 
        <table width="100%">
        <tr valign="top">
            <td width="300">
                <div class="alert alert-success"> 
        			<span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
                    Asignación guardada con &eacute;xito<br />
                    <b style="font-size:20px;"><? echo $_POST["tipo"]."/".$folio; ?></b>
                </div>
                <?php 
				construir_boton("transacciones.php","","izquierda","Ver historial de transacciones",2); 
				construir_boton("acf_asignacion.php","","izquierda","Realizar otra Asignación",2);
				?>
            </td>
            <td style="padding-left:15px;">
            	<iframe src="<?php echo "mods/home/aju_pdf.php?id=".$movi; ?>" width="100%" height="400"></iframe>
            </td>
        </tr>
        </table>
		<?php 
		break; 
		
	case "baja":
		$folio = folio_proximo(); // obtengo folio proximo de un doc ajuste 
		$sql = "INSERT INTO movimientos ( 
			mov_per_id,
			mov_responsable,
			mov_cco_id,
			mov_tipo, 
			mov_concepto,
			mov_bodega,
			mov_folio,
			mov_fecha,  
			mov_extra_1,
			mov_extra_2,
			mov_extra_3,
			mov_extra_4,
			mov_extra_5,
			mov_glosa,
			mov_total 
		) VALUES (  
			'".$_SESSION["per_conectado"]["per_id"]."',
			'".$_POST["responsable"]."',
			'$_POST[centro_costo]',
			'$_POST[tipo]',
			'$_POST[concepto]',
			'$_POST[bodega]',
			'$folio', 
			'".date("Y-m-d")."', 
			'$_POST[extra_1]',
			'$_POST[extra_2]',
			'$_POST[extra_3]',
			'$_POST[extra_4]',
			'$_POST[extra_5]',
			'$_POST[glosa]',
			'$_POST[valor]' 
		)";  
		$res = mysqli_query($cnx,$sql);
		$movi = mysqli_insert_id($cnx);  
			
		$res2 = sql_activos_fijos("*"," and acf_id = '".$_REQUEST["activo"]."'"); 
		$activo = mysqli_fetch_array($res2); 
		
		$valor = $_REQUEST["valor"] * 1;
		$ubicacion = $activo["acf_ubicacion"];
		$producto = $activo["acf_producto"]; 
		
		$sql3 = "INSERT INTO movimientos_detalle (
			det_mov_id,
			det_ubi_id,
			det_producto,
			det_acf_id,
			det_egreso,
			det_valor,
			det_total
		) VALUES (
			'$movi',
			'$ubicacion', 
			'$producto',
			'".$_REQUEST["activo"]."',
			'1',
			'$valor',
			'$valor'
			
		)"; 
		$res3 = mysqli_query($cnx,$sql3);  
		 
		$sql3 = "update activos_fijos set 
		acf_disponible = '4',
		acf_responsable = '',
		acf_valor_salida = '$_REQUEST[valor]',
		acf_concepto_salida = '$_REQUEST[concepto]',
		acf_fecha_salida = '"._fec($_POST["fecha"],9)."' 
		where acf_id = '".$_REQUEST["activo"]."' ";  
		$res3 = mysqli_query($cnx,$sql3);	

		// elimino depreciaciones proyectadas
		$sql2 = "delete from depreciaciones where dep_periodo >= '".date("Y-m")."-15'";
		$sql2.= "and dep_acf_id = '".$_REQUEST["activo"]."' ";   
		$res2 = mysqli_query($cnx,$sql2);
		$num2 = mysqli_num_rows($res2); 
		?> 
        <table width="100%">
        <tr valign="top">
            <td width="300">
                <div class="alert alert-success"> 
        			<span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
                    BAJA guardada con &eacute;xito<br />
                    <b style="font-size:20px;"><? echo $_POST["tipo"]."/".$folio; ?></b>
                </div>
                <?php 
				construir_boton("transacciones.php","","izquierda","Ver historial de transacciones",2);  
				construir_boton("acf_baja.php","","izquierda","Crear otra Baja de Activo",2);
				?>
            </td>
            <td style="padding-left:15px;">
            	<iframe src="<?php echo "mods/home/aju_pdf.php?id=".$movi; ?>" width="100%" height="400"></iframe>
            </td>
        </tr>
        </table>
		<?php 
		break;
		
	case "devolucion": 
		$folio = folio_proximo(); // obtengo folio proximo de un doc ajuste 
		
		$sql = "INSERT INTO movimientos ( 
			mov_per_id,
			mov_responsable,
			mov_cco_id,
			mov_tipo, 
			mov_concepto,
			mov_bodega,  
			mov_folio,
			mov_fecha, 
			mov_extra_1,
			mov_extra_2,
			mov_extra_3,
			mov_extra_4,
			mov_extra_5,
			mov_glosa 
		) VALUES (  
			'".$_SESSION["per_conectado"]["per_id"]."',
			'".$_POST["respo"]."',
			'$_POST[centro_costo]',
			'$_POST[tipo]',
			'$_POST[concepto]',
			'".$_ubicaciones_bod_id[$_POST["ubicacion"]]."',
			'$folio', 
			'".date("Y-m-d")."',                        
			'$_POST[extra_1]',
			'$_POST[extra_2]',
			'$_POST[extra_3]',
			'$_POST[extra_4]',
			'$_POST[extra_5]',
			'$_POST[glosa]'                     
		)"; 
		$res = mysqli_query($cnx,$sql); 
		$movi = mysqli_insert_id($cnx);  
		
		$activos = $_REQUEST["activos"];                                                                                                                                                                                                                                                                                                                         
		$notas = $_REQUEST["notas"];
		$disponible = $_REQUEST["disponible"];
		
		for($i = 0;$i < count($activos);$i++){
			
			$res2 = sql_activos_fijos("*"," and acf_id = '".$activos[$i]."'"); 
			$activo = mysqli_fetch_array($res2); 
			
			$valor = $activo["acf_valor"] * 1;
			$ubicacion = $activo["acf_ubicacion"];
			$producto = $activo["acf_producto"]; 
			
			$sql3 = "INSERT INTO movimientos_detalle (
				det_mov_id,
				det_ubi_id,
				det_producto,
				det_acf_id,
				det_ingreso,
				det_valor,
				det_total, 
				det_notas
			) VALUES (
				'$movi',
				'$ubicacion', 
				'$producto',
				'".$activos[$i]."',
				'1',
				'$valor',
				'$valor' ,
				'".$notas[$i]."' 
			)"; 
			$res3 = mysqli_query($cnx,$sql3); 
						
			$sql3 = "update activos_fijos set 
			acf_disponible = '".$disponible[$i]."', 
			acf_responsable = '',
			acf_fecha_pres_asig = '',
			acf_fecha_pres_asig_compromiso = ''  
			where acf_id = '".$activos[$i]."'  ";
			$res3 = mysqli_query($cnx,$sql3);	 
			
			$total+= $valor;
		}
		$sql3 = "update movimientos set mov_total = '$total' where mov_id = '$movi' ";  
		$res3 = mysqli_query($cnx,$sql3);
		?> 
        <table width="100%">
        <tr valign="top">
            <td width="300">
                <div class="alert alert-success"> 
        			<span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
                    Devolución guardada con &eacute;xito<br />
                    <b style="font-size:20px;"><? echo $_POST["tipo"]."/".$folio; ?></b>
                </div>
                <?php 
				construir_boton("transacciones.php","","izquierda","Ver historial de transacciones",2); 
				construir_boton("acf_devolucion.php","","izquierda","Realizar Otra Devolución",2);
				?>
            </td>
            <td style="padding-left:15px;">
            	<iframe src="<?php echo "mods/home/aju_pdf.php?id=".$movi; ?>" width="100%" height="400"></iframe>
            </td>
        </tr>
        </table>
		<?php 
		break; 

	case "reubicacion":
		$folio = folio_proximo(); // obtengo folio proximo de un doc ajuste
		
		$sql = "INSERT INTO movimientos ( 
			mov_per_id,
			mov_responsable,
			mov_cco_id,
			mov_tipo, 
			mov_concepto,
			mov_bodega,
			mov_folio,
			mov_fecha,  
			mov_extra_1,
			mov_extra_2,
			mov_extra_3,
			mov_extra_4,
			mov_extra_5,
			mov_glosa 
		) VALUES (  
			'".$_SESSION["per_conectado"]["per_id"]."',
			'".$_POST["responsable"]."',
			'$_POST[centro_costo]',
			'$_POST[tipo]',
			'$_POST[concepto]',
			'".$_ubicaciones_bod_id[$_POST["ubicacion"]]."',
			'$folio', 
			'"._fec($_POST["fecha"],9)."', 
			'$_POST[extra_1]',
			'$_POST[extra_2]',
			'$_POST[extra_3]',
			'$_POST[extra_4]',
			'$_POST[extra_5]',
			'$_POST[glosa]'  
		)";  
		$res = mysqli_query($cnx,$sql);
		$movi = mysqli_insert_id($cnx); 

		$activos = $_POST["activo"];
		foreach($activos as $acti){
			$res2 = sql_activos_fijos("*"," and acf_id = '$acti'"); 
			$activo = mysqli_fetch_array($res2); 

			$ubicacion = $activo["acf_ubicacion"];
			$producto = $activo["acf_producto"]; 
			
			$sql3 = "INSERT INTO movimientos_detalle (
				det_mov_id,
				det_ubi_id,
				det_producto,
				det_acf_id,
				det_valor,
				det_total,
				det_egreso 
			) VALUES (
				'$movi',
				'$ubicacion', 
				'$producto',
				'$acti',
				'$valor',
				'$valor' ,
				1  
			)"; 
			$res3 = mysqli_query($cnx,$sql3); 
			
			$sql3 = "INSERT INTO movimientos_detalle (
				det_mov_id,
				det_ubi_id,
				det_producto,
				det_acf_id,
				det_valor,
				det_total,
				det_ingreso 
			) VALUES (
				'$movi',
				'$_REQUEST[destino]', 
				'$producto',
				'$acti',
				'$valor',
				'$valor' ,
				1  
			)"; 
			$res3 = mysqli_query($cnx,$sql3); 
						
			$sql3 = "update activos_fijos set acf_ubicacion = '".$_REQUEST["destino"]."' where acf_id = '$acti'  ";
			$res3 = mysqli_query($cnx,$sql3);
		}
		?>
        <table width="100%">
        <tr valign="top">
            <td width="300">
                <div class="alert alert-success"> 
        			<span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
                    Activos reubicados de <b><? echo $_bodegas[$_POST["bodega"]]; ?></b><br>
                    a la ubicación <b><? echo $_ubicaciones[$_POST["destino"]]; ?></b>
                </div>
                <?php 
				construir_boton("transacciones.php","","izquierda","Ver historial de transacciones",2); 
				construir_boton("acf_reubicacion.php","","izquierda","Realizar Otra Reubicación",2);
				?>
            </td>
            <td style="padding-left:15px;">
            	<iframe src="<?php echo "mods/home/aju_pdf.php?id=".$movi; ?>" width="100%" height="400"></iframe>
            </td>
        </tr>
        </table>
		<?php 
		break;
	
	case "mantenimiento":
		?>
        <table width="100%">
        <tr valign="top">
            <td width="300">
                <div class="alert alert-success"> 
        			<span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
                    Productos marcados como "En Mantenimiento" con &eacute;xito<br />
                    Para volver a dejar disponibles, debe preferir 
                </div>
                <?php 
				construir_boton("transacciones.php","","izquierda","Volver a Documentos de Movimientos",2);  
				?>
            </td>
            <td style="padding-left:15px;">
            	<iframe src="<?php echo "mods/home/aju_pdf.php?id=".$movi; ?>" width="100%" height="400"></iframe>
            </td>
        </tr>
        </table>
		<?php 
		break;  
}
?>