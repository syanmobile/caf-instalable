<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Devolución de Activos Fijos";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "acf_devolucion.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function save(){ 
	if(document.getElementById("glosa").value == ""){
		alerta_js("Es obligación ingresar NOTAS");
		return;	
	}
	var a = $(".produ").fieldSerialize();
	if(a == ""){
		alerta_js("Debe elegir algún Activo Fijo a devolver");
		return;
	}
	if(confirm("Grabar Documento?")){
		carg("acf_save.php","");
	}
}
function comenzar(bodega,resp,resp_nom){
	document.getElementById("respo").value = resp;
	document.getElementById("respo_nom").value = resp_nom; 
	document.getElementById("bodega").value = bodega;
	$("#inicio").hide("fast");
	$("#cabecera").show("fast");
	
	var div = document.getElementById("pendientes"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/acf_devolucion_ajax.php",a+"&modo=pendientes&bodega="+bodega,div);
}
function cancelar(id){
	$("#lugar_"+id).hide();
	$("#btnes_"+id).show();
	$("#operacion_"+id).html("");
}
function devolver(acf,id){
	$("#lugar_"+id).show();
	$("#btnes_"+id).hide();
	var div = document.getElementById("operacion_"+id); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/acf_devolucion_ajax.php",a+"&modo=devolver&acf="+acf+"&id="+id,div);
}
function extravio(acf,id){
	$("#lugar_"+id).show();
	$("#btnes_"+id).hide();
	var div = document.getElementById("operacion_"+id); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/acf_devolucion_ajax.php",a+"&modo=extravio&acf="+acf+"&id="+id,div);
}
</script>

<table style="margin-bottom:5px; width:100%">
<tr valign="top"> 
    <td width="400">
    	<?php
		$conceptos = unserialize($_configuracion["activos_conceptos"]);
		?>
    	<input type="hidden" name="modo" value="devolucion" class="campos" />
    	<input type="hidden" name="tipo" id="tipo" value="A" class="campos" />
    	<input type="hidden" name="concepto" value="4" class="campos"> 
    	<input type="hidden" name="bodega" id="bodega" value="" class="campos"> 
        
        <div id="inicio">
		<table class="table table-striped table-bordered table-condensed"> 
        <thead>
            <tr>
                <th colspan="2">Trabajadores con Activos</th>
                <th width="1">Activos</th>
                <th width="1"></th>
            </tr>
        </thead>
        <tbody>
        	<?php
			$res = sql_activos_fijos("*,count(*) as stock"," and (acf_disponible = 2 or acf_disponible = 6) and acf_responsable <> 0 group by acf.acf_responsable,ubi_bodega order by per_nombre asc"); 
			if(mysqli_num_rows($res) > 0){
				while($row = mysqli_fetch_array($res)){ 
					?>
                    <tr>
                        <td width="1"><? 
							$imagen = ($row["per_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$row["per_imagen"];
							?><img src="<?php echo $imagen; ?>" style="width:50px !important;">
						</td> 
                        <td>
                        	<b><?php echo $_personas[$row["acf_responsable"]]; ?></b><br>
                        	<? echo $row["per_rut"]; ?><br>
                        	Lugar: <?php echo $_bodegas[$row["ubi_bodega"]]; ?>
                        </td>
                        <td style="text-align: center; padding-top: 5px;">
                        	<span class="label label-info" style=" font-size: 20px !important;"><? echo $row["stock"]; ?></span>
                        </td>
                        <td><?
						construir_boton("comenzar('".$row["ubi_bodega"]."','".$row["acf_responsable"]."','".$_personas[$row["acf_responsable"]]."')","1","derecha","Devolver activos",4); 
						?></td>
                    </tr>
                    <?
				}
			}else{
				?>
                <tr>
                	<td colspan="10">
                	<div class="alert alert-success">
                		<h4>Sin devoluciones pendientes</h4>
                	</div>
                	</td>
                </tr>
                <?
			}
			?> 
        </tbody>
		</table> 
		</div>
        
        <div id="cabecera" style="display:none;"> 
        <table class="table table-striped table-bordered table-condensed"> 
        <thead>
            <tr>
                <th colspan="2">Información Movimiento</th>
            </tr>
        </thead>
        <tbody>   
            <tr> 
				<th width="100" style="text-align:right">Responsable:</th>
				<td>
				<input type="text" readonly id="respo_nom" class="w10 campos">
				<input type="hidden" name="respo" id="respo" class="campos">
				</td>   
			</tr>  
            <tr> 
                <th style="text-align:right">Notas:</th>
				<td><input type="text" name="glosa" id="glosa" class="campos w10" value="Devolución de Activos"></td>
            </tr>
            <?php 
			for($i = 1;$i <= 5;$i++){
				if(_opc("extra_mov_".$i) > 0){
					$res = sql_campos("*"," and cam_id = "._opc("extra_mov_".$i));  
					$row = mysqli_fetch_array($res); 
					?> 
					<tr> 
						<th style="text-align: right;"><? echo $row["cam_nombre"]; ?>:</th>
						<td><? 
			            campo_dinamico($row["cam_tipo"],"extra_".$i,$row["cam_opciones"],"",$url_base);
			            ?></td> 
					</tr> 
					<?php
				}
			} 
			?> 
        </tbody>
        </table>
        <?php 
        construir_boton("","","grabar","Guardar Movimiento",3);  
        construir_boton("acf_devolucion.php","","izquierda","Atrás",2); 
		?>
        </div>	
    </td> 
	<td style="padding-left:10px;" id="pendientes">
   		<?php
		$res = sql_activos_fijos("*"," and (acf_disponible = 2 or acf_disponible = 6) and acf_responsable <> 0 
			order by acf.acf_producto asc, acf.acf_serie asc"); 
		if(mysqli_num_rows($res) > 0){
			?> 
			<table class="table table-striped table-bordered tabledrag table-condensed" id="tablita"> 
			<thead>
				<tr>    
                    <th>Imagen</th>
					<th>Activo Fijo</th>   
                    <th>Ult.Movimiento</th>   
                    <th>Compromiso</th> 
                    <th>Elegir</th>
				</tr>
			</thead>
			<tbody> 
			<?
			while($row = mysqli_fetch_array($res)){
				?>
				<tr>  
					<td><? 
					$imagen = ($row["pro_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$row["pro_imagen"];
					?><img src="<?php echo $imagen; ?>" style="width:80px !important;"></td> 
					<td>
						<b><? echo $row["pro_nombre"]; ?></b><br />
						<b>Codigo</b>: <? echo $row["pro_codigo"]; ?><br>
						<b>Serie</b>: <? echo $row["acf_serie"]; ?><br>
						<b>Ubicacion</b>: <?php echo $row["ubi_nombre"]; ?><br>
						<b>Lugar</b>: <?php echo $_bodegas[$row["ubi_bodega"]]; ?>
					</td>   
					<td>
						<?php
						$sql2 = "SELECT * FROM movimientos_detalle a 
						left join movimientos b on a.det_mov_id = b.mov_id 
						left join personas c on b.mov_per_id = c.per_id 
						where a.det_producto = '$row[acf_producto]' and a.det_lote = '$row[acf_serie]' 
						order by a.det_mov_id desc limit 0,1 ";
						$res2 = mysqli_query($cnx,$sql2);
						$row2 = mysqli_fetch_array($res2);
						?>
						<b>Folio</b>: <? echo $row2["mov_folio"]; ?><br>
						<b>Realizado:</b> <?php echo _fec($row2["mov_fecha"],5); ?>, <?php echo substr($row2["mov_log"],11,5); ?><br>
						<b>Autor:</b> <? echo $_personas[$row2["mov_per_id"]]; ?><br>
						<b>Responsable:</b> <?php echo $_personas[$row["acf_responsable"]]; ?>
					</td>
					<td>
						<?php
						if($row["acf_disponible"] == 2){
							echo "<b>Estado:</b> Prestado<br>";
							echo "<b>Compromiso de Entrega:</b> "._fec($row["acf_fecha_pres_asig_compromiso"],5)."<br>";
							$dias = dias_transcurridos($row["acf_fecha_pres_asig_compromiso"],date("Y-m-d")); 
			                if($dias < 0){
			                    ?>
			                    <span class="label label-danger"><? echo ($dias * -1); ?> días de ATRASO</span>
			                    <?
			                }else{
			                    ?>
			                    <span class="label label-success">Debe entregar en <? echo $dias; ?> días</span>
			                    <?
			                } 
						}else{
							echo "<b>Estado:</b> Asignado<br>";
		                    ?>
		                    <span class="label label-info">No tiene plazo de entrega</span>
		                    <?
						}
						?>
					</td>
					<td><?
					construir_boton("comenzar('".$row["ubi_bodega"]."','".$row["acf_responsable"]."','".$_personas[$row["acf_responsable"]]."')","1","derecha","Devolver activos",4); 
					?></td>
				</tr>
				<? 
			}
			?>
			</tbody>
			</table> 
			
			<script language="javascript">
            $(document).ready(function() { 
                $('#tablita').DataTable({ 
					"columns": [   
						{ "width": "1px" },   
						null ,  
						null ,  
						null  ,  
						{ "width": "1px" } 
					  ],
					 "paging":   false,
					"info":     false,
					"lengthMenu": [100000],
					"oLanguage": {
						"sSearch": "Busque un activo y presione la flecha para agregar: "
					}
				});
            } );   
            </script> 
			<?  
		} 
		?>
    </td>
</tr>
</table> 