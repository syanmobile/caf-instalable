<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Importar Clasificación"; 
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

construir_boton("cat_listado.php","","izquierda","Volver",2);
?> 
<script language="javascript">
$(document).ready(function() {
	$('#archivo').on('change',function(){ 
		$('#multiple_upload_form').ajaxForm({
			target:'#procesando_upload',
			beforeSubmit:function(e){
				$('.uploading').show();
			},
			success:function(e){
				$('.uploading').hide();
			},
			error:function(e){
			}
		}).submit(); 
	});
});
function vista(valor){
	if(valor == ""){
		$(".fila_rojo").show();
		$(".fila_ok").show();
	}else{
		if(valor == "rojo"){
			$(".fila_rojo").show();
			$(".fila_ok").hide();
		}else{
			$(".fila_rojo").hide();
			$(".fila_ok").show();
		}
	}
}
</script>  
<form method="post" name="multiple_upload_form" id="multiple_upload_form" 
enctype="multipart/form-data"
action="<?php echo $url_web; ?>mods/home/cat_importar_validacion.php"> 
	<table width="100%">
	<tr valign="top">
		<th width="1" style="text-align:right; padding-right:10px;">
            Subir un documento:<br />
            <input type="file" name="archivo" id="archivo">
            <br />
            <br />
            <?php 
			construir_boton("grabar();","","grabar","Importar Datos",4); // funcion propia icono y texto
			?>
		</th>
		<td style="border-left:2px solid #CCC; padding-left:15px;">  
			<div class="uploading none" style="display:none;">
				<label>&nbsp;</label>
				<img src="img/uploading.gif"/>
			</div>
			<div id="eliminando"></div> 
			<div id="procesando_upload">
            <script language="javascript">			
			function grabar(){
				alert("Cargue los datos");
			}
			</script>
            </div>
		</td>
	</tr>
	</table>
</form> 

<br />
<table class="table table-bordered table-hover table-condensed">  
<tr>
	<td width="20"><img src="img/icons/table.png" /></td> 
            
    <td><strong>Formato del archivo a Importar:</strong><br /><a href="javascript:visor('formatos.php?formato=clasificacion');">Presione aqui para Visualizar</a></td>
</tr>
</table>