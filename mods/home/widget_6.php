<div class="col-md-<? echo $ancho; ?> grid-item">
	<div class="panel panel-primary">  
		<div class="panel-heading">
			<b>Situación de sus Activos</b>
		</div>
		<div class="panel-body"> 
			<?
			$res = sql_activos_fijos("acf_disponible,count(*) as stock"," and pro_tipo <> 'INS' group by acf_disponible ");  
			if(mysqli_num_rows($res) == 0){ 
				?>
				<div class="alert alert-danger" style="margin:20px;">
					<strong>Sin activos fijos</strong>
				</div>
				<?php 
				exit();
			}else{
				while($row = mysqli_fetch_array($res)){ 
					switch($row["acf_disponible"]){
						case 0: $no_disponibles += $row["stock"]; break;
						case 1: $disponibles += $row["stock"]; break;
						case 2: $prestadas += $row["stock"]; break;
						case 3: $mantenimiento+= $row["stock"]; break;
						case 4: $baja+= $row["stock"];  break;
						case 5: $r_mantenimiento+= $row["stock"]; break; 
						case 6: $asignadas+= $row["stock"]; break;
					}
					$productos+= $row["stock"]; 
				}  
			}
			$prestadas_ = round(_porc($prestadas,$productos),2);
			$disponibles_ = round(_porc($disponibles,$productos),2);
			$no_disponibles_ = round(_porc($no_disponibles,$productos),2);
			$mantenimiento_ = round(_porc($mantenimiento,$productos),2);
			$baja_ = round(_porc($baja,$productos),2);
			$r_mantenimiento_ = round(_porc($r_mantenimiento,$productos),2); 
			$asignadas_ = round(_porc($asignadas,$productos),2);

			if($prestadas_ > 0){
				$labeles .= ";Prestadas";
				$valores .= ";".$prestadas_;
			}
			if($disponibles_ > 0){
				$labeles .= ";Disponibles";
				$valores .= ";".$disponibles_;
			}
			if($mantenimiento_ > 0){
				$labeles .= ";Mantenimiento";
				$valores .= ";".$mantenimiento_;
			}
			if($no_disponibles_ > 0){
				$labeles .= ";No Disponibles";
				$valores .= ";".$no_disponibles_;
			}
			if($asignadas_ > 0){
				$labeles .= ";Asignadas";
				$valores .= ";".$asignadas_;
			}
			?> 
			<iframe src="mods/home/widget_grafico_donas.php?label=<? echo $labeles; ?>&valor=<? echo $valores; ?>" frameborder="0" scrolling="no" style="width: 100%; height: 190px;"></iframe> 	

			<table class="table table-condensed table-bordered">
			<thead>
			<tr>
				<th width="1">Activos</th> 
				<th>Situación</th>
				<th width="1">%</th> 
			</tr>
			</thead>
			<tbody>
			<?
			if($prestadas > 0){
				?>
				<tr>
					<th style="text-align: center;"><? echo _num2($prestadas); ?></th>
					<td>Prestadas</td>
					<td style="text-align: center;"><? echo $prestadas_; ?>%</td>
				</tr>
				<? 
			}
			if($disponibles > 0){
				?>
				<tr>
					<th style="text-align: center;"><? echo _num2($disponibles); ?></th>
					<td>Disponibles</td>
					<td style="text-align: center;"><? echo $disponibles_; ?>%</td>
				</tr>
				<? 
			}
			if($no_disponibles > 0){
				?>
				<tr>
					<th style="text-align: center;"><? echo _num2($no_disponibles); ?></th>
					<td>No disponibles</td>
					<td style="text-align: center;"><? echo $no_disponibles_; ?>%</td>
				</tr>
				<? 
			}
			if($mantenimiento > 0){
				?>
				<tr>
					<th style="text-align: center;"><? echo _num2($mantenimiento); ?></th>
					<td>Mantenimiento</td>
					<td style="text-align: center;"><? echo $mantenimiento_; ?>%</td>
				</tr>
				<? 
			}
			if($baja > 0){
				?>
				<tr>
					<th style="text-align: center;"><? echo _num2($baja); ?></th>
					<td>Baja</td>
					<td style="text-align: center;"><? echo $baja_; ?>%</td>
				</tr>
				<? 
			}
			if($r_mantenimiento > 0){
				?>
				<tr>
					<th style="text-align: center;"><? echo _num2($r_mantenimiento); ?></th>
					<td>Requiere mantenimiento</td>
					<td style="text-align: center;"><? echo $r_mantenimiento_; ?>%</td>
				</tr>
				<? 
			}
			if($asignadas > 0){
				?>
				<tr>
					<th style="text-align: center;"><? echo _num2($asignadas); ?></th>
					<td>Asignadas</td>
					<td style="text-align: center;"><? echo $asignadas_; ?>%</td>
				</tr>
				<? 
			} 
			?>
			</tbody>
			</table>
		</div>
	</div>
</div>