<?php 
require("../../inc/conf_dentro.php");
			
construir_boton("vista('');","","buscar","Ver Todo",4);
construir_boton("vista('rojo');","","buscar","Fallas",4);
construir_boton("vista('ok');","","buscar","Registros OK",4);
?>
<table class="table table-bordered table-hover table-condensed"> 
<thead> 
    <tr>
        <th width="1">#</th>
        <th>Etiqueta</th>
        <th>Serie</th>
        <th>Cod.Producto</th>
        <th>Fecha Ingreso</th>
        <th>Monto</th>
        <th>Cod.Proveedor</th>
        <th>Folio</th>
        <th>Cod.CCosto</th>
        <th>Cod.Ubicacion</th>
        <th>Fecha Inicio Depreciación</th>
        <th>Valor Residual</th>
        <th>Validación</th>
    </tr>
</thead>
<tbody>
<?php
$tipo_dep = _opc("tipo_depreciacion");
$codigos = array();

$capt = date("YmdHis");
if (isset($_FILES["archivo"]) && is_uploaded_file($_FILES['archivo']['tmp_name'])) {							
    $fp = fopen($_FILES['archivo']['tmp_name'], "r");
    while (!feof($fp)){ 
    	$contenido = fgets($fp);
		$nlinea++;
		if($nlinea > 1){  
			$contenidos[] = $contenido;	
			$data = explode(";", $contenido); 

			if(count($data) > 1){ 	 
				$producto = rtrim($data[2]); 
				$proveedor = rtrim($data[5]); 

				if(!in_array($producto,$productos_consultados)){
					$productos_consultados[] = $producto;
					if($where_productos <> ""){ $where_productos .= ","; }
					$where_productos .= "'".$producto."'";
				}
				if(!in_array($proveedor,$proveedor_consultados)){
					$proveedor_consultados[] = $proveedor;
					if($where_proveedor <> ""){ $where_proveedor .= ","; }
					$where_proveedor .= "'".$proveedor."'";
				} 
			}
		}
	}
}
// Optimizo el buscar Productos existentes...................
$sql = "SELECT pro_codigo FROM productos where pro_codigo in ( $where_productos )";
///echo $sql."<br>";
$respro = mysqli_query($cnx,$sql); 
$querys++; 
if(mysqli_num_rows($respro) > 0){
	while($rowpro = mysqli_fetch_array($respro)){
		$productos_que_existen[] = $rowpro["pro_codigo"];
	}
}
// Optimizo el buscar Auxiliares existentes...................
$sql = "SELECT aux_codigo FROM auxiliares where aux_codigo in ( $where_proveedor )";
//echo $sql."<br>";
$respro = mysqli_query($cnx,$sql); 
$querys++; 
if(mysqli_num_rows($respro) > 0){
	while($rowpro = mysqli_fetch_array($respro)){
		$proveedores_que_existen[] = $rowpro["aux_codigo"];
	}
} 

// Recorro nuevamente los datos.... y ahora si valido..
foreach($contenidos as $contenido){
	$data = explode(";", $contenido); 

	$etiqueta = rtrim($data[0]);
	$serie = rtrim($data[1]);
	$producto = rtrim($data[2]); 
	$fecha = rtrim($data[3]);
	$monto = rtrim($data[4]);
	$proveedor = rtrim($data[5]);
	$folio = rtrim($data[6]);
	$cco = rtrim($data[7]); 
	$ubicacion = rtrim($data[8]);
	$fecha_ini_dep = rtrim($data[9]);
	$valor_residual = rtrim($data[10]); 
	$notas = rtrim($data[11]);  

	$total_campos = 12; 

	//Validaciones
	$errores = ""; 
	if($producto == "" || $ubicacion == ""){ 
		$errores.= "<li>Campos obligatorio no ingresados: ";
		if($producto == ""){ $errores .= "<br>- Cod.Producto"; }
		if($ubicacion == ""){ $errores .= "<br>- Cod.Ubicación"; }
		$errores.= "</li>";
	} 
	if(!in_array($producto,$productos_que_existen)){
		$errores.= "<li>Producto no existe</li>"; 
	}
	if($proveedor <> ""){ 
		if(!in_array($proveedor,$proveedores_que_existen)){
			$errores.= "<li>Proveedor no existe</li>";  
		}
	} 
	if($_centro_costos_cod[$cco] == ""){ 
		$errores.= "<li>CCO Desconocido</li>"; 
	} 
	if($_ubicaciones_ubi_id[$ubicacion] == ""){ 
		$errores.= "<li>Ubicacion Desconocida</li>"; 
	}
	if(!preg_match("/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/", $fecha)){
		$errores.= "<li>Fecha Ingreso debe ser YYYY-mm-dd</li>";
	}
	if(!preg_match("/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/", $fecha_ini_dep)){
		$errores.= "<li>Fecha Inicio Dep. debe ser YYYY-mm-dd</li>";
	}
	$linea++;
	?>
	<tr class="fila_<?php echo ($errores <> "")?"rojo":"ok"; ?>">
		<td><? echo $linea; ?></td> 
		<?
		for($i = 0;$i < $total_campos;$i++){
			?><td><? echo $data[$i]; ?></td><?
		}

		if($errores <> ""){
			$fallo = "S";
			?><td style="padding-left:15px;" class="alert alert-danger"><? echo $errores; ?></td><?php
		}else{
			?><td style="padding-left:15px;"  class="alert alert-success"><li>Todo OK</li></td><?php
		}
		?>
	</tr>
	<?  
} 
?>
</tbody>
</table>

<?
echo "QUERYS: ".$querys;
?>

<script language="javascript"> 
function grabar(){
    <?php
    if($fallo == ""){
        ?>
        document.multiple_upload_form.action = "<?php echo $url_web; ?>mods/home/acf_importar_grabacion.php";
        $('#multiple_upload_form').ajaxForm({
            target:'#procesando_upload',
            beforeSubmit:function(e){
                $('.uploading').show();
            },
            success:function(e){
                $('.uploading').hide();
            },
            error:function(e){
            }
        }).submit();
        <?
    }else{
        ?>
		alert("Deben estar todos los datos validados");
        <?
    }
    ?>
}
