<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabación"; 
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
 
switch($_POST["modo"]){   
		
	case "entrega":  
		$folio = folio_proximo(); // obtengo folio proximo de un doc ajuste
		
		$sql = "INSERT INTO movimientos ( 
			mov_per_id, 
			mov_tipo, 
			mov_concepto,
			mov_responsable,
			mov_cco_id,
			mov_bodega, 
			mov_folio,
			mov_fecha,
			mov_auxiliar, 
			mov_extra_1,
			mov_extra_2,
			mov_extra_3,
			mov_extra_4,
			mov_extra_5,
			mov_glosa 
		) VALUES (  
			'".$_SESSION["per_conectado"]["per_id"]."', 
			'$_POST[tipo]',
			'$_POST[concepto]',
			'$_POST[responsable]',
			'$_POST[centro_costo]',
			'$_POST[bodega]', 
			'$folio',
			'"._fec($_POST["fecha"],9)."',
			'$_POST[auxiliar]', 
			'$_POST[extra_1]',
			'$_POST[extra_2]',
			'$_POST[extra_3]',
			'$_POST[extra_4]',
			'$_POST[extra_5]',
			'$_POST[glosa]' 
		)"; 
		$res = mysqli_query($cnx,$sql);
		$movi = mysqli_insert_id($cnx);   
		
		$productos = explode(";",$_REQUEST["productos"]);
		$cantidades = explode(";",$_REQUEST["cantidades"]); 
		
		for($i = 0;$i < count($productos);$i++){ 
			
			$cantidades[$i] = $cantidades[$i] * 1;
			$info = explode("___",$productos[$i]);
			
			$resp = sql_productos("*","and pro.pro_id = '".$info[0]."'");  
			if(mysqli_num_rows($resp) > 0){
				$rowp = mysqli_fetch_array($resp);
				$costo = $rowp["pro_valor"] * 1;
			}
			
			if($cantidades[$i] > 0){
				$subtotal = $cantidades[$i] * $costo;
				$sql3 = "INSERT INTO movimientos_detalle (
					det_mov_id,
					det_ubi_id,
					det_producto, 
					det_egreso,
					det_valor,
					det_total
				) VALUES (
					'$movi',
					'".$info[1]."', 
					'".$info[0]."',
					'".$cantidades[$i]."',
					'$costo',
					'$subtotal'

				)"; 
				$res3 = mysqli_query($cnx,$sql3);   							
				$final += $subtotal;
			}
		}
		
		$sql4 = "update movimientos set mov_total = '".$final."' where mov_id = '$movi' ";
		$res4 = mysqli_query($cnx,$sql4);
		?> 
        <table width="100%">
        <tr valign="top">
            <td width="300">
                <div class="alert alert-success"> 
        			<span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
                    Reubicación guardado con &eacute;xito<br />
                    <b style="font-size:20px;"><? echo $_POST["tipo"]."/".$folio; ?></b>
                </div>
                <?php 
				construir_boton("transacciones.php","","izquierda","Ir a Transacciones",2);  
				?>
            </td>
            <td style="padding-left:15px;">
            	<iframe src="<?php echo "mods/home/aju_pdf.php?id=".$movi; ?>" width="100%" height="400"></iframe>
            </td>
        </tr>
        </table>
		<?php 
		break; 

	case "toma_inventario": 
		$folio = folio_proximo(); // obtengo folio proximo de un doc ajuste
		
		$sql = "INSERT INTO movimientos ( 
			mov_per_id, 
			mov_tipo, 
			mov_concepto,
			mov_responsable,
			mov_cco_id,
			mov_bodega, 
			mov_folio,
			mov_fecha,
			mov_auxiliar, 
			mov_extra_1,
			mov_extra_2,
			mov_extra_3,
			mov_extra_4,
			mov_extra_5,
			mov_glosa 
		) VALUES (  
			'".$_SESSION["per_conectado"]["per_id"]."', 
			'$_POST[tipo]',
			'$_POST[concepto]',
			'$_POST[responsable]',
			'$_POST[centro_costo]',
			'$_POST[bodega]', 
			'$folio',
			'"._fec($_POST["fecha"],9)."',
			'$_POST[auxiliar]', 
			'$_POST[extra_1]',
			'$_POST[extra_2]',
			'$_POST[extra_3]',
			'$_POST[extra_4]',
			'$_POST[extra_5]',
			'$_POST[glosa]' 
		)"; 
		$res = mysqli_query($cnx,$sql);
		$movi = mysqli_insert_id($cnx);   
		
		$ajustes = explode(";",$_REQUEST["ajuste_lineas"]); 
		
		for($i = 0;$i < count($ajustes);$i++){ 
			
			$info = explode(",",$ajustes[$i]);
			
			$resp = sql_productos("*","and pro.pro_id = '".$info[0]."'");  
			if(mysqli_num_rows($resp) > 0){
				$rowp = mysqli_fetch_array($resp);
				$costo = $rowp["pro_valor"] * 1;
			}
			
			$info[2] = $info[2] * 1; 
			if($info[2] <> 0){
				if($info[2] > 0){
					$ingreso = $info[2];
					$egreso = 0;
					$subtotal = $info[2] * $costo;
				}else{
					$ingreso = 0;
					$egreso = $info[2] * -1;
					$subtotal = $info[2] * $costo;
				}
				$sql3 = "INSERT INTO movimientos_detalle (
					det_mov_id,
					det_ubi_id,
					det_producto, 
					det_egreso,
					det_ingreso,
					det_valor,
					det_total
				) VALUES (
					'$movi',
					'".$info[1]."', 
					'".$info[0]."',
					'".$egreso."',
					'".$ingreso."',
					'$costo',
					'$subtotal'

				)"; 
				$res3 = mysqli_query($cnx,$sql3);   							
				$final += $subtotal; 
			}
		}
		
		$sql4 = "update movimientos set mov_total = '".$final."' where mov_id = '$movi' ";
		$res4 = mysqli_query($cnx,$sql4);
		?> 
        <table width="100%">
        <tr valign="top">
            <td width="300">
                <div class="alert alert-success"> 
        			<span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
                    Toma de Inventario guardada con &eacute;xito<br />
                    <b style="font-size:20px;"><? echo $_POST["tipo"]."/".$folio; ?></b>
                </div>
                <?php 
				construir_boton("transacciones.php","","izquierda","Ir a Transacciones",2);  
				?>
            </td>
            <td style="padding-left:15px;">
            	<iframe src="<?php echo "mods/home/aju_pdf.php?id=".$movi; ?>" width="100%" height="400"></iframe>
            </td>
        </tr>
        </table>
		<?php 
		break;  
} 
?>