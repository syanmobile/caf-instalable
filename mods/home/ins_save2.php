<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabación";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

switch($_POST["modo"]){
	case "crear":
		$sql = "INSERT INTO productos ( 
			pro_codigo,
			pro_nombre, 
			pro_descripcion,
			pro_codigo_barra,
			pro_categoria,
			pro_unidad, 
			
			pro_stock_minimo, 
			pro_stock_maximo,

			pro_tipo, 
			pro_imagen , 

			pro_extra_1,
			pro_extra_2,
			pro_extra_3,
			pro_extra_4,
			pro_extra_5,
			pro_extra_6,
			pro_extra_7,
			pro_extra_8,
			pro_extra_9,
			pro_extra_10
		) VALUES ( 
			'$_POST[codigo]',
			'$_POST[nombre]', 
			'$_POST[descripcion]', 
			'$_POST[codigo_barra]',
			'$_POST[categoria]',
			'$_POST[unidad]', 
			
			'$_POST[stock_minimo]', 
			'$_POST[stock_maximo]', 

			'INS', 
			'$_POST[imagen]',

			'$_POST[extra_1]',
			'$_POST[extra_2]',
			'$_POST[extra_3]',
			'$_POST[extra_4]',
			'$_POST[extra_5]',
			'$_POST[extra_6]',
			'$_POST[extra_7]',
			'$_POST[extra_8]',
			'$_POST[extra_9]',
			'$_POST[extra_10]'
		)"; 
		$res = mysqli_query($cnx,$sql); 
		$_POST["id"] = mysqli_insert_id($cnx);
		?>
        <div class="alert alert-success"> 
           Insumo <strong>'<? echo $_POST["nombre"]; ?>'</strong> creado con &eacute;xito
        </div>
		<?php 
		break; 
		
	case "editar": 
		$SQL_ = "UPDATE productos SET 
			pro_nombre = '$_POST[nombre]', 
			pro_descripcion = '$_POST[descripcion]', 
			pro_codigo_barra = '$_POST[codigo_barra]',
			pro_categoria = '$_POST[categoria]',
			pro_unidad = '$_POST[unidad]', 
			
			pro_stock_minimo = '$_POST[stock_minimo]',
			pro_stock_maximo = '$_POST[stock_maximo]',
 
			pro_imagen = '$_POST[imagen]',

			pro_extra_1 = '$_POST[extra_1]',
			pro_extra_2 = '$_POST[extra_2]',
			pro_extra_3 = '$_POST[extra_3]',
			pro_extra_4 = '$_POST[extra_4]',
			pro_extra_5 = '$_POST[extra_5]',
			pro_extra_6 = '$_POST[extra_6]',
			pro_extra_7 = '$_POST[extra_7]',
			pro_extra_8 = '$_POST[extra_8]',
			pro_extra_9 = '$_POST[extra_9]',
			pro_extra_10 = '$_POST[extra_10]' 
			
		WHERE pro_id = '$_POST[id]' ";  
		$res = mysqli_query($cnx,$SQL_);  
		?>   
        <div class="alert alert-success"> 
           Insumo <strong>'<? echo $_POST["nombre"]; ?>'</strong> editado con &eacute;xito
        </div>  
		<?php 
		$sql = "delete from campos_relaciones where nub_key = '$_POST[id]' and nub_tipo = 'PRO' ";  
		$res = mysqli_query($cnx,$sql);  
		break;
		
	case "eliminar":
		$sql = "delete from activos_fijos where acf_producto = '$_REQUEST[codigo]' ";  
		$res = mysqli_query($cnx,$sql);   
		
		$sql = "delete from movimientos_detalle where det_producto = '$_REQUEST[codigo]' ";  
		$res = mysqli_query($cnx,$sql); 
		
		$sql = "delete from productos where pro_codigo = '$_REQUEST[codigo]' ";  
		$res = mysqli_query($cnx,$sql); 
		
		$sql = "delete from toma_inventarios_detalle where det_producto = '$_REQUEST[codigo]' ";  
		$res = mysqli_query($cnx,$sql); 
		?>   
        <div class="alert alert-info"> 
           <b>INSUMO ELIMINADO "<? echo $_REQUEST["codigo"]; ?>"</b>
        </div>  
		<?php  
		break;
}
 
construir_boton("ins_listado.php","","buscar","Listado de insumos",2);
if($_REQUEST["modo"] <> "eliminar"){
	construir_boton("ins_editar.php","&id=".$_POST["id"],"editar","Editar este insumo",2);

	$campos_adicionales = explode(";;;",$_REQUEST["campos_adicionales"]);
	for($i = 1;$i < count($campos_adicionales);$i++){ 
		$sql = "insert into campos_relaciones (nub_tipo,nub_key,nub_campo,nub_valor,nub_forma) values 
		('PRO','$_POST[id]',".$campos_adicionales[$i].",'".$_REQUEST["campo_adicional_".$i]."',0)";
		$res = mysqli_query($cnx,$sql); 
	} 
}
construir_boton("ins_nuevo.php","","crear","Crear otro insumo",2);
?>