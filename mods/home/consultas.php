<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Consultar";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "consultas.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>
<div style="background-color: #f8ffd1; padding:10px; border:1px solid #ccc; margin-bottom: 10px;">
    <table width="100%" class="tabla_opciones">
    <tbody> 
        <tr>        
            <td><input type="text" name="texto" id="texto" class="campos w10" value="<? 
            echo $_REQUEST["texto"]; ?>"></td>
            <td width="1"><?php
    		construir_boton("consultas.php","","buscar","Buscar",2);
    		?></td>
        </tr>
    </tbody>
    </table> 
</div>

<?php
if($_REQUEST["texto"] <> ""){
    ?>
    <ul class="nav nav-tabs" role="tablist">
    <li class="active">
        <a href="#tab_1" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Activos (<span id="act"></span>)</a>
    </li>
    <li>
        <a href="#tab_2" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Insumos (<span id="ins"></span>)</a>
    </li>
    <li>
        <a href="#tab_3" role="tab" data-toggle="tab" style="color:#000 !important; font-weight:bold;">Personas (<span id="per"></span>)</a>
    </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <?php 
            /** ACTIVOS *********************************************************************/
            $filtros .= " and (pro.pro_codigo like '%".$_REQUEST["texto"]."%' ";
            $filtros .= " or pro.pro_nombre like '%".$_REQUEST["texto"]."%' ";
            $filtros .= " or pro.pro_codigo_barra like '%".$_REQUEST["texto"]."%') ";
            
            $res = sql_activos_fijos("*"," $filtros ORDER BY acf.acf_producto asc, acf.acf_serie asc"); 
            $act = mysqli_num_rows($res);
            if($act > 0){
                ?>
                <table class="table table-striped table-bordered tabledrag table-condensed"> 
                <thead>
                    <tr>                          
                        <th width="1">Codigo</th>
                        <th width="1">Serie</th>
                        <th width="1">Etiqueta</th>  
                        <th>Producto</th>
                        
                        <th>Lugar/Ubicacion</th>
                        <th>Responsable</th> 
                        
                        <th width="1">Estado</th>
                    </tr>
                </thead>
                <tbody> 
                <?
                while($row = mysqli_fetch_array($res)){ 
                    ?>
                    <tr>  
                        <th><?php echo $row["pro_codigo"]; ?></th>
                        <th><?php echo $row["acf_serie"]; ?></th>
                        <th><?php if($row["acf_etiqueta"] <> ""){ echo $row["acf_etiqueta"]; } ?></th>  
                        <td><?php echo $row["pro_nombre"]; ?></td>
                                    
                        <td><?php echo $_ubicaciones_bod[$row["ubi_id"]]." - ".$row["ubi_nombre"]; ?></td> 
                        <td><?php echo $row["per_nombre"]; ?></td>  
                        
                        <td><?php activo_fijo_estado($row["acf_disponible"]); ?></td> 
                    </tr>
                    <?  
                }
                ?>
                </tbody>
                </table>
                <? 
            }else{
                ?>
                <div class="alert alert-danger">
                    <strong>No se encontraron activos</strong>
                </div>
                <?php 
            } 
            ?>
        </div>
        <div class="tab-pane" id="tab_2">
            <?
            /** ACTIVOS *********************************************************************/
            $filtros2 .= " and (pro_codigo like '%".$_REQUEST["texto"]."%' ";
            $filtros2 .= " or pro_nombre like '%".$_REQUEST["texto"]."%' ";
            $filtros2 .= " or pro_codigo_barra like '%".$_REQUEST["texto"]."%') ";

            $res = sql_insumos("*,sum(total) as stock"," $filtros2 
            GROUP BY pro_codigo ORDER BY pro_nombre asc","","");
            $ins = mysqli_num_rows($res);
            if($ins > 0){
                ?>
                <table class="table table-striped table-bordered tabledrag table-condensed"> 
                <thead>
                    <tr>   
                        <th width="1">Codigo</th> 
                        <th>Producto</th>    
                        
                        <th width="1">Minimo</th> 
                        <th width="1">Stock</th> 
                        <th width="1">xComprar</th> 
                    </tr>
                </thead>
                <tbody> 
                <?
                while($row = mysqli_fetch_array($res)){ 
                    ?>
                    <tr>   
                        <th><?php echo $row["pro_codigo"]; ?></th> 
                        <td><?php echo $row["pro_nombre"]; ?></td>     
                                    
                        <td style="text-align:center; "><?php echo _num($row["pro_stock_minimo"]); ?></td> 
                        <td style="text-align:center; "><span class="label label-<?php
                        if($row["stock"] > $row["pro_stock_minimo"]){
                            echo 'success';
                        }else{
                            echo 'danger';
                        }
                        ?>"><?php echo _num($row["stock"]); ?></span></td> 
                        <td style="text-align:center;"><?php 
                        if($row["stock"] < $row["pro_stock_minimo"]){
                            echo _num($row["pro_stock_minimo"] - $row["stock"]);
                        }else{
                            echo "0";
                        }
                        ?></td>   
                    </tr>
                    <?  
                }
                ?>
                </tbody>
                </table> 
                <? 
            }else{
                ?>
                <div class="alert alert-danger">
                    <strong>No se encontraron insumos</strong>
                </div>
                <?php 
            } 
            ?>
        </div>
        <div class="tab-pane" id="tab_3">
            <?php
            $extras2 = " and (per_nombre like '%".$_REQUEST["texto"]."%') ";
            $res = sql_personas("*"," $extras2  ORDER BY per.per_nombre asc ","","");  
            $per = mysqli_num_rows($res);
            if($per > 0){
                ?>
                <table class="table table-bordered table-condensed"> 
                <thead>
                <tr> 
                    <th width="1">Rut</th>
                    <th>Nombre</th> 
                    <th>Tipo</th> 
                    <th>Correo</th> 
                    <th>Perfil</th>   
                </tr>
                </thead>
                <tbody> 
                <?php 
                while($row = mysqli_fetch_array($res)){
                    ?>
                    <tr> 
                        <td><?php echo $row["per_rut"]; ?></td>
                        <th><?php echo $row["per_nombre"]; ?></th> 
                        <td><?php echo ($row["per_tipo"])?"SISTEMA":"USUARIO"; ?></td>
                        <td><?php echo $row["per_correo"]; ?></td> 
                        <?php
                        for($i = 1;$i <= 20;$i++){
                            if($extras["Extra_".$i] <> ""){
                                ?><td><?php echo $row["per_extra_".$i]; ?></td><?php
                            }
                        } 
                        ?>
                        <td><?php echo $row["pfl_nombre"]; ?></td>  
                    </tr>
                    <?php }
                ?> 
                </tbody>
                </table>  
                <?php 
            }else{
                ?>
                <div class="alert alert-danger">
                    <strong>No se encontraron personas</strong>
                </div>
                <?php 
            }
            ?>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
        $("#act").html("<? echo ($act * 1); ?>");
        $("#ins").html("<? echo ($ins * 1); ?>");
        $("#per").html("<? echo ($per * 1); ?>");
    });
    </script>
    <? 
}else{
    ?>
    <div class="alert alert-info">
        <strong>Aún no ingresa lo que necesita buscar</strong>
    </div>
    <?php 
}
?>