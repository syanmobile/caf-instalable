<?php
require("../../inc/conf_dentro.php");
 
switch($_REQUEST["modo"]){
	case "disponibles": 
		$res = sql_activos_fijos("*"," and acf_disponible <> 0 and acf_disponible <> 4 
			and ubi.ubi_id = '$_REQUEST[ubicacion]' order by acf.acf_producto asc "); 
		if(mysqli_num_rows($res) > 0){
			?>
			<table class="table table-striped table-bordered table-condensed" id="tablita"> 
			<thead>
				<tr>    
					<th>Imagen</th> 
					<th>Activo Fijo</th>
					<th>Informacion</th>
					<th>Situación</th>
					<th>Inf.Contable</th>
					<th>Elegir</th> 
				</tr>
			</thead>
			<tbody> 
			<?
			while($row = mysqli_fetch_array($res)){

				$sql2 = "select * from depreciaciones where dep_periodo >= '".date("Y-m")."-01' and dep_periodo <= '".date("Y-m")."-15' ";
				$sql2.= "and dep_acf_id = '$row[acf_id]' ";  
				$res2 = mysqli_query($cnx,$sql2);
				$num2 = mysqli_num_rows($res2);
				unset($row2);
				if($num2 > 0){
					$row2 = mysqli_fetch_array($res2); 
				}  
				?>
				<tr valign="top" id="acf<? echo $row["acf_id"]; ?>">
					<td><? 
					$imagen = ($row["pro_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$row["pro_imagen"];
					?><img src="<?php echo $imagen; ?>" style="width:80px !important;"></td> 
					<td>
						<b><? echo $row["pro_nombre"]; ?></b><br />
						<b>Codigo</b>: <? echo $row["pro_codigo"]; ?><br>
						<b>Serie</b>: <? echo $row["acf_serie"]; ?><br>
						<b>Etiqueta</b>: <?php echo $row["acf_etiqueta"]; ?><br>
						<b>Ubicacion</b>: <?php echo $row["ubi_nombre"]; ?>  
					</td>
                    <td>
                    	<strong>Tipo</strong>: <? 
                    	echo $_tipo_activo[$row["pro_tipo"]];
                    	if($_grupos[$row["pro_grupo"]] <> ""){
                    		?><br><strong>Grupo</strong>: <? echo $_grupos[$row["pro_grupo"]];
                    	}
                    	if($_subgrupos[$row["pro_subgrupo"]] <> ""){
                    		?><br><strong>Subgrupo</strong>: <? echo $_subgrupos[$row["pro_subgrupo"]];
                    	}
                    	if($_categorias[$row["pro_categoria"]] <> ""){
                    		?><br><strong>Categoria</strong>: <? echo $_categorias[$row["pro_categoria"]];
                    	} 
						?>   
                    </td>
                    <td>
                    	<b>Prox.Control:</b> <? echo _fec($row["acf_fecha_mantencion"],5); ?><br>
                    	<b>Condicion:</b> <?php echo $row["acf_condicion"]; ?><br>
						<b>Estado del activo:</b><br><?php activo_fijo_estado($row["acf_disponible"]); ?>
					</td>
                    <td>
                    	<b>Valor Compra:</b> $<?php echo _num($row["acf_valor"]); ?><br>
                    	<?
						if($num2 > 0){
							?>
	                    	<b>Depreciación Acumulada:</b> <?php echo _num($row2["dep_acumulada"]); ?><br> 
	                    	<b>Valor Neto en Libros:</b> <?php echo _num($row2["dep_neto"]); 
	                    }
	                    ?>
					</td> 	
                    <td style="padding: 0px;">
                    	<?
						if($num2 > 0){
							?><a href="javascript:elegir('<? echo $row["acf_id"]; ?>','<? echo $row2["dep_neto"]; ?>');"><img src="img/add.png" width="60"></a><?
						}else{
							?><a href="javascript:elegir('<? echo $row["acf_id"]; ?>','<? echo $row["acf_valor"]; ?>');"><img src="img/add.png" width="60"></a><?
						}
						?>
                    </td>
				</tr> 
				<?  
			}
			?>
			</tbody>
			</table>
			<script language="javascript">
            $(document).ready(function() {  
				$('#tablita').DataTable({ 
					"columns": [     
						{ "width": "1px" },    
						null ,   
						null,  
						null, 
						null,
						{ "width": "1px" }   
					  ],
					"paging":   false,
					"info":     false,
					"lengthMenu": [100000],
					"oLanguage": {
						"sSearch": "Busque un activo y presione la flecha para agregar: "
					}
				});
            } );   
            </script> 
			<?
		}else{
			?>
			<div class="alert alert-danger">
				<strong>Sin registros creados</strong>
			</div>
			<?php 
		} 
		break; 
		
	case "detalle": 
		if($_REQUEST["activo"] <> ""){
			?> 
			<table class="table table-striped table-bordered table-condensed"> 
			<thead>
				<tr>
					<th width="1"></th> 
					<th>Producto</th>
					<th width="1">Valor</th> 
				</tr>
			</thead>
			<tbody>
			<?php 
			$res = sql_activos_fijos("*"," and acf_id = '".$_REQUEST["activo"]."'"); 
			$prod = mysqli_fetch_array($res); 
			?>
			<tr>
				<td><?php
				construir_boton("quitar_linea('".$prod["acf_id"]."');","1","eliminar","Borrar Linea",4);
				?></td>  
				<td><?php echo $prod["pro_nombre"]; ?><br>
				<?php echo $prod["pro_codigo"]; ?> / <strong>Serie: <?php echo $prod["acf_serie"]; ?></strong><br>
				Ubicación: <? echo $prod["ubi_nombre"]; 
				?></td>
				<th>$<? echo _num($_REQUEST["valor"]); ?></th>
			</tr>
			</tbody>
			</table>
			<?
		}else{
			?>
			<div class="alert alert-danger">
				<h3>Agregue un activo al movimiento</h3>
			</div>
			<?
		}
		break;
}
?> 