<?php
require("../../inc/conf_dentro.php");
 
switch($_REQUEST["modo"]){
	case "disponibles": 
		$res = sql_activos_fijos("*"," and acf_disponible <> 0 
		and acf_disponible <> 4 and ubi.ubi_id = '$_REQUEST[ubicacion]' 
		order by acf.acf_producto asc "); 
		if(mysqli_num_rows($res) > 0){
			?>
			<script type="text/javascript"> 
			$(document).ready(function(){
				//Checkbox
				$("input[name=checktodos1]").change(function(){
					var valor = $("input[name=checktodos1]:checked").length;
					if(valor){
						 $('input[alt1=1]').each( function() {
							  this.checked = true;
						 });
					}else{
						 $(".asis1").removeAttr("checked");
					} 
				});
			});
			</script>
			<table class="table table-striped table-bordered tabledrag table-condensed"> 
			<thead>
				<tr>  
					<th width="1"><input name="checktodos1" type="checkbox"></th> 
					
					<th width="1">Activo</th> 
					<th width="1">Serie</th>
					<th width="1">Etiqueta</th>

					<th width="1">Cod.Prod</th>  
					<th>Producto</th>
					
					<th>Lugar/Ubicacion</th>
					<th>Responsable</th>  
					
					<th width="1">Estado</th>
				</tr>
			</thead>
			<tbody> 
			<?
			while($row = mysqli_fetch_array($res)){ 
				?>
				<tr> 
					<td width="1"><input type="checkbox" value="<?php echo $row["acf_id"]; ?>" name="activo[]" alt1="1" class="campos asis1"></td>
					
					<th><?php echo $row["acf_codigo"]; ?></th> 
					<th><?php echo $row["acf_serie"]; ?></th>
					<th><?php if($row["acf_etiqueta"] <> ""){ echo $row["acf_etiqueta"]; } ?></th> 
					
					<th><?php echo $row["pro_codigo"]; ?></th> 
					<td><?php echo $row["pro_nombre"]; ?></td>
								
					<td><?php echo $_ubicaciones_bod[$row["ubi_id"]]." - ".$row["ubi_nombre"]; ?></td> 
					<td><?php echo $row["per_nombre"]; ?></td> 
					
					<td><?php activo_fijo_estado($row["acf_disponible"]); ?></td> 
				</tr>
				<?  
			}
			?>
			</tbody>
			</table>   
			<?
		}else{
			?>
			<div class="alert alert-danger">
				<strong>Sin registros creados</strong>
			</div>
			<?php 
		} 
		break;  
}
?> 