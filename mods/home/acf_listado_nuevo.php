<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
construir_breadcrumb(strtoupper($_tipo_activo[$_REQUEST["fil_tipo"]]));
//----------------------------------------------------------------------------------------
$pagina = "acf_listado.php"; 

include("ayuda.php");
//----------------------------------------------------------------------------------------

// Arreglo de los filtros en esta pagina
$campos_filtros = array("fil_codigo","fil_codigo2","fil_serie","fil_etiqueta","fil_descripcion","fil_categoria","fil_ubicacion","fil_responsable","fil_estado","fil_auxiliar","fil_compra_inicio","fil_compra_termino","fil_lugar");
for($iex = 1;$iex <= 10;$iex++){
	$campos_filtros[] = "fil_extra_pro_".$iex;
	$campos_filtros[] = "fil_extra_acf_".$iex;
} 

if($_REQUEST["limpiar"] == "S"){
	foreach($campos_filtros as $campito){
		$_REQUEST["".$campito] = "";
		$_SESSION["filtros"][$pagina][$campito] = "";		
	}
}
if($_REQUEST["mantener_filtros"] == ""){
	foreach($campos_filtros as $campito){ 
		$_REQUEST["".$campito] = $_SESSION["filtros"][$pagina][$campito]; 
	}	
}else{
	foreach($campos_filtros as $campito){
		$_SESSION["filtros"][$pagina][$campito] = $_REQUEST[$campito];		
	}
}

// Campos extras de activos/productos
for($iex = 1;$iex <= 10;$iex++){				 
    $res = sql_campos("*"," and cam_producto = $iex");  
	if(mysqli_num_rows($res) > 0){
		while($row = mysqli_fetch_array($res)){
			$_campo_pro[] = array($row["cam_nombre"],$iex);
		}
	}
}
for($iex = 1;$iex <= 10;$iex++){				 
    $res = sql_campos("*"," and cam_activo = $iex");  
	if(mysqli_num_rows($res) > 0){
		while($row = mysqli_fetch_array($res)){
			$_campo_acf[] = array($row["cam_nombre"],$iex);
		}
	}
}
?>
<input type="hidden" name="mantener_filtros" value="S" class="campos">

<table width="100%">
<tr valign="top">
	<td>

	<table class="table table-bordered table-info table-condensed"> 
	<tbody>
	<tr>
		<td>
			<b>Código Activo:</b><br>
			<input type="text" name="fil_codigo2" id="fil_codigo2" class="campos w10" value="<? 
			echo $_REQUEST["fil_codigo2"]; ?>">
		</td>
		<td>
			<b>Código Producto:</b><br>
			<input type="text" name="fil_codigo" id="fil_codigo" class="campos w10" value="<? 
			echo $_REQUEST["fil_codigo"]; ?>"> 
		</td>
		<td>
			<b>Nombre Producto:</b><br>
			<input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? 
			echo $_REQUEST["fil_descripcion"]; ?>">
		</td>
		<td>
			<b>Serie:</b><br>
			<input type="text" name="fil_serie" id="fil_serie" class="campos w10" value="<? 
			echo $_REQUEST["fil_serie"]; ?>">
		</td>
		<td>
			<b>Etiqueta:</b><br>
			<input type="text" name="fil_etiqueta" id="fil_etiqueta" class="campos w10" value="<? 
			echo $_REQUEST["fil_etiqueta"]; ?>">
		</td>
	</tr>
	<tr>
		<td>
			<b>Clasificación:</b><br>
			<select name="fil_categoria" id="fil_categoria" class="campos buscador w10">
			<option value=""></option>
			<?php
			$res = sql_categorias("*"," order by cat.cat_nombre asc"); 
			if(mysqli_num_rows($res) > 0){
				while($row = mysqli_fetch_array($res)){
					?>
		            <option value="<? echo $row["cat_id"]; ?>" <?
		            if($_REQUEST["fil_categoria"] == $row["cat_id"]){ echo "selected"; } 
		            ?>>C<? echo $row["cat_codigo"]." - ".$row["cat_nombre_largo"]; ?></option>
		            <?
				}
			}
			?></select>
		</td>
		<td>
			<b>Ubicación:</b><br>
			<select name="fil_ubicacion" id="fil_ubicacion" class="campos buscador w10">
			<option value=""></option>
			<?php
			$res = sql_ubicaciones("*"," order by bod.bod_nombre asc, ubi.ubi_nombre asc"); 
			if(mysqli_num_rows($res) > 0){
				while($row = mysqli_fetch_array($res)){
					?>
					<option value="<? echo $row["ubi_id"]; ?>" <?
					if($_REQUEST["fil_ubicacion"] == $row["ubi_id"]){ echo "selected"; } 
					?>>U<? echo $row["ubi_codigo"]." - ".$row["ubi_nombre"]; ?></option>
					<?
				}
			}
			?></select>
		</td>
		<td>
			<b>Responsable:</b><br>  
			<select name="fil_responsable" id="fil_responsable" class="campos buscador">
			<option value=""></option> 
			<?php
			$res = sql_personas("*","   ORDER BY per.per_nombre asc ");  
			if(mysqli_num_rows($res) > 0){
				while($row = mysqli_fetch_array($res)){
					?>
					<option value="<? echo $row["per_id"]; ?>" <?
					if($row["per_id"] == $_REQUEST["fil_responsable"]){ echo "selected"; } 
					?>><?php echo $row["per_nombre"]; ?></option>
					<?
				}
			}
			?> 
			</select>
		</td>
		<td>
			<b>Centro Costo:</b><br>
		    <select name="fil_cco" id="fil_cco" class="campos buscador">
		    <option value=""></option>
		    <?php
			$res = sql_centro_costo("*","order by cco_codigo asc");  
			if(mysqli_num_rows($res) > 0){
				while($datos = mysqli_fetch_array($res)){
					?><option value="<?php echo $datos["cco_id"]; ?>" <?php 
					if($_REQUEST["fil_cco"] == $datos["cco_id"]){ echo "selected"; } 
					?>>CC<?php echo $datos["cco_codigo"]; ?> - <?php echo $datos["cco_nombre"]; ?></option>
		            <?
				}
			}
			?>
		    </select> 
		</td>
		<td>
			<b>Estado:</b><br>
			<select name="fil_estado" id="fil_estado" class="campos buscador">
				<option value="">Todos</option>  
				<option value="0" <? 
				if($_REQUEST["fil_estado"] == "0"){ echo "selected"; } ?>>No Disponible</option>
				<option value="1" <? 
				if($_REQUEST["fil_estado"] == "1"){ echo "selected"; } ?>>Disponible</option>
				<option value="2" <? 
				if($_REQUEST["fil_estado"] == "2"){ echo "selected"; } ?>>Prestado</option>
				<option value="3" <? 
				if($_REQUEST["fil_estado"] == "3"){ echo "selected"; } ?>>En Mantenimiento</option>
				<option value="4" <? 
				if($_REQUEST["fil_estado"] == "4"){ echo "selected"; } ?>>De Baja</option>
				<option value="5" <? 
				if($_REQUEST["fil_estado"] == "5"){ echo "selected"; } ?>>Req.Mantenimiento</option>
				<option value="6" <? 
				if($_REQUEST["fil_estado"] == "6"){ echo "selected"; } ?>>Asignado</option>
			</select>
		</td>
	</tr>
	</tbody>
	</table> 
 
	<table class="table table-bordered table-info table-condensed" style="display: none;" id="filtros_avanzados"> 
	<tbody>
	<tr>
		<td width="150">
			<b>Tipo:</b><br>
			<select name="fil_tipo" id="fil_tipo" class="campos buscador">  
			<?php
			foreach($_tipo_activos as $tp){
				if(in_array("act_".strtolower($tp)."_ver",$opciones_persona)){
					?><option value="<? echo $tp; ?>" <? if($_REQUEST["fil_tipo"] == $tp){ echo "selected"; } ?>><?
					echo $_tipo_activo[$tp]; ?></option>
					<?
				}
			}
			?>
			</select>
		</td>
		<td width="300">
			<b>Proveedor:</b><br>
			<select name="fil_auxiliar" id="fil_auxiliar" class="campos" style="width: 100% !important;"><?
			if($_REQUEST["fil_auxiliar"] <> ""){
				$res = sql_auxiliares("*"," and aux.aux_id = '$_REQUEST[fil_auxiliar]' "); 
				$num = mysqli_num_rows($res); 
				if($num > 0){ 
					while($row = mysqli_fetch_array($res)){
						?><option value="<? echo $row["aux_id"]; ?>"><? echo utf8_encode($row["aux_nombre"]); ?></option><?
					}
				}
			}
			?></select>
		</td>
		<td width="200">
			<b>Lugar:</b><br>
			<select name="fil_lugar" id="fil_lugar" class="campos buscador" style="width: 100% !important;">
			<option value=""></option>
			<?
			$res = sql_bodegas("*"," ORDER BY bod_nombre asc");
			$num = mysqli_num_rows($res); 
			if($num > 0){ 
				while($row = mysqli_fetch_array($res)){
					?><option value="<? echo $row["bod_id"]; ?>"<?
					if($_REQUEST["fil_lugar"] == $row["bod_id"]){ echo "selected"; } 
					?>><? 
					echo utf8_encode($row["bod_codigo"]." ".$row["bod_nombre"]); 
					?></option><?
				}
			} 
			?></select>
		</td>
		<td width="130">
			<b>Documento Ingreso:</b><br>
			<input type="text" name="fil_doc_ingreso" id="fil_doc_ingreso" class="campos w10" value="<? echo $_REQUEST["fil_doc_ingreso"]; ?>">  
		</td>
		<td width="210">
			<b>Fecha Compra:</b><br>
			<input type="text" name="fil_compra_inicio" id="fil_compra_inicio" class="campos fecha" value="<? echo $_REQUEST["fil_compra_inicio"]; ?>" style="width: 70px; float: left;">
			<input type="text" name="fil_compra_termino" id="fil_compra_termino" class="campos fecha" value="<? echo $_REQUEST["fil_compra_termino"]; ?>" style="width: 70px; float: left;">  
		</td>
		<?
		foreach($_campo_pro as $dato){
			?><td width="170">
			<b><? echo $dato[0]; ?>:</b><br>
			<input type="text" name="fil_extra_pro_<? echo $dato[1]; ?>" class="campos w10" value="<? 
			echo $_REQUEST["fil_extra_pro_".$dato[1]]; ?>">
			</td>
			<?
		}
		foreach($_campo_acf as $dato){
			?><td width="170">
			<b><? echo $dato[0]; ?>:</b><br>
			<input type="text" name="fil_extra_acf_<? echo $dato[1]; ?>" class="campos w10" value="<? 
			echo $_REQUEST["fil_extra_acf_".$dato[1]]; ?>">
			</td>
			<?
		}
		?>
	</tr>
	<tr>
		<td>
			<b>Ubicaciones(%):</b><br>
			<input type="text" name="fil_ubicaciones" id="fil_ubicaciones" class="campos w10" value="<? echo $_REQUEST["fil_ubicaciones"]; ?>">
		</td>
		<td colspan="20">
		</td>
	</tr>
	</tbody>
	</table>

	</td> 
	<td width="1" style="padding-left: 10px;">
		<? construir_boton("buscador_general('acf_listado_nuevo.php')","","filtrar","Avanzados",4); ?>
		<? construir_boton("acf_listado.php","&fil_tipo=".$_REQUEST["fil_tipo"]."&limpiar=S","limpiar","Limpiar",8); ?>
		<br><br>
		<? construir_boton("avanzados()","","filtrar","Avanzados",4); ?>
	</td>
</tr>
</table>
<script type="text/javascript"> 
function avanzados(){
	$("#filtros_avanzados").toggle();
} 
$("#fil_auxiliar").select2({        
    ajax: {
        url: "mods/home/aux_ajax2.php",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term // search term
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data, function(obj) {
                    return { id: obj.id, text: obj.text };
                })
            };
        },
        cache: true
    },
    minimumInputLength: 3 
}); 
</script> 

<?php  
/****************************************************************************************/
if($_REQUEST["fil_codigo"] <> ""){
	$filtros .= " and pro.pro_codigo like '".$_REQUEST["fil_codigo"]."' ";
}
if($_REQUEST["fil_codigo2"] <> ""){
	$filtros .= " and acf.acf_codigo like '".$_REQUEST["fil_codigo2"]."' ";
}
if($_REQUEST["fil_serie"] <> ""){
	$filtros .= " and acf.acf_serie like '".$_REQUEST["fil_serie"]."' ";
}
if($_REQUEST["fil_etiqueta"] <> ""){
	$filtros .= " and acf.acf_etiqueta like '".$_REQUEST["fil_etiqueta"]."' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and pro.pro_nombre like '".$_REQUEST["fil_descripcion"]."' ";
} 
if($_REQUEST["fil_unidad"] <> ""){
	$filtros .= " and pro.pro_unidad = '".$_REQUEST["fil_unidad"]."' ";
}  
if($_REQUEST["fil_categoria"] <> ""){
	$filtros .= " and pro.pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
}
if($_REQUEST["fil_cco"] <> ""){
	$filtros .= " and acf.acf_cco_id = '".$_REQUEST["fil_cco"]."' ";
}
if($_REQUEST["fil_ubicacion"] <> ""){
	$filtros .= " and acf.acf_ubicacion = '".$_REQUEST["fil_ubicacion"]."' ";
} 
if($_REQUEST["fil_lugar"] <> ""){
	$filtros .= " and ubi.ubi_bodega = '".$_REQUEST["fil_lugar"]."' ";
} 
if($_REQUEST["fil_ubicaciones"] <> ""){ 
	$res2 = sql_ubicaciones("*"," and ubi.ubi_codigo like '$_REQUEST[fil_ubicaciones]'");
	if(mysqli_num_rows($res2) > 0){
		while($row2 = mysqli_fetch_array($res2)){
			if($ubi_ids <> ""){
				$ubi_ids .= ",";
			}
			$ubi_ids .= $row2["ubi_id"];
		}
	}
	$filtros .= " and acf.acf_ubicacion in ($ubi_ids) ";
}  
if($_REQUEST["fil_tipo"] <> ""){
	$filtros .= " and pro.pro_tipo = '".$_REQUEST["fil_tipo"]."' ";
}    
if($_REQUEST["fil_compra_inicio"] <> ""){
	$filtros .= " and acf.acf_fecha_ingreso >= '"._fec($_REQUEST["fil_compra_inicio"],9)."' ";
} 
if($_REQUEST["fil_compra_termino"] <> ""){
	$filtros .= " and acf.acf_fecha_ingreso <= '"._fec($_REQUEST["fil_compra_termino"],9)."' ";
} 
if($_REQUEST["fil_responsable"] <> ""){
	$filtros .= " and acf.acf_responsable = '".$_REQUEST["fil_responsable"]."' ";
} 
if($_REQUEST["fil_estado"] <> ""){
	$filtros .= " and acf.acf_disponible = '".$_REQUEST["fil_estado"]."' ";
}
if($_REQUEST["fil_auxiliar"] <> ""){
	$filtros .= " and acf.acf_proveedor = '".$_REQUEST["fil_auxiliar"]."' ";
}
if($_REQUEST["fil_doc_ingreso"] <> ""){
	$filtros .= " and acf.acf_nro_factura = '".$_REQUEST["fil_doc_ingreso"]."' ";
}
for($ie = 1;$ie <= 10;$ie++){
	if($_REQUEST["fil_extra_acf_".$ie] <> ""){
		$filtros .= " and acf.acf_extra_".$ie." like '".$_REQUEST["fil_extra_acf_".$ie]."' ";
	}
	if($_REQUEST["fil_extra_pro_".$ie] <> ""){
		$filtros .= " and pro.pro_extra_".$ie." like '".$_REQUEST["fil_extra_pro_".$ie]."' ";
	}
}
?>
<input type="hidden" name="pagina" id="pagina" value="acf_listado.php" class="campos">
<?
$sql_paginador = sql_activos_fijos_rapido("*"," $filtros ORDER BY acf.acf_producto asc, acf.acf_serie asc","s",""); 
$cant_paginador = 10;
require("../../_paginador.php");
$res = mysqli_query($cnx,$sql_final); 
if(mysqli_num_rows($res) > 0){
	?>
	<table class="table table-striped table-bordered tabledrag table-condensed" style="margin-bottom: 0px;"> 
	<thead>
		<tr>  
			<th width="65"></th> 
			<th width="1">Imagen</th>
 
			<th>Códigos</th>
			<th>Producto</th>
			 
			<th>Ubicación</th>
			<th>Responsable</th>
			<th>Info Ingreso</th>

			<th>Clasificación</th>
			<th>Más Información</th> 

			<th width="1">Estado</th>
		</tr>
	</thead>
	<tbody> 
	<?
	while($row = mysqli_fetch_array($res)){  
		?>
		<tr> 
			<td><? 
			construir_boton("acf_info.php","&acf=".$row["acf_id"],"buscar","Ver Activo Fijo"); 
			construir_boton("acf_editar.php","&id=".$row["acf_id"],"editar","Editar Activo Fijo");
			construir_boton("visor('acf_pdf.php?acf=".$row["acf_id"]."')","1","imprimir","Imprimir Ficha",4);
			if($row["acf_etiqueta"] <> ""){
				construir_boton("abrir_pagina('cdu_pdf.php?cdu[]=".$row["acf_etiqueta"]."')","1","barcode","Imprimir Código",4);
			}
			?></td> 
			<td><? 
			$imagen = ($row["pro_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$row["pro_imagen"];
			?><img src="<?php echo $imagen; ?>" style="width:40px !important;"></td> 
			<td>
				<b>Cod.Activo:</b> <?php echo $row["acf_codigo"]; ?><br>
				<b>Serie:</b> <?php echo $row["acf_serie"]; ?><br>
				<b>Etiqueta:</b> <?php echo $row["acf_etiqueta"]; ?>
			</td>
			<td>
				<?php echo $row["pro_nombre"]; ?><br>
				<b><?php echo $row["pro_codigo"]; ?></b>
			</td> 	
			<td><?php 
			if($row["acf_ubicacion"] <> 0){
				echo $_ubicaciones[$row["acf_ubicacion"]]; ?><br>
				<b><?php echo $_ubicaciones_codigo[$row["acf_ubicacion"]]; ?></b><?
			}
			?></td> 
			<td><?php echo $_personas[$row["acf_responsable"]]; ?></td> 
			<td>
				Fecha Ingreso: <?php echo _fec($row["acf_fecha_ingreso"],5); ?><br>
				Doc.Ingreso: <? echo $row["acf_nro_factura"]; ?><br>
				Proveedor: <? echo $_SESSION["auxiliares"][$row["acf_proveedor"]]; ?><br>
				Valor Compra (<? echo $_configuraciones["cfg_moneda"]; ?>): <?php 
				echo _num($row["acf_valor"],$_configuraciones["cfg_moneda_decimal"]);
				if($row["acf_valor_sec"] > 0){
					?><br>
					2da Moneda (<? echo $_configuraciones["cfg_moneda_sec"]; ?>): <?php 
					echo _num($row["acf_valor_sec"],$_configuraciones["cfg_moneda_sec_decimal"]); 
				}
				?>
			</td>  
			<td>
				<?php echo $_categorias[$row["pro_categoria"]]; ?><br>
				<b><?php echo $_categorias_codigo[$row["pro_categoria"]]; ?></b>
			</td> 
			<td>
				<b>C.Costo:</b> <? echo $_centro_costos_corto[$row["acf_cco_id"]]; ?>
				<?
				foreach($_campo_pro as $dato){
					if($row["pro_extra_".$dato[1]] <> ""){
						?><br><b><? echo $dato[0]; ?>:</b> <? echo $row["pro_extra_".$dato[1]]; ?><?
					}
				}
				foreach($_campo_acf as $dato){
					if($row["acf_extra_".$dato[1]] <> ""){
						?><br><b><? echo $dato[0]; ?>:</b> <? echo $row["acf_extra_".$dato[1]]; ?><?
					}
				}
				?>
			</td> 
			<td><?php activo_fijo_estado($row["acf_disponible"]); ?></td> 
		</tr>
		<?  
	}
	?>
	</tbody>
	</table> 
	<?
	echo $paginador_dibujo;
}else{
	?>
	<div class="alert alert-danger">
		<strong>Sin registros creados</strong>
	</div>
	<?php 
} 
?>