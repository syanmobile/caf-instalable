<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Listado Unidades";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "uni_listado.php"; 
include("ayuda.php");
//---------------------------------------------------------------------------------------- 
?>
<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td><?php
		construir_boton("uni_nuevo.php","","crear","Nueva Unidad",2);
		construir_boton("uni_importar.php","","importar","Importar Unidades",2);
		?></td>
    </tr>
</tbody>
</table>
<?php
$res = sql_unidades("*","   ORDER BY uni.uni_nombre asc","","");
if(mysqli_num_rows($res) > 0){
    ?>
    <table class="table table-striped table-bordered table-condensed" id="tablita"> 
    <thead>
    <tr>
        <th width="1" align="center">&nbsp;</th>
        <th width="1" align="center">Código</th>
        <th>Unidad</th>
    </tr>
    </thead>
    <tbody> 
    <?
    while($row = mysqli_fetch_array($res)){
        ?>
        <tr fila="<?php echo $row["uni_codigo"] ;?>" id="<?php echo $row["uni_codigo"] ;?>">  
            <td style="text-align:center;"><?php
			construir_boton("uni_editar.php","&codigo=".$row["uni_codigo"],"editar","Editar");
			?></td>
            <th style="text-align: center"><?php echo $row["uni_codigo"]; ?></th>
            <td><?php echo $row["uni_nombre"]; ?></td>
        </tr>
        <? 
    }
    ?> 
    </tbody>
    </table> 
	
	<script language="javascript">
	$(document).ready(function() { 
		$('#tablita').DataTable({
		  "columns": [   
			{ "width": "1px" }, 
			{ "width": "1px" },  
			null  
		  ],
		  lengthMenu: [20000000],
		  "bPaginate": false,
		  "bLengthChange": false
		});
	} );   
	</script>  
    <?php
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin registros creados</strong>
    </div>
    <?php
}
?>