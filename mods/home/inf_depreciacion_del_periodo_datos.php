<?php   
include("../../inc/conf_dentro.php");

/****************************************************************************************/
if($_REQUEST["fil_codigo"] <> ""){
	$filtros .= " and pro.pro_codigo like '".$_REQUEST["fil_codigo"]."' ";
}
if($_REQUEST["fil_codigo2"] <> ""){
	$filtros .= " and acf.acf_codigo like '".$_REQUEST["fil_codigo2"]."' ";
}
if($_REQUEST["fil_serie"] <> ""){
	$filtros .= " and acf.acf_serie like '".$_REQUEST["fil_serie"]."' ";
}
if($_REQUEST["fil_etiqueta"] <> ""){
	$filtros .= " and acf.acf_etiqueta like '".$_REQUEST["fil_etiqueta"]."' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and pro.pro_nombre like '".$_REQUEST["fil_descripcion"]."' ";
} 
if($_REQUEST["fil_unidad"] <> ""){
	$filtros .= " and pro.pro_unidad = '".$_REQUEST["fil_unidad"]."' ";
}  
if($_REQUEST["fil_categoria"] <> ""){
	$filtros .= " and pro.pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
}
if($_REQUEST["fil_cco"] <> ""){
	$filtros .= " and acf.acf_cco_id = '".$_REQUEST["fil_cco"]."' ";
}
if($_REQUEST["fil_ubicacion"] <> ""){
	$filtros .= " and acf.acf_ubicacion = '".$_REQUEST["fil_ubicacion"]."' ";
}   
if($_REQUEST["fil_tipo"] <> ""){
	$filtros .= " and pro.pro_tipo = '".$_REQUEST["fil_tipo"]."' ";
}  
if($_REQUEST["fil_responsable"] <> ""){
	$filtros .= " and acf.acf_responsable = '".$_REQUEST["fil_responsable"]."' ";
} 
if($_REQUEST["fil_estado"] <> ""){
	$filtros .= " and acf.acf_disponible = '".$_REQUEST["fil_estado"]."' ";
}
for($ie = 1;$ie <= 10;$ie++){
	if($_REQUEST["fil_extra_acf_".$ie] <> ""){
		$filtros .= " and acf.acf_extra_".$ie." like '".$_REQUEST["fil_extra_acf_".$ie]."' ";
	}
	if($_REQUEST["fil_extra_pro_".$ie] <> ""){
		$filtros .= " and pro.pro_extra_".$ie." like '".$_REQUEST["fil_extra_pro_".$ie]."' ";
	}
}
if($_REQUEST["fecha"] <> "" && $filtros <> ""){
	?> 
	<table class="table table-striped table-bordered tabledrag table-condensed" id="tablita_1"> 
	<thead>
		<tr>     
			<th>CodActivo</th> 
			<th>Descripcion</th>
			<th>CCosto</th> 
			<th>F.Ingreso</th>
			<th style="text-align:center;">Vida Util</th>
			<th>Doc.Ingreso</th> 
			<th style="text-align:right;">Valor Activo</th>
			<?
			if($_REQUEST["fil_moneda"] <> ""){
				?>
				<th style="text-align:right;">Tipo Cambio</th>
				<?
			}
			?>
			<th>F.InicioDep</th>
			<th>F.Baja</th>
			<th style="text-align:right;">Residual</th>
			<th style="text-align:right;">Valor para Dep</th>
			<th style="text-align:center;">Vida Util Remanente</th>
			<th style="text-align:right;">Cuota Mensual Dep</th>
			<th style="text-align:right;">Depreciacion Acumulada</th>
			<th style="text-align:right;">Valor neto en Libros</th>
		</tr>
	</thead>
	<tbody>
	<?php
	// Filtro si es que es moneda secundaria---------
	if($_REQUEST["fil_moneda"] <> ""){ 
		$filtros .= " and acf.acf_valor_sec > 0 ";
	}
	//-------------------------------------------------- 

	$res = sql_activos_fijos_rapido("*"," 
	and acf.acf_mes_ano_inicio_depreciacion <= '"._fec($_REQUEST["fecha"],9)."' $filtros ","",""); 
	$num = mysqli_num_rows($res);
	if($num > 0){
		while($row = mysqli_fetch_array($res)){

			// Ajustar la cuota de depreciación----------------------------------------
			$meses = $row["acf_vida_util"] * 1;
			if($row["acf_tipo_depreciacion"] == "L"){
				$meses_divisor = $meses;
			}else{
				$meses_divisor = round($meses / 3);
			}
			$valor_activo = round($row["acf_valor_a_depreciar"] * 1);
			$row["acf_cuota_depreciacion"] = ($valor_activo / $meses_divisor);
			//-------------------------------------------------------------------------
 
			$row["meses_transcurridos"] = calcula_meses_transcurridos($row["acf_mes_ano_inicio_depreciacion"],_fec($_REQUEST["fecha"],9)); 

			if($_REQUEST["fil_moneda"] <> ""){
				$row["acf_valor"] = ($row["acf_valor"] / $row["acf_valor_sec"]);
				$row["acf_valor_residual"] = ($row["acf_valor_residual"] / $row["acf_valor_sec"]);
				$row["acf_cuota_depreciacion"] = ($row["acf_cuota_depreciacion"] / $row["acf_valor_sec"]);
				$row["acf_valor_a_depreciar"] = ($row["acf_valor_a_depreciar"] / $row["acf_valor_sec"]);
				$decimales = $_configuraciones["cfg_moneda_sec_decimal"];
				$tipo_cambio = $row["acf_valor_sec"];
			}else{
				$tipo_cambio = 0;
				$decimales = $_configuraciones["cfg_moneda_decimal"];
			} 

			$meses_transcurridos = $row["meses_transcurridos"] * 1;
			if($meses_transcurridos > $row["acf_vida_util"]){
				$meses_transcurridos = $row["acf_vida_util"];
			}
			$meses_restantes = $row["acf_vida_util"] - $meses_transcurridos;

			$dep_acumulada_valor = $meses_transcurridos * $row["acf_cuota_depreciacion"];
			$neto = $row["acf_valor"] - $dep_acumulada_valor;
			?> 
			<tr>   
				<td><? echo $row["acf_codigo"]; ?></td>
				<td><? echo $row["pro_nombre"]; ?></td> 
				<td><? echo $_centro_costos_corto[$row["acf_cco_id"]]; ?></td> 
				<td><?php echo _fec($row["acf_fecha_ingreso"],5); ?></td>
				<td style="text-align: center;"><? echo $row["acf_vida_util"]; ?></td>  
				<td><?php echo $row["acf_nro_factura"]; ?></td> 
				<td style="text-align: right;"><?php echo _num($row["acf_valor"],$decimales); ?></td>
				<?
				if($_REQUEST["fil_moneda"] <> ""){ 
					?>
					<td style="text-align: right;"><?php echo _num($tipo_cambio,$decimales); ?></td>
					<?
				}
				?>
				<td><?php echo substr($row["acf_mes_ano_inicio_depreciacion"],0,7); ?></td> 
				<td><?php echo substr(_fec($row["acf_fecha_salida"],5),0,7); ?></td> 
				<td style="text-align: right;"><? echo _num($row["acf_valor_residual"],$decimales); ?></td>
				<td style="text-align: right;"><? echo _num($row["acf_valor_a_depreciar"],$decimales); ?></td>
				<td style="text-align: center;"><? echo $meses_restantes; ?></td> 

				<td style="text-align: right;"><? echo _num($row["acf_cuota_depreciacion"],3); ?></td>
				<td style="text-align: right;"><? echo _num($dep_acumulada_valor,$decimales); ?></td>
				<td style="text-align: right;"><? echo _num($neto,$decimales); ?></td>
			</tr>
			<?
			// Total completo
			$compras += $row["acf_valor"];
			$dep_acumulada += $dep_acumulada_valor;
			$dep_neto += $neto;

			// Totales por Tipo
			if(!in_array($row["pro_tipo"],$protipo)){
				$protipo[] = $row["pro_tipo"];
			}
			$AR_dep_compra[$row["pro_tipo"]] += $row["acf_valor"];
			$AR_dep_acum[$row["pro_tipo"]] += $dep_acumulada_valor;
			$AR_dep_neto[$row["pro_tipo"]] += $neto;
		} 
	} 
	?>
	<tr style="background-color:#FFFBD2;">
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th style="text-align: right;"><? echo _num($compras,$decimales); ?></th>
		<? 
		if($_REQUEST["fil_moneda"] <> ""){ 
			?><th></th><?
		}
		?>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th style="text-align: right;"><? echo _num($dep_acumulada,$decimales); ?></th>
		<th style="text-align: right;"><? echo _num($dep_neto,$decimales); ?></th>
	</tr> 
	</tbody> 
	</table>

	<div style="width: 450px;">
		<table class="table table-striped table-bordered tabledrag table-condensed" id="tablita_2">
		<thead>
		<tr>
			<th>Resumen Contable</th>  
			<th>Total Compras</th>  
			<th>Dep Acumulada</th> 
			<th>Val Neto en Libros</th> 
		</tr>
		</thead>
		<tbody>
		<?
		foreach($protipo as $ptipo){
			?>
			<tr>
				<td style="text-align: right;"><b><? echo $_tipo_activo[$ptipo]; ?></b></td>
				<td style="text-align: right;"><? echo _num($AR_dep_compra[$ptipo],$decimales); ?></td>
				<td style="text-align: right;"><? echo _num($AR_dep_acum[$ptipo],$decimales); ?></td>
				<td style="text-align: right;"><? echo _num($AR_dep_neto[$ptipo],$decimales); ?></td>
			</tr>	
			<? 
		} 
		?> 
		<tr style="background-color:#FFFBD2;">
			<th></th>
			<th style="text-align: right;"><? echo _num($compras,$decimales); ?></th>
			<th style="text-align: right;"><? echo _num($dep_acumulada,$decimales); ?></th>
			<th style="text-align: right;"><? echo _num($dep_neto,$decimales); ?></th>
		</tr> 
		</tbody> 
		</table>
	</div>

	<script language="javascript"> 
	$("#tablita_1").tableExport({
	    formats: ["xlsx"], //Tipo de archivos a exportar ("xlsx","txt", "csv", "xls")
	    position: 'button',  // Posicion que se muestran los botones puedes ser: (top, bottom)
	    bootstrap: false,//Usar lo estilos de css de bootstrap para los botones (true, false)
	    fileName: "Inf.Depreciacion",    //Nombre del archivo 
	    buttonContent: "Exportar Detalle a Excel",
	});
	$("#tablita_2").tableExport({
	    formats: ["xlsx"], //Tipo de archivos a exportar ("xlsx","txt", "csv", "xls")
	    position: 'button',  // Posicion que se muestran los botones puedes ser: (top, bottom)
	    bootstrap: false,//Usar lo estilos de css de bootstrap para los botones (true, false)
	    fileName: "Inf.Depreciacion.Resumen",    //Nombre del archivo
	    buttonContent: "Exportar Resumen a Excel",
	});
	</script>
	<?
}else{
	?>
	<div class="alert alert-info">
		Aplique algún filtro
	</div>
	<?
}
?>