<?php 
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition:  filename=\"inf_entradas_resumen.xls\";");

require("../../inc/conf_dentro.php");

$fechaInicio = strtotime(str_replace("/","-",$_REQUEST["desde"])) ;
$fechaFin = strtotime(str_replace("/","-",$_REQUEST["hasta"])) ;

if($_REQUEST["bodega"] <> ""){
	$filtros .= " and mov.mov_bodega = '$_REQUEST[bodega]' ";
}
if($_REQUEST["concepto"] <> ""){
	$filtros .= " and mov.mov_concepto = '$_REQUEST[concepto]' ";
}
if($_REQUEST["auxiliar"] <> ""){
	$filtros .= " and mov.mov_auxiliar = '$_REQUEST[auxiliar]' ";
}

// ---------------------------------------------------------------------------------------------------
if($_REQUEST["fil_codigo"] <> ""){
	$filtros .= " and pro.pro_codigo like '%".$_REQUEST["fil_codigo"]."%' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and pro.pro_nombre like '%".$_REQUEST["fil_descripcion"]."%' "; 
}
if($_REQUEST["fil_barra"] <> ""){
	$filtros .= " and pro.pro_codigo_barra like '%".$_REQUEST["fil_barra"]."%' ";
}
for($i = 1;$i <= 20;$i++){
	if($_REQUEST["fil_extra_".$i] <> ""){
		$filtros .= " and pro.pro_extra_".$i." like '%".$_REQUEST["fil_extra_".$i]."%' ";
	}
}
if($_REQUEST["fil_unidad"] <> ""){
	$filtros .= " and pro.pro_unidad = '".$_REQUEST["fil_unidad"]."' ";
}
if($_REQUEST["fil_grupo"] <> ""){
	$filtros .= " and pro.pro_grupo = '".$_REQUEST["fil_grupo"]."' ";
}
if($_REQUEST["fil_subgrupo"] <> ""){
	$filtros .= " and pro.pro_subgrupo = '".$_REQUEST["fil_subgrupo"]."' ";
}
if($_REQUEST["fil_categoria"] <> ""){
	$filtros .= " and pro.pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
}
if($_REQUEST["fil_tipo"] <> ""){
	$filtros .= " and pro.pro_tipo = '".$_REQUEST["fil_tipo"]."' ";
}
// ---------------------------------------------------------------------------------------------------

for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
	$dia[] = date("d/m", $i);

	$res = sql_movimientos_detalle("*,sum(det.det_ingreso) as det_ingreso"," 
	and con.con_tipo = 'E' and det.det_ingreso > 0 and mov.mov_fecha = '".date("Ymd", $i)."' $filtros   
	group by mov.mov_id,det.det_producto 
	order by mov.mov_fecha asc, mov.mov_folio asc"); 
	if(mysqli_num_rows($res) > 0){ 
		while($row = mysqli_fetch_array($res)){  
			if(!in_array($row["pro_codigo"],$productos)){
				$productos[] = $row["pro_codigo"];
			} 
			$cantidades[$row["pro_codigo"]] += $row["det_ingreso"];
			$descripcion[$row["pro_codigo"]] = $row["pro_nombre"];
		} 
	}
}
?>  
<table border="1" cellpadding="2" cellspacing="0"> 
<thead>
	<tr>    
		<th>Codigo</th>
		<th>Producto</th>
		<th style="text-align:right;">Cantidad</th>
	</tr>
</thead>
<tbody>
<?
foreach($productos as $pro){ 
	?>
	<tr>  
		<td><?php echo $pro; ?></td>
		<td><?php echo utf8_decode($descripcion[$pro]); ?></td>
		<td style="text-align:center;"><?php echo $cantidades[$pro]; ?></td>
	</tr>
	<? 
	$total += $cantidades[$pro];
}
?> 
</tbody>
<tfoot>
	<tr>
		<th colspan="2">TOTAL ITEMS</th>
		<th style="text-align:center;"><? echo $total; ?></th>
	</tr>
</tfoot>
</table> 