<?php
require("../../inc/conf_dentro.php");

switch($_REQUEST["modo"]){   
	
	case "codigo_unico":
		$res = sql_auxiliares("*"," and aux_codigo = '$_REQUEST[codigo]'"); 
		if(mysqli_num_rows($res) == 0){
			construir_boton("#","","check","Código Disponible",4);
			?>
			<script language="javascript">
            carg("aux_save.php","");
            </script>
			<?
		}else{
			construir_boton("#","","eliminar","Ya existe este código",4);
		}
		break;
		
	case "upload": 
		$archivo_name = "aux_".date("YmdHis")."_"._sa2($_FILES['archivo']['name']);

		$tmp_name 	= $_FILES['archivo']['tmp_name'];
		$size 		= $_FILES['archivo']['size'];
		$type 		= $_FILES['archivo']['type'];
		$error 		= $_FILES['archivo']['error']; 
		/**************************************************/
		$add="../../upload/".$_SESSION["key_id"]."/".$archivo_name; //file name where the file will be stored, upload is the directory 
		if(move_uploaded_file ($tmp_name,$add)){
			?> 
            <input type="hidden" id="imagen" name="imagen" class="campos" value="<?php echo $archivo_name; ?>">
			<?php
			chmod("$add",0777);
		}else{
			?> 
			<div class="alert alert-danger" style="margin-top:10px;">
				No se pudo subir la imagen
			</div> 
            <input type="hidden" id="imagen" name="imagen" class="campos">
			<?php
			chmod("$add",0777);
		}
		break;
		
	case "buscar":
		$res = sql_auxiliares("*"," and aux_codigo = '$_REQUEST[codigo]' "); 
		if(mysqli_num_rows($res) > 0){
			$row = mysqli_fetch_array($res); 
			?>
			<script language="javascript">
			document.getElementById("auxiliar").value = "<?php echo $row["aux_codigo"]; ?>";
			document.getElementById("auxiliar_nomb").value = "<?php echo $row["aux_nombre"]; ?>"; 
			</script>
			<?
		}else{
			?>
			<script language="javascript">
			alerta_js("No existe el Proveedor");
			document.getElementById("auxiliar").value = "";
			document.getElementById("auxiliar_nomb").value = ""; 
			</script>
			<?
		}
		break;
		
	case "busqueda":  
		if($_REQUEST["fma_codigo"] <> ""){
			$filtros .= " and aux.aux_codigo like '%".$_REQUEST["fma_codigo"]."%' ";
		}
		if($_REQUEST["fma_nombre"] <> ""){
			$filtros .= " and aux.aux_nombre like '%".$_REQUEST["fma_nombre"]."%' ";
		}
		$res = sql_auxiliares("*"," $filtros "); 
		$num = mysqli_num_rows($res);
		?>
		<h4 id="titulo_alerta"><? echo $num; ?> resultado(s) de Proveedor(es)</h4> 
        <?
		if($num > 0){
			?>
			<table class="table table-bordered table-hover table-condensed"> 
			<thead>
			<tr> 
				<th width="80">Opción</th>
				<th width="1">Código</th>
				<th>Nombre Proveedor</th>
			</tr>
			</thead>
			<tbody> 
			<?php 
			while($row = mysqli_fetch_array($res)){
				?>
				<tr>    
					<td><?
					construir_boton("elegir_auxiliar('$row[aux_codigo]','$row[aux_nombre]')","","derecha","Elegir",4); 
					?></td>
					<th><?php echo $row["aux_codigo"]; ?></th>
					<td><?php echo $row["aux_nombre"]; ?></td> 
				</tr>
				<?php 
			}
			?> 
			</tbody>
			</table>  
			<?php
		}else{
			?>
			<div class="alert alert-danger">
				<strong>Sin resultados</strong>
			</div>
			<?php 
		}
		break;
		
	case "opciones":  
		$res = sql_auxiliares("*"," and aux.aux_nombre like '%$_REQUEST[q]%' "); 
		$num = mysqli_num_rows($res); 
		if($num > 0){ 
			while($row = mysqli_fetch_array($res)){
				$data[] = array('id' => $row['aux_id'], 'text' => utf8_encode($row["aux_nombre"])); 
			}
			echo json_encode($data);
		}
		break;
		
	case "inicio":
		?>
		<h4 id="titulo_alerta">Filtros de Proveedores</h4> 
        <table class="table table-bordered table-hover table-condensed"> 
        <thead>
        <tr> 
            <th>Realice algún filtro</th>                        
        </tr>
        </thead>
        <tbody> 
        <tr>
            <td>
            <b>Código:</b><br />
            <input type="text" name="fma_codigo" id="fma_codigo" class="fma_campos w10" value="<? echo $_REQUEST["fma_codigo"]; ?>"></td>
        </tr> 
        <tr>
            <td>
            <b>Nombre:</b><br />
            <input type="text" name="fma_nombre" id="fma_nombre" class="fma_campos w10" value="<? echo $_REQUEST["fma_nombre"]; ?>"></td>
        </tr>
        </tbody>
        </table>
        <script language="javascript">
		$(document).ready(function(){
			$(".buscador").chosen({
				width:"95%",
				no_results_text:'Sin resultados con',
				allow_single_deselect:true 
			});
		});
		</script>
        <?
		construir_boton("fma_buscar()","","buscar","Buscar Proveedor",4);
				
		break;
		
	case "inicio_input":
		?> 
		<table width="100%">
		<tr>
			<td><?php input_auxiliar2($_REQUEST["codigo"]); ?></td>
			<td width="1"><?php construir_boton("crear_auxiliar();","1","crear","Crear",4); ?></td>
		</tr>	
		</table>
        <script language="javascript">
		$(document).ready(function(){
			$(".buscador").chosen({
				width:"95%",
				no_results_text:'Sin resultados con',
				allow_single_deselect:true 
			});
		});
		</script>
        <?
		break;
		
	case "crear_form":
		?>
		<h4 id="titulo_alerta">Crear Proveedor (Modo express)</h4>
		<div id="lugar_aux_save"></div>		 
        <table class="table table-bordered table-condensed"> 
	    <tr>
	        <th width="80">Código:</th>
	        <td><input type="text" name="aux_codigo" id="aux_codigo" class="campos w10" style="text-align:center; width:130px !important;"></td>
	    </tr> 
	    <tr>
	        <th>RUT:</th>
	        <td><input type="text" name="aux_rut" id="aux_rut" class="campos w10" style="text-align:center; width:130px !important;"></td>
	    </tr>    
	    <tr>
	        <th>Nombre:</th>
	        <td><input type="text" name="aux_nombre" id="aux_nombre" class="campos w10"></td> 
	    </tr>  
	    </table>
	    <script type="text/javascript">
	    $(document).ready(function() {
			$("#aux_rut").Rut({
				format_on: 'keyup',
			   on_error: function(){
					alert('El rut ingresado es incorrecto');
					document.getElementById("aux_rut").value = "";
			   }
			}); 
		});
	    </script>
        <?
		construir_boton("save_auxiliar()","","grabar","Guardar Proveedor",4);
		?><br><br><?		
		break;  
	
	case "crear_rapido": 
		$res = sql_auxiliares("*"," and aux_codigo = '$_REQUEST[aux_codigo]'"); 
		if(mysqli_num_rows($res) == 0){
			$sql = "INSERT INTO auxiliares ( 
				aux_codigo,
				aux_rut,
				aux_nombre 
			) VALUES ( 
				'$_POST[aux_codigo]',
				'$_POST[aux_rut]', 
				'$_POST[aux_nombre]' 
			)"; 
			$res = mysqli_query($cnx,$sql); 
			$id = mysqli_insert_id($cnx);
			?>
			<script language="javascript">
            $('#modGeneral').modal('hide');
            input_auxiliar('<? echo $id; ?>');
            </script>
			<?
		}else{
			?>
			<div class="alert alert-danger">
				Ya existe este código
			</div>
			<?
		}
		break;
}
?> 