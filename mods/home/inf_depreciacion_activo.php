<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Depreciación por Activo Fijo en Detalle (1)";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------  
?>
<script type="text/javascript">
function filtrar(){
	var a = $(".campos").fieldSerialize();
	var div = document.getElementById("datos");
	AJAXPOST("mods/home/inf_depreciacion_activo_datos.php",a,div);
}
</script>

<table class="table table-bordered"> 
<tr>
    <th width="1">Activo:</th>
    <td>
    <select name="activo" id="activo" class="campos buscador">
    <option value=""></option>
	<?php
	// Sacar los productos con movimiento
	$res = sql_activos_fijos("*"," and pro.pro_tipo in ($_tipo_activos_depreciables) ORDER BY acf.acf_producto asc, acf.acf_serie asc",""); 
	$num = mysqli_num_rows($res);
	if($num > 0){
		while($row = mysqli_fetch_array($res)){
			?><option value="<?php echo $row["acf_id"]; ?>" <?php
			if($row["acf_id"] == $_REQUEST["activo"]){ echo "selected"; $datos = $row; } ?>><?php
			echo $row["acf_codigo"]." - ".$row["pro_nombre"]." | CodProd: ".$row["pro_codigo"]." | Serie: ".$row["acf_serie"];  
			?></option><?php 
		}
	}
    ?>
    </select></td> 
    <td width="1"><?
	construir_boton("filtrar()","","buscar","Filtrar",4);  
	?></td>
</tr> 
</table>

<div id="datos"></div>