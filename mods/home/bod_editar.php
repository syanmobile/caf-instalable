<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Editar Lugar";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

$res = sql_bodegas("*"," and bod_id = '$_REQUEST[id]' ");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<strong>No se encontró el dato</strong>
	</div>
    <?php
	construir_boton("bod_listado.php","","buscar","Listado de lugares",2);
	exit();
}
?>
<script language="javascript">
function save(){
	if(document.getElementById("nombre").value == ""){
		alerta_js("Es obligación ingresar el nombre");
		return;	
	}
	carg("bod_save.php","");
}
</script> 
 
<input type="hidden" name="modo" value="editar" class="campos"> 
<input type="hidden" name="id" value="<? echo $_REQUEST["id"]; ?>" class="campos">   

<table width="100%">
	<tr valign="top">
		<td width="400">
			<table class="table  table-bordered"> 
			<tbody>
				<tr>
					<th width="120" style="text-align:right;">Código:</th>
					<td>
					<input type="text" class="campos" disabled value="<? echo $datos["bod_codigo"]; ?>">
					<input type="hidden" name="codigo" class="campos" value="<?php echo $datos["bod_codigo"]; ?>"> 
					</td>
				</tr>
				<tr>
					<th style="text-align:right;">Nombre:</th>
					<td><input type="text" class="campos" id="nombre" name="nombre" value="<? echo $datos["bod_nombre"]; ?>"></td>
				</tr>  
			</tbody>
			</table>
			<?php
			construir_boton("","","grabar","Guardar",3);
			construir_boton("bod_listado.php","","eliminar","Cancelar",2);
			?>  
		</td>  
		<td style="padding-left: 10px;"> 
			<table class="table table-bordered"> 
			<tbody>
				<tr>
					<th colspan="3">Marque quien tiene acceso:</th>
				</tr>
				<?
				$res_pfl = sql_personas("*"," ORDER BY per.per_nombre asc");
				if(mysqli_num_rows($res_pfl) > 0){
					while($row_pfl = mysqli_fetch_array($res_pfl)){
						?>
						<tr>
							<td width="1"><input type="checkbox" value="<?php echo $row_pfl["per_id"]; ?>" name="persona[]" class="campos" <?
							if(mysqli_num_rows(mysqli_query($cnx,"select * from personas_bodegas where per_id = '".$row_pfl["per_id"]."' and bod_id = '".$datos["bod_id"]."' ")) > 0){
								echo "checked"; 
							}
							?>></td>
							<th width="1"><?php echo $row_pfl["per_rut"]; ?></th>
							<td><?php echo $row_pfl["per_nombre"]; ?></td>
						</tr>
						<?php
					}
				}
				?>
			</tbody>
			</table> 
		</td>
	</tr>
</table>