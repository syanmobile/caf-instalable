<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabación Unidad de Medida";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

switch($_POST["modo"]){
	case "crear":
		$res = sql_unidades("*"," and uni.uni_codigo = '$_REQUEST[codigo]'","","");
		if(mysqli_num_rows($res) > 0){
			?>
            <div class="alert alert-danger"> 
               <b>ERROR</b>, código ya existe: '<strong><? echo $_POST["codigo"]; ?></strong>'
            </div>
            <?
		}else{ 
			$sql = "INSERT INTO unidades ( 
				uni_codigo,
				uni_nombre 
			) VALUES ( 
				'$_POST[codigo]',
				'$_POST[nombre]' 
			)";
			$res = mysqli_query($cnx,$sql);
			$_POST["id"] = mysqli_insert_id($cnx);
			?>
			<div class="alert alert-success"> 
			
				<strong>Unidad de Medida '<? echo $_POST["nombre"]; ?>' creado con &eacute;xito</strong>
			</div>
			<?php 
		}
		break;
		
	case "editar":
		$SQL_ = "UPDATE unidades SET 
			uni_nombre = '$_POST[nombre]' 
		WHERE uni_id = '$_POST[id]' ";   
		$res = mysqli_query($cnx,$SQL_);
		?>   
        <div class="alert alert-success"> 
           Unidad de Medida <strong>'<? echo $_POST["nombre"]; ?>'</strong> editado con &eacute;xito
        </div>  
		<?php  
		break;
}

construir_boton("uni_listado.php","","buscar","Listado Unidades",2);
if($_REQUEST["modo"] <> "eliminar"){
	construir_boton("uni_editar.php","&id=".$_POST["id"],"editar","Editar esta Unidad de Medida",2);
}
construir_boton("uni_nuevo.php","","crear","Crear otra Unidad de Medida ",2);
?>