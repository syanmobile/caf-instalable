<?php 
require("../../inc/conf_dentro.php");
			
construir_boton("vista('');","","buscar","Ver Todo",4);
construir_boton("vista('rojo');","","buscar","Fallas",4);
construir_boton("vista('ok');","","buscar","Registros OK",4);
?>
<table class="table table-bordered table-hover table-condensed"> 
<thead> 
    <tr>
        <th width="1">#</th>
        <th width="1">Código</th>
        <th>Nombre</th>
        <th>Grupo</th>
        <th>Validación</th>
    </tr>
</thead>
<tbody>
<?php 
$capt = date("YmdHis");
if (isset($_FILES["archivo"]) && is_uploaded_file($_FILES['archivo']['tmp_name'])) {							
    $fp = fopen($_FILES['archivo']['tmp_name'], "r");
    while (!feof($fp)){
		$data = explode(";", fgets($fp));  
		$nlinea++;
		if($nlinea > 1){  
			if(count($data) > 1){  
				$codigo = rtrim($data[0]);
				$nombre = rtrim($data[1]);
				$cgrup = rtrim($data[2]);
				$total_campos = 3;

				//Validaciones
				$errores = ""; 
				if($codigo == "" || $nombre == "" || $cgrup == ""){ 
					$errores.= "<li>Un campo obligatorio no ingresado</li>";
				}
				if(subgrupo_existe($codigo)){ 
					$errores.= "<li>Subgrupo ya existe</li>";
				}
				if(!grupo_existe($cgrup)){ 
					$errores.= "<li>No existe el grupo</li>";
				}
				$linea++;
				?>
				<tr class="fila_<?php echo ($errores <> "")?"rojo":"ok"; ?>">
					<td><? echo $linea; ?></td>
					<?
					for($i = 0;$i < $total_campos;$i++){
						?><td><? echo _u8e($data[$i]); ?></td><?
					}

					if($errores <> ""){
						$fallo = "S";
						?><td style="padding-left:15px;" class="alert alert-danger"><? echo $errores; ?></td><?php
					}else{
						?><td style="padding-left:15px;"  class="alert alert-success"><li>Todo OK</li></td><?php
					}
					?>
				</tr>
				<?  
			}
		}// nlinea > 1
    }
} 
?>
</tbody>
</table>

<script language="javascript"> 
function grabar(){
    <?php
    if($fallo == ""){
        ?>
        document.multiple_upload_form.action = "<?php echo $url_web; ?>mods/home/sgru_importar_grabacion.php";
        $('#multiple_upload_form').ajaxForm({
            target:'#procesando_upload',
            beforeSubmit:function(e){
                $('.uploading').show();
            },
            success:function(e){
                $('.uploading').hide();
            },
            error:function(e){
            }
        }).submit();
        <?
    }else{
        ?>
		alert("Deben estar todos los datos validados");
        <?
    }
    ?>
}
</script> 