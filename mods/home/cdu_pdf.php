<?php
ob_start(); 
require("../../inc/conf_dentro.php"); 

// para utilizar la librería
define('FPDF_FONTPATH','font/');
require_once('../../pdf/fpdf.php');

// Parametros pdf
$pdf=new FPDF("L","mm",array(102,40));
$pdf->AliasNbPages();

$pdf->AddPage();

$nombre_empresa = _opc("nombre_empresa");

$cdus = $_REQUEST["cdu"];
foreach($cdus as $datos){
	$cdu = explode(",",$datos);
	
	foreach($cdu as $codigo){
		if($codigo <> ""){ 
			
			$xhoja++;
			if($xhoja > 2){
				$pdf->AddPage();
				$xhoja = 1;
				$ancho = 0;
			} 	 

			$pdf->Ln(4);

			$pdf->SetFont('Arial','b',10); 
			$pdf->Text(10 + $ancho,10,$nombre_empresa);
			$pdf->SetFont('Arial','',10); 
			$pdf->Text(10 + $ancho,25,$codigo);   
			$pdf->Image($url_base.'pdf/image.php?code=code128&o=1&dpi=72&t=30&r=1&rot=0&text='.$codigo.'&f1=-1&f2=8&a1=&a2=NULL&a3=',10 + $ancho,12,32,9,'PNG');
			
			$ancho+= 45;
		} 
	} 
}
$pdf->Output(); 
?>