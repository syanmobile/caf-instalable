<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabación Salida"; 
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

switch($_POST["modo"]){
		
	case "editar":
		$sql = "delete from movimientos where mov_id = ".$_REQUEST["id"];
		$res = mysqli_query($cnx,$sql);
		$sql = "delete from movimientos_detalle where det_mov_id = ".$_REQUEST["id"];
		$res = mysqli_query($cnx,$sql); 
		// No hay un break porque la idea es ingresar otro documento una vez eliminado el que se edito
	case "crear": 
		$sql = "INSERT INTO movimientos ( 
			mov_per_id,
			mov_tipo, 
			mov_concepto,
			mov_bodega, 
			mov_folio,
			mov_fecha,
			mov_auxiliar,
			mov_responsable,
			mov_extra_1,
			mov_extra_2,
			mov_extra_3,
			mov_extra_4,
			mov_extra_5,
			mov_glosa,
			mov_total 
		) VALUES (  
			'".$_SESSION["per_conectado"]["per_id"]."',
			'$_POST[tipo]',
			'$_POST[concepto]',
			'$_POST[bodega]', 
			'$_POST[folio]',
			'"._fec($_POST["fecha"],9)."',
			'$_POST[auxiliar]', 
			'$_POST[responsable]', 
			'$_POST[extra_1]',
			'$_POST[extra_2]',
			'$_POST[extra_3]',
			'$_POST[extra_4]',
			'$_POST[extra_5]',
			'$_POST[glosa]',
			'$_POST[total]' 
		)";
		$res = mysqli_query($cnx,$sql);
		$movi = mysqli_insert_id($cnx);
		
		$prod_det = explode("*****",$_REQUEST["prod_det"]);
		$lote_det = explode("*****",$_REQUEST["lote_det"]);
		$cant_det = explode("*****",$_REQUEST["cant_det"]);
		$prec_det = explode("*****",$_REQUEST["prec_det"]);
		for($i = 1;$i < count($prod_det);$i++){			
			
			if($cant_det[$i] > 0){ /************************************************************************************/
				
				$resp = sql_productos("*","and pro.pro_codigo = '".$prod_det[$i]."'");  
				if(mysqli_num_rows($resp) > 0){
					$producto = mysqli_fetch_array($resp);
				}
				
				if($lote_det[$i] == ""){
					$subtotal = $cant_det[$i] * $prec_det[$i];
					$sql3 = "INSERT INTO movimientos_detalle (
						det_mov_id, 
						det_producto,
						det_egreso,
						det_valor,
						det_total 
					) VALUES (
						'$movi', 
						'".$producto["pro_id"]."',
						'".$cant_det[$i]."',
						'".$prec_det[$i]."',
						'$subtotal'
						
					)"; 
					$res3 = mysqli_query($cnx,$sql3);  
				}else{
					$lotes = explode(",,,,,",$lote_det[$i]);
					if(count($lotes) > 1){
						for($i2 = 1;$i2 < count($lotes);$i2++){
							$info = explode("_____",$lotes[$i2]); 
							
							$subtotal = $info[1] * $prec_det[$i];					
							$sql3 = "INSERT INTO movimientos_detalle (
								det_mov_id, 
								det_producto,
								det_egreso,
								det_valor,
								det_total,
								det_lote  
							) VALUES (
								'$movi',  
								'".$producto["pro_id"]."',
								'1',
								'".$prec_det[$i]."',
								'".$prec_det[$i]."',
								'".$info[0]."' 
							)";  
							$res3 = mysqli_query($cnx,$sql3);  
							
							// Cambia estado al Activo a Baja							
							$res3 = sql_activos_fijos("*","and acf_producto = '".$producto["pro_id"]."' 
							and acf_serie = '".$info[0]."'");
							if(mysqli_num_rows($res3) > 0){
								$row3 = mysqli_fetch_array($res3); 
								
								$sql4 = "update activos_fijos set 
								acf_disponible = '4' ,
								acf_fecha_salida = '".date("Y-m-d")."'
								where acf_id = '".$row3["acf_id"]."' ";
								$res4 = mysqli_query($cnx,$sql4); 
							}
						}
					}
				}
			} /************************************************************************************/
		}
		?>
        <table width="100%">
        <tr valign="top">
            <td width="300">
                <div class="alert alert-success"> 
        			<span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="font-size:30px; float:left; margin-right:10px;"></span>
                    Documento de Salida guardado con &eacute;xito<br />
                    <b style="font-size:20px;"><? echo $_POST["tipo"]."/".$_POST["folio"]; ?></b>
                </div>
                <?php 
				construir_boton("transacciones.php","","buscar","Volver a Transacciones",2); 
				construir_boton("sal_nuevo.php","","crear","Crear Otro Documento de Salida",2);
				?>
            </td>
            <td style="padding-left:15px;">
            	<iframe src="<?php echo "mods/home/aju_pdf.php?id=".$movi; ?>" width="100%" height="500"></iframe>
            </td>
        </tr>
        </table> 
		<?php   
		break; 
} 
?>