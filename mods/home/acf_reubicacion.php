<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Reubicación de Activos Fijos";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "acf_reubicacion.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function save(){  
	if(document.getElementById("glosa").value == ""){
		alerta_js("Es obligación ingresar NOTAS");
		return;	
	}
	if(document.getElementById("destino").value == ""){ 
		alert_js("Indique la ubicación de destino");
		return;
	}
	if(confirm("Grabar Documento?")){
		carg("acf_save.php","");
	}
}
function cargar_productos(){ 
	if(document.getElementById("ubicacion").value == ""){ 
		$("#disponibles").html("");
	}else{  
		var div = document.getElementById("disponibles"); 
		var a = $(".campos").fieldSerialize();  
		$("#disponibles").html("<h4>Cargando activos fijos, espere un momento...</h4>");
		AJAXPOST("mods/home/acf_reubicacion_ajax.php",a+"&modo=disponibles",div);
	}
} 
</script>

<table style="margin-bottom:5px; width:100%">
<tr valign="top"> 
    <td width="400"> 
    	<input type="hidden" name="modo" value="reubicacion" class="campos" />
    	<input type="hidden" name="tipo" id="tipo" value="A" class="campos" /> 
    	<input type="hidden" name="concepto" id="concepto" value="5" class="campos" /> 
    	<input type="hidden" name="fecha" value="<? echo date("d/m/Y"); ?>" class="campos"> 
    	
		<table class="table table-striped table-bordered table-condensed"> 
        <thead>
            <tr>
                <th colspan="4">Información Movimiento</th>
            </tr>
        </thead>
        <tbody> 
		    <tr>
		        <th width="110" style="text-align:right;">Folio</th>
		        <td><input type="text" value="Autom." disabled class="campos w5"></td>
		    </tr> 
		    <tr>
		        <th style="text-align:right;">Ubicación Actual:</th>
		        <td>
		            <select name="ubicacion" id="ubicacion" class="campos buscador w10" onchange="javascript:cargar_productos();">
					<option value=""></option>
					<?php
					$res = sql_ubicaciones("*"," order by bod.bod_nombre asc, ubi.ubi_nombre asc"); 
					if(mysqli_num_rows($res) > 0){
						while($row = mysqli_fetch_array($res)){
							?>
							<option value="<? echo $row["ubi_id"]; ?>" <?
							if($_REQUEST["fil_ubicacion"] == $row["ubi_id"]){ echo "selected"; } 
							?>>U<? echo $row["ubi_codigo"]." - ".$row["ubi_nombre"]; ?></option>
							<?
						}
					}
					?></select>
		        </td>
		    </tr> 
            <tr>
                <th style="text-align:right;">Ubicación Nueva:</th>
                <td>
                    <select name="destino" id="destino" class="campos buscador">
                    <option value=""></option>
                    <?php
                    $res = sql_ubicaciones("*"," ORDER BY bod.bod_nombre asc, ubi.ubi_nombre asc","","");
                    if(mysqli_num_rows($res) > 0){
                        while($row = mysqli_fetch_array($res)){
                            ?><option value="<? echo $row["ubi_id"]; ?>" <?
                            if($row["ubi_id"] == $datos["acf_ubicacion"]){ echo "selected"; } 
							?>>U<? echo $row["ubi_codigo"]." - ".$row["ubi_nombre"]; ?></option>
                            <?
                        }
                    }
                    ?>
                    </select>
				</td>
            </tr> 
            <tr>
                <th style="text-align:right;">Autoriza:</th> 
				<td>
				<select name="responsable" id="responsable" class="campos buscador">
				<option value=""></option>
				<?php 
				$res = sql_personas("*"," and per.per_tipo = 1 and per.per_elim = 0  ORDER BY per.per_nombre asc ");  
				if(mysqli_num_rows($res) > 0){
					while($row = mysqli_fetch_array($res)){
						?>
						<option value="<? echo $row["per_id"]; ?>"><?php echo $row["per_nombre"]; ?></option>
						<?
					}
				} 
				?>
				</select>
				</td>   
			</tr> 
            <tr>
                <th style="text-align:right;">Notas:</th>
                <td><input type="text" name="glosa" id="glosa" class="campos w10" value="Reubicación de activos"></td>
            </tr> 
            <?php 
			for($i = 1;$i <= 5;$i++){
				if(_opc("extra_mov_".$i) > 0){
					$res = sql_campos("*"," and cam_id = "._opc("extra_mov_".$i));  
					$row = mysqli_fetch_array($res); 
					?> 
					<tr> 
						<th style="text-align: right;"><? echo $row["cam_nombre"]; ?>:</th>
						<td><? 
			            campo_dinamico($row["cam_tipo"],"extra_".$i,$row["cam_opciones"],"",$url_base);
			            ?></td> 
					</tr> 
					<?php
				}
			} 
			?> 
        </tbody>
        </table>    
		<?php 
        construir_boton("","","grabar","Guardar Movimiento",3); 
        ?>
		<br><br>
   		<div id="operacion"></div>	
    </td> 
	<td style="padding-left:10px;" id="disponibles"> 
    </td>
</tr>
</table> 