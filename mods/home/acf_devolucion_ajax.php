<?php
require("../../inc/conf_dentro.php");
 
switch($_REQUEST["modo"]){
	case "devolver": 
		?>		
		<input type="hidden" name="activos[]" value="<?php echo $_REQUEST["acf"]; ?>" class="campos produ">
		<table class="table table-striped table-bordered tabledrag table-condensed"> 
            <tr>
            	<td>
            	<b>¿El activo quedara disponible?:</b><br>
				<select name="disponible[]" class="campos w10">
					<option value="1">SI</option>
					<option value="0">NO, dejar en estado: NO DISPONIBLE</option>
					<option value="5">NO, dejar en estado: REQUIERE MANTENIMIENTO</option>
				</select>
           		</td>
            </tr>
			<tr>
				<td>
				<b>Ingrese alguna observación:</b><br>
				<input type="text" name="notas[]" value="" class="w10 campos"></td>
			</tr>
		</table> 
		<?php
		construir_boton("cancelar('".$_REQUEST["id"]."')","","izquierda","Atrás",4);
		break; 
		
	case "extravio": 
		?>		
		<div class="alert alert-danger">Si el activo fijo esta extraviado, se deberá crear un documento de salida como BAJA de Activo Fijo</div> 
		<?php
		construir_boton("cancelar('".$_REQUEST["id"]."')","","izquierda","Atrás",4);
		break; 
		
	case "pendientes": 
		$res = sql_activos_fijos("*"," and acf_disponible = 2 and ubi_bodega = '$_REQUEST[bodega]' and acf_responsable = '".$_REQUEST["respo"]."' order by acf.acf_producto asc, acf.acf_serie asc"); 
		if(mysqli_num_rows($res) > 0){
			?>
			<table class="table table-striped table-bordered tabledrag table-condensed" id="tablita"> 
			<thead>
				<tr>    
					<th colspan="2">Producto</th>
                    <th>Operación</th> 
				</tr>
			</thead>
			<tbody> 
			<?
			while($row = mysqli_fetch_array($res)){ 
				$i++;
				?>
				<tr>  
					<td><? 
					$imagen = ($row["pro_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$row["pro_imagen"];
					?><img src="<?php echo $imagen; ?>" style="width:80px !important;"></td>
					<td width="350">
						<b><?php echo $row["pro_nombre"]; ?></b><br />
						<strong>Cod.Activo</strong>: <? echo $row["acf_codigo"]; ?><br />
						<strong>Cod.Producto</strong>: <? echo $row["pro_codigo"]; ?><br />
						<strong>Cod.Barra</strong>: <?php echo $row["pro_codigo_barra"]; ?><br />					
						<strong>Serie</strong>: <? echo $row["acf_serie"]; ?><br />					
						<strong>Ubicación</strong>: <? echo $row["ubi_nombre"]; ?>
                    </td>
                    <td>
						<div id="btnes_<?php echo $i; ?>">
						<?php
						construir_boton("devolver('".$row["acf_id"]."','$i')","","check","Devolver a Stock",4);
						?> 
						</div>
						<div id="lugar_<?php echo $i; ?>" style="display: none;">
							<div id="operacion_<?php echo $i; ?>"></div>	
						</div>
                    </td>
				</tr>
				<? 
			}
			?>
			</tbody>
			</table> 
			<?
		}else{
			?>
			<div class="alert alert-danger">
				<strong>Sin registros creados</strong>
			</div>
			<?php 
		} 
		break; 
}
?> 