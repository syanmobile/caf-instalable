<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Listado Grupos";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "grup_listado.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>
<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td><?php
		construir_boton("grup_nuevo.php","","crear","Nuevo Grupo",2); 
		?></td>
    </tr>
</tbody>
</table>
<?php
$res = sql_grupos("*","   ORDER BY gru.gru_nombre asc","","");
if(mysqli_num_rows($res) > 0){
    
    ?>
    <table class="table table-striped table-bordered table-condensed"> 
    <thead>
    <tr>
        <th width="1" align="center">&nbsp;</th>
        <th width="1" align="center">Código</th>
        <th>Grupo</th>
    </tr>
    </thead>
    <tbody> 
    <?
    while($row = mysqli_fetch_array($res)){
        ?>
        <tr fila="<?php echo $row["gru_id"] ;?>" id="<?php echo $row["gru_id"] ;?>">  
            <td style="text-align:center;"><?php
			construir_boton("grup_editar.php","&id=".$row["gru_id"],"editar","Editar");
			?></td>
            <th style="text-align: center"><?php echo $row["gru_codigo"]; ?></th>
            <td><?php echo $row["gru_nombre"]; ?></td>
        </tr>
        <? 
    }
    ?> 
    </tbody>
    </table>   
    <?php
    
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin registros creados</strong>
    </div>
    <?php
}
?>