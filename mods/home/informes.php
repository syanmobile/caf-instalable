<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Informes ";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script type="text/javascript">
function descargar(pagina){
	var div = document.getElementById("descargar_lugar");
	AJAXPOST("mods/home/"+pagina,"",div);
}
</script>
<?
//$grupos_1[] = array("inf3_ver","inf_activos_x_grusubgrupo.php","Activos por Grupo/Subgrupo","grupos","Informe Activos por Grupo/Subgrupo");
$grupos_1[] = array("inf4_ver","inf_activos_x_ubicacion.php","Productos por Ubicación","if_map-marker_299087",
	"Informe Productos por Ubicación");
$grupos_1[] = array("inf5_ver","inf_activos_x_categorias.php","Productos por Clasificación","categorias",
	"Informe Productos por Clasificación"); 
$grupos_1[] = array("inf16_ver","inf_activos_prestados_asignados.php","Activos Prestados y Asignados",
	"if_BT_line_add_905638","Informe Activos Prestados y Asignados");
$grupos_1[] = array("inf18_ver","inf_activos_con_responsables.php","Activos con Responsables",
	"move_waiting_to_participant_green","Informe Activos con Responsables");


$grupos_2[] = array("inf14_ver","inf_mantenimientos.php","Mantenimientos de Activos Fijos",
	"mood_level_green","Informe Mantenimientos de Activos Fijos");
$grupos_2[] = array("inf15_ver","inf_vencimientos_garantias.php","Vencimientos de Garantías",
	"iconfinder_10_330408","Informe Vencimientos de Garantías");
$grupos_2[] = array("inf17_ver","inf_vencimientos_mantenimientos.php","Vencimientos de Mantenimientos",
	"iconfinder_10_330408","Informe Vencimientos de Mantenimientos");


$grupos_3[] = array("inf19_ver","inf_depreciacion_activo.php","Depreciación por Activo Fijo en Detalle",
	"money","Informe Depreciación por Activo Fijo en Detalle");
$grupos_3[] = array("inf20_ver","inf_depreciacion_del_periodo.php","Depreciación Contable del Período",
	"money","Informe Depreciación Contable del Período");
$grupos_3[] = array("inf23_ver","inf_de_depreciacion_por_periodo.php","Depreciación por Período",
	"money","Informe Depreciación por Período");


$grupos_4[] = array("inf8_ver","inf_insumos_x_ubicacion.php","Insumos por Ubicación",
	"if_map-marker_299087","Informe Insumos por Ubicación");
$grupos_4[] = array("inf9_ver","inf_insumos_x_categorias.php","Insumos por Clasificación",
	"categorias","Informe Insumos por Clasificación");
$grupos_4[] = array("inf11_ver","inf_insumos_stock_critico.php","Stock Crítico de Insumos",
	"flag_green","Informe Stock Crítico de Insumos");


$grupos_5[] = array("inf12_ver","inf_kardex_producto.php","Kardex Producto",
	"producto_buscar","Informe Kardex Producto");
$grupos_5[] = array("inf13_ver","transacciones.php","Histórico de Transacciones",
	"salidas","Informe Histórico de Transacciones");
$grupos_5[] = array("inf26_ver","inf_bitacora_persona.php","Bitácora Personal",
	"salidas","Informe de la Persona para ver su situación actual");


$grupos_6[] = array("inf21_ver","inf_altas.php","Informe Altas",
	"report","Informe de Altas de Activos fijos"); 
$grupos_6[] = array("inf22_ver","inf_bajas.php","Informe Bajas",
	"report","Informe de Bajas de Activos fijos");
$grupos_6[] = array("inf1_ver","inf_entradas.php","Resumen de Ingresos por Período",
	"report","Informe Resumen de Ingresos por Período"); 
$grupos_6[] = array("inf2_ver","inf_salidas.php","Resumen de Salidas por Período",
	"report","Informe Resumen de Salidas por Período");


$grupos_7[] = array("inf24_ver","inf_de_depreciacion_por_periodo_trib.php","Depreciación por Período Tributario",
	"money","Informe Depreciación por Período Tributario");


$grupos_8[] = array("inf25_ver","inf_todos.php","Activos Fijos Cargados","importar");
?>
<style type="text/css">
body{ background-color:#EAEAEA !important; } 
.boton_inicio{  
	border-top: 1px solid #CCC;
	width: 100%; 
}
.boton_inicio a{
	color:#000 !important;
	text-decoration:none;
}
.boton_inicio span{
	font-size:11px; 
	font-weight:bold;
}
.boton_inicio:hover{  
	background-color:#FFC;
}
a:hover .span{ 
	font-size:13px !important; 
}
</style> 
<div class="row"> 
	<div class="col-md-3 grid-item">
		<?php
		$tit = "";
		foreach($grupos_1 as $informe){
			if(in_array($informe[0],$opciones_persona)){
				if($tit == ""){
					?>
					<div class="panel panel-primary">  
					<div class="panel-heading">
						<b>Informes de Activos y Productos</b>
					</div> 
					<div class="panel-body" style="padding:0px;">
					<?
				}
				$tit = "S";
				?>
				<div class="boton_inicio">
					<a href="javascript:carg('<? echo $informe[1]; ?>','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="45" style="text-align:center"><img src="img/modulos/<? echo $informe[3]; ?>.png" width="40"></td> 
						<td>
						<span><? echo $informe[2]; ?></span>
						<p><? echo $informe[4]; ?></p>
						</td>
					</tr>
					</table>
					</a> 
				</div>
				<? 
			}
		}
		if($tit <> ""){
			echo "</div></div>";
		} 
		?>
	</div>
	<div class="col-md-3 grid-item"> 
		<?php
		$tit = "";
		foreach($grupos_2 as $informe){
			if(in_array($informe[0],$opciones_persona)){
				if($tit == ""){
					?>
					<div class="panel panel-primary">  
					<div class="panel-heading">
						<b>Informes de Control</b>
					</div> 
					<div class="panel-body" style="padding:0px;">
					<?
				}
				$tit = "S";
				?>
				<div class="boton_inicio">
					<a href="javascript:carg('<? echo $informe[1]; ?>','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="45" style="text-align:center"><img src="img/modulos/<? echo $informe[3]; ?>.png" width="40"></td> 
						<td>
						<span><? echo $informe[2]; ?></span>
						<p><? echo $informe[4]; ?></p>
						</td>
					</tr>
					</table>
					</a> 
				</div>
				<? 
			}
		} 
		if($tit <> ""){
			echo "</div></div>";
		}
		?> 
	</div>
	<div class="col-md-3 grid-item"> 
		<?php
		$tit = "";
		foreach($grupos_6 as $informe){
			if(in_array($informe[0],$opciones_persona)){
				if($tit == ""){
					?>
					<div class="panel panel-primary">  
					<div class="panel-heading">
						<b>Informes de Consolidados</b>
					</div> 
					<div class="panel-body" style="padding:0px;">
					<?
				}
				$tit = "S";
				?>
				<div class="boton_inicio">
					<a href="javascript:carg('<? echo $informe[1]; ?>','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="45" style="text-align:center"><img src="img/modulos/<? echo $informe[3]; ?>.png" width="40"></td> 
						<td>
						<span><? echo $informe[2]; ?></span>
						<p><? echo $informe[4]; ?></p>
						</td>
					</tr>
					</table>
					</a> 
				</div>
				<? 
			}
		}  
		if($tit <> ""){
			echo "</div></div>";
		}
		?> 
	</div>
	<div class="col-md-3 grid-item"> 
		<?php
		$tit = "";
		foreach($grupos_4 as $informe){
			if(in_array($informe[0],$opciones_persona)){
				if($tit == ""){
					?>
					<div class="panel panel-primary">  
					<div class="panel-heading">
						<b>Informes de Insumos</b>
					</div> 
					<div class="panel-body" style="padding:0px;">
					<?
				}
				$tit = "S";
				?>
				<div class="boton_inicio">
					<a href="javascript:carg('<? echo $informe[1]; ?>','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="45" style="text-align:center"><img src="img/modulos/<? echo $informe[3]; ?>.png" width="40"></td> 
						<td>
						<span><? echo $informe[2]; ?></span>
						<p><? echo $informe[4]; ?></p>
						</td>
					</tr>
					</table>
					</a> 
				</div>
				<? 
			}
		} 
		if($tit <> ""){
			echo "</div></div>";
		}
		?>
	</div> 
	<div style="clear:both;"></div>
	<div class="col-md-3 grid-item"> 
		<?php
		$tit = "";
		foreach($grupos_5 as $informe){
			if(in_array($informe[0],$opciones_persona)){
				if($tit == ""){
					?>
					<div class="panel panel-primary">  
					<div class="panel-heading">
						<b>Informes de Historial</b>
					</div> 
					<div class="panel-body" style="padding:0px;">
					<?
				}
				$tit = "S";
				?>
				<div class="boton_inicio">
					<a href="javascript:carg('<? echo $informe[1]; ?>','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="45" style="text-align:center"><img src="img/modulos/<? echo $informe[3]; ?>.png" width="40"></td> 
						<td>
						<span><? echo $informe[2]; ?></span>
						<p><? echo $informe[4]; ?></p>
						</td>
					</tr>
					</table>
					</a> 
				</div>
				<? 
			}
		} 
		if($tit <> ""){
			echo "</div></div>";
		}
		?> 
	</div> 
	<div class="col-md-3 grid-item"> 
		<?php
		$tit = "";
		foreach($grupos_3 as $informe){
			if(in_array($informe[0],$opciones_persona)){
				if($tit == ""){
					?>
					<div class="panel panel-primary">  
					<div class="panel-heading">
						<b>Informes de Depreciación</b>
					</div> 
					<div class="panel-body" style="padding:0px;">
					<?
				}
				$tit = "S";
				?>
				<div class="boton_inicio">
					<a href="javascript:carg('<? echo $informe[1]; ?>','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="45" style="text-align:center"><img src="img/modulos/<? echo $informe[3]; ?>.png" width="40"></td> 
						<td>
						<span><? echo $informe[2]; ?></span>
						<p><? echo $informe[4]; ?></p>
						</td>
					</tr>
					</table>
					</a> 
				</div>
				<? 
			}
		} 
		if($tit <> ""){
			echo "</div></div>";
		}
		?>
	</div> 
	<div class="col-md-3 grid-item"> 
		<?php
		$tit = "";
		foreach($grupos_7 as $informe){
			if(in_array($informe[0],$opciones_persona)){
				if($tit == ""){
					?>
					<div class="panel panel-primary">  
					<div class="panel-heading">
						<b>Informes de Contables</b>
					</div> 
					<div class="panel-body" style="padding:0px;">
					<?
				}
				$tit = "S";
				?>
				<div class="boton_inicio">
					<a href="javascript:carg('<? echo $informe[1]; ?>','');">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="45" style="text-align:center"><img src="img/modulos/<? echo $informe[3]; ?>.png" width="40"></td> 
						<td>
						<span><? echo $informe[2]; ?></span>
						<p><? echo $informe[4]; ?></p>
						</td>
					</tr>
					</table>
					</a> 
				</div>
				<? 
			}
		} 
		if($tit <> ""){
			echo "</div></div>";
		}
		?>
	</div> 
	<div class="col-md-3 grid-item"> 
		<?php
		$tit = "";
		foreach($grupos_8 as $informe){
			if(in_array($informe[0],$opciones_persona)){
				if($tit == ""){
					?>
					<div class="panel panel-primary">  
					<div class="panel-heading">
						<b>Información para Descargar</b>
					</div> 
					<div class="panel-body" style="padding:0px;">
					<?
				}
				$tit = "S";
				?>
				<div class="boton_inicio">
					<a href="mods/home/descargar_todo.php" target="_blank">
					<table width="100%" style="margin:3px;">
					<tr>
						<td width="45" style="text-align:center"><img src="img/modulos/<? 
						echo $informe[3]; ?>.png" width="40"></td> 
						<td>
						<span><? echo $informe[2]; ?></span>
						<p>Presione para preparar archivo con información</p>
						</td> 
					</tr>
					</table>
					</a> 
				</div>
				<? 
			}
		} 
		if($tit <> ""){
			echo "</div></div>";
		}
		?>
	</div> 
</div>