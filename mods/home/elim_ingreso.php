<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Eliminar Ingreso";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

$res = sql_movimientos("*"," and mov.mov_id = ".$_REQUEST["id"]); 
if(mysqli_num_rows($res) > 0){
    $row = mysqli_fetch_array($res);
}
?> 
<table class="table table-bordered table-condensed"> 
<thead>
    <tr>
        <th colspan="2">Movimiento</th>
    </tr>    
</thead>
<tbody> 
    <tr>    
        <th width="1">Fecha</th>
        <td><?php echo _fec($row["mov_fecha"],5); ?></td> 
    </tr>
    <tr>
        <th>Folio</th>
        <td style="text-align:center;"><?php echo $row["mov_folio"]; ?></td> 
    </tr>
    <tr>
        <th>Tipo</th>
        <td><?php echo $_tipos[$row["mov_tipo"]]; ?></td>
    </tr>
    <tr>
        <th>Concepto</th> 
        <td><?php echo $_conceptos[$row["mov_concepto"]]; ?></td>
    </tr>
    <tr>
        <th>Notas</th>
        <td><?php echo $row["mov_glosa"]; ?></td>
    </tr>
    <tr>
        <th>Lugar</th>
        <td><?php echo $_bodegas[$row["mov_bodega"]]; ?></td>
    </tr>
    <tr>
        <th>Autor</th>
        <td><?php echo $_personas[$row["mov_per_id"]]; ?></td>
    </tr>
    <tr>
        <th>Responsable</th> 
        <td><?php echo $_personas[$row["mov_responsable"]]; ?></td>
    </tr>
    <tr>
        <th>Total</th>
        <td style="text-align: right"><?php echo _num($row["mov_total"]); ?></td>
    </tr>  
</tbody>
</table>