<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Editar Centro de Costo";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

$res = mysqli_query($cnx,"select * from centro_costo where cco_id = '$_REQUEST[id]'");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<strong>No se encontró el dato</strong>
	</div>
    <?php
	construir_boton("cco_listado.php","","buscar","Listado de Centros de Costo",2);
	exit();
}
?>
<script language="javascript">
function save(){
	if(document.getElementById("nombre").value == ""){
		alerta_js("Es obligación ingresar el nombre");
		return;	
	}
	carg("cco_save.php","");
}
</script> 

<form class="form-horizontal" role="form" method="post">
	<input type="hidden" name="modo" value="editar" class="campos"> 
	<input type="hidden" name="id" value="<? echo $_REQUEST["id"]; ?>" class="campos"> 
    <div class="form-group">
        <label for="orden" class="col-sm-2 control-label">Código <span class="oblig">(*)</span></label>
        <div class="col-sm-2">
            <input type="text" name="codigo" class="form-control campos" value="<? echo $datos["cco_codigo"]; ?>"> 
        </div>
    </div>
    <div class="form-group">
        <label for="nombre" class="col-sm-2 control-label">Nombre <span class="oblig">(*)</span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control campos" id="nombre" name="nombre" value="<? echo $datos["cco_nombre"]; ?>">
        </div>
    </div>  
    
    <div class="form-group">
        <div class="col-sm-2"/>
        <div class="col-sm-10">
        	<?php
			construir_boton("","","grabar","Guardar",3);
			construir_boton("cco_listado.php","","eliminar","Cancelar",2);
			?>
        </div>
    </div>
</form> 