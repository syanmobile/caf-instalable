<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Depreciación por Período (3)";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------  

if($_REQUEST["fil_periodo"] == ""){
	$_REQUEST["fil_periodo"] = date("Y");
}
?>  
<script type="text/javascript">
function buscar(){
	var a = $(".campos").fieldSerialize();
	var div = document.getElementById("resultados");
	AJAXPOST("mods/home/inf_de_depreciacion_por_periodo_datos.php",a,div);
} 
</script> 
 
<table class="table table-bordered table-info table-condensed"> 
<tbody> 
<tr>
    <td width="1">
        <b>Período:</b><br>
        <select class="campos" id="fil_periodo" name="fil_periodo">
            <?
            for($i = date("Y") + 20;$i > 1990;$i--){
                ?>
                <option value="<? echo $i; ?>" <?
                if($_REQUEST["fil_periodo"] == $i){ echo "selected"; } 
                ?>><? echo $i; ?></option><?
            }
            ?>
        </select>
    </td> 
    <td>
        <b>Código Activo:</b><br>
        <input type="text" name="fil_codigo2" id="fil_codigo2" class="campos w10" value="<? 
        echo $_REQUEST["fil_codigo2"]; ?>">
    </td>
    <td>
        <b>Nombre Producto:</b><br>
        <input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? 
        echo $_REQUEST["fil_descripcion"]; ?>">
    </td> 
    <td>
        <b>Clasificación:</b><br>
        <select name="fil_categoria" id="fil_categoria" class="campos buscador w10">
        <option value=""></option>
        <?php
        $res = sql_categorias("*"," order by cat.cat_nombre asc"); 
        if(mysqli_num_rows($res) > 0){
            while($row = mysqli_fetch_array($res)){
                ?>
                <option value="<? echo $row["cat_id"]; ?>" <?
                if($_REQUEST["fil_categoria"] == $row["cat_id"]){ echo "selected"; } 
                ?>>C<? echo $row["cat_codigo"]." - ".$row["cat_nombre"]; ?></option>
                <?
            }
        }
        ?></select>
    </td> 
    <td>
        <b>Centro Costo:</b><br>
        <select name="fil_cco" id="fil_cco" class="campos buscador">
        <option value=""></option>
        <?php
        $res = sql_centro_costo("*","order by cco_codigo asc");  
        if(mysqli_num_rows($res) > 0){
            while($datos = mysqli_fetch_array($res)){
                ?><option value="<?php echo $datos["cco_id"]; ?>" <?php 
                if($_REQUEST["fil_cco"] == $datos["cco_id"]){ echo "selected"; } 
                ?>>CC<?php echo $datos["cco_codigo"]; ?> - <?php echo $datos["cco_nombre"]; ?></option>
                <?
            }
        }
        ?>
        </select> 
    </td> 
    <td>
        <b>Tipo:</b><br>
        <select name="fil_tipo" id="fil_tipo" class="campos buscador">
        <option value="">Todos</option>  
        <?php
        foreach($_tipo_activos as $tp){
            if(in_array("act_".strtolower($tp)."_ver",$opciones_persona)){
                ?><option value="<? echo $tp; ?>" <? if($_REQUEST["fil_tipo"] == $tp){ echo "selected"; } ?>><?
                echo $_tipo_activo[$tp]; ?></option>
                <?
            }
        }
        ?>
        </select> 
    </td>
    <?php
    if($_configuraciones["cfg_moneda_sec"] <> ""){
        ?>
        <td width="1">
            <b>Moneda:</b><br>
            <select name="fil_moneda" id="fil_moneda" class="campos">
            <option value=""><? echo $_configuraciones["cfg_moneda"]; ?></option> 
            <option value="sec" <? if($_REQUEST["fil_moneda"] == "sec"){ echo "selected"; } 
            ?>><? echo $_configuraciones["cfg_moneda_sec"]; ?></option>
            </select> 
        </td>
        <? 
    }
    ?>
    <td width="1"><?  
    construir_boton("buscar()","","buscar","Filtrar",4); 
    construir_boton("inf_de_depreciacion_por_periodo.php","","limpiar","Limpiar",8); 
    ?></td>
</tr>
</tbody>
</table> 

<div id="resultados"></div>