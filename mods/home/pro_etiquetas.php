<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Imprimir Etiquetas de Producto";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript">	
$(document).ready(function(){
	lineas();
});
function generar(){
	$("#detalle").html("Buscando...");
	var div = document.getElementById("detalle"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/pro_etiquetas_ajax.php","modo=generar&lineas="+document.getElementById("lineas_sol").value,div);
}
function buscar(){
	$("#resultados").html("Buscando...");
	var div = document.getElementById("resultados"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/pro_etiquetas_ajax.php",a+"&modo=buscar",div);
}	 
function lineas(){
	var div = document.getElementById("lineas"); 
	var a = $(".campos").fieldSerialize();  
	$("#lineas").html("Cargando...");
	AJAXPOST("mods/home/pro_etiquetas_ajax.php",a+"&modo=lineas",div);
}
function agregar(producto){
	var cantidad = prompt("Ingresar Cantidad:", "");
	cantidad = cantidad * 1;
	document.getElementById("lineas_sol").value = document.getElementById("lineas_sol").value + "***" + producto + ";;;" + cantidad;
	lineas();
}
function quitar_linea(id){
	var div = document.getElementById("lineas"); 
	var a = $(".campos").fieldSerialize();   
	AJAXPOST("mods/home/pro_etiquetas_ajax.php",a+"&modo=quitar&elim="+id,div,false,lineas);
}
</script>

 
<table width="100%">
<tbody>
	<tr valign="top"> 
		<td width="300">
			<table class="table table-striped table-bordered table-condensed">
			<thead>
				<tr>
					<th colspan="2">Buscador de Producto en Solicitud</th>
				</tr>
			</thead>
			<tbody>
				<tr> 
					<td><input type="text" name="producto" class="campos w10" placeholder="Ingrese codigo o descripcion"></td>
					<td width="1"><?php
					construir_boton("buscar();","1","buscar","Buscar",4);
					?></td>
				</tr> 
			</tbody>
			</table>
			<div style="overflow: scroll; width: 100%; height: 500px;" id="resultados"></div> 
		</td>
		<td style="padding-left: 10px;">
			<div id="lineas"></div>
			<div id="detalle" style="margin-top: 8px;"></div> 
		</td>
	</tr>
</tbody> 
</table> 