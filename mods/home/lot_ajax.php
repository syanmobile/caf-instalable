<?php
require("../../inc/conf_dentro.php");

switch($_REQUEST["modo"]){  
		
	case "lotes":
		$lotes = explode(",,,,,",$_REQUEST["lotes"]);
		$definidos = (count($lotes) * 1) - 1;
		?>
        <script language="javascript">
		$(document).ready(function(){
			$(".fecha2").mask("99/99/9999");
			setTimeout(function() { $('input[name="lote_codigo"]').focus() }, 500); 
		});
		function validar(e) { 
			tecla = (document.all) ? e.keyCode : e.which; 
			if (tecla==13){ 
				agregar_lote();
			}
		}
		function elegir_ser(serie){
			document.getElementById("lote_codigo").value = serie;
		}
		</script>
		<h4 id="titulo_alerta">Definir Lotes/Series</h4> 
		
		<div style="overflow: scroll; height: 400px; width: 100%; padding: 5px;">
		<table width="100%" style="margin-bottom:5px;">
        <tr valign="top">
        	<td width="220" style="padding-right: 10px;">
				<table class="table table-bordered table-condensed">
				<tr> 
					<td>
					<?php
					$res = sql_productos("*"," and pro.pro_codigo = '".$_REQUEST["pro"]."'"); 
					$prod = mysqli_fetch_array($res);
					echo "<b>".$prod["pro_codigo"]."</b><br>".$prod["pro_nombre"];
					?>
					</td>
				</tr>
				<?php
				if($prod["pro_tipo"] <> "INS"){
					?>
					<tr>
						<td>
						<?php
						$res2 = sql_activos_fijos("*","and acf_producto = '$prod[pro_id]' order by acf_serie desc");
						if(mysqli_num_rows($res2) > 0){
							?>
							<b>Activos Fijos:</b>
							<ul>
							<?
							while($row2 = mysqli_fetch_array($res2)){
								?><li><a href="javascript:elegir_ser('<? echo $row2["acf_serie"]; ?>');"><? echo $row2["acf_serie"]; ?></a></li><?
								$series[] = $row2["acf_serie"];
							}
							?></ul>
							<?
						}else{
							?>
							<span style="color: red;"><b>No existen Activos Fijos para este producto</b></span>
							<?
						}
						?>
						</td>
					</tr>
					<?
				}
				?>
				</table>
			</td>
			<td>
				<table width="100%" style="margin-bottom:5px;">
				<tr>
					<td width="1">
					<table class="table table-bordered table-condensed">
						<tr>
							<th width="1">Cantidad:</th>
							<td style="text-align: center;"><?php echo $_REQUEST["cant"]; ?></td>
						</tr>
						<tr>
							<th width="1">Definido:</th>
							<td style="text-align: center;"><?php echo $definidos; ?></td>
						</tr>
					</table>
					</td>
					<td style="padding-left: 10px;"><?
					$pendiente = $_REQUEST["cant"] - $definidos;
					if($pendiente == 0){
						construir_boton("#","","check","LISTO",4); 
					}
					?></td>
					<td style="padding-left: 10px;">
						<table class="table table-bordered table-condensed">
						<tr> 
							<td>
							<b>Forma de Agregar:</b><br>
							<select id="extra" name="extra">
							<option value="">Modo Individual</option>
							<option value="2" <? if($_REQUEST["lotes_modo"] == "2"){ echo "selected"; } ?>>Modo Correlativo</option>
							</select>
							</th>
						</tr>
						</table>
					</td>
				</tr>
				</table>

				<table class="table table-bordered table-condensed"> 
				<thead>
				<tr>
					<th width="1"></th> 
					<th>Serie</th> 
				</tr>
				</thead>
				<tbody>
				<?php 
				if(count($lotes) > 1){
					for($i = 1;$i < count($lotes);$i++){ 
						?>
						<tr>
							<td><?php
							construir_boton("quitar_lote(',,,,,".$lotes[$i]."');","1","eliminar","Quitar serie",4);
							?></td> 
							<td><?php echo $lotes[$i]; ?></td> 
						</tr>
						<?
					}
					?>
					<tr>
						<td colspan="2"><?php
						construir_boton("quitar_lote('');","","eliminar","Quitar todas las series definidas",4);
						?></td>
					</tr>
					<?
				}

				$pendiente = $_REQUEST["cant"] - $definidos;
				if($pendiente > 0){
					?>
					<input type="hidden" name="lote_cantidad" id="lote_cantidad" value="<? echo $pendiente; ?>" class="campos">
					<tr>
						<td><? construir_boton("agregar_lote()","1","crear","Agregar",4); ?></td> 
						<td><input type="text" name="lote_codigo" id="lote_codigo" onkeypress="javascript:validar(event)" class="campos w10"></td>
					</tr>
					<?
				}else{
					if($pendiente < 0){
						?>
						<tr>
							<td colspan="8" style="background-color:#FF9; color:#F00; font-size:13px; padding:10px;"><b>ATENCION:</b><br />La cantidad total definida, no coincide con lo requerido</td>
						</tr>
						<? 
					}
				}
				?>
				</tbody>
				</table>
			</td>
		</tr>
		</table>
		</div> 
		
		<div id="agregando"></div>
        <?
		break;
		
	case "mostrar_lotes": 
		$lotes = explode(",,,,,",$_REQUEST["lotes"]);
		if(count($lotes) > 1){
			for($i = 1;$i < count($lotes);$i++){ 
				?>
                <span class="label label-info"><?php echo $lotes[$i]; ?></span>
                <?
			}
		}
		?>
        <div style="height: 5px;"></div>
        <input type="hidden" id="total_definido" value="<?php echo (count($lotes) - 1); ?>" />
        <?
		break;
		
	case "agregar_lote":
		if($_REQUEST["extra"] == ""){
			?>
			<script language="javascript">
			document.getElementById("lotes").value = document.getElementById("lotes").value + ",,,,,<?php echo $_REQUEST["lote_codigo"]; ?>";
			definir_lotes();
			</script>
			<?
		}else{
			?>
			<script language="javascript">
			document.getElementById("lotes").value = document.getElementById("lotes").value + "<?php 
			
			$serial = $_REQUEST["lote_codigo"] * 1;
			for($i = 0;$i < $_REQUEST["lote_cantidad"];$i++){
				echo ",,,,,".($serial + $i);
			}
			?>";
			definir_lotes();
			</script>
			<?
		}
		break; 
}
?> 