<?php
require("../../inc/conf_dentro.php"); 

$res = sql_campos("*"," and cam_activo = 1 order by cam_nombre asc");  
if(mysqli_num_rows($res) > 0){
	?>
	<table class="table table-bordered table-condensed"> 
	<thead>
		<tr>
			<th colspan="2">Campos personalizados del Activo Fijo</th>
		</tr>
	</thead>    
	<tbody> 
		<?
		while($row = mysqli_fetch_array($res)){

			// dato grabado?
			$sql2 = "select * from campos_relaciones where nub_tipo = 'ACT' and nub_key = '$_REQUEST[acf]' 
			and nub_campo = $row[cam_id] ";
			$res2 = mysqli_query($cnx,$sql2);
			if(mysqli_num_rows($res2) > 0){
				$row2 = mysqli_fetch_array($res2);
				$infor = $row2["nub_valor"];
			}else{
				$infor = "";
			}
			?> 
			<tr> 
				<th width="130" style="text-align: right;"><? echo $row["cam_nombre"]; ?>:</th>
				<td><? 
	            campo_dinamico($row["cam_tipo"],"campo_adicional_".$row["cam_id"],$row["cam_opciones"],$infor,$url_base);
	            ?></td> 
			</tr>
			<? 
		} 
		?>
	</tbody>
	</table>
	<?
}
?> 

<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $(".buscador").chosen({
        width:"95%",
        no_results_text:'Sin resultados con',
        allow_single_deselect:true
    });
    $(".fecha").datepicker();
    $(".fecha2").mask("99/99/9999");
    $(".numero").numeric();
    $(".hora").mask("99:99");
    $(".moneda").maskMoney({thousands:'.',  precision: 0, allowZero:true,  prefix: '$ '});
});
</script>