<?php
require("../../inc/conf_dentro.php");
?> 
<table class="table table-striped table-bordered tabledrag table-condensed"> 
	<thead>
		<tr>  
			<th style="text-align:center;" width="1"></th>
			<th style="text-align:center;" width="1">Fecha</th>
			<th style="text-align:center;" width="1">Folio</th>  
			<th width="150">Concepto</th>
			<th>Proveedor</th>
			<th>Responsable</th>
			<th>Ubicación</th>
			<th>Notas</th>
			<th>Autor</th>   
			<th width="120">Realizado</th> 
		</tr>
	</thead>
	<tbody>
	<?php 	
    $res = sql_movimientos_detalle("*"," 
    and pro_codigo = '$_REQUEST[codigo]' and det.det_lote = '".$_REQUEST["serial"]."'  
	order by mov.mov_fecha asc, mov.mov_id asc","",""); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){  
			?>
			<tr>  
				<td style="text-align:center;"><?php
				construir_boton("visor('aju_pdf.php?id=".$row["mov_id"]."')","1","imprimir","Transaccion #".$row["mov_id"],4);
				?></td>  
				<td style="text-align:center;"><?php echo _fec($row["mov_fecha"],5); ?></td>
				<td style="text-align:center;"><?php echo $row["mov_folio"]; ?></td>    
				<td><?php 
				if($row["det_ingreso"] > 0){
					?><span class="label label-success"><? echo $row["con_nombre"]; ?></span><?
				}
				if($row["det_egreso"] > 0){
					?><span class="label label-danger"><? echo $row["con_nombre"]; ?></span><?
				}
				if($row["det_validacion"] > 0){
					?><span class="label label-info"><? echo $row["con_nombre"]; ?></span><?
				}
				?></td>   
				<td><?php echo $row["aux_nombre"]; ?></td>
				<td><?php echo $_personas[$row["mov_responsable"]]; ?></td>
				<td><?php echo $row["ubi_nombre"]; ?></td>   
				<td><?php echo $row["mov_glosa"]; ?></td>  
				<td><?php echo nom_persona($row["mov_per_id"]); ?></td>
				
				<td><? echo $row["mov_log"]; ?></td>  
			</tr>
			<? 
        }
	}else{
		?>
		<tr>
			<td colspan="10">Sin movimientos entre las fechas</td>
		</tr>
		<?
	}
    ?> 
	</tbody>
</table>