<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Tabla IPC";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "ipc_listado.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>
<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td><?php
		construir_boton("ipc_crear.php","","crear","Abrir Año",2);
		?></td>
    </tr>
</tbody>
</table>

<table class="table table-striped table-bordered table-condensed" id="tablita"> 
<thead>
<tr>
    <th></th>
    <th style="text-align: center;">Año</th>
    <th style="text-align: center;">IPC A.Anterior</th>
    <?
    for($i = 1;$i <= 12;$i++){
        ?><th style="text-align: center;"><? echo _mes($i); ?></th><?
    }
    ?>
</tr>
</thead>
<tbody> 
<?
$res = mysqli_query($cnx,"select * from tabla_ipc order by ipc_periodo asc");
if(mysqli_num_rows($res) > 0){
    while($row = mysqli_fetch_array($res)){
        $ano = substr($row["ipc_periodo"],0,4);
        $mes = substr($row["ipc_periodo"],4,2) * 1;
        if(!in_array($ano,$anos)){
            $anos[] = $ano;
            $anos_ant[$ano] = $row["ipc_anterior"];
        }
        $dato[$ano][$mes] = $row["ipc_valor"];
    }
}
if(count($anos) > 0){
    foreach($anos as $ano){
        ?>
        <tr>  
            <td style="text-align:center;"><?php
            construir_boton("ipc_editar.php","&ano=".$ano,"editar","Editar Periodo",2);
            ?></td>
            <th style="text-align: center;"><?php echo $ano; ?></th>
            <td style="text-align: center;"><? echo $anos_ant[$ano]; ?></td>
            <?
            for($i = 1;$i <= 12;$i++){
                ?><td style="text-align: center;"><?php echo $dato[$ano][$i]; ?></td><?
            }
            ?>
        </tr>
        <? 

    }
}else{
    ?>
    <tr>
        <td colspan="20">SIN PERIODOS CREADOS</td>
    </tr>
    <?
}
?> 
</tbody>
</table>  