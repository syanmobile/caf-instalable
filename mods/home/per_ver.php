<?php
require("../../inc/conf_dentro.php");

$res = sql_persona("*","where per.per_id = '$_REQUEST[id]'","");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<strong>No se encontró la Persona</strong>
	</div>
    <?php
	exit();
}
//----------------------------------------------------------------------------------------
$titulo_pagina = "Ficha Persona: ".$datos["per_nombres"]." ".$datos["per_paterno"]." ".$datos["per_materno"];
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function cambiar(){
	carg("per_ver.php","");
}
</script>
  
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li class="active"><a href="#tab_1" role="tab" data-toggle="tab">Informaci&oacute;n</a></li>
</ul>

<div class="tab-content">
	<div class="tab-pane active" id="tab_1">
    	<table width="100%">
        <tr valign="top">
            <td>
                <table class="table  table-bordered"> 
                <tbody>
                    <tr>
                        <th width="120" style="text-align:right;">Rut:</th>
                        <td><?php echo $datos["per_rut"]; ?></td>
                    </tr>
                    <tr>
                        <th style="text-align:right;">Nombre Completo:</th>
                        <td><?php echo $datos["per_nombre"]; ?></td>
                    </tr>
                    <tr>
                        <th style="text-align:right;">Correo:</th>
                        <td><?php echo $datos["per_correo"]; ?></td>
                    </tr>
                </tbody>
                </table>
			</td>
    		<td width="200" style="padding-left:10px;">
        		<table class="table  table-bordered"> 
        		<tbody>
                    <tr>
                        <th width="1" style="text-align:right;">Perfil:</th>
                        <td><?php echo $datos["pfl_nombre"]; ?></td>
                    </tr>
                    <tr>
                        <th style="text-align:right;">Clave:</th>
                        <td><?php echo $datos["per_clave"]; ?></td>
                    </tr> 
                    <tr>
                        <th style="text-align:right;">Estado:</th>
                        <td><?php echo ($datos["per_elim"] == "0")?"Activado":"Desactivado"; ?></td>
                    </tr> 
                </tbody>
                </table>
            </td>
		</tr>
        </table>
    </div>
</div>  

<div style="border-top:1px solid #CCC; padding-top:10px;">
    <table width="100%">
    <tr>
        <td><?php
        construir_boton("per_listado.php","","izquierda","Volver al listado de Personas",2); 
        construir_boton("per_editar.php","&id=".$_REQUEST["id"],"editar","Editar Persona",2);
        construir_boton("per_ver.php","&id=".$_REQUEST["id"],"refrescar","Refrescar Ficha",2);
        ?></td>
        <th width="1">Ficha:</th>
        <td width="1"><select name="id" id="id" class="campos" onchange="javascript:cambiar();">
        <?php
        $res = sql_personas("*"," where per.per_id <> 1 order by per.per_nombre asc");
        while($row = mysqli_fetch_array($res)){
            ?><option value="<?php echo $row["per_id"]; ?>" <?php
            if($row["per_id"] == $_REQUEST["id"]){ echo "selected"; } ?>><?php
            echo $row["per_nombre"]; 
			
			if($row["per_elim"] == 1){
				echo " (**ELIMINADO**)";
			}
			?></option><?php } 
        ?>
        </select></td>
    </tr>
    </table> 
</div>
<br /><br />