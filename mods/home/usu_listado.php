<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Usuarios";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "usu_listado.php"; 
include("ayuda.php");
//---------------------------------------------------------------------------------------- 
  
$res = sql_personas("*"," and per.per_tipo = 0 and per.per_id > 1 ORDER BY per.per_nombre asc ");  
$num = mysqli_num_rows($res); 
?>  
<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td> 
            <?php
            if($num == $_SESSION["key_info"]["usuarios"]){
                construir_boton("alerta_js('Ha llegado al limite de usuarios')","","crear","Nuevo Usuario",4); 
            }else{
                construir_boton("usu_nuevo.php","","crear","Nuevo Usuario",2); 
                construir_boton("usu_log.php","","buscar","Historial de operaciones",2); 
            }
			?>
        </td>
        <td width="200" style="text-align: right;">
            Usuarios disponibles: <b><? echo $num."/".$_SESSION["key_info"]["usuarios"]; ?></b>
        </td>
        <td width="200" style="padding-left: 10px;"> 
            <div class="progress" style="margin-bottom: 2px;">
                <?php
                $nume = round(_porc($num,$_SESSION["key_info"]["usuarios"]));
                ?>
                <div class="progress-bar" role="progressbar" style="width: <? echo $nume; ?>%; <? 
                if($num == $_SESSION["key_info"]["usuarios"]){ echo 'background-color: #DC3545 !important;'; } 
                ?>" aria-valuenow="<? echo $nume; ?>" aria-valuemin="0" aria-valuemax="100"><? echo $nume; ?>%</div>
            </div>
        </td>
    </tr>
</tbody>
</table> 
<?php
if($num > 0){
    ?>
    <table class="table table-bordered table-condensed"> 
    <thead>
    <tr>
        <th width="1" align="center">&nbsp;</th>  
        <th>Usuario</th>   
        <th width="1">Estado</th>  
    </tr>
    </thead>
    <tbody> 
    <?php 
    while($row = mysqli_fetch_array($res)){
        ?>
        <tr valign="top" <?php
        if($row["per_elim"] == 1){ ?> style="background-color:#ffeded;" <? } ?>>  
            <td style="text-align:center;"><?php 
			construir_boton("usu_editar.php","&id=".$row["per_id"],"editar","Editar");  
			?></td>  
            <td>
                <b><?php echo $row["per_nombre"]; ?></b><br> 
                <?php echo $row["per_correo"]; ?><br>
                <?php echo $row["pfl_nombre"]; ?>
            </td>    
            <td><?php 
            if($row["per_elim"]){
                ?><span class="label label-danger">Inactivo</span><?
            }else{
                ?><span class="label label-success">Activo</span><?
            } 
            ?></td> 
        </tr>
        <?php 
    }
    ?> 
    </tbody>
    </table>  
    <?php 
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin registros creados</strong>
    </div>
    <?php 
}
?>