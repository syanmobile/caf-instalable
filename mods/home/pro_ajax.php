<?php
require("../../inc/conf_dentro.php");

switch($_REQUEST["modo"]){
		
	case "existe":
		$res = sql_productos("*"," and pro_codigo = '$_REQUEST[codigo]' or pro_codigo_barra = '$_REQUEST[codigo]'"); 
		if(mysqli_num_rows($res) == 0){ 
			?>
			<script language="javascript">
			alerta_js("Producto no existe");
			document.getElementById("producto").value = "";
			$(document).ready(function(){  
				setTimeout(function() { $('input[name="<?php echo $campo; ?>"]').focus() }, 500);
			});
			</script>
			<?
		}else{
			?>
			<script language="javascript">
			<?php echo $_REQUEST["funcion"]; ?>();
			</script>
			<?
		} 
		break;    
		
	case "buscar":
		$res = sql_productos("*"," and pro_codigo = '$_REQUEST[codigo]' "); 
		if(mysqli_num_rows($res) > 0){
			$row = mysqli_fetch_array($res);
			?>
			<script language="javascript"> 
			document.getElementById("codi_<?php echo $_REQUEST["id"]; ?>").value = "<?php echo $row["pro_codigo"]; ?>";
			document.getElementById("nomb_<?php echo $_REQUEST["id"]; ?>").value = "<?php echo $row["pro_nombre"]; ?>";
			document.getElementById("ptipo_<?php echo $_REQUEST["id"]; ?>").value = "<?php echo $row["pro_tipo"]; ?>"; 
			
			$("#codi_<?php echo $_REQUEST["id"]; ?>").prop('readonly', true);	
			$("#btn1_prd_<?php echo $_REQUEST["id"]; ?>").hide();
			$("#btn2_prd_<?php echo $_REQUEST["id"]; ?>").show();
			</script>
			<?
		}else{
			?>
			<script language="javascript">
			alerta_js("No existe el Producto");
			document.getElementById("codi_<?php echo $_REQUEST["id"]; ?>").value = "";
			document.getElementById("nomb_<?php echo $_REQUEST["id"]; ?>").value = "";
			document.getElementById("ptipo_<?php echo $_REQUEST["id"]; ?>").value = ""; 
			document.getElementById("codi_<?php echo $_REQUEST["id"]; ?>").focus();
			</script>
			<?
		}
		break;
		
	case "busqueda":  
		if($_REQUEST["fmp_codigo"] <> ""){
			$filtros .= " and pro.pro_codigo like '%".$_REQUEST["fmp_codigo"]."%' ";
		}
		if($_REQUEST["fmp_descripcion"] <> ""){
			$filtros .= " and pro.pro_nombre like '%".$_REQUEST["fmp_descripcion"]."%' ";
		}
		if($_REQUEST["fmp_unidad"] <> ""){
			$filtros .= " and pro.pro_unidad = '".$_REQUEST["fmp_unidad"]."' ";
		}
		if($_REQUEST["fmp_grupo"] <> ""){
			$filtros .= " and pro.pro_grupo = '".$_REQUEST["fmp_grupo"]."' ";
		}
		if($_REQUEST["fmp_subgrupo"] <> ""){
			$filtros .= " and pro.pro_subgrupo = '".$_REQUEST["fmp_subgrupo"]."' ";
		}
		if($_REQUEST["fmp_categoria"] <> ""){
			$filtros .= " and pro.pro_categoria = '".$_REQUEST["fmp_categoria"]."' ";
		}
		if($_REQUEST["fmp_tipo"] <> ""){
			$filtros .= " and pro.pro_tipo = '".$_REQUEST["fmp_tipo"]."' ";
		}
		$res = sql_productos("*","   $filtros order by pro_codigo asc"); 
		$num = mysqli_num_rows($res);
		?>
		<h4 id="titulo_alerta"><? echo $num; ?> resultado(s) de Productos</h4> 
        <?
		if($num > 0){
			?>
			<table class="table table-bordered table-hover table-condensed"> 
			<thead>
			<tr> 
				<th width="80">Opción</th>
				<th width="1">Código</th>
				<th>Producto</th>
				<th>Unidad</th>  
				<th>Categoria</th>
				<th>Tipo</th>
			</tr>
			</thead>
			<tbody> 
			<?php 
			while($row = mysqli_fetch_array($res)){
				?>
				<tr>    
					<td><?
					construir_boton("elegir_producto('$row[pro_codigo]','$row[pro_nombre]','$row[pro_tipo]')","","derecha","Elegir",4); 
					?></td>
					<th><?php echo $row["pro_codigo"]; ?></th>
					<td><?php echo $row["pro_nombre"]; ?><br />
                    <span style="font-size:9px;">Cod.Barra: <?php echo $row["pro_codigo_barra"]; ?>
                    </td> 
					<td><?php echo $row["uni_nombre"]; ?></td>  
					<td><?php echo $row["cat_nombre"]; ?></td> 
					<td><?php echo $row["pro_tipo"]; ?></td>
				</tr>
				<?php 
			}
			?> 
			</tbody>
			</table>  
			<?php
		}else{
			?>
			<div class="alert alert-danger">
				<strong>Sin resultados</strong>
			</div>
			<?php 
		}
		break;
		
	case "upload": 
		$archivo_name = "pro_".date("YmdHis")."_"._sa2($_FILES['archivo']['name']);

		$tmp_name 	= $_FILES['archivo']['tmp_name'];
		$size 		= $_FILES['archivo']['size'];
		$type 		= $_FILES['archivo']['type'];
		$error 		= $_FILES['archivo']['error']; 
		/**************************************************/
		$add="../../upload/".$_SESSION["key_id"]."/".$archivo_name; //file name where the file will be stored, upload is the directory
		if(move_uploaded_file ($tmp_name,$add)){
			?> 
            <input type="hidden" id="imagen" name="imagen" class="campos" value="<?php echo $archivo_name; ?>">
			<?php
			chmod("$add",0777);
		}else{
			?> 
			<div class="alert alert-danger" style="margin-top:10px;">
				No se pudo subir la imagen
			</div> 
            <input type="hidden" id="imagen" name="imagen" class="campos">
			<?php
			chmod("$add",0777);
		}
		break;
		
	case "codigo_unico":
		$res = sql_productos("*"," and pro_codigo = '$_REQUEST[codigo]'"); 
		if(mysqli_num_rows($res) == 0){
			construir_boton("#","","check","Código Disponible",4);
			
			if($_REQUEST["clonar"] == ""){
				?>
				<script language="javascript">
				carg("pro_save.php","");
				</script>
				<?
			}else{
				?>
				<script language="javascript">
				clonar_save2();
				</script>
				<?
			}
		}else{
			construir_boton("#","","eliminar","Ya existe este código",4);
		}
		break;
		
	case "inicio":
		?>
		<h4 id="titulo_alerta">Filtros de Productos</h4> 
        <table class="table table-bordered table-hover table-condensed"> 
        <thead>
        <tr> 
            <th>Realice algún filtro</th>                        
        </tr>
        </thead>
        <tbody> 
        <tr>
            <td>
            <b>Código:</b><br />
            <input type="text" name="fmp_codigo" id="fmp_codigo" class="fmp_campos w10" value="<? echo $_REQUEST["fmp_codigo"]; ?>"></td>
        </tr> 
        <tr>
            <td>
            <b>Descripción:</b><br />
            <input type="text" name="fmp_descripcion" id="fmp_descripcion" class="fmp_campos w10" value="<? echo $_REQUEST["fmp_descripcion"]; ?>"></td>
        </tr>
        <tr>
            <td>
            <b>Unidad:</b><br />
            <select name="fmp_unidad" id="fmp_unidad" class="fmp_campos buscador w10">
			<option value=""></option>
			<?php
			$res = sql_unidades("*"," order by uni.uni_nombre asc"); 
			if(mysqli_num_rows($res) > 0){
				while($row = mysqli_fetch_array($res)){
					?>
					<option value="<? echo $row["uni_codigo"]; ?>" <?
					if($_REQUEST["fmp_unidad"] == $row["uni_codigo"]){ echo "selected"; } 
					?>><? echo $row["uni_codigo"]." - ".$row["uni_nombre"]; ?></option>
					<?
				}
			}
			?></select></td>
        </tr>
        <tr>
            <td>
            <b>Grupo:</b><br />
            <select name="fmp_grupo" id="fmp_grupo" class="fmp_campos buscador w10">
			<option value=""></option>
			<?php
			$res = sql_grupos("*"," order by gru.gru_nombre asc"); 
			if(mysqli_num_rows($res) > 0){
				while($row = mysqli_fetch_array($res)){
					?>
					<option value="<? echo $row["gru_codigo"]; ?>" <?
					if($_REQUEST["fmp_grupo"] == $row["gru_codigo"]){ echo "selected"; } 
					?>><? echo $row["gru_codigo"]." - ".$row["gru_nombre"]; ?></option>
					<?
				}
			}
			?></select></td>
        </tr>
        <tr>
            <td>
            <b>Subgrupo:</b><br />
            <select name="fmp_subgrupo" id="fmp_subgrupo" class="fmp_campos buscador w10">
			<option value=""></option>
			<?php
			$res = sql_subgrupos("*"," order by sgru.sgru_nombre asc"); 
			if(mysqli_num_rows($res) > 0){
				while($row = mysqli_fetch_array($res)){
					?>
					<option value="<? echo $row["sgru_codigo"]; ?>" <?
					if($_REQUEST["fmp_subgrupo"] == $row["sgru_codigo"]){ echo "selected"; } 
					?>><? echo $row["sgru_codigo"]." - ".$row["sgru_nombre"]; ?></option>
					<?
				}
			}
			?></select></td>
        </tr>
        <tr>
            <td>
            <b>Categorias:</b><br />
            <select name="fmp_categoria" id="fmp_categoria" class="fmp_campos buscador w10">
			<option value=""></option>
			<?php
			$res = sql_categorias("*"," order by cat.cat_nombre asc"); 
			if(mysqli_num_rows($res) > 0){
				while($row = mysqli_fetch_array($res)){
					?>
					<option value="<? echo $row["cat_codigo"]; ?>" <?
					if($_REQUEST["fmp_categoria"] == $row["cat_codigo"]){ echo "selected"; } 
					?>><? echo $row["cat_codigo"]." - ".$row["cat_nombre"]; ?></option>
					<?
				}
			}
			?></select></td>
        </tr>
        <tr>
            <td>
            <b>Tipo:</b><br />
            <select name="fmp_tipo" id="fmp_tipo" class="fmp_campos w10">
			<option value=""></option>   
		    <?php
			foreach($_tipo_activos as $tp){
				if(_opc("activo_".$tp) == "1"){
					?><option value="<? echo $tp; ?>" <? if($_REQUEST["fmp_tipo"] == $tp){ echo "selected"; } ?>><?
					echo $_tipo_activo[$tp]; ?></option>
					<?
				}
			}
			?>
            </select></td>
        </tr>
        </tbody>
        </table>
        <script language="javascript">
		$(document).ready(function(){
			$(".buscador").chosen({
				width:"95%",
				no_results_text:'Sin resultados con',
				allow_single_deselect:true 
			});
		});
		</script>
        <?
		construir_boton("fmp_buscar()","","buscar","Buscar Productos",4);
				
		break;

	case "inicio_input":
		?> 
		<table width="100%">
		<tr>
			<td>
				<select name="producto" id="producto" class="campos buscador" onchange="javascript:elegir();">
			    <option value=""></option>
				<?php
				// Sacar los productos con movimiento
				$res = sql_productos("*"," order by pro_nombre asc");
				$num = mysqli_num_rows($res);
				if($num > 0){
					while($row = mysqli_fetch_array($res)){
						?><option value="<?php echo $row["pro_id"]; ?>" <?
						if($_REQUEST["prod"] == $row["pro_id"]){ echo "selected"; } ?>><?php
						echo $row["pro_codigo"]." - ".$row["pro_nombre"];  
						?></option><?php 
					}
				}
			    ?>
			    </select>
			</td>
			<td width="1"><?php construir_boton("crear_producto();","1","crear","Crear",4); ?></td>
		</tr>	
		</table>
        <script language="javascript">
		$(document).ready(function(){
			$(".buscador").chosen({
				width:"95%",
				no_results_text:'Sin resultados con',
				allow_single_deselect:true 
			});
			<?
			if($_REQUEST["prod"] <> ""){
				?>elegir();<?
			}
			?>
		});
		</script>
        <?
		break;
		
	case "crear_form":
		?>
		<h4 id="titulo_alerta">Crear Producto (Modo express)</h4>
		<div id="lugar_pro_save"></div>		 
        <table class="table table-bordered table-condensed"> 
	    <tr>
	        <th width="100" style="text-align:right;">Código:</th>
	        <td><input type="text" name="pro_codigo" id="pro_codigo" class="campos w10" style="text-align:center; width:130px !important;"></td>
	    </tr>       
		<tr>
			<th style="text-align:right;">Nombre:</th>
			<td><input type="text" name="pro_nombre" id="pro_nombre" class="campos w10"></td> 
		</tr> 
		<tr>
			<th style="text-align:right;">Tipo Producto:</th> 
			<td><?php input_tipo_activo('','S'); ?></td>
		</tr>  
	    </table> 
        <?
		construir_boton("save_producto()","","grabar","Guardar Producto",4);
		?><br><br><?		
		break;  
	
	case "crear_rapido": 
		$res = sql_productos("*"," and pro_codigo = '$_REQUEST[pro_codigo]'"); 
		if(mysqli_num_rows($res) == 0){
			$sql = "INSERT INTO productos ( 
				pro_codigo,
				pro_nombre,
				pro_tipo
			) VALUES ( 
				'$_POST[pro_codigo]',
				'$_POST[pro_nombre]', 
				'$_POST[tipo_activo]'
			)"; 
			$res = mysqli_query($cnx,$sql); 
			$id = mysqli_insert_id($cnx);
			?>
			<script language="javascript">
            $('#modGeneral').modal('hide');
            input_producto('<? echo $id; ?>');
            </script>
			<?
		}else{
			?>
			<div class="alert alert-danger">
				Ya existe este código
			</div>
			<?
		}
		break;
}
?> 