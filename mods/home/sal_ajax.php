<?php
require("../../inc/conf_dentro.php");

switch($_REQUEST["modo"]){
	case "eliminar": 
		$sql = "select * from movimientos where mov_id = '$_REQUEST[id]' ";
		$res = mysqli_query($cnx,$sql);
		if(mysqli_num_rows($res) > 0){
			$row2 = mysqli_fetch_array($res);
			$cabecera = str_replace("'","´",json_encode($row2));
		}
		$sql = "select * from movimientos_detalle where det_mov_id = '$_REQUEST[id]' ";
		$res = mysqli_query($cnx,$sql);
		if(mysqli_num_rows($res) > 0){
			while($row = mysqli_fetch_array($res)){
				$detalle .= "<hr>".str_replace("'","´",json_encode($row));
			}
		}
		
		$comentario = str_replace("'","´",$_POST["comentario"]);
		$documento = "Folio ".$row2["mov_folio"]." | Tipo ".$row2["mov_tipo"]." | Bodega: ".$row2["mov_bodega"]." | Concepto: ".$row2["mov_concepto"]." | Fecha: "._fec($row2["mov_fecha"],5);
		$sql = "insert into movimientos_eliminados (eli_documento,eli_cabecera,eli_detalle,eli_comentario,eli_realizado,eli_usuario) 
		values ('$documento','$cabecera','$detalle','$comentario','".date("Y-m-d H:i:s")."','".$_SESSION["per_conectado"]["per_id"]."')";
		$res = mysqli_query($cnx,$sql);
		
		$sql = "delete from movimientos where mov_id = ".$_REQUEST["id"];
		$res = mysqli_query($cnx,$sql);
		$sql = "delete from movimientos_detalle where det_mov_id = ".$_REQUEST["id"];
		$res = mysqli_query($cnx,$sql); 
		break;
		
	case "valorizar": 
		$res = sql_activos_fijos("*,count(*) as stock,sum(acf_valor) as valor"," and pro_codigo = '$_REQUEST[codigo]' "); 
		if(mysqli_num_rows($res) > 0){
			$row = mysqli_fetch_array($res);
			$valor = round(($row["valor"] * 1) / ($row["stock"] * 1));
		}
		?>
		<table width="100%">
			<tr>
				<td><input type="text" name="prec" id="prec" style="text-align:right" class="campos numero w10" onchange="javascript:calcular();" value="<? echo $valor; ?>"></td>
				<td width="1"><?php
				construir_boton("cargar_precio();","1","izquierda","Cargar",4);
				?></td>
			</tr>
		</table>
		<script language="javascript">
		$(document).ready(function(){
			$('.numero2').priceFormat({
				prefix: '',
				centsLimit: 0,
				centsSeparator: ',',
				thousandsSeparator: '.'
			});
			$(".numero").numeric();
		}); 
		</script>
		<?
		break;
		
	case "detalle":
		?>
        <input type="hidden" name="prod_det" id="prod_det" value="<?php echo $_REQUEST["prod_det"]; ?>" class="campos">
        <input type="hidden" name="lote_det" id="lote_det" value="<?php echo $_REQUEST["lote_det"]; ?>" class="campos">
        <input type="hidden" name="cant_det" id="cant_det" value="<?php echo $_REQUEST["cant_det"]; ?>" class="campos"> 
        <input type="hidden" name="prec_det" id="prec_det" value="<?php echo $_REQUEST["prec_det"]; ?>" class="campos">
        
        <script language="javascript">
		function cargar_precio(){
			if(document.getElementById("codi_1").value == ""){
				alert("Seleccione un codigo producto");
			}else{
				AJAXPOST("mods/home/sal_ajax.php","modo=valorizar&codigo="+document.getElementById("codi_1").value,document.getElementById("prec_lugar"));				
			}
		}
		</script>
        
        <table class="table table-bordered table-hover table-condensed">
        <thead>
        <tr> 
            <th colspan="2">Producto</th>
            <th width="150">Lotes/Series</th>  
            <th width="70">Cantidad</th>
            <th width="120">Valor</th>
            <th width="100">Total</th>
            <th width="1"></th>
        </tr>
        </thead>
        <tbody> 
            <tr> 
                <td colspan="2">
				<?php input_producto('',1); ?>
                <div id="operacion"></div>
                </td> 
                <td>
				<div id="mostrar_lotes"><input type="hidden" id="total_definido" value="0"></div>
				<?php
                construir_boton("definir_lotes();","","caja","Definir",4);
                ?>
                <input type="hidden" name="lotes" id="lotes" class="campos">
                <input type="hidden" name="lotes_modo" id="lotes_modo" class="campos">
                </td> 
                <td><input type="text" name="cant" id="cant" class="campos numero inpcen w10" onchange="javascript:calcular();"></td>
                <td id="prec_lugar">
                <table width="100%">
                	<tr>
                		<td><input type="text" name="prec" id="prec" style="text-align:right" class="campos numero w10" onchange="javascript:calcular();"></td>
                		<td width="1"><?php
						construir_boton("cargar_precio();","1","izquierda","Cargar",4);
						?></td>
                	</tr>
                </table>
				</td>
                <td><input type="text" name="stot" id="stot" style="text-align:right" class="campos numero2 w10" readonly></td>
                <td><?php
                construir_boton("agregar_linea();","1","crear","Agregar Linea",4);
                ?></td>       
            </tr>
            <?php
			$prod_det = explode("*****",$_REQUEST["prod_det"]);
			$lote_det = explode("*****",$_REQUEST["lote_det"]);
			$cant_det = explode("*****",$_REQUEST["cant_det"]);
			$prec_det = explode("*****",$_REQUEST["prec_det"]);
			for($i = 1;$i < count($prod_det);$i++){
				
				$res = sql_productos("*"," and pro.pro_codigo = '".$prod_det[$i]."'"); 
				$prod = mysqli_fetch_array($res);
				
				if($cant_det[$i] > 0){
					$lineas++;
					?>
					<tr>
						<th width="1"><?php echo $prod["pro_codigo"]; ?></th>
						<td><?php echo $prod["pro_nombre"]; ?></td>
						<td><?php
						$lotes = explode(",,,,,",$lote_det[$i]);
						if(count($lotes) > 1){
							for($i2 = 1;$i2 < count($lotes);$i2++){ 
								?>
								<span class="label label-default"><?php echo $lotes[$i2]; ?></span>
								<?
							}
						}
						$subtotal = $prec_det[$i] * $cant_det[$i];
						$total += $subtotal;
						?></td> 
						<td style="text-align:center;" onclick="javascript:edicion_rapida(<?php echo $i; ?>,'cant',<?php echo $cant_det[$i]; ?>);"><?php echo _num($cant_det[$i]); ?></td>
						<td style="text-align:right;" onclick="javascript:edicion_rapida(<?php echo $i; ?>,'prec',<?php echo $prec_det[$i]; ?>);"><?php echo _num($prec_det[$i]); ?></td>
						<td style="text-align:right;"><?php echo _num($subtotal); ?></td>
						<td><?php
						construir_boton("quitar_linea('".$i."');","1","eliminar","Borrar Linea",4);
						?></td> 
					</tr>
					<?
				}
			}
			?> 
            <tr>
                <th colspan="5" style="text-align:right;">TOTAL:</th>
                <th style="text-align:right;"><?php echo _num($total); ?></th>
                <th></th>
            </tr>
        </tbody>
        </table>
        <input type="hidden" id="lineas" class="campos w10" value="<?php echo $lineas; ?>">
        <input type="hidden" name="total" id="total" class="campos w10" value="<?php echo $total; ?>">
        <script language="javascript">
		$(document).ready(function(){
			$('.numero2').priceFormat({
				prefix: '',
				centsLimit: 0,
				centsSeparator: ',',
				thousandsSeparator: '.'
			});
			$(".numero").numeric();
		}); 
		function calcular(){
			var total = 0;
			var subtotal = 0;
			var cantidad = 0;
			var precio = 0;
			
			cant = document.getElementById("cant").value * 1;
			document.getElementById("cant").value = cant;
			
			prec = document.getElementById("prec").value;
			prec = prec.replace(".", "") * 1;
			document.getElementById("prec").value = prec;
			
			stot = cant * prec;
			document.getElementById("stot").value = stot;
		}
		</script>
		<? 
		break;
		
	case "agregar_linea":
		$res = sql_productos("*"," and pro.pro_codigo = '".$_REQUEST["codi_1"]."'"); 
		if(mysqli_num_rows($res) == 0){
			?>
            <script language="javascript">
			alerta_js("El código del producto no existe");
			</script>
            <?
			exit();
		}else{
			?>
            <script language="javascript">
			document.getElementById("prod_det").value = document.getElementById("prod_det").value + "*****<?php echo $_REQUEST["codi_1"]; ?>";
			document.getElementById("cant_det").value = document.getElementById("cant_det").value + "*****<?php echo $_REQUEST["cant"]; ?>";
			document.getElementById("lote_det").value = document.getElementById("lote_det").value + "*****<?php echo $_REQUEST["lotes"]; ?>";
			document.getElementById("prec_det").value = document.getElementById("prec_det").value + "*****<?php echo str_replace(".","",$_REQUEST["prec"]); ?>";
			detalle();
            </script>
            <?php
		}
		break;
		
	case "quitar_linea":
		$prod_det = explode("*****",$_REQUEST["prod_det"]);
		$lote_det = explode("*****",$_REQUEST["lote_det"]);
		$cant_det = explode("*****",$_REQUEST["cant_det"]);
		$prec_det = explode("*****",$_REQUEST["prec_det"]);
		for($i = 1;$i < count($prod_det);$i++){
			if($i <> $_REQUEST["elim"]){
				$prod_ .= "*****".$prod_det[$i];
				$lote_ .= "*****".$lote_det[$i];
				$cant_ .= "*****".$cant_det[$i];
				$prec_ .= "*****".$prec_det[$i];
			}
		}
		?>
		<script language="javascript">
		document.getElementById("prod_det").value = "<?php echo $prod_; ?>";
		document.getElementById("lote_det").value = "<?php echo $lote_; ?>";
		document.getElementById("cant_det").value = "<?php echo $cant_; ?>";
		document.getElementById("prec_det").value = "<?php echo $prec_; ?>";
		detalle();
		</script>
		<?php 
		break;
		
	case "solicitar_picking":
		?>
        <input type="radio" name="responsable" value="0" checked class="campos2" style="display:none">
		<h4 id="titulo_alerta">Solicitar Picking</h4>   
        <table class="table table-bordered table-condensed">
        <thead>
        <tr>
        	<th width="1"></th>
            <th>Responsable</th>
            <th>Picking solicitados</th>
        </tr>
        </thead>
        <tbody> 
        <?php
		$res = sql_personas("*","   ORDER BY per.per_nombre asc ");  
		if(mysqli_num_rows($res) > 0){
			while($row = mysqli_fetch_array($res)){
				?>
                <tr>
                    <td><input type="radio" name="responsable" value="<? echo $row["per_id"]; ?>" class="campos2" /></td>
                    <td><?php echo $row["per_nombre"]; ?></td>
                    <td><?
					$res2 = sql_movimientos("*"," where con.con_tipo = 'S' and mov.mov_picking_responsable = '$row[per_id]' and mov.mov_estado = 'PND' order by mov.mov_id desc");
					if(mysqli_num_rows($res2) > 0){
						while($row2 = mysqli_fetch_array($res2)){
						}
					}else{
						echo "Libre";
					}
					?></td>
                </tr>
                <?
			}
		}
		?>
        <tr>
        </tr>	
        </table>
        <?
		construir_boton("grabar_picking()","","grabar","Solicitar Picking",4);
		?><br /><br />
		<script language="javascript">
		function grabar_picking(){ 
			var a = $(".campos2").fieldSerialize(); 
			AJAXPOST(url_base+modulo_base+"sal_ajax.php",a+"&modo=solicitar_picking_save",document.getElementById("modGeneral_lugar"));
		}
		</script>
		<?
		break;
		
	case "solicitar_picking_save": 
		$picking = 0;
		$res = sql_movimientos("*","   order by mov.mov_picking desc limit 0,1"); 
		if(mysqli_num_rows($res) > 0){
			$row = mysqli_fetch_array($res);
			$picking = $row["mov_picking"] * 1;
		}
		$picking++;
		
		foreach($_REQUEST["d"] as $doc){
			$SQL_ = "UPDATE movimientos SET "; 
			$SQL_.= "mov_picking = '$picking', ";
			$SQL_.= "mov_picking_responsable = '$_POST[responsable]' ";
			$SQL_.= "WHERE mov_id = '$doc' ";  
			$res = mysqli_query($cnx,$SQL_);
		}
		?>   
        <div class="alert alert-success"> 
            <strong>Nuevo Picking solicitado con &eacute;xito (Id: <?
            echo $picking; ?>)</strong> 
        </div>  
		<?php  
		break;
}
?> 