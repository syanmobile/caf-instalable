<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Editar Tipo Activo";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$res = mysqli_query($cnx,"select * from activos_tipos where tac_id = '$_REQUEST[id]' ");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<strong>No se encontró el dato</strong>
	</div>
    <?php
	construir_boton("imple_tac.php","","izquierda","Volver",2);
	exit();
}
?>
<script language="javascript"> 
function save(){  
	carg("imple_tac_save.php","");
}
</script>

<input type="hidden" name="modo" value="editar" class="campos">
<input type="hidden" name="id" value="<? echo $_REQUEST["id"]; ?>" class="campos"> 
    
<table class="table table-striped table-bordered table-condensed">
	<tr> 
	    <th width="150" style="text-align:right;">Código:</th>
	    <td><input type="text" class="form-control campos" id="codigo" name="codigo" value="<?php echo $datos["tac_codigo"]; ?>"></td>
	</tr>
	<tr> 
	    <th style="text-align:right;">Nombre Activo:</th>
	    <td><input type="text" class="form-control campos" id="nombre" name="nombre" value="<?php echo $datos["tac_nombre"]; ?>"></td>
	</tr>
	<tr> 
	    <th style="text-align:right;">Icono:</th>
	    <td><input type="text" class="form-control campos" id="icono" name="icono" value="<?php echo $datos["tac_icono"]; ?>"><br>
	    	<a href="https://fontawesome.com/icons?d=gallery&m=free" target="_blank">https://fontawesome.com/icons?d=gallery&m=free</a><br>
	    	<a href="https://getbootstrap.com/docs/3.3/components/" target="_blank">https://getbootstrap.com/docs/3.3/components/</a></td>
	</tr>
	<tr> 
	    <th style="text-align:right;">Ejemplos:</th>
	    <td><input type="text" class="form-control campos" id="ejemplo" name="ejemplo" value="<?php echo $datos["tac_ejemplo"]; ?>"></td>
	</tr>
	<tr> 
	    <th style="text-align:right;">Amortizable:</th>
	    <td>
			<select name="amortizable" id="amortizable" class="campos">
			<option value="1" <?php if($datos["tac_amortizable"] == "1"){ echo "selected"; } ?>>SI</option>
			<option value="0" <?php if($datos["tac_amortizable"] == "0"){ echo "selected"; } ?>>-</option> 
			</select>
		</td>
	</tr>
	<tr> 
	    <th style="text-align:right;">Depreciable:</th>
	    <td>
			<select name="depreciable" id="depreciable" class="campos">
			<option value="1" <?php if($datos["tac_depreciable"] == "1"){ echo "selected"; } ?>>SI</option>
			<option value="0" <?php if($datos["tac_depreciable"] == "0"){ echo "selected"; } ?>>-</option> 
			</select>
		</td>
	</tr>
	<tr> 
	    <th style="text-align:right;">Revalorizable:</th>
	    <td>
			<select name="revalorizable" id="revalorizable" class="campos">
			<option value="1" <?php if($datos["tac_revalorizable"] == "1"){ echo "selected"; } ?>>SI</option>
			<option value="0" <?php if($datos["tac_revalorizable"] == "0"){ echo "selected"; } ?>>-</option> 
			</select>
		</td>
	</tr>
	<tr> 
	    <th style="text-align:right;">Readecuable:</th>
	    <td>
			<select name="readecuable" id="readecuable" class="campos">
			<option value="1" <?php if($datos["tac_readecuable"] == "1"){ echo "selected"; } ?>>SI</option>
			<option value="0" <?php if($datos["tac_readecuable"] == "0"){ echo "selected"; } ?>>-</option> 
			</select>
		</td>
	</tr>
	<tr>
		<th style="text-align:right;">Activo:</th>
		<td>
			<select name="activo" id="activo" class="campos">
			<option value="1" <?php if($datos["tac_activo"] == "1"){ echo "selected"; } ?>>Activado</option>
			<option value="0" <?php if($datos["tac_activo"] == "0"){ echo "selected"; } ?>>Desactivado</option> 
			</select>
		</td>
	</tr> 
</table> 
<?php
construir_boton("","","grabar","Guardar",3);
construir_boton("imple_tac.php","","eliminar","Cancelar",2);
?>   