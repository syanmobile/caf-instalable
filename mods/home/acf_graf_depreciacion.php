<?php
require("../../inc/conf_dentro.php");
require("../../_css.php");

$res3 = sql_activos_fijos("*","and acf_id = '$_REQUEST[id]' "); 
$num3 = mysqli_num_rows($res3); 
if($num3 == 0){
	?>
	<div class="alert alert-danger" style="margin:20px;">
		<strong>Sin activos fijos</strong>
	</div>
	<?php 
	exit(); 
}
$datos = mysqli_fetch_array($res3);
 
$periodo = explode("-",$datos["acf_mes_ano_inicio_depreciacion"]);
$ano = $periodo[1] * 1;
$mes = $periodo[0] * 1;
$meses = $datos["acf_vida_util"] * 1;
if($datos["acf_tipo_depreciacion"] == "L"){
	$meses_divisor = $meses;
}else{
	$meses_divisor = round($meses / 3);
}
$residual = $datos["acf_valor_residual"] * 1;
$valor_activo = $datos["acf_valor"] * 1;
$valor = $valor_activo - $residual;

if($mes < 1 || $ano < 1 || $valor < 1 || $meses < 1){
	?>
	<div class="alert alert-danger" style="margin:20px;">
		<strong>No se puede generar el gráfico</strong><br>
		Debe ingresar: Valor Compra + Mes/Ano depreciación + Vida Util
	</div>
	<?php 
	exit(); 
}

$valor_mensual = round(($valor / $meses_divisor),2);

for($i = $ano;$i <= date("Y");$i++){
	if($reinicio == ""){
		$mes_partida = $mes;
		$reinicio = "s";
	}else{
		$mes_partida = 1;
	}
	for($ii = $mes_partida;$ii <= 12;$ii++){ 
		$acumulado += $valor_mensual;
		$totales[$i][$ii] = $acumulado; 
	}
}
?>
<!DOCTYPE HTML>
<html>
<head>
<script>
window.onload = function () { 
	
	var chart = new CanvasJS.Chart("chartContainer", {
		animationEnabled: true,
		theme: "light2",
		title:{
			text: "Gráfico Evolución Dep Acumulada Año <? echo date("Y"); ?>"
		},
		axisY:{
			includeZero: false
		},
		data: [{        
			type: "line",       
			dataPoints: [
				<?php
				if($ano < date("Y")){
					$inicio = 1;
				}else{
					$inicio = $mes;
				}
				for($i = $inicio;$i <= 12;$i++){
					$valor_mes = $totales[date("Y")][$i] * 1;
					if($aux <> ""){ echo ","; } $aux = "S";
					echo '{ y: '.$valor_mes.', indexLabel: "'._mes($i).': $'._num($valor_mes).'" }';
				}
				?> 
			]
		}]
	});
	chart.render();

}
</script>
</head>
<body>
<div id="chartContainer" style="height: 290px; width: 100%;"></div>
<script src="../../js/jquery-1.11.1.min.js"></script>
<script src="../../js/jquery.canvasjs.min.js"></script>
</body>
</html>