<div class="col-md-<? echo $ancho; ?> grid-item">
	<div class="panel panel-primary">  
		<div class="panel-heading">
			<b>Activos Fijos por Tipo</b>
		</div>
		<div class="panel-body"> 
			<?php
			$res = mysqli_query($cnx,"select * from resumen_tipo order by res_total desc");
			if(mysqli_num_rows($res) > 0){
				?>
				<table class="table table-condensed table-bordered"> 
				<thead>
					<tr>
						<th width="1">Stock</th>
						<th>Tipo de Activo</th>
					</tr>
				</thead>
				<tbody>
				<? 
				while($row = mysqli_fetch_array($res)){ 
					$i_w9++;
					?>
					<tr <? if($i_w9 > 5){ echo 'class="oculto_w9" style="display:none;"'; } ?>>  
						<th style="text-align: center;"><? echo _num2($row["res_total"]); ?></th>
						<td><? echo $_tipo_activo[$row["res_tipo"]]; ?></td> 
					</tr>
					<? 
				}
				?>  
				</tbody>
				</table>
				<?
				if($i_w9 > 5){
	                construir_boton("mostrar_w9()","","buscar","Ver más",4); 
	            }
                ?>                
				<script type="text/javascript">
				function mostrar_w9(){
					$(".oculto_w9").toggle("fast");
				}	
				</script>
				<?
			}else{
				?>
				<div class="alert alert-danger">
					<strong>Sin stock de activos</strong>
				</div>
				<?php 
			}
			?>  
		</div>
	</div>
</div>