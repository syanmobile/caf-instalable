<?php 
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition:  filename=\"inf_entradas_".date("YmdHis").".xls\";");

require("../../inc/conf_dentro.php");

$fechaInicio = strtotime(str_replace("/","-",$_REQUEST["desde"])) ;
$fechaFin = strtotime(str_replace("/","-",$_REQUEST["hasta"])) ;

if($_REQUEST["bodega"] <> ""){
	$filtros .= " and mov.mov_bodega = '$_REQUEST[bodega]' ";
}
if($_REQUEST["concepto"] <> ""){
	$filtros .= " and mov.mov_concepto = '$_REQUEST[concepto]' ";
}
if($_REQUEST["auxiliar"] <> ""){
	$filtros .= " and mov.mov_auxiliar = '$_REQUEST[auxiliar]' ";
}

// ---------------------------------------------------------------------------------------------------
if($_REQUEST["fil_codigo"] <> ""){
	$filtros .= " and pro.pro_codigo like '%".$_REQUEST["fil_codigo"]."%' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and pro.pro_nombre like '%".$_REQUEST["fil_descripcion"]."%' "; 
}
if($_REQUEST["fil_barra"] <> ""){
	$filtros .= " and pro.pro_codigo_barra like '%".$_REQUEST["fil_barra"]."%' ";
}
for($i = 1;$i <= 20;$i++){
	if($_REQUEST["fil_extra_".$i] <> ""){
		$filtros .= " and pro.pro_extra_".$i." like '%".$_REQUEST["fil_extra_".$i]."%' ";
	}
}
if($_REQUEST["fil_unidad"] <> ""){
	$filtros .= " and pro.pro_unidad = '".$_REQUEST["fil_unidad"]."' ";
}
if($_REQUEST["fil_grupo"] <> ""){
	$filtros .= " and pro.pro_grupo = '".$_REQUEST["fil_grupo"]."' ";
}
if($_REQUEST["fil_subgrupo"] <> ""){
	$filtros .= " and pro.pro_subgrupo = '".$_REQUEST["fil_subgrupo"]."' ";
}
if($_REQUEST["fil_categoria"] <> ""){
	$filtros .= " and pro.pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
}
if($_REQUEST["fil_tipo"] <> ""){
	$filtros .= " and pro.pro_tipo = '".$_REQUEST["fil_tipo"]."' ";
}
// ---------------------------------------------------------------------------------------------------
?>  
<table border="1" cellpadding="2" cellspacing="0"> 
<thead> 
	<tr>   
		<th>Folio</th>
		<th>Fecha</th>
		<th>Lugar</th>
		<th>Concepto</th>
		<th colspan="2">Proveedor</th> 
		<th>Cantidad</th>
		<th colspan="2">Producto</th>
	</tr> 
</thead>
<tbody>
<?
for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
	$dia[] = date("d/m", $i);

	$res = sql_movimientos_detalle("*,sum(det_ingreso) as ingreso"," 
	and mov.mov_tipo = 'E' and mov.mov_fecha = '".date("Ymd", $i)."' $filtros 
	group by mov.mov_id,pro.pro_codigo"); 
	if(mysqli_num_rows($res) > 0){ 
		while($row = mysqli_fetch_array($res)){  
			?>
			<tr> 
				<td style="text-align:center;"><?php echo $row["mov_folio"]; ?></td>
				<td style="text-align:center;"><?php echo _fec($row["mov_fecha"],5); ?></td>
				<td style="text-align:center;"><?php echo $row["mov_bodega"]; ?></td>
				<td><?php echo $row["con_nombre"]; ?></td>
				<td><?php echo $row["aux_rut"]; ?></td>
				<td><?php echo utf8_decode($row["aux_nombre"]); ?></td>
				<td style="text-align:center;"><?php echo $row["ingreso"]; ?></td>
				<td><?php echo $row["pro_codigo"]; ?></td>
				<td><?php echo utf8_decode($row["pro_nombre"]); ?></td>
			</tr>
			<?
		} 
	}
}
?> 
</tbody> 
</table> 