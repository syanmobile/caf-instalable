<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Editar Campo Personalizado";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

$res = sql_campos("*","and cam_id = '$_REQUEST[id]'","","");  
if(mysqli_num_rows($res) > 0){
	$datos = mysqli_fetch_array($res);
}else{
	?>
	<div class="alert alert-danger"> 
		<strong>No se encontró el dato</strong>
	</div>
    <?php
	construir_boton("cam_listado.php","","buscar","Listado de Campos",2);
	exit();
}
?>
<script language="javascript">
function save(){
	if(document.getElementById("nombre").value == ""){
		alerta_js("Es obligación ingresar el nombre");
		return;	
	}
    if(document.getElementById("tipo").value == ""){
        alerta_js("Es obligación ingresar el tipo");
        return; 
    }
	carg("cam_save.php","");
}
function tipo_campo(valor){
    switch(valor){
        case "check":
        case "radio":
        case "select":
            $("#opciones_").show();
            break;
        default:
            $("#opciones_").hide();
            break;
    }
}
$(document).ready(function() {
    $('#opciones').tagsInput({width:'auto'});
});
</script> 

<form class="form-horizontal" role="form" method="post">
	<input type="hidden" name="modo" value="editar" class="campos"> 
	<input type="hidden" name="id" value="<? echo $_REQUEST["id"]; ?>" class="campos"> 
    <div class="form-group">
        <label for="orden" class="col-sm-2 control-label">Nombre <span class="oblig">(*)</span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control campos" id="nombre" name="nombre" value="<? echo $datos["cam_nombre"]; ?>">
        </div>
    </div> 
    <div class="form-group">
        <label for="orden" class="col-sm-2 control-label">Tipo <span class="oblig">(*)</span></label>
        <div class="col-sm-3">
            <select class="form-control campos" id="tipo" name="tipo" onchange="javascript:tipo_campo(this.value);"> 
                <option value="texto" <? if($datos["cam_tipo"] == "texto"){ echo "selected"; } ?>>Texto</option>
                <option value="textarea" <? if($datos["cam_tipo"] == "textarea"){ echo "selected"; } ?>>Textarea</option>
                <option value="numero" <? if($datos["cam_tipo"] == "numero"){ echo "selected"; } ?>>Número</option>
                <option value="fecha" <? if($datos["cam_tipo"] == "fecha"){ echo "selected"; } ?>>Fecha</option>
                <option value="select" <? if($datos["cam_tipo"] == "select"){ echo "selected"; } ?>>Selección</option>
                <option value="archivo" <? if($datos["cam_tipo"] == "archivo"){ echo "selected"; } ?>>Archivo</option>
                <?
                /*
                <option value="imagen" <? if($datos["cam_tipo"] == "imagen"){ echo "selected"; } ?>>Imagen</option>
                <option value="imagenes" <? if($datos["cam_tipo"] == "imagenes"){ echo "selected"; } ?>>Imagenes</option>
                <option value="check" <? if($datos["cam_tipo"] == "check"){ echo "selected"; } ?>>Checkbox</option>
                <option value="radio" <? if($datos["cam_tipo"] == "radio"){ echo "selected"; } ?>>Radio</option>
                */
                ?>
            </select>
        </div>
    </div> 
    <div class="form-group" id="opciones_" <?
    switch($datos["cam_tipo"]){
        case "check":
        case "radio":
        case "select":    
            break;
        default:
            echo 'style="display: none;"'; 
            break;
    } 
    ?>>
        <label for="orden" class="col-sm-2 control-label">Opciones</label>
        <div class="col-sm-10">
            <textarea rows="8" class="form-control campos" id="opciones" name="opciones"><?php echo $datos["cam_opciones"]; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="orden" class="col-sm-2 control-label">Para Producto</label>
        <div class="col-sm-5">
           <select name="producto" class="campos">
            <option value="0" <? if($datos["cam_producto"] == "0"){ echo "selected"; } 
            ?>>-</option>
            <option value="1" <? if($datos["cam_producto"] == "1"){ echo "selected"; } 
            ?>>Campo disponible en la ficha del producto</option>
            </select>
        </div>
    </div> 
    <div class="form-group">
        <label for="orden" class="col-sm-2 control-label">Para Activo</label>
        <div class="col-sm-5">
           <select name="activo" class="campos">
            <option value="0" <? if($datos["cam_activo"] == "0"){ echo "selected"; } 
            ?>>-</option>
            <option value="1" <? if($datos["cam_activo"] == "1"){ echo "selected"; } 
            ?>>Campo disponible en la ficha del activo</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-2"/>
        <div class="col-sm-10">
        	<?php
			construir_boton("","","grabar","Guardar",3);
			construir_boton("cam_listado.php","","eliminar","Cancelar",2);
			?>
        </div>
    </div>
</form> 