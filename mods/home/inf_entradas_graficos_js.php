<?php 
$fechaInicio = strtotime(str_replace("/","-",$_REQUEST["desde"])) ;
$fechaFin = strtotime(str_replace("/","-",$_REQUEST["hasta"])) ;

if($_REQUEST["bodega"] <> ""){
	$filtros .= " and mov.mov_bodega = '$_REQUEST[bodega]' ";
}
if($_REQUEST["concepto"] <> ""){
	$filtros .= " and mov.mov_concepto = '$_REQUEST[concepto]' ";
}
if($_REQUEST["auxiliar"] <> ""){
	$filtros .= " and mov.mov_auxiliar = '$_REQUEST[auxiliar]' ";
}

// ---------------------------------------------------------------------------------------------------
if($_REQUEST["fil_codigo"] <> ""){
	$filtros .= " and pro.pro_codigo like '%".$_REQUEST["fil_codigo"]."%' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and pro.pro_nombre like '%".$_REQUEST["fil_descripcion"]."%' ";
}
if($_REQUEST["fil_barra"] <> ""){
	$filtros .= " and pro.pro_codigo_barra like '%".$_REQUEST["fil_barra"]."%' ";
}
for($i = 1;$i <= 20;$i++){
	if($_REQUEST["fil_extra_".$i] <> ""){
		$filtros .= " and pro.pro_extra_".$i." like '%".$_REQUEST["fil_extra_".$i]."%' ";
	}
}
if($_REQUEST["fil_unidad"] <> ""){
	$filtros .= " and pro.pro_unidad = '".$_REQUEST["fil_unidad"]."' ";
}
if($_REQUEST["fil_grupo"] <> ""){
	$filtros .= " and pro.pro_grupo = '".$_REQUEST["fil_grupo"]."' ";
}
if($_REQUEST["fil_subgrupo"] <> ""){
	$filtros .= " and pro.pro_subgrupo = '".$_REQUEST["fil_subgrupo"]."' ";
}
if($_REQUEST["fil_categoria"] <> ""){
	$filtros .= " and pro.pro_categoria = '".$_REQUEST["fil_categoria"]."' ";
}
if($_REQUEST["fil_tipo"] <> ""){
	$filtros .= " and pro.pro_tipo = '".$_REQUEST["fil_tipo"]."' ";
}
// ---------------------------------------------------------------------------------------------------
?>
<table width="100%">
	<tr valign="top">
		<td>
			<?
    		construir_boton("excel()","","importar","Excel",4);
			?>
			<table class="table table-bordered table-hover table-condensed"> 
			<thead>
				<tr>   
					<th style="text-align:center;" width="1">Folio</th>
					<th style="text-align:center;" width="1">Fecha</th>
					<th style="text-align:center;" width="1">Lugar</th>
					<th>Concepto</th>
					<th colspan="2">Proveedor</th> 
					<th style="text-align:right;" width="1">Cantidad</th>
					<th colspan="2">Producto</th> 
				</tr>
			</thead>
			<tbody>
			<?
			for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
				$dia[] = date("d/m", $i);

				$res = sql_movimientos_detalle("*,sum(det_ingreso) as ingreso"," 
				and mov.mov_tipo = 'E' and mov.mov_fecha = '".date("Ymd", $i)."' $filtros 
				group by mov.mov_id,pro.pro_codigo","","S"); 
				if(mysqli_num_rows($res) > 0){ 
					while($row = mysqli_fetch_array($res)){ 
						?>
						<tr> 
							<td style="text-align:center;"><?php echo $row["mov_folio"]; ?></td>
							<td style="text-align:center;"><?php echo _fec($row["mov_fecha"],5); ?></td>
							<td style="text-align:center;"><?php echo $row["mov_bodega"]; ?></td>
							<td><?php echo $row["con_nombre"]; ?></td>
							<td width="70"><?php echo $row["aux_rut"]; ?></td>
							<td><?php echo $row["aux_nombre"]; ?></td>
							<td style="text-align:center;"><?php echo $row["ingreso"]; ?></td>
							<td width="1"><?php echo $row["pro_codigo"]; ?></td>
							<td><?php echo $row["pro_nombre"]; ?></td> 
						</tr>
						<?
						if(!in_array($row["pro_codigo"],$productos)){
							$productos[] = $row["pro_codigo"];
						} 
						$cantidades[$row["pro_codigo"]] += $row["ingreso"];
						$descripcion[$row["pro_codigo"]] = $row["pro_nombre"];
					} 
				}
			}
			?> 
			</tbody>
			</table>
		</td>
		<td style="padding-left: 20px;">
			<?
    		construir_boton("excel2()","","importar","Excel",4);
			?>
			<table class="table table-bordered table-hover table-condensed"> 
			<thead>
				<tr>    
					<th>Codigo</th>
					<th>Producto</th>
					<th style="text-align:right;" width="1">Cantidad</th>
				</tr>
			</thead>
			<tbody>
			<?
			foreach($productos as $pro){ 
				?>
				<tr>  
					<td width="1"><?php echo $pro; ?></td>
					<td><?php echo $descripcion[$pro]; ?></td>
					<td style="text-align:center;"><?php echo $cantidades[$pro]; ?></td>
				</tr>
				<? 
				$total += $cantidades[$pro];
			}
			?> 
			</tbody>
			<tfoot>
				<tr>
					<th colspan="2">TOTAL ITEMS</th>
					<th style="text-align:center;"><? echo $total; ?></th>
				</tr>
			</tfoot>
			</table>
		</td>
	</tr>
</table>