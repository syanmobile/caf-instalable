<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Abrir Año en Tabla IPC";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "ipc_listado.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------
?>
<script type="text/javascript">
function save(){   
    carg("ipc_save.php","");
}
</script>

<input type="hidden" name="modo" value="editar" class="campos">
<input type="hidden" name="ano" value="<? echo $_REQUEST["ano"]; ?>" class="campos">
<table class="table table-striped table-bordered table-condensed"> 
<thead>
<tr>
    <th>Año</th>
    <th>IPC A.Anterior</th>
    <?
    for($i = 1;$i <= 12;$i++){
        ?><th width="70" style="text-align: center;"><? echo _mes($i); ?></th><?
    }
    ?>
</tr>
</thead>
<tbody> 
    <tr>  
        <th><? echo $_REQUEST["ano"]; ?></th>
        <?
        for($i = 1;$i <= 12;$i++){
            $mes = ($i < 10)?"0".$i:$i;

            $sql= "select * from tabla_ipc where ipc_periodo = '".$_POST["ano"].$mes."'"; 
            $res = mysqli_query($cnx,$sql);
            $row = mysqli_fetch_array($res);

            if($i == 1){
                ?><td><input type="text" name="anterior" class="campos inpcen w10" value="<?
                echo $row["ipc_anterior"]; ?>"></td><?
            }
            ?><td><input type="text" name="mes_<? echo $i; ?>" class="campos inpcen w10" value="<?
            echo $row["ipc_valor"]; ?>"></td><?
        }
        ?>
    </tr> 
</tbody>
</table>   
<br> 
<?php 
construir_boton("","","grabar","Guardar Año",3);
construir_boton("ipc_listado.php","","eliminar","Cancelar",2);
?> 