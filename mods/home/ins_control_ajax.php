<?php 
require("../../inc/conf_dentro.php");
 
switch($_REQUEST["modo"]){
	case "disponibles": 
		$res = sql_insumos("*"," and ubi_bodega = '$_REQUEST[bodega]' order by pro_nombre asc "); 
		if(mysqli_num_rows($res) > 0){
			construir_boton("imprimir()","","imprimir","Imprimir Hoja para Tomar Inventario",4); 
			while($row = mysqli_fetch_array($res)){
				if($_grupos[$row["pro_grupo"]] == ""){
					$row["gru_nombre"] = "* Sin grupo *";
				}else{
					$row["gru_nombre"] = $_grupos[$row["pro_grupo"]];
				}
				if($_subgrupos[$row["pro_subgrupo"]] == ""){
					$row["sgru_nombre"] = "* Sin subgrupo *";
				}else{
					$row["sgru_nombre"] = $_subgrupos[$row["pro_subgrupo"]];
				}
				
				if($row["gru_nombre"] <> $gru){
					?>
					<table class="table table-striped table-bordered tabledrag table-condensed"> 
					<thead>
					<tr>
						<th colspan="15"><?php echo $row["gru_nombre"]; ?></th>
					</tr>
					</thead>
					<tbody>
					<?
					$gru = $row["gru_nombre"];
					$sgru = "";
				}
				if($row["sgru_nombre"] <> $sgru){ 
					?>
					<tr>
						<th style="background-color:#FFFBD2;" colspan="15"><?php echo $row["sgru_nombre"]; ?></th>
					</tr>
					<tr>   
						<th>Producto</th>
						<th width="220">Ubicación</th>
						<th width="100" style="text-align: center;">Stock</th>
						<th width="100" style="text-align: center;">Conteo</th>
						<th width="100" style="text-align: center;">Diferencia</th>
					</tr>
					<?
					$sgru = $row["sgru_nombre"];
				} 
				$i++; 
				?>
				<input type="hidden" value="<? echo $row["det_producto"].",".$row["det_ubi_id"]; ?>" id="prod_<? echo $i; ?>">
				<input type="hidden" value="<? echo $row["total"]; ?>" id="cant_<? echo $i; ?>">
				<input type="hidden" value="-<? echo $row["total"]; ?>" id="dife_<? echo $i; ?>">
				<tr>  
					<td><? echo $row["pro_codigo"]; ?> - <? echo $row["pro_nombre"]; ?></td>
					<td><? echo $row["ubi_codigo"]; ?> - <? echo $row["ubi_nombre"]; ?></td>
					<td style="text-align: center; font-size: 18px;"><? echo $row["total"]; ?></td>
					<td><input type="text" value="0" class="w10" onchange="javascript:calcular();"  
					style="text-align: center; font-size: 15px; height: 30px;" id="cont_<? echo $i; ?>"></td>
					<td style="text-align: center;" id="dife_text_<? echo $i; ?>">
						<span class="label label-danger" style=" font-size: 15px !important;">-<? echo $row["total"]; ?></span>
					</td>
				</tr>
				<?
			}
			?> 
			</tbody>
			</table>
			<textarea id="ajuste_lineas" name="ajuste_lineas" class="campos" style="display: none;"></textarea>

			<script language="javascript"> 
            function calcular(){
				var texto = "";
				var cant = 0; 
				var cont = 0; 
				var dife = 0; 
				<?php
				for($i2 = 1;$i2 <= $i;$i2++){
					?>
					cant = document.getElementById("cant_<? echo $i2; ?>").value * 1;
					cont = document.getElementById("cont_<? echo $i2; ?>").value * 1;
					dife = cont - cant;
					document.getElementById("dife_<? echo $i2; ?>").value = dife;
					if(dife == 0){
						$("#dife_text_<? echo $i2; 
							?>").html('<span class="label label-default" style=" font-size: 15px !important;">'+dife+'</span>');
					}else{
						if(dife < 0){
							$("#dife_text_<? echo $i2; 
								?>").html('<span class="label label-danger" style=" font-size: 15px !important;">'+dife+'</span>');
						}else{
							$("#dife_text_<? echo $i2; 
								?>").html('<span class="label label-success" style=" font-size: 15px !important;">'+dife+'</span>');
						}
					}
					if(dife != 0){
						texto = texto + ";" + document.getElementById("prod_<? echo $i2; ?>").value + "," + dife;
					}
					<?
				}
				?>
				document.getElementById("ajuste_lineas").value = texto;
			} 
            </script> 
			<?
		}else{
			?>
			<div class="alert alert-danger">
				<strong>Sin registros creados</strong>
			</div>
			<?php 
		}  
		break;
}
?> 