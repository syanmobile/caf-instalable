<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Nuevo Centro de Costo";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function save(){ 
	if(document.getElementById("codigo").value == ""){
		alerta_js("Es obligación ingresar el código");
		return;	
	}
	if(document.getElementById("nombre").value == ""){
		alerta_js("Es obligación ingresar el nombre");
		return;	
	} 
    var div = document.getElementById("codigo_unico");
    AJAXPOST("mods/home/cco_ajax.php","&modo=codigo_unico&codigo="+document.getElementById("codigo").value,div);
}
</script> 

<form class="form-horizontal" role="form" method="post">
	<input type="hidden" name="modo" value="crear" class="campos"> 
     
    <div class="form-group">
        <label for="orden" class="col-sm-2 control-label">Código <span class="oblig">(*)</span></label>
        <div class="col-sm-5">
            <table width="100%">
            <tr>
            <td width="1"><input type="text" name="codigo" id="codigo" class="campos w10" style="text-align:center; width:130px !important;"></td>
            <td id="codigo_unico" style="padding-left:5px;"></td>
            </tr>
            </table>
        </div>
    </div>
    <div class="form-group">
        <label for="nombre" class="col-sm-2 control-label">Nombre <span class="oblig">(*)</span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control campos" id="nombre" name="nombre">
        </div>
    </div>  
    
    <div class="form-group">
        <div class="col-sm-2"/>
        <div class="col-sm-10">
        	<?php
			construir_boton("","","grabar","Guardar",3);
			construir_boton("cco_listado.php","","eliminar","Cancelar",2);
			?>
        </div>
    </div>
</form> 