<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabación";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

switch($_POST["modo"]){
	case "crear": 
		$sql = "INSERT INTO campos ( 
			cam_nombre,
			cam_tipo,
			cam_opciones,
			cam_producto,
			cam_activo
		) VALUES ( 
			'$_POST[nombre]',
			'$_POST[tipo]',
			'$_POST[opciones]',
			'$_POST[producto]',
			'$_POST[activo]'
		)";
		$res = mysqli_query($cnx,$sql);
		$_POST["id"] = mysqli_insert_id($cnx);
		?>
		<div class="alert alert-success"> 
		
			<strong>Campo '<? echo $_POST["nombre"]; ?>' creado con &eacute;xito</strong>
		</div>
		<?php  
		break;
		
	case "editar":
		$SQL_ = "UPDATE campos SET 
			cam_nombre = '$_POST[nombre]' ,
			cam_tipo = '$_POST[tipo]',
			cam_opciones = '$_POST[opciones]',
			cam_producto = '$_POST[producto]',
			cam_activo= '$_POST[activo]' 
		WHERE cam_id = '$_POST[id]' ";   
		$res = mysqli_query($cnx,$SQL_);
		?>   
        <div class="alert alert-success"> 
           Campo <strong>'<? echo $_POST["nombre"]; ?>'</strong> editado con &eacute;xito
        </div>  
		<?php  
		break;
}

construir_boton("cam_listado.php","","buscar","Listado de Campos",2);
if($_REQUEST["modo"] <> "eliminar"){
	construir_boton("cam_editar.php","&id=".$_POST["id"],"editar","Editar este Campo",2);
}
construir_boton("cam_nuevo.php","","crear","Crear otro Campo",2);
?>