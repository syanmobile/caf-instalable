<?php 
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Nueva Garantía de activos";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script type="text/javascript">
$(document).ready(function(){
	input_auxiliar("");
});



	
function save(){
	if(document.getElementById("lineas").value == ""){
		//alerta_js("Es obligación seleccionar un ACTIVO");
		//return;	
	} 
	
	if(confirm("Grabar Garantia?")){
		carg("gar_save.php","");
	}
} 
	
	
//**************************************
function crear_auxiliar(){
	AJAXPOST(url_base+modulo_base+"aux_ajax.php","modo=crear_form",document.getElementById("modGeneral_lugar"),false,function(){			
		$('#modGeneral').modal('show'); 
	});
}
	function save_auxiliar(){  
	if(document.getElementById("aux_codigo").value == ""){
		document.getElementById("aux_codigo").focus();
		alert("Es obligación ingresar el CODIGO");
		return;	
	} 
	if(document.getElementById("aux_nombre").value == ""){
		document.getElementById("aux_nombre").focus();
		alert("Es obligación ingresar el NOMBRE");
		return;	
	}
	var a = $(".campos").fieldSerialize();  
	var div = document.getElementById("lugar_aux_save");
	AJAXPOST("mods/home/aux_ajax.php",a+"&modo=crear_rapido",div);
}
function input_auxiliar(codigo){
	AJAXPOST(url_base+modulo_base+"aux_ajax.php","modo=inicio_input&codigo="+codigo,document.getElementById("input_auxiliar"));
}
</script> 



<input type="hidden" name="modo" value="crear" class="campos">
<table width="100%" class="tabla_opciones">
<tbody> 
	<tr valign="top">
		<td width="35%">
			<table class="table table-bordered table-condensed"> 
			<thead>
			<tr>
				<th colspan="4">Detalles de la Garantía</th>
			</tr>
			</thead> 
			<tbody>
			<tr>
				<th>Proveedor:</th>
				<td id="input_auxiliar" colspan="3"></td>        
			</tr> 
			<tr>
				<th>Póliza:</th>
				<td><input type="text" name="poliza" id="poliza" class="campos" style="float: left;"></td>
				<th width="120">Nro Referencia:</th>
	        	<td><input type="text" name="nro_referencia" id="nro_referencia" class="campos" style="float: left;"></td>
			</tr> 
			<tr> 
			<th>Inicio:</th>
	        <td><input type="text" name="fecha_ini" id="fecha_ini" class="fecha campos" style="float: left;" autocomplete="off"></td>
	        <th>Término:</th>
	        <td><input type="text" name="fecha_fin" id="fecha_fin" class="fecha campos" style="float: left;" autocomplete="off"></td>
        	</tr>
        	<tr>
		        <th>Costo:</th>
		        <td colspan="3"><input type="text" name="costo" id="costo" class="campos" style="float: left;"></td>
	        </tr> 
			<tr>
				<th>Notas:</th>
				<td colspan="3"><textarea name="notas" id="notas" class="campos w10" rows="4"></textarea></td>
			</tr>

			<tr>
	          <th>Archivos:</th>
	          	<td colspan="3">
	
	                <form method="post" name="multiple_upload_form" id="multiple_upload_form" 
						enctype="multipart/form-data"
						action="<?php echo $url_web; ?>mods/home/gar_ajax.php">
							<input type="file" name="archivo" id="archivo">
						    <div class="uploading none" style="display:none;">
						        <label>&nbsp;</label>
						        <img src="img/uploading.gif"/>
						    </div>
						    <div id="procesando_upload">
						    	 <input type="hidden" class="campos" name="arcdir" id="arcdir" value="" readonly>	
							</div> 
					</form> 
	            </td>
            </tr>
			
			</tbody>
			</table>

			<br>
			<?php 
				construir_boton("save()","","grabar","Guardar Garantía",4);
				construir_boton("gar_listado.php","","eliminar","Cancelar",2);
			?>

		</td>
		<td width="380" style="padding-left: 15px">


	    	<div id="detalle" style="margin: 10px 0px;">
	    		








































	    	</div>
			<input type="hidden" id="lineas" name="lineas" class="campos"> 
	    </td>
	</tr>
</tbody>
</table>








<script language="javascript">
$(document).ready(function(){
	$(".buscador").chosen({
		width:"95%",
		no_results_text:'Sin resultados con',
		allow_single_deselect:true 
	});
	$(".fecha").datepicker();
	
	$('#archivo').on('change',function(){ 
    	if($("#arcdir").val()==""){
    		if(confirm("Desea Subir el archivo?")){
	    		$('#multiple_upload_form').ajaxForm({
		            target:'#procesando_upload',
		            data:{ modo: 'upload_archivo'}, 
		            beforeSubmit:function(e){
		                $('.uploading').show();
		            },
		            success:function(e){
		                $('.uploading').hide();
		            },
		            error:function(e){
		            }
		        }).submit(); 
	    	}else{
	    		$(this).val("");
	    	}
    	}else{
    		alert("ya cargó un archivo. Eliminelo y podrá cargar otro");
    		$(this).val("");
    	}
    });


});

function eliminar_archivo(){

    var a = $(".campos").fieldSerialize();
    AJAXPOST("mods/home/gar_ajax.php",a+"&modo=eliminar_archivo",false,false, function (){
    	document.getElementById("arcdir").value=""; 
    	$("#alert_archivo").alert('close');
    	$("#archivo").val("");
    });
}
</script>






<!-- Modal Productos --> 
<div class="modal fade" id="modActivos" tabindex="-1" role="dialog" aria-labelledby="modActivos" aria-hidden="true">
    <div class="modal-dialog" style="width:1100px;">
        <div class="modal-content" style="padding:15px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <table width="100%">
            <tr valign="top">
            	<td width="300"> 
                	<div id="fmp_filtros">XXXXX</div>
                </td>
            	<td>
                	<div id="fmp_resultados" style="margin-left:10px; overflow:scroll; height:500px; padding-right:5px;">ZZZZ</div>
                </td>
            </tr>
            </table>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script language="javascript"> 
function fmp_filtros(){
	var div = document.getElementById("fmp_filtros");
	var a = $(".fmp_campos").fieldSerialize();
	AJAXPOST(url_base+modulo_base+"gar_ajax.php",a+"&modo=fmp_inicio",div);
}
function __fmp_buscar(){
	$("#fmp_resultados").html("Espere un momento...");
	var div = document.getElementById("fmp_resultados");
	var a = $(".fmp_campos").fieldSerialize(); 
	//AJAXPOST(url_base+modulo_base+"aux_ajax.php",a+"&modo=busqueda",div);
}
function productos_popup(){ 
	fmp_filtros();	
	$('#modActivos').modal('show');
}
function __busc_auxiliar(){
	var div = document.getElementById("load_auxiliar");
	var codigo = document.getElementById("auxiliar").value;
	//AJAXPOST(url_base+modulo_base+"aux_ajax.php","modo=buscar&codigo="+codigo,div);
}
function __elegir_auxiliar(codi,nomb){ 
	document.getElementById("auxiliar").value = codi;
	document.getElementById("auxiliar_nomb").value = nomb; 
	$('#modAuxiliares').modal('hide');
}
</script>