<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Personas";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "per_listado.php"; 
include("ayuda.php");
//----------------------------------------------------------------------------------------

$res = sql_personas("*"," and per.per_tipo = 1 ORDER BY per.per_nombre asc ");  
$num = mysqli_num_rows($res); 
?> 
<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td>
			<?php
            construir_boton("per_nuevo.php","","crear","Nueva Persona",2); 
			?>
        </td> 
        <td width="200" style="text-align: right;">
            Personas: <b><? echo $num; ?></b>
        </td>
    </tr>
</tbody>
</table> 
<?php 
if($num > 0){
    ?>
    <table class="table table-bordered table-condensed"> 
    <thead>
    <tr>
        <th width="1" align="center">&nbsp;</th> 
        <th width="1">Imagen</th> 
        <th>Nombre Persona</th>     
        <th width="1">Estado</th>  
    </tr>
    </thead>
    <tbody> 
    <?php 
    while($row = mysqli_fetch_array($res)){
        ?>
        <tr fila="<?php echo $row["per_id"] ;?>" id="<?php echo $row["per_id"] ;?>" <?php
        if($row["per_elim"] == 1){ ?> style="background-color:#ffeded;" <? } ?>>  
            <td style="text-align:center;"><?php 
			construir_boton("per_editar.php","&id=".$row["per_id"],"editar","Editar"); 
            construir_boton("inf_bitacora_persona.php","&id=".$row["per_id"],"imprimir","Ver Informe"); 
			?></td> 
            <td><? 
            $imagen = ($row["per_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$row["per_imagen"];
            ?><img src="<?php echo $imagen; ?>" style="width:40px !important;"></td>
            <td><b><?php echo $row["per_nombre"]; ?></b><br>
            RUT: <?php echo $row["per_rut"]; ?><br>
            Correo: <?php echo $row["per_correo"]; ?></td>    
            <td><?php 
            if($row["per_elim"]){
                ?><span class="label label-danger">Inactivo</span><?
            }else{
                ?><span class="label label-success">Activo</span><?
            } 
            ?></td>   
        </tr>
        <?php 
    }
    ?> 
    </tbody>
    </table>  
    <?php
	echo $paginador_dibujo;
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin registros creados</strong>
    </div>
    <?php 
}
?>