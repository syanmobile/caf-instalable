<div class="col-md-4 grid-item">
	<div class="panel panel-primary">  
		<div class="panel-heading">
			<b>Responsables con Activos Fijos</b>
			<div style="float: right;">
				<a href="javascript:ampliar_responsables_con_activos()" style="color:#fff;">(Ampliar)</a>
			</div>
		</div>
		<div class="panel-body"> 
			<?php
			$total = 0;
			$labeles = "";
			$labeles_largo = "";
			$valores = "";

			$res = sql_activos_fijos("*,count(*) as stock"," and (acf_disponible = 2 or acf_disponible = 6) and acf_responsable <> 0 group by acf.acf_responsable order by stock desc limit 0,10"); 
			if(mysqli_num_rows($res) > 0){
				?>
				<table class="table table-condensed table-bordered"> 
				<thead>
					<tr>
						<th>Responsable</th>
						<th width="1">Activo(s)</th>
					</tr>
				</thead>
				<tbody>
				<?
				while($row = mysqli_fetch_array($res)){ 
					$total += $row["stock"] * 1;
					?>
					<tr> 
						<td style=" padding: 1px 5px;"><?php echo $_personas[$row["acf_responsable"]]; ?></td>
						<th style="text-align: center; padding: 1px 5px;"><? echo _num2($row["stock"]); ?></th>
					</tr>
					<?
					$labeles.= ";".$_personas[$row["acf_responsable"]];
					$labeles_largo.= ";".$_personas[$row["acf_responsable"]]." (".$row["stock"].")";
					$valores.= ";".$row["stock"];
				}
				$sin_responsable = $total_de_activos - $total;
				if($sin_responsable > 0){
					?>
					<tr>  
						<td style="padding: 1px 5px;">Sin Responsable</td> 
						<th style="text-align: center; padding: 1px 5px;"><? echo _num2($sin_responsable); ?></th>
					</tr>
					<?
					$labeles.= ";Sin Responsable";
					$labeles_largo.= ";Sin Responsable (".$sin_responsable.")";
					$valores.= ";".$sin_responsable;
				}
				?>
				</tbody>
				</table>
				<?
			}else{
				?> 
				<div class="alert alert-success">
					<strong>AL DIA</strong> (Sin trabajadores morosos)
				</div>
				<?
			}
			?>  
			<iframe src="mods/home/widget_grafico_donas.php?label=<? echo $labeles; ?>&valor=<? echo $valores; ?>&alto=240" frameborder="0" scrolling="no" style="width: 100%; height: 230px;"></iframe> 
			<script type="text/javascript">
			function ampliar_responsables_con_activos(){
				visor("widget_grafico_donas.php?label=<? echo $labeles_largo; ?>&valor=<? echo $valores; ?>&alto=390&titulo=Activos clasificados por Responsables&imprimir=s");
			}
			</script> 
		</div>
	</div>
</div>