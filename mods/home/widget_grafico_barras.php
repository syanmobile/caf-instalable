<?php
require("../../inc/conf_dentro.php"); 

$labels = explode(";",$_REQUEST["label"]);
$valores = explode(";",$_REQUEST["valor"]);
?>
<script src="../../js/jquery-1.11.1.min.js"></script>
<script src="../../js/graph/highcharts.js"></script>
<script src="../../js/graph/modules/exporting.js"></script>
<script src="../../js/graph/modules/export-data.js"></script>
<script src="../../js/graph/modules/accessibility.js"></script>

<script src="../../js/main.js"></script>

<div id="container" style="width: 100%; height: <? if($_REQUEST["alto"] == ""){ echo "200"; }else{ echo $_REQUEST["alto"]; } ?>px; margin: 0 auto"></div>
	
<script language="javascript">
$(function () {
	$('#container').highcharts({
		colors: ["#C0392B","#9B59B6","#2980B9","#1ABC9C","#27AE60","#F1C40F","#E67E22","#95A5A6","#34495E", "#E74C3C","#8E44AD","#3498DB","#16A085","#2ECC71", "#F39C12","#D35400","#7F8C8D","#566573"],
		chart: {
	        type: 'bar'
	    },
	    title: false,
	    xAxis: {
	        categories: [<?
			for($i = 1;$i < count($labels);$i++){
				switch($_REQUEST["nombre"]){
					case "bodega": 
						$label = $_bodegas[$labels[$i]]; 
						break;
					case "tipo": 
						$label = $_tipo_activo[$labels[$i]]; 
						break;
					default: 
						$label = $labels[$i]; 
						break;
				}
				$label =  preg_replace("/[\r\n|\n|\r]+/", " ", $label);
				if($i > 1){ echo ", "; }
				?>'<? echo $label; ?>'<?
			} 
			?>],
	        title: {
	            text: null
	        }
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Population (millions)',
	            align: 'high'
	        },
	        labels: {
	            overflow: 'justify'
	        }
	    },
	    tooltip: {
	        valueSuffix: ' millions'
	    },
	    plotOptions: {
	        bar: {
	            dataLabels: {
	                enabled: true
	            }
	        }
	    },
	    legend: {
	        layout: 'vertical',
	        align: 'right',
	        verticalAlign: 'top',
	        x: -40,
	        y: 80,
	        floating: true,
	        borderWidth: 1, 
	        shadow: true
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Activos',
	        data: [<?
			for($i = 1;$i < count($valores);$i++){
				if($i > 1){ echo ", "; }
				echo $valores[$i];
			} 
			?>]
	    } ]
	});
});
</script>
