<div class="col-md-<? echo $ancho; ?> grid-item">
	<div class="panel panel-primary">  
		<div class="panel-heading">
			<b>Insumos por Clasificación</b>
		</div>
		<div class="panel-body"> 
			<?      
			$sql2 = "select *,sum(total) as stock from insumos group by pro_categoria order by stock desc ";
			$res = mysqli_query($cnx,$sql); 
			if(mysqli_num_rows($res) > 0){
				?>
				<table class="table table-condensed table-bordered"> 
				<thead>
					<tr>
						<th width="1">Stock</th>
						<th>Clasificación</th>
					</tr>
				</thead>
				<tbody>
				<?
				while($row = mysqli_fetch_array($res)){  
					$i_w1++;
					?>
					<tr <? if($i_w1 > 5){ echo 'class="oculto_w1" style="display:none;"'; } ?>>  
						<th style="text-align: center;"><? echo _num2($row["stock"]); ?></th>
						<td><? echo $_categorias[$row["pro_categoria"]]; ?></td> 
					</tr>
					<? 
				}
				?>  
				</tbody>
				</table>
				<?
                if($i_w1 > 5){
                	construir_boton("mostrar_w1()","","buscar","Ver más",4); 
                }
                ?> 
				<script type="text/javascript">
				function mostrar_w1(){
					$(".oculto_w1").toggle("fast");
				}	
				</script>
				<?
			}else{
				?>
				<div class="alert alert-danger">
					<strong>Sin stock de insumos</strong>
				</div>
				<?php 
			}
			?>  
		</div>
	</div>
</div>