<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabación";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

switch($_POST["modo"]){
	case "crear":
		$res = sql_bodegas("*"," and bod.bod_codigo = '$_REQUEST[codigo]'","","");
		if(mysqli_num_rows($res) > 0){
			?>
            <div class="alert alert-danger"> 
               <b>ERROR</b>, código ya existe: '<strong><? echo $_POST["codigo"]; ?></strong>'
            </div>
            <?
		}else{ 
			$sql = "INSERT INTO bodegas ( 
				bod_codigo,
				bod_nombre 
			) VALUES ( 
				'$_POST[codigo]',
				'$_POST[nombre]' 
			)";
			$res = mysqli_query($cnx,$sql);
			$_POST["id"] = mysqli_insert_id($cnx);
		
			$per = $_REQUEST["persona"];
			foreach($per as $persona){
				$sql = "insert into personas_bodegas (bod_id,per_id) values ('$_POST[id]','$persona')";
				$res = mysqli_query($cnx,$sql);
			}
			?>
			<div class="alert alert-success">  
                <span class="glyphicon glyphicon-ok-sign" aria-hidden="true" style="font-size:24px; float:left; margin-right:10px;"></span>
				<strong>Lugar '<? echo $_POST["nombre"]; ?>' creada con &eacute;xito</strong>
			</div>
			<?php   
		}
		break;
		
	case "editar": 
		$SQL_ = "UPDATE bodegas SET 
			bod_nombre = '$_POST[nombre]' 
		WHERE bod_id = '$_POST[id]' ";   
		$res = mysqli_query($cnx,$SQL_);  
		
		$sql = "delete from personas_bodegas where bod_id = '$_POST[id]' ";
		$res = mysqli_query($cnx,$sql);
		
		$per = $_REQUEST["persona"];
		foreach($per as $persona){
			$sql = "insert into personas_bodegas (bod_id,per_id) values ('$_POST[id]','$persona')";
			$res = mysqli_query($cnx,$sql);
		}
		?>   
        <div class="alert alert-success"> 
            Lugar <strong>'<? echo $_POST["nombre"]; ?>'</strong> editado con &eacute;xito
        </div>  
		<?php  
		break;
}

construir_boton("bod_listado.php","","buscar","Listado de lugares",2);
if($_REQUEST["modo"] <> "eliminar"){
	construir_boton("bod_editar.php","&id=".$_POST["id"],"editar","Editar lugar",2);
}
construir_boton("bod_nuevo.php","","crear","Crear otro lugar",2);
?>