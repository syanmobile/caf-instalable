  
<table width="100%">
<tr valign="top">
	<td width="25%">
        <table class="table table-bordered table-info table-condensed"> 
		<tbody>
		<tr>
			<td> 
				<b>Código Activo:</b><br>
				<input type="text" name="filtro_codigo_activo" id="filtro_codigo_activo" value="<? 
				echo $_REQUEST["filtro_codigo_activo"]; ?>" placeholder="Usar % para búsquedas" class="campos w10">

				<b>Serie:</b><br>
				<input type="text" name="filtro_serie" id="filtro_serie" class="campos w10" value="<? 
				echo $_REQUEST["filtro_serie"]; ?>" placeholder="Usar % para búsquedas">

				<b>Etiqueta:</b><br>
				<input type="text" name="filtro_etiqueta" id="filtro_etiqueta" class="campos w10" value="<? 
				echo $_REQUEST["filtro_etiqueta"]; ?>" placeholder="Usar % para búsquedas">

				<b>Código Producto:</b><br>
				<input type="text" name="filtro_codigo" id="filtro_codigo" value="<? 
				echo $_REQUEST["filtro_codigo"]; ?>" placeholder="Usar % para búsquedas" class="campos w10">

				<b>Nombre Producto:</b><br>
				<input type="text" name="filtro_descripcion" id="filtro_descripcion" class="campos w10" value="<? 
				echo $_REQUEST["filtro_descripcion"]; ?>" placeholder="Usar % para búsquedas">
			</td>
		</tr>
		</tbody>
		</table>
	</td>
	<td style="padding-left: 10px;" width="35%">
		<table class="table table-bordered table-info table-condensed"> 
		<tbody>
		<tr>
			<td>
				<b>Clasificación:</b><br>
				<select name="filtro_categoria" id="filtro_categoria" class="campos" style="width: 100%;">
				<?
				if($_REQUEST["filtro_categoria"] <> ""){
					$res = sql_categorias("*"," and cat.cat_id = '".$_REQUEST["filtro_categoria"]."'"); 
		            if(mysqli_num_rows($res) > 0){
		                while($row = mysqli_fetch_array($res)){
		                    ?>
		                    <option value="<? echo $row["cat_id"]; ?>"><? 
		                    echo $row["cat_codigo"]." - ".$row["cat_nombre"]; 
		                    ?></option>
		                    <?
		                }
		            }
				}
				?>
				</select>

				<b>Ubicación:</b><br>
				<select name="filtro_ubicacion" id="filtro_ubicacion" class="campos" style="width: 100%;">
				<?
				if($_REQUEST["filtro_ubicacion"] <> ""){
					$res = sql_ubicaciones("*"," and ubi.ubi_id = '".$_REQUEST["filtro_ubicacion"]."'"); 
		            if(mysqli_num_rows($res) > 0){
		                while($row = mysqli_fetch_array($res)){
		                    ?>
		                    <option value="<? echo $row["ubi_id"]; ?>"><? 
		                    echo $row["ubi_codigo"]." - ".$row["ubi_nombre"]; 
		                    ?></option>
		                    <?
		                }
		            }
				}
				?>
				</select>

				<b>Responsable:</b><br>
				<select name="filtro_responsable" id="filtro_responsable" class="campos" style="width: 100%;">
				<?php
				if($_REQUEST["filtro_responsable"] <> ""){
		            $res = sql_personas("*"," and per.per_id = '".$_REQUEST["filtro_responsable"]."'");  
		            if(mysqli_num_rows($res) > 0){
		                while($row = mysqli_fetch_array($res)){
		                    ?>
		                    <option value="<? echo $row["per_id"]; ?>"><?php 
		                    echo $row["per_nombre"]; 
		                    ?></option>
		                    <?
		                }
		            }
	            }
	            ?>  
				</select>

				<b>Centro Costo:</b><br>
			    <select name="filtro_cco" id="filtro_cco" class="campos" style="width: 100%;"> 
			    <?php
			    if($_REQUEST["filtro_cco"] <> ""){
		            $res = sql_centro_costo("*"," and cco.cco_id = '".$_REQUEST["filtro_cco"]."'");  
		            if(mysqli_num_rows($res) > 0){
		                while($row = mysqli_fetch_array($res)){
		                    ?><option value="<?php echo $row["cco_id"]; ?>"><?php 
		                    echo $row["cco_codigo"]; ?> - <?php echo $row["cco_nombre"]; ?></option>
		                    <?
		                }
		            }
	            } 
			    ?> 
			    </select>

				<b>Tipo del Activo:</b><br>
				<select name="filtro_tipo" id="filtro_tipo" class="campos" style="width: 100%;">
				<option value="">- Todos -</option>
				<?php
				foreach($_tipo_activos as $tp){
					if(in_array("act_".strtolower($tp)."_ver",$opciones_persona)){
						?><option value="<? echo $tp; ?>" <? if($_REQUEST["filtro_tipo"] == $tp){ echo "selected"; } ?>><?
						echo $_tipo_activo[$tp]; ?></option>
						<?
					}
				}
				?>
				</select>
			</td>
		</tr>
		</tbody>
		</table> 
	</td>
	<td style="padding-left: 10px;">
		<table class="table table-bordered table-info table-condensed"> 
		<tbody>
		<tr>
			<td>
				<b>Proveedor:</b><br> 
				<select name="filtro_auxiliar" id="filtro_auxiliar" class="campos" style="width: 100%;">
				<?
				if($_REQUEST["filtro_auxiliar"] <> ""){
					$res = sql_auxiliares("*"," and aux.aux_id = '".$_REQUEST["filtro_auxiliar"]."'"); 
		            if(mysqli_num_rows($res) > 0){
		                while($row = mysqli_fetch_array($res)){
		                    ?>
		                    <option value="<? echo $row["aux_id"]; ?>"><? 
		                    echo $row["aux_codigo"]." - ".$row["aux_nombre"]; 
		                    ?></option>
		                    <?
		                }
		            }
				}
				?>
				</select>

				<b>Lugar:</b><br>
				<select name="filtro_lugar" id="filtro_lugar" class="campos" style="width: 100%;"> 
			    <?php
			    if($_REQUEST["filtro_lugar"] <> ""){
		            $res = sql_bodegas("*"," and bod.bod_id = '".$_REQUEST["filtro_lugar"]."'");  
		            if(mysqli_num_rows($res) > 0){
		                while($row = mysqli_fetch_array($res)){
		                    ?><option value="<?php echo $row["bod_id"]; ?>"><?php 
		                    echo $row["bod_codigo"]; ?> - <?php echo $row["bod_nombre"]; ?></option>
		                    <?
		                }
		            }
	            } 
			    ?>
				</select>

				<b>Nro.Documento:</b><br>
				<input type="text" name="filtro_doc_ingreso" id="filtro_doc_ingreso" class="campos w10" value="<? echo $_REQUEST["filtro_doc_ingreso"]; ?>">

				<b>Fecha Compra:</b><br>
				<input type="text" name="filtro_compra_inicio" id="filtro_compra_inicio" class="campos fecha" value="<? echo $_REQUEST["filtro_compra_inicio"]; ?>" style="width: 70px; float: left;" placeholder="Desde">
				<input type="text" name="filtro_compra_termino" id="filtro_compra_termino" class="campos fecha" value="<? echo $_REQUEST["filtro_compra_termino"]; ?>" style="width: 70px; float: left;" placeholder="Hasta"><br><br>

				<b>Estado del Activo:</b><br>
				<select name="filtro_estado" id="filtro_estado" class="campos w10">
					<option value="">Todos</option>  
					<option value="0" <? 
					if($_REQUEST["filtro_estado"] == "0"){ echo "selected"; } ?>>No Disponible</option>
					<option value="1" <? 
					if($_REQUEST["filtro_estado"] == "1"){ echo "selected"; } ?>>Disponible</option>
					<option value="2" <? 
					if($_REQUEST["filtro_estado"] == "2"){ echo "selected"; } ?>>Prestado</option>
					<option value="3" <? 
					if($_REQUEST["filtro_estado"] == "3"){ echo "selected"; } ?>>En Mantenimiento</option>
					<option value="4" <? 
					if($_REQUEST["filtro_estado"] == "4"){ echo "selected"; } ?>>De Baja</option>
					<option value="5" <? 
					if($_REQUEST["filtro_estado"] == "5"){ echo "selected"; } ?>>Req.Mantenimiento</option>
					<option value="6" <? 
					if($_REQUEST["filtro_estado"] == "6"){ echo "selected"; } ?>>Asignado</option>
				</select>
			</td>
		</tr>
		</tbody>
		</table>
	</td>
	<td width="1" style="padding-left: 10px;"> 
		<?  
		construir_boton($pagina,"","buscar","Filtrar",2);
		construir_boton($pagina,"","limpiar","Limpiar",8); 
		construir_boton("acf_importar.php","","importar","Importar",2);  
		?>
	</td>
</tr>
</table> 

<script type="text/javascript"> 
$("#filtro_auxiliar").select2({        
    ajax: {
        url: "mods/home/filtros_ajax.php?modo=auxiliares",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term // search term
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data, function(obj) {
                    return { id: obj.id, text: obj.text };
                })
            };
        },
        cache: true
    },
    minimumInputLength: 3 
}); 
$("#filtro_categoria").select2({        
    ajax: {
        url: "mods/home/filtros_ajax.php?modo=categorias",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term // search term
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data, function(obj) {
                    return { id: obj.id, text: obj.text };
                })
            };
        },
        cache: true
    },
    minimumInputLength: 3 
});
$("#filtro_ubicacion").select2({        
    ajax: {
        url: "mods/home/filtros_ajax.php?modo=ubicaciones",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term // search term
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data, function(obj) {
                    return { id: obj.id, text: obj.text };
                })
            };
        },
        cache: true
    },
    minimumInputLength: 3 
});
$("#filtro_responsable").select2({        
    ajax: {
        url: "mods/home/filtros_ajax.php?modo=responsables",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term // search term
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data, function(obj) {
                    return { id: obj.id, text: obj.text };
                })
            };
        },
        cache: true
    },
    minimumInputLength: 3 
});
$("#filtro_cco").select2({        
    ajax: {
        url: "mods/home/filtros_ajax.php?modo=cco",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term // search term
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data, function(obj) {
                    return { id: obj.id, text: obj.text };
                })
            };
        },
        cache: true
    },
    minimumInputLength: 3 
});
$("#filtro_lugar").select2({        
    ajax: {
        url: "mods/home/filtros_ajax.php?modo=lugares",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term // search term
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data, function(obj) {
                    return { id: obj.id, text: obj.text };
                })
            };
        },
        cache: true
    },
    minimumInputLength: 3 
});
</script> 

<?
/****************************************************************************************/
if($_REQUEST["filtro_codigo"] <> ""){
	$filtros .= " and pro.pro_codigo like '".$_REQUEST["filtro_codigo"]."' ";
	$fil_texto .= "<b>Cod.Producto:</b> ".$_REQUEST["filtro_codigo"];
}
if($_REQUEST["filtro_codigo_activo"] <> ""){
	$filtros .= " and acf.acf_codigo like '".$_REQUEST["filtro_codigo_activo"]."' ";
	if($fil_texto <> ""){ $fil_texto.= ", "; }
	$fil_texto .= "<b>Cod.Activo:</b> ".$_REQUEST["filtro_codigo_activo"];
}
if($_REQUEST["filtro_serie"] <> ""){
	$filtros .= " and acf.acf_serie like '".$_REQUEST["filtro_serie"]."' ";
	if($fil_texto <> ""){ $fil_texto.= ", "; }
	$fil_texto .= "<b>Serie:</b> ".$_REQUEST["filtro_serie"];
}
if($_REQUEST["filtro_etiqueta"] <> ""){
	$filtros .= " and acf.acf_etiqueta like '".$_REQUEST["filtro_etiqueta"]."' ";
	if($fil_texto <> ""){ $fil_texto.= ", "; }
	$fil_texto .= "<b>Etiqueta:</b> ".$_REQUEST["filtro_etiqueta"];
}
if($_REQUEST["filtro_descripcion"] <> ""){
	$filtros .= " and pro.pro_nombre like '".$_REQUEST["filtro_descripcion"]."' ";
	if($fil_texto <> ""){ $fil_texto.= ", "; }
	$fil_texto .= "<b>Nombre Producto:</b> ".$_REQUEST["filtro_descripcion"];
} 
if($_REQUEST["filtro_unidad"] <> ""){
	$filtros .= " and pro.pro_unidad = '".$_REQUEST["filtro_unidad"]."' ";
	if($fil_texto <> ""){ $fil_texto.= ", "; }
	$fil_texto .= "<b>Unidad:</b> ".$_REQUEST["filtro_unidad"];
}  
if($_REQUEST["filtro_categoria"] <> ""){
	$filtros .= " and pro.pro_categoria = '".$_REQUEST["filtro_categoria"]."' ";
	if($fil_texto <> ""){ $fil_texto.= ", "; }
	$fil_texto .= "<b>Categoría:</b> ".$_REQUEST["filtro_categoria"];
}
if($_REQUEST["filtro_cco"] <> ""){
	$filtros .= " and acf.acf_cco_id = '".$_REQUEST["filtro_cco"]."' ";
	if($fil_texto <> ""){ $fil_texto.= ", "; }
	$fil_texto .= "<b>Centro Costo:</b> ".$_REQUEST["filtro_cco"];
}
if($_REQUEST["filtro_ubicacion"] <> ""){
	$filtros .= " and acf.acf_ubicacion = '".$_REQUEST["filtro_ubicacion"]."' ";
	if($fil_texto <> ""){ $fil_texto.= ", "; }
	$fil_texto .= "<b>Ubicación:</b> ".$_REQUEST["filtro_ubicacion"];
} 
if($_REQUEST["filtro_lugar"] <> ""){
	$filtros .= " and ubi.ubi_bodega = '".$_REQUEST["filtro_lugar"]."' ";
	if($fil_texto <> ""){ $fil_texto.= ", "; }
	$fil_texto .= "<b>Lugar:</b> ".$_REQUEST["filtro_lugar"];
} 
if($_REQUEST["filtro_tipo"] <> ""){
	$filtros .= " and pro.pro_tipo = '".$_REQUEST["filtro_tipo"]."' ";
	if($fil_texto <> ""){ $fil_texto.= ", "; }
	$fil_texto .= "<b>Tipo:</b> ".$_REQUEST["filtro_tipo"];
}    
if($_REQUEST["filtro_compra_inicio"] <> ""){
	$filtros .= " and acf.acf_fecha_ingreso >= '"._fec($_REQUEST["filtro_compra_inicio"],9)."' ";
	if($fil_texto <> ""){ $fil_texto.= ", "; }
	$fil_texto .= "<b>Fecha Compra(Desde):</b> ".$_REQUEST["filtro_compra_inicio"];
} 
if($_REQUEST["filtro_compra_termino"] <> ""){
	$filtros .= " and acf.acf_fecha_ingreso <= '"._fec($_REQUEST["filtro_compra_termino"],9)."' ";
	if($fil_texto <> ""){ $fil_texto.= ", "; }
	$fil_texto .= "<b>Fecha Compra(Hasta):</b> ".$_REQUEST["filtro_compra_termino"];
} 
if($_REQUEST["filtro_responsable"] <> ""){
	$filtros .= " and acf.acf_responsable = '".$_REQUEST["filtro_responsable"]."' ";
	if($fil_texto <> ""){ $fil_texto.= ", "; }
	$fil_texto .= "<b>Responsable:</b> ".$_REQUEST["filtro_responsable"];
} 
if($_REQUEST["filtro_estado"] <> ""){
	$filtros .= " and acf.acf_disponible = '".$_REQUEST["filtro_estado"]."' ";
	if($fil_texto <> ""){ $fil_texto.= ", "; }
	$fil_texto .= "<b>Estado:</b> ".$_REQUEST["filtro_estado"];
}
if($_REQUEST["filtro_auxiliar"] <> ""){
	$filtros .= " and acf.acf_proveedor = '".$_REQUEST["filtro_auxiliar"]."' ";
	if($fil_texto <> ""){ $fil_texto.= ", "; }
	$fil_texto .= "<b>Proveedor:</b> ".$_REQUEST["filtro_auxiliar"];
}
if($_REQUEST["filtro_doc_ingreso"] <> ""){
	$filtros .= " and acf.acf_nro_factura = '".$_REQUEST["filtro_doc_ingreso"]."' ";
	if($fil_texto <> ""){ $fil_texto.= ", "; }
	$fil_texto .= "<b>Nro.Documento:</b> ".$_REQUEST["filtro_doc_ingreso"];
}  
?>