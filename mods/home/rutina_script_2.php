<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------

$res = mysqli_query($cnx,"select * from activos_tipos ");  
if(mysqli_num_rows($res) > 0){
	while($datos = mysqli_fetch_array($res)){
		$vida_util[$datos["tac_codigo"]] = $datos["tac_vu_dep_lineal"] * 12;
	}
}

$res = sql_activos_fijos("*"," order by acf_id asc");  
if(mysqli_num_rows($res) > 0){ 
	while($datos = mysqli_fetch_array($res)){

		$fecha_inicio = $datos["acf_fecha_ingreso"];

		//sumo x meses 
		$meses = $vida_util[$datos["pro_tipo"]];
		$fecha_termino = date("Y-m-d",strtotime($fecha_inicio."+ ".($meses- 1)." month")); 
		
		switch($datos["acf_tipo_depreciacion"]){
			case "L": $meses_divisor = $meses; break;
			case "A": $meses_divisor = round($meses / 3); break;
		}
		
		$valor_activo = $datos["acf_valor"] * 1;
		$residual = $datos["acf_valor_residual"] * 1;
		$valor = $valor_activo - $residual; 

		$valor_mensual = round(($valor / $meses_divisor),2);

		$sql3 = "update activos_fijos set
		acf_vida_util = '$meses',
		acf_mes_ano_inicio_revalorizacion = '$fecha_inicio',
		acf_mes_ano_inicio_depreciacion = '$fecha_inicio',
		acf_mes_ano_termino_depreciacion = '$fecha_termino',
		acf_valor_a_depreciar = '$valor',
		acf_cuota_depreciacion = '$valor_mensual' 
		where acf_id = '$datos[acf_id]' ";
		echo $sql3."<br>";
		$res3 = mysqli_query($cnx,$sql3);
	}
}
?>