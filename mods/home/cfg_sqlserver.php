<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Test SQL Server";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?> 
<script language="javascript">
function test(){ 
	var div = document.getElementById("testing"); 
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/cfg_sqlserver_ajax.php",a+"&modo=test",div);
}
function test2(extra){ 
	var div = document.getElementById("resultados"+extra);  
	var a = $(".campos").fieldSerialize();  
	AJAXPOST("mods/home/cfg_sqlserver_ajax.php",a+"&modo=query&ext="+extra,div);
} 
</script>

<table class="table table-striped table-bordered table-condensed">
<tr>
	<th width="130">Conexión SQL Server:</th>
    <td width="300"><input type="text" name="coneccion" value="<?php echo _opc("sql_db"); ?>" class="campos"></td>
    <td width="150"> 
	<?php
    construir_boton("test()","","derecha","Testear Conexión",4);
    ?></td> 
    <td id="testing"></td>
</tr>
</table>   

<table class="table table-striped table-bordered table-condensed">
<tr>
	<th width="130">Query SQL Server:</th>
    <td width="1"> 
	<textarea name="query1" class="campos" rows="9" style="width: 300px;"></textarea>
    </td>
    <td><?php
    construir_boton("test2(1)","1","derecha","Probar Query",4);
    ?></td><td>
    <div id="resultados1" style="overflow: scroll; height: 220px; width: 100%;"></div></td>
</tr>
<tr>
	<th width="130">Query SQL Server:</th>
    <td> 
	<textarea name="query2" class="campos" rows="9" style="width: 300px;"></textarea>
    </td>
    <td><?php
    construir_boton("test2(2)","1","derecha","Probar Query",4);
    ?></td><td>
    <div id="resultados2" style="overflow: scroll; height: 220px; width: 100%;"></div></td>
</tr>
<tr>
	<th width="130">Query SQL Server:</th>
    <td> 
	<textarea name="query3" class="campos" rows="9" style="width: 300px;"></textarea>
    </td>
    <td width="1"><?php
    construir_boton("test2(3)","1","derecha","Probar Query",4);
	?></td><td>
    <div id="resultados3" style="overflow: scroll; height: 220px; width: 100%;"></div></td>
</tr>
</table>