<?php
require("../../inc/conf_dentro.php");

$tipo_dep = _opc("tipo_depreciacion");

$res = sql_activos_fijos_rapido("*"," LIMIT $_REQUEST[inicio],$_REQUEST[cantidad]"); 
if(mysqli_num_rows($res) > 0){
	?>
	<table border="1"> 
	<thead>
		<tr>  
			<th width="1">ID</th>
			<th>Código</th>
			<th>Fecha Dep</th>
			<th>CodProd</th>
			<th>Producto</th>
            <th>Vida Util</th>
            <th>Valor Compra</th>
			<th>Cuota Dep</th>
		</tr>
	</thead>
	<tbody> 
	<?
	while($row = mysqli_fetch_array($res)){ 
        if($tipo_dep == "L"){
            $vida_util_[$producto] = $_tipo_activos_lineal[$row["pro_tipo"]];
        }else{
            $vida_util_[$producto] = $_tipo_activos_acele[$row["pro_tipo"]];
        } 
        $vida_util = $vida_util_[$producto];
        $fecha_ter_dep = suma_a_fecha_meses($row["acf_mes_ano_inicio_depreciacion"],$vida_util);

        switch($tipo_dep){
            case "L": $meses_divisor = $vida_util; break;
            case "A": $meses_divisor = round($vida_util / 3); break;
        }
            
        $valor_a_depreciar = $row["acf_valor"] - $valor_residual; 

        $cuota_depreciacion = round(($valor_a_depreciar / $meses_divisor),2);
		?>
		<tr> 
            <td><?php echo $row["acf_id"]; ?></td>
            <td><?php echo $row["acf_codigo"]; ?></td>
            <td><?php echo $row["acf_mes_ano_inicio_depreciacion"]; ?></td>
            <td><?php echo $row["pro_codigo"]; ?></td>
			<td><?php echo $row["pro_nombre"]; ?></td>
			<td><?php echo $vida_util; ?></td>
            <td><?php echo _num($row["acf_valor"],$_configuraciones["cfg_moneda_decimal"]); ?></td>  
			<td><?php echo _num($cuota_depreciacion); ?></td>
			<td><?
			$sql2 = "update activos_fijos set 
			acf_mes_ano_termino_depreciacion = '$fecha_ter_dep', 
			acf_cuota_depreciacion = '$cuota_depreciacion',
			acf_valor_a_depreciar = '$valor_a_depreciar' 
			where acf_id = '$row[acf_id]' ";
			$res2 = mysqli_query($cnx,$sql2);
			echo $sql2;
			?></td>
		</tr>
		<?  
	}
	?>
	</tbody>
	</table> 
	<?php 
} 
?>