<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Centros de Costo";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
$pagina = "cco_listado.php"; 
include("ayuda.php");
//---------------------------------------------------------------------------------------- 
?>
<table width="100%" class="tabla_opciones">
<tbody> 
    <tr>
        <td><?php
		construir_boton("cco_nuevo.php","","crear","Nuevo Centro de Costo",2);
		construir_boton("cco_importar.php","","importar","Importar Centros de Costo",2);
		?></td>
    </tr>
</tbody>
</table>

<table class="table table-bordered table-condensed table-info" style="margin-bottom:0px !important;">  
<tbody> 
<tr>
    <td width="100">
    <b>Código:</b><br />
    <input type="text" name="fil_codigo" id="fil_codigo" class="campos w10" value="<? echo $_REQUEST["fil_codigo"]; ?>" placeholder="Usar % para búsquedas"></td> 
    <td>
    <b>Nombre:</b><br />
    <input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? echo $_REQUEST["fil_descripcion"]; ?>" placeholder="Usar % para búsquedas"></td> 
    <td width="1"><?
	construir_boton("cco_listado.php","","buscar","Filtrar");
	?></td> 
</tr>
</tbody>
</table> 

<input type="hidden" name="pagina" id="pagina" value="cco_listado.php" class="campos">
<?php
if($_REQUEST["fil_codigo"] <> ""){
	$filtros .= " and cco.cco_codigo like '".$_REQUEST["fil_codigo"]."' ";
}
if($_REQUEST["fil_descripcion"] <> ""){
	$filtros .= " and cco.cco_nombre like '".$_REQUEST["fil_descripcion"]."' ";
}
$sql_paginador = sql_centro_costo("*"," $filtros ORDER BY cco_codigo asc","S");
$cant_paginador = 12;
require("../../_paginador.php");
$res = mysqli_query($cnx,$sql_final);
if(mysqli_num_rows($res) > 0){
    ?>
    <table class="table table-striped table-bordered table-condensed" id="tablita"> 
    <thead>
    <tr>
        <th width="1" align="center">&nbsp;</th>
        <th width="1" align="center">Código</th>
        <th>Centro de Costo</th>
    </tr>
    </thead>
    <tbody> 
    <?
    while($row = mysqli_fetch_array($res)){
        ?>
        <tr fila="<?php echo $row["cco_id"] ;?>" id="<?php echo $row["cco_id"] ;?>">  
            <td style="text-align:center;"><?php
			construir_boton("cco_editar.php","&id=".$row["cco_id"],"editar","Editar");
			?></td>
            <th style="text-align: center"><?php echo $row["cco_codigo"]; ?></th>
            <td><?php echo $row["cco_nombre"]; ?></td>
        </tr>
        <? 
    }
    ?> 
    </tbody>
    </table>  
    <?php
	echo $paginador_dibujo;
}else{
    ?>
    <div class="alert alert-danger">
        <strong>Sin registros creados</strong>
    </div>
    <?php
}
?>