<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Asociar Activos sin Etiquetas";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function asociar_etiqueta(acf){
	var div = document.getElementById("lugarcito");  
	AJAXPOST("mods/home/cdu_ajax2.php","modo=asociar&acf="+acf,div);
}
function crear_etiqueta(acf){
	AJAXPOST("mods/home/acf_ajax.php","modo=crear_etiqueta&acf="+acf,document.getElementById("modGeneral_lugar"),false,function(){            
        $('#modGeneral').modal('show');

    });
}
</script>

<table width="100%">
	<tr valign="top">
		<td> 

			<table class="table table-bordered table-condensed table-info" style="margin-bottom:0px !important;">  
			<tbody> 
			<tr>
			    <th width="1">Producto:</th>
			    <td><input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? echo $_REQUEST["fil_descripcion"]; ?>"></td>  
			    <td width="1"><?
				construir_boton("acf_sin_etiquetas.php","","buscar","Filtrar",2); 
				?></td> 
			</tr>
			</tbody>
			</table>

			<input type="hidden" name="pagina" id="pagina" value="acf_sin_etiquetas.php" class="campos">
			<?
			$res = mysqli_query($cnx,"select acf_id from activos_fijos");
			$total = mysqli_num_rows($res); 

			if($_REQUEST["fil_descripcion"] <> ""){
				$extras .= "and pro.pro_nombre like '%".$_REQUEST["fil_descripcion"]."%' ";
			}
			$sql_paginador = sql_activos_fijos("*"," and acf_etiqueta = '' $extras ORDER BY acf.acf_producto asc, acf.acf_serie asc","s");
			$cant_paginador = 20;
			require("../../_paginador.php");


			$listas = $total - $num_paginador; 
			$porc = _num(_porc($listas,$total),1);
			?>
			<div class="progress" style="margin-bottom: 4px;">
			  <div class="progress-bar" role="progressbar" aria-valuenow="<? echo $porc; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <? echo ($porc * 1); ?>%;">
			    Activos etiquetados: <? echo $listas."/".$total; ?> (<b><? echo $porc; ?>%</b>)
			  </div>
			</div>
			<b>PENDIENTES: <? echo $num_paginador; ?> activos sin etiquetas</b>
			<?
			$res = mysqli_query($cnx,$sql_final); 
			if(mysqli_num_rows($res) > 0){
				?>
				<table class="table table-striped table-bordered tabledrag table-condensed"> 
				<thead>
					<tr>  
						<th width="1"></th> 

						<th>Códigos</th>
						<th>Producto</th>
						 
						<th>Ubicación</th>

						<th>Clasificación</th>   
					</tr>
				</thead>
				<tbody> 
				<?
				while($row = mysqli_fetch_array($res)){ 
					?>
					<tr> 
						<td><div id="label<? echo $row["acf_id"]; ?>"><? construir_boton("crear_etiqueta(".$row["acf_id"].")","","barcode","Etiquetar",4); ?></div></td>

						<td>
							<b>Cod.Activo:</b> <?php echo $row["acf_codigo"]; ?><br>
							<b>Serie:</b> <?php echo $row["acf_serie"]; ?>
						</td>
						<td>
							<?php echo $row["pro_nombre"]; ?><br>
							<b><?php echo $row["pro_codigo"]; ?></b>
						</td> 	
						<td><?php 
						if($row["acf_ubicacion"] <> 0){
							echo $_ubicaciones[$row["acf_ubicacion"]]; ?><br>
							<b><?php echo $_ubicaciones_codigo[$row["acf_ubicacion"]]; ?></b><?
						}
						?></td>   
						<td>
							<?php echo $_categorias[$row["pro_categoria"]]; ?><br>
							<b><?php echo $_categorias_codigo[$row["pro_categoria"]]; ?></b>
						</td>  
					</tr>
					<?  
				}
				?>
				</tbody>
				</table> 
				<?
				echo $paginador_dibujo;
			}else{
				?>
				<div class="alert alert-success">
					<strong>PERFECTO!!!</strong>
					Tienes todos tus activos fijos con etiquetas
				</div>
				<?php 
			} 
			?>
			</td>
		<td width="50%" style="padding-left: 10px;">
			<div id="lugarcito"></div>
		</td>
	</tr>
</table>