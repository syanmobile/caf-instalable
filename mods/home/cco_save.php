<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Grabación";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------

switch($_POST["modo"]){
	case "crear": 
		$sql = "INSERT INTO centro_costo ( 
			cco_codigo,
			cco_nombre 
		) VALUES ( 
			'$_POST[codigo]',
			'$_POST[nombre]' 
		)";
		$res = mysqli_query($cnx,$sql);
		$_POST["id"] = mysqli_insert_id($cnx);
		?>
		<div class="alert alert-success"> 
		
			<strong>Centro de Costo '<? echo $_POST["nombre"]; ?>' creado con &eacute;xito</strong>
		</div>
		<?php  
		break;
		
	case "editar":
		$SQL_ = "UPDATE centro_costo SET 
			cco_codigo = '$_POST[codigo]', 
			cco_nombre = '$_POST[nombre]' 
		WHERE cco_id = '$_POST[id]' ";   
		$res = mysqli_query($cnx,$SQL_);
		?>   
        <div class="alert alert-success"> 
           Centro de Costo <strong>'<? echo $_POST["nombre"]; ?>'</strong> editado con &eacute;xito
        </div>  
		<?php  
		break;
}

construir_boton("cco_listado.php","","buscar","Listado Centro de Costo",2);
if($_REQUEST["modo"] <> "eliminar"){
	construir_boton("cco_editar.php","&id=".$_POST["id"],"editar","Editar este Centro de Costo",2);
}
construir_boton("cco_nuevo.php","","crear","Crear otro Centro de Costo",2);
?>