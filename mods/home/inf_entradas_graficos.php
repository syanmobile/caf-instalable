<?php
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Informe Consolidado Entradas";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script language="javascript">
function excel(){
	var a = $(".campos").fieldSerialize();
	window.open("mods/home/inf_entradas_graficos_excel.php?"+a);
}
function excel2(){
	var a = $(".campos").fieldSerialize();
	window.open("mods/home/inf_entradas_graficos_excel2.php?"+a);
}
</script>
<table class="table table-bordered table-condensed"> 
<tr>    
    <td width="1"><b>Desde:</b><br>
    <input type="text" class="campos fecha" id="desde" name="desde" value="<?php
    if($_REQUEST["desde"] == ""){  
        $_REQUEST["desde"] = "01/".date('m/Y');
    }
    echo $_REQUEST["desde"]; ?>" size="10"></td>
    
    <td width="1"><b>Hasta:</b><br>
    <input type="text" class="campos fecha" id="hasta" name="hasta" value="<?php
    if($_REQUEST["hasta"] == ""){
        $_REQUEST["hasta"] = date("d/m/Y");
    }
    echo $_REQUEST["hasta"]; ?>" size="10"></td>
    
    <td><b>Lugar:</b><br>
    <?php input_bodega($_REQUEST["bodega"]); ?></td> 
    
    <td><b>Concepto:</b><br>
	<?php input_concepto($_REQUEST["concepto"],'E'); ?></td>
   
    <td><b>Producto:</b><br>
    <input type="text" name="fil_descripcion" id="fil_descripcion" class="campos w10" value="<? echo $_REQUEST["fil_descripcion"]; ?>"></td>
    
        <?
	/*
    <td>
    <b>Grupo:</b><br />
    <select name="fil_grupo" id="fil_grupo" class="campos buscador w10">
    <option value=""></option>
    <?php
    $res = sql_grupos("*"," order by gru.gru_nombre asc"); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["gru_codigo"]; ?>" <?
            if($_REQUEST["fil_grupo"] == $row["gru_codigo"]){ echo "selected"; } 
            ?>><? echo $row["gru_codigo"]." - ".$row["gru_nombre"]; ?></option>
            <?
        }
    }
    ?></select></td> 
    <td>
    <b>Subgrupo:</b><br />
    <select name="fil_subgrupo" id="fil_subgrupo" class="campos buscador w10">
    <option value=""></option>
    <?php
    $res = sql_subgrupos("*"," order by sgru.sgru_nombre asc"); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["sgru_codigo"]; ?>" <?
            if($_REQUEST["fil_subgrupo"] == $row["sgru_codigo"]){ echo "selected"; } 
            ?>><? echo $row["sgru_codigo"]." - ".$row["sgru_nombre"]; ?></option>
            <?
        }
    }
    ?></select></td> 
    <td>
    <b>Categorias:</b><br />
    <select name="fil_categoria" id="fil_categoria" class="campos buscador w10">
    <option value=""></option>
    <?php
    $res = sql_categorias("*"," order by cat.cat_nombre asc"); 
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            ?>
            <option value="<? echo $row["cat_codigo"]; ?>" <?
            if($_REQUEST["fil_categoria"] == $row["cat_codigo"]){ echo "selected"; } 
            ?>><? echo $row["cat_codigo"]." - ".$row["cat_nombre"]; ?></option>
            <?
        }
    }
    ?></select></td>
	*/
	?>
     
    <td width="120">
    <b>Tipo:</b><br />
    <select name="fil_tipo" id="fil_tipo" class="campos w10">
    <option value=""></option>
    <?php
    foreach($_tipo_activos as $tp){
        if(_opc("activo_".$tp) == "1"){
            ?><option value="<? echo $tp; ?>" <? if($_REQUEST["fil_tipo"] == $tp){ echo "selected"; } ?>><?
            echo $_tipo_activo[$tp]; ?></option>
            <?
        }
    }
    ?> 
    </select></td>
         
    <td width="1"><?php
    construir_boton("inf_entradas_graficos.php","","buscar","Buscar",2);
    ?></td>  
</tr>
</table>
<input type="hidden" name="pagina" value="inf_entradas_graficos.php" class="campos">
<?php
if($_REQUEST["pagina"] <> ""){
	include("inf_entradas_graficos_js.php");
}else{
	?>
	<div class="alert alert-info">Presione Buscar para mostrar la información</div>
	<?
}
?>