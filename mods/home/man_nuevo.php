<?php 
require("../../inc/conf_dentro.php");
//----------------------------------------------------------------------------------------
$titulo_pagina = "Nueva Mantención";
construir_breadcrumb($titulo_pagina);
//----------------------------------------------------------------------------------------
?>
<script type="text/javascript">
function save(){
	if(document.getElementById("activo").value == ""){
		alerta_js("Es obligación seleccionar un ACTIVO");
		return;	
	} 
	if(document.getElementById("titulo").value == ""){
		alerta_js("Es obligación ingresar el TITULO");
		return;	
	} 
	if(confirm("Grabar Mantención?")){
		carg("man_ajax.php","");
	}
} 
</script> 

<table class="table table-bordered table-condensed"> 
<thead>
<tr>
	<th colspan="2">Mantención a Activo</th>
</tr>
</thead> 
<tbody> 
<tr>
	<th width="110">Activo Fijo:</th>
	<td>
	<select name="activo" id="activo" class="campos buscador">
	<option value=""></option>
	<? 
	$res = sql_activos_fijos("*"," ORDER BY acf.acf_producto asc, acf.acf_serie asc ");  
	if(mysqli_num_rows($res) > 0){
		while($row = mysqli_fetch_array($res)){
			?>
			<option value="<? echo $row["acf_id"]; ?>" <? if($row["acf_id"] == $_REQUEST["acf"]){ echo "selected"; } ?>><?php echo $row["acf_codigo"]." - ".$row["pro_nombre"]." | Cod.Prod: ".$row["pro_codigo"]." | Serie: ".$row["acf_serie"]; ?></option>
			<?
		}
	} 
	?>
	</select>
	</td>
</tr>
</tbody>
</table>

<input type="hidden" name="modo" value="crear" class="campos">
<table width="100%" class="tabla_opciones">
<tbody> 
	<tr valign="top">
		<td width="40%">
			<table class="table table-bordered table-condensed"> 
			<thead>
			<tr>
				<th colspan="4">Detalles de la Mantención</th>
			</tr>
			</thead> 
			<tbody>
			<tr>
				<th width="110">Folio:</th>
				<td><input type="text" disabled value="Autom." style="text-align: center;"></td>
			</tr> 
			<tr>
				<th>Titulo:</th>
				<td><input type="text" name="titulo" id="titulo" class="campos w10"></td>
			</tr>
			<tr>
				<th>Descripción:</th>
				<td><textarea name="descripcion" id="descripcion" class="campos w10" rows="6"></textarea></td>
			</tr> 
			<tr>
				<th>Notas:</th>
				<td><textarea name="notas" id="notas" class="campos w10" rows="4"></textarea></td>
			</tr>  
			<tr>
				<th>Responsable:</th>
				<td>
				<select name="responsable" id="responsable" class="campos buscador">
				<option value=""></option>
				<? 
				$res = sql_personas("*","  and per.per_tipo = 1 and per.per_elim = 0  ORDER BY per.per_nombre asc ");  
				if(mysqli_num_rows($res) > 0){
					while($row = mysqli_fetch_array($res)){
						?>
						<option value="<? echo $row["per_id"]; ?>"><?php echo $row["per_nombre"]; ?></option>
						<?
					}
				} 
				?>
				</select>
				</td>
			</tr>   
			</tbody>
			</table>
		</td>
		<td width="380" style="padding-left: 15px;">
			<table class="table table-bordered table-condensed"> 
			<thead>
			<tr>
				<th colspan="2">Información</th>
			</tr>
			</thead> 
			<tbody>
			<tr>
				<th width="120">Inicio:</th>
				<td><input type="text" name="inicio1" id="inicio1" class="fecha campos" style="float: left;"> 
				<input type="text" name="inicio2" id="inicio2" class="hora campos" style="float: left; padding-left: 15px; width: 50px; text-align: center;"></td> 
			</tr>
			<tr>
				<th>Término:</th>
				<td><input type="text" name="termino1" id="termino1" class="fecha campos" style="float: left;"> 
				<input type="text" name="termino2" id="termino2" class="hora campos" style="float: left; padding-left: 15px; width: 50px; text-align: center;"></td>
			</tr>
			<tr>
				<th>Fecha Límite:</th>
				<td colspan="3"><input type="text" name="limite" id="limite" class="fecha campos" style="float: left;"></td>
			</tr>
			<tr>
				<th>Tipo:</th>
				<td><select name="tipo" id="tipo" class="campos">
				<option value="P">Preventiva</option>
				<option value="C">Correctiva</option>
				</select></td>
			</tr>
			<tr>
				<th>Prioridad:</th>
				<td><select name="prioridad" id="prioridad" class="campos">
				<option value="1">Urgente–Importante</option>
				<option value="2">Urgente–No importante</option>
				<option value="3">No urgente–Importante</option>
				<option value="4" selected>No urgente–No importante</option> 
				</select></td>
			</tr>
			<tr> 
				<th>Costo:</th>
				<td><input type="text" name="costo" id="costo" class="campos w4"></td>
			</tr>
			<tr>
				<th>Horas Hombre:</th>
				<td><input type="text" name="hh" id="hh" class="campos w4"></td>
			</tr>
			<tr>
				<th>Estado:</th>
				<td><select name="estado" id="estado" class="campos">
				<option value="PND">Pendiente</option>
				<option value="TER">Terminado</option>
				</select></td>
			</tr>
			</tbody>
			</table> 

			<table class="table table-bordered table-condensed"> 
			<thead>
			<tr>
				<th colspan="2">Próxima Mantención</th>
			</tr>
			</thead> 
			<tbody>
			<tr>
				<th width="120">Fecha:</th>
				<td><input type="text" name="proxima" id="proxima" class="fecha campos" style="float: left;"></td>
			</tr>
			</tbody>
			</table>
		</td>
		<td style="padding-left: 15px;">
			<table class="table table-bordered table-condensed"> 
			<thead>
			<tr>
				<th colspan="2">Información Adicional</th>
			</tr>
			</thead> 
			<tbody> 
			<?php 
			$extras = unserialize($_configuracion["mantenimientos_extras"]);
			for($i = 1;$i < count($extras);$i++){
				if($extras["Extra_".$i] <> ""){
					?>
					<tr>
						<th width="100"><?php echo $extras["Extra_".$i]; ?>:</th>
						<td><input type="text" name="extra_<? echo $i; ?>" id="extra_<? echo $i; ?>" class="campos w10" value="<? echo $row3["man_extra_".$i]; ?>"></td>
					</tr>
					<?php
				}
			} 
			?>  
			<tr>
				<th>Evidencias:</th>
				<td> 
					<?
					$imagen = ($row3["man_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$row3["man_imagen"];
					?>
					<img src="<?php echo $imagen; ?>" id="imagen_ruta" class="img-thumbnail" style="width:250px !important;"> 
					<form method="post" name="multiple_upload_form" id="multiple_upload_form" enctype="multipart/form-data" action="<?php echo $url_web; ?>mods/home/acf_ajax_man.php"> 
						<input type="file" name="archivo" id="archivo" accept='image/*'>

						<div class="uploading none" style="display:none;"><img src="img/uploading.gif"/></div>
						<div id="eliminando"></div> 
						<div id="procesando_upload">
							<input type="hidden" name="imagen" value="<?php echo $row3["man_imagen"]; ?>" class="campos" />
						</div>
					</form> 
				</td>
			</tr>
			</tbody>
			</table> 
		</td>
	</tr>
</tbody>
</table>
<?php 
construir_boton("save()","","grabar","Guardar Mantenimiento",4);
construir_boton("man_listado.php","","eliminar","Cancelar",2);
?>

<script language="javascript">
$(document).ready(function(){
	$(".buscador").chosen({
		width:"95%",
		no_results_text:'Sin resultados con',
		allow_single_deselect:true 
	});
	$(".fecha").datepicker();
	$(".hora").mask("99:99");  
	
	$('#archivo').on('change',function(){ 
		$('#multiple_upload_form').ajaxForm({
			target:'#procesando_upload',
			data: { modo: "upload" },
			beforeSubmit:function(e){
				$('.uploading').show();
			},
			success:function(e){
				$('.uploading').hide();
				$("#imagen_ruta").attr("src","upload/<?php echo $_SESSION["key_id"]; ?>/"+document.getElementById("imagen").value); 
			},
			error:function(e){
			}
		}).submit(); 
	});
});
</script>