<?php
require("../../inc/conf_dentro.php");
?>   
<div style="padding:2px;">
	<h3>Activos Fijos</h3>
	<table class="table table-bordered table-info table-condensed"> 
	<tbody>
	<tr>
		<td>
			<b>Clasificación:</b><br>
			<select name="filtro_categoria" id="filtro_categoria" class="campos" style="width: 100%;">
			<?
			if($_REQUEST["filtro_categoria"] <> ""){
				$res = sql_categorias("*"," and cat.cat_id = '".$_REQUEST["filtro_categoria"]."'"); 
	            if(mysqli_num_rows($res) > 0){
	                while($row = mysqli_fetch_array($res)){
	                    ?>
	                    <option value="<? echo $row["cat_id"]; ?>"><? 
	                    echo $row["cat_codigo"]." - ".$row["cat_nombre"]; 
	                    ?></option>
	                    <?
	                }
	            }
			}
			?>
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<b>Ubicación:</b><br>
			<select name="filtro_ubicacion" id="filtro_ubicacion" class="campos" style="width: 100%;">
			<?
			if($_REQUEST["filtro_ubicacion"] <> ""){
				$res = sql_ubicaciones("*"," and ubi.ubi_id = '".$_REQUEST["filtro_ubicacion"]."'"); 
	            if(mysqli_num_rows($res) > 0){
	                while($row = mysqli_fetch_array($res)){
	                    ?>
	                    <option value="<? echo $row["ubi_id"]; ?>"><? 
	                    echo $row["ubi_codigo"]." - ".$row["ubi_nombre"]; 
	                    ?></option>
	                    <?
	                }
	            }
			}
			?>
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<b>Responsable:</b><br>
			<select name="filtro_responsable" id="filtro_responsable" class="campos" style="width: 100%;">
			<?php
			if($_REQUEST["filtro_responsable"] <> ""){
	            $res = sql_personas("*"," and per.per_id = '".$_REQUEST["filtro_responsable"]."'");  
	            if(mysqli_num_rows($res) > 0){
	                while($row = mysqli_fetch_array($res)){
	                    ?>
	                    <option value="<? echo $row["per_id"]; ?>"><?php 
	                    echo $row["per_nombre"]; 
	                    ?></option>
	                    <?
	                }
	            }
            }
            ?>  
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<b>Tipo del Activo:</b><br>
			<select name="filtro_tipo" id="filtro_tipo" class="campos" style="width: 100%;">
			<option value="">- Todos -</option>
			<?php
			foreach($_tipo_activos as $tp){
				if(in_array("act_".strtolower($tp)."_ver",$opciones_persona)){
					?><option value="<? echo $tp; ?>" <? if($_REQUEST["filtro_tipo"] == $tp){ echo "selected"; } ?>><?
					echo $_tipo_activo[$tp]; ?></option>
					<?
				}
			}
			?>
			</select>
		</td>
	</tr> 
	<tr>
		<td>
			<b>Lugar:</b><br>
			<select name="filtro_lugar" id="filtro_lugar" class="campos" style="width: 100%;"> 
		    <?php
		    if($_REQUEST["filtro_lugar"] <> ""){
	            $res = sql_bodegas("*"," and bod.bod_id = '".$_REQUEST["filtro_lugar"]."'");  
	            if(mysqli_num_rows($res) > 0){
	                while($row = mysqli_fetch_array($res)){
	                    ?><option value="<?php echo $row["bod_id"]; ?>"><?php 
	                    echo $row["bod_codigo"]; ?> - <?php echo $row["bod_nombre"]; ?></option>
	                    <?
	                }
	            }
            } 
		    ?>
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<b>Estado Activo:</b><br>
			<select name="filtro_estado" id="filtro_estado" class="campos w10">
				<option value="">Todos</option>  
				<option value="0" <? 
				if($_REQUEST["filtro_estado"] == "0"){ echo "selected"; } ?>>No Disponible</option>
				<option value="1" <? 
				if($_REQUEST["filtro_estado"] == "1"){ echo "selected"; } ?>>Disponible</option>
				<option value="2" <? 
				if($_REQUEST["filtro_estado"] == "2"){ echo "selected"; } ?>>Prestado</option>
				<option value="3" <? 
				if($_REQUEST["filtro_estado"] == "3"){ echo "selected"; } ?>>En Mantenimiento</option>
				<option value="4" <? 
				if($_REQUEST["filtro_estado"] == "4"){ echo "selected"; } ?>>De Baja</option>
				<option value="5" <? 
				if($_REQUEST["filtro_estado"] == "5"){ echo "selected"; } ?>>Req.Mantenimiento</option>
				<option value="6" <? 
				if($_REQUEST["filtro_estado"] == "6"){ echo "selected"; } ?>>Asignado</option>
			</select>
		</td>
	</tr>
	</tbody>
	</table> 

	<a href="javascript:buscar();" title="Buscar" class="css_btn_class"><span class="glyphicon glyphicon-search" style="font-size:18px;"></span> Filtrar Activos</a>

	<script type="text/javascript"> 
	$("#filtro_categoria").select2({        
	    ajax: {
	        url: "mods/home/filtros_ajax.php?modo=categorias",
	        dataType: 'json',
	        delay: 250,
	        data: function (params) {
	            return {
	                q: params.term // search term
	            };
	        },
	        processResults: function (data) {
	            return {
	                results: $.map(data, function(obj) {
	                    return { id: obj.id, text: obj.text };
	                })
	            };
	        },
	        cache: true
	    },
	    minimumInputLength: 3 
	});
	$("#filtro_ubicacion").select2({        
	    ajax: {
	        url: "mods/home/filtros_ajax.php?modo=ubicaciones",
	        dataType: 'json',
	        delay: 250,
	        data: function (params) {
	            return {
	                q: params.term // search term
	            };
	        },
	        processResults: function (data) {
	            return {
	                results: $.map(data, function(obj) {
	                    return { id: obj.id, text: obj.text };
	                })
	            };
	        },
	        cache: true
	    },
	    minimumInputLength: 3 
	});
	$("#filtro_responsable").select2({        
	    ajax: {
	        url: "mods/home/filtros_ajax.php?modo=responsables",
	        dataType: 'json',
	        delay: 250,
	        data: function (params) {
	            return {
	                q: params.term // search term
	            };
	        },
	        processResults: function (data) {
	            return {
	                results: $.map(data, function(obj) {
	                    return { id: obj.id, text: obj.text };
	                })
	            };
	        },
	        cache: true
	    },
	    minimumInputLength: 3 
	}); 
	$("#filtro_lugar").select2({        
	    ajax: {
	        url: "mods/home/filtros_ajax.php?modo=lugares",
	        dataType: 'json',
	        delay: 250,
	        data: function (params) {
	            return {
	                q: params.term // search term
	            };
	        },
	        processResults: function (data) {
	            return {
	                results: $.map(data, function(obj) {
	                    return { id: obj.id, text: obj.text };
	                })
	            };
	        },
	        cache: true
	    },
	    minimumInputLength: 3 
	});
	</script>  
	
	<?
	/****************************************************************************************/
	if($_REQUEST["filtro_codigo"] <> ""){
		$filtros .= " and pro.pro_codigo like '".$_REQUEST["filtro_codigo"]."' "; 
	}
	if($_REQUEST["filtro_codigo_activo"] <> ""){
		$filtros .= " and acf.acf_codigo like '".$_REQUEST["filtro_codigo_activo"]."' "; 
	}
	if($_REQUEST["filtro_serie"] <> ""){
		$filtros .= " and acf.acf_serie like '".$_REQUEST["filtro_serie"]."' "; 
	}
	if($_REQUEST["filtro_etiqueta"] <> ""){
		$filtros .= " and acf.acf_etiqueta like '".$_REQUEST["filtro_etiqueta"]."' "; 
	}
	if($_REQUEST["filtro_descripcion"] <> ""){
		$filtros .= " and pro.pro_nombre like '".$_REQUEST["filtro_descripcion"]."' "; 
	} 
	if($_REQUEST["filtro_unidad"] <> ""){
		$filtros .= " and pro.pro_unidad = '".$_REQUEST["filtro_unidad"]."' "; 
	}  
	if($_REQUEST["filtro_categoria"] <> ""){
		$filtros .= " and pro.pro_categoria = '".$_REQUEST["filtro_categoria"]."' "; 
	}
	if($_REQUEST["filtro_cco"] <> ""){
		$filtros .= " and acf.acf_cco_id = '".$_REQUEST["filtro_cco"]."' "; 
	}
	if($_REQUEST["filtro_ubicacion"] <> ""){
		$filtros .= " and acf.acf_ubicacion = '".$_REQUEST["filtro_ubicacion"]."' "; 
	} 
	if($_REQUEST["filtro_lugar"] <> ""){
		$filtros .= " and ubi.ubi_bodega = '".$_REQUEST["filtro_lugar"]."' "; 
	} 
	if($_REQUEST["filtro_tipo"] <> ""){
		$filtros .= " and pro.pro_tipo = '".$_REQUEST["filtro_tipo"]."' "; 
	}    
	if($_REQUEST["filtro_compra_inicio"] <> ""){
		$filtros .= " and acf.acf_fecha_ingreso >= '"._fec($_REQUEST["filtro_compra_inicio"],9)."' "; 
	} 
	if($_REQUEST["filtro_compra_termino"] <> ""){
		$filtros .= " and acf.acf_fecha_ingreso <= '"._fec($_REQUEST["filtro_compra_termino"],9)."' "; 
	} 
	if($_REQUEST["filtro_responsable"] <> ""){
		$filtros .= " and acf.acf_responsable = '".$_REQUEST["filtro_responsable"]."' "; 
	} 
	if($_REQUEST["filtro_estado"] <> ""){
		$filtros .= " and acf.acf_disponible = '".$_REQUEST["filtro_estado"]."' "; 
	}
	if($_REQUEST["filtro_auxiliar"] <> ""){
		$filtros .= " and acf.acf_proveedor = '".$_REQUEST["filtro_auxiliar"]."' "; 
	}
	if($_REQUEST["filtro_doc_ingreso"] <> ""){
		$filtros .= " and acf.acf_nro_factura = '".$_REQUEST["filtro_doc_ingreso"]."' "; 
	}

	if($filtros <> ""){  
		?>
		<input type="hidden" name="pagina" id="pagina" value="<? echo $pagina; ?>" class="campos">
		<?
		$sql_paginador = sql_activos_fijos_rapido("*"," $filtros ORDER BY acf.acf_producto asc, acf.acf_serie asc","s",""); 
		$cant_paginador = 10;
		require("../../_paginador.php");
		$res = mysqli_query($cnx,$sql_final); 
		if(mysqli_num_rows($res) > 0){
			?>
			<table class="table table-striped table-bordered tabledrag table-condensed" style="margin-bottom: 0px;"> 
			<thead>
				<tr>  
					<th width="65"></th> 
					<th width="1">Imagen</th>
		 
					<th>Códigos</th>
					<th>Producto</th>
					 
					<th>Lugar</th>
					<th>Ubicación</th>
					<th>Responsable</th>
					<th>Info Ingreso</th>

					<th>Clasificación</th>
					<th>Más Información</th> 

					<th width="1">Estado</th>
				</tr>
			</thead>
			<tbody> 
			<?
			while($row = mysqli_fetch_array($res)){  
				?>
				<tr> 
					<td><? 
					construir_boton("acf_info.php","&acf=".$row["acf_id"],"buscar","Ver Activo Fijo"); 
					construir_boton("acf_editar.php","&id=".$row["acf_id"],"editar","Editar Activo Fijo");
					construir_boton("visor('acf_pdf.php?acf=".$row["acf_id"]."')","1","imprimir","Imprimir Ficha",4);
					if($row["acf_etiqueta"] <> ""){
						construir_boton("abrir_pagina('cdu_pdf.php?cdu[]=".$row["acf_etiqueta"]."')","1","barcode","Imprimir Código",4);
					}
					?></td> 
					<td><? 
					$imagen = ($row["pro_imagen"] == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$row["pro_imagen"];
					?><img src="<?php echo $imagen; ?>" style="width:40px !important;"></td> 
					<td>
						<b>Cod.Activo:</b> <?php echo $row["acf_codigo"]; ?><br>
						<b>Serie:</b> <?php echo $row["acf_serie"]; ?><br>
						<b>Etiqueta:</b> <?php echo $row["acf_etiqueta"]; ?>
					</td>
					<td>
						<?php echo $row["pro_nombre"]; ?><br>
						<b><?php echo $row["pro_codigo"]; ?></b>
					</td> 	
					<td><?php 
					if($row["ubi_bodega"] <> 0){
						echo $_bodegas[$row["ubi_bodega"]]; ?><br>
						<b><? echo $_bodegas_codigo[$row["ubi_bodega"]]; ?></b><?
					}
					?></td>
					<td><?php
					if($row["acf_ubicacion"] <> 0){
						echo $_ubicaciones[$row["acf_ubicacion"]]; ?><br>
						<b><?php echo $_ubicaciones_codigo[$row["acf_ubicacion"]]; ?></b><?
					}
					?></td> 
					<td><?php echo $_personas[$row["acf_responsable"]]; ?></td> 
					<td>
						Fecha Ingreso: <?php echo _fec($row["acf_fecha_ingreso"],5); ?><br>
						Doc.Ingreso: <? echo $row["acf_nro_factura"]; ?><br>
						Proveedor: <? echo $_SESSION["auxiliares"][$row["acf_proveedor"]]; ?><br>
						Valor Compra (<? echo $_configuraciones["cfg_moneda"]; ?>): <?php 
						echo _num($row["acf_valor"],$_configuraciones["cfg_moneda_decimal"]);
						if($row["acf_valor_sec"] > 0){
							?><br>
							2da Moneda (<? echo $_configuraciones["cfg_moneda_sec"]; ?>): <?php 
							echo _num($row["acf_valor_sec"],$_configuraciones["cfg_moneda_sec_decimal"]); 
						}
						?>
					</td>  
					<td>
						<?php echo $_categorias[$row["pro_categoria"]]; ?><br>
						<b><?php echo $_categorias_codigo[$row["pro_categoria"]]; ?></b>
					</td> 
					<td>
						<b>C.Costo:</b> <? echo $_centro_costos_corto[$row["acf_cco_id"]]; ?>
						<?
						foreach($_campo_pro as $dato){
							if($row["pro_extra_".$dato[1]] <> ""){
								?><br><b><? echo $dato[0]; ?>:</b> <? echo $row["pro_extra_".$dato[1]]; ?><?
							}
						}
						foreach($_campo_acf as $dato){
							if($row["acf_extra_".$dato[1]] <> ""){
								?><br><b><? echo $dato[0]; ?>:</b> <? echo $row["acf_extra_".$dato[1]]; ?><?
							}
						}
						?>
					</td> 
					<td><?php activo_fijo_estado($row["acf_disponible"]); ?></td> 
				</tr>
				<?  
			}
			?>
			</tbody>
			</table> 
			<?
			echo $paginador_dibujo;
		}else{
			?>
			<div class="alert alert-danger">
				<strong>Sin resultados</strong>
			</div>
			<?php 
		} 
	}else{
		?>
		<div class="alert alert-danger">
			<strong>Realice un filtro para ver resultados</strong>
		</div>
		<?php 
	} 
	?>
</div>