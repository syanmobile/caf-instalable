<!-- Bootstrap core CSS -->
<link href="<?php echo $url_base; ?>css/theme/bootstrap_default.css" rel="stylesheet">
<link href="<?php echo $url_base; ?>css/estilos.css" rel="stylesheet">
<link href="<?php echo $url_base; ?>css/fontawesome-free/css/all.css" rel="stylesheet">

<!-- Librerias CSS --> 
<link href="<?php echo $url_base; ?>css/chosen.css" rel="stylesheet">
<link href="<?php echo $url_base; ?>css/datepicker.css" rel="stylesheet">
<link href="<?php echo $url_base; ?>css/lightbox.css" rel="stylesheet">
<link href="<?php echo $url_base; ?>css/bootstrap-wysihtml5.css" rel="stylesheet">
<link href="<?php echo $url_base; ?>css/uploadfile.css" rel="stylesheet"> 
<link href="<?php echo $url_base; ?>css/jquery.tagsinput.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="<?php echo $url_base; ?>css/sticky-footer-navbar.css" rel="stylesheet">
<link href="<?php echo $url_base; ?>upl/uploadify.css" rel="stylesheet">  

<link href="<?php echo $url_base; ?>js/jquery.dataTables.min.css" rel="stylesheet">

<link href="<?php echo $url_base; ?>css/autocomplete.css" type="text/css" rel="stylesheet"></link>

<link href="<?php echo $url_base; ?>css/select2.css" type="text/css" rel="stylesheet"></link>

<link rel="stylesheet" type="text/css" media="print" href="<?php echo $url_base; ?>css/impresora.css" /> 

<style type="text/css">

.css_btn_class {
  width:100% !important;
  color:#777777 !important;
  text-align:center !important; 
  font-size:18px;
  font-family:Arial;
  font-weight:bold;
  -moz-border-radius:8px;
  -webkit-border-radius:8px;
  border-radius:8px;
  border:1px solid #dcdcdc;
  padding:10px 10px;
  text-decoration:none;
  background:-moz-linear-gradient( center top, #ededed 5%, #dfdfdf 100% );
  background:-ms-linear-gradient( top, #ededed 5%, #dfdfdf 100% );
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf');
  background:-webkit-gradient( linear, left top, left bottom, color-stop(5%, #ededed), color-stop(100%, #dfdfdf) );
  background-color:#ededed; 
  display:inline-block;
  text-shadow:1px 1px 0px #ffffff;
  -webkit-box-shadow:inset 1px 1px 0px 0px #ffffff;
  -moz-box-shadow:inset 1px 1px 0px 0px #ffffff;
  box-shadow:inset 1px 1px 0px 0px #ffffff;
}.css_btn_class:hover {
  background:-moz-linear-gradient( center top, #dfdfdf 5%, #ededed 100% );
  background:-ms-linear-gradient( top, #dfdfdf 5%, #ededed 100% );
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#dfdfdf', endColorstr='#ededed');
  background:-webkit-gradient( linear, left top, left bottom, color-stop(5%, #dfdfdf), color-stop(100%, #ededed) );
  background-color:#dfdfdf;
}.css_btn_class:active {
  position:relative;
  top:1px;
} 
</style>