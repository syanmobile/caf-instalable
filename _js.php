<script language="javascript">
var url_base = "<?php echo $url_base; ?>";
var modulo_base = "mods/home/";
	
// Inhabilita el boton BACK ----------------------------------------
history.pushState(null, null, location.href);
window.onpopstate = function () {
	history.go(1);
};
</script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<script src="<?php echo $url_base; ?>js/jquery-1.11.1.min.js"></script> 
<script src="<?php echo $url_base; ?>js/jquery.form2.js"></script>
<script src="<?php echo $url_base; ?>js/bootstrap.min.js"></script> 

<!-- Librerias JS --> 
<script src="<?php echo $url_base; ?>js/main.js"></script>
<script src="<?php echo $url_base; ?>js/jquery.Rut.js"></script> 

<script>
// creamos el objeto audio
var audioElement = document.createElement('audio');
audioElement.setAttribute('src', '<? echo $url_base; ?>sonido-error.mp3');
</script>
	
<script src="<?php echo $url_base; ?>js/funciones.2.js"></script>
<script src="<?php echo $url_base; ?>js/funciones_popup.js"></script>

<script src="<?php echo $url_base; ?>js/bootstrap-datepicker.js"></script>
<script src="<?php echo $url_base; ?>js/modernizr.custom.js"></script>
<script src="<?php echo $url_base; ?>js/lightbox-2.6.min.js"></script>
<script src="<?php echo $url_base; ?>js/wysihtml5-0.3.0.js"></script>
<script src="<?php echo $url_base; ?>js/bootstrap-wysihtml5.js"></script>
<script src="<?php echo $url_base; ?>js/chosen.jquery.js"></script>
<script src="<?php echo $url_base; ?>js/jquery.numberformatter.js"></script>
<script src="<?php echo $url_base; ?>js/jquery.tablednd.js"></script>
<script src="<?php echo $url_base; ?>js/jquery.uploadfile.js"></script> 
<script src="<?php echo $url_base; ?>js/jquery.maskedinput.js"></script>
<script src="<?php echo $url_base; ?>js/jquery.maskMoney.js"></script>
<script src="<?php echo $url_base; ?>js/jquery.numeric.js"></script>
<script src="<?php echo $url_base; ?>js/jquery.tagsinput.js"></script>
<script src="<?php echo $url_base; ?>js/jquery.priceformat.js"></script> 

<script src="<?php echo $url_base; ?>js/jquery.dataTables.js"></script>

<script src="<?php echo $url_base; ?>js/autocomplete.jquery.js"></script>

<script src="<?php echo $url_base; ?>js/select2.js"></script>  

<!-- EXCEL -->
<script src="<?php echo $url_base; ?>js/excel/FileSaver.min.js"></script>
<script src="<?php echo $url_base; ?>js/excel/Blob.min.js"></script>
<script src="<?php echo $url_base; ?>js/excel/xls.core.min.js"></script>
<script src="<?php echo $url_base; ?>js/excel/tableexport.js"></script> 