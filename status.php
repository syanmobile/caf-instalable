<?php
session_start(); 
error_reporting(0); 
require("inc/db.php");    
?>
<link href="css/theme/bootstrap_default.css" rel="stylesheet">

<h1>Status CAF</h1>
<table class="table table-bordered table-condensed">
<thead>
	<tr>
		<th colspan="2">Base de Datos</th>
	</tr>
</thead>
<tbody>
	<tr>
		<th width="150">Host:</th>
		<td><? echo $app_db_host; ?></td>
	</tr>
	<tr>
		<th>Usuario:</th>
		<td><? echo $app_db_usuario; ?></td>
	</tr>
	<tr>
		<th>Clave:</th>
		<td><? echo $app_db_password; ?></td>
	</tr>
	<tr>
		<th>Base de datos:</th>
		<td><? echo $app_db_basededatos; ?></td>
	</tr>
	<tr>
		<th>Test de conexión:</th>
		<td><?  
		$cnx = mysqli_connect($app_db_host,$app_db_usuario,$app_db_password);
		if(!$cnx){
			printf("Falla de conexión: %s\n", mysqli_connect_error());
		}else{
			echo "OK";
		}
		?></td>
	</tr>
</tbody>
</table>

<table class="table table-bordered table-condensed">
<thead>
	<tr>
		<th colspan="2">Librerías</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td><? 
		$librerias[] = "funciones.php";
		$librerias[] = "funciones_dibujo.php";
		$librerias[] = "funciones_campo.php";
		$librerias[] = "widgets.php";
		$librerias[] = "navegacion.php"; 

		$librerias[] = "sqls/erp_activos.php";
		$librerias[] = "sqls/erp_auxiliar.php"; 
		$librerias[] = "sqls/erp_bodega.php"; 
		$librerias[] = "sqls/erp_campos.php"; 
		$librerias[] = "sqls/erp_categoria.php"; 
		$librerias[] = "sqls/erp_centro_costo.php"; 
		$librerias[] = "sqls/erp_configuracion.php"; 
		$librerias[] = "sqls/erp_concepto.php"; 
		$librerias[] = "sqls/erp_control.php"; 
		$librerias[] = "sqls/erp_movimiento.php"; 
		$librerias[] = "sqls/erp_perfil.php";    
		$librerias[] = "sqls/erp_persona.php";    
		$librerias[] = "sqls/erp_producto.php";    
		$librerias[] = "sqls/erp_toma_inventario.php";    
		$librerias[] = "sqls/erp_ubicacion.php";    
		$librerias[] = "sqls/erp_unidad.php";    
		$librerias[] = "validaciones.php";  

		foreach($librerias as $libreria){
			echo "<b>".$libreria."</b> | Cargando....  ";
			require("inc/".$libreria);
			echo "OK<br>";
		}
		?></td>
	</tr>
</tbody>
</table>

<table class="table table-bordered table-condensed">
<thead>
	<tr>
		<th colspan="2">Valida calcula_meses_transcurridos()</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td><?
		$inicio = "2019-01-01";
		$termino = "2020-08-23";
		calcula_meses_transcurridos($inicio,$termino);

		$inicio = "2019-02-01";
		$termino = "2019-12-23";
		calcula_meses_transcurridos($inicio,$termino);

		$inicio = "2019-01-01";
		$termino = "2020-03-23";
		calcula_meses_transcurridos($inicio,$termino);

		$inicio = "2018-01-01";
		$termino = "2020-03-23";
		calcula_meses_transcurridos($inicio,$termino);

		$inicio = "2020-01-01";
		$termino = "2020-03-23";
		calcula_meses_transcurridos($inicio,$termino);

		$inicio = "2020-01-01";
		$termino = "2020-01-01";
		calcula_meses_transcurridos($inicio,$termino);

		$inicio = "2020-01-01";
		$termino = "2020-07-07";
		calcula_meses_transcurridos($inicio,$termino);

		$inicio = "2020-01-01";
		$termino = "2021-07-07";
		calcula_meses_transcurridos($inicio,$termino);
		?></td>
</tr>
</tbody>
</table>