<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>CAF | <?php echo _opc('nombre_empresa'); ?></title>

    <?php require("_js.php"); ?>
    <?php require("_css.php"); ?>
  </head>

  <body>

    <!-- Wrap all page content here -->
    <div id="wrap">
	  <?php require("_menu.php"); ?>

      <!-- Begin page content -->
      <div class="container">
      	<div id="lugar_ajax">
