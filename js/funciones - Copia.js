
/*********************/
function hoydia(valor,input){  
	document.getElementById(input).value = valor;
}
function comunas2(region,ini){  
	var division = document.getElementById("comuna");
	AJAXPOST("ajax_general.php","&comunas&region="+region+"&ini="+ini,division);  
}
function avisos_resumen(){
	var division = document.getElementById("myModal2"); 
	AJAXPOST("mods/home/ord_planificacion_ajax.php","modo=caducados",division,false,function(){
		$('#myModal2').modal('show');
	});
}  
function quitar_accion(acc){ 
	if(confirm("Quitar esta accion?")){
		$("[filacc="+acc+"]").fadeOut("normal");
        SoloEnviar("mods/crm/acc_ajax.php","&modo=quitar_accion&acc="+acc);
	}
}
function crear_aviso(){
	var division = document.getElementById("myModal2"); 
	AJAXPOST("mods/home/ord_planificacion_ajax.php","modo=crear_aviso",division,false,function(){
		$('#myModal2').modal('show');
	});
} 
function enviar_aviso(){ 
	var division = document.getElementById("myModal2_"); 
	var a = $(".especx").fieldSerialize();   
	AJAXPOST("mods/home/ord_planificacion_ajax.php",a+"&modo=enviar_aviso",division); 
}
function cerrar_aviso(id){
	$("[fila=avisar_"+id+"]").fadeOut("normal");
	SoloEnviar("mods/home/ord_planificacion_ajax.php","modo=cerrar_aviso&pla="+id);
} 
function cerrar_avisos(){
	$("#todos_avisos").fadeOut("normal");
	SoloEnviar("mods/home/ord_planificacion_ajax.php","modo=cerrar_avisos");
}  
function avisa_luego(id){ 
	$("[fila=avisar_"+id+"]").fadeOut("normal");
	SoloEnviar("mods/home/ord_planificacion_ajax.php","modo=avisa_luego&pla="+id);
}
function cambiar_estado_accion(id){
	var division = document.getElementById("accion_"+id);
	AJAXPOST("mods/crm/acc_ajax.php","modo=cambiar_estado&acc="+id,division);
}
function abrir_cliente(id,pest){
	var division = document.getElementById("operacion_home");
	AJAXPOST("mods/home/cli_ajax.php","&modo=abrir_cliente&id="+id+"&pest="+pest,division);
}
function alertas_resumen(){
	var division = document.getElementById("myModal2"); 
	AJAXPOST("mods/crm/alerta_acciones.php","",division,false,function(){
		$('#myModal2').modal('show');
	});
}
function mis_tareas(){
	var a = $(".especx").fieldSerialize();  
	var division = document.getElementById("lugar_principal"); 
	AJAXPOST("mods/crm/mis_acciones.php",a,division ); 
}
function filtro_Patente(){ 
	var division = document.getElementById("myModal2"); 
	AJAXPOST("mods/ordenes/veh_patentes.php","",division,false,function(){
		$('#myModal2').modal('show');
	}); 
}
function accion_ver_p(id,popap){
	var division = document.getElementById("lugar_principal"); 
	AJAXPOST("mods/crm/ver_accion.php","&id="+id+"&popap="+popap,division);
}
function accion_cambiar(){
	if(val_obligatorio("operacion_tarea_p") == false){ return; }
	var division = document.getElementById("lugar_principal");
	var a = $(".campos3").fieldSerialize(); 
	AJAXPOST("mods/crm/ver_accion.php",a,division);
}
function abrir_ot_p(id){
	$('#myModal2').modal('hide');
	carg("ord_editar.php","&id="+id);
}
function abrir_cliente_p(id){
	$('#myModal2').modal('hide');
	var division = document.getElementById("lugar_principal");
	AJAXPOST("mods/home/cli_ajax.php","&modo=abrir_cliente&id="+id+"&pest=6",division);
}
function proy_cli(id){
	var division = document.getElementById("proyecto_a");
	AJAXPOST("mods/crm/acc_ajax.php","&modo=proy_cliente&id="+id,division);
}
function abrir_cliente_p2(id){
	$('#myModal2').modal('hide');
	carg("cli_ver2.php","&id="+id);
}
function crear_accion(cli,cli2){
	var division = document.getElementById("lugar_principal");
	AJAXPOST(url_base+"mods/crm/acc_ajax.php","modo=crear_accion&cli="+cli+"&cli2="+cli2,division);	
}
function editar_accion(acc){
	var division = document.getElementById("lugar_principal");
	AJAXPOST(url_base+"mods/crm/acc_ajax.php","modo=editar_accion&acc="+acc,division);	
}
function save_accion(){  
	if(val_obligatorio("tipo_ticket") == false){ return; }
	if(val_obligatorio("asunto_ticket") == false){ return; }
	if(val_obligatorio("motivos_ticket") == false){ return; }
	
	var fecha1 = document.getElementById("fecha_inicio").value;
	var fecha2 = document.getElementById("fecha_termino").value;
	if (compare_dates(fecha1, fecha2)){ 
		alert("Fecha vencimiento es menor a la fecha.");
		document.getElementById("fecha_termino").focus();
		return; 
	}else{
	} 
	
	var division = document.getElementById("lugar_principal");
	var a = $(".campos").fieldSerialize(); 
	AJAXPOST(url_base+"mods/crm/acc_ajax.php",a+"&modo=guardar_accion",division);	
}
function compare_dates(fecha, fecha2)
      {
        var xMonth=fecha.substring(3, 5);
        var xDay=fecha.substring(0, 2);
        var xYear=fecha.substring(6,10);
        var yMonth=fecha2.substring(3, 5);
        var yDay=fecha2.substring(0, 2);
        var yYear=fecha2.substring(6,10);
        if (xYear> yYear)
        {
            return(true)
        }
        else
        {
          if (xYear == yYear)
          {
            if (xMonth> yMonth)
            {
                return(true)
            }
            else
            {
              if (xMonth == yMonth)
              {
                if (xDay> yDay)
                  return(true);
                else
                  return(false);
              }
              else
                return(false);
            }
          }
          else
            return(false);
        }
    }
/**************************************/
function existencia(campo,tipo){
	var valor = document.getElementById(campo+"").value;
	AJAXPOST("mods/sistema/existencias.php","&tipo="+tipo+"&valor="+valor,false,false,resultado_verificar);	
	function resultado_verificar(con){
		res = eval("("+con.responseText+")");
		if(res.estado){ // SI LOS DATOS FUERON CORRECTOS
			return true;
		}else{
			alert("No existe: "+valor);
			return false;
		}
	}
}
/****************************************/
function repositorio(){
	$("#lugar_modal").html(''); 
	var division = document.getElementById("lugar_modal"); 
	AJAXPOST(url_base+"repositorio_inicio.php","",division);
}
/******************************************/
function ayuda(){
	$("#lugar_modal").html(''); 
	var division = document.getElementById("lugar_modal"); 
	AJAXPOST(url_base+"ayuda.php","&menu=0&indice=0",division,false,function(){
		$('#myModal').modal('show');
	});
}
function ayuda_dentro(menu,indice){
	var division = document.getElementById("lugar_modal"); 
	AJAXPOST(url_base+"ayuda.php","&menu="+menu+"&indice="+indice,division);
}
/*****************************************/
function pagina(p){
	var pagina = document.getElementById("pagina").value;
	carg(pagina,"&p="+p);
}
/*****************************************/
function chg_orden(campo){
	if(campo == document.getElementById("ord_campo").value){
		if(document.getElementById("ord_modo").value == "asc"){
			document.getElementById("ord_modo").value = "desc";
		}else{
			document.getElementById("ord_modo").value = "asc";
		}
	}else{
		document.getElementById("ord_campo").value = campo;
	}
	pagina(1);
}
function chg_orden2(campo){
	if(campo == document.getElementById("ord_campo2").value){
		if(document.getElementById("ord_modo2").value == "asc"){
			document.getElementById("ord_modo2").value = "desc";
		}else{
			document.getElementById("ord_modo2").value = "asc";
		}
	}else{
		document.getElementById("ord_campo2").value = campo;
	}
	cargar_acciones();
}
/****************************************/ 
function carg(pagina,extra){ 
	var division = document.getElementById("lugar_principal");
	var a = $(".campos").fieldSerialize(); 
	AJAXPOST(url_base+modulo_base+pagina,a+extra,division,false,function(){ 
		$(".buscador").chosen({
			width:"95%",
			no_results_text:'Sin resultados con',
			allow_single_deselect:true 
		});
		$(".fecha").datepicker();
		$(".numero").numeric();
		$(".hora").mask("99:99"); 
		$(".moneda").maskMoney({thousands:'.',  precision: 0, allowZero:true,  prefix: '$ '});
	}); 
}
function carg2(pagina,extra){ 
	var division = document.getElementById("lugar_principal");
	var a = $(".campos").fieldSerialize(); 
	$("#lugar_principal").html('<img src="'+url_base+'img/loading.gif">');
	AJAXPOST(url_base+"mods/mobile/"+pagina,a+extra,division,false,function(){ 
		$(".buscador").chosen({
			width:"95%",
			no_results_text:'Sin resultados con',
			allow_single_deselect:true 
		});
		$(".fecha").datepicker();
		$(".numero").numeric();
		$(".hora").mask("99:99"); 
		$(".moneda").maskMoney({thousands:'.',  precision: 0, allowZero:true,  prefix: '$ '});
	}); 
}
function carg3(pagina,extra){ 
	var division = document.getElementById("lugar_principal"); 
	$("#lugar_principal").html('<img src="'+url_base+'img/loading.gif">');
	AJAXPOST(url_base+modulo_base+pagina,extra,division,false,function(){ 
		$(".buscador").chosen({
			width:"95%",
			no_results_text:'Sin resultados con',
			allow_single_deselect:true 
		});
		$(".fecha").datepicker();
		$(".numero").numeric();
		$(".hora").mask("99:99"); 
		$(".moneda").maskMoney({thousands:'.',  precision: 0, allowZero:true,  prefix: '$ '});
	}); 
}
/****************************************/ 
function cambiar_mdl(id,car){
	SoloEnviar(url_base+"_ajax.php","modo=cambiar_modulo&id="+id+"&carpeta="+car,function(){
		ir_a('index.php');
	}); 
}
function ver_propiedad(id){
	SoloEnviar(url_base+"_ajax.php","modo=ver_propiedad",function(){
		ir_a('index.php?cli='+id);
	}); 
}
function ver_remate(id){
	SoloEnviar(url_base+"_ajax.php","modo=ver_remate",function(){
		ir_a('index.php?rem='+id);
	}); 
}
/****************************************/ 
function ir_a(pagina){
	location.href = url_base+pagina;
}
/*******************************************/
function alerta_js(tit,msj){
	$("#titulo_alerta_pop").html(tit);
	$("#texto_alerta_pop").html(msj);
	$('#myModal3').modal('show');
}
function vista_impresion(url){
	$("#frame_popup").attr('src',url);
	$('#myModal4').modal('show');
}
/******************************************/
function crearInput(pagina,celda,ancho) {
	dato_id = $(celda).parent().attr("fila");
	if(ancho == null)ancho = "auto";
	celda.ondblclick = function() {return false}
	txt = celda.innerHTML;
	celda.innerHTML = '';
	obj = celda.appendChild(document.createElement('input'));
	obj.style.width=ancho;
	obj.class = "form-control";
	obj.value = txt;
	obj.select()
	obj.focus();
	
	obj.onblur = function() {
		modo=$(celda).attr("modo");
		txt = this.value;
		celda.removeChild(obj);
		celda.innerHTML = txt;
		celda.ondblclick = function() {crearInput(pagina,celda, ancho)}
		modificarDatos(pagina,modo,celda,txt,dato_id);//Agregar AQUI funcion para guardar datos por AJAX  
	}
	obj.onkeyup = function(event){
	if(event.keyCode == 38){ //FLECHA SUPERIROR
		$(obj).blur();
		$(celda).parent().prev().children("[modo="+$(celda).attr("modo")+"]").dblclick();
	}
	if(event.keyCode == 40){ //FLECHA SUPERIROR
		$(obj).blur();
		$(celda).parent().next().children("[modo="+$(celda).attr("modo")+"]").dblclick();
	}
	if(event.keyCode == 13){
		$(obj).blur();
	}
	}
}
function crearInput2(dato_id,pagina,celda,ancho) { 
	if(ancho == null)ancho = "auto";
	celda.ondblclick = function() {return false}
	txt = celda.innerHTML;
	celda.innerHTML = '';
	obj = celda.appendChild(document.createElement('input'));
	obj.style.width=ancho;
	obj.class = "form-control";
	obj.value = txt;
	obj.select()
	obj.focus();
	
	obj.onblur = function() {
		modo=$(celda).attr("modo");
		txt = this.value;
		celda.removeChild(obj);
		celda.innerHTML = txt;
		celda.ondblclick = function() {crearInput(pagina,celda, ancho)}
		modificarDatos(pagina,modo,celda,txt,dato_id);//Agregar AQUI funcion para guardar datos por AJAX  
	}
	obj.onkeyup = function(event){
	if(event.keyCode == 38){ //FLECHA SUPERIROR
		$(obj).blur();
		$(celda).parent().prev().children("[modo="+$(celda).attr("modo")+"]").dblclick();
	}
	if(event.keyCode == 40){ //FLECHA SUPERIROR
		$(obj).blur();
		$(celda).parent().next().children("[modo="+$(celda).attr("modo")+"]").dblclick();
	}
	if(event.keyCode == 13){
		$(obj).blur();
	}
	}
}
function modificarDatos(pagina,modo,celda,txt, dato_id){
	var datos = "modo="+modo+"&valor="+escape(txt)+"&dato_id="+dato_id;
	AJAXPOST(url_base+modulo_base+pagina,datos,celda,"Espere..");
}
function val_obligatorio(campo){
	var existe = $("#"+campo).prop("disabled");
	if(!existe){
		$("#"+campo).removeClass("alerta");
		var valor = document.getElementById(campo).value + ""; 
		if(valor == ""){
			$("#"+campo).addClass("alerta");
			alert("Indique todos los campos obligatorios (*).");
			document.getElementById(campo+"").focus();
			return false;
		}
		return true;
	}else{
		return true;
	}
} 
function mostrar_ocultar(valor){
	$("#"+valor).toggle("fast");
}

/***********************/
function ordenar(campo){
    if(document.getElementById("campo").value == campo){
        if(document.getElementById("orden").value == "asc"){
            document.getElementById("orden").value = "desc";
        }else{
            document.getElementById("orden").value = "asc";
        }
    }else{
        document.getElementById("campo").value = campo;
    }
    carg(document.getElementById("pagina").value,"");
}
function registros(cantidad){
    if(cantidad != "buscar"){
        document.getElementById("cantidad").value = cantidad;
    }else{
        document.getElementById("cantidad").value = document.getElementById("cant").value;
    }
    document.getElementById("hoja").value = 1;
    carg(document.getElementById("pagina").value,"");
}
function hojear(hoja){
    document.getElementById("hoja").value = hoja;
    carg(document.getElementById("pagina").value,"");
} 
