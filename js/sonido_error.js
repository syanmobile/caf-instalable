var myAudioContext, myBuffers = {}, mySource;
$(document).ready(function(){
	init_error();
});
function init_error() {
	if ('webkitAudioContext' in window) {
		myAudioContext = new webkitAudioContext();
		fetchSounds();
	}
}
function fetchSounds() {
	var request = new XMLHttpRequest();
	request = new XMLHttpRequest();
	request.open('GET', 'sonido-error.mp3', true);
	request.responseType = 'arraybuffer';
	request.addEventListener('load', bufferSound, false);
	request.send();
}
function bufferSound(event) {
	var request = event.target;
	var buffer = myAudioContext.createBuffer(request.response, false);
	myBuffers['beep'] = buffer;
}
function playSound() {
	var source = myAudioContext.createBufferSource();
	source.buffer = myBuffers['beep'];
	source.loop = false;
	source.connect(myAudioContext.destination);
	source.noteOn(0);
	mySource = source;
}
function sonido_error() {
	if ('webkitAudioContext' in window) {
		playSound();
	} else {
		var snd = document.getElementById('audio');
		snd.play();
	}
};