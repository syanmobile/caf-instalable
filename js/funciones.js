
function sonido_error() {
	// iniciamos el audio
	audioElement.play();
} 

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
	$(".buscador").chosen({
		width:"95%",
		no_results_text:'Sin resultados con',
		allow_single_deselect:true 
	});
	$(".fecha").datepicker();
	$(".fecha2").mask("99/99/9999");
	$(".numero").numeric();
	$(".hora").mask("99:99"); 
	$(".moneda").maskMoney({thousands:'.',  precision: 0, allowZero:true,  prefix: '$ '});
});
function edicion_rapida(linea,campo,valor){
	var nuevos_valores = "";
	var info = document.getElementById(campo+"_det").value;
	var valor = prompt("Ingresar valor:", valor);
	if (valor != null) {
		var valores = info.split("*****");
		for(i = 1;i < valores.length;i++){
			if(i == linea){
				nuevos_valores = nuevos_valores + "*****" + valor;
			}else{
				nuevos_valores = nuevos_valores + "*****" + valores[i];
			}
		}
		document.getElementById(campo+"_det").value = nuevos_valores;
		detalle();
	}
}
function compare_dates(fecha, fecha2)
  {
	var xMonth=fecha.substring(3, 5);
	var xDay=fecha.substring(0, 2);
	var xYear=fecha.substring(6,10);
	var yMonth=fecha2.substring(3, 5);
	var yDay=fecha2.substring(0, 2);
	var yYear=fecha2.substring(6,10);
	if (xYear> yYear)
	{
		return(true)
	}
	else
	{
	  if (xYear == yYear)
	  {
		if (xMonth> yMonth)
		{
			return(true)
		}
		else
		{
		  if (xMonth == yMonth)
		  {
			if (xDay> yDay)
			  return(true);
			else
			  return(false);
		  }
		  else
			return(false);
		}
	  }
	  else
		return(false);
	}
}
/*****************************************/
function pagina(p){
	var pagina = document.getElementById("pagina").value;
	carg(pagina,"&p="+p);
}
/*****************************************/
function chg_orden(campo){
	if(campo == document.getElementById("ord_campo").value){
		if(document.getElementById("ord_modo").value == "asc"){
			document.getElementById("ord_modo").value = "desc";
		}else{
			document.getElementById("ord_modo").value = "asc";
		}
	}else{
		document.getElementById("ord_campo").value = campo;
	}
	pagina(1);
}
/****************************************/ 
function carg(pagina,extra){  
	var division = document.getElementById("lugar_principal");
	var a = $(".campos").fieldSerialize(); 
	AJAXPOST("pages/"+pagina,a+extra,division,false,function(){ 
		/*
		$(".buscador").chosen({
			width:"95%",
			no_results_text:'Sin resultados con',
			allow_single_deselect:true 
		});
		$(".fecha").datepicker();
		$(".fecha2").mask("99/99/9999");
		$(".numero").numeric();
		$(".hora").mask("99:99"); 
		$(".moneda").maskMoney({thousands:'.',  precision: 0, allowZero:true,  prefix: '$ '});
    	$('[data-toggle="tooltip"]').tooltip(); 
    	*/
	}); 
} 
function carg3(pagina,extra){
	var division = document.getElementById("lugar_principal");  
	AJAXPOST("pages/"+pagina,extra,division,false,function(){ 
		/*
		$(".buscador").chosen({
			width:"95%",
			no_results_text:'Sin resultados con',
			allow_single_deselect:true 
		});
		$(".fecha").datepicker();
		$(".fecha2").mask("99/99/9999");
		$(".numero").numeric();
		$(".hora").mask("99:99"); 
		$(".moneda").maskMoney({thousands:'.',  precision: 0, allowZero:true,  prefix: '$ '});
		*/
	}); 
}
/*******************************************/
function alerta_js(msj){ 
	$("#texto_alerta_pop").html(msj);
	$('#modAlertas').modal('show');
}
function alerta_sonido(msj){
	sonido_error();
	$("#texto_alerta_pop").html(msj);
	$('#modAlertas').modal('show');
}
function exito_js(msj){ 
	$("#texto_exito_pop").html(msj);
	$('#modExitos').modal('show');
}
function vista_impresion(url){
	$("#frame_popup").attr('src',url);
	$('#myModal4').modal('show');
}
/******************************************/
function crearInput(pagina,celda,ancho) {
	dato_id = $(celda).parent().attr("fila");
	if(ancho == null)ancho = "auto";
	celda.ondblclick = function() {return false}
	txt = celda.innerHTML;
	celda.innerHTML = '';
	obj = celda.appendChild(document.createElement('input'));
	obj.style.width=ancho;
	obj.class = "form-control";
	obj.value = txt;
	obj.select()
	obj.focus();
	
	obj.onblur = function() {
		modo=$(celda).attr("modo");
		txt = this.value;
		celda.removeChild(obj);
		celda.innerHTML = txt;
		celda.ondblclick = function() {crearInput(pagina,celda, ancho)}
		modificarDatos(pagina,modo,celda,txt,dato_id);//Agregar AQUI funcion para guardar datos por AJAX  
	}
	obj.onkeyup = function(event){
	if(event.keyCode == 38){ //FLECHA SUPERIROR
		$(obj).blur();
		$(celda).parent().prev().children("[modo="+$(celda).attr("modo")+"]").dblclick();
	}
	if(event.keyCode == 40){ //FLECHA SUPERIROR
		$(obj).blur();
		$(celda).parent().next().children("[modo="+$(celda).attr("modo")+"]").dblclick();
	}
	if(event.keyCode == 13){
		$(obj).blur();
	}
	}
}
function modificarDatos(pagina,modo,celda,txt, dato_id){
	var datos = "modo="+modo+"&valor="+escape(txt)+"&dato_id="+dato_id;
	AJAXPOST("pages/"+pagina,datos,celda,"Espere..");
}
function val_obligatorio(campo){
	var existe = $("#"+campo).prop("disabled");
	if(!existe){
		$("#"+campo).removeClass("alerta");
		var valor = document.getElementById(campo).value + ""; 
		if(valor == ""){
			$("#"+campo).addClass("alerta");
			alert("Indique todos los campos obligatorios (*).");
			document.getElementById(campo+"").focus();
			return false;
		}
		return true;
	}else{
		return true;
	}
} 
function mostrar_ocultar(valor){
	$("#"+valor).toggle("fast");
	$("."+valor).toggle("fast");
}

/***********************/
function ordenar(campo){
    if(document.getElementById("campo").value == campo){
        if(document.getElementById("orden").value == "asc"){
            document.getElementById("orden").value = "desc";
        }else{
            document.getElementById("orden").value = "asc";
        }
    }else{
        document.getElementById("campo").value = campo;
    }
    carg(document.getElementById("pagina").value,"");
}
function registros(cantidad){
    if(cantidad != "buscar"){
        document.getElementById("cantidad").value = cantidad;
    }else{
        document.getElementById("cantidad").value = document.getElementById("cant").value;
    }
    document.getElementById("hoja").value = 1;
    carg(document.getElementById("pagina").value,"");
}
function hojear(hoja){
    document.getElementById("hoja").value = hoja;
    carg(document.getElementById("pagina").value,"");
} 
function abrir_pagina(pagina){
	window.open("pages/"+pagina);
}
