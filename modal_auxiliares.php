
<!-- Modal Productos --> 
<div class="modal fade" id="modAuxiliares" tabindex="-1" role="dialog" aria-labelledby="modAuxiliares" aria-hidden="true">
    <div class="modal-dialog" style="width:1100px;">
        <div class="modal-content" style="padding:15px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <table width="100%">
            <tr valign="top">
            	<td width="300"> 
                	<div id="fma_filtros"></div>
                </td>
            	<td>
                	<div id="fma_resultados" style="margin-left:10px; overflow:scroll; height:500px; padding-right:5px;"></div>
                </td>
            </tr>
            </table>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script language="javascript"> 
function fma_filtros(){
	var div = document.getElementById("fma_filtros");
	var a = $(".fma_campos").fieldSerialize();
	AJAXPOST(url_base+modulo_base+"aux_ajax.php",a+"&modo=inicio",div);
}
function fma_buscar(){
	$("#fma_resultados").html("Espere un momento...");
	var div = document.getElementById("fma_resultados");
	var a = $(".fma_campos").fieldSerialize(); 
	AJAXPOST(url_base+modulo_base+"aux_ajax.php",a+"&modo=busqueda",div);
}
function auxiliares_popup(){ 
	fma_filtros();	
	$('#modAuxiliares').modal('show');
}
function busc_auxiliar(){
	var div = document.getElementById("load_auxiliar");
	var codigo = document.getElementById("auxiliar").value;
	AJAXPOST(url_base+modulo_base+"aux_ajax.php","modo=buscar&codigo="+codigo,div);
}
function elegir_auxiliar(codi,nomb){ 
	document.getElementById("auxiliar").value = codi;
	document.getElementById("auxiliar_nomb").value = nomb; 
	$('#modAuxiliares').modal('hide');
}
</script>