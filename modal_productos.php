
<!-- Modal Productos -->
<input type="hidden" id="id_pro">
<div class="modal fade" id="modProductos" tabindex="-1" role="dialog" aria-labelledby="modProductos" aria-hidden="true">
    <div class="modal-dialog" style="width:1100px;">
        <div class="modal-content" style="padding:15px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <table width="100%">
            <tr valign="top">
            	<td width="300"> 
                	<div id="fmp_filtros"></div>
                </td>
            	<td>
                	<div id="fmp_resultados" style="margin-left:10px; overflow:scroll; height:500px; padding-right:5px;"></div>
                </td>
            </tr>
            </table>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script language="javascript"> 
function fmp_filtros(){
	var div = document.getElementById("fmp_filtros");
	var a = $(".fmp_campos").fieldSerialize();
	AJAXPOST(url_base+modulo_base+"pro_ajax.php",a+"&modo=inicio",div);
}
function fmp_buscar(){
	$("#fmp_resultados").html("Espere un momento...");
	var div = document.getElementById("fmp_resultados");
	var a = $(".fmp_campos").fieldSerialize(); 
	AJAXPOST(url_base+modulo_base+"pro_ajax.php",a+"&modo=busqueda",div);
}
function productos_popup(id){
	document.getElementById("id_pro").value = id;
	fmp_filtros();	
	$('#modProductos').modal('show');
} 
function busc_producto(id){
	var div = document.getElementById("load_producto_"+id);
	var codigo = document.getElementById("codi_"+id).value;
	AJAXPOST(url_base+modulo_base+"pro_ajax.php","modo=buscar&codigo="+codigo+"&id="+id,div);
} 
function camb_producto(id){
	$("#codi_"+id).prop('readonly', false);
	$("#btn1_prd_"+id).show();
	$("#btn2_prd_"+id).hide();
	
	document.getElementById("id_pro").value = "";
	document.getElementById("codi_"+id).value = "";
	document.getElementById("nomb_"+id).value = "";
	document.getElementById("ptipo_"+id).value = "";  
}
function elegir_producto(codi,nomb,tipo){
	var id = document.getElementById("id_pro").value;
	document.getElementById("codi_"+id).value = codi;
	document.getElementById("nomb_"+id).value = nomb; 
	document.getElementById("ptipo_"+id).value = tipo; 
	$('#modProductos').modal('hide');
	
	$("#codi_"+id).prop('readonly', true);
	$("#btn1_prd_"+id).hide();
	$("#btn2_prd_"+id).show();
}
</script>