<?php  
/*********************************************************/
function _ayuda($texto){
	?><a data-toggle="tooltip" data-placement="right" title="<?php echo $texto; ?>"><img src="img/icons/question.png"</a><?
}
function validar_codigo($campo,$pagina,$cargar){
	?>
    <div id="ajax_oper_<?php echo $campo; ?>"></div>
    <script language="javascript">
    $(document).ready(function(){  
        setTimeout(function() { $('input[name="<?php echo $campo; ?>"]').focus() }, 500);
    }); 
    function validar(e) {
      tecla = (document.all) ? e.keyCode : e.which;
      if (tecla==13){
		  AJAXPOST("mods/home/<?php echo $pagina; ?>","modo=existe&codigo="+document.getElementById("<?php echo $campo; ?>").value+"&funcion=ok_<?php 
		  echo $campo; ?>",document.getElementById("ajax_oper_<?php echo $campo; ?>"));
	  }
    } 
    function ok_<?php echo $campo; ?>(){
    	carg('<?php echo $cargar; ?>',''); 
	}
	</script>
    <?
}
function _t1($texto){
	?><p style="color: #3b3b3b; border-bottom: 1px solid #dddddd; padding:4px; margin:0; font-weight:bold; background-color:#fff; font-family:Arial, Helvetica, sans-serif; font-size:11px;"><?php echo $texto; ?></p><?php }
function _t2($texto){
	?><p style="color: #644c69; border-bottom: 1px solid #dddddd; padding:4px;  background-color:#F3FBFE; font-size:11px; font-family:Arial, Helvetica, sans-serif; margin:0;"><?php echo $texto; ?></p><?php }
function _p($datos){
	?><pre><?php print_r($datos); ?></pre><?php }
function Datos($Tabla,$Condicion,$Campos){
       $result=sql_query("SELECT ".$Campos." FROM ".$Tabla." where ".$Condicion);
       return $result;
}
function _u8d($texto){
	//return utf8_decode(_upa($texto));
	return utf8_decode($texto);
}
function _u8e($texto){
	//return utf8_encode(_upa($texto));
	return utf8_encode($texto);
}
function _upa($texto){
	return strtoupper($texto);
}
function _vs(){
	if($_SESSION["per_conectado"]["per_id"] == ""){
		_go("logout.php");
		exit();
	} 
} 
function _opc($codigo){ 
	global $_configuraciones;
	return $_configuraciones["cfg_".$codigo];
}
function _op($id){
	$operaciones = explode(",",$_SESSION["erp_slp"]["pfl_acciones"]);
	if(in_array($id,$operaciones)){
		return true;
	}else{
		return false;
	}
}
// limpiar textos de acentos
function _sa($s){ 
	$s = _sa2($s);
	$s = _comillas($s);
	return $s;
} 
// limpiar nombre archivo
function _sa2($string){
     $conservar = '0-9a-z.'; // juego de caracteres a conservar
     $regex = sprintf('~[^%s]++~i', $conservar); // case insensitive
     $string = preg_replace($regex, '', $string);
     return $string;
}
function _go($pagina){
	?><script language="javascript">location.href="<?php echo $url_base.$pagina; ?>";</script><?php }
function _al($alerta){
	?><script language="javascript">alert("<?php echo $alerta; ?>");</script><?php }
function _h($valor){
	$horas = intval($valor / 60);
	$minutos = $valor % 60;
	if($horas < 10){ $horas = "0".$horas; }
	if($minutos < 10){ $minutos = "0".$minutos; }
	return $horas.":".$minutos;
}
function _num($valor,$decimales = 0){ // Formato de n�mero 
	return number_format($valor,$decimales,",",".");
	
}
function _num2($valor,$decimales = 0){ // Formato de n�mero  
	return number_format($valor,$decimales,"",".");
	
}    
function _uf($valor){ // Formato de n�mero 
	return number_format($valor,2,",","."); 
}
function _porc($valor,$valor2){
	if($valor2 > 0){
		$porc = (($valor * 100) / $valor2);
	}else{
		$porc = 0;
	}
	return $porc;
}
function _t($valor){
	return htmlentities($valor);
}
function _dia($dia){
	$dias = array("Domingo","Lunes","Martes","Mi&eacute;rcoles","Jueves","Viernes","S&aacute;bado");
	return $dias[$dia];
}
function _fec($valor,$formato = "1"){
	$dias = array("Domingo","Lunes","Martes","Mi&eacute;rcoles","Jueves","Viernes","S&aacute;bado");
	switch($formato){
		// Fecha llega as�: 20130513 *************************************************************************************************************
		case "1": // Retorna: d/m/Y
			return substr($valor,6,2)."/".substr($valor,4,2)."/".substr($valor,0,4);
			break;
		case "2": // Retorna: Ymd
			return substr($valor,6,4).substr($valor,3,2).substr($valor,0,2);
			break;
		case "3": // Retorna: Y-m-d
			return substr($valor,6,4)."/".substr($valor,3,2)."/".substr($valor,0,2);
			break; 
		case "4": // Retorna: Jueves 4 de Mayo, 2014 
			$dia = substr($valor,6,2);
			$mes = substr($valor,4,2);
			$ano = substr($valor,0,4); 
			$diasemana = jddayofweek(cal_to_jd(CAL_GREGORIAN, $mes, $dia, $ano), 0); 
			return $dias[$diasemana]." ".intval($dia)." de "._mes(intval($mes)).", ".$ano; 
			break;
		
		// Fecha llega as�: 2014-11-09 *************************************************************************************************************
		case "5": // Retorna: 14/05/2013
			$valor = substr($valor,0,10);			
			if($valor == "0000-00-00" || $valor == "" || $valor == "0000/00/00"){ return ""; }
			return substr($valor,8,2)."/".substr($valor,5,2)."/".substr($valor,0,4); 
			break;
		case "52": // Retorna: 14/05/2013 a las 15:30
			$valor2 = substr($valor,0,10);
			if($valor2 == "0000-00-00" || $valor2 == "" || $valor2 == "0000/00/00"){ return ""; }
			if(substr($valor,11,5) <> "00:00"){
				$hora = " a las ".substr($valor,11,5);
			}else{
				$hora = "";
			}
			return substr($valor,8,2)."/".substr($valor,5,2)."/".substr($valor,0,4).$hora;
			break;
		case "6": // Retorna: 20130423
			return substr($valor,0,4).substr($valor,5,2).substr($valor,8,2);
			break; 
		case "8": // Retorna: Mayo 2014 
			$mes = substr($valor,5,2);
			$ano = substr($valor,0,4); 
			return _mes(intval($mes))." ".$ano; 
			break; 
		case "11": // Retorna: May-14 
			$mes = substr($valor,5,2);
			$ano = substr($valor,2,2); 
			return _mes2(intval($mes))."".$ano; 
			break;
		case "10": // Retorna: 14/05/13
			$valor = substr($valor,0,10);			
			if($valor == "0000-00-00" || $valor == "" || $valor == "0000/00/00"){ return ""; }
			return substr($valor,8,2)."/".substr($valor,5,2)."/".substr($valor,2,2);
			break; 
		case "12": // Retorna: Jueves 4 de Mayo, 2014 
			$dia = substr($valor,8,2);
			$mes = substr($valor,5,2);
			$ano = substr($valor,0,4); 
			$diasemana = jddayofweek(cal_to_jd(CAL_GREGORIAN, $mes, $dia, $ano), 0); 
			return $dias[$diasemana]." ".intval($dia)." de "._mes(intval($mes)).", ".$ano; 
		
		// Fecha llega as�: 12-11-2013 *************************************************************************************************************
		case "7": // Retorna: 201304
			return substr($valor,6,4).substr($valor,3,2);
			break; 
		case "9": // Retorna: 2013-12-28
			return substr($valor,6,4)."/".substr($valor,3,2)."/".substr($valor,0,2);
			break;
			
		// Fecha esta en SQL: Aug 2 2017 12:00AM ************************************************************************************************************* 	
		case "SQL":
			$partes = explode(" ",$valor);
			return $partes[2]."-"._mes3($partes[0])."-".$partes[1];
		case "SQL2":
			$partes = explode(" ",$valor);
			return $partes[1]."/"._mes3($partes[0])."/".$partes[2];
	}	
} 
function _pesochileno($valor,$modo){
	if(!$modo){
		return "$ "._num($valor);
	}else{
		$valor = str_replace("$","",$valor);
		$valor = str_replace(" ","",$valor);
		$valor = str_replace(".","",$valor);
		return $valor * 1;
	}
}
function _mes($num){
	$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	return $meses[$num-1];
} 
function _mes2($num){
	$meses = array("Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic");
	return $meses[$num-1];
}
function _mes3($mes){
	$meses = array("Jan" => "01", "Feb" => "02", "Mar" => "03", "Apr" => "04", "May" => "05", "Jun" => "06", "Jul" => "07", "Aug" => "08", "Sep" => "09", "Oct" => "10", "Nov" => "11", "Dec" => "12");
	return $meses[$mes];
}
function paginador($sql,$indice,$cantidad){ /***********************************************/
    $_SESSION["cantidad_global"] = $cantidad;

    $res = sql_query($sql);
    $num = mysqli_num_rows($res);

    $pages = intval($num / $cantidad);
    $resta = ($num % $cantidad);
    if($resta > 0){ $pages++; }
    $lugar = ($cantidad * ($indice - 1));

    $sql = $sql." limit ".$lugar.",".$cantidad;
    echo "<div style='display:none;'>".$sql."</div>";
		
	$inicio_paginador = $indice;		
	$inicio_paginador = $inicio_paginador - 10;
	if($inicio_paginador < 1){
		$inicio_paginador = 1;
	}
	$limite = $inicio_paginador + 25;
	if($limite > $pages){
		$limite = $pages;
	}	
    ?> 
    <div class="pagination">
        <table class="tablesorter">
        <tr>
            <td width="90">P&aacute;gina <b><?php echo $indice; ?></b> de <b><?php echo $pages; ?></b></td>
        <td>
            <ul id="pagination-flickr">
            <?php if($inicio_paginador > 1){
				?>
				<li><a href="javascript:hojear(1);">1</a></li>
				<li><p>...</p></li>
				<?php }
            for($i = $inicio_paginador;$i <= $limite;$i++){
                if($i == $indice){
                    ?><li class="active"><?php echo $i; ?></li><?php }else{
                    ?><li><a href="javascript:hojear(<?php echo $i; ?>);"><?php echo $i; ?></a></li><?php }
            }
			if($limite < $pages){
				?>
				<li><p>...</p></li>
				<li><a href="javascript:hojear(<?php echo $pages; ?>);"><?php echo $pages; ?></a></li>
				<?php }
            ?>
            </ul> 
        </td>
        <td width="1">
            <select onchange="javascript:registros(this.value);"> 
            <option value="1"	<?php if($cantidad == 1){ echo "selected"; } ?>>1</option>
            <option value="20"	<?php if($cantidad == 20){ echo "selected"; } ?>>20</option>
            <option value="50"	<?php if($cantidad == 50){ echo "selected"; } ?>>50</option>
            <option value="100"	<?php if($cantidad == 100){ echo "selected"; } ?>>100</option>
            <option value="500"	<?php if($cantidad == 500){ echo "selected"; } ?>>500</option>
            <option value="1000" <?php if($cantidad == 1000){ echo "selected"; } ?>>1000</option>
            <option value="100000000" <?php if($cantidad == 100000000){ echo "selected"; } ?>>Todos</option>
            </select>
        </td>
        </tr>
        </table>
    </div>
    <?php return $sql;
}

//Funcion para sumar dias de manera numerica y correlativa a una fecha
function suma_fechas($fecha,$ndias){
      if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha))
              list($dia,$mes,$a�o)=explode("/", $fecha);
      if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha))
              list($dia,$mes,$a�o)=explode("-",$fecha);
        $nueva = mktime(0,0,0, $mes,$dia,$a�o) + $ndias * 24 * 60 * 60;
        $nuevafecha=date("d/m/Y",$nueva);
      return ($nuevafecha);  
}
function suma_a_fecha_meses($fecha,$meses,$tipo = "Y-m-d"){
	if(preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha)){
		$fecha = $fecha;
	}else{
		$fecha = _fec($fecha,5);
	}
	$fecha = str_replace("/","-",$fecha);
	$nuevafecha = date($tipo,strtotime($fecha."+ ".$meses." month")); 
    return ($nuevafecha);  
}
function _cl($valor){
	$valor = str_ireplace("SELECT ","",$valor);
	$valor = str_ireplace("COPY ","",$valor);
	$valor = str_ireplace("DELETE ","",$valor);
	$valor = str_ireplace("DROP ","",$valor);
	$valor = str_ireplace("DUMP ","",$valor);
	$valor = str_ireplace("FROM ","",$valor);
	$valor = str_ireplace(" OR ","",$valor);
	$valor = str_ireplace("%","",$valor);
	$valor = str_ireplace("LIKE ","",$valor);
	$valor = str_ireplace("--","",$valor);
	$valor = str_ireplace("^","",$valor);
	$valor = str_ireplace("[","",$valor);
	$valor = str_ireplace("]","",$valor);
	$valor = str_ireplace("\\","",$valor);
	$valor = str_ireplace("!","",$valor);
	$valor = str_ireplace("�","",$valor);
	$valor = str_ireplace("?","",$valor);
	$valor = str_ireplace("=","",$valor);
	$valor = str_ireplace("&","",$valor);
	$valor = str_ireplace("'","�",$valor);
	$valor = str_ireplace('"','��',$valor);
	return $valor;
} 
function _comillas($string){  
	$string = str_replace("'","",$string);
	$string = str_replace('"','',$string);
	return $string; 
}
function maximo($datos){
	foreach($datos as $dato){ 
		if(is_array($dato)){
			foreach($dato as $dat){ 
				if($maximo < $dat){
					$maximo = $dat;
				}
			}
		}else{
			if($maximo < $dato){
				$maximo = $dato;
			}
		}
	} 
	return $maximo;
}
function tamano_archivo($peso , $decimales = 2 ){
	if($peso > 0){
		$clase = array(" Bytes", " KB", " MB", " GB", " TB"); 
		return round($peso/pow(1024,($i = floor(log($peso, 1024)))),$decimales ).$clase[$i];
	}else{
		return 0;
	}
}

function dia_semana ($dia, $mes, $ano) {
    $dias = array(7,1,2,3,4,5,6);
    return $dias[date("w", mktime(0, 0, 0, $mes, $dia, $ano))];
}
function cortar_palabras($texto_recibido,$maximo){
	$arreglo_partes = array();
	$contado_largo = 0;
	$texto = "";
	$texto_largo = strlen($texto_recibido);
	$texto_partes = explode(" ",$texto_recibido);
	foreach($texto_partes as $parte){ 
		$parte_largo = strlen($parte);
		if($maximo < ($contando_largo + $parte_largo)){
			$arreglo_partes[] = $texto;
			$contando_largo = $parte_largo;
			$texto = "";
		}else{
			$contando_largo += $parte_largo;
		}
		if($texto <> ""){ $texto.= " "; $contando_largo++; }
		$texto .= $parte; 
	}
	$arreglo_partes[] = $texto;
	return $arreglo_partes;
}
function agregar_ceros($numero,$largo){
	$largo_actual = strlen($numero);
	for($i = $largo_actual;$i < $largo;$i++){
		$final .= "0";
	}
	$final .= $numero;
	return $final;
}
function folio_contable($dato){
	return (substr($dato,4,10) * 1)."/".substr($dato,2,2);
}
function filtro_listado($label,$field,$campo,$modo){
	?><a href="javascript:chg_orden('<?php echo $field; ?>');" class="link_titulo"><?php echo $label; ?></a><?php
	if($field == $campo){
		if($modo == "asc"){
			?><img src="img/icons/down.png"><?php
		}else{
			?><img src="img/icons/up.png"><?php
		}
	}
}
function filtro_listado2($label,$field,$campo,$modo){
	?><a href="javascript:chg_orden2('<?php echo $field; ?>');" class="link_titulo"><?php echo $label; ?></a><?php
	if($field == $campo){
		if($modo == "asc"){
			?><img src="img/icons/down.png"><?php
		}else{
			?><img src="img/icons/up.png"><?php
		}
	}
}
function numtoletras($xcifra)
{ 
$xarray = array(0 => "Cero",
1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE", 
"DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE", 
"VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA", 
100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
);
//
$xcifra = trim($xcifra);
$xlength = strlen($xcifra);
$xpos_punto = strpos($xcifra, ".");
$xaux_int = $xcifra;
$xdecimales = "00";
if (!($xpos_punto === false))
	{
	if ($xpos_punto == 0)
		{
		$xcifra = "0".$xcifra;
		$xpos_punto = strpos($xcifra, ".");
		}
	$xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
	$xdecimales = substr($xcifra."00", $xpos_punto + 1, 2); // obtengo los valores decimales
	}
 
$XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
$xcadena = "";
for($xz = 0; $xz < 3; $xz++)
	{
	$xaux = substr($XAUX, $xz * 6, 6);
	$xi = 0; $xlimite = 6; // inicializo el contador de centenas xi y establezco el l&#65533;mite a 6 d&#65533;gitos en la parte entera
	$xexit = true; // bandera para controlar el ciclo del While	
	while ($xexit)
		{
		if ($xi == $xlimite) // si ya lleg&#65533; al l&#65533;mite m�ximo de enteros
			{
			break; // termina el ciclo
			}
 
		$x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
		$xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres d&#65533;gitos)
		for ($xy = 1; $xy < 4; $xy++) // ciclo para revisar centenas, decenas y unidades, en ese orden
			{
			switch ($xy) 
				{
				case 1: // checa las centenas
					if (substr($xaux, 0, 3) < 100) // si el grupo de tres d&#65533;gitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
						{
						}
					else
						{
						$xseek = $xarray[substr($xaux, 0, 3)]; // busco si la centena es n&#65533;mero redondo (100, 200, 300, 400, etc..)
						if ($xseek)
							{
							$xsub = subfijo($xaux); // devuelve el subfijo correspondiente (Mill&#65533;n, Millones, Mil o nada)
							if (substr($xaux, 0, 3) == 100) 
								$xcadena = " ".$xcadena." CIEN ".$xsub;
							else
								$xcadena = " ".$xcadena." ".$xseek." ".$xsub;
							$xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
							}
						else // entra aqu&#65533; si la centena no fue numero redondo (101, 253, 120, 980, etc.)
							{
							$xseek = $xarray[substr($xaux, 0, 1) * 100]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
							$xcadena = " ".$xcadena." ".$xseek;
							} // ENDIF ($xseek)
						} // ENDIF (substr($xaux, 0, 3) < 100)
					break;
				case 2: // checa las decenas (con la misma l&#65533;gica que las centenas)
					if (substr($xaux, 1, 2) < 10)
						{
						}
					else
						{
						$xseek = $xarray[substr($xaux, 1, 2)];
						if ($xseek)
							{
							$xsub = subfijo($xaux);
							if (substr($xaux, 1, 2) == 20)
								$xcadena = " ".$xcadena." VEINTE ".$xsub;
							else
								$xcadena = " ".$xcadena." ".$xseek." ".$xsub;
							$xy = 3;
							}
						else
							{
							$xseek = $xarray[substr($xaux, 1, 1) * 10];
							if (substr($xaux, 1, 1) * 10 == 20)
								$xcadena = " ".$xcadena." ".$xseek;
							else	
								$xcadena = " ".$xcadena." ".$xseek." Y ";
							} // ENDIF ($xseek)
						} // ENDIF (substr($xaux, 1, 2) < 10)
					break;
				case 3: // checa las unidades
					if (substr($xaux, 2, 1) < 1) // si la unidad es cero, ya no hace nada
						{
						}
					else
						{
						$xseek = $xarray[substr($xaux, 2, 1)]; // obtengo directamente el valor de la unidad (del uno al nueve)
						$xsub = subfijo($xaux);
						$xcadena = " ".$xcadena." ".$xseek." ".$xsub;
						} // ENDIF (substr($xaux, 2, 1) < 1)
					break;
				} // END SWITCH
			} // END FOR
			$xi = $xi + 3;
		} // ENDDO
 
		if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
			$xcadena.= " DE";
 
		if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
			$xcadena.= " DE";
 
		// ----------- esta l&#65533;nea la puedes cambiar de acuerdo a tus necesidades o a tu pa&#65533;s -------
		if (trim($xaux) != "")
			{
			switch ($xz)
				{
				case 0:
					if (trim(substr($XAUX, $xz * 6, 6)) == "1")
						$xcadena.= "UN BILLON ";
					else
						$xcadena.= " BILLONES ";
					break;
				case 1:
					if (trim(substr($XAUX, $xz * 6, 6)) == "1")
						$xcadena.= "UN MILLON ";
					else
						$xcadena.= " MILLONES ";
					break;
				case 2:
					if ($xcifra < 1 )
						{
						$xcadena = "CERO PESOS";
						}
					if ($xcifra >= 1 && $xcifra < 2)
						{
						$xcadena = "UN PESO";
						}
					if ($xcifra >= 2)
						{
						$xcadena.= " PESOS"; // 
						}
					break;
				} // endswitch ($xz)
			} // ENDIF (trim($xaux) != "")
		// ------------------      en este caso, para M&#65533;xico se usa esta leyenda     ----------------
		$xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
		$xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles 
		$xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
		$xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles 
		$xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
		$xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
		$xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
	} // ENDFOR	($xz)
	return trim($xcadena);
} // END FUNCTION
 
 
function subfijo($xx)
	{ // esta funci&#65533;n regresa un subfijo para la cifra
	$xx = trim($xx);
	$xstrlen = strlen($xx);
	if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
		$xsub = "";
	//	
	if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
		$xsub = "MIL";
	//
	return $xsub;
} // END FUNCTION 
function dias_transcurridos($fecha_i,$fecha_f){
        $dias      = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias = floor($dias);            
        return $dias;
}

function thumbnail($imagen){
	if(file_exists("../../upload/".$_SESSION["key_id"]."/".$imagen)){
		if(!file_exists("../../thumbnail/".$_SESSION["key_id"]."/".$imagen)){ 
			
			// load image and get image size
			$img = imagecreatefromjpeg("../../upload/".$_SESSION["key_id"]."/".$imagen);
			$width = imagesx( $img );
			$height = imagesy( $img );
			
			// calculate thumbnail size
			$new_width = 400;
			$new_height = floor( $height * ( $new_width / $width ) );
			
			// create a new tempopary image
			$tmp_img = imagecreatetruecolor( $new_width, $new_height );

			// copy and resize old image into new image 
			imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

			// save thumbnail into a file
			imagejpeg( $tmp_img,"../../thumbnail/".$_SESSION["key_id"]."/".$imagen,100);
		}
	}
}
function calcula_meses_transcurridos($inicio,$termino){
	$ano_ini = substr($inicio,0,4);
	$mes_ini = substr($inicio,5,2);
	$ano_ter = substr($termino,0,4);
	$mes_ter = substr($termino,5,2);

	for($ano = $ano_ini;$ano <= $ano_ter;$ano++){
		if($ano < $ano_ter){
			for($mes = $mes_ini;$mes <= 12;$mes++){
				$meses++;
			}
		}else{
			for($mes = $mes_ini;$mes <= $mes_ter;$mes++){
				$meses++;
			}
		}
		$mes_ini = 1;
	}
	/*
	echo "Inicio: ".$inicio."<br>";
	echo "Termino: ".$termino."<br>";
	echo "Meses: ".$meses."<br><br>";
	*/
	return $meses;
}
?>