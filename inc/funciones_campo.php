<?php 
function campo_dinamico($tipo,$nombre_campo,$opciones,$valor,$url_base){  
	$opciones = explode(",",$opciones);
	switch($tipo){
        case "texto";
        	?><input type="text" name="<? echo $nombre_campo; ?>" class="campos w10" value="<? echo $valor; ?>"><?
        	break;

        case "textarea";
        	?><textarea rows="3" name="<? echo $nombre_campo; ?>" class="campos w10"><? echo $valor; ?></textarea><?
        	break;

        case "numero":
        	?><input type="text" name="<? echo $nombre_campo; ?>" class="campos numero" value="<? echo $valor; ?>"><?
        	break;

        case "fecha":
        	?><input type="text" name="<? echo $nombre_campo; ?>" class="campos fecha" value="<? echo $valor; ?>"><?
        	break;

        case "fecha2":
            ?><input type="text" name="<? echo $nombre_campo; ?>" class="campos fecha2" value="<? echo $valor; ?>"><?
            break;

        case "archivo":
            if($valor <> ""){
                ?>
                Archivo actual (<a href="upload/<? echo $_SESSION["key_id"]."/".$valor; ?>" target="_blank">Abrir</a><br>
                <input type="hidden" readonly value="<?php echo $valor; ?>" class="campos">
                <?
            }
            ?>
			<form method="post" name="upload_<?php echo $nombre_campo; ?>" id="upload_form_<?php echo $nombre_campo; ?>" enctype="multipart/form-data" action="<?php echo $url_base; ?>mods/home/upload.php"> 
                <input type="file" name="archivo_<? echo $nombre_campo; ?>" id="archivo_<? echo $nombre_campo; ?>">
                
                <div class="uploading_<? echo $nombre_campo; ?>" style="display:none;"><img src="img/uploading.gif"/></div>
                <div id="procesando_<? echo $nombre_campo; ?>">
                    <input type="hidden" name="<? echo $nombre_campo; ?>" value="<?php echo $valor; ?>" class="campos">
                </div>
            </form> 

            <script type="text/javascript"> 
            $(document).ready(function() {
                $('#archivo_<? echo $nombre_campo; ?>').on('change',function(){  
                    $('#upload_form_<? echo $nombre_campo; ?>').ajaxForm({
                        target:'#procesando_<? echo $nombre_campo; ?>',
                        data: { campo: "<? echo $nombre_campo; ?>" },
                        beforeSubmit:function(e){ 
                            $('.uploading_<? echo $nombre_campo; ?>').show();
                        },
                        success:function(e){  
                            $('.uploading_<? echo $nombre_campo; ?>').hide();
                        },
                        error:function(e){ 
                        }
                    }).submit(); 
                });
            });
            </script>
        	<?
        	break;

        case "imagen":
            $imagen = ($valor == "")?"img/sin_imagen.jpg":"upload/".$_SESSION["key_id"]."/".$valor;
            ?>
            <img src="<?php echo $imagen; ?>" id="<?php echo $nombre_campo; ?>_ruta" class="img-thumbnail" style="width:250px !important;">
            <form method="post" name="upload_form_<?php echo $nombre_campo; ?>" id="upload_form_<?php echo $nombre_campo; 
            ?>" enctype="multipart/form-data" action="<?php echo $url_base; ?>mods/home/upload.php"> 
                <input type="file" name="archivo_<? echo $nombre_campo; ?>" id="archivo_<? echo $nombre_campo; ?>" accept='image/*'>
                
                <div class="uploading_<? echo $nombre_campo; ?>" style="display:none;"><img src="img/uploading.gif"/></div>
                <div id="procesando_<? echo $nombre_campo; ?>">
                    <input type="hidden" name="<? echo $nombre_campo; ?>" value="<?php echo $valor; ?>" class="campos" />
                </div>
            </form> 
            <?
            break;

        case "imagenes":
        	?><input type="text" name="<? echo $nombre_campo; ?>" class="campos fecha w10" value="<? echo $valor; ?>"><?
        	break;

        case "check":
        	?><input type="text" name="<? echo $nombre_campo; ?>" class="campos fecha w10" value="<? echo $valor; ?>"><?
        	break;

        case "radio":
        	?><input type="text" name="<? echo $nombre_campo; ?>" class="campos fecha w10" value="<? echo $valor; ?>"><?
        	break;

        case "select":
        	?><select name="<? echo $nombre_campo; ?>" class="campos buscador w10">
        		<option value=""></option>
        		<?php
        		foreach($opciones as $opcion){
        			?><option value="<? echo $opcion; ?>" <? if($opcion == $valor){ echo "selected"; } 
        			?>><? echo $opcion; ?></option><?
        		}
        		?>
        	</select><?
        	break;
    }
}
function nombre_categoria($codigo,$nombre = ""){
    $res = sql_categorias("*"," and cat.cat_codigo = '$codigo'","","");
    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_array($res)){
            if($row["cat_padre"] <> ""){
                $nombre = " > ".$row["cat_nombre"].$nombre;
                $nombre = nombre_categoria($row["cat_padre"],$nombre);
            }else{
                $nombre = $row["cat_nombre"].$nombre;
            } 
        }
    }
    return $nombre;
}
?>