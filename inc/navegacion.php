<?php
// Insumos..........................................................................
$permisos_set[] = array("Insumos","Inventario de Insumos","invi",1,0,0,0,0);
$permisos_set[] = array("Insumos","Consumo de Insumos","cons",0,0,0,0,1);
$permisos_set[] = array("Insumos","Toma de Inventario de Insumos","tomi",0,0,0,0,1);

// Recursos..........................................................................
$permisos_set[] = array("Recursos","Personas","pers",1,1,1,1,0);
$permisos_set[] = array("Recursos","Proveedores","auxi",1,1,1,1,0);

// Trabajos..........................................................................
$permisos_set[] = array("Trabajos","Mantención de Activos","mant",1,1,1,1,0);
$permisos_set[] = array("Trabajos","Garantías de Activos","gara",1,1,1,1,0);
$permisos_set[] = array("Trabajos","Controles de Activos en Ubicaciones","cont",1,1,1,1,0);
$permisos_set[] = array("Trabajos","Administración de Etiquetas","etiq",1,1,1,1,0); 
$permisos_set[] = array("Trabajos","Impresión de Etiquetas","etim",0,0,0,0,1);

// Operaciones..........................................................................
$permisos_set[] = array("Operaciones","Préstamo de Activos","pres",0,0,0,0,1);
$permisos_set[] = array("Operaciones","Asignación de Activos","asig",0,0,0,0,1);
$permisos_set[] = array("Operaciones","Devolución de Activos","devo",0,0,0,0,1);
$permisos_set[] = array("Operaciones","Reubicación de Activos","reub",0,0,0,0,1);
$permisos_set[] = array("Operaciones","Ingreso de Activos/Insumos","ingr",0,0,0,0,1);
$permisos_set[] = array("Operaciones","Baja de Activo","baja",0,0,0,0,1);

// Inventario..........................................................................
$permisos_set[] = array("Inventario","Tomas de Inventario","toma",0,0,0,0,1);

// Informes..........................................................................
$permisos_set[] = array("Informes","Resumen de Ingresos por Período","inf1",1,0,0,0,0);
$permisos_set[] = array("Informes","Resumen de Salidas por Período","inf2",1,0,0,0,0);
$permisos_set[] = array("Informes","Productos por Ubicación","inf4",1,0,0,0,0);
$permisos_set[] = array("Informes","Productos por Clasificación","inf5",1,0,0,0,0);
//$permisos_set[] = array("Informes","Activos por Tipos","inf6",1,0,0,0,0);
$permisos_set[] = array("Informes","Activos Prestados y Asignados","inf16",1,0,0,0,0);
$permisos_set[] = array("Informes","Activos con Responsables","inf18",1,0,0,0,0); 
$permisos_set[] = array("Informes","Insumos por Ubicación","inf8",1,0,0,0,0);
$permisos_set[] = array("Informes","Insumos por Clasificación","inf9",1,0,0,0,0); 
$permisos_set[] = array("Informes","Stock Crítico de Insumos","inf11",1,0,0,0,0);
$permisos_set[] = array("Informes","Depreciación de Activo Fijo","inf19",1,0,0,0,0);
$permisos_set[] = array("Informes","Depreciación del Período","inf20",1,0,0,0,0);
$permisos_set[] = array("Informes","Depreciación por Período","inf23",1,0,0,0,0);
$permisos_set[] = array("Informes","Depreciación por Período Tributario","inf24",1,0,0,0,0);
$permisos_set[] = array("Informes","Kardex de Producto","inf12",1,0,0,0,0);
$permisos_set[] = array("Informes","Histórico de Transacciones","inf13",1,0,0,0,0);
$permisos_set[] = array("Informes","Informe Bitácora Persona","inf26",1,0,0,0,0);
$permisos_set[] = array("Informes","Mantenimientos de Activos Fijos","inf14",1,0,0,0,0);
$permisos_set[] = array("Informes","Vencimientos de Garantías","inf15",1,0,0,0,0);
$permisos_set[] = array("Informes","Vencimientos de Mantenimientos","inf17",1,0,0,0,0);
$permisos_set[] = array("Informes","Informe de Altas","inf21",1,0,0,0,0);
$permisos_set[] = array("Informes","Informe de Bajas","inf22",1,0,0,0,0);
$permisos_set[] = array("Informes","Activos Fijos Cargados","inf25",1,0,0,0,0);

// Tablas.......................................................................... 
$permisos_set[] = array("Tablas","Lugares","lug",1,1,1,1,0);
$permisos_set[] = array("Tablas","Ubicaciones","ubi",1,1,1,1,0);
$permisos_set[] = array("Tablas","Insumos","insu",1,1,1,1,0);
$permisos_set[] = array("Tablas","Productos","pro",1,1,1,1,0);
$permisos_set[] = array("Tablas","Clasificación de Productos","cat",1,1,1,1,0);  
$permisos_set[] = array("Tablas","Unidades de Productos","unid",1,1,1,1,0);
$permisos_set[] = array("Tablas","Centros de Costo","ccos",1,1,1,1,0);

// Configuración..........................................................................
$permisos_set[] = array("Configuración","Mi Empresa","mie",1,0,1,0,0); 
$permisos_set[] = array("Configuración","Tabla IPC","ipc",1,0,1,0,0); 
$permisos_set[] = array("Configuración","Campos Personalizados","cam",1,1,1,1,0);
$permisos_set[] = array("Configuración","Usuarios","usua",1,1,1,1,0);
$permisos_set[] = array("Configuración","Perfiles de Usuarios","pfl",1,1,1,1,0);

// PDA..........................................................................
$permisos_set[] = array("PDA","Crear Producto","mob8",0,0,0,0,1);
$permisos_set[] = array("PDA","Levantar Activos","mob9",0,0,0,0,1);
$permisos_set[] = array("PDA","Consultar Producto","mob1",0,0,0,0,1);
$permisos_set[] = array("PDA","Buscar un TAG","mob2",0,0,0,0,1);
$permisos_set[] = array("PDA","Control de Activos Fijos","mob3",0,0,0,0,1);
$permisos_set[] = array("PDA","Control con RFID","mob4",0,0,0,0,1);
$permisos_set[] = array("PDA","Entregar AF/Insumos","mob5",0,0,0,0,1);
$permisos_set[] = array("PDA","Devolución de Activos","mob6",0,0,0,0,1);
$permisos_set[] = array("PDA","Asociar Etiquetas","mob7",0,0,0,0,1);
?>