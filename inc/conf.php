<?php
session_start(); 
error_reporting(0); 

/*****************************/ 
require("db.php");      
$cnx = mysqli_connect($app_db_host,$app_db_usuario,$app_db_password) or die("Falla en conexion MySQL");
mysqli_select_db($cnx,$app_db_basededatos); 
mysqli_query($cnx,"SET NAMES 'utf8'"); 

/*****************************/
require("funciones.php"); 
require("funciones_dibujo.php"); 
require("funciones_campo.php");
require("widgets.php"); 
require("navegacion.php"); 
/*****************************/   
require("sqls/erp_activos.php");
require("sqls/erp_auxiliar.php");
require("sqls/erp_bodega.php");
require("sqls/erp_campos.php"); 
require("sqls/erp_categoria.php");  
require("sqls/erp_centro_costo.php"); 
require("sqls/erp_configuracion.php");  
require("sqls/erp_concepto.php"); 
require("sqls/erp_control.php");  
require("sqls/erp_garantia.php");  
require("sqls/erp_movimiento.php");  
require("sqls/erp_perfil.php"); 
require("sqls/erp_persona.php");  
require("sqls/erp_producto.php"); 
require("sqls/erp_toma_inventario.php"); 
require("sqls/erp_ubicacion.php"); 
require("sqls/erp_unidad.php"); 

// Validaciones que se deben realizar siempre
require("validaciones.php"); 

$res_cfg = mysqli_query($cnx,"select * from configuraciones ");
if(mysqli_num_rows($res_cfg) > 0){
	while($row_cfg = mysqli_fetch_array($res_cfg)){
		$_configuraciones = $row_cfg; 
	} 
}

// Todos los tipos
$_tipos["A"] = "AJUSTE";
$_tipos["E"] = "ENTRADA";
$_tipos["S"] = "SALIDA";

// Tipos activos
$res_ta = mysqli_query($cnx,"select * from activos_tipos order by tac_orden asc");
while($row_ta = mysqli_fetch_array($res_ta)){
	$_tipo_activos[] = $row_ta["tac_codigo"];
	if($row_ta["tac_activo"]){
		// Mostraremos solo los Tipos de Activos (Activos) en el menu, incluso para el Prohibido
		$_tipo_activos_menu[] = $row_ta["tac_codigo"];
	}
	$_tipo_activos_icono[$row_ta["tac_codigo"]] = $row_ta["tac_icono"];
	$_tipo_activo[$row_ta["tac_codigo"]] = $row_ta["tac_nombre"];
	$_tipo_cuenta_contable[$row_ta["tac_codigo"]] = $row_ta["tac_cuenta_contable"];
	if($row_ta["tac_depreciable"]){
		if($_tipo_activos_depreciables <> ""){ $_tipo_activos_depreciables.= ","; }
		$_tipo_activos_depreciables .= "'".$row_ta["tac_codigo"]."'";
	}
	$_tipo_activos_lineal[$row_ta["tac_codigo"]] = $row_ta["tac_vu_dep_lineal"] * 12;
	$_tipo_activos_acele[$row_ta["tac_codigo"]] = $row_ta["tac_vu_dep_acele"] * 12;

	// Activos..........................................................................
	$permisos_set[] = array("Activos",$row_ta["tac_nombre"],"act_".strtolower($row_ta["tac_codigo"]),1,0,0,0,0); 
}  

// Conceptos Ingreso
$_concepto_ing[1] = "Apertura";
$_concepto_ing[9] = "Compra";
$_concepto_ing[11] = "Traspaso";
$_concepto_ing[12] = "Reciclado";
$_concepto_ing[13] = "Reparado";
$_concepto_ing[14] = "Modernización";

// Conceptos Baja
$_concepto_baj[2] = "Daño Irreparable";
$_concepto_baj[15] = "Obsolescencia";
$_concepto_baj[7] = "Venta";
$_concepto_baj[8] = "Merma o Hurto";
$_concepto_baj[16] = "Recambio";
$_concepto_baj[6] = "Consumo";

// Conceptos Ajuste
$_concepto_aju[3] = "Préstamo";
$_concepto_aju[10] = "Asignación";
$_concepto_aju[4] = "Devolución";
$_concepto_aju[5] = "Reubicación";
$_concepto_aju[17] = "Toma Inventario";

foreach($_concepto_ing as $codigo=>$valor){ $_conceptos[$codigo] = $valor; }
foreach($_concepto_baj as $codigo=>$valor){ $_conceptos[$codigo] = $valor; }
foreach($_concepto_aju as $codigo=>$valor){ $_conceptos[$codigo] = $valor; }

// Todas las bodegas 
$res_cfg = sql_bodegas("*",""); 
if(mysqli_num_rows($res_cfg) > 0){ 
	while($row_cfg = mysqli_fetch_array($res_cfg)){
		$_bodegas_codigos[] = $row_cfg["bod_codigo"];
		$_bodegas[$row_cfg["bod_id"]] = $row_cfg["bod_nombre"];
		$_bodegas_codigo[$row_cfg["bod_id"]] = $row_cfg["bod_codigo"];
	}
}
$_bodegas[0] = "- Sin bodega -";

// Todas los categorias
$res_cfg = sql_categorias("*","");
if(mysqli_num_rows($res_cfg) > 0){
	while($row_cfg = mysqli_fetch_array($res_cfg)){
		$_categorias_codigos[] = $row_cfg["cat_codigo"];
		$_categorias[$row_cfg["cat_id"]] = $row_cfg["cat_nombre"];
		$_categorias_codigo[$row_cfg["cat_id"]] = $row_cfg["cat_codigo"];
		$_categorias_largo[$row_cfg["cat_id"]] = $row_cfg["cat_nombre_largo"];
		$_categorias_cod[$row_cfg["cat_codigo"]] = $row_cfg["cat_id"];
	}
}
$_categorias[0] = "- Sin clasificación -";

// Todas los operaciones
$res_cfg = mysqli_query($cnx,"select * from personas_operaciones 
where per_id = '".$_SESSION["per_conectado"]["per_id"]."' ");
if(mysqli_num_rows($res_cfg) > 0){
	while($row_cfg = mysqli_fetch_array($res_cfg)){
		$_operaciones[] = $row_cfg["ope_id"];
	} 
}

// Todas los personas
$res_cfg = sql_personas("*","");
if(mysqli_num_rows($res_cfg) > 0){
	$_personas[0] = "Sin Persona"; 
	while($row_cfg = mysqli_fetch_array($res_cfg)){
		$_personas[$row_cfg["per_id"]] = $row_cfg["per_nombre"];
		$_personas_perfil[$row_cfg["per_id"]] = $row_cfg["pfl_nombre"];
	} 
}

// Todas las tablas grandes 
if($_SESSION["load_grandes_tablas"] == ""){
	$_SESSION["load_grandes_tablas"] = "S";

	$res_cfg = sql_auxiliares("*","");
	if(mysqli_num_rows($res_cfg) > 0){
		while($row_cfg = mysqli_fetch_array($res_cfg)){
			$_SESSION["auxiliares"][$row_cfg["aux_id"]] = $row_cfg["aux_nombre"];
		} 
	}
}

// Todas los unidades
$res_cfg = sql_unidades("*","");
if(mysqli_num_rows($res_cfg) > 0){
	while($row_cfg = mysqli_fetch_array($res_cfg)){
		$_unidades_codigos[] = $row_cfg["ubi_codigo"];
		$_unidades[$row_cfg["uni_id"]] = $row_cfg["uni_nombre"];
		$_unidades_cod[$row_cfg["uni_codigo"]] = $row_cfg["uni_id"];
	}
}
$_unidades[0] = "- Sin unidad -";

// Todas los ubicaciones
$res_cfg = sql_ubicaciones("*","");
if(mysqli_num_rows($res_cfg) > 0){ 
	while($row_cfg = mysqli_fetch_array($res_cfg)){
		$_ubicaciones_codigos[] = $row_cfg["ubi_codigo"];
		$_ubicaciones[$row_cfg["ubi_id"]] = $row_cfg["ubi_nombre"];
		$_ubicaciones_codigo[$row_cfg["ubi_id"]] = $row_cfg["ubi_codigo"];
		$_ubicaciones_control[$row_cfg["ubi_id"]] = $row_cfg["ubi_ultimo_control"];
		$_ubicaciones_bod[$row_cfg["ubi_id"]] = $row_cfg["bod_nombre"]; 
		$_ubicaciones_bod_id[$row_cfg["ubi_id"]] = $row_cfg["bod_id"]; 
		$_ubicaciones_ubi_id[$row_cfg["ubi_codigo"]] = $row_cfg["ubi_id"];
	} 
} 
$_ubicaciones[0] = "- Sin ubicación -"; 

// Todas los centros de costo
$res_cfg = sql_centro_costo("*","");
if(mysqli_num_rows($res_cfg) > 0){
	while($row_cfg = mysqli_fetch_array($res_cfg)){
		$_centro_costos_codigos[] = $row_cfg["cco_codigo"];
		$_centro_costos[$row_cfg["cco_id"]] = $row_cfg["cco_codigo"]." - ".$row_cfg["cco_nombre"];
		$_centro_costos_corto[$row_cfg["cco_id"]] = $row_cfg["cco_codigo"];
		$_centro_costos_cod[$row_cfg["cco_codigo"]] = $row_cfg["cco_id"];
	} 
}  

if($_SESSION["per_conectado"]["per_id"] <> ""){
	// Info Persona conectada 
	$res = sql_personas("*","and per.per_id = '".$_SESSION["per_conectado"]["per_id"]."'");
	if(mysqli_num_rows($res) > 0){
		$row = mysqli_fetch_array($res); 
		$_SESSION["per_conectado"] = $row; 
	}
	if($row["per_id"] == 1){
		unset($opciones_persona);
		foreach($permisos_set as $pem){
			$opciones_persona[] = $pem[2]."_ver";
			$opciones_persona[] = $pem[2]."_crear";
			$opciones_persona[] = $pem[2]."_editar";
			$opciones_persona[] = $pem[2]."_eliminar";
			$opciones_persona[] = $pem[2]."_realizar";
		} 
	}else{  
		$opciones_persona = explode(",",$_SESSION["per_conectado"]["pfl_navegacion"]);
	}
}
?>