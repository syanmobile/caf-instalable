<?php 
// Funciones MotorSQL -----------------------------------------
class MotorSQL{
  private $conexion; 
  private $total_consultas;
  public function MotorSQL(){ 
    if(!isset($this->conexion)){
      $this->conexion = (mysqli_connect("","","","")) or die(mysqli_error()); 
    }
  }
  public function consulta($consulta){ 
    $this->total_consultas++; 
    $resultado = mysqli_query($this->conexion,$consulta);
    if(!$resultado){ 
      echo 'MySQL Error: ' . mysqli_error();
      exit;
    }
    return $resultado;
  }
  public function fetch_array($consulta){
   return mysqli_fetch_array($consulta);
  }
  public function num_rows($consulta){
   return mysqli_num_rows($consulta);
  }
  public function insert_id(){
   return mysqli_insert_id($this->conexion);
  }
  public function getTotalConsultas(){
   return $this->total_consultas; 
  }
}

$db = new MotorSQL();
$db2 = new MotorSQL();
?>