<?php 
function cabecera_filtro($campo,$titulo,$campo_act,$modo_act){
	?>
    <table>
    <tr>
    <td><a href="javascript:ordenar('<?php echo $campo; ?>');" style="color:#FF9 !important;"><?php echo $titulo; ?></a></td>
	<td width="1"><?php
	if($campo == $campo_act){
		if($modo_act == "desc"){
			?><img src="img/icons/up.png" style=" float:right;"><?php
		}else{
			?><img src="img/icons/down.png" style=" float:right;"><?php
		}
	}
	?></td>
    </tr>
    </table>
    <?php
}
function paginador_nuevo($sql,$indice,$cantidad = 20){ /***********************************************/ 
    $_SESSION["cantidad_global"] = $cantidad;
  
    $res = sql_query($sql);
    $num = mysqli_num_rows($res);

    $pages = intval($num / $cantidad);
    $resta = ($num % $cantidad);
    if($resta > 0){ $pages++; }
    $lugar = ($cantidad * ($indice - 1));

    $sql = $sql." limit ".$lugar.",".$cantidad;
    echo "<div style='display:none;'>".$sql."</div>";
		
	$inicio_paginador = $indice;		
	$inicio_paginador = $inicio_paginador - 10;
	if($inicio_paginador < 1){
		$inicio_paginador = 1;
	}
	$limite = $inicio_paginador + 25;
	if($limite > $pages){
		$limite = $pages;
	}	
    ?>  
    <table width="100%" id="pagination-flickr" style="background-color:#E1F9FB; border:1px solid #3FF;"> 
    <tr>
        <td width="130" style="text-align:right; padding-right:10px;">P&aacute;gina <b><? echo $indice; ?></b> de <b><? echo $pages; ?></b>:</td>
		<?
		if($inicio_paginador > 1){
			?>
			<td width="1"><a href="javascript:hojear(1);">1</a></td>
			<td width="30"><p>...</p></td>
			<?
		}
		for($i = $inicio_paginador;$i <= $limite;$i++){
			if($i == $indice){
				?><td width="1" class="active4"><? echo $i; ?></td><?
			}else{
				?><td width="1"><a href="javascript:hojear(<? echo $i; ?>);"><? echo $i; ?></a></td><?
			}
		}
		if($limite < $pages){
			?>
			<td width="30"><p>...</p></td>
			<td width="1"><a href="javascript:hojear(<? echo $pages; ?>);"><? echo $pages; ?></a></td>
			<?
		}
		?>
        <td>&nbsp;</td>
        <td width="1">Registros:</td> 
        <td width="1" style="padding:0px 5px;"><strong><?php echo _num($num); ?></strong></td>
        <td style="text-align:right;" width="1">
            <select onchange="javascript:registros(this.value);"> 
            <option value="1"	<? if($cantidad == 1){ echo "selected"; } ?>>1</option>
            <option value="20"	<? if($cantidad == 20){ echo "selected"; } ?>>20</option>
            <option value="50"	<? if($cantidad == 50){ echo "selected"; } ?>>50</option>
            <option value="100"	<? if($cantidad == 100){ echo "selected"; } ?>>100</option>
            <option value="500"	<? if($cantidad == 500){ echo "selected"; } ?>>500</option>
            <option value="1000" <? if($cantidad == 1000){ echo "selected"; } ?>>1000</option>
            <option value="100000000" <? if($cantidad == 100000000){ echo "selected"; } ?>>Todos</option>
            </select>
        </td>
    </tr>
    </table> 
    <?php
    return $sql;
} 
function construir_breadcrumb($titulo){
	global $cnx;
	_vs(); 
	?>
	<div class="breadcrumb" style="margin:1px 0 2px 0 !important;"> 
		<table width="100%">
		<tr> 
			<td>
				<? echo date("d"); ?> de <? echo _mes(date("m")); ?>, <? echo date("Y"); ?> &nbsp;|&nbsp; <b id="txt"></b> &nbsp;|&nbsp; V.1.91 <?
				$moneda_sec = _opc("moneda_sec");
				$moneda_valor = _num(_opc("moneda_ult_cambio"),_opc("moneda_sec_decimal"));
				if($moneda_sec <> ""){
					?>
					| &nbsp;<b>Valor <? echo $moneda_sec; ?></b>: <a href="javascript:cambiar_cambio();"><? echo $moneda_valor; ?></a>
					<?
				}
				?>
			</td>
			<td style="text-align:right;">
				Usuario: <strong><?php echo $_SESSION["per_conectado"]["per_nombre"]; ?></strong>
			</td>
			<td width="100" style="text-align: right; display: none;">
				| Asistente: <a href="javascript:asistencia_btn();" id="asistente_button"><?php
				echo ($_SESSION["per_asistente"])?"ON":"OFF";
				?></a>
			</td>
		</tr>
		</table>
	</div>
   <script>
   	function cambiar_cambio(){
   		AJAXPOST(url_base+modulo_base+"cfg_moneda_cambio.php","modo=formulario",document.getElementById("modGeneral_lugar"),false,function(){            
	        $('#modGeneral').modal('show');

	    });
   	}
   	function grabar_cambio(){
   		var a = $(".campos4").fieldSerialize();
   		AJAXPOST(url_base+modulo_base+"cfg_moneda_cambio.php",a+"&modo=grabar",document.getElementById("modGeneral_lugar"),false,function(){            
	        $('#modGeneral').modal('show');

	    });
   	}
   	function asistencia_btn(){ 
		var div = document.getElementById("asistente_button");
		AJAXPOST("mods/home/per_ajax.php","modito=asistente",div);
   	}
	function startTime() {
		var today = new Date();
		var h = today.getHours();
		var m = today.getMinutes();
		var s = today.getSeconds();
		m = checkTime(m);
		s = checkTime(s);
		document.getElementById('txt').innerHTML =
		h + ":" + m + ":" + s;
		var t = setTimeout(startTime, 500);
	}
	function checkTime(i) {
		if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
		return i;
	}
	   startTime();
	</script>
	<?
	if($titulo <> ""){
		?>
		<h3 style="margin-top:10px;"><?php echo $titulo; ?></h3>
		<?php 
	}

	$sql = "insert into navegacion_log (log_per_id,log_detalle,log_fechahora) values 
	('".$_SESSION["per_conectado"]["per_id"]."','$titulo','".date("Y-m-d H:i:s")."')";
	$res = mysqli_query($cnx,$sql);
} 
function icon_doc($file,$tamano = 16){ 
	$nombre = explode(".",$file);
	$extension = strtolower($nombre[count($nombre) - 1]);
	switch($extension){
		case "doc":
		case 'docx':
			?><img src="img/<?php echo $tamano; ?>px/doc.png"><?php break;
		case 'xls':
			?><img src="img/<?php echo $tamano; ?>px/xls.png"><?php break;
		case 'xlsx':
			?><img src="img/<?php echo $tamano; ?>px/xlsx.png"><?php break;
		case 'txt':
			?><img src="img/<?php echo $tamano; ?>px/txt.png"><?php break;
		case 'ppt':
		case 'pptx':
			?><img src="img/<?php echo $tamano; ?>px/ppt.png"><?php break;
		case 'jpg':
		case 'jpeg':
			?><img src="img/<?php echo $tamano; ?>px/jpg.png"><?php break;
		case 'gif':
			?><img src="img/<?php echo $tamano; ?>px/gif.png"><?php break;
		case 'bmp':
			?><img src="img/<?php echo $tamano; ?>px/bmp.png"><?php break;
		case 'png':
			?><img src="img/<?php echo $tamano; ?>px/png.png"><?php break;
		case 'pdf':
			?><img src="img/<?php echo $tamano; ?>px/pdf.png"><?php break;
		case 'zip':
			?><img src="img/<?php echo $tamano; ?>px/zip.png"><?php break;
		case 'rar':
			?><img src="img/<?php echo $tamano; ?>px/rar.png"><?php break;
		
	}
}
function construir_boton($pagina,$datos,$icono,$titulo = "",$tipo_dibujo = 1,$clase_extra = ""){ 
	global $url_base; 
	switch($tipo_dibujo){
		case "1":
			?> 
			<a href="javascript:carg('<?php echo $pagina; ?>','<?php echo $datos; ?>');" title="<?php echo $titulo; ?>" class="btn_wms">
			<div class="btn_sistema <? echo $clase_extra; ?>">
			<img src="<? echo $url_base; ?>img/icons/<?php echo iconos_definicion($icono); ?>">
			</div>
			</a>
			<?php
			break;
		case "2":
			?>
			<a href="javascript:carg('<?php echo $pagina; ?>','<?php echo $datos; ?>');" title="<?php echo $titulo; ?>" class="btn_wms">
			<div class="btn_sistema <? echo $clase_extra; ?>">			
			<table>
			<tr>
			<td width="1"><img src="<? echo $url_base; ?>img/icons/<?php echo iconos_definicion($icono); ?>"></td>
			<td style="padding-left:3px;"><span><?php echo $titulo; ?></span></td>
			</tr>
			</table>
			</div>
			</a>
			<?php
			break;
		case "3":
			?>
			<a href="javascript:save();" title="<?php echo $titulo; ?>" class="btn_wms">
			<div class="btn_sistema <? echo $clase_extra; ?>">
			<table>
			<tr>
			<td width="1"><img src="<? echo $url_base; ?>img/icons/<?php echo iconos_definicion($icono); ?>"></td>
			<td style="padding-left:3px;"><span><?php echo $titulo; ?></span></td>
			</tr>
			</table>
			</div>
			</a>
			<?php
			break;
		case "4":
			?>
			<a href="javascript:<?php echo $pagina; ?>" title="<?php echo $titulo; ?>" class="btn_wms">
			<div class="btn_sistema <? echo $clase_extra; ?>">
			<table>
			<tr>
			<td width="1"><img src="<? echo $url_base; ?>img/icons/<?php echo iconos_definicion($icono); ?>"></td>
			<?php
			if($datos == ""){
				?><td style="padding-left:3px;"><span><?php echo $titulo; ?></span></td><?
			}
			?>
			</tr>
			</table> 
			</div>
			</a>
			<?php
			break;
		case "5":
			?>
			<a href="javascript:<?php echo $pagina; ?>" title="<?php echo $titulo; ?>" class="btn_wms">
			<div class="btn_sistema <? echo $clase_extra; ?>">
			<img src="<? echo $url_base; ?>img/icons/<?php echo iconos_definicion($icono); ?>">
			</div>
			</a>
			<?php
			break;
		case "6":
			?>
			<a href="javascript:carg3('<?php echo $pagina; ?>','<?php echo $datos; ?>');" title="<?php echo $titulo; ?>" class="btn_wms">
			<table>
			<tr>
			<td width="1"><img src="<? echo $url_base; ?>img/icons/<?php echo iconos_definicion($icono); ?>"></td>
			<td style="padding-left:3px;"><span><?php echo $titulo; ?></span></td>
			</tr>
			</table>
			</a>
			<?php
			break;
		case "7":
			?>
			<a href="javascript:carg3('<?php echo $pagina; ?>','<?php echo $datos; ?>');" title="<?php echo $titulo; ?>" class="btn_wms">
			<div class="btn_sistema <? echo $clase_extra; ?>">
			<img src="<? echo $url_base; ?>img/icons/<?php echo iconos_definicion($icono); ?>"> 
			</div>
			</a>
			<?php
			break;
		case "8":
			?>
			<a href="javascript:carg3('<?php echo $pagina; ?>','<?php echo $datos; ?>');" title="<?php echo $titulo; ?>" class="btn_wms">
			<div class="btn_sistema <? echo $clase_extra; ?>">
			<table>
			<tr>
			<td width="1"><img src="<? echo $url_base; ?>img/icons/<?php echo iconos_definicion($icono); ?>"></td>
			<td style="padding-left:3px;"><span><?php echo $titulo; ?></span></td>
			</tr>
			</table>    
			</div>
			</a>
			<?php
			break;
	} 
}
function construir_boton2($pagina,$datos,$icono,$titulo,$tipo_dibujo = 1,$clase_extra = ""){ 
	global $url_base; 
	switch($tipo_dibujo){
		case "1":
			?> 
			<a href="javascript:carg2('<?php echo $pagina; ?>','<?php echo $datos; ?>');" title="<?php echo $titulo; ?>">
            <div class="btn_sistema <? echo $clase_extra; ?>">
			<img src="<? echo $url_base; ?>img/icons/<?php echo iconos_definicion($icono); ?>">
			</div>
            </a>
			<?php
			break;
		case "2":
			?>
			<a href="javascript:carg2('<?php echo $pagina; ?>','<?php echo $datos; ?>');" title="<?php echo $titulo; ?>">
            <div class="btn_sistema <? echo $clase_extra; ?>">
			<img src="<? echo $url_base; ?>img/icons/<?php echo iconos_definicion($icono); ?>">
            <div style="float:right; margin:2px 0px 2px 4px;"><span><?php echo $titulo; ?></span></div>
			</div>
            </a>
			<?php
			break;
		case "3":
			?>
			<a href="javascript:save();" title="<?php echo $titulo; ?>">
            <div class="btn_sistema <? echo $clase_extra; ?>">
			<img src="<? echo $url_base; ?>img/icons/<?php echo iconos_definicion($icono); ?>">
            <div style="float:right; margin:2px 0px 2px 4px;"><span><?php echo $titulo; ?></span></div>
			</div>
            </a>
			<?php
			break;
		case "4":
			?>
			<a href="javascript:<?php echo $pagina; ?>" title="<?php echo $titulo; ?>">
            <div class="btn_sistema <? echo $clase_extra; ?>">
			<img src="<? echo $url_base; ?>img/icons/<?php echo iconos_definicion($icono); ?>">
            <?php
			if($datos == ""){
				?><div style="float:right; margin:2px 0px 2px 4px;"><span><?php echo $titulo; ?></span></div><?php
			}
			?>
			</div>
            </a>
			<?php
			break;
		case "5":
			?>
			<a href="javascript:<?php echo $pagina; ?>" title="<?php echo $titulo; ?>">
            <div class="btn_sistema <? echo $clase_extra; ?>">
			<img src="<? echo $url_base; ?>img/icons/<?php echo iconos_definicion($icono); ?>">
            </div>
            </a>
			<?php
			break;
	}
} 
$_SESSION["iconos"] = array(
	array("ayuda","question.png"),
	array("buscar","magnifier.png"),
	array("crear","plus.png"),
	array("editar","pencil.png"),
	array("enviar","mail__arrow.png"),
	array("eliminar","cross.png"),
	array("filtrar","funnel.png"),
	array("grabar","disk_black.png"),
	array("imprimir","printer.png"),
	array("camion","truck.png"),
	array("refrescar","arrow_circle_double.png"),
	array("check","tick.png"),
	array("check2","tick_circle.png"),
	array("cancelar","slash.png"),
	array("pregunta","question.png"),
	array("carro","shop.png"),
	array("menos","minus.png"),
	array("menos2","minus_circle.png"),
	array("calendario","calendar_day.png"),
	array("tiempo","clock_frame.png"),
	array("izquierda","arrow_180.png"),
	array("derecha","arrow.png"),
	array("comentario","balloon.png"),
	array("campana","bell.png"),
	array("tag","bookmark.png"),
	array("libros","books.png"),
	array("caja","box.png"),
	array("maletin","briefcase.png"),
	array("calculadora","calculator.png"),
	array("camara","camera.png"),
	array("nota","card__pencil.png"),
	array("notas","cards_address.png"),
	array("checklist","clipboard__pencil.png"),
	array("alerta","exclamation.png"),
	array("carpeta","folder_open.png"),
	array("configurar","gear.png"),
	array("martillo","hammer.png"),
	array("casa","home.png"),
	array("fotos","images.png"),
	array("info","information_frame.png"),
	array("llave","key.png"),
	array("ayuda","lifebuoy.png"),
	array("candado","lock.png"),
	array("iman","magnet.png"),
	array("pin","pin.png"),
	array("enchufe","plug.png"),
	array("barcode","barcode.png"),
	array("puzzle","puzzle.png"),
	array("nota_editar","receipt_pencil.png"),
	array("notas_editar","receipts_pencil.png"),
	array("hoja","report.png"),
	array("estrella","star.png"),
	array("stick_editar","sticky_note__pencil.png"),
	array("tabla","table.png"),
	array("posicion","position.png"),
	array("llave2","wrench.png"),
	array("importar","importar.png"),
	array("limpiar","broom.png"),
	array("peso","chosen-sprite.png")
);
function iconos_definicion($elegido){
	$iconos = $_SESSION["iconos"];
	foreach($iconos as $item){
		if($item[0] == $elegido){
			$icono = $item[1];
		}
	}
	return $icono;
} 
function envio_correo($post_info){
	// Proceso de envio de correo (28-08-2014)--------------------------------------------
	$body = $post_info["correo_mensaje"];
	$asunto = $post_info["correo_asunto"];
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= 'From: '.$post_info["correo_de"]."\r\n"; 
	if($post_info["correo_cc"] <> ""){
		$headers .= 'Cc: '.$post_info["correo_cc"]."\r\n";
	}
	?>   
	<div class="alert alert-success"> 
		<strong>Se envi&oacute; correo, a los siguientes contactos:</strong><br /><?php 
		$nombre_adic = $post_info["nombre_adi"];
		$correo_adic = $post_info["correo_adi"];
		if(count($nombre_adic) > 0){
			for($i = 0;$i < count($nombre_adic);$i++){
				if($nombre_adic[$i] <> "" && $correo_adic[$i] <> ""){
					echo "<br><b>".$nombre_adic[$i]; ?></b> (<?php echo $correo_adic[$i]; ?>)<?php if($correos <> ""){
						$correos .= ",";
					}
					$correos .= $correo_adic[$i];
				}
			}
		}
		@mail($correos, $asunto, $body, $headers); 
		?> 
	</div>  
	<?php
	//-----------------------------------------------------------------------------------------
}
?>