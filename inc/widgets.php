<?
// Widgets del inicio

//0
$widgets[] = array(
	"Insumos con Stock Critico",
	"Listado con los insumos que están con stock crítico",
	3
);

//1
$widgets[] = array(
	"Insumos por Clasificación",
	"Listado totalizado de insumos por Clasificación",
	3
);

//2
$widgets[] = array(
	"Responsables con Activos Fijos",
	"Listado de personas con activos fijos prestados/asignados",
	3
);

//3
$widgets[] = array(
	"Activos Fijos por Clasificación",
	"Listado totalizado de activos fijos por Clasificación",
	3
);

//4
$widgets[] = array(
	"Indicador Stock Crítico Insumos",
	"Gráfico que indica cuantos insumos tienen stock crítico",
	4
);

//5
$widgets[] = array(
	"Situación Vencimiento Mantenciones",
	"Gráfico que indica cuantas mantenciones están al día, atrasadas y por vencer",
	4
);

//6
$widgets[] = array(
	"Situación de sus Activos",
	"Gráfico que indica los estados en que se encuentran sus activos fijos",
	4
); 

//7
$widgets[] = array(
	"Situación Vencimiento de Garantías",
	"Gráfico que indica los estados en que se encuentran sus activos fijos",
	4
);

//8
$widgets[] = array(
	"Activos Fijos por Ubicación",
	"Listado totalizado de activos fijos por Ubicación",
	4
);

//9
$widgets[] = array(
	"Activos Fijos por Tipo",
	"Listado totalizado de activos fijos por Tipo",
	4
);
?>