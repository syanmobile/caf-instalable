        <div class="left-sidebar-pro">
            <nav id="sidebar">
                <div class="sidebar-header" style="background-color: #fff; padding: 0px;">
                    <img src="upload/demostracion/syan.png" width="100">
                </div>
                <div class="left-custom-menu-adp-wrap">
                    <ul class="nav navbar-nav left-sidebar-menu-pro">
                        <li class="nav-item">
                            <a href="index.php" class="nav-link"><i class="fa big-icon fa-home"></i> <span class="mini-dn">Home</span></a>
                        </li>
                        <?
                        if(
                          in_array("act_equ_ver",$opciones_persona) or 
                          in_array("act_her_ver",$opciones_persona) or 
                          in_array("act_dig_ver",$opciones_persona) or 
                          in_array("act_sum_ver",$opciones_persona)
                        ){
                          ?>
                        <li class="nav-item">
                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-folder-open"></i> <span class="mini-dn">ACTIVOS</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX"> 
                                <?php
                                  if(in_array("act_equ_ver",$opciones_persona)){
                                    ?><a href="javascript:carg3('acf_listado.php','&fil_tipo=EQU');">Equipos</a><?
                                  } 
                                  if(in_array("act_her_ver",$opciones_persona)){
                                    ?><a href="javascript:carg3('acf_listado.php','&fil_tipo=HER');">Herramientas</a><?
                                  } 
                                  if(in_array("act_dig_ver",$opciones_persona)){
                                    ?><a href="javascript:carg3('acf_listado.php','&fil_tipo=DIG');">Digitales</a><?
                                  } 
                                  if(in_array("act_sum_ver",$opciones_persona)){
                                    ?><a href="javascript:carg3('acf_listado.php','&fil_tipo=SUM');">Suministros</a><?
                                  }
                                  ?>  
                            </div>
                        </li> 
                        <?
                        }
                        ?>
                    </ul>
                </div>
            </nav>
        </div>