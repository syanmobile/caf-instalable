<? 
function sql_activos_fijos_rapido($c,$agregado = "",$sintaxis = "",$show = ""){
	global $cnx;
	$sql = "select $c from activos_fijos acf ";
	$sql .= "left join productos pro on pro.pro_id = acf.acf_producto "; 
	$sql .= "left join ubicaciones ubi on ubi.ubi_id = acf.acf_ubicacion "; 
	//$sql .= "left join personas per on per.per_id = acf.acf_responsable ";
	$sql .= " where 1 ";
	if($show <> ""){ echo $sql.$agregado."<br>"; } 
	if($sintaxis == ""){ return mysqli_query($cnx,$sql.$agregado); }else{ return $sql.$agregado; } 
}
function sql_activos_fijos_rapido2($c,$agregado = "",$sintaxis = "",$show = ""){
	global $cnx;
	$sql = "select $c from activos_fijos acf ";
	//$sql .= "left join productos pro on pro.pro_id = acf.acf_producto "; 
	//$sql .= "left join ubicaciones ubi on ubi.ubi_id = acf.acf_ubicacion "; 
	//$sql .= "left join personas per on per.per_id = acf.acf_responsable ";
	$sql .= " where 1 ";
	if($show <> ""){ echo $sql.$agregado."<br>"; } 
	if($sintaxis == ""){ return mysqli_query($cnx,$sql.$agregado); }else{ return $sql.$agregado; } 
}
function sql_activos_fijos($c,$agregado = "",$sintaxis = "",$show = ""){
	global $cnx;
	$sql = "select $c from activos_fijos acf ";
	$sql .= "left join productos pro on pro.pro_id = acf.acf_producto "; 
	$sql .= "left join ubicaciones ubi on ubi.ubi_id = acf.acf_ubicacion "; 
	$sql .= "left join personas per on per.per_id = acf.acf_responsable ";
	$sql .= " where 1 ";
	if($show <> ""){ echo $sql.$agregado."<br>"; } 
	if($sintaxis == ""){ return mysqli_query($cnx,$sql.$agregado); }else{ return $sql.$agregado; } 
}
function activo_fijo_estado($estado,$extra = ""){
	if($extra == ""){
		switch($estado){
			case 0: ?><span class="label label-danger">No Disponible</span><? break;
			case 1: ?><span class="label label-success">Disponible</span><? break;
			case 2: ?><span class="label label-info">Prestado</span><? break;
			case 3: ?><span class="label label-warning">En Mantenimiento</span><? break;
			case 4: ?><span class="label label-danger">De Baja</span><? break;
			case 5: ?><span class="label label-warning">Requiere Mantenimiento</span><? break; 
			case 6: ?><span class="label label-info">Asignado</span><? break;
		} 
	}else{
		switch($estado){
			case 0: $texto = "No Disponible"; break;
			case 1: $texto = "Disponible"; break;
			case 2: $texto = "Prestado"; break;
			case 3: $texto = "En Mantenimiento"; break;
			case 4: $texto = "De Baja"; break;
			case 5: $texto = "Requiere Mantenimiento"; break; 
			case 6: $texto = "Asignado"; break;
		}
		return $texto;
	}
}

function sql_acf_mantenimientos($c,$agregado = "",$sintaxis = "",$show = ""){
	global $cnx;
	$sql = "select $c from mantenciones_activos mnt ";
	$sql.= "left join activos_fijos acf on mnt.man_acf_id = acf.acf_id ";
	$sql .= "left join ubicaciones ubi on ubi.ubi_id = acf.acf_ubicacion "; 
	$sql.= "left join productos pro on pro.pro_id = acf.acf_producto ";
	$sql .= " where 1 ";
	if($show <> ""){ echo $sql.$agregado."<br>"; } 
	if($sintaxis == ""){ return mysqli_query($cnx,$sql.$agregado); }else{ return $sql.$agregado; } 
} 

function sql_acf_documentos($c,$agregado = "",$sintaxis = "",$show = ""){
	global $cnx;
	$sql = "select $c from documentos_activos doc ";
	$sql.= "left join activos_fijos acf on doc.doc_acf_id = acf.acf_id ";
	$sql.= "left join productos pro on pro.pro_id = acf.acf_producto ";
	$sql .= " where 1 ";
	if($show <> ""){ echo $sql.$agregado."<br>"; } 
	if($sintaxis == ""){ return mysqli_query($cnx,$sql.$agregado); }else{ return $sql.$agregado; } 
} 
function sql_insumos($c,$agregado = "",$sintaxis = "",$show = ""){
	global $cnx;
	$sql = "select $c from insumos ";
	$sql .= " where 1 ";
	if($show <> ""){ echo $sql.$agregado."<br>"; }
	if($sintaxis == ""){ return mysqli_query($cnx,$sql.$agregado); }else{ return $sql.$agregado; } 
}


function id_tipo_activo($codigo){
	$res = "select * from activos_tipos where tac_codigo = '$codigo' ";
	if(mysqli_num_rows($res) > 0){
		$datos = mysqli_fetch_array($res);
        return $datos["tac_id"];

	}else{
		return false;
	}
}

function sql_etiquetas($c,$agregado = "",$sintaxis = "",$show = ""){
	global $cnx;
	$sql = "select $c from etiquetas eti ";
	$sql.= "left join activos_fijos acf on eti.eti_activo = acf.acf_id ";
	$sql.= "left join productos pro on pro.pro_id = acf.acf_producto ";
	$sql .= " where 1 ";
	if($show <> ""){ echo $sql.$agregado."<br>"; }
	if($sintaxis == ""){ return mysqli_query($cnx,$sql.$agregado); }else{ return $sql.$agregado; } 
}
?>