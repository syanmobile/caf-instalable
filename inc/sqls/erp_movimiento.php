<?php 
function sql_movimientos($c,$agregado = "",$sintaxis = "",$show = ""){
	global $cnx;
	$sql  = "select $c from movimientos mov "; 
	 
	$sql .= "left join auxiliares aux 	on aux.aux_id	= mov.mov_auxiliar ";  
	
	$sql .= "where 1 ";     
	if($show <> ""){ echo $sql.$agregado."<br>"; } 
	if($sintaxis == ""){ return mysqli_query($cnx,$sql.$agregado); }else{ return $sql.$agregado; } 
}
function sql_movimientos_detalle($c,$agregado = "",$sintaxis = "",$show = ""){
	global $cnx;
	$sql  = "select $c from movimientos_detalle det ";	
	
	$sql .= "left join movimientos mov on det.det_mov_id = mov.mov_id ";  
	
	$sql .= "left join auxiliares aux 	on aux.aux_id	= mov.mov_auxiliar "; 
	
	$sql .= "left join productos pro 	on pro.pro_id	= det.det_producto ";  
	
	$sql .= "where 1 ";    
	if($show <> ""){ echo $sql.$agregado."<br>"; } 
	if($sintaxis == ""){ return mysqli_query($cnx,$sql.$agregado); }else{ return $sql.$agregado; } 
}
function sql_movimientos_detalle_activo($c,$agregado = "",$sintaxis = "",$show = ""){
	global $cnx;
	$sql  = "select $c from movimientos_detalle det ";	
	
	$sql .= "left join movimientos mov on det.det_mov_id = mov.mov_id ";  
	
	$sql .= "left join auxiliares aux 	on aux.aux_id	= mov.mov_auxiliar "; 
	
	$sql .= "left join productos pro 	on pro.pro_id	= det.det_producto "; 
	
	$sql .= "left join activos_fijos acf 	on acf.acf_id	= det.det_acf_id ";  
	
	$sql .= "where 1 ";    
	if($show <> ""){ echo $sql.$agregado."<br>"; } 
	if($sintaxis == ""){ return mysqli_query($cnx,$sql.$agregado); }else{ return $sql.$agregado; } 
}  

function sql_movimientos_detalle_clasif($c,$agregado = "",$sintaxis = "",$show = ""){
	global $cnx;
	$sql  = "select $c from movimientos_detalle det ";	
	$sql .= "left join movimientos mov on det.det_mov_id = mov.mov_id ";  
	$sql .= "left join auxiliares aux 	on aux.aux_id	= mov.mov_auxiliar "; 
	$sql .= "left join productos pro 	on pro.pro_id	= det.det_producto "; 
	$sql .= "left join categorias cat 	on cat.cat_id = pro.pro_categoria ";
	$sql .= "where 1 ";    
	if($show <> ""){ echo $sql.$agregado."<br>"; } 
	if($sintaxis == ""){ return mysqli_query($cnx,$sql.$agregado); }else{ return $sql.$agregado; } 
}

function movimiento_existe($auxiliar,$folio,$tipo,$concepto){
	$res = sql_movimientos("*"," and mov.mov_auxiliar = '$auxiliar' and mov.mov_folio = '$folio' and mov.mov_tipo = '$tipo' and mov.mov_concepto = '$concepto' ");
	if(mysqli_num_rows($res) > 0){
		return false;
	}else{
		return true;
	}
} 
?>