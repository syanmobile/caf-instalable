<?php
define('FPDF_FONTPATH','font/');
require('../../pdf/WriteTag.php');

//$pdf=new FPDF();
$pdf=new PDF_WriteTag();
$pdf->AliasNbPages();

//------------------------------------------------------------------/

$pdf->AddPage();
$pdf->SetTitle($titulo_pdf);
  
$pdf->Image($_SESSION["key_info"]["logo"],10,8,30,0,'PNG');

$pdf->SetFont("Arial","b",10); 
$pdf->Cell(0,4,utf8_decode(_opc("razon_social")),0,1,'R');

$pdf->SetFont("Arial","",8);
$pdf->Cell(0,4,utf8_decode(_opc("rut")),0,1,'R');
$pdf->Cell(0,4,utf8_decode(_opc("direccion").", "._opc("ciudad")),0,1,'R');
$pdf->Cell(0,4,utf8_decode(_opc("telefonos")),0,1,'R');
$pdf->Cell(0,4,utf8_decode(_opc("website")),0,1,'R');
$pdf->Ln(2);

if($titulo_pdf <> ""){
	$pdf->SetFont("Arial","b",14);
	$pdf->Cell(0,7,utf8_decode($titulo_pdf),1,1,"C");
	$pdf->Ln(3);
}
?>