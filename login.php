<?php
require("inc/conf.php");
require("deteccion_mobil.php");

if($_POST){ 
	$res = mysqli_query($cnx,"select * from personas per 
	where (per.per_clave = '".md5($_POST["clave"])."' or per.per_clave = '".$_POST["clave"]."') and per.per_usuario = '".$_POST["usuario"]."' 
	and per.per_elim = 0");
	if(mysqli_num_rows($res) > 0){
		$row = mysqli_fetch_array($res);
 
		if(strtolower($_POST["usuario"]) <> "prohibido"){
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'From: noresponder@controlaf.cl';
			$email_message = "Te pillamos po compadre!";
			@mail("rgarcia@syan.cl", "Usuario ".$_REQUEST["usuario"]." accedio, desde la IP ".$_SERVER['REMOTE_ADDR'].", en la base de datos: ".strtoupper($_REQUEST["db"]), $email_message, $headers); 
		}
		 
		$_SESSION["per_conectado"] = $row;
		$_SESSION["per_asistente"] = $row["per_asistente"];
		$_SESSION["mod_conectado"] = 1;
		$_SESSION["mod_conectado_base"] = "home"; 
		$_SESSION["web"] = $url_base;

		// Limpio los conectados...
        $coneccion_termino = new DateTime(date("Y-m-d H:i:s"));
        $res2 = mysqli_query($cnx,"select * from conectados");
        if(mysqli_num_rows($res2) > 0){ 
            while($row2 = mysqli_fetch_array($res2)){
                $coneccion_inicio = new DateTime($row2["con_fechahora"]);
                $interval = $coneccion_inicio->diff($coneccion_termino);
                if(($interval->format('%H') * 1) > 0 || ($interval->format('%d') * 1) > 0){
                    $res3 = mysqli_query($cnx,"delete from conectados where con_per_id = ".$row2["con_per_id"]);
                }
            }
        }
        // Inserto al usuario en los conectados
        $res4 = mysqli_query($cnx,"delete from conectados where con_per_id = ".$_SESSION["per_conectado"]["per_id"]);
        $res4 = mysqli_query($cnx,"insert into conectados (con_per_id ,con_fechahora) 
                    values (".$_SESSION["per_conectado"]["per_id"].",'".date("Y-m-d H:i:s")."')");

        $res4 = mysqli_query($cnx,"update personas set per_ultimo_acceso = '".date("Y-m-d H:i:s")."' 
                    where per_id = '".$_SESSION["per_conectado"]["per_id"]."' ");

		_go("index.php");
	}else{
		_al("Login incorrecto");
		_go("logout.php");
	}
} 
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
    <link rel="shortcut icon" href="img/favicon.png">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>CONTROL DE ACTIVOS</title>
    <!-- Bootstrap core CSS -->
    <link href="css/theme/bootstrap_default.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
	<style> 
    body { font-family: Helvetica, Arial, sans-serif;  
        width: 100%;  
   background: url(img/back.jpg) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
    }
    pre, code {
        font-family: Monaco, Menlo, Consolas, "Courier New", monospace;
        font-size: 12px;
        color: #333;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
      }
    pre { border: 1px solid #CCC; background-color: #EEE; color: #333; padding: 10px; overflow: scroll; }
    code { padding: 2px 4px; background-color: #F7F7F9; border: 1px solid #E1E1E8; color: #D14; } 
	</style>
</head>
<body>
 
<div class="container">  
	<form class="form-signin" action="login.php" method="post">
		<?
		if(_opc('nombre_empresa') == ""){
			?> 
            <div style="font-size: 32px; font-weight: bold; text-align: center; color: #015190;">Control de Activos</div>
			<div class="alert alert-danger" style="text-align: center;">
				<strong>CUENTA INCORRECTA</strong><br>
				Contáctarse a <a href="mailto:soporte@controldeactivos.cl">soporte@controldeactivos.cl</a>, ya que no cuenta con una base de datos activa
			</div>
			<br><br>
			<?
		}else{
			?>
            <div style="text-align:center; font-size: 44px; font-weight: bold; padding-bottom: 20px;">
                <img src="upload/<? echo $_REQUEST["db"]; ?>/<? echo $_REQUEST["db"]; ?>.png" width="100%">
            </div>
            <div style="font-size: 32px; font-weight: bold; text-align: center; color: #015190;">Control de Activos</div>
            <label for="inputEmail" class="sr-only">Usuario Persona</label>
            <input id="usuario" name="usuario" class="form-control" placeholder="Usuario Persona" required autofocus>
            <label for="inputPassword" class="sr-only">Clave</label>
            <input id="inputPassword" name="clave" class="form-control" placeholder="Ingrese clave" required type="password">
            <input type="hidden" name="base" class="form-control" value="<?php echo $_REQUEST["base"]; ?>">
            <button class="btn btn-lg btn-primary btn-block" type="submit">Acceder</button>			
			<?	
			if($mobile_browser == 2){
				?><script language="javascript">location.href="mobile/";</script><?
			}
		}
		?> 
        <div style="padding:10px; text-align:center;"> 
            <div style="font-size:12px; margin-bottom:3px;"><b>CAF</b>, una solución desarrollado por</div>
            <table width="100%">
            <tr>
            <td style="padding-right: 5px; text-align: right; width: 50%;"><a href="http://www.auditsoft.cl/" target="_blank"><img src="img/auditsoft.png" height="25"></a></td>            
            <td><a href="http://www.syanmobile.cl/" target="_blank"><img src="img/syan.png" height="25"></a></td>
			</tr>
			</table>
        </div> 
      </form>  
</div>

<script src="js/jquery-1.11.1.min.js"></script>  
<script src="js/jquery.Rut.js"></script> 
</body>
</html>